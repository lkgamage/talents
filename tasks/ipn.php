<?php
include('../lib/autoload.php');



// production
$endpoint_secret = 'whsec_amNXdNxQTyOZwujq6JDutaTRxilFbTw4';

// testing
$endpoint_secret = 'whsec_K7KF3B7odhqRzqCD67a4C8YycVyvlBep';

$payload = @file_get_contents('php://input');
$signature = !empty($_SERVER['HTTP_STRIPE_SIGNATURE']) ? $_SERVER['HTTP_STRIPE_SIGNATURE'] : NULL;



try {
    $event = \Stripe\Webhook::constructEvent(
        $payload, $signature, $endpoint_secret
    );
} catch(\UnexpectedValueException $e) {
    // Invalid payload
	applog("IPN recived with bad payload, abort");
    http_response_code(400);
    exit();
} catch(\Stripe\Exception\SignatureVerificationException $e) {
    // Invalid signature
	applog("IPN recived with bad pay load, abort");
    http_response_code(400);
    exit();
}

// processing event
$event = json_decode($payload);
applog("IPN Event ID ".$event->id." of {$event->type} received");





if($event->type == 'customer.subscription.updated' || $event->type == 'customer.subscription.created'){
	// update current subscription status
	applog("{$event->type} processing {$event->data->object->id}");
	
	// get invoice 
	$invoice = App::getInvoice($event->data->object->latest_invoice);
	
	if(!empty($invoice)){
		applog("Invoice found {$invoice->id}, Stripe ubscription status is {$event->data->object->status}");
		
		if($event->data->object->status == 'active'){
			
			if(empty($event->data->object->cancel_at_period_end)){
				
				/// cancel_at_period_end :true
				
				applog("Active subscription, update properties");
				
				// newly activated
				$subscription = $invoice->getSubscription();
				//$subscription->begin_date = ($event->data->object->status == 'trialing') ? date("Y-m-d",$event->data->object->trial_end ) : date('Y-m-d', $event->data->object->current_period_start);					
				$subscription->end_date = date('Y-m-d', $event->data->object->current_period_end);
				$subscription->active = 1;
				$subscription->save();
				applog("Subscription id {$subscription->id} activated");
				
				// update invoice status
				if($invoice->status != DataType::$INVOICE_PAID) {
					$invoice->status = DataType::$INVOICE_PAID;
					$invoice->amount = $event->data->object->plan->amount/100;
					$invoice->save();
					applog("Invoice {$invoice->id} marked as paid");
					
					// check payment status
					$payment = $invoice->getPayment();
					if(empty($payment)){
						$payment = new Payment();
						$payment->invoice_id = $invoice->id;
					}
					
					$payment->status =DataType::$PAYMENT_SUCCESS;
					$payment->success = 1;
					$payment->amount = ($event->data->object->plan->amount/100);
					//$payment->charge_id = $intent->charges->data[0]->id;
					$payment->save();
					applog("Payment {$payment->id} marked as paid");
				}
				
				$catsub = $subscription->getCategorySubscription();
				if(!empty($catsub)){
					$catsub->activate();
					applog("Category subscription {$catsub->id} activated as default");
				}
				else{
					$catsub = new CategorySubscription();
					$catsub->category_id = $subscription->category_id;
					$catsub->stripe_id = $event->data->object->id;
					$catsub->save();
					$catsub->activate();
					applog("Category subscription {$catsub->id} created and activated as default");
				}
				
				applog("Sendng notifications to client");
				// send notification to customer
				$com = new ComEngine();

				$desc = $invoice->getDesciption();
				$contact = $invoice->getContact();

				$subject = $desc['subject'];
				
				$line1 = $desc['desc'];
				$line2 = "Your card has been charged {$desc['amount']}.";

				$com->paymentEmail ($contact, $subject, $line1, $line2 );
				applog("Confirmation email sent to {$contact['email']}");
				// notified to customer

				$message = $desc['desc'];

				$com->paymentSMS ($contact, $message );
				applog("Confirmation SMS sent to {$contact['phone']}");

				} 
				else{
					// cancel_at_period_end :true
					applog("Scubscription scheduled to be canceled at the end of period");
					// nothing to do here
				}
			
			
		}
		elseif($event->data->object->status == 'incomplete'){
			// could be a new subscription
			// since we have a in-active subscription attached to the 
			// invoice, we have nothing to do here			
			applog("Subscription logic has been handled in frontend, ignore incomplete event");
		}
		elseif($event->data->object->status == 'trialing'){
			// this is also handled at the front end
			applog("Subscription logic has been handled in frontend, ignore trialing event");
		}
		
		
	}
	else{
		// no invoice found
		// new subscription record to be created
		applog("No invoice found for {$event->data->object->latest_invoice}, creating new invoice and subscription");
		$catsub = App::getCategorySubscription($event->data->object->id);	
		
		if(empty($catsub)){
			// Category subscripton not found, This is likely not happned
			applog("Category subscrption {$event->data->object->id} not found, abort ");
			exit();			
		}
		
		$package = App::getPackage($event->data->object->plan->id);
		
		if(empty($package)){
			applog("Given price ID {$event->data->object->plan->id} does not exists, abort");
			exit();
		}
		
		$category = $catsub->getCategory();
		$profile = $category->getProfile();
		
		// create subscription record
		$subscription = new Subscription();
		$subscription->category_id = $category->id;
		$subscription->package_id = $package->id;
		$subscription->sub_date = 'NOW';
		//$subscription->begin_date = ($event->data->object->status == 'trialing') ? date("Y-m-d",$event->data->object->trial_end ) : date('Y-m-d', $event->data->object->current_period_start);
		$subscription->exp_date = date('Y-m-d', $event->data->object->current_period_end);
		$subscription->active = ($event->data->object->status  == 'active') ? 1 : 0;
		$subscription->jobs = $package->jobs;
		$subscription->manage = $package->manage;
		$subscription->catsub_id = $catsub->id;
		$subscription->save();
		applog("Subscripton id {$subscription->id} created");
		
		// create invoice
		$invoice = new Invoice();
		$invoice->customer_id = $profile->customer_id;
		$invoice->profile_id = $profile->id;
		$invoice->subscription_id = $subscription->id;
		$invoice->currency = 'USD';
		$invoice->amount = $event->data->object->plan->amount/100;
		$invoice->stripe_id = $event->data->object->latest_invoice;
		$invoice->type = DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION;
		$invoice->status = ($event->data->object->status  == 'active') ? DataType::$INVOICE_PAID : DataType::$INVOICE_PENDING;		
		$invoice->effective_date = ($event->data->object->status == 'trialing') ? date("Y-m-d",$event->data->object->trial_end ) : 'NOW';
		$invoice->save();
		applog("invoice {$invoice->id} created with effective date {$invoice->effective_date}");
		
		// creating payment record
		$payment = new Payment();
		$payment->invoice_id = $invoice->id;
		$payment->status = ($event->data->object->status  == 'active') ? DataType::$PAYMENT_SUCCESS : DataType::$PAYMENT_PENDING;
		$payment->success = ($event->data->object->status  == 'active') ? 1 : 0;
		$payment->amount = ($event->data->object->plan->amount/100);
		//$payment->charge_id = $intent->charges->data[0]->id;
		$payment->save();
		applog("Payment record {$payment->id} created for invoice {$invoice->id}");
		
		// send notification to customer
		$com = new ComEngine();

		$desc = $invoice->getDesciption();
		$contact = $invoice->getContact();

		$subject = $desc['subject'];

		$line1 = $desc['desc'];
		$line2 = "Your card has been charged {$desc['amount']}.";

		$com->paymentEmail ($contact, $subject, $line1, $line2 );
		applog("Confirmation email sent to {$contact['email']}");
		
		$message = $desc['desc'];

		$com->paymentSMS ($contact, $message );
		applog("Confirmation SMS sent to {$contact['phone']}");
		
	}
	
	
	// update current subscription status ends
}
elseif($event->type == 'customer.subscription.deleted' && $event->data->object->status == 'canceled'){
	// immiditely delete the subscription
	//"status": "canceled"
	// immiditely delete the subscription ends
	applog("Subscription {$event->data->object->id} delete request received");
	
	$invoice = App::getInvoice($event->data->object->latest_invoice);
	
	if(!empty($invoice)){
		
		$subscription = $invoice->getSubscription();
		$subscription->delete();
		applog("Subscription {$subscription->id} deleted");
		
	}
	
	// delete current invoice
	$invoice->delete();
	applog("Invoice {$invoice->id} deleted");
	
	
}
// invoice.payment_succeeded
//invoice.payment_succeeded

elseif($event->type == 'invoice.payment_succeeded'){
	// activate subscription
	applog("Invoice payment success event received for {$event->data->object->id}");
	
	$invoice = App::getInvoice($event->data->object->id);

	if(empty($invoice)){
		// likely not happned as invoice created when creating subscription
		applog("Invoice not found, creating subscription, invoice and payment record");
		
		if(!empty($event->data->object->subscription)){
			// invoice is for a subscription
			applog("Invoice is for a subscription {$event->data->object->subscription} payment");
			$catsub = App::getCategorySubscription($event->data->object->subscription);
			
			if(empty($catsub)){
				// category must be added from the user end
				applog("Category subscrption {$event->data->object->subscription} not found, abort ");
				exit();			
			}
			
			$package = App::getPackage($event->data->object->lines->data[0]->plan->id);
		
			if(empty($package)){
				applog("Given price ID {$event->data->object->lines->data[0]->plan->id} does not exists, abort");
				exit();
			}

			$category = $catsub->getCategory();
			$profile = $category->getProfile();

			// create subscription record
			$subscription = new Subscription();
			$subscription->category_id = $category->id;
			$subscription->package_id = $package->id;
			$subscription->sub_date = 'NOW';
			//$subscription->begin_date = date('Y-m-d', $event->data->object->current_period_start);
			//$subscription->exp_date = date('Y-m-d', $event->data->object->current_period_end);
			//$subscription->active = ($event->data->object->status  == 'active') ? 1 : 0;
			$subscription->jobs = $package->jobs;
			$subscription->manage = $package->manage;
			$subscription->catsub_id = $catsub->id;
			$subscription->save();
			applog("Subscripton id {$subscription->id} created (inactive - begin/exp dates unknown)");

			// create invoice
			$invoice = new Invoice();
			$invoice->customer_id = $profile->customer_id;
			$invoice->profile_id = $profile->id;
			$invoice->subscription_id = $subscription->id;
			$invoice->currency = 'USD';
			$invoice->amount = $event->data->object->total/100;
			$invoice->stripe_id = $event->data->object->id;
			$invoice->type = DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION;
			$invoice->status = DataType::$INVOICE_PAID;		
			$invoice->effective_date = 'NOW';
			$invoice->save();
			applog("invoice {$invoice->id} created with effective date {$invoice->effective_date}");

			// creating payment record
			$payment = new Payment();
			$payment->invoice_id = $invoice->id;
			$payment->status =  DataType::$PAYMENT_SUCCESS;
			$payment->intent_id = $event->data->object->payment_intent;
			$payment->charge_id = $event->data->object->charge;
			$payment->success = 1;
			$payment->amount = $invoice->amount;
			//$payment->charge_id = $intent->charges->data[0]->id;
			$payment->save();
			applog("Payment record {$payment->id} created for invoice {$invoice->id}");
			
		}
		else{
			// invoice is for one time payment
		}
		
	}
	else{
		// invoice found
		applog("Invoice {$invoice->id} found. activating ");
		
		// newly activated
		$subscription = $invoice->getSubscription();
		if(!empty($subscription)){
			$subscription->active = 1;
			$subscription->save();
			applog("Subscription {$subscription->id} activated");
		}		

		// update invoice status
		if($invoice->status != DataType::$INVOICE_PAID) {
			$invoice->status = DataType::$INVOICE_PAID;
			$invoice->amount = $event->data->object->total/100;
			$invoice->save();
			applog("Invoice {$invoice->id} marked as paid");

			// check payment status
			$payment = $invoice->getPayment();
			if(empty($payment)){
				$payment = new Payment();
				$payment->invoice_id = $invoice->id;
			}

			$payment->status =DataType::$PAYMENT_SUCCESS;
			$payment->success = 1;
			$payment->amount = $invoice->amount;
			$payment->intent_id = $event->data->object->payment_intent;
			$payment->charge_id = $event->data->object->charge;
			//$payment->charge_id = $intent->charges->data[0]->id;
			$payment->save();
			applog("Payment record {$payment->id} marked as paid");
		}

		
		
		
	}
	
	// subscription activating ends
}
elseif($event->type == 'invoice.payment_failed'){
	
	if(!empty($event->data->object->subscription)){
		//subscriptoin invoice.payment_failed
		applog("Invoice payment failed event received for {$event->data->object->id}");

		$invoice = App::getInvoice($event->data->object->id);

		if(empty($invoice)){
			applog("Invoice not found, creating new invoice and subscription record");
			
			applog("Invoice is for a subscription {$event->data->object->subscription} payment");
			
			$catsub = App::getCategorySubscription($event->data->object->subscription);
			
			if(empty($catsub)){
				// category must be added from the user end
				applog("Category subscrption {$event->data->object->subscription} not found, abort ");
				exit();			
			}
			
			$package = App::getPackage($event->data->object->lines->data[0]->plan->id);
		
			if(empty($package)){
				applog("Given price ID {$event->data->object->lines->data[0]->plan->id} does not exists, abort");
				exit();
			}

			$category = $catsub->getCategory();
			$profile = $category->getProfile();

			// create subscription record
			$subscription = new Subscription();
			$subscription->category_id = $category->id;
			$subscription->package_id = $package->id;
			$subscription->sub_date = 'NOW';
			//$subscription->begin_date = date('Y-m-d', $event->data->object->current_period_start);
			//$subscription->exp_date = date('Y-m-d', $event->data->object->current_period_end);
			//$subscription->active = ($event->data->object->status  == 'active') ? 1 : 0;
			$subscription->jobs = $package->jobs;
			$subscription->manage = $package->manage;
			$subscription->catsub_id = $catsub->id;
			$subscription->active = 0;
			$subscription->save();
			applog("Subscripton id {$subscription->id} created (inactive - begin/exp dates unknown)");

			// create invoice
			$invoice = new Invoice();
			$invoice->customer_id = $profile->customer_id;
			$invoice->profile_id = $profile->id;
			$invoice->subscription_id = $subscription->id;
			$invoice->currency = 'USD';
			$invoice->amount = $event->data->object->total/100;
			$invoice->stripe_id = $event->data->object->id;
			$invoice->type = DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION;
			$invoice->status = DataType::$INVOICE_PENDING;		
			$invoice->effective_date = 'NOW';
			$invoice->save();
			applog("pening invoice {$invoice->id} created with effective date {$invoice->effective_date}");

			// creating payment record
			$payment = new Payment();
			$payment->invoice_id = $invoice->id;
			$payment->status =  DataType::$PAYMENT_FAILED;
			$payment->intent_id = $event->data->object->payment_intent;
			$payment->charge_id = $event->data->object->charge;
			$payment->success = 0;
			$payment->amount = $invoice->amount;
			//$payment->charge_id = $intent->charges->data[0]->id;
			$payment->save();
			applog("Failed Payment record {$payment->id} created for invoice {$invoice->id}");
			
			// notify customer
			$com = new ComEngine();
			
			$desc = $invoice->getDesciption();
			$contact = $invoice->getContact();
			
			$subject = "Your payment for Curatalent was declined";
			$line1 = "We could not process the payment for your {$desc['skill']} - {$desc['package']} subscription.";
			$line2 = "Please log into your account and settle the invoice to re-activate.";
			
			$com->paymentEmail ($contact, $subject, $line1, $line2 );
			applog("Reminder email sent to {$contact['email']}");
			
			$message = "Payment for {$desc['skill']} - {$desc['package']} subscription was declined. You may not receive bookings.";
			
			$com->paymentSMS ($contact, $message );			
			applog("Reminder SMS sent to {$contact['phone']}");
			
		}
		else{
			applog("Invoice {$invoice->id} found, no further actions");
		}
		
	}
	else{
		// single credit payment failed
	}
	
}

applog("IPN event {$event->id} processed");


// creating log entries
ob_start();

//echo $signature."\n";
print_r($payload);

$txt = ob_get_clean();
$txt .= "\n------------------------------------------------------\n";

$file = Config::$log_path.'/ipnlog.txt';
file_put_contents($file, $txt, FILE_APPEND);


?>