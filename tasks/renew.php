<?php
/*************************************************
* This script renew subscriptions
* adding free subscriptions for those 
* have expired or expiring within next 24 hours
*************************************************/

include('../lib/autoload.php');

$query = "select * from expired_subscriptions";

$buffer = new Buffer();
$buffer->_datasql = $query;
$buffer->_countsql = "SELECT count(*) as total from expired_subscriptions";

$buffer->_pagesize = 500;
$total =  $buffer->getTotal();
$pages = ceil($total/500);

$error = NULL;


for ($p = 1; $p <= $pages; $p++ ){
	echo "<h1>{$p}</h1>";
	$data = $buffer->getData($p);
	
	$data = Util::groupMultiArray($data, 'customer_id');

	foreach  ($data as $customer_id => $customer_data) {
		
		$contact = App::getCustomerContact($customer_id);
		$log = array();

		foreach ($customer_data as $item) {

			//Util::debug($item);
			
			$ending = strtotime($item['expired']);
			$begin = strtotime("+1 day", $ending);
			$ends = ($item['validity'] == 30) ? strtotime("+1 month ", $ending) : strtotime("+1 year ", $ending);


			if(empty($item['active']) || !empty($item['deleted'])){
				// package is no longer available, notify customer and transfer to free package

				$error = 'package';

			}
			elseif($item['price'] == 0) {
				
				// renew free subscription
				$subscription = new Subscription();
				$subscription->category_id = $item['category_id'];
				$subscription->package_id = $item['package_id'];
				$subscription->sub_date = 'NOW';
				$subscription->begin_date = date("Y-m-d", $begin);
				$subscription->exp_date = date("Y-m-d", $ends);
				$subscription->active = 1;
				$subscription->jobs = $item['jobs'];
				$subscription->manage = $item['manage'];
				$subscription->save();
				
				// create and settle invoice
				$invoice = new Invoice();
				$invoice->type = DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION;
				$invoice->customer_id = $item['customer_id'];
				$invoice->profile_id = $item['profile_id'];
				$invoice->currency = 'USD';				
				$invoice->status = DataType::$INVOICE_PAID;
				$invoice->due_date = date("Y-m-d H:i:s");
				$invoice->effective_date = "NOW";				
				$invoice->save();
				
							

			}

/*
			if(!empty($error)) {

				$newpackage = App::getFreePackage($item['skill_id']);

				if(!empty($newpackage)){

					$subscription = new Subscription();

					if(!empty($item['agency_id'])){
						$subscription->agency_id = $item['agency_id'];			
					}
					else{
						$subscription->talent_id = $item['talent_id'];			
					}

					$subscription->skill_id = $item['skill_id'];
					$subscription->package_id = $newpackage->id;
					$subscription->term = $item['term'];
					$subscription->fees = ($item['term'] == 'm') ? $newpackage->price_monthly : $newpackage->price_annualy;
					$subscription->jobs = $newpackage->jobs;
					$subscription->manage = $newpackage->manage;	
					$subscription->active = 1;
					$subscription->sub_date = "NOW";
					$subscription->begin_date = date("Y-m-d", $begin);
					$subscription->exp_date = date("Y-m-d", $ends);
					$subscription->save();				
					
				}
				else {

					// Tell the client there is no free packages for this category
					
					
					continue;

				}
				

			} // on error
			*/
			if(!empty($subscription)){
				
				// notify  customer
				$log[] = array(
					'profile' => $item['profile_id'],
					'customer' => $item['customer_id'],
					'category' => $item['category_id'],
					'package' => $item['package_id'],
					'status' => 'Success'
				);
				
			}
			else{
				$log[] = array(
					'profile' => $item['profile_id'],
					'customer' => $item['customer_id'],
					'category' => $item['category_id'],
					'package' => $item['package_id'],
					'status' => 'Error, Package Inactive'
				);
			}
			

		}
		// each subscription
		
		// notify customer
		echo json_encode($log)."\n";
		//Util::debug($log);
	
	//break;

	}
	// each customer
	
	
}// each page


 

?>