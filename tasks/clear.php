<?php
/***
* Clean up all uploads and system caches
*/

include('../lib/autoload.php');

// cleanup system files

if(file_exists(Config::$cache_path.'/system/skills.ccf')){
	unlink(Config::$cache_path.'/system/skills.ccf');
}

if(file_exists(Config::$cache_path.'/system/tags.ccf')){
	unlink(Config::$cache_path.'/system/tags.ccf');
}

// cleanup image cache

$folder = Config::$base_path.'/contents';
if ($dir = opendir($folder)){
	
	 while (($file = readdir($dir)) !== false) {
		 
		 if($file == '.' || $file == '..'){
			 continue;			 
		 }
		 
		 $path = Config::$base_path.'/contents/'.$file;
		 
		 if(is_dir($path)){
			 
			 if ($sub = opendir($path)){
				 				 
				 while (($content = readdir($sub)) !== false) {
					 
					 if($content == '.' || $content == '..'){
						 continue;			 
					 }
					 				 
					 echo $path.'/'.$content."<br>";
					// unlink($path.'/'.$content);
						 
				 }
				 
				 closedir($sub);
				 
			 } 
			 
		 }
		
	 }
	
 	closedir($dir);	
}


// cleanup temp folder
$folder = Config::$base_path.'/tmp';
if ($dir = opendir($folder)){
	
	 while (($file = readdir($dir)) !== false) {
		 
		 if($file == '.' || $file == '..'){
			 continue;			 
		 }
		 
		 echo $folder.'/'.$file."<br>";
		// unlink($folder.'/'.$file);
		 
	 }
	
	closedir($dir);	
}



echo 'ok';
?>