<pre>
<?php
include('../lib/autoload.php');

echo "Creating sitemap file\n";

$sitemap_file = Config::$base_path."/sitemap.txt";
$lines = 1;
		
$file = fopen($sitemap_file, "w"); 
				
// home page
fwrite($file, Config::$base_url."\n");
		
// support documents
$handle = opendir(Config::$base_path.'/templates/support');
while (false !== ($entry = readdir($handle))) {

	if($entry == '.' || $entry == '..'){
		continue;
	}

	if($entry == 'home.php'){
		fwrite($file, Config::$base_url.'/support'."\n");
	}
	else{
		fwrite($file, Config::$base_url.'/support/'.str_replace('.php','',$entry)."\n");
	}
	
	$lines++;
}
closedir($handle);

// search pages
$skills = App::getSkills();
foreach ($skills as $item){
	
	$name  = strtolower($item['name']);

	// find category name
	if(strpos($name,'(') !== false) {
		
		$part1 = explode('(', $name);
		$part2 = explode(')', $part1[1]);
		$name = $part1[0].'|'.str_replace("/", "|",$part2[0]);
	}
	
	
	if(strpos($name,'|') !== false) {

		$np = explode('|', $name);
		foreach ($np as $p){
			fwrite($file, Config::$base_url.'/search/'.urlencode(strtolower($p))."\n");
			$lines++;
		}

	}
	elseif(strpos($name,'/') !== false) {

		$np = explode('/', $name);
		foreach ($np as $p){
			fwrite($file, Config::$base_url.'/search/'.urlencode(strtolower($p))."\n");
			$lines++;
		}

	}
	else{
		fwrite($file, Config::$base_url.'/search/'.urlencode(strtolower($item['name']))."\n");
		$lines++;
	}	
}


		
// index pages
$buffer = App::getPublishedPages();
$buffer->_pagesize = 100;
$total = $buffer->getTotal();
$pages = ceil($total/100);

for($p = 1; $p <= $pages; $p++ ){

	$data = $buffer->getData($p);

	foreach ($data as $item){

		fwrite($file, Config::$base_url. '/'.str_replace(' ', '+', $item['handle'])."\n");
		$lines++;
	}

}



fclose($file);

echo "Number of lines : {$lines}\nFile size: ";

$size = filesize($sitemap_file);
echo number_format($size/(1024*1024), 3)."MB";
echo "\nFile completed";
?>
</pre>