<?php


header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

include_once('../lib/datatype.php');
include_once('../lib/database.php');
include_once('../lib/session.php');
include_once('../lib/util.php');

$db = new Database();
$customer = Session::getCustomer();

if(empty($customer)){
	$data = array('terminate' => 1, 'nid' => 0);
	echo "event: message\n";
	echo "data: " . json_encode($data) . "\n\n";
	echo str_pad('', 4096) . "\n";			
	ob_flush();
	flush();
	$db->close();
	exit();	
}

$last = 0; 

if(!empty($_GET['path']) && is_numeric($_GET['path']) && strlen(trim($_GET['path'])) == 10){
	$last = (int)$_GET['path']; 
}

function check_abort(){
  /*if (connection_aborted()) {
  
  // close message queue
  }*/
}

/**************************************************************/



if($last == 0) {
	// initial load

	// get initial data
	$data = $db->query("SELECT n.id AS nid, CONCAT(n.type,n.ref) AS ref, n.message AS message, n.type, n.ref AS id,  i.thumb AS image, DATE_FORMAT(n.created, '%b %e, %h:%i')  AS `date`, 
n.seen, UNIX_TIMESTAMP() AS `now`
FROM notifications n 
LEFT JOIN notifications n2  ON (n.type = n2.type AND n.ref = n2.ref AND n.created < n2.created)
LEFT JOIN images i ON (i.id = n.image_id)
WHERE n2.created IS NULL AND n.customer_id = ".$customer['id']." ORDER BY n.created desc  limit 25");
	
	$time = $db->query("SELECT UNIX_TIMESTAMP() AS `time`", NULL, 1);
	
	if(!empty($data)) {
		
		$data = array_reverse($data);
		
		foreach ($data as $item){
					
			echo "event: message\n";
			echo "data: " . json_encode($item) . "\n\n";
			echo str_pad('', 4096) . "\n";
			
			if($item['nid'] > $maxid){
				$maxid = $item['nid'];	
			}
		}
	}

		
	echo "event: message\n";
	echo "data: " . json_encode($time) . "\n\n";
	echo str_pad('', 4096) . "\n";

	
	
	// wait 2 seconds
	ob_flush();
	flush();
	
	$db->close();
	sleep(2);
	$db->connect();
	
}




while (true) {
	
	$chats = false;
	
	
	$data = $db->query("SELECT n.id as nid, CONCAT(n.type,n.ref) AS ref, n.message AS message, n.type, n.ref as id,  i.thumb AS image, DATE_FORMAT(n.modified, '%b %e, %h:%i')  as date, n.seen, UNIX_TIMESTAMP() AS `time`
FROM notifications n
LEFT JOIN images i ON (i.id = n.image_id)
WHERE n.customer_id = ".$customer['id']." AND  n.seen = 0 AND n.created >= FROM_UNIXTIME(".$last.")");
	
	if(!empty($data)) {
		
		foreach ($data as $item){
			
			if($item['type'] == 'c'){
				$chats = true;	
			}
		
			echo "event: message\n";
			echo "data: " . json_encode($item) . "\n\n";
			echo str_pad('', 4096) . "\n";
			
			if($item['nid'] > $maxid){
				$maxid = $item['nid'];	
			}
		}
		
		
	}
	
	if($chats){
		
		// check chat rooms
	
		$cond = array();
		$cond[] = "r.customer_id = '".$customer['id']."'";
		$cond[] = "r.created >= FROM_UNIXTIME(".$last.")";
		$cond[] = "isnull(r.deleted) ";
			
		$chats = $db->query("SELECT m.id, r.id as r, m.board_id as b, m.customer_id AS s, IF(isnull(m.deleted),m.message,'') AS m, m.edited AS e, DATE_FORMAT(m.created,'%b %e') AS d, TIME_FORMAT(m.created,'%h:%i%p') AS t , m.quote_id AS qid, q.customer_id AS qs, 
	SUBSTR(q.message, 0,20) AS qm, IF(isnull(m.deleted),0,1) as del
	FROM messages m
	JOIN message_recipients r ON (r.message_id = m.id)
	LEFT JOIN messages q ON (q.id = m.quote_id AND ISNULL(q.deleted))
	WHERE ".implode(" AND ", $cond )." 
	ORDER BY id desc", NULL, 10);
			
		if(!empty($chats)) {
			$chats = array_reverse($chats);
		}
		
		foreach ($chats as $line){
			
			$line['type'] = 'cd';
			$line['self'] = $customer['id'];
			
			echo "event: message\n";
			echo "data: " . json_encode($line) . "\n\n";
			echo str_pad('', 4096) . "\n";
			
			if($max_chat < $line['id']){
				$max_chat = $line['id'];
			}			
		}		
	}
	
	
      
	ob_flush();
  	flush();
  	$db->close();
	sleep(2);
	$db->connect();

  	if (connection_aborted()) {
    	break;
  	}
}


ob_end_flush();

$db->close();


register_shutdown_function("check_abort");

?>