var mapService;
var autocomplete; 
var month_abbr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

$(document).ready(function(e) {
    
addPageListeners();
	
});

function addPageListeners () {
	bindFormFields();	
	initWidgets();
	initContextMenu();
	initPlaceSearch();
}

function bindFormFields () {
	console.log('boo');
	$('.field-wrapper input, .field-wrapper select, .field-wrapper textarea').off('focus',moveLabelAway).on('focus',moveLabelAway)
	
		
		$.each($('.dynamic-label'), function(){
			if($('#'+$(this).attr('for')).val()){
				$(this).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
			}		
		});
	
	$('.custom-option input').off('change', showCustomOptions).on('change', showCustomOptions);	
	$.each($('.custom-option input'), showCustomOptions);
	
	
	
	$('form').each(function(index, element) {
        
		if($(this).data('submit')){
			$(this).off('submit').on('submit', genFormHandle);
		}
		
		if($(this).data('init')){
			var cf = $(this).data('init');			
			try{
				
				if(window[fc]){
					window[cf](this);	
				}
				else{
					console.log(cf+ ' is not a valid function');
				}
			}
			catch(ex){
				console.log(cf+ ' is not a valid function');	
			}
		}
		
		if($(this).data('validate')){
			
			vld = $(this).data('validate');
			if(window[vld]){
				vo = $(this).validate(window[vld]);	
				$(this).data('validator', vo);
				
				// add on blur listners
				$(this).find('input,select,textarea').blur(function(){
					
					if(this.id){
						var vld = $(this).parents('form:eq(0)').data('validator');
					
						vld.element('#'+this.id);
					
					}
					
				});
			}						
		}
		
    });
}

function genFormHandle (){

	var cf = $(this).data('submit');
	try{
				
		if(window[cf]){
			return window[cf](this);	
		}
		else{
			console.log(cf+ ' is not a valid function');
			notify("Application error!", false);
		}
	}
	catch(ex){
		console.log(ex);
		notify("Application error!", false);	
	}
	
	return false;
}

function submitForm (form_id, block){

	is_valid = true;
	
	form = $('#'+form_id);
	var vld = form.data('validator');
	
	if(!is_form_valid(form_id)){
		return;	
	}
	
	action = form.attr('action');
	data = form.serialize();
	
	cl = new call(action, data, null, true);
	cl.post();
	
	if(block){
		block();
	}	
	
}


function is_form_valid (form_id) {
	
	is_valid = true;
	
	form = $('#'+form_id);
	var vld = form.data('validator');
	
	if(vld){
		
		var fields = form.find('input,select,textarea');
		
		for(var f = 0; f < fields.length; f++){
			
			
			try {
				if(fields[f].id) {
					if(!vld.element('#'+fields[f].id)){
						is_valid = false;	
					}
				}
			}
			catch(ex){
				console.log(fields[f]);
				console.log("Error found");
			}					
		}		
		
		
		if(!is_valid){
			notify('Please fix errors');	
			return false;
		}
		
		
	}
	
	return true;
	
}

function moveLabelAway () {
	
	$(this).off('focus', moveLabelAway);
	
	$(this).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
	
}

function showCustomOptions () {
	
	if($(this)[0].checked){
		$(this).parents('.custom-option:eq(0)').addClass('custom-option-selected');
	}
	else{
		$(this).parents('.custom-option:eq(0)').removeClass('custom-option-selected');
	}
}


/** Ajax request wrapper   **/

var Tcall = function (url, data, element, json, msg){
	
	// call status, -1 : failed,  0: not sent, 1: in progress, 2:completed
	this.status = 0;
	// url to call
	this.url = base+url;
	// request method
	this.method = 'GET';
	// data to be sent, if POST, post data else, URL parameters
	this.data = data;
	// is this call return json response
	this.json = json ? true : false;
	
	// success callback function
	this.success = null;
	// failur callback function
	this.fail = null;
	
	// DOM element to add data
	this.element = element ? element : '#pagelet';
	// whether add data or append 
	this.append = false;

	// message to show while loading
	this.message = msg;	
	// notification id
	this.msgid = null;
	
	// what to retry sending request
	this.retry = false;
	// number of attempts made to resend request
	this.attempts = 0;	
	
	 
}


Tcall.prototype.post = function () {
	
	this.method = 'POST';
	this.send();
}
Tcall.prototype.get = function () {
	this.method = 'GET';
	this.send();
}

Tcall.prototype.load = function () {
	this.method = this.data ? 'POST' : 'GET';
	this.send();
}

Tcall.prototype.send = function () {
	
	this.attempts++;
	
	if(this.success){
		callback = wrap(this, this.success);
	}
	else {
		callback = wrap(this, callComplete);	
	}
	
	if(this.fail){
		faliur = wrap2(this, this.fail);	
	}
	else{
		failur = wrap2(this, callError);	
	}
	
	o = {
		method: this.method,
		url: this.url,
		error: failur,
		success: callback	
	}
	
	if(this.json){
		o.dataType = 'json';
	}
	
	if(this.data){
		
		if(typeof this.data == 'object'){
			qs = [];
			for(k in this.data){
				qs.push(encodeURIComponent(k)+'='+encodeURIComponent(this.data[k]));	
			}
			o.data = qs.join('&');	
		}
		else{
			o.data = this.data;	
		}
	}
	
	
	if(this.message){
		this.msgid = notify(this.message);
	}
	
	$.ajax(o);

}


function call (url, data, element, json, msg) {
	return 	new Tcall(url, data, element, json, msg)
}

function callComplete (res, obj) {
	
	unblock();	
	
	if(obj.json){
		
		if(res.message){
			obj.msgid = notify(res.message, res.status);
		}
		else{
			clearNotify();	
		}
		
		if(res.trigger){
			
			if(res.trigger == 'alert') {
				new Dialog( {
					title: 'Alert',
					message : res.message,
					mask : true,
					buttons: {
							ok : {label : 'Ok'}
						}
				});	
			}
			else if(res.trigger == 'redirect') {
				window.location.href = base+res.data;
			}
			else {
				if(window[res.trigger]) {
					window[res.trigger](res, obj);
					//return;
				}
				else{
					console.log(res.trigger+' trigger undefined');	
				}
			}
		}
		
		if(res.html && obj.element){
							
			if(obj.append){
				$(obj.element).append(res.html);
			}
			else{
				$(obj.element).html(res.html);
			}
			
			addPageListeners();			
		}
		
		if(res.nodes){
			
			for(p = 0; p < res.nodes.length; p++){
				
				if(	res.nodes[p].id || res.nodes[p].class ){
					
					tele = res.nodes[p].class ? '.'+res.nodes[p].class : '#'+res.nodes[p].id;
					
					if(res.nodes[p].hasOwnProperty('append')){
						$(tele).append(res.nodes[p].append);	
					}
					
					if(res.nodes[p].hasOwnProperty('html')){
						$(tele).html(res.nodes[p].html);	
					}
					
					if(res.nodes[p].hasOwnProperty('value')){
						$(tele).val(res.nodes[p].value);	
					}
					
					if(res.nodes[p].options){
						ov = '';
						for(k in res.nodes[p].options){
							ov += '<option value="'+k+'">'+res.nodes[p].options[k]+'</option>';						}
						$(tele).html(ov);	
					}
					
					if(res.nodes[p].trigger && window[res.nodes[p].trigger]){
						window[res.nodes[p].trigger](res, obj);
					}
				}
				else{
					console.log("Node id undefined");	
				}
			}			
			
			addPageListeners();
		}
		
		if(res.error){
			
			if(document.getElementById('_server_error')){
				$('#_server_error').html('<div class="_server_error">'+res.error+'</div>');
			}
			else{
				new Dialog( {
					title: 'Something went wrong!',
					message : res.error,
					mask : true,
					width: 300,
					data:5,
					buttons: {
							cancel : {label : 'Close', 'class' : 'button-alt'}
						}
				});	
			}
		}
								
	}
	else{
		
		if(obj.element) {
			
			if(obj.append){
				$(obj.element).append(res);
			}
			else{
				$(obj.element).html(res);
			}
		}
		
	}
	
	
}

function callError (e, obj) {
	
	if(obj.msgid){
		notificationManager.remove(obj.msgid, true);
	}
	
	if(e == 'error'){
		obj.msgid = notify("Something went wrong. Please try again.", false);
	}
	else if(e == 'timeout'){
		obj.msgid = notify("Server not reachable. Check your internet connection.", false);
	}
	else if(e == "parsererror"){
		obj.msgid = notify("Oops! Something went wrong. Please try again.", false);	
	}
		
	if(obj.retry && obj.attempt < 3){
		obj.send();	
	}
	
	unblock();
	
}




function wrap (obj, callback){
	return function (res) {		
		var data = obj;		
		callback(res, data);			
	}	
}

function wrap2 (obj, callback){
	return function (a, b, c) {		
		var data = obj;		
		callback(b, data);			
	}	
}


/**************** Notification manager ********************/

var tom = null;
function notify (msg) {
	
	$('#notifications').html('<a class="notification">'+msg+'</a>');
	if(tom){
		clearTimeout(tom);
	}	
	tom = setTimeout('clearNotify()', 5000);
}

function clearNotify (){
	$('.notification').fadeOut('fast', function(){ $(this).remove()});
	if(tom){
		clearTimeout(tom);
	}	
}

function block () {
	$('#blocker').fadeIn('fast');
}

function unblock () {
	$('#blocker').fadeOut('slow');
}

/*
function rp () {

	c = Math.floor(Math.random() * 100);
	
	if(c%3 == 0){
		a = null;	
	}
	else{
		a = c%2 == 0 ? true : false;	
	}
	
	notify("my "+c +" message", a);
	
}
*/

/************************* Time picker *****************************/

var TimePicker = function (jele) {
	
	this.active_class = 'tpa';
	
	uid = jele.id+'_time';
	
	$(jele).after ('<div class="timepicker" id="'+uid+'" data-id="'+jele.id+'" style="display:none"> <div class="tp-outer"> <div class="tp-left"><div class="tp-header">hours</div><div class="tp-wrapper tp-hour"><div class="tp-row"> <a data-val="1">1</a> <a data-val="2">2</a> <a data-val="3">3</a> <a data-val="4">4</a> </div><div class="tp-row"> <a data-val="5">5</a> <a data-val="6">6</a> <a data-val="7">7</a> <a data-val="8">8</a> </div><div class="tp-row"> <a data-val="9">9</a> <a data-val="10">10</a> <a data-val="11">11</a> <a data-val="12">12</a> </div></div><div class="tp-header">AM/PM</div><div class="tp-wrapper"><div class="tp-row tp-ampm"> <a data-val="AM">AM</a> <a data-val="PM" >PM</a> </div></div> </div> <div class="tp-right"><div class="tp-header">Minutes</div><div class="tp-row tp-minutes tp-wrapper"> <a data-val="00">:00</a> <a  data-val="15">:15</a> <a data-val="30">:30</a> <a data-val="45">:45</a> </div> </div> </div> </div>');
	
	$('.timepicker a').unbind().click(function (){
		pe = $(this).parents('div.timepicker:eq(0)');
		be = pe.data('id');
		
		$(this).parents('.tp-wrapper:eq(0)').find('a.selected').removeClass('selected');
		$(this).addClass('selected');
		
		sh = pe.find('.tp-hour a.selected');
		sm = pe.find('.tp-minutes a.selected');
		sa = pe.find('.tp-ampm a.selected');
		
		if(!pe.hasClass('focused')){

			pe.addClass('focused');
		}
		
		if(sh.length > 0 && sm.length > 0 && sa.length > 0){
			tpv = sh.data('val')+':'+sm.data('val')+' '+sa.data('val');	
			$('#'+be).val(tpv)[0].focus();
			cb = $('#'+be).data('changed');	
			if(cb){
				window[cb](tpv);
			}
			
			setTimeout("pe.removeClass('focused'); ",250);
			
		}
		
	});
	
	tv = $(jele).val();
	
	if(tv){
		// populate time
		tv = tv.split(/[: ]/);
		$('.tp-hour a').each(function(){
			if($(this).data('val') == tv[0]){
				$(this).addClass('selected');	
			}
		});	
		$('.tp-minutes a').each(function(){
			if($(this).data('val') == tv[1]){
				$(this).addClass('selected');	
			}
		});
		$('.tp-ampm a').each(function(){
			if($(this).data('val') == tv[2]){
				$(this).addClass('selected');	
			}
		});
	}
	
	$(jele).focus(function(){
		$('#'+this.id+'_time').css({display:'block'});	
	});
	
	$(jele).blur(function(){
		
		setTimeout("hideTimePicker('"+this.id+"')", 200)
	});
}



function initWidgets () {
	
	initCalendar ();
	initInlineEdit();
	
	$.each($('.timepick'), function (){
		new TimePicker(this);
	});
	
	// linked table rows
	$('table.linked tr').each(function (){
		
		if($(this).hasClass('-linked-row')){
			return;	
		}
		
		$(this).addClass('-linked-row');
		
		$(this).click(function(){
			cbp = $(this).parents('table:eq(0)');
			if(!$(this).data('id')){
				return;	
			}
			
			cb = cbp.data('callback');
			if(window[cb]){
				window[cb](this);
				
				if(cbp.parents('div.pagelet-body').length > 0){
					cbp.parents('div.pagelet-body:eq(0)').find('tr.linked-row-selected').removeClass('linked-row-selected');	
				}
				else {				
					cbp.find('tr.linked-row-selected').removeClass('linked-row-selected');					
				}
				$(this).addClass('linked-row-selected');
			}
			else{
				console.log(cb+': callback undefined ');	
			}
				
		});
			
	});
	
	/**
	* Auto load 
	*/
	$('.autoload').each(function(index, element) {
        
		var dt = $(this).data;
		
		refreshObject(dt);
		
    });
	
	
	// panel switches
	$('input.switcher').each(function(){
		
		if($(this).hasClass('-switch-added')){
			return;	
		}
		
		$(this).addClass('-switch-added');
		
		$(this).change(function(){

			if(this.checked){	
				idata = $(this).data();
				frm = $(this).parents('form:eq(0)');
				frm.find(idata.off).css({display:'none'});
				frm.find(idata.on).css({display:'block'});
			}
				
		});
		
		
		if(this.checked){				
			idata = $(this).data();
			frm = $(this).parents('form:eq(0)');
			frm.find(idata.off).css({display:'none'});
			frm.find(idata.on).css({display:'block'});
		}
			
	});
	
	
	// number formatters
	$('input.number').each(function(index, element) {
		
		if($(this).hasClass('-number')){
			return;	
		}
		
		$(this).addClass('-number');
		
		$(this).keyup(formatNumber).keyup();
        
    });
	
	// galery
	$('.gal').each(function(index, element) {
        
		if($(this).hasClass('_gal')){
			return;	
		}
		
		$(this).addClass('_gal').click(showGallery);
		
		
    });
	
	// selectable box
	$('.selectable-box').each(function(index, element) {
        
		if($(this).hasClass('_selectable')){
			return;
		}
		
		$(this).addClass('_selectable');
		
		$(this).click(function(){
			
			$(this).parents('div:eq(0)').find('.selectable-box').removeClass('selected-box');
			$(this).addClass('selected-box');
				
		});
		
		if($(this).find('input')[0].checked){
			$(this).addClass('selected-box');
		}
		
    });
	
	// soart switch
	$('.sort-switch').each( function () {
		
		if($(this).hasClass('_sorting')){
			return;	
		}
		
		$(this).addClass('_sorting');
		$(this).click(function(){
			
			eid = '#'+$(this).data('id');
			if($(eid).val() == 'asc'){
				$(eid).val('desc');	
				$(this).removeClass('asc').addClass('desc');
			}
			else{
				$(eid).val('asc');
				$(this).removeClass('desc').addClass('asc');		
			}
			
			if($(this).data('callback')){
				cb = $(this).data('callback');
				
				if(window[cb]){
					window[cb](this);	
				}
			}
				
		});
			
	});
	
}

function initCalendar () {

	// date picker
	$.each($('.datepick'), function (){
		
		if($(this).hasClass('-datepicker')){
			return;	
		}
		
		$(this).addClass('-datepicker')
		
		var eid = this.id;
		var fv = $(this).val();
		$(this).after('<div class="vanilla-calendar" id="'+eid+'_cal" data-id="'+eid+'"></div>');
						
		var mcl =  new VanillaCalendar({
		selector: '#'+eid+"_cal",
		//pastDates: false,
		onSelect: function (dt, ele){
			
				dt = new Date(dt.date);

				var tid = $(ele).parents('div.vanilla-calendar:eq(0)').data('id');
				var dtxt =  ToDate(dt);
				$('#'+tid).val(dtxt);
				
				cb = $('#'+tid).data('changed');	
				if(cb){
					window[cb](dtxt);
				}
				$('#'+tid+'_cal').css({display:'none'});	
			}
		});
		
		
		
		$(this).focus(function () {
			$('#'+eid+"_cal").css({display:'block'}).removeClass('focused');	
		});
		$(this).blur(function () {
			
			//if(this.value || this.value != ''){
				setTimeout("hideCalendar('"+this.id+"')", 200);	
			//}
		});
		
		$('#'+eid+"_cal").click(function () {
			if(!$(this).hasClass('focused')){
				$(this).addClass('focused');	
			}
		});			
			//mcl.set({date: strToDate(fv)});	
			$('#'+eid+"_cal").css({display:'none'});
		
	
	});
		
}

function initInlineEdit () {
	
	$('.inline-edit').each(function(index, element) {
		
		if($(this).hasClass('-inline-edit')){
			return;	
		}
        		
		$(this).addClass('-inline-edit').append('<a class="list-row-edit"></a>');
		
		$(this).find('.list-row-edit').click(function(){
			
			var pe = $(this).parents('.-inline-edit:eq(0)');
			var mp = pe.data();
			var pp = $(this).parents('div.list').eq(0).data();
			sp = {};

			$.extend(sp, pp, mp);

			sp.name = pe.find('.list-key').text();
			sp.value = pe.find('.list-value').text();
			pe.find('.list-value').data('value', sp.value);
			
			if(!sp.label){
				sp.label = sp.name;	
			}
			
			$('.is_editing').removeClass('is_editing');
			pe.addClass('is_editing');
						
			inline_edit(sp);
			
		});	
		
    });
}



function formatNumber () {
	var dv = $(this).val();
	if(parseFloat(dv)){
		$(this).val(dv.toString().replace(/\,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ","));	
	}
}

/** creating a new layer and retun id to load contents */
var layer_index = 0;
function createLayer (ref, no_min ){
	
	if(!ref){
		ref = '';	
	}
	else if($('div[data-ref='+ref+']').length > 0){
		
			tid = $('div.layer[data-ref='+ref+']').data('id');		
			
			if($('.layer-active').length == 1){
				$('.layer-active:eq(0)').find('.layer-min').click();				
			}
			
			$('#layer_'+tid).css({display:'block'}).addClass('layer-active');
			$('#tabhead'+tid).remove();
			return;
	}
	
	layer_index++;
	
	if(no_min){
		$(document.body).append('<div class="layer layer-active" id="layer_'+layer_index+'" data-id="'+layer_index+'" data-ref="'+ref+'"><div id="container'+layer_index+'"></div></div>');
		$('#layer_'+layer_index+' .layer-close').click(closeLayer);
	}
	else {
		$(document.body).append('<div class="layer layer-active" id="layer_'+layer_index+'" data-id="'+layer_index+'" data-ref="'+ref+'"><div class="layer-controls"><a class="layer-min"></a><a class="layer-close"></a></div><div id="container'+layer_index+'"></div></div>');
		$('#layer_'+layer_index+' .layer-min').click(minimizeLayer);
		$('#layer_'+layer_index+' .layer-close').click(closeLayer);
	}
	
	
	
	return 'container'+layer_index;
}

function minimizeLayer () {
	
	la = $('div.layer-active'); //$(this).parents('.layer:eq(0)');
	if(la.length == 0){
		return;	
	}
	
	tid = la.data('id');
	tt = la.find('.page-title:eq(0)').text();
	
	if(!tt){
		tt = 'New Tab';	
	}
	
	$('#tab-holder').append('<div class="tab-header" id="tabhead'+tid+'" data-id="'+tid+'" title="'+tt+'"><span>'+tt+'</span></div>');
	
	$('#tabhead'+tid).unbind().click(maximizeLayer);
	la.css({display:'none'}).removeClass('layer-active');
}

function maximizeLayer () {
	
	if($('.layer-active').length == 1){
		console.log('closing');
		$('.layer-active:eq(0)').find('.layer-min').click();
		
	}
	
	tid = $(this).data('id');
	$('#layer_'+tid).css({display:'block'}).addClass('layer-active');
	$(this).remove();
}

function closeLayer (lid) {

	if(typeof lid != 'object') {
		$('div.layer[data-ref='+lid+']').remove();
	}
	else {
		$(this).parents('.layer:eq(0)').remove();
	}
}

function closeActiveLayer () {
	
	als = $('.layer-active').length;
	if(als > 0){
		$('.layer-active').eq(als-1).remove();	
	}
	
}


function hideCalendar (cid) {
	
	if(!$('#'+cid+'_cal').hasClass('focused')){
		$('#'+cid+'_cal').css({display:'none'});
	}
}

function hideTimePicker (tid) {

	if(!$('#'+tid+'_time').hasClass('focused')){
		$('#'+tid+'_time').css({display:'none'});
	}	
}


function ToDate (dt){

//	var cd = ['Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	
	return month_abbr[dt.getMonth()]+' '+dt.getDate()+', '+dt.getFullYear();

	/*
	cd = dt.getDate() < 10 ? '0'+dt.getDate() : dt.getDate();
	cm = dt.getMonth() < 9 ? '0'+(dt.getMonth()+1) : (dt.getMonth()+1);
	
	return dt.getFullYear()+'-'+cm+'-'+cd;
	*/
}

function strToDate (str) {

	
	var dp = str.split(' ');
	mi = month_abbr.indexOf(dp[2]);

	return new Date(dp[3], mi, dp[1]);
	
}


var manualbuffer = function  (c_id, b_id, t, key) {
	
	this.c = c_id;
	this.b = b_id;
	this.key = key;
	this.t = t;
	this.page = 1;
	this.mid;
	this.prg = false;
	
	$('#'+this.b).click(wrap (this, function(e, o){
		
		if(o.prg){
			return;	
		}
		o.prg = true;
		
		if($('#'+o.b).data('theme') == 'orange'){
			$('#'+o.b).addClass('button-loading-orange').html('&nbsp;');	
		}
		else {
			$('#'+o.b).addClass('button-loading-blue').html('&nbsp;');	
		}
		
		o.page++;
		bc = call('/bff','key='+encodeURIComponent(o.key)+'&page='+o.page, null, true, "Loading..");
		bc.success = wrap(o, function (res, o) {
			$('#'+o.c).append(res.html);

			if(res.data.loaded >= o.t){
				$('#'+o.b).css({display: 'none'});
			}
			else {
				$('#'+o.b).removeClass('loading').html($('#'+o.b).data('label'));
			}
			
			o.page = res.data.page;
			if(o.mid){
				notificationManager.remove(o.mid, true);
			}
			o.prg = false;
			
			clearNotify();
		});
		bc.post();
		o.mid = bc.msgid;
	}));
		
	
}

/******************** Context Menu ********************/
function initContextMenu () {
	
	cmenus = $('.context-menu');
	if(cmenus.length > 0){
		
		cmenus.each( function() {
			
			if(!$(this).hasClass('ctx')){
				new ContextMenu(this);
			}	
		});
	
		$(document.body).append('<div id="contextmenu"></div>');
	
	}
	
	
}

var ContextMenu = function (ele) {
	
	var dt = $(ele).data();
	
	this.id = dt.id;
	this.show = false;
	this.menu = dt.menu;
	this.callback = dt.callback;

	$(ele).addClass('ctx');
	$(ele).click(this, function (e){
		e.stopPropagation();
		obj = window[e.data.menu];
		os  = $(this).offset();
		
		var cx = '';
		for(k in obj){
			
			cx += '<a href="javascript:void(0)" data-value="'+k+'" >'+obj[k]+'</a>';
			
		}
		
		$('#contextmenu').html(cx).css({display:'block'});
		$('#contextmenu a').click(e.data.id, window[e.data.callback]);
		
		$('#contextmenu').css({top:os.top+32, left:os.left-$('#contextmenu').width()+32});
		setTimeout("$(document.body).on('click', hideContextMenu)",200);
	});
	
}

function hideContextMenu () {
	$(document.body).off('click', hideContextMenu);
	$('#contextmenu').css({display:'none'});
}

/**
new Dialog( {
	title: 'Confirm',
	message : 'Are you sure?',
	mask : true,
	width: 300,
	buttons: {
			ok : {label : 'Ok', 'class' : '', callback: runCommand},
			cancel : {label : 'Cancel', 'class' : '', callback: closeDialog}
		}
});
 
	callback ( value, data, dialog);
*/

var Dialog = function (opt) {
	
	this.id = Math.round(Math.random()*100000);
	this.title = (opt && opt.title) ? opt.title : '&nbsp;';
	this.message = (opt && opt.message) ? opt.message : '&nbsp;';
	this.mask = (opt && opt.mask) ? true : false;
	this.width = (opt && opt.width && opt.width > 360) ? opt.width : null;
	this.height = (opt && opt.height) ? opt.height : 'auto';
	this.data = (opt && opt.data) ? opt.data : {};
	
	this.close = function(){
		
		if(this.mask && $('.dialog').length == 1){
			$('#mask').fadeOut('fast');
		}
		
		$('#d'+this.id).remove();
	}
	
	this.buttons = (opt && opt.buttons) ? opt.buttons : {'ok' : {label : 'Ok', 'class' : '', callback :null}};
	
	// render
	if(this.mask){
		if($('#mask').length == 0){
			$(document.body).append('<div id="mask" class="mask"></div>');
			
		}
		$('#mask').fadeIn('fast');
	}
	
	dc = '<div class="dialog" id="d'+this.id+'" '+(this.width ? 'style="width:'+this.width+'px"' : '')+'>';
	dc += '<div class="dialog-header"><div class="dialog-title">'+this.title+'</div><a href="javascript:void(0)" class="dialog-close" >X</a></div>';
    dc += '<div class="dialog-body">'+this.message+'</div>';
    dc += '<div class="dialog-buttons">';
	
	if(this.buttons['delete']){
		dc += '<div  class="dilog-buttons-left"><a  href="javascript:void(0)" class="button-trash" id="d'+this.id+'_delete" data-value="delete" >'+this.buttons['delete'].label+'</a></div>';
	}
	
	dc += '<div class="dilog-buttons-center"></div>';
	
	dc += '<div class="dilog-buttons-right">';
	
	for(b in this.buttons){
		
		if(b == 'delete'){
			continue;	
		}
		
		dc += '<a class="button '+(this.buttons[b].class ? this.buttons[b].class : '')+'" data-value="'+b+'" id="d'+this.id+'_'+b+'">'+this.buttons[b].label+'</a>';
	}
	dc += '</div></div></div>';
	
	$(document.body).append(dc);
	
	$('#d'+this.id+' a.dialog-close').click(closeDialog);
	
	for(b in this.buttons){
		$('#d'+this.id+'_'+b).click(this, function(e){
			dv = $(this).data('value');
			cb = e.data.buttons[dv].callback;
			if(cb) {
				cb(dv, e.data.data, e.data);
			}
			else {
				closeDialog();	
			}
		});
	}
	
	this.position = function () {
		
		$('#d'+this.id).css({height:''});
		
		ww = $(window).width();
		wh = $(window).height();
		dw = (this.width) ? this.width : $('#d'+this.id).width();
		dh = $('#d'+this.id).height();
		
		if(dw > ww){
			dw = ww;	
		}
		
		dt = Math.round((wh-dh)/2);
		dt = dt > 50 ? dt -50 : dt;
		
		if(dh > wh){
			dh = wh;
			$('#d'+this.id+' .dialog-body').css({overflow:'auto'});	
			$('#d'+this.id).css({top: dt, left:Math.round((ww-dw)/2), width:dw, height:dh });
		}
		else{
				$('#d'+this.id).css({top: dt, left:Math.round((ww-dw)/2), width:dw});
		}

	}
	
	$(window).resize(this, function(e){
		e.data.position();
	});
	this.position();
	DialogManager.add(this);
}


var DialogManager = { dialogs : []};

DialogManager.add = function (dlg) {
	DialogManager.dialogs.push(dlg);
}

DialogManager.get = function () {
	
	if(DialogManager.dialogs.length > 0){
		return DialogManager.dialogs.pop();	
	}
	
	return null;
}
DialogManager.getAll = function () {
	return 	DialogManager.dialogs;
}

DialogManager.close = function () {
	
	if(DialogManager.dialogs.length > 0){
		dlg = DialogManager.dialogs.pop();
		dlg.close();	
	}
	
	
}

DialogManager.closeAll = function () {
	
	if(DialogManager.dialogs.length > 0){
		for(var i = 0; i < DialogManager.dialogs.length; i++) {
			dlg = DialogManager.dialogs.pop();
			dlg.close();
		}
	}
}

function closeDialog () {
	DialogManager.close();
}


function redirect (res){
	
	if(res.data && res.data.url){
		window.location.href = base+res.data.url;
	}
}


function initTabs () {

	$('.tabs div.tab').unbind().click(function(){
		
		if(!$(this).hasClass('tab-active')){
			con = $(this).parents('div.pagelet:eq(0)');
			
			if(con.length == 0){
				con = $('#pagelet');
			}
			
			if(con.length == 1) {
				con.find('.tabs div.tab').removeClass('tab-active');
				$(this).addClass('tab-active');
				con.find('.tab-body').css({display:'none'});
				//con.find('.'+$(this).data('id')).css({);
			}
		}
		
		var dt = $(this).data();
		if(dt.url){
			notify("Loading...");
			
			if (dt.id){
				$('#'+dt.id).data('url', dt.url);
				$('#'+dt.id).css({display:'block'}).load(base+dt.url, function(){ 
				clearNotify();
				addPageListeners();
				});
			}
			else{
				$('.tab-body:eq(0)').data('url', dt.url);
				$('.tab-body:eq(0)').css({display:'block'}).load(base+dt.url, function(){ 
				clearNotify();
				addPageListeners();
				});
			}
			
		}
		else if (dt.id){
			$('#'+dt.id).css({display:'block'});
		}
		
		
	});
	
	// activate current tab
	$('.tab-active').click();
	
}


function reloadTab (con_id){
	
	url = $('#'+con_id).data('url');
	notify("Loading...");
	if(url){
		$('#'+con_id).css({display:'block'}).load(base+url, function(){ 
				clearNotify();
				addPageListeners();
			});	
	}	
}

function reloadActiveTab () {

	activetab = $('.layer-active .tab-active:eq(0)');
	
	if(activetab.length = 1){
		reloadTab(activetab.data('id'));
	}
	
}

function reloadActiveGrid () {
	
	$('#pagelet .linked-row-selected:eq(0)').click();
	
}


function inline_edit (dt) {

	if(dt.url){
		
		var chtml = '<div class="padding">';
		
		if(dt.key){
			chtml += '<div class="field"> <label>Name</label> <input type="text" id="inline_edit_name" value="'+(dt.label ? dt.label : '')+'" maxlength="255"> </div> <div class="field"> <label>Value</label> <input type="text" id="inline_edit_value" value="'+(dt.value ? dt.value : '')+'" maxlength="255"> </div>  ';	
		}
		else if(dt.field){
			
			if(dt.type && dt.type == 'number'){
				chtml += '<div class="field"> <label>'+(dt.label ? dt.label : '')+'</label> <input type="text" id="inline_edit_value" value="'+(dt.value ? dt.value : '')+'" maxlength="10" class="input-short"> </div>  ';
			}
			else if(dt.type && dt.type == 'date'){
				chtml += '<div class="field"> <label>'+(dt.label ? dt.label : '')+'</label> <input type="text" id="inline_edit_value" value="'+(dt.value ? dt.value : '')+'" maxlength="255" class="input-short datepick"> </div>  ';
			}
			else if(dt.type && dt.type == 'text'){
				chtml += '<div class="field"> <label>'+(dt.label ? dt.label : '')+'</label> <textarea  id="inline_edit_value" maxlength="4000" >'+(dt.value ? dt.value.replace('Add a description','') : '')+'</textarea> </div>  ';
			}
			else {
				chtml += '<div class="field"> <label>'+(dt.label ? dt.label : '')+'</label> <input type="text" id="inline_edit_value" value="'+(dt.value ? dt.value : '')+'" maxlength="255"> </div>  ';
			}
		}
		
		if(dt.id){
			chtml += '<div class="columns field"> <div class="column-fix"><a onClick=\'delete_object(null,'+JSON.stringify(dt)+')\' href="javascript:void(0)" class="button-trash">Delete</a></div><div class="column"></div> </div>';	
		}
		
		chtml += '</div>';
	
		new Dialog( {
			title: (dt.title) ? dt.title : 'Edit',
			message : chtml,
			mask : true,
			width: 300,
			data:dt,
			buttons: {
					ok : {label : (dt.id || dt.field) ? 'Update' : 'Create', 'class' : 'button-active', callback: function(v, dt){
						
						if($('#inline_edit_name').length > 0){
								
							kv = $('#inline_edit_name').val();
							
							if(kv && kv !== ''){
								
								dt.key = kv;
							}
							else{
								notify('Invalid name');
								return;	
							}									
						}
						else{
							kv = dt.label;
						}
						
						vv = $('#inline_edit_value').val();
						dt.value = vv;
						
						if(dt.type){
							
							if(dt.type == 'number'){
								dt.value = dt.value ? parseFloat(dt.value.replace(/\,/g,'')) : 0;
						   
							   if(isNaN(dt.value)){
								   notify("Invalid number");
									return;	
							   }
							   
							   vv = dt.value.toString().replace(/\,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							}								
						}
						
						if($('.is_editing').length > 0){
							
							pe = $('.is_editing').eq(0);
							pe.find('.list-value').html(vv);
							pe.find('.list-key').html(kv);
													
						}
						
						dt.action = 'save';
						
						cl = new call(dt.url, dt, null, true, 'Working...');
						cl.success = function(res, obj){
								
								if(!res.status){
									wef = $('.is_editing 	.list-value');
									wef.text(wef.data('value'));
								}
								
								callComplete(res, obj);
							}
						cl.post();						
												
						closeDialog();	
					}},
					cancel : {label : 'Cancel', 'class' : 'button-disabled', callback: function(){ 
						$('.is_editing').removeClass('is_editing'); 
						closeDialog();
					}}
				}
		});
		
		if(dt.type && dt.type == 'date'){
			initCalendar ();	
		}
		
		setTimeout('$("#inline_edit_name, #inline_edit_value")[0].focus()', 500);
			
	}
	else if(sp.callback && window[sp.callback]){				
		window[sp.callback](sp);	
	}
}

function delete_object (obj) {
	
	obj = decodeURIComponent(obj);
	obj = JSON.parse(obj);
	
	new Dialog( {
	title: 'Confirm',
	message : 'Are you sure you want to delete?',
	mask : true,
	width: 300,
	data:obj,
	buttons: {
			ok : {label : 'Delete','class' : 'button-active', callback: function (v, dt) {
				
				if(dt.url){
					
					dt.action = 'delete';
					cl = new call(dt.url, dt, null, true, 'Working...');
					cl.post();	
					closeDialog();
				}
				
				closeDialog();
					
			}},
			cancel : {label : 'Cancel', 'class' : 'button-disabled'}
			}
	});
	
}

/***** Photo galery ***********/
var gallery_photos = [];
var gallery_index = 0;
var gallery_target = null;
var gallery_is_loading = false;

function showGallery () {
	
	allp = $(this).parents('div:eq(0)').find('.gal');
	gallery_target = $(this).parents('div:eq(0)').data();
	
	gallery_photos = [];
	this_id = $(this).data('id');

	for(g = 0; g < allp.length; g++){
		pd = allp.eq(g).data();
		gallery_photos.push({id: pd.id, name: pd.name, title: allp[g].title});
		
		if(pd.id == this_id){
			gallery_index = g;
		}
	}
		
	$('#gallery').remove();
	
	gt = '<div id="gallery">';
	gt += '<div id="galary-loader"><div id="galary-loading"></div></div>';
	gt += '  <div class="gallery-canvas" id="gallery-canvas"><span class="gallery-sp"></span><img src="'+photo_base+'/thumb/'+gallery_photos[gallery_index].name+'"> <span class="gallery-sp"></span></div>';
	gt += '  <div id="gallery-caption">'+gallery_photos[gallery_index].title+'</div>';
	gt += '  <div class="gallery-nav"> <a class="gallery-nav-left"></a> <a class="gallery-nav-right"></a> </div>';
	gt += '  <a class="gallery-close">X</a> <a class="gallery-delete"></a> </div>';
		
	$('body').append(gt);
	
	$('#gallery-canvas img')[0].onload = stopGalareyLoader;
	
	$('.gallery-close').click(galleryClose);	//
	$('.gallery-nav-right').click(galleryNext);	
	$('.gallery-nav-left').click(galleryPrev);
	
	setTimeout('galaryLoading()',5);
	$('#gallery-canvas img')[0].src = photo_base+'/full/'+gallery_photos[gallery_index].name;	
	
	
	$(document).keydown(function(e) {
		
		e.preventDefault();
		console.log(e.originalEvent.keyCode);
        switch(e.originalEvent.keyCode){
			case 39: /* left arrow */
			case 32: /* space bar */
			case 13: /* enter */
			case 38: /* Top */
					galleryNext();
					break;
			case 40: /* bottom */		
			case 37: galleryPrev();
					break;
			case 27: galleryClose();
					
		}
    });
	
	
	$('.gallery-delete').click(function () {
		
		new Dialog( {
		title: 'Confirm',
		message : 'Do you want to delete this photo?',
		mask : false,
		width: 300,
		data:5,
		buttons: {
				ok : {label : 'Delete', 'class' : 'button-active', callback: function(){
					
					cl = new call('/sd/delete_doc', 'doc_id='+gallery_photos[gallery_index].id+'&type='+gallery_target.type+'&object='+gallery_target.object+'&id='+gallery_target.id, null, true, 'Deleting image...' );
					cl.post();					
					closeDialog();
						
				}},
				cancel : {label : 'Cancel', 'class' : 'button-disabled'}
			}
	});
				
		});
}

function stopGalareyLoader () {
	
	if($('#gallery-canvas img')[0].src.indexOf('\/full\/') > 0) {
		console.log('stopping');
		gallery_is_loading = false;
		$('#galary-loading').stop().css({width:0});	
	}
}

function galaryLoading(){
	
	gallery_is_loading = true;
	
	$('#galary-loading').stop().animate({width:'100%'},3000, function (){
		$('#galary-loading').stop().css({width:0});	
		
		
	});
	
}
function galleryNext () {
			
		if(gallery_index < gallery_photos.length - 1 ){
			gallery_index++;			
		}	
		else{
			gallery_index = 0;	
		}
		$('#gallery-canvas img')[0].src = photo_base+'/thumb/'+gallery_photos[gallery_index].name;
		setTimeout('galaryLoading()',5);
		$('#gallery-canvas img')[0].src = photo_base+'/full/'+gallery_photos[gallery_index].name;
		$('#gallery-caption').html(gallery_photos[gallery_index].title);
	
}

function galleryPrev () {
	
			
		if(gallery_index > 0 ){
			gallery_index--;			
		}
		else{
			gallery_index = gallery_photos.length -1;	
		}
		$('#gallery-canvas img')[0].src = photo_base+'/thumb/'+gallery_photos[gallery_index].name;
		setTimeout('galaryLoading()',5);
		$('#gallery-canvas img')[0].src = photo_base+'/full/'+gallery_photos[gallery_index].name;
		$('#gallery-caption').html(gallery_photos[gallery_index].title);
	
}

function galleryClose () {
	
	$('#gallery').fadeOut('fast', function() {
		$(this).remove();	
	});
	
	$(document).off('keydown');		
}

/**************** Place search *******************/

function initPlaceSearch () {

	$('.addresspick').each(function(index, element) {
		
		mapService = new google.maps.places.Autocomplete(this, {});
		google.maps.event.addListener(mapService, 'place_changed', function() {
			place = mapService.getPlace();
			console.log(place);
			address = AddressPicker.parseAddress(place);
			console.log(address);
			AddressPicker.populate(address);
		});
		
		
    });
	console.log('attached');

	

}


var AddressPicker = {};

AddressPicker.parseAddress = function ( address ) {
		
		var comp = {'place' : address.name};
		
		for (c = 0; c < address.address_components.length ; c++){
		
			var ac = address.address_components[c];
			
					
			if(ac['types'][0] == 'route'){
				comp['route'] = ac.long_name;
			}
			else if(ac['types'][0] == 'neighborhood'){
				comp['neighborhood'] = ac.long_name;
			}
			else if(ac['types'][0] == 'political'){
				comp['political'] = ac.long_name;
			}
			else if(ac['types'][0] == 'locality'){
				comp['locality'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_3'){
				comp['subcity'] = ac.long_name;
			}
			else if(ac['types'][0] == 'postal_code'){
				comp['postal'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_2'){
				comp['city'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_1'){
				comp['region'] = (/^[a-zA-Z0-9 \.]+$/.test(ac.short_name)) ? ac.short_name : ac.long_name ;
			}
			else if(ac['types'][0] == 'country'){
				comp['country'] = ac.short_name;
			}
			else if(ac['types'][0] == 'establishment'){
				comp['place'] = (/^[a-zA-Z0-9 \.]+$/.test(ac.short_name)) ? ac.short_name : ac.long_name;
			}
		
		}
		
		// compose address
		comp['address'] = [];
		/*if(comp['place']){
			comp['address'].push(comp['place']);
			delete comp['place'];
		}*/
		if(comp['route']){
			comp['address'].push(comp['route']);
			delete comp['route'];
		}
		if(comp['neighborhood']){
			comp['address'].push(comp['neighborhood']);
			delete comp['neighborhood'];
		}
		if(comp['locality']){
			if(comp['city'] != comp['locality']) {
				comp['address'].push(comp['locality']);
			}
			delete comp['locality'];
		}
		if(comp['political']){
			if(comp['city'] != comp['political']) {
				comp['address'].push(comp['political']);
			}
			delete comp['political'];
		}
		if(comp['subcity']){
			if(comp['subcity'] != comp['city']) {
				comp['address'].push(comp['subcity']);
			}
			delete comp['subcity'];
		}
		
		comp['address'] = comp['address'].join(', ');
		
		if((!comp['city'] || comp['city'] == '') && (comp['region'] && comp['region'] != '')) {
			comp['city'] = comp['region'];
			comp['region'] = '';
		}
		
		return comp;
		
	}

AddressPicker.populate = function (dt){
	
		if(dt.place) {
			$('._place').val(dt.place).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._place').val('');	
		}
		if(dt.address) {
			$('._address').val(dt.address).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._address').val('');	
		}
		if(dt.city) {
			$('._city').val(dt.city).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._address').val('');
		}
		if(dt.region) {
			$('._region').val(dt.region).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._region').val('');	
		}
		if(dt.postal) {
			$('._postal').val(dt.postal).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._postal').val('');	
		}
		
		if(dt.country) {
			$('._country').val(dt.country).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._country').val('');	
		}
				
}


function AddressCall (res, obj){
			console.log(res);
			console.log(obj);
	if(res && res.results && res.results.length > 0){
		obj.setValues(res.results);
	}
	else{
		// close autofill
		obj.clear();
	}
	
}

/************  Sitewide common function ***********/
var x_otp_counter;
var x_otp_progress = false;

function initOTP () {
	x_otp_counter = 120;
	otpCountDown();
	
	$('#vcode').keyup(function() {
		$('#_server_error').html('');

		if(this.value.length == 6){
			validateOTP();
		}
	})[0].focus();
	
	//$('#vcode').on('paste', validateOTP);
}

function otpCountDown () {
	
	if(x_otp_counter <= 0){
		
		document.getElementById('otp_resend').innerHTML = '<a href="javascript:void(0)" onClick="resendOTP()" >Re-send one time passcode</a>';
		
	}
	else{
		otp_delay_m = Math.trunc(x_otp_counter/60);
		otp_delay_s = x_otp_counter%60;
		
		document.getElementById('otp_delay').innerHTML = otp_delay_m+':'+(otp_delay_s < 10 ? '0' : '')+otp_delay_s;
		
		setTimeout('otpCountDown()',1000);
	}
	
	x_otp_counter--;
}



function refreshObject (dt){
	
	
		
}
