

/*************** CURA framework ******************/

var cura = {
		action:{},
		validator:{},
		forms: {},
		widgets: {},
		util: {},
		handler : {},
		attach : {},
		location:{},
		ncount : 0,
		nlast : 0,
		data: {
				months : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
				days : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
				st: ['','st','nd','rd','th','th']
			}
	};


cura.bind = function () {
	
	// bind all action elements into automatic handles
	$('[cura]').not('.cura').each(function() {
        
		$(this).addClass('cura');

		var types = $(this).data('type');
		var act = $(this).attr('cura');
				
		if(!cura.action[act]){
			//console.log('CURA Action not found: '+act);
			return;	
		}
		
		var action = cura.action[act];
		
		if(types){
			types = types.split(',');
			
			for(var a = 0; a < types.length; a++){
				
				if(types[a] == 'load'){
					// run immidiately
					cura.execute($(this).attr('cura'), this);
				}
				else {
					$(this).on(types[a], cura.interact);
				}
			}
		}
		else{
				
			if(this.nodeName == 'FORM' || action.submit){
				cura.forms.addValidation(action);				
			}
			
			if(this.nodeName == 'FORM'){
				$(this).on('submit', cura.interact);
			}
			else{
				$(this).on('click', cura.interact);
			}
			
		}
		
    });
	
	/* initialize all widgets */
	for(var w in cura.attach){
		cura.attach[w]();	
	}
	
}

/** User interaction on element **/
cura.interact = function (e){
	
	//e.stopPropagation();
	e.preventDefault();
	
	cura.execute($(this).attr('cura'), this, e);
	
	return false;
}

/** Execute an action */
cura.execute = function (action_name, element, para, obj) {
	
	var action = null;
	
	if(cura.action[action_name]){
		action = cura.action[action_name];	
	}
	else{
	//	console.log('CURA Action not found: '+action_name);
		return;
	}
	
	var data = {};
	
	if(element) {
		let edt = $(element).data();
		
		if(edt){
			
			for(var k in edt){		
				data[k] = edt[k];
			}
		
		}
	}
	
	if(action.data){
		data = 	cura.util.mergeData(action.data, data);		
	}
		
	if(action.callback && action.callback(data,  element, para, obj) === false) {
		return;		
	}
	
	if(!action.url){
		return;
	}

	data._action = action_name;

	if(action.submit){

		if(!cura.forms.validate(action)){
			notify( action.error ? action.error : 'Please fix errors');
			return;
		}
		
		var formdata = $('#'+action.submit).serializeArray();

		for(var a = 0; a < formdata.length; a++){
			data[formdata[a].name] = formdata[a].value;	
		}
	}

	if(action.confirm) {
		
		if(!action.confirmed){
			new Dialog({
				title:'Confirm',
				message: action.confirm,
				data:{element:element, action: action_name},
				mask:true,
				buttons: {
						ok: {label: (action.confirm_button ? action.confirm_button : 'Confirm'), callback : function (v, dt) {
							cura.action[dt.action].confirmed = true;
							cura.execute(dt.action, element);
							
							unblock();
							closeDialog();
						}						
						},
						cancel : {label : 'Dismiss', class: 'button-alt'}
					}
			});
			
			return;
		}
				
	}
	
	if(action.loading){
		$('#'+action.loading).html('<div class="inline-loader"></div>');
	}
	let msg = null;
	
	if(action.message === false){
		msg = null;
	}
	else{
		msg = (action.message ? action.message : 'Working..')
	}
	
	//console.log(data);		
	if(data){
		
		data.tz = cura.tz;
		data.dfp = cura.dfp ? cura.dfp : null;
		
		/* send POST */
		cura.util.call(action.url, data, msg , 
			(action.html ? false : true), (action.element ? action.element : null)
		 ).post();			 
	}
	else{
		cura.util.call(action.url, data,msg , 
			(action.html ? false : true), (action.element ? action.element : null)
		 ).send();	
	}
	
	if(!action.hasOwnProperty('mask') || action.mask ){
		block();	
	}
	
	if(action.confirmed){
		delete(action.confirmed);	
	}	
}

/** Local hostory */
cura.history = {_store:[]};

cura.history.add = function (co){
	
	if(cura.history._store.length == 10){
		cura.history._store.shift();
	}
	
	cura.history._store.push(co);
	
}

cura.history.go = function(steps){
	
	steps = parseInt(steps);
	
	if(!steps && steps !== 0){
		steps = 1;	
	}
	
	var ts = null;
	for(var h = 0; h <= steps; h++){
		
		if(cura.history._store.length > 0){
			ts = cura.history._store.pop();	
		}
	}
	
	if(ts){
		ts.send();	
	}
		
	//steps++;
	//var ts = cura.history._store.length - steps;	
	//cura.history._store[ts].send();
	
}

/** Form validation function */
cura.forms.validate = function (action) {

	is_valid = true;
	
	form = $('#'+action.submit);
	var vld = action.validator;
	
	if(vld){
		
		var fields = form.find('input,select,textarea');
		
		for(var f = 0; f < fields.length; f++){			
			
			try {
				if(fields[f].id) {
					if(!vld.element('#'+fields[f].id)){
						is_valid = false;	
					}
				}
			}
			catch(ex){
				console.log(fields[f]);
				console.log("Error found");
			}					
		}		
				
		if(!is_valid){
			return false;
		}	
		
	}
	
	return true;

}

cura.forms.addValidation = function (action) {
	/* Add element blur listeners */
		
	if(action.validate){
		
		form = $('#'+action.submit);
		action.validator = form.validate(action.validate);
		
		form.find('input,select,textarea').blur(wrap(action.validator, function(e, validator){
			if(this.id) {
				validator.element('#'+this.id);
			}
		}));	
	}	
}

/** Utility function **/
cura.util.mergeData  = function ( data1, data2) {

	var out = [];
	
	if(data1){	
		for (var k in data1){
			out[k] = (typeof data1[k] == 'function') ? data1[k]() : data1[k];	
		}
	}
	
	if(data2){
		
		for(k in data2){
			out[k] = (typeof data2[k] == 'function') ? data2[k]() : data2[k];		
		}
	}
	return out;	
}

/*** Format number by adding commas **/
cura.util.formatNumber = function () {
	var dv = $(this).val();
	if(parseFloat(dv)){
		$(this).val(dv.toString().replace(/\,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ","));	
	}
}

/** make ajax request **/
cura.util.call = function  (url, data,msg , json, element) {
	return 	new CURACall(url, data,msg , json, element)
}

cura.handler.callComplete = function (res, obj) {
	

	//console.log('Time:'+( new Date().getTime() - obj._start)) ;
	
	unblock();
	
	if(obj.action){
		
		if(obj.json &&  typeof res.data == 'object' && cura.action[action]){
			cura.action[action].data = cura.util.mergeData(cura.action[action].data, res.data);			
		}
		
		cura.execute(obj.action);	
	}
	
	if(obj.json){
		
		if(res.message){
			obj.msgid = notify(res.message, res.status);
		}
		else{
			clearNotify();	
		}
			
		
		if((res.html || res.append) && res.id){
			
			if(res.id == 'dash-contents'){
				cura.history.add(obj);
				setTimeout('$(window).scrollTop(0)',400);	
			}
							
			if(res.append){
				console.log('appending');
				$('#'+res.id).append(res.append);
			}
			else{
				$('#'+res.id).html(res.html);
			}
			
			cura.bind();			
		}
		
		if(res.trigger){
			
			cura.execute(res.trigger, null, res, obj);
		}
	
		if(res.action){
			
			cura.execute(res.action, null, res, obj);
		}
		
		if(res.nodes){
			
			for(p = 0; p < res.nodes.length; p++){
				
				if(	res.nodes[p].id || res.nodes[p].class ){
					
					tele = res.nodes[p].class ? '.'+res.nodes[p].class : '#'+res.nodes[p].id;
					
					if(res.nodes[p].hasOwnProperty('append')){
						$(tele).append(res.nodes[p].append);	
					}
					
					if(res.nodes[p].hasOwnProperty('html')){
						$(tele).html(res.nodes[p].html);	
					}
					
					if(res.nodes[p].hasOwnProperty('value')){
						$(tele).val(res.nodes[p].value);	
					}
					
					if(res.nodes[p].options){
						ov = '';
						for(k in res.nodes[p].options){
							ov += '<option value="'+k+'">'+res.nodes[p].options[k]+'</option>';						}
						$(tele).html(ov);	
					}
					
					if(res.nodes[p].trigger){
						cura.execute(res.nodes[p].trigger);
					}
				}
				else{
					console.log("Node id undefined");	
				}
			}			
			
			cura.bind();
		}
		
		if(res.error){
			
			if(document.getElementById('_server_error')){
				$('#_server_error').html('<div class="_server_error">'+res.error+'</div>');
				cura.bind();
			}
			else{
				new Dialog( {
					title: 'Alert',
					message : res.error,
					mask : true,
					width: 300,
					data:5,
					buttons: {
							cancel : {label : 'Close', 'class' : 'button-alt'}
						}
				});	
			}
		}
		
		if(res.page && res.page.title && res.page.url){
			document.title = res.page.title+' - Curatalent';
			window.history.pushState({}, res.page.title, res.page.url);				
		}
								
	}
	else{
		
		if(obj.element) {
			
			if(obj.append){
				$(obj.element).append(res);
			}
			else{
				$(obj.element).html(res);
			}
			
			cura.bind();
		}
	}
}

cura.handler.callFailed = function (xhr, e, obj){
	
	if(e == 'error'){
		obj.msgid = notify("Something went wrong. Please try again.", false);
	}
	else if(e == 'timeout'){
		obj.msgid = notify("Server not reachable. Check your internet connection.", false);
	}
	else if(e == "parsererror"){
		obj.msgid = notify("Something went wrong. <br>Please try again later", false);	
		console.log(obj);
	}
		
	if(obj.retry && obj.attempt < 3){
		obj.send();	
	}
	
	unblock();
	
	// log error report
	if(e != 'timeout' && obj.url.indexOf('error_report') == -1 && obj.url.indexOf('error_contact') == -1){
		
		let bin = {};
		bin.request = JSON.stringify(obj);
		bin.response = xhr.responseText;
		
		let cl = new CURACall('/sd/error_report',bin,'Please wait...', true);
		cl.post();
		
	}
	
}

/** dynamic label helper **/
cura.handler.dynamicLabel = function (){
	$(this).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
}

cura.attach.dynamicLabel = function () {
	
	$('.field-wrapper input, .field-wrapper select, .field-wrapper textarea').off('focus', cura.handler.dynamicLabel).on('focus', cura.handler.dynamicLabel).each( function () {
		
			if($(this).val()){
				$(this).trigger('focus');
			}			
		});	
		
}


cura.attach.dropdown = function () {
	
	$('select').each(function(i, e) {
		
		if($(this).hasClass('_dropdown')){
			return;
		}
		
		$(this).addClass('_dropdown');
		
		if(!this.id){
			this.id = cura.util.randomID();
		}
		
		$(this).click(function(e) {
			
			
			
			if(window.innerWidth <= 600) {
				
				this.blur();
				e.stopPropagation();
				
				if($('#dropdown').length == 0){
					$("body").append('<div id="dropdown"></div>');
				}
				
				let lbl = $(this).parents('div.field-wrapper:eq(0)').find('label:eq(0)');
				
				lbl = lbl.length > 0 ? lbl.text() : this.name;
								
				let txt = '<div class="header flex"><div class="resizable">'+lbl+'</div><div class="header-close">X</div></div>';
				
				let options = $(this).find('option');
				
				
				
				for(let a = 0; a < options.length; a++){
					txt += '<a cura="dropdown_set" data-id="'+this.id+'" data-value="'+options.eq(a).val()+'">'+options.eq(a).text()+'</a>'
				}
				
				//txt += '</div>';
				
				
				$("#dropdown").html(txt);
				$("#dropdown a").click(function (e){
					e.stopPropagation();
					let dt = $(this).data();
					$('#'+dt.id).val(dt.value).change();
					hideDropdown();
				});
				
				$('#dropdown .header-close').click(function() {
					hideDropdown();
				})
				
				$('#dropdown').css({top:'auto'});
				$('body').addClass('noscroll');
				
				$(document.body).on('click', hideDropdown);
			}
			
		});
		
		this.blur();
	});
	
}

/** custom option helper **/
cura.handler.customOption = function () {

	if(this.checked){
		$('.custom-option-selected').removeClass('custom-option-selected');
		$(this).parents('.custom-option:eq(0)').addClass('custom-option-selected');
	}
		
}

cura.attach.customOption = function () {
	$('.custom-option input').off('change',cura.handler.customOption).on('change',cura.handler.customOption).trigger('change');
}

cura.attach.tooTips = function () {
	
	$('.tooltip').off('click').on('click', function(e){
		e.stopPropagation();
		$('.tooltip').removeClass('active');
		$(this).addClass('active');
		$(document.body).on('click',hideToolTip);	
	});	
}

/** Time pickers **/
cura.attach.timepicker = function () {
	
	$('.timepick').each(function(){
		new TimePicker(this);
	});
	
}

/** Date  pickers **/
cura.attach.datepicker = function () {
	
	$('.datepick').each(function (){

		$(this).removeClass('datepick')
		
		var eid = this.id;
		
		var fv = $(this).val();
		var cal_id = eid+'_cal';
		
		if($('#'+cal_id).length == 0) {
			$(this).after('<div class="vanilla-calendar" id="'+cal_id+'" data-id="'+eid+'"></div>');
		}
		
		if($(window).width() - $(this).offset().left < 350){
			$('#'+cal_id).css({right:0});
		}
						
		var mcl =  new VanillaCalendar({
		selector: '#'+eid+"_cal",
		pastDates: $(this).hasClass('past'),
		onSelect: function (dt, ele){
			
				dt = new Date(dt.date);

				var tid = $(ele).parents('div.vanilla-calendar:eq(0)').data('id');
				var dtxt =  ToDate(dt);
				$('#'+tid).val(dtxt);
				
				cb = $('#'+tid).data('changed');
				if(cb) {	
					if(window[cb]){
						window[cb](dtxt);
					}
					else if (cura.action[cb]){
						cura.execute(cb,$('#'+tid)[0], dtxt);
					}
				}
				$('#'+tid+'_cal').css({display:'none'});	
			}
		});
		
		
		
		$(this).click(function () {
			$('#'+eid+"_cal").css({display:'block'}).removeClass('focused');	
		});
		$(this).blur(function () {
			
			setTimeout( wrap(this.id, function(e, cid){
					if(!$('#'+cid+'_cal').hasClass('focused')){
						$('#'+cid+'_cal').css({display:'none'});
					}
				}), 200);
			
		});
		
		$('#'+eid+"_cal").click(function () {
			if(!$(this).hasClass('focused')){
				$(this).addClass('focused');	
			}
		});			
			//mcl.set({date: strToDate(fv)});	
			$('#'+eid+"_cal").css({display:'none'});
		
	
	});
}

/** automatic number formatter **/
cura.attach.numbers = function () {

	$('input.number').each(function(index, element) {
		$(this).removedClass('number');
		
		$(this).keyup(cura.util.formatNumber).trigger('keyup');
        
    });
	
}

/** jumper controller **/
cura.attach.jumper = function(){
	
	$('.jumper').each(function(index, element) {
		
		if($(this).hasClass('_jumper')){
			return;	
		}
		
		$(this).addClass('_jumper');
		
		$(this).find('a:first-child').click(function(){
			
			var p = $(this).parent('div:eq(0)');
			var dt = p.data();
			cura.execute(dt.action, p, -1);
				
		});
		
		$(this).find('a:last-child').click(function(){
			
			var p = $(this).parent('div:eq(0)');
			var dt = p.data();
			cura.execute(dt.action, p, 1);
				
		});
				
	});
	
}

/** address picker widget **/
cura.attach.addresspicker = function () {
	
	if(!window['google'] || !google.maps){
		return;	
	}
	
	$('.addresspick').each(function(index, element) {
		
		$(this).removeClass('addresspick');
		
		mapService = new google.maps.places.Autocomplete(this, {});
		google.maps.event.addListener(mapService, 'place_changed', function() {
			place = mapService.getPlace();
			//console.log(place);
			address = cura.util.parseAddress(place);
			//console.log(address);
			cura.util.populateAddress(address);
		});
		
    });
}

cura.attach.citypicker = function () {
	
	if(window['google']) {
	
	$('.citypick').each(function(index, element) {
		$(this).removeClass('citypick');
		
		mapService = new google.maps.places.Autocomplete(this, {types:["(cities)"]});
		google.maps.event.addListener(mapService, 'place_changed', function() {
			place = mapService.getPlace();
			//console.log(place);
			address = cura.util.parseAddress(place);
			//console.log(address);
			cura.util.populateAddress(address);
		});
		
		
		});
		
	}
	else{
		console.log('wating till google loads');
	}
		
}

cura.attach.profile_slider = function (){
	
	$('.profile-slider').each(function() {
		
		if(!$(this).hasClass('attched')){
			
			$(this).addClass('attched');
			
			let ph = $(this).find('.slide-pane').height();
			if(ph < 430){
				ph = 430;
			}
			$(this).height(ph);
			
			$(this).find('.left-nav').click(function(){
				
				let ww = $(window).width();
				let pane = $(this).parents('div.profile-slider:eq(0)').find('.slide-pane');
				let os = pane.offset();
				let ow = pane.width();
				
				if(os.left == 0){
					pane.css({'left':(ow-ww)*-1});
				}
				else if(os.left*-1 > ww){
					pane.css({'left':os.left+ww});
					
				}
				else{
					pane.css({'left':0});
				}
				
				
			});
			$(this).find('.right-nav').click(function(){
				
				let ww = $(window).width();
				let pane = $(this).parents('div.profile-slider:eq(0)').find('.slide-pane');
				let os = pane.offset();
				let ow = pane.width();

				if(os.left+ow > ww){
					if(os.left+ow - ww > ww){
						pane.css({'left':os.left-ww});
					}
					else if(os.left+ow - ww <50){
						pane.css({'left':0});
					}
					else{
						pane.css({'left':os.left-(os.left+ow - ww)});
					}
				}
				else{
					pane.css({'left':0});
				}
			});
		}

	});
	
}

cura.util.parseAddress = function (address) {

		var comp = {'place_id' : address.place_id ,'name' : address.name};	
		
		type = address.types.pop();
		
		comp['display'] = (type == 'establishment' ? address.name+', ' : '')+address.formatted_address;
		
		for (c = 0; c < address.address_components.length ; c++){
		
			var ac = address.address_components[c];
			
					
			if(ac['types'][0] == 'route'){
				comp['route'] = ac.long_name;
			}
			else if(ac['types'][0] == 'neighborhood'){
				comp['neighborhood'] = ac.long_name;
			}
			else if(ac['types'][0] == 'political'){
				comp['political'] = ac.long_name;
			}
			else if(ac['types'][0] == 'locality'){
				comp['locality'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_3'){
				comp['subcity'] = ac.long_name;
			}
			else if(ac['types'][0] == 'postal_code'){
				comp['postal'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_2'){
				comp['city'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_1'){
				comp['region'] = (/^[a-zA-Z0-9 \.]+$/.test(ac.short_name)) ? ac.short_name : ac.long_name ;
			}
			else if(ac['types'][0] == 'country'){
				comp['country'] = ac.short_name;
			}
			else if(ac['types'][0] == 'establishment'){
				//comp['place'] = (/^[a-zA-Z0-9 \.]+$/.test(ac.short_name)) ? ac.short_name : ac.long_name;
			}
		
		}
		
		// compose address
		comp['address'] = [];
		/*if(comp['place']){
			comp['address'].push(comp['place']);
			delete comp['place'];
		}*/
		if(comp['route']){
			comp['address'].push(comp['route']);
			delete comp['route'];
		}
		if(comp['neighborhood']){
			comp['address'].push(comp['neighborhood']);
			delete comp['neighborhood'];
		}
		if(comp['locality']){
			if(comp['city'] != comp['locality']) {
				comp['address'].push(comp['locality']);
			}
			delete comp['locality'];
		}
		if(comp['political']){
			if(comp['city'] != comp['political']) {
				comp['address'].push(comp['political']);
			}
			delete comp['political'];
		}
		if(comp['subcity']){
			if(comp['subcity'] != comp['city']) {
				comp['address'].push(comp['subcity']);
			}
			delete comp['subcity'];
		}
		
		comp['address'] = comp['address'].join(', ');
		
		if((!comp['city'] || comp['city'] == '') && (comp['region'] && comp['region'] != '')) {
			comp['city'] = comp['region'];
			comp['region'] = '';
		}
		
		return comp;
	
}

cura.util.populateAddress = function (dt) {	

		cura.location = dt;
		
	/*
		if(dt.place) {
			$('._place').val(dt.place).trigger('focus');
		}
		else{
			$('._place').val('');	
		}
		if(dt.address) {
			$('._address').val(dt.address).trigger('focus');
		}
		else{
			$('._address').val('');	
		}
		if(dt.city) {
			$('._city').val(dt.city).trigger('focus');
		}
		else{
			$('._address').val('');
		}
		if(dt.region) {
			$('._region').val(dt.region).trigger('focus');
		}
		else{
			$('._region').val('');	
		}
		if(dt.postal) {
			$('._postal').val(dt.postal).trigger('focus');
		}
		else{
			$('._postal').val('');	
		}
		
		if(dt.country) {
			$('._country').val(dt.country).trigger('focus');
		}
		else{
			$('._country').val('');	
		}				
	*/
}


/** Ajax request wrapper   **/

var CURACall = function (url, data, msg, json, element){
	
	// call status, -1 : failed,  0: not sent, 1: in progress, 2:completed
	this.status = 0;
	// url to call
	this.url = base+url;
	// request method
	this.method = 'GET';
	// data to be sent, if POST, post data else, URL parameters
	this.data = data;
	// is this call return json response
	this.json = json ? true : false;
	
	// success callback function
	this.success = null;
	
	// success fallback action 
	//(only execute if there is no success function)
	this.action = null;
	// failur callback function
	this.fail = null;
	
	// DOM element to add data
	this.element = element ? element : '#pagelet';
	// whether add data or append 
	this.append = false;

	// message to show while loading
	this.message = msg;	
	// notification id
	this.msgid = null;
	
	// what to retry sending request
	this.retry = false;
	// number of attempts made to resend request
	this.attempts = 0;	
	
	
	this._start = 0;
 
}


CURACall.prototype.post = function () {
	
	this.method = 'POST';
	this.send();
}
CURACall.prototype.get = function () {
	this.method = 'GET';
	this.send();
}

CURACall.prototype.load = function () {
	this.method = this.data ? 'POST' : 'GET';
	this.send();
}

CURACall.prototype.send = function () {
	
	this.attempts++;
	
	if(this.success){
		callback = wrap(this, this.success);
	}
	else {
		callback = wrap(this, cura.handler.callComplete);	
	}
	
	if(this.fail){
		faliur = wrap3(this, this.fail);	
	}
	else{
		failur = wrap3(this, cura.handler.callFailed);	
	}
	
	o = {
		method: this.method,
		url: this.url,
		error: failur,
		success: callback	
	}
	
	if(this.json){
		o.dataType = 'json';
	}
	
	if(this.data){
		
		if(typeof this.data == 'object'){
			qs = [];
			for(k in this.data){
				qs.push(encodeURIComponent(k)+'='+encodeURIComponent(this.data[k]));	
			}
			o.data = qs.join('&');	
		}
		else{
			o.data = this.data;	
		}
	}
	
	
	if(this.message){
		this.msgid = notify(this.message);
	}
	
	this._start = new Date().getTime();
	$.ajax(o);

}

cura.action.error_contact = {
	url:'/sd/error_contact',
	callback:function(dt, e) {
		
		let val = $('#user_contact').val();
	
		if(val && val.length > 5){
			dt.contact = val;
			dt.id = $('#user_contact').data('id');
	
			closeDialog();
			return true;
		}
	
		closeDialog();
		return false;
	},
		mask:false
}


cura.action.get_tags = {
	url:'/sd/get_tags',
	mask:false,
	message:false
}

cura.action.cura_tags = {
	callback: function(dt, e , res){
		cura.tags = res.data;
	}
}

/************** General methods **************/
var tom = null;
function notify (msg) {
	
	$('#notifications').html('<a class="notification">'+msg+'</a>');
	if(tom){
		clearTimeout(tom);
	}	
	tom = setTimeout('clearNotify()', 5000);
}

function clearNotify (){
	$('.notification').fadeOut('fast', function(){ $(this).remove()});
	if(tom){
		clearTimeout(tom);
	}	
}

function block () {
	$('#blocker').fadeIn('fast');
}

function unblock () {
	$('#blocker').fadeOut('fast');
}


function wrap (obj, callback){
	return function (res) {		
		var data = obj;		
		callback(res, data);			
	}	
}

function wrap2 (obj, callback){
	return function (a, b, c) {		
		var data = obj;		
		callback(b, data);			
	}	
}
function wrap3 (obj, callback){
	return function (a, b, c) {		
		var data = obj;		
		callback(a,b, data);			
	}	
}




var TimePicker = function (jele) {
	
	$(jele).removeClass('timepick');
	
	this.active_class = 'tpa';
	
	uid = jele.id+'_time';
	
	var dtcodes = ' <div class="tp-outer"> <div class="tp-left"><div class="tp-header">hours</div><div class="tp-wrapper tp-hour"><div class="tp-row"> <a data-val="1">1</a> <a data-val="2">2</a> <a data-val="3">3</a> <a data-val="4">4</a> </div><div class="tp-row"> <a data-val="5">5</a> <a data-val="6">6</a> <a data-val="7">7</a> <a data-val="8">8</a> </div><div class="tp-row"> <a data-val="9">9</a> <a data-val="10">10</a> <a data-val="11">11</a> <a data-val="12">12</a> </div></div><div class="tp-header">AM/PM</div><div class="tp-wrapper"><div class="tp-row tp-ampm"> <a data-val="AM">AM</a> <a data-val="PM" >PM</a> </div></div> </div> <div class="tp-right"><div class="tp-header">Minutes</div><div class="tp-row tp-minutes tp-wrapper"> <a data-val="00">:00</a> <a  data-val="15">:15</a> <a data-val="30">:30</a> <a data-val="45">:45</a> </div> </div> </div> ';
	
	if($('#'+uid).length == 0){
		$(jele).after ('<div class="timepicker" id="'+uid+'" data-id="'+jele.id+'" style="display:none">'+dtcodes+'</div>');
	}
	else {
		$('#'+uid).html(dtcodes);
	}
	
	val = $(jele).val();
	if(val && val.length > 5){
		sep = val.split(':');
		sh = parseInt(sep[0]);
		sm = parseInt(sep[1].substr(0,2));
		sa = sep[1].substr(2,2);
		
		tp = $('#'+uid);
		tp.find('.tp-hour a:eq('+(sh-1)+')').addClass('selected');
		
		if(sm == 0){
			tp.find('.tp-minutes a:eq(0)').addClass('selected');
		}
		else if(sm == 15){
			tp.find('.tp-minutes a:eq(1)').addClass('selected');
		}
		else if(sm == 30){
			tp.find('.tp-minutes a:eq(2)').addClass('selected');
		}
		else {
			tp.find('.tp-minutes a:eq(3)').addClass('selected');
		}
		
		if(sa.toLowerCase() == 'am'){
			tp.find('.tp-ampm a:eq(0)').addClass('selected');
		}
		else{
			tp.find('.tp-ampm a:eq(1)').addClass('selected');
		}
	}
	
	$('.timepicker a').unbind().click(function (){
		pe = $(this).parents('div.timepicker:eq(0)');
		be = pe.data('id');
		
		$(this).parents('.tp-wrapper:eq(0)').find('a.selected').removeClass('selected');
		$(this).addClass('selected');
		
		sh = pe.find('.tp-hour a.selected');
		sm = pe.find('.tp-minutes a.selected');
		sa = pe.find('.tp-ampm a.selected');
		
		if(!pe.hasClass('focused')){

			pe.addClass('focused');
		}
		
		if(sh.length > 0 && sm.length > 0 && sa.length > 0){
			tpv = sh.data('val')+':'+sm.data('val')+' '+sa.data('val');	
			$('#'+be).val(tpv).removeClass('focused');
			//[0].focus();
			$('#'+be+'_time').css({display:'none'});
			cb = $('#'+be).data('changed');	
			if(cb) {
				if(window[cb]){
					window[cb](tpv);
				}
				else if (cura.action[cb]){
					cura.execute(cb,$('#'+be)[0], tpv);
				}
			}
			
			setTimeout("pe.removeClass('focused'); ",250);
			
		}
		
	});
	
	tv = $(jele).val();
	//console.log(tv);
	if(tv){
		// populate time
		tv = tv.split(/[: ]/);
		$('#'+uid+' .tp-hour a').each(function(){
			if($(this).data('val') == tv[0]){
				$(this).addClass('selected');	
			}
		});	
		$('#'+uid+' .tp-minutes a').each(function(){
			if($(this).data('val') == tv[1]){
				$(this).addClass('selected');	
			}
		});
		$('#'+uid+' .tp-ampm a').each(function(){
			if($(this).data('val') == tv[2]){
				$(this).addClass('selected');	
			}
		});
	}
	
	$(jele).click(function(){
		$('#'+this.id+'_time').css({display:'block'});	
	});
	
	$(jele).blur(function(){
		setTimeout( wrap(this.id, function(e, tid){
			
			var ele = $('#'+tid);
			
			if(!$('#'+tid+'_time').hasClass('focused')){
				$('#'+tid+'_time').css({display:'none'});
				var cb = ele.data('close');
	
				if(cb) {
					if(window[cb]){
						window[cb](ele.val());
					}
					else if (cura.action[cb]){
						cura.execute(cb,ele, ele.val());
					}
				}
			}		
		}), 200);
	});
}


var manualbuffer = function  (c_id, b_id, t, key) {
	
	this.c = c_id;
	this.b = b_id;
	this.key = key;
	this.t = t;
	this.page = 1;
	this.mid;
	this.prg = false;
	
	$('#'+this.b).click(wrap (this, function(e, o){
		
		if(o.prg){
			return;	
		}
		o.prg = true;
		
		if($('#'+o.b).data('theme') == 'orange'){
			$('#'+o.b).addClass('button-loading-orange').html('&nbsp;');	
		}
		else {
			$('#'+o.b).addClass('button-loading-blue').html('&nbsp;');	
		}
		
		o.page++;
		bc = new  CURACall('/sd/bff','key='+encodeURIComponent(o.key)+'&page='+o.page, null, true, "Loading..");
		bc.success = wrap(o, function (res, o) {
			$('#'+o.c).append(res.html);
			cura.bind();

			if(res.data.loaded >= o.t){
				$('#'+o.b).css({display: 'none'});
			}
			else {
				$('#'+o.b).removeClass('loading button-loading-blue button-loading-orange').html($('#'+o.b).data('label'));
			}
			
			o.page = res.data.page;
			if(o.mid){
				notificationManager.remove(o.mid, true);
			}
			o.prg = false;
			
			clearNotify();
		});
		bc.post();
		o.mid = bc.msgid;
	}));
		
	
}

/******************** Context Menu ********************/

function hideContextMenu () {
	$(document.body).off('click', hideContextMenu);
	$('#contextmenu').css({display:'none'});
}

function hideDropdown () {
	$(document.body).off('click', hideDropdown);
	$('#dropdown').css({top:'150%'});
	$('body').removeClass('noscroll');
	
}


var Dialog = function (opt) {
	
	this.id = Math.round(Math.random()*100000);
	this.title = (opt && opt.title) ? opt.title : null;
	this.message = (opt && opt.message) ? opt.message : '&nbsp;';
	this.mask = (opt && opt.mask) ? true : false;
	this.width = (opt && opt.width && opt.width > 360) ? opt.width : null;
	this.height = (opt && opt.height) ? opt.height : 'auto';
	this.data = (opt && opt.data) ? opt.data : {};
	this.keep_mask = (opt && opt.keep_mask) ? opt.keep_mask : 0;
	
	this.close = function(){
	//	console.log(this);
		if(this.mask && !this.keep_mask && $('.dialog').length == 1){
			$('#mask').fadeOut('fast');
			//console.log( 'closing amsk');
		}
		
		$('#d'+this.id).removeClass('dialog-show').addClass('dialog-hide');
		setTimeout("$('#d"+this.id+"').remove()",375);
		//$('#d'+this.id).remove();
	}
	if(opt.buttons === false) {
		this.buttons = {};
	}
	else {
	
		this.buttons = (opt && opt.buttons) ? opt.buttons : {'ok' : {label : 'Ok', 'class' : '', callback :null}};
	}
	
	
	// render
	if(this.mask){
		if($('#mask').length == 0){
			$(document.body).append('<div id="mask" class="mask"></div>');
			
		}
		$('#mask').fadeIn('fast');
	}
	
	dc = '<div class="dialog" id="d'+this.id+'" '+(this.width ? 'style="style="min-width:'+this.width+'px"' : '')+'>';
	
	if(this.title) {
		dc += '<div class="dialog-header"><div class="dialog-title">'+this.title+'</div><a href="javascript:void(0)" class="dialog-close" >X</a></div>';
	}
	
    dc += '<div class="dialog-body '+((!this.title) ? 'borderless' : '')+'">'+this.message+'</div>';
    dc += '<div class="dialog-buttons">';
	
	if(this.buttons['delete']){
		dc += '<div  class="dilog-buttons-left"><a  href="javascript:void(0)" class="button-trash" id="d'+this.id+'_delete" data-value="delete" >'+this.buttons['delete'].label+'</a></div>';
	}
	
	//dc += '<div class="dilog-buttons-center"></div>';
	
	//dc += '<div class="dilog-buttons-right">';
	
	for(b in this.buttons){
		
		if(b == 'delete'){
			continue;	
		}
		
		dc += '<a class="button '+(this.buttons[b].class ? this.buttons[b].class : '')+'" data-value="'+b+'" id="d'+this.id+'_'+b+'">'+this.buttons[b].label+'</a>';
	}
	//dc += '</div>';
	dc += '</div></div>';
	
	$(document.body).append(dc);
	
	$('#d'+this.id+' a.dialog-close').click(closeDialog);
	//$('#d'+this.id).draggable({handle:'.dialog-title'});
	
	
	for(b in this.buttons){
		$('#d'+this.id+'_'+b).click(this, function(e){
			dv = $(this).data('value');
			cb = e.data.buttons[dv].callback;
			act = e.data.buttons[dv].action;
			if(act){
				cura.execute(act, this, e.data.data);
			}
			else if(cb) {
				cb(dv, e.data.data, e.data);
			}
			else {
				closeDialog();	
			}
		});
	}
	
	this.position = function () {
		
		$('#d'+this.id).css({height:''});
		
		ww = $(window).width();
		wh = $(window).height();
		dw = (this.width) ? this.width : $('#d'+this.id).width();
		dh = $('#d'+this.id).height();
		
		if(dw > ww){
			dw = ww;	
		}
		
		dt = Math.round((wh-dh)/2);
		dt = dt > 50 ? dt -50 : dt;
		
		if(dh > wh){
			dh = wh;
			$('#d'+this.id+' .dialog-body').css({overflow:'auto'});	
			$('#d'+this.id).css({top: dt, left:Math.round((ww-dw)/2), width:dw, height:dh });
		}
		else{
				$('#d'+this.id).css({top: dt, left:Math.round((ww-dw)/2), width:dw});
		}

	}
	
	$(window).resize(this, function(e){
		e.data.position();
	}).keyup(closeDilaogListner);
	

	this.position();
	DialogManager.add(this);
	
	cura.bind();
	
	$('#d'+this.id).addClass('dialog-show');
}


var DialogManager = { dialogs : []};

DialogManager.add = function (dlg) {
	DialogManager.dialogs.push(dlg);
}

DialogManager.get = function () {
	
	if(DialogManager.dialogs.length > 0){
		return DialogManager.dialogs.pop();	
	}
	
	return null;
}
DialogManager.getAll = function () {
	return 	DialogManager.dialogs;
}

DialogManager.close = function () {
	
	if(DialogManager.dialogs.length > 0){
		dlg = DialogManager.dialogs.pop();
		dlg.close();	
	}
}

DialogManager.closeAll = function () {
	
	if(DialogManager.dialogs.length > 0){
		for(var i = 0; i < DialogManager.dialogs.length; i++) {
			dlg = DialogManager.dialogs.pop();
			dlg.close();
		}
	}
}

function closeDialog () {
	$(window).off('keyup',closeDilaogListner);
	DialogManager.close();
}

function closeDilaogListner (e) {

	if(e.which == 27){
		closeDialog();
	}
}

/************* Image upload *************/

cura.action.update_profile_picture = {
	
	callback: function(){
		$('#image_browse').off('change').on('change', function(){
		
			img = $('#image_browse')[0].files[0];
			
			if(img.type.indexOf('image/') < 0){
				console.log('Unsupported file '+ufile.name);
				return;	
			}
			
			var reader = new FileReader();
			reader.onload = function (event) {
				var img = new Image();
				img.onload = function() { showCropTool (this, true) };
				img.src = event.target.result;
			}
			reader.readAsDataURL(img);	
			
		});
		$('#image_browse').click();	
	}
}

function uploadProfileImage () {
	
	cd = $('#pi-canvas').data();
	ost = $('#pi-canvas').position();
	
	$('#image-form').remove();
	
	dt = $('#profile_changer').data();
	
	frm = '<form id="image-form">';
	frm += '<input type="hidden" name="scale" value="'+cd.zoom+'">';
	frm += '<input type="hidden" name="left" value="'+ost.left+'">';
	frm += '<input type="hidden" name="top" value="'+ost.top+'">';
	frm += '<input type="hidden" name="mark" value="200">';
	
	if(cura.gallery.data.image_id) {
		frm += '<input type="hidden" name="image_id" value="'+cura.gallery.data.image_id+'">';
	}
	else {
	
		frm += '<input type="hidden" name="image" value="'+encodeURIComponent($('#pi-image').attr('src'))+'">';
	}
	
	for(var k in dt) {
		frm += '<input type="hidden" name="'+k+'" value="'+dt[k]+'">';
	}
	
	
	frm += '</form>';
	
	$(document.body).append(frm);
	
	closeDialog();
	
	fpnid = notify("Updating image...");
	block();
	
	$.ajax({
		url: base+"/cd/update_image",
	   type: "POST",
	   data:  new FormData($('#image-form')[0]),
	   contentType: false,
	   cache: false,
	   processData:false,
	   beforeSend : function(){},
	   success: function(res){
		   
		   unblock();
		   
		   if(res){
			   
			   if(res.status){
				   
				   if(res.data.class.indexOf('customer') !== -1){
					   $('.user-info img').attr('src', res.data.thumb);
				   }				   
				  $('img.'+res.data.class).attr('src', res.data.i400);

				   $('#image-form').remove();				   
				}
			  
			  	notify(res.message, res.status);
			
		   }
		},
		error: function(e) {
			unblock();
			notify("Error updating image.", false);	
		}          
    });
}



function zoomPi ( val){
	
	cz = parseFloat($('#pi-canvas').data('zoom'));
	
	if(val > 0 && cz < 2){
		cz += 0.1;		
	}
	else {
		cz -= 0.1;		
	}
	
	nx = Math.round(parseFloat($('#pi-canvas').data('width'))*cz);
	//console.log(nx);
	if(nx >= 200) {
	
		$('#pi-image').width(nx);
		$('#pi-canvas').data('zoom', cz);
	}
}

/*****************************************/
//image crop tool
function showCropTool (img, is_profile, profile_mask){
	
	txt = '<div class="pi-crop"><div id="pi-canvas" data-zoom="1"><div id="pi-front"></div></div>';
	
	if(is_profile || profile_mask) {
		txt += '<div id="pi-profile"></div>';
	}
	else{
		txt += '<div id="pi-activement"></div>';
	}
	txt += '<div class="zoom-tools"><a class="zoom-in" onClick="zoomPi(1)"></a><a class="zoom-out" onClick="zoomPi(-1)"></a></div></div>';
	
	new Dialog( {
			//title: 'Update Profile Image',
			message : txt,
			mask : true,
			buttons: {
					ok : {label : 'Done', 'class' : '', callback: is_profile ? uploadProfileImage : uploadCuraImage},
					cencel : {label : 'Cancel', 'class' : 'button-alt'}
				}
				
		});
		
		if(is_profile || profile_mask) {
		$('#pi-canvas').unbind().draggable({
				handle:'#pi-front',
				stop : function (e, ui) {
					
					np = {};										
					// reposition of out of bounds
					pos = ui.position;
					cw = $('#pi-canvas').width();
					ch = $('#pi-canvas').height();

					if(pos.left > 100){
						np.left = 100;
					}
					
					if(pos.top > 100){
						np.top = 100;	
					}
					
					if(pos.left + cw < 300){
						np.left = (300 - cw);	
					}
					
					if(pos.top + ch < 300){
						np.top = 300 - ch;	
					}
					
					if((np.left || np.top)){
						$('#pi-canvas').animate(np,300);
					}
				}
			});
		}
		else {
			$('#pi-canvas').unbind().draggable({
				handle:'#pi-front',
				stop : function (e, ui) {
					
					np = {};										
					// reposition of out of bounds
					pos = ui.position;
					cw = $('#pi-canvas').width();
					ch = $('#pi-canvas').height();

					if(pos.left > 35){
						np.left = 35;
					}
					
					if(pos.top > 142){
						np.top = 142;	
					}
					
					if(pos.left + cw < 365){
						np.left = (365 - cw);	
					}
					
					if(pos.top + ch < 257){
						np.top = 257 - ch;	
					}
					
					if((np.left || np.top)){
						$('#pi-canvas').animate(np,300);
					}
				}
			});
		}
		
		
		img.id = 'pi-image';
		$(img).appendTo('#pi-canvas');
				
		iw = $('#pi-image').width();
		ih = $('#pi-image').height();
		
		if(is_profile || profile_mask) {
			
			if(iw > ih){
				isc = Math.ceil((200/ih)*10)/10;
			}
			else {
				isc = Math.ceil((200/iw)*10)/10;
			}
		}
		else{
			isc = Math.ceil((330/iw)*10)/10;
		}
		
		nw = Math.round(iw*isc);
		$('#pi-canvas').data('zoom', isc).data('width', iw);
		
		nt = Math.floor((400 - Math.round(ih*isc))/2);
		nl = Math.floor((400 - nw)/2);
		
		$('#pi-image').width(nw);
		$('#pi-canvas').css({top:nt, left:nl});
		//console.log([nw, nl, nt])
}


/************* Pyament cards *************/
function isCardValid (field_id) {
	
	var val = $.trim($('#'+field_id).val());
	var card_length = null;
	
	$('#'+field_id).removeClass("card-visa card-master card-amex card-discover card-jcb card-dc");
	
	if($('#payment_card_num-error').length == 0){
		$('#'+field_id).after('<label id="'+field_id+'-error" class="error" for="payment_card_num"></label>');
	}
	else {
		$('#'+field_id+'-error').css({display:'none'}).html("");
	}
	
	if(!val.match(/^(?=.*\d)[\d ]+$/)){
		$('#'+field_id+'-error').css({display:'block'}).html("Invalid card number");
		return false;
	}
	
	if(val){
		
		if(val.length > 4){
			$('#'+field_id).val(val.replace(/(\d{4}(?!\s))/g, "$1 "));
		}
		
		if(val.match(/^4/)){
			$('#'+field_id).addClass("card-visa");
			card_length = [13,16,19];
			return true;
		}
		else if(val.match(/^(51|52|53|54|55|5018|5020|5038|5893|6304|6759|6761|6762|6763|2[2-7])/)){
			$('#'+field_id).addClass("card-master");
			card_length = [13,16,17,18,19];
			return true;
		}
		else if(val.match(/^(34|37)/)){
			$('#'+field_id).addClass("card-amex");
			card_length = [15];
			return true;
		}
		else if(val.match(/^(300|301|302|303|304|305|36|54)/)){
			$('#'+field_id).addClass("card-dc");
			//$('#'+field_id+'-error').css({display:'block'}).html("Sorry! Diners Club cards not supported");
			card_length = [14,16];
		}
		else if(val.match(/^(6011|622[1-9]|644|645|646|647|648|649|65)/)){
			$('#'+field_id).addClass("card-discover");
			//$('#'+field_id+'-error').css({display:'block'}).html("Sorry! Discover cards not supported");
			card_length = [16,19];
		}
		else if(val.match(/^(35[2-8])/)){
			$('#'+field_id).addClass("card-jcb");
		//	$('#'+field_id+'-error').css({display:'block'}).html("Sorry! JCB cards not supported");
			card_length = [14,16];
		}
		else{
			if(val.length > 3) {
				$('#'+field_id+'-error').css({display:'block'}).html("Card type not recognized ");
			}
			card_length = null;	
		}
		
	}
	
}

/************** Date formatting ***********/
function ToDate (dt){
	
	var cm = ['Jan', 'Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var cd = ['Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	
	return cd[dt.getDay()]+' '+dt.getDate()+' '+cm[dt.getMonth()]+' '+dt.getFullYear();
}

function strToDate (str) {
	
	var cm = ['Jan', 'Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	
	var dp = str.split(' ');
	mi = cm.indexOf(dp[2]);

	return new Date(dp[3], mi, dp[1]);
}




/*************** default actions ***********/
/* display alert */
cura.action.alert = {
	callback : function (data,e, res){
		new Dialog( {
				title: 'Alert',
				message : res.message,
				mask : true,
				buttons: {
						ok : {label : 'Ok'}
					}
			});	
	}	
}

cura.action.redirect = {
	callback : function (data, e, res){
		notify("Page is loading...");
		if(res && res.data) {
			window.location.href = base+res.data;
		}
		else if (data && data.url){
			window.location.href = base+'/'+data.url;
		}
	}
}

cura.action.redirect_to = {
	callback : function (dt){
		notify("Page is loading...");
		window.location.href = base+dt.url;	
	}
}
cura.action.reload = {
	callback : function (dt){
		window.location.href = window.location.href;	
	}
}

cura.action.dialog = {
	callback: function (data, e, res) {
		new Dialog(res.data);			
	}
}

cura.action.history = {
	callback: function (dt, e) {
		
		var steps = dt.steps ? parseInt(dt.steps) : 1;
		cura.history.go(steps);
	}
}

cura.action.recent = {
	callback: function (dt, e) {
		cura.history.go(0);
	}
}

cura.action.scroll = {
	callback: function (dt, e){
		if(e){
			setTimeout('$(window).scrollTop('+($(e).offset().top-70)+')', 100);
		}
	}
}

cura.action.login = {
	req: null,
	callback: function (dt, e, res, req) {
		cura.action.login.req = req;
		
		new Dialog({
				title:false,
				message: res.html,
				mask:true,
				buttons: []
			});
		
	}
}

cura.action.login_complete = {
	callback: function (dt, e , res) {
		closeDialog();
		if(cura.action.login.req){
			cura.action.login.req.post();
		}
		else if (res.url){
			window.location.href = base+res.url;	
		}
	}
}



// sample action
cura.action.sample = {
		submit : 'form_id',
		url : 'process.php',
		data: '', /** additional data to post into server **/
		validation: {},
		callback : function (data){},
		message : 'message to display',
		mask: true, // default true	
		confirm: "message to confirm"
};

cura.action.datepicker = {
	
	callback : function (dt, ele){
		
		$(ele).addClass('vanilla-calendar');
		
		if(dt.date){
			dp = dt.date.split('-');
			initdate = new Date(dp[0], dp[1]-1, dp[2]);	
			//console.log(initdate);		
		}
		else{
			initdate = null;	
		}
		
		var vc = new VanillaCalendar({
			selector: '#'+ele.id,
			pastDates: $(ele).hasClass('past'),
			//date:initdate,
			onSelect: function (dt, ele){
									
					cn = $(ele).parents('div.vanilla-calendar:eq(0)').data('change');
					
					cura.execute(cn, null,{date:dt.date} );
					
											
				}
			});
		
	}
}

cura.action.convert_time = {
	callback: function (d, e, p){
		
		var val = e.value;
		var pv = 0;
	//	console.log(val)
		if(val){
			val = val.toLowerCase();
			
			let parts = $.trim(val).split(' ');

			// convert to minutes
			for(let p = 0; p < parts.length; p++){
				
				let cv = 0;

				try {
					if(parts[p].indexOf('m') > 0){
						cv = parseFloat($.trim(parts[p].replace('m','')));
					}
					else if(parts[p].indexOf('h') > 0){
						cv = parseFloat($.trim(parts[p].replace('h','')))*60;
					}
					else if(parts[p].indexOf('d') > 0){
						cv = parseFloat($.trim(parts[p].replace('d','')))*1440;
					}
					else{
						cv = parseFloat(parts[p]);
						
						if(cv < 15){
							cv = cv *60;
						}
					}
				} catch (e) {}
				
				if(cv && cv > 0){
					pv += cv;
				}
				
			}
			
			// format
			if(pv > 0){
				let td = pv >= 1440 ? Math.floor(pv/1440) : 0;
				let th = pv >= 60 ? Math.floor((pv%1440)/60) : 0;
				let tm = Math.round((pv%1440)%60);
				val = ((td > 0)? td+'d ': '')+(th > 0 ? th+'h ': '')+(tm > 0 ? tm+'m' : '');			
			}
			
			e.value = val ? val : '';	
		}
			
	}
}

cura.action.tabs = {
	callback : function(dt, e) {
		
		e = $(e)
		p = e.parents('div.tabs:eq(0)');
		
		p.find('a').removeClass('active');
		e.addClass('active');
		
		$('#'+p.data('id')+' > div').css({display:'none'});
		$('#'+dt.id).css({display:'block'});
		
		if(dt.action){
			cura.execute(dt.action, e);	
		}
	}
}

cura.action.popup = {	
	callback : function (data, e, res){
		
		var option = {
			title : res.data.title ? res.data.title : null,
			message: res.html,
			data: res.data.para ? res.data.para : null,
			buttons : {},
			mask: res.data.mask ? 	res.data.mask : 0			
		}
		
		if(res.data.buttons){
			option.buttons	= res.data.buttons;
		}
		else if (res.data.buttons === false) {
			
		}
		else{
			option.buttons.cancel = {label : 'Cancel', class: 'button-alt'}	
		}
		
		new Dialog(option);
	}	
}

cura.action.close_dialog = {
	callback: function() {
		closeDialog();	
	}
}

cura.action.inline_edit = {
	callback: function (dt, e) {
		
		e = $(e)
		p = e.parents('div:eq(0)');
		
		p.addClass('_inew');
		
		var src = p.find('.inline-contents');
		src.find('br').replaceWith('~^~');
		
		var ch = src.height();		
		var st = src.text();
				
		if(st){
			st = st.replace('Add your notes','');
			st = st.replace(/\~\^\~/g,"\n");	
		}
		
		p.find('.inline-contents, .inline-editor').css({display:'none'});
		
		txt = '<div class="inline-editing">';
		txt += '  <textarea class="inline-text" style="height:'+(ch+30)+'px">'+st+'</textarea>';
		txt += '  <div class="button-container"> <a class="button" cura="inline_confirm" >Update</a>';
		txt += '    <div class="gap"></div>';
		txt += '    <a class="button button-alt" cura="inline_cancel" >Cancel</a>';
		txt += '    <div class="center"> </div>';
		txt += '  </div>';
		txt += '</div>';
		
		p.append(txt);
		
		src.html(st.replace(/\n/g, '<br/>'));
		
		cura.bind();
		
	}
}

cura.action.inline_confirm = {
	callback : function (dt, e){
		e = $(e);
		p = e.parents('div._inew:eq(0)');
		
		dt = p.find('.inline-editor').data();
		var val = p.find('.inline-editing textarea').val();
		if(val){
			val = val.replace(/\n/g, '<br/>');	
		}
		p.find('.inline-contents').html(val);
		p.find('.inline-contents, .inline-editor').css({display:'block'});
		p.find('.inline-editing').remove();
		
		p.find('.inline-editor').data('value',val);
		cura.execute(dt.action, p.find('.inline-editor'));
	}
}

cura.action.inline_cancel = {
	callback : function (dt, e){
		e = $(e);
		p = e.parents('div._inew:eq(0)');
		p.find('.inline-contents, .inline-editor').css({display:'block'});
		p.find('.inline-editing').remove();
	}
}

cura.action.refresh = {
	callback : function(){
		window.location.href = window.location.href;	
	}
}

cura.action.set_value = {
	callback : function (dt){		
		if(dt.id ){
			$('#'	+dt.id).val(dt.value);
		}
			
	}
}

cura.action.device = {
	url:'/sd/device',
	callback: function (dt, e) {
		client = new ClientJS();
		dt.device = getDevice();
		
	},
	mask:false,
	message:false
}


cura.action.favorite = {
	url:'/sd/like',
	message: false,
	mask:false,
	callback: function (dt, e){
		$('#fav_'+dt.id).addClass('pulse');	
	}
}

cura.action.favorite_update = {
	callback : function (dt, e, res) {
		if(res && res.status){
			
			if(res.data.action) {
				$('#fav_'+res.data.id).addClass('added').removeClass('pulse');	
				
				$(document.body).append('<span class="favorite added" id="fv_'+res.data.id+'"></span>');
				ele = $('#fv_'+res.data.id);
				toff = $('#fav_'+res.data.id).offset();
				ele.css({left:toff.left, top:toff.top});
				ele.addClass('popheart');
				setTimeout("$('#fv_"+res.data.id+"').remove()", 500);
				
			}
			else{
				$('#fav_'+res.data.id).removeClass('added pulse');	
			}
		}
	}
}


cura.action.otp_timer = {
	callback: function (){
		x_otp_counter = 120;
		otpCountDown();
		
		$('#vcode').keyup(function() {
			$('#_server_error').html('');
	
			if(this.value.length == 6){
				cura.execute('validate_otp');
			}
		})[0].focus();
	}
}

cura.action.validate_login = {
	submit:'login_form',
	url: '/sd/login_validate',
	callback: function (data){
					$('#login_country_select').css({display:'none'});
				},
	validate:{rules:{username: {required:true}}, messages:{username: {required:'Please enter email address or mobile number'}}},	
	message: 'Looking up your account'
}

cura.action.change_login_country = {
	callback: function () {
		$('#login_country_select').css({display:'block'});
		$('#_server_error').html('');
	}
}


cura.action.resend_otp = {
	submit: 'otp_form',
	url:'/sd/resend_otp',
	message: 'Re-sending code...'	
}
cura.action.publish_page = {
	url: '/cd/page_publish',
	callback: function(dt, e) {
		//console.log(dt);
		if(!dt.ref || dt.ref == '0'){
			cura.action.publish_page.confirm = null;
			//console.log('Confirmation removed');
		}
		
	},
	confirm:'This is the first time you are going to publish your page.<br><br>Have you updated all your page content?',
	confirm_button:'Yes, Publish',
	message : 'Publishing your page'
}

cura.action.greet = {
	callback : function(dt, e){
		var d = new Date();
		if(d.getHours() >= 12) {
			$(e).html("Good Evening!");
		}
		else{
			$(e).html("Good Morning!");
		}
	}
}

cura.action.reveal = {
	callback : function (dt, e){
		if(dt.id){
			$('#'+dt.id).slideDown('fast');	
		}
		
		if(dt.class){
			$('.'+dt.id).slideDown('fast');	
		}
	}
}

cura.action.validate_otp = {
	
	submit: 'otp_form',
	url:'/sd/validate_otp',
	validate:{rules:{vcode: {required:true, minlength:5}}, messages:{vcode: {required:"Please enter one time pass code", minlength:'Passcode is too short'}}},
	callback: function (dt){ 
		if(x_otp_progress){
			return false;
		}
		else{
			 x_otp_progress = true;	
			setTimeout("x_otp_progress = false",2000);
			
		}
		dt.device = getDevice();
	},
	message: 'Validating code...'	
}

cura.action.validate_otp_end = {
	callback : function () {x_otp_progress = false}	
}

cura.action.context = {
	callback: function (dt, e, v){
		
		var comp = {data:dt, element:e}
		
		$('#contextmenu').remove();
		
		var menu = '<div id="contextmenu">';
		
		if(dt.action && cura.action[dt.action]){
			
			var attrs = '';
			for(var a in dt){
				if(a == 'cura' || a == 'action'){
					continue;	
				}
				attrs += 'data-'+a+'="'+dt[a]+'" ';	
			}
			
			moptions = (typeof cura.action[dt.action].menu == 'function') ? cura.action[dt.action].menu(dt) : cura.action[dt.action].menu;
			
			for(var k in moptions){
				menu += '<a cura="'+dt.action+'" data-action="'+k+'" '+attrs+' >'+moptions[k]+'</a>';	
			}
			 
		}
		menu += '</div>';
		
		$(document.body).append(menu);
		
		ww = $(window).width();
		
		if(ww > 600) {
		
			os  = $(e).offset();
			ew = $(e).width();
			eh = $(e).height();


			cp = v.originalEvent.pageX;
			mw = $('#contextmenu').width();
			tp = os.left;

			if(mw < ew){

				if(os.left+ew - cp-15 >= mw){
					tp = cp-15;	
				}
				else{
					tp = ew	- mw + os.left;
				}			
			}

			if(ww < tp+mw){
				tp = ww - mw - 10;	
			}

			tl = os.top+eh;

			if(tl > v.originalEvent.pageY +15){
				tl = v.originalEvent.pageY +15;
			}
			//console.log(v.originalEvent);

			$('#contextmenu').css({top:tl, left:tp, position:'absolute'});
		
		}
		else{
			$('#contextmenu').css({top:'auto', left:0, right:0, bottom:0, position:'fixed'});
		}
		
		
		setTimeout("$(document.body).on('click', hideContextMenu)",200);
		cura.bind();
	}
}

cura.action.expand_text = {
	callback:function(dt, e){		

		if($(e).hasClass('expanded')){
			$('#'+dt.id).addClass('hidden');
			$(e).html('view more...').removeClass('expanded');
		}
		else{
			$('#'+dt.id).removeClass('hidden');
			$(e).html('view less...').addClass('expanded')
		}
		
		
		
	}
}


cura.action.currency_select = {
	url:'/sd/currency_select'
}

cura.action.currency_update = {
	url:'/sd/currency_update',
	submit:'currency_update',
	messahe:'Updating...'
}


cura.action.activate_menu = {
	callback: function() {
		if($('.dash-menu-wrapper').css('display') == 'none'){
			$('.dash-menu-wrapper').slideDown();
			
			$(document.body).off('click',closeMainMenu).on('click',closeMainMenu)		
		}
		else{
			$('.dash-menu-wrapper').slideUp();
		}
	}
}

cura.action.gdpr_accept = {
	url:'/sd/gdpr_accept',
	callback: function(){
		$('#gdpr').removeClass('gdpr_show');
	},
	message:'Thank you!'
}



function closeMainMenu () {
	$(document.body).off('click',closeMainMenu)
	$('.dash-menu-wrapper').slideUp();	
}


/******************* OTP ************/
var x_otp_counter;
var x_otp_progress = false;


function otpCountDown () {
	
	if(x_otp_counter <= 0){
		
		document.getElementById('otp_resend').innerHTML = '<a href="javascript:void(0)" cura="resend_otp" >Re-send one time passcode</a>';
		cura.bind();
	}
	else{
		otp_delay_m = Math.trunc(x_otp_counter/60);
		otp_delay_s = x_otp_counter%60;
		
		document.getElementById('otp_delay').innerHTML = otp_delay_m+':'+(otp_delay_s < 10 ? '0' : '')+otp_delay_s;
		
		setTimeout('otpCountDown()',1000);
	}
	
	x_otp_counter--;
}

function hideUsermenu () {
	$(document.body).off('click',hideUsermenu);
	$('.user-menu').fadeOut('fast');
}

function hideToolTip () {
	$('.tooltip').removeClass('active');
	$(document.body).off('click',hideToolTip);	
}
/******************* Push Notifications *************************/

function push_permission () {
	
	pushMessaging.getToken()
	.then(function(token){
		//console.log(token);
		cura.action.push_token.data.token = token;
		cura.execute('push_token',null);
		$('#push_enable_'+cura.dfp).html('<span class="txt-green">Enabling...</span>');	
	})
	.catch(function(err){
		//console.log(err);	
		new Dialog({
				title:'Push Notifications',
				message: "Push notification disabled or <br>device does not support this service.",
				mask:true,
				buttons: {
						cancel : {label : 'Done'}
					}
			});
	});
	
	notify("Enabling...");	
}

cura.action.push_token = {
	url:'/cd/pushtoken_update',
	data: {token:null}	
}


function onPushMessage (payload) {
	
	if(payload && payload.data){
		
		const type = payload.data.type;
	
		if(type == 'checkin'){
			$('#push_enable_'+payload.data.signature).html('<span class="txt-green">Enabled</span>');
			new Dialog({
				title:'Push Notifications',
				message: "Push notifications successfully enabled on this device ",
				mask:true,
				buttons: {
						cancel : {label : 'Done'}
					}
			});
			
			$('#push_enable_'+cura.dfp).html('<span class="txt-green">Enabled</span>');	
		}
		else{
			/** add notification */
			if(type == 'newbooking' || type == 'bookingaccepted' || type == 'bookingconfirmed' || type == 'bookingrejected' || type == 'bookingchanged'){
				
				addToNotification ('b'+payload.data.id, 
									payload.data.message, 
									payload.data.image, 
									'just now', 
									'/dashboard/booking/'+payload.data.id
									);
				
				showQucikNote (
					payload.data.message,
					payload.data.image,
					'/dashboard/booking/'+payload.data.id 
				)
			}
			else if(type == 'chat'){
				
				if(!_chatrooms.rooms[payload.data.id]){
				
					addToNotification ('c'+payload.data.id, 
										payload.data.message, 
										payload.data.image, 
										'just now', 
										null,
										{cura:'chat_open', board: payload.data.id }
										);
					
					showQucikNote (
						payload.data.message,
						payload.data.image,
						null,
						{cura:'chat_open', board: payload.data.id }
					)
					
					if($('#booking_msg_board').length > 0 && $('#booking_msg_board').data('board') == payload.data.id){
					
					cura.execute('message_refresh');
				}
				
				}
				else if (!SSE.status){
					SSE_connect();	
				}
				
			}
			
		}
		
	}
	console.log('Message received');
	//console.log(payload);
}

function showNotificationPane (e){
	
	e.stopPropagation();
	$('#notify label').remove();
	cura.ncount = 0;
	
	var pane = $('#notify_pane');
	
	if(pane.hasClass('open')){
		hideNotificationPane(e);
		return;	
	}
	
	pane.css({display:'block'});
	
	var os = $('#header').offset();
	var ow = $('#header').width();
	var pw = pane.width();
	var ph = pane.height();
	pane.css({top:(40 - ph), left:(os.left + ow - pw), 'max-height': ($(window).height()-80)}).animate({top:50},200);
	$(document.body).on('click',hideNotificationPane);
	pane.addClass('open');
	
	cura.util.call('/cd/notify_ack',"nid="+SSE.nid).post();
	hideQuickNote();
}

function hideNotificationPane (e){
		
	$(document.body).off('click',hideNotificationPane);
	
	if(!$('#notify_pane').hasClass('open')){
		return;	
	}
	
	var pane = $('#notify_pane');
	var ph = pane.height();
	pane.animate({top:40 - ph},200);
	pane.removeClass('open');
	
}

function showQucikNote (msg, img, url, data){
	
	if($('#notify_pane').hasClass('open')){
		return;
	}
	
	if(!document.getElementById('quicknote')){
		$(document.body).append('<div id="quicknote"></div>');
	}
	
	var qn = $('#quicknote');
	
	qn.html(notificationRow('qna',msg, img, null, url, data));
	
	var os = $('#header').offset();
	var ow = $('#header').width();
	var pw = qn.width();
	var ph = qn.height();
	qn.css({top:-200, left:(os.left + ow - pw)}).animate({top:55},200);
	$(qn).on('click',hideQuickNote);
	
	setTimeout('hideQuickNote()',10000);
	cura.bind();
	
	if(isWindowHidden()) {
		$('#chat_sound')[0].play();
	}
	
}

function hideQuickNote () {
	$('#quicknote').animate({top:-200},200);
}

function addToNotification (id, msg, img, dt, url, data, seen){
	
	if($('#'+id).length > 0){
		$('#'+id).remove();
	}
	
	if(cura.ncount < 99 && (!seen || seen == '0')) {
		cura.ncount++;
	}
	
	
	$('#notify_pane').prepend(notificationRow(id,msg, img, dt, url, data));
	
	
	
	if(cura.ncount > 0) {
		$('#notify').html('<label>'+cura.ncount+'</label>');;
	}
	
	if($('#notify_pane').hasClass('open')){
		
	}
}


function notificationRow (id, msg, img, dt, url, data){
	
	var txt = '<a id="'+id+'" '+(url ? 'href="'+base+url+'"' : '')+' ';
	
	if(data){
		for(var k in data){
			
			if(k == 'cura'){
				txt += k+'="'+data[k]+'" ';	
			}
			else{
				txt += 'data-'+k+'="'+data[k]+'" ';	
			}
		}
	}
	
	txt += ' >';
	txt += '<img src="'+(img ? img : base+'/images/bell.jpg')+'">';
	txt += '<span>'+msg
	if(dt) {
		txt += '<label>'+dt+'</label>';
	}
	txt += '</span></a>';
	
	return txt;
}

/**************************************************************/

var SSE = { status:0, source:null, last:0, elapse:0, nid:0, count:0, live:false, time:0, connect:true,  con:null};

SSE_init = function () {
	
	if(typeof(EventSource) !== "undefined") {
		SSE.elapse = 0;
		SSE.con = setInterval(SSE_track, 1000);
		setTimeout('SSE_connect()', 1000);
	}
}

SSE_connect = function () {

	SSE.elapse = 0;
	SSE.status = 1;
	SSE.count = 0;
	/*
	if(Object.keys(_chatrooms.rooms).length > 0){
		
		cp = '';
		for( k in _chatrooms.rooms){
			
			if(cp < _chatrooms.rooms[k].last_id){
				cp = _chatrooms.rooms[k].last_id;
			}
		}
	}
	
	url = base+"/live?"+(cp ? 'cr='+cp+'&' : '')+( SSE.nid ? 'nid='+SSE.nid+'&' : '');
	*/
	url = base+"/live/"+SSE.time;
	
	SSE.source = new EventSource(url);
	SSE.source.addEventListener('message', SSE_handle);
	console.log('connected');
}

SSE_disconnect = function () {
	SSE.source.close();
	SSE.status = 0;
	console.log('disconnected');
	SSE.live = true;		
}


SSE_track = function () {
	if(!SSE.connect){
		return;
	}
	
	SSE.last++;
	SSE.elapse++;

	if(SSE.status && (Object.keys(_chatrooms.rooms).length == 0 || SSE.count == 0 ) && SSE.last >= 5){
	//	console.log(['elapse:',SSE.elapse,'last:', SSE.last]);
		SSE_disconnect();
		SSE.elapse = 0;
		SSE.last = 0;
	}
	else if(SSE.status && Object.keys(_chatrooms.rooms).length > 0 && SSE.count > 0 && SSE.last >= 30){
	//	console.log(['elapse:',SSE.elapse,'last:', SSE.last]);
		SSE_disconnect();
		SSE.elapse = 0;
		SSE.last = 0;
	}
	else if(!SSE.status && SSE.elapse >= 5){
	//	console.log(['elapse:',SSE.elapse,'last:', SSE.last]);
		SSE_connect();
		
	}
	
//	console.log(['elapse:',SSE.elapse,'last:', SSE.last]);
}


SSE_handle = function (e) {
		
	if(e.data){
		SSE.count++;
		var md = JSON.parse(e.data);
		console.log(md);
		if(SSE.nid < parseInt(md.nid)){
			SSE.nid = parseInt(md.nid);	
		}
		
		if(md.terminate){
			//SSE.connect = false;
			SSE_disconnect();
			clearInterval(SSE.con);
			console.log('User not logged in');
			return;
		}
		
		if(md.time){
			SSE.time = md.time;
			
			if(!md.type){
				return;	
			}
		}
		
		if(md.type == 'b') {
			
			addToNotification (md.ref, 
								md.message, 
								md.image, 
								md.date, 
								'/dashboard/booking/'+md.id,
								null,
								md.seen
									);
			if(!md.seen || md.seen == "0") {
				showQucikNote (
						md.message,
						md.image, 
						'/dashboard/booking/'+md.id					
					)
			}
		}
		else if(md.type == 'c') {

			if(!_chatrooms.rooms[md.id]){
				
				addToNotification (md.ref,
									md.message, 
									md.image, 
									md.date,  
									null,
									{cura:'chat_open', board: md.id },
									md.seen
									);
				if(!md.seen || md.seen == "0" && SSE.live) {
					showQucikNote (
						md.message,
						md.image, 
						null,
						{cura:'chat_open', board: md.id }
					)
				}
				
				if($('#booking_msg_board').length > 0 && $('#booking_msg_board').data('board') == md.id){
					console.log('exe');
					cura.execute('message_refresh');
				}
			
			}
			else {
				
				
			}
			
		}
		else if(md.type == 'cd') {
			if($('#msg_'+md.id).length == 0) {
				showMessages(md.b, [md], md.self, 0);
			}
		}
		
		
	}
	
	
}
/***************************************************************/
var animcontaiers = [];
function initAnimations () {
	
	$('.fade, .slide, .slideleft, .slideright').addClass('transit');
	
	if($('.fade, .slide, .slideleft, .slideright').length >  0){
	
		
		$('.fade, .slide, .slideleft, .slideright').each(function() {
			
			let observer = new IntersectionObserver(entries =>{
		
				entries.forEach(entry => {
					
					let diff = (100/entry.boundingClientRect.height)
					console.log(diff);
					if(entry.intersectionRatio >= 0.1){
						
						if(entry.target.classList.contains('slide')){
							entry.target.classList.replace('slide','slideIn');
						}
						if(entry.target.classList.contains('fade')){
							entry.target.classList.replace('fade','fadeIn');
						}
						if(entry.target.classList.contains('slideleft')){
							entry.target.classList.replace('slideleft','slideleftIn');
						}
						if(entry.target.classList.contains('slideright')){
							entry.target.classList.replace('slideright','sliderightIn');
						}
						
					}

					//$(entry).removeClass('fade slide slideleft slideright');
				});

			},{threshold:0.1});
			
			observer.observe(this);
			
		});
		
		
	}
	
	//animateElements();
}

function animateElements () {
	let targets = document.querySelector('.fading, .slide, .slideleft, .slideright');
	
	console.log(targets);
	
	const observer = new IntersectionObserver(entries =>{
		
		entries.forEach(entry => {
			console.log(entry);
			entry.target.classList.remove('fading');
			entry.target.classList.remove('slide');
			entry.target.classList.remove('slideleft');
			entry.target.classList.remove('slideright');
			
			//$(entry).removeClass('fade slide slideleft slideright');
		});
		
	},{threshold:0.25}); 
	
	observer.observe(targets);
}

function shareOnFB (url){
	
	  FB.ui({
		display: 'popup',
		method: 'share',
		href: base+'/'+(url ? url : ''),
	  }, function(response){
		  window.location.href = base+'/dashboard';
	  });
	
}

cura.util.randomID  = function () {
	var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

/****************************************************************/
	
// initialize framework
$(document).ready(function(e) {
    cura.bind();
	cura.tz = (new Date().getTimezoneOffset())*-1;	
	
	if($('.miximized').length > 0) {
		$('.page-container, .space_block').css({'min-height': $(window).height()});
	}
	else {
		$('.page-container, .space_block').css({'min-height': $(window).height()-150});
	}
	
	$('.user-info').click(function(e){ e.stopPropagation(); $('.user-menu').css({display:'block'}); $(document.body).on('click',hideUsermenu); });
	
	$('#notify').click( showNotificationPane );
	
	/* subscribe for SSE */
	SSE_init();
	
	// initialize animations
	initAnimations();
	
	// GDPR popup
	let gdprc = getCookie('GDPR');
	if(!gdprc || gdprc == ''){
		$('#gdpr').addClass('gdpr_show');
	}
	
	// correctly set base url on production
	if(window.location.href.indexOf('https') >= 0) {
		let uparts = window.location.href.split('/');
		base = "https://"+uparts[2];
	}
	
	
		
	
});

function palceInit () {
	cura.attach.citypicker();
	console.log('initilizing address pick');
}

/************ Page visibility check *************/

function isWindowHidden () {
	
	if (typeof document.hidden !== "undefined") { 
		// Opera 12.10 and Firefox 18 and later support
	  	return  document.hidden;
	} else if (typeof document.msHidden !== "undefined") {
		return document.msHidden;
	} else if (typeof document.webkitHidden !== "undefined") {
	  	return document.webkitHidden;
	}
	
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function decodeJWT (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};