
var ct_editing = null;
var ct_uploading = null;

cura.action.page_edit = {
	callback : function (dt, e, ev) {
		
		ct_editing = e;		
		$('#inline-editing').remove();
		
		if(dt.field != 'image'){
			
			
			var fw = $(e).width();
			var fh = $(e).height();
			
			if(fw < 300){
				fw = 300;	
			}
			else{
				fw += 15;	
			}
			
			var txt = '<div id="inline-editing" class="inline-editing" style="width:'+fw+'px;">';
			txt += '  <textarea id="inline-text" class="inline-text" style="height:'+((dt.field == 'title') ? 50 : (fh > 150 ? fh : 150))+'px;">'+$(e).html().replace(/<br\s*[\/]?>/gi,"\n")+'</textarea>';
			txt += '  <div class="button-container"><div class="center"> <a class="button" cura="page_update"  >Update</a>';
			txt += '    <div class="gap"></div>';
			txt += '    <a class="button button-alt" cura="inline_cancel" >Cancel</a>';
			txt += '     </div>';
			txt += '  </div>';
			txt += '</div>';
			
			$(document.body).append(txt);
			$('#inline-editing').offset($(e).offset());
			
			
			
			cura.bind();
			document.getElementById("inline-text").focus();
			
		}
		else{
			dt.action = 'browseImage';
			cura.action.context.callback(dt, e, ev);
			// browse image
			//$('#image_browse').off('change').on('change', _browserCuraImage);
			//$('#image_browse').click();
		}
		
	}	
}

cura.action.browse_banner = {
	callback: function(){
		$('#image_browse').off('change').on('change', _browserBannerImage);
		$('#image_browse').click();	
	}
}

cura.action.browseImage = {
	menu:{'browse':'Browse Gallery', 'upload' : 'Upload Image'}	,
	callback: function(dt, e){
		
		cura.gallery.data = {};
		
		
		if(dt.action == 'upload'){
			
			if (dt.ref && (dt.ref == 'talent' || dt.ref == 'profile' || dt.ref == 'customer')){
				$('#image_browse').off('change').on('change', browserProfileImage);
				$('#image_browse').click();		
			}
			else if(dt.ref && dt.ref == 'banner'){
				$('#image_browse').off('change').on('change', _browserBannerImage);
				$('#image_browse').click();	
			}
			else if(dt.crop){
				$('#image_browse').off('change').on('change',cropUploadImage);
				$('#image_browse').click();	
			}
			else{
				$('#image_browse').off('change').on('change',uploadCuraImage);
				$('#image_browse').click();	
			}
		}
		else{
			// open gallery
			cura.execute('open_gallery', e);
		}
		hideContextMenu();
		
	}
}


cura.action.inline_cancel = {
	callback : function () {
		$('#inline-editing').remove();
	}
}

cura.action.page_update = {
	url: '/cd/page_update',
	callback: function (dt, e) {
		
		dt.page_id = $('#public-page').data('id');
		dt.value = $('#inline-text').val();
		edt = $(ct_editing).data();
		for(k in edt){
			dt[k] = edt[k];	
		}
		
		if(!dt.value){
			dt.value = '';
		}
		
		$(ct_editing).html(dt.value.replace(/(?:\r\n|\r|\n)/g,'<br>'));
		$('#inline-editing').remove();
	},
	mask:false
}




cura.action.page_part_updated = {
	callback : function (dt, e , res){
		
		if(res && res.status && res.data && res.data.class){
			if($('.'+res.data.class).length > 0){
				$('.'+res.data.class).html(res.data.val);
			}
			else if (res.data.section == 'e'){
				$('.profile-video').html('<iframe frameborder="0" type="text/html" src="'+res.data.val+'" width="100%" height="100%" allowfullscreen allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"> </iframe>');
			}
		}
	}
}

cura.action.page_update_image = {
	callback: function (dt, e , res){
		console.log([dt, e,res]);
		
		cls = '.'+res.data.part.section+'_'+res.data.part.index+'_img';
		console.log(cls);
		if($(cls).length > 0) {
			$(cls).attr('src',res.data.img.url); 
		}
		else {		
			ct_uploading.find('img')[0].src = res.data.img.i400;	
		}
	}
}

function cropUploadImage () {
	
	img = $('#image_browse')[0].files[0];
	
	if(img.type.indexOf('image/') < 0){
		console.log('Unsupported file '+img.name);
		return;	
	}
	
	var reader = new FileReader();
	reader.onload = function (event) {
		var img = new Image();
		
		if($(ct_editing).data('section') == 'b'){
			img.onload = function () { showCropTool (this, false, true) };
		}
		else{
			img.onload = function () { showCropTool (this, false) };
		}
		
		img.src = event.target.result;
	}
	
	reader.readAsDataURL(img);
	
}


function uploadCuraImage (v, dt) {
	
	
	if(!cura.gallery.data.image_id) {
	
		img = $('#image_browse')[0].files[0];
			
		if(img && img.type.indexOf('image/') < 0){
			console.log('Unsupported file '+img.name);
			return;	
		}
			
		var data = new FormData(document.getElementById('image_form'));
	
	}
	else {
		var data = new FormData();
		data.append('image_id', cura.gallery.data.image_id);
	}

	data.append('page_id', $('#public-page').data('id'));
	
	ct_uploading = $(ct_editing);
	
	edt = ct_uploading.data();
	for(var k in edt){
		data.append(k,  edt[k]);	
	}
	
	if(v && v == 'ok'){
		// called after crop
		for(var k in dt){
			data.append(k,  dt[k]);	
		}
		
		cd = $('#pi-canvas').data();
		ost = $('#pi-canvas').position();
		
		data.append('scale', cd.zoom);
		data.append('left', ost.left);
		data.append('top', ost.top);
		
		closeDialog();
	}
	
	notify("Updating image...");
	console.log("calling");
	console.log(data);
	$.ajax({
		url: base+'/cd/page_update_image',
		data:data,
		type: "POST",
		contentType: false,
		processData:false,
		success: wrap({json:true} ,cura.handler.callComplete)		
	});
	
	
}

/******************** Profile image changer ***************/

function browserProfileImage (){
	console.log('browserProfileImage');
	img = $('#image_browse')[0].files[0];
	
	if(img.type.indexOf('image/') < 0){
		console.log('Unsupported file '+ufile.name);
		return;	
	}
	
	var reader = new FileReader();
	reader.onload = function (event) {
		var img = new Image();
		img.onload = function () { showCropTool(this, true)};
		img.src = event.target.result;
	}
	reader.readAsDataURL(img);
}

cura.action.page_part_add = {
	callback: function (dt, e){
		
		if(dt.section == 'b'){
			
			ln = $('.service-row').length > 0 ? $('.service-row:last-child').data('index')+1 : 0 ;
			
			$('.service-block').append('<div class="service-row" data-index="'+ln+'"  ><div class="service-image" cura="page_edit" data-section="b" data-field="image" data-index="'+ln+'" data-crop="1"><img class="b_'+ln+'_img" src="'+(base+'/images/static/service1.jpg')+'"></div><div class="service-remove "><a cura="page_part_remove" data-section="b"   title="Remove" ></a></div><div class="service-title" cura="page_edit" data-section="b" data-field="title" data-index="'+ln+'">My Service</div><div class="service-row-desc" cura="page_edit" data-section="b" data-field="body" data-index="'+ln+'">My Service description</div></div>');
			cura.bind();
		}
		else if(dt.section == 'd'){
			
			var is_odd = ($('.achievement').length%2 == 1);
			
			var index = $('.achievement').length > 0 ? $('.achievement:last-child').data('index')+1 : 0;
			
			var txt = '<div class="column-2"><div class="achievement" data-index="'+index+'"><a class="achievement-remove" cura="page_part_remove" data-section="d" data-index="'+index+'" ></a> <div class="award-image" cura="page_edit" data-section="d" data-field="image" data-index="'+index+'" data-crop="1"><img src="'+base+'/images/static/award'+(index%4)+'.jpg"></div><div class="award-title" cura="page_edit" data-section="d" data-index="'+index+'" data-field="title">My Biggest Achievement</div><div class="award-desc" cura="page_edit" data-section="d" data-index="'+index+'" data-field="body">Achievement description</div></div></div>';
			
			
			if(is_odd){
				$('.achievement-row:last-child').append(txt);
			}
			else{
				$('.achievement-row:last-child').after('<div class="columns achievement-row">'+txt+'</div>');
			}
			
			cura.bind();
			
		}
		else if(dt.section == 'f'){
			
			
			var index = $('.portfolio-item').length == 0 ? 0 : $('.portfolio-item:first-child').data('index');
			
			index++;	
			
			
			$('#portfolio').prepend('<a id="pft_'+index+'" class="portfolio-item" cura="portfolio_open" data-index="'+index+'"> <img class="f_'+index+'_img"  src="'+base+'/images/static/sunset-ocean.png"> <span class="f_'+index+'_title">Click to edit</span> </a>');
			
			cura.bind();
			
		}
		
	}
}

cura.action.page_part_remove = {
	url: '/cd/page_part_remove',
	callback: function (dt, e){
		
		dt.page_id = $('#public-page').data('id');
		
		if(cura.action.page_part_remove.confirmed){
			
			if(dt.section == 'b') {
				$(e).parents('div.service-row:eq(0)').remove();
				
			}
			else if(dt.section == 'd') {
				p = $(e).parents('div:eq(0)');
				p.remove();
				
			}
			else if (dt.section == 'f'){
				$('#pft_'+dt.index).remove();
				$('#portfolio-mask').css({display:'none'});
				$(document.body).removeClass('noscroll');
				
			}
		}
	},
	confirm:'Are you sure you want to remove this?',
	confirm_button:'Remove',
	message:'Removing...'	
}

cura.action.page_section_switch = {
	url:'/cd/page_section_switch',
	callback: function (dt, e) {
	
		dt.page_id = $('#public-page').data('id');
		dt.active = e.checked ? 1 : 0;
	
	},
	mask:false
}

cura.action.page_video_edit = {
	callback : function() {
		$('#video-update-dialog').css({display:'block'});
		$('#video-embed').val('');
	}
}

cura.action.page_video_cancel = {
	callback : function() {
		$('#video-update-dialog').css({display:'none'});
	}
}

cura.action.page_video_update = {
	url: '/cd/page_update',
	callback: function( dt) {
		
		var codes = $('#video-embed').val();
		
		if(!codes || codes == ''){
			notify("Please paste codes");
			return;	
		}
		
		url = extractVideo(codes);
		
		if(url === false) {
			notify("We do not recognize the video.<br>Please add video URL or embed codes");
			return false;	
		}
				
		$('#video-update-dialog').css({display:'none'});
		
		// posting
		dt.page_id = $('#public-page').data('id');
		dt.value = url;
		dt.field = 'body';
		dt.section = 'e';
	}
}



cura.action.portfolio_open = {
	//url:'/cd/portfolio_view',
	callback : function (dt){
		
		$('.portfolio-max').removeClass('onshow');
		
		if($('#pf_'+dt.index).length == 1) {
			$('#pf_'+dt.index).addClass('onshow');
		}
		else{
			
			// create dummy
			var index = dt.index;
			
			txt = '<div class="portfolio-max onshow" id="pf_'+index+'">';
			txt += '	<div class="portfolio-image" >';
			
			txt += '<a  href="javascript:void(0)" class="portfolio-delete"  cura="page_part_remove" data-section="f"  data-index="'+index+'"></a>';
			txt += '<a  href="javascript:void(0)" class="portfolio-upload"  cura="page_edit" data-section="f" data-field="image" data-index="'+index+'"></a>';
			
			txt += '    <img cura="page_edit" data-section="f" data-field="image" data-index="'+index+'" class="f_'+index+'_img" src="'+base+'/images/portfolio-upload.png">';
			txt += '    </div>';
			txt += '    <div class="portfolio-content">';
			txt += '    	<div class="portfolio-title f_'+index+'_title" cura="page_edit" data-section="f" data-index="'+index+'" data-field="title">Add Title</div>';
			txt += '        <div class="portfolio-description" cura="page_edit" data-section="f" data-index="'+index+'" data-field="body">Add description</div>';
			txt += '    </div>';
			txt += '</div>';
			
			$('#portfolio-container').append(txt);
			
			cura.bind();	
		}
		
		$('#portfolio-mask').css({display:'block'});
		$(document.body).addClass('noscroll');
		
		if($('.portfolio-max').length > 1){
			
			$('.portfolio-prev, .portfolio-next').css({display:'block', top: Math.round(($(window).height()-50-128)/2)});
			
			lft = $('#pf_'+dt.index).find('.portfolio-image').width();			
			$('.portfolio-next').css({left:lft-128});
		}
		else{
			$('.portfolio-prev, .portfolio-next').css({display:'none'});
		}
		
		
		
	}
}

cura.action.portfolio_show = {
	callback : function (dt, e, res) {
		$('#portfolio-mask').html(res.html).css({display:'block'});
		$(document.body).addClass('noscroll');
	}	
}

cura.action.portfolio_prev = {
	callback : function () {
		
		tp = $('.onshow');
		pv = tp.prev('div.portfolio-max:eq(0)');
		
		if(pv.length == 0){
			pv = $('div.portfolio-max:last-child');
		}
		
		tp.removeClass('onshow');
		pv.addClass('onshow');
		
		lft = pv.find('.portfolio-image').width();			
		$('.portfolio-next').css({left:lft-128});
	}
}

cura.action.portfolio_next = {
	callback : function (dt, e) {
		tp = $('.onshow');
		pv = tp.next('div.portfolio-max:eq(0)');
		
		if(pv.length == 0){
			pv = $('div.portfolio-max:first-child');
		}
		
		tp.removeClass('onshow');
		pv.addClass('onshow');
		
		lft = pv.find('.portfolio-image').width();			
		$('.portfolio-next').css({left:lft-128});
	}
}

cura.action.portfolio_close = {
	callback : function (dt){
		$('#portfolio-mask').css({display:'none'});
		$(document.body).removeClass('noscroll');
	}
}

cura.action.update_social_links = {
	url:'/cd/update_social_links',
	submit:'update_social_links',
	callback : function (dt) {
		dt.page_id = $('#public-page').data('id');	
		
		var a = $('#social_fb').val();
				if(a && a != '' && !(a.indexOf('://www.facebook.com') > 0 || a.indexOf('://facebook.com') > 0 || a.indexOf('://fb.com') > 0  )){
			notify("Invalid Facebook URL");
			return false;
		}
		
		
		 a = $('#social_tw').val();
		if(a && a != '' && !(a.indexOf('://www.twitter.com') > 0 || a.indexOf('://twitter.com') > 0 || a.indexOf('://t.co') > 0 ||  a.indexOf('://www.t.co') > 0  )){
			notify("Invalid Twitter URL");
			return false;
		}
		
		 a = $('#social_yt').val();
		if(a && a != '' && !(a.indexOf('://www.youtube.com') > 0 || a.indexOf('://youtube.com') > 0 || a.indexOf('://www.youtu.be') > 0 || a.indexOf('://youtu.be') > 0  )){
			notify("Invalid Youtube URL");
			return false;
		}
		
		 a = $('#social_ig').val();
		if(a && a != '' && !(a.indexOf('://www.instagram.com') > 0 || a.indexOf('://instagram.com') > 0  )){
			notify("Invalid Instagram URL");
			return false;
		}
		
		 a = $('#social_ln').val();
		if(a && a != '' && !(a.indexOf('linkedin.com/') > 0 )){
			notify("Invalid LinkedIn URL");
			return false;
		}
		
		 a = $('#social_pn').val();
		if(a && a != '' && !(a.indexOf('://www.pinterest.com') > 0 || a.indexOf('://pinterest.com') > 0  )){
			notify("Invalid Pinterest URL");
			return false;
		}
		
	},
	validate: {"rules":{"ww":{"url":true},"fb":{"url":true},"tw":{"url":true},"yt":{"url":true},"ig":{"url":true},"ln":{"url":true},"pn":{"url":true},"tz":{"url":true}},"messages":{"ww":{"url":"Invalid URL"},"fb":{"url":"Invalid URL"},"tw":{"url":"Invalid URL"},"yt":{"url":"Invalid URL"},"ig":{"url":"Invalid URL"},"ln":{"url":"Invalid URL"},"pn":{"url":"Invalid URL"},"tz":{"url":"Invalid URL"}}},
	
}

cura.action.social_link_reset = {
	callback : function (dt, e, res) {
		
		if(res && res.status){
			
			$('div.social-links.social-link-display a').css({display:'none'});
			
			if(res.data){
			
				for (var k in res.data){
					ele = $('div.social-link-display a.social-'+k);
					ele.attr('href',res.data[k]).css({display:'inline-block'});
				}
					
			}			
		}
		
		$('.social-link-display').css({display:'block'});
		$('.social-link-editor').css({display:'none'});
		$(window).scrollTop($('.social-strip').offset().top - 50);
			
	}
}

cura.action.social_link_open = {
	callback : function () {
		$('.social-link-display').css({display:'none'});
		$('.social-link-editor').css({display:'block'});
	}
}

cura.action.social_link_close = {
	callback : function () {
		$('.social-link-display').css({display:'block'});
		$('.social-link-editor').css({display:'none'});
		$(window).scrollTop($('.social-strip:eq(0)').offset().top - 50);
	}
}

/** Check avilability ***/
cura.action.check_date = {
	url: '/sd/check_date',
	submit:'inline-booking-form',
	
	validate:{"rules":{"date":{"required":true},"time":{"required":true}},"messages":{"date":{"required":"Please select a date"},"time":{"required":"Please pick a time"}}}
}

cura.action.picktime_open = {
	callback: function () {
		console.log('trigged');
		$('#time').css({display:'block'}).click();	
	}
}


cura.action.add_datetime ={
	callback: function(dt, e) {
		new TimePicker($('#time')[0]);
	}
}

cura.action.packages_show = {
	url:'/sd/packages_show',
	mask:false,
	message:false
}

/******************* Banner change ***********************/

function browserBannerImage () {
	$('#image_browse').off('change').on('change', _browserBannerImage);
	$('#image_browse').click();
}

function _browserBannerImage () {
	
	img = $('#image_browse')[0].files[0];
	
	if(img.type.indexOf('image/') < 0){
		console.log('Unsupported file '+img.name);
		return;	
	}
	
	var reader = new FileReader();
	reader.onload = function (event) {
		var img = new Image();
		img.onload = editBannerImage;
		img.src = event.target.result;
	}
	reader.readAsDataURL(img);
}

function editBannerImage () {
	
	$('.bi-crop').css({display:'block'});
	$('#bi-buttons').css({display:'flex'});
	
	$('#bi-canvas').draggable({
		stop: function (e, ui) {
			np = {};										
			// reposition of out of bounds
			pos = ui.position;
			cw = $('#bi-canvas').width();
			ch = $('#bi-canvas').height();
			bw = $('#bi-crop').width();
			bh = $('#bi-crop').height();
console.log(pos);
			if(pos.left > 0){
				np.left = 0;
			}
			
			if(pos.top > 0){
				np.top = 0;	
			}
			
			if(pos.left + cw < bw){
				np.left = (bw - cw);	
			}
			
			if(pos.top + ch < bh){
				np.top = bh - ch;	
			}

			if(np.hasOwnProperty('left') || np.hasOwnProperty('top')){
				$('#bi-canvas').animate(np,300);
			}
		}
	});
	
	this.id = 'bi-image';
	$(this).appendTo('#bi-canvas');
				
	iw = $('#bi-image').width();
	ih = $('#bi-image').height();
	bw = $('#bi-crop').width();
	isc = Math.ceil((bw/iw)*10)/10;
	nw = Math.round(iw*isc);	
	console.log([bw, isc]);
	$('#bi-canvas').data('zoom', isc).data('width', iw);
	
	$('#bi-image').width(nw);
	$('#bi-canvas').css({top:0, left:0});
	
	// reposition
	let winw = $(window).width();
	let bc = Math.round((winw-bw)/2);
	$('#bi-crop').css({left:bc});
	$('.zoom-tools').width(winw-100).css({left:Math.abs(bc)+50});
}


function zoomBi ( val){
	
	cz = parseFloat($('#bi-canvas').data('zoom'));
	
	if(val > 0 && cz < 2){
		cz += 0.1;		
	}
	else {
		cz -= 0.1;		
	}
	
	nx = Math.round(parseFloat($('#bi-canvas').data('width'))*cz);
	bw = $('#bi-crop').width();
	
	if(nx >= bw) {
	
		$('#bi-image').width(nx);
		$('#bi-canvas').data('zoom', cz);
	}
	
	// reposition if required
	np = {};										
			// reposition of out of bounds
	pos = $('#bi-canvas').position();
	cw = $('#bi-canvas').width();
	ch = $('#bi-canvas').height();
	bw = $('#bi-crop').width();
	bh = $('#bi-crop').height();

	if(pos.left > 0){
		np.left = 0;
	}
	
	if(pos.top > 0){
		np.top = 0;	
	}
	
	if(pos.left + cw < bw){
		np.left = (bw - cw);	
	}
	
	if(pos.top + ch < bh){
		np.top = bh - ch;	
	}

	if(np.hasOwnProperty('left') || np.hasOwnProperty('top')){
		$('#bi-canvas').animate(np,300);
	}
}


function cropBannerImage () {
	
	cd = $('#bi-canvas').data();
	ost = $('#bi-canvas').position();
	
	$('#image-form').remove();
	
	frm = '<form id="image-form">';
	frm += '<input type="hidden" name="page_id" value="'+($('#public-page').data('id'))+'">';
	frm += '<input type="hidden" name="scale" value="'+cd.zoom+'">';
	frm += '<input type="hidden" name="left" value="'+ost.left+'">';
	frm += '<input type="hidden" name="top" value="'+ost.top+'">';
	frm += '<input type="hidden" name="width" value="'+$('#bi-crop').width()+'">';
	frm += '<input type="hidden" name="height" value="'+$('#bi-crop').height()+'">';
	frm += '<input type="hidden" name="window" value="'+$(window).width()+'">';
	frm += '<input type="hidden" name="image" value="'+encodeURIComponent($('#bi-image').attr('src'))+'">';
	frm += '</form>';
	
	fpnid = notify("Updating banner...");
	block();
	
	$(document.body).append(frm);
	
	$.ajax({
		url: base+"/cd/page_update_banner",
	   type: "POST",
	   data:  new FormData($('#image-form')[0]),
	   contentType: false,
	   cache: false,
	   processData:false,
	   beforeSend : function(){},
	   success: function(res){
		   
		   unblock();
		   
		   if(res){
			   
			   if(res.status){
				   closeBannerImage();
				   $('#image-form').remove();
				   $('#page-header').css({'background-image' : 'url('+res.data.url+')'});
				   
				}
			  
			  	notify(res.message);
			
		   }
		},
		error: function(e) {
			unblock();
			notify("Error updating banner.", false);	
		}          
    });
	
}

function closeBannerImage () {
	
	$('.bi-crop').css({display:'none'});
	$('#bi-buttons').css({display:'none'});
	$('#bi-image').remove();
}




var gal_images = [];
var gal_index = 0;

function openGalery () {
	
	gal_images = [];
	
	var ci = this.src;
	
	imgs  = $(this).parents('div.fi-images:eq(0)').find('img');
	for(var i = 0; i < imgs.length; i++){
		if(ci == imgs[i].src){
			gal_index = i;	
		}

		gal_images.push(imgs[i].src.replace('/i400', '/images').replace('/i600', '/images'));
	}
	
	gtxt = '<div class="image-gallery" id="image-gallery">';
	
	if(gal_images.length > 1){
		gtxt += '<div class="image-scroll-left" ></div> <div class="image-scroll-right" ></div>';	
	}
	gtxt += '<a class="image-gallery-close">X</a> </div>';
	
	$(document.body).append(gtxt);
	
	$('.image-gallery-close').click(
		function () {
			$('#image-gallery').fadeOut('fast', function(){
				$(this).remove();
			});
		}
	);
	
	$('#image-gallery').css('background-image',"url("+gal_images[gal_index]+")");
	
	if(gal_images.length > 1){
		
		$('.image-scroll-right').click(function(){
				
			if(gal_images.length > gal_index +1){
				gal_index++;	
			}
			else{
				gal_index = 0;	
			}
			$('#image-gallery').css('background-image',"url("+gal_images[gal_index]+")");	
		});
		
		$('.image-scroll-left').click(function(){
				
			if(gal_index  > 0){
				gal_index--;	
			}
			else{
				gal_index = gal_images.length -1;	
			}
			$('#image-gallery').css('background-image',"url("+gal_images[gal_index]+")");	
		});
		
	}
}


function extractVideo (src){
	
	var attr = {};
	
	

	if(src.indexOf('<iframe') >= 0){
		
		var cp = src.split('<iframe');
		cp = cp[1].split('>');
		
		var attrs = cp[0].split(/\s+/);
		
		for(k = 0; k < attrs.length; k++){
			var c = attrs[k].replace('=','|').split('|');
			
			if(c[0] == 'src' ){
				attr[c[0]] = c[1];
				break;	
			}
		}
		
		if(attr['src']){
		
			if(attr['src'].indexOf('https://www.youtube.com') == 0 || attr['src'].indexOf('https://www.youtu.be')){
				return attr['src'];
			}
			
			if(attr['src'].indexOf('https://www.facebook.com') == 0 ){
				console.log('Here');
				let up = attr['src'].split('href=');
				if(up.length > 1){
					up = up[1].split('&');
					console.log(up);
					return decodeURIComponent(up[0]);
				}				
				
			}
			
			if(attr['src'].indexOf('https://www.dailymotion.com') == 0 ){
				return attr['src'];
			}
			
			if(attr['src'].indexOf('https://player.vimeo.com') == 0 ){
				return attr['src'];
			}
			
		}
		
		return  false;	
		
	}
	else{
		
		src = $.trim(src);
		
		if(src.indexOf('https://www.youtube.com') === 0 ){
			
			var up = src.split('?');
			var pairs = {};
			
			if(up.length == 1){
				return false;	
			}
			
			up = up[1].split('&');
			
			
			for(var a = 0; a < up.length; a++){
				pair = up[a].split('=');
				pairs[pair[0]] = pair[1];	
			}
		
			if(pairs.v){
				return 'https://www.youtube.com/embed/'+pairs.v;	
			}
			
			return false;
		}
		else if (src.indexOf('https://youtu.be/') === 0) {			
			
			let parts = src.split('/');
			parts = parts[parts.length - 1].split('?');
			
			if(parts[0]){
				return 'https://www.youtube.com/embed/'+parts[0];
			}			
		}
		else if(src.indexOf('https://www.dailymotion.com') === 0  || src.indexOf('https://vimeo.com') === 0 || src.indexOf('https://www.youtu.be') === 0 || src.indexOf('https://dai.ly') === 0){
			
			var up = src.split('?');
			up = up[0].split('/');
			
			vk = up.pop();
			
			if(src.indexOf('https://www.dailymotion.com') === 0 || src.indexOf('https://dai.ly') === 0){
				return 	'https://www.dailymotion.com/embed/video/'+vk+'?autoplay=1';
			}
			else if(src.indexOf('https://vimeo.com') === 0){
				return 	'https://player.vimeo.com/video/'+vk;
			}
			else {
				return 'https://www.youtube.com/embed/'+vk;	
			}
		}
		
		return false;
	}
	
	
}