var updateManage = {
  timer: null,
  paused: false
}

var _chatrooms = {};
_chatrooms.self = null;
_chatrooms.rooms = {};
_chatrooms.add = function (config) {


  var cr = new ChatRoom(config);
  _chatrooms.rooms[config.id] = cr;
  cr.refresh();

  _chatrooms.maximize(config.id);
}

_chatrooms.remove = function (id) {

  if (_chatrooms.rooms[id]) {
    _chatrooms.rooms[id].hide();
    delete _chatrooms.rooms[id];
  }
}

_chatrooms.maximize = function (id) {

  if (_chatrooms.rooms[id]) {

    for (var r in _chatrooms.rooms) {

      if (r == id) {
        $('#chat_box_' + r).removeClass('chat-minimized');
        $('#chat_box_' + id).off('click').css({
          bottom: 0, top:'', left:'', right:''
        });
        var cb = $('#chat_box_' + id + ' .chat-body');

        cb[0].scrollTop = cb[0].scrollHeight;

      } else {

        if (!$('#chat_box_' + r).hasClass('chat-minimized')) {
          _chatrooms.minimize(r, true);
        }

      }

    }

    _chatrooms.reArrange();
  }
}

_chatrooms.minimize = function (id, no_arrnge) {

  $('#chat_box_' + id).addClass('chat-minimized');
  $('#chat_box_' + id).on('click', function () {
    _chatrooms.maximize($(this).data('id'));
  });
  $('#chat_box_' + id + ' .chat-notify').html('').data('count', 0);

  if (!no_arrnge) {
    _chatrooms.reArrange();
  }
}

_chatrooms.reArrange = function () {

  var b = 5;
  let ww = $(window).width();
  for (var r in _chatrooms.rooms) {

    if (ww > 600) {

      if ($('#chat_box_' + r).hasClass('chat-minimized')) {
        $('#chat_box_' + r).css({ bottom: b, left:''   });
        b += 70;
      }
	
    }
	  else{
		 if ($('#chat_box_' + r).hasClass('chat-minimized')) {
			$('#chat_box_' + r).css({ left: b, bottom:''});
			b += 70;
		  } 
	  }

  }
}

var ChatRoom = function (config) {

  this.id = config.id;
  this.name = config.name;
  this.image = config.image;
  this.link = config.link;
  this.type = config.type;
  this.first_id = null;
  this.last_id = null;
  this.participants = config.participants;
  this.mute = config.mute;

  this.render();
}

ChatRoom.prototype.render = function () {

  if ($('#chat_box_' + this.id).length > 0) {
    // make it active
    this.maximize();
  } else {
    let ct;
    ct = '<div class="chat-box ' + (this.mute ? 'mute' : '') + '" id="chat_box_' + this.id + '" data-id="' + this.id + '">';
    ct += '<div class="chat-inner">'
    ct += '<span class="chat-notify"></span>';
    ct += '  <div class="chat-header">';
    ct += '    <div class="chat-controls">';
    ct += '      <div class="chat-spacer">&nbsp;</div>';
    ct += '      <a cura="context" data-id="' + this.id + '" data-action="chat_option" class="chat-option" title=""></a><a cura="chat_minimize" class="chat-min" data-id="' + this.id + '">&#8211;</a><a cura="chat_close" class="chat-close" data-id="' + this.id + '">x</a></div>';
    ct += '    <div class="chat-board">';
    ct += '      <div class="chat-image"><img src="' + (this.image ? this.image : base + '/images/placeholder-boy.png') + '"></div>';
    ct += '      <div class="chat-board-name">' + this.name + ' ';
    ct += (this.type) ? '<span>' + this.type + '</span>' : '';
    ct += '</div>';
    ct += '      <a class="chat-mute"></a> </div>';
    ct += '  </div>';
    ct += '  <div class="chat-body"> </div>';
    ct += '  <div class="chat-footer">';
    ct += '    <div class="chat-footer-wrap"> ';
    //ct += '<a class="message-add-attachment">+</a>';
    ct += '      <div class="chat-message-input" contenteditable="true" data-board="' + this.id + '"></div>';
    ct += '      <a class="chat-message-button" cura="chat_send" data-board="' + this.id + '"></a> </div>';
    ct += '  </div>';
    ct += '</div>';
    ct += '</div>';

    $(document.body).append(ct);

    $('#chat_box_' + this.id + ' .chat-message-input').keyup(function (e) {

      if (e.which == 13 && e.originalEvent.shiftKey === false) {
        cura.execute('chat_send', this);
        console.log(e.which);
      }

    });

    cura.bind();

    //this.refresh();
    $('#chat_box_' + this.id + ' .chat-body').scroll(function (e) {

      if (this.scrollTop == 0 && !$(this).hasClass('loaded') && !$(this).hasClass('loading')) {
        // get previous messages
        $(this).addClass('loading');
        cura.execute('chat_history', $(this).parents('div.chat-box:eq(0)'));
      }

    });

  }

}

ChatRoom.prototype.hide = function () {
  $('#chat_box_' + this.id).remove();
}

ChatRoom.prototype.refresh = function () {

  ele = $('#chat_box_' + this.id)[0];
  cura.execute('chat_feed', ele);
}


/********** Chat action ************/

cura.action.chat_open = {
  url: '/chat/open',
  mask: false,
  message: 'Opening messages',
  callback: function (dt, e) {
	  
	  if(dt.board){
		 if(_chatrooms.rooms[dt.board]){
			 _chatrooms.maximize(dt.board);
			 return false;
		 } 
	  }

    if (e) {
      $(e).removeClass('bg-blue');
    }
  }
}

cura.action.chat_show = {
  callback: function (dt, e, res) {

    if (res.data) {
      _chatrooms.add(res.data);
    }

  }
}

cura.action.chat_send = {
  url: '/chat/send',
  callback: function (dt, e) {

    ele = $(e).parents('div.chat-footer-wrap:eq(0)').find('div.chat-message-input');
    msg = $.trim(ele.html());

    if (!msg || msg == '') {
      return false;
    }

    dt.message = msg;
    dt.l = _chatrooms.rooms[dt.board].last_id
    ele.html("");

  },
  mask: false,
  message: false
}

cura.action.chat_close = {
  callback: function (dt) {
    _chatrooms.remove(dt.id);
  }
}

cura.action.chat_feed = {
  url: '/chat/feed',
  callback: function (dt, e) {

    if (_chatrooms.rooms[dt.id]) {
      //dt.f = _chatrooms.rooms[dt.id].first_id;	
      dt.l = _chatrooms.rooms[dt.id].last_id;
      updateManage.pause = true;
    } else {
      return false;
    }
  },
  mask: false,
  message: false
}

cura.action.chat_history = {
  url: '/chat/feed',
  callback: function (dt, e) {
    console.log(dt);
    if (_chatrooms.rooms[dt.id]) {
      dt.f = _chatrooms.rooms[dt.id].first_id;
      updateManage.pause = true;
    } else {
      return false;
    }
  },
  mask: false,
  message: false
}

cura.action.show_chat_messages = {
  callback: function (dt, e, res) {

    updateManage.pause = false;
    _chatrooms.self = res.data.self;

    if (res.data.messages) {

      showMessages(res.data.board, res.data.messages, res.data.self, res.data.dir);
    } else if (res.data.boards) {

      for (var b in res.data.boards) {
        showMessages(b, res.data.boards[b], res.data.self);
      }
    }
  }
}


cura.action.show_history = {
  callback: function (dt, e, res) {

    updateManage.pause = false;

    if (res.data.messages) {

      showMessages(res.data.board, res.data.messages, res.data.self, 1);
    }

  }
}

cura.action.chat_option = {
  url: '/chat/manage',
  callback: function (dt) {
    hideContextMenu();
  },
  menu: function (dt) {
    out = {
      'clear': 'Delete'
    };

    if (_chatrooms.rooms[dt.id]) {
      room = _chatrooms.rooms[dt.id];
      me = room.participants[_chatrooms.self]

      if (room.mute) {
        out['unmute'] = 'Unmute';
      } else {
        out['mute'] = 'Mute';
      }

      if (room.type == 'Event') {
        out['quit'] = 'Exit';
      } else {

        if (me.removed == "1") {
          out['unblock'] = 'Unblock ' + room.type;
        } else {

          out['block'] = 'Block ' + room.type;
        }

      }


    }
    return out;
  },
  mask: false,
  message: false
}

cura.action.chat_minimize = {
  callback: function (dt, e) {
    _chatrooms.minimize(dt.id);
  }
}

cura.action.board_action = {
  callback: function (dt, e, res) {

    if (!res.status) {
      return;
    }

    if (res.data.action == 'clear' || res.data.action == 'block') {
      _chatrooms.remove(res.data.id);
    } else if (res.data.action == 'mute') {
      $('#chat_box_' + res.data.id).addClass('mute');
      _chatrooms.rooms[res.data.id].mute = true;
    } else if (res.data.action == 'unmute') {
      $('#chat_box_' + res.data.id).removeClass('mute');
      _chatrooms.rooms[res.data.id].mute = false;
    } else if (res.data.action == 'remove') {
      $('#msg_' + res.data.id).remove(false);
    } else if (res.data.action == 'removeall') {
      $('#msg_' + res.data.id).after('<div class="control_msg">Message deleted</div>');
      $('#msg_' + res.data.id).remove(true);
    }

  }
}

cura.action.message_option = {
  url: '/chat/manage',
  callback: function (dt) {
    hideContextMenu();
  },
  menu: function (dt) {
    if (dt == 'in') {
      return {
        'remove': 'Delete'
      }
    } else {
      return {
        'remove': 'Delete for me',
        'removeall': 'Delete for all'
      }
    }
  },
  mask: false,
  message: false
}


/* dir: 0 -> append, dir: 1 -> prepend */
function showMessages(bid, messages, self, dir) {

  if (_chatrooms.rooms[bid]) {

    var room = $('#chat_box_' + bid + ' .chat-body');
    var chatters = _chatrooms.rooms[bid].participants;

    if (messages && messages.length > 0) {

      if (dir) {
        if (!_chatrooms.rooms[bid].first_id || _chatrooms.rooms[bid].first_id > parseInt(messages[messages.length - 1].id)) {
          _chatrooms.rooms[bid].first_id = parseInt(messages[messages.length - 1].id);
        }
      } else {
        if (!_chatrooms.rooms[bid].first_id || _chatrooms.rooms[bid].first_id > parseInt(messages[0].id)) {
          _chatrooms.rooms[bid].first_id = parseInt(messages[0].id);
        }

        if (!_chatrooms.rooms[bid].last_id || _chatrooms.rooms[bid].last_id < parseInt(messages[messages.length - 1].id)) {
          _chatrooms.rooms[bid].last_id = parseInt(messages[messages.length - 1].id);
        }
      }

      var d = messages[0].d;

      for (var m = 0; m < messages.length; m++) {

        var msg = messages[m];

        var point = true;

        if (dir) {
          if (messages[m - 1] && messages[m - 1].s == messages[m].s && messages[m - 1].d == messages[m].d) {
            point = false;
          }
        } else {
          if (messages[m + 1] && messages[m + 1].s == messages[m].s && messages[m + 1].d == messages[m].d) {
            point = false;
          }
        }

        if (d != msg.d) {

          if (dir) {
            room.prepend('<div class="chat-message-date">' + d + '</div>');
          } else {
            room.append('<div class="chat-message-date">' + msg.d + '</div>');
          }
        }

        var ct = '';

        if (msg.del == '1') {
          ct += '<div class="control_msg">' + chatters[msg.s].name + ' deleted the message</div>';

          if (dir) {
            room.prepend(ct);
          } else {
            room.append(ct);
          }
        } else if (msg.s == self) {

          ct += '<div id="msg_' + msg.id + '" class="chat-message sent ' + (point ? 'point' : '') + '">';
          ct += '<div class="chat-spacer"><a cura="context" data-action="chat_option" data-dir="out" data-id="' + msg.id + '"></a></div>';
          ct += '<div class="chat-message-content">';
          //	ct += '';
          //ct += '<div class="chat-spacer"></div>';
          //	ct += '<span>'+(msg.t).toLowerCase()+'</span>';
          if (point) {
            ct += '<div class="chat-message-header"><a>' + chatters[msg.s].name + '</a></div>';
          }
          //	ct += '';
          ct += '<div class="chat-message-body">' + msg.m.replace(/(?:\r\n|\r|\n)/g, '<br>') + '</div>';
          ct += '<div class="chat-message-footer">' + (msg.t).toLowerCase() + '</div>';
          ct += '</div>';
          ct += '<div class="chat-message-image">';
          if (point) {
            ct += '<img src="' + (chatters[msg.s].image ? chatters[msg.s].image : base + '/images/placeholder-boy.png') + '">';
          }
          ct += '</div>';
          ct += '</div>';

          if (dir) {
            room.prepend(ct);
          } else {
            room.append(ct);
          }

        } else {
          ct = '<div id="msg_' + msg.id + '"  class="chat-message received ' + (point ? 'point' : '') + '">';
          ct += '<div class="chat-message-image">';

          if (point) {
            ct += '<img src="' + (chatters[msg.s].image ? chatters[msg.s].image : base + '/images/placeholder-boy.png') + '">';
          }

          ct += '</div>';
          ct += '<div class="chat-message-content">';
          //ct += '';
          if (point) {
            ct += '<div class="chat-message-header"><a>' + chatters[msg.s].name + '</a></div>';
          }
          //ct += '<span>'+(msg.t).toLowerCase()+'</span>';
          ct += '<div class="chat-message-body">' + msg.m.replace(/(?:\r\n|\r|\n)/g, '<br>') + '</div>';
          ct += '<div class="chat-message-footer">' + (msg.t).toLowerCase() + '</div>';
          ct += '</div>';
          ct += '<div class="chat-spacer"><a cura="context" data-action="chat-message_option" data-dir="in" data-id="' + msg.id + '"></a></div>';
          ct += '</div>';

          if (dir) {
            room.prepend(ct);
          } else {
            room.append(ct);
          }
        }

        d = msg.d;


      } // ecah message

      // ack
      cura.util.call('/chat/ack', "b=" + bid + "&l=" + _chatrooms.rooms[bid].last_id).post();
      cura.bind();

      if (isWindowHidden()) {
        $('#chat_sound')[0].play();
      }

    }

    if (!dir) {
      room[0].scrollTop = room[0].scrollHeight;

      var ne = $('#chat_box_' + bid + ' .chat-notify');
      var nc = ne.data('count');

      if (nc) {
        nc = parseInt(nc) + messages.length;
      } else {
        nc = messages.length;
      }
      ne.html('<label>' + (nc < 10 ? nc : 9) + '</label>');
      ne.data('count', nc);

    } else {

      if (messages.length > 0) {
        room[0].scrollTop = $('#msg_' + messages[0].id).offset().top - room.offset().top;
      }

      room.removeClass('loading');

      if (messages.length < 10) {
        room.addClass('loaded');
      }
    }
  } else {
    // open room
    //	cura.execute('chat_open',{id:bid},null);	
  }

}

function checkChatUpdates() {

  if (updateManage.pause || isWindowHidden()) {
    return;
  }

  if (_chatrooms.rooms && Object.keys(_chatrooms.rooms).length > 0) {

    var ks = 0;

    for (var k in _chatrooms.rooms) {

      if (_chatrooms.rooms[k].last_id && _chatrooms.rooms[k].last_id > ks) {
        ks = _chatrooms.rooms[k].last_id
      }
    }

    cura.util.call('/chat/updates', 'l=' + ks, null, true).post();
  }

}

$(document).ready(function (e) {
  // updateManage.timer = setInterval(checkChatUpdates,2000);

  $(document.body).append('<div style="display:none"><audio id="chat_sound"  src="' + base + '/sounds/chat.mp3"></audio><audio id="notification_sound" src="' + base + '/sounds/notification.mp3"></audio></div>');
	
	$(window).resize(_chatrooms.reArrange);

});
