cura.gallery = {data: {}, element:{}};

cura.action.open_gallery = {
	url:'/cd/gallery_images',
	mask:false,
	callback: function(dt, e){
		
		cura.gallery.data = dt;
		cura.gallery.element = e;
		
		$(document.body).addClass('noscroll');
		
		if($('#gallery').length == 0) {
		
			$(document.body).append('<div class="gallery" id="gallery"> <div class="stage" id="stage"></div> <div class="gallery-buttons">  <div class="center"></div> <a class="button" cura="gallery_image_selected">Select</a> <div class="gap1"></div> <a class="button button-alt" cura="close_gallery">Close</a> </div> </div>');
					
		}
		
		$('#gallery').css('display','block');
		
	}
}

cura.action.close_gallery = {
	callback: function(){
		$('#gallery').css('display','');
		$(document.body).removeClass('noscroll');
	}
}
cura.action.select_image = {
	callback: function (dt, e, ev){
		console.log(ev);
		
		if(!ev.originalEvent.ctrlKey && !ev.originalEvent.shiftKey) {		
			$('#gallery_stage div.target').removeClass('target');		
		}
		
		$(e).addClass('target');
		
		if(ev.originalEvent.shiftKey){
			
			if($('div.target').length > 1){
				
				var range = false;
				var elements = $('.gallery-image');
				
				for(var i = 0; i < elements.length; i++){
					
					if(elements.eq(i).hasClass('target')){
						
						if(range) {
							break;
						}
						range = !range;	
						
						
					}
					
					if(range){
						elements.eq(i).addClass('target');
					}
					
				}
				
			}
			
		}
		
		
	}
}

cura.action.gallery_image_remove = {
	url:'/cd/image_remove',
	confirm:'Are you sure you want to remove this image?',
	confirm_button:'Remove',
	mask:false
	
}

cura.action.image_removed = {
	callback: function(dt, e, res){
		
		if(res.id){
			$('#img_'+res.id).fadeOut('slow', function(){ $(this).remove(); });	
		}
			
	}
}

cura.action.gallery_image_selected = {
	
	callback:function (){
		
		
		
		console.log(cura.gallery.data);
		//console.log(cura.gallery.element);
		
		var sel = $('#gallery_stage div.target');
		
		if(sel.length == 0){
			notify('Please select an image');
			return;	
		}
		
		cura.gallery.data.image_id = sel.data('id');
		dt = cura.gallery.data;
		sel = sel.eq(0);		
		
		
		if(dt.ref && (dt.ref == 'talent' || dt.ref == 'agency' || dt.ref == 'customer')) {
			var img = new Image();
			img.onload = function () { showCropTool (this, true) };
			img.src = sel.eq(0).data('url');
		}
		else if(ct_editing && $(ct_editing).data('section') == 'b'){
			var img = new Image();
			img.onload = function () { showCropTool (this, false, true) };
			img.src = sel.data('url');
		}
		else if(dt.crop){
			var img = new Image();
			img.onload = function () { showCropTool (this, false) };
			img.src = sel.data('url');
		}
		else{
			// send data to update
			/*
			notify("Updating image...");
	
			$.ajax({
				url: base+'/cd/page_update_image',
				data:dt,
				type: "POST",
				success: wrap({json:true} ,cura.handler.callComplete)		
			});
			*/
			uploadCuraImage(null, dt);
		}
		
		
		$('#gallery').css('display','');
		$(document.body).removeClass('noscroll');
	}
	
}