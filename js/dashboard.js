// Dashboard


cura.action.dash_menu = {
	url:'/dash',
	callback:function(dt){
		cura.action.dash_menu.url = '/dash/'+dt.id;
		cura.action.dash_menu.data = {}; 
		closeMainMenu();
	}
}

cura.action.get_dash_page = {
		url:'/dash',
		callback: function (dt, e, res){
			
			if(document.getElementById('wizard')){
				window.location.href = window.location.href;
			}
			else{
				cura.action.get_dash_page.url = '/dash/'+res.data;
			}
			closeMainMenu();
		}
	}

cura.action.ctl_confirm = {
	url:'/cd/ctl_confirm'
}

/**** Account page ***************/
cura.action.customer_info_edit = {
	url:'/cd/customer_edit',
	loading:'customer_info_edit',
	mask:false
	
}

cura.action.customer_info_update ={
	callback : function () {
		// manually validate form
		$('#dob-error, #gender-error').css({display:'none'});
		
		if($('#dob_day').length > 0 && !validateDOB()){
			$('#dob-error').css({display:'block'});	
			notify("Invalid date for birthday");
			return;
		}
		
		if($('input.gender').length > 0 && $('input.gender:checked').length == 0){
			$('#gender-error').css({display:'block'});	
			notify("Please select gender");
			return;
		}
		
		if(!cura.forms.validate('customer_info_submit')){
			return;
		}
		
		txt = '';
		
		
		if($('#user_firstname').length > 0) {
			txt += '<div class="field-wrapper">';
			txt += '<label class="dynamic-label dynamic-label-fold" for="user_firstname" >First Name</label>';
			txt += $('#user_firstname').val() +'</div>';
		}
		
		if($('#user_lastname').length > 0) {
			txt += '<div class="field-wrapper">';
			txt += '<label class="dynamic-label dynamic-label-fold" for="user_firstname" >Last Name</label>';
			txt += $('#user_lastname').val() +'</div>';
		}
		
		if($('#agency_name').length > 0) {
			txt += '<div class="field-wrapper">';
			txt += '<label class="dynamic-label dynamic-label-fold" for="user_firstname" >Agency Name</label>';
			txt += $('#agency_name').val() +'</div>';
		}
		
		if($('#agency_type').length > 0) {
			txt += '<div class="field-wrapper">';
			txt += '<label class="dynamic-label dynamic-label-fold" for="user_firstname" >Agency Type</label>';
			txt += $('#agency_type option:selected').html() +' Agency</div>';
		}
		
		if($('#display_name').length > 0) {
			txt += '<div class="field-wrapper">';
			txt += '<label class="dynamic-label dynamic-label-fold" for="user_firstname" >Talent\'s Display Name</label>';
			txt += $('#display_name').val() +'</div>';
		}
		
		
		
		if($('#dob_day').length > 0) {
			txt += '<div class="field-wrapper">';
			txt += '<label class="dynamic-label dynamic-label-fold" for="user_firstname" >Date Of Birth</label>';
			txt += $('#dob_month').val()+'/'+$('#dob_day').val()+'/'+ $('#dob_year').val()+'</div>';
		
		}
		
		if($('input.gender').length > 0) { 
			gn = parseInt($('input.gender:checked').val());
			
			txt += '<div class="field-wrapper">';
			txt += '<label class="dynamic-label dynamic-label-fold" for="user_firstname" >Gender</label>';
			if(gn == 1){
				txt += 'Male';
			}
			else if(gn == 2){
				txt += 'Female';
			}
			else if(gn == 3){
				txt += 'Transgender Male';
			}
			else if(gn == 4){
				txt += 'Transgender Female';
			}
			txt += '</div>';
		
		}

		new Dialog( {
				title: 'Confirm Detail',
				message : txt,
				mask : true,
				width:375,
				buttons: {
						ok : {label : 'Save Detail', action: 'customer_info_submit'},
						cancel : {label : 'Cancel', class: 'button-alt'}
					}
			});
	},
	mask:true
}

cura.action.customer_info_close = {
	callback : function () {
		$('#customer_info_edit').html('');
	}
}


cura.action.customer_info_submit = {
	url : '/cd/customer_info_update',
	submit: 'customer_info_update',
	mask:true,
	validate: {"rules":{"user_firstname":{"required":true},"user_lastname":{"required":true},"gender":{"required":true}},"messages":{"user_firstname":{"required":"First name is required"},"user_lastname":{"required":"Last name is required"},"gender":{"required":"Please select gender"}}},
	callback: function () { closeDialog();  },
	message: 'Updating personal detail...'
}


cura.action.customer_city_edit = {
	url : '/cd/customer_edit',
	loading:'customer_location',
	mask:false
}

cura.action.customer_tz_edit = {
	url : '/cd/customer_edit',
	loading:'customer_timezone',
	mask:false
}

cura.action.customer_currency_edit = {
	url : '/cd/customer_edit',
	loading:'customer_currency',
	mask:false
}

cura.action.customer_setting_close ={
	callback: function () {
		$('#customer_setting').html('');	
	}
}

cura.action.customer_setting_update = {
	url : '/cd/customer_setting_update',
	callback:function(dt, e) {
		if(cura.location && cura.location.place_id){			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
		}
	},
	submit:'customer_setting_update',
	message: "Updating...",
	mask:false
}

cura.action.customer_phone_add = {
	url:'/cd/phone_edit',
	mask:false	
}

cura.action.customer_phone_edit = {
	url:'/cd/phone_edit',
	callback : function (dt) {
		$('#phone_edit_'+dt.id).html('<div class="inline-loader"></div>');
	},
	mask:false	
}

cura.action.phone_update = {
	url : '/cd/phone_update',
	submit:'phone_update',
	callback: function() {
		setTimeout('closeDialog()',10);
	},
	validate: {"rules":{"phone_type":{"required":true},"phone_phone":{"required":true,"digits":true,"minlength":6,"maxlength":14}},"messages":{"phone_type":{"required":"Phone Type is required"},"phone_phone":{"required":"Please enter phone number","digits":"Phone number can have digits only.","minlength":"Phone number is too short","maxlength":"Phone number is too long"}}},
	mask:false
}

cura.action.make_primary = {
	url:'/cd/make_primary',
	mask:false
}

cura.action.set_primary = {
	url:'/cd/set_primary',
	callback: function() {
		let cv = $('#code').val();
		
		if(!cv || cv.length < 5 ){
			notify("Invalid OTP");
			return false;
		}
		setTimeout('closeDialog()',10);
	},
	submit:'otp_form',
	validate:{"rules":{"code":{"required":true, minlenth:5},},"messages":{"code":{"required":"Please  enter OTP code", minlength:'Please enter 6 character code '}}},
}

cura.action.phone_close = {	
	callback : function (dt) {
		if(dt.id){
			$('#phone_edit_'+dt.id).html('');
		}
		else{
			$('#phone_add').html('');
		}
	}
}

cura.action.phone_delete = {
	url: '/cd/phone_delete',
	confirm: 'Are you sure you want to remove this phone number?',
	confirm_button: 'Remove'	
}

cura.action.email_edit = {
	url:'/cd/email_edit',
	mask:false,
	callback: function (dt) {
		
		if(dt.id) {
			$('#email_edit_'+dt.id).html('<div class="inline-loader"></div>');
		}
		else {
			$('#email_add').html('<div class="inline-loader"></div>');
		}
	}
}



cura.action.customer_email_add = {
	url:'/cd/email_edit',
	mask:false	
}

cura.action.email_update = {
	submit:'email_update',
	url: '/cd/email_update',
	callback: function () {
		let val = $('#email_email').val();
		if(!val || val == '' || val.indexOf('@') < 1){
			notify('Invalid email address');
			return false;
		}
		
		setTimeout('closeDialog()',10);
	},
	validate: {rules:{email_email: {required: true, email:true}}, messages:{email_email:{required: 'Please enter email address', email:'Invalid email address'}}},
	mask:false
}	

cura.action.email_close = {	
	callback : function (dt) {
		if(dt.id){
			$('#email_edit_'+dt.id).html('');
		}
		else{
			$('#email_add').html('');
		}
	}
}

cura.action.email_delete = {
	url: '/cd/email_delete',
	confirm: 'Are you sure you want to remove this email address?',
	confirm_button: 'Remove'	
}

cura.action.verify_contact = {
	url: '/cd/verify_contact'
}

cura.action.validate_auth = {
	url: '/cd/validate_auth',
	submit: 'validate_auth',
	validate: {rules:{vcode: {required: true, 'min-length':5}}, messages:{vcode: {required: 'Please enter authorization code', 'min-length':'Authorization code is too short'}}},
	callback: function (){
			vcode = $('#vcode').val();
			return vcode && vcode.length >= 6;
		}
}

cura.action.contact_auth_completed = {
	callback: function () {
		closeDialog();
		cura.execute('get_dash_page', null, {data:'account'});	
	}
}

cura.action.switch_profile = {
	url:'/cd/switch_profile',
	message: 'Loading dashboard...',
}

cura.action.package_edit = {
	url:'/cd/package_edit',
	mask:false,
	callback: function (dt) {
/*
		if(dt.id) {
			$('#package_edit_'+dt.id).html('<div class="inline-loader"></div>');
		}
		else {
			$('#package_add').html('<div class="inline-loader"></div>');
		}*/
	}
}

cura.action.package_update = {
	url:'/cd/package_update',
	submit:'package_update',
	mask:false	,
	validate:{"rules":{"package_name":{"required":true},"package_description":{"required":true},"package_timespan":{"required":true},"package_fee":{"required":true,"number":true},"package_advance_percentage":{"required":true,"digits":true,"min":25,"max":100}, "package_items":{"digits":true}},"messages":{"package_name":{"required":"Please enter a name for the package"},"package_description":{"required":"Add a brief description about the package"},"package_timespan":{"required":"Package time span is required"},"package_fee":{"required":"Please enter fee for this package","number":"Invalid number"},"package_advance_percentage":{"required":"Advance Percentage is required","digits":"Invalid value","min":"Advance Percentage is too low","max":"Advance Percentage is too heigh"}, "package_items":{"digits":"Please enter a number"}}}
}

cura.action.package_close = {
	callback: function(dt) {
		
		if(dt.id) {
			$('#package_edit_'+dt.id).html('');
		}
		else {
			$('#package_add').html('');
		}
	}
}

cura.action.package_delete = {
	url: '/cd/package_delete',
	confirm: 'Are you sure you want to remove this package?<br><br><div class="anote">*You may temparary disable package by changing status.</div>',
	confirm_button: 'Remove'
}

cura.action.search_booking  = {
	url : '/cd/search_booking',
	submit:'search_booking',
	mask:false
}

cura.action.toggle_profiles = {
	callback : function() {
		$('.profile-selection').slideToggle(300);
	}
}


cura.action.get_booking = {
	url: '/cd/booking_get',
	mask:false	
}

cura.action.message_refresh = {
	url: '/cd/booking_messages',
	callback : function (dt, e) {
		
		dt.booking_id = $('#booking_msg_board').data('id');
		
	},
	mask:false,
	message:false
}

cura.action.appoinments_get = {
	callback : function(dt,para) {
		// TO DO
		console.log(para);
	}
}


cura.action.package_change = {
	url: '/cd/package_change',
	loading:'package_change',
	mask:false
}
/*
cura.action.date_change = {
	url: '/cd/date_change',
	callback: function () {
		$('#date_change').html('<div class="inline-loader"></div>');
	},
	mask:false
}
*/
cura.action.package_offer_cancel = {
	callback: function () {
		$('#package_change').html('');
	}
}

cura.action.date_offer_cancel = {
	callback: function () {
		$('#date_change').html('');
	}
}

cura.action.package_offer = {
	callback: function () {
		if($('input[name=suggested_package]:checked').length == 0){
			notify("Please select a package");
			return false	
		}
		else{
			chk = $('input[name=suggested_package]:checked');
			prt = chk.parents('.selection-wrapper:eq(0)');
			prt.css({display:'none'});	
			txt = chk.next('div').html();
			prt.after('<div class="sep"></div><div  class="min-info-desc">YOUR OFFER:</div><div>'+txt+'<br>'+chk.next('div').next('div').html()+'</div><div class="sep"></div><a class="dash-link" cura="package_offer_cancel" >Remove</a>');
			
			cura.bind();
		}
	}	
}
/*
cura.action.date_offer = {
	callback: function () {
		
		var sd = $('#suggested_date').val();
		var st = $('#suggested_time').val();
		
		if(sd == $('#suggested_date').data('value') && $('#suggested_time').val() == $('#suggested_time').data('value')){
			notify("Pick a different date/time");
			return;	
		}
		
		if(!sd || sd == '' || !st || st == ''){
			notify("Please select a date and time");
			return;	
		}
		
		chk = $('#suggested_date');
		prt = chk.parents('.selection-wrapper:eq(0)');
		prt.css({display:'none'});	

		prt.after('<div class="sep"></div><div  class="min-info-desc">YOUR SUGGESTION:</div><div>'+sd+'<br>'+st+'</div><div class="sep"></div><a class="dash-link" cura="date_offer_cancel" >Remove</a>');
		
		cura.bind();
		
	}	
}
*/
cura.action.expence_add = {
	count:0,
	callback : function () {
		
		txt = '<div class="field-wrapper">';
		txt += '  <label class="dynamic-label">Type of the expence</label>';
		txt += '  <select id="expence_type" class="input-short">';
		
		for(x in expence_types){
			txt += '<option value="'+(x)+'">'+(expence_types[x])+'</option>';	
		}
		
		txt += '  </select>';
		txt += '</div>';
		txt += '<div class="field-wrapper">';
		txt += '  <label class="dynamic-label">Description</label>';
		txt += '  <textarea id="expence_desc" class="input-short"></textarea>';
		txt += '</div>';
		txt += '<div class="field-wrapper">';
		txt += '  <label class="dynamic-label">Amount ('+currency+')</label>';
		txt += '  <input type="text" id="expence_amount" class="input-short">';
		txt += '</div>';
		
		
		
		new Dialog( {
				title: 'Additional Expences',
				message : txt,
				mask : true,
				buttons: {
						ok : {label : 'Add Expence', callback: function () {
							
							type = $('#expence_type').val();
							desc = $('#expence_desc').val();
							amount = $('#expence_amount').val();
							amount = parseFloat(amount);
							
							if(!amount || amount < 0){
								notify("Invalid amount");
								return false;
							}
							
							txt = '<div class="list-select expence" data-value="'+amount+'">';
							txt += '<input type="hidden" name="expences['+cura.action.expence_add.count+'][type]" value="'+type+'">';
							txt += '<input type="hidden" name="expences['+cura.action.expence_add.count+'][desc]" value="'+desc+'">';
							txt += '<input type="hidden" name="expences['+cura.action.expence_add.count+'][amount]" value="'+amount+'">';
							txt += '<div class="resizable">'+expence_types[type]; 
							if(desc && desc != '') {
								txt += '<span class="min-info-desc">'+desc +'</span>';
							}
							txt += '</div>';
							txt += '<div class="">'+currency+' '+amount.toString().replace(/\,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</div>';
							
							txt += '<div style="padding:1rem 0"><a title="Remove" class="icon-trash" cura="expence_remove"></div>';
							
							txt += '</div>';
							
							if($('#additional_exp input').length > 0){
								$('#additional_exp').append(txt);	
							}
							else{
								$('#additional_exp').html(txt);	
							}
							
							closeDialog();
							cura.bind();
							cura.action.expence_add.count++;
							
							cura.execute('recalculate_booking_total');
								
						}},
						cance: {label : 'Cancel', class:'button-alt'}
					}
			});		
	}
}


cura.action.expence_remove = {
	callback: function (dt, ele) {
		console.log(ele);
		new Dialog( {
				title: 'Confirm',
				message : 'Are you sure you want to remove this expence?',
				mask : true,
				width: 300,
				data:ele,
				buttons: {
						ok : {label : 'Remove', callback: function(l, ele){

							$(ele).parents('div.list-select:eq(0)').remove();
							
							if($('#additional_exp input').length == 0){
								$('#additional_exp').html('<div class="txt-light">'+$('#additional_exp').data('label')+'</div>');
							}
							
							closeDialog();
							
							cura.execute('recalculate_booking_total');
							
						}},
						cancel : {label : 'Close', 'class' : 'button-alt'}
					}
			});
	}
}

cura.action.recalculate_booking_total = {	
	callback : function () {
		
		var discount = $('#discount').val();
		console.log(discount);
		if(discount){
			discount = Math.abs(parseFloat(discount));
			
			if(discount > 0){
				
				if($('.discount-row').length == 0){
					$('.total-row').before('<div class="list-select expence discount-row" data-value="-'+discount+'">	<div class="resizable">Discount</div> <div id="discount_amount">'+(currency+' '+discount.toString().replace(/\,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ","))+'</div></div>');
				}
				else{
					$('#discount_amount').html(currency+' '+discount.toString().replace(/\,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ","));
					$('.discount-row').data('value', '-'+discount);
				}
				
			}
			else{
				$('.discount-row').remove();
			}
		}
		else{
			$('.discount-row').remove();	
		}
		
		var total = 0;
		var expences = $('.expence');
		for(var a = 0; a < expences.length; a++){
			total += parseFloat(expences.eq(a).data('value'));	
		}
		$('#total_expence').html(currency+' '+total.toString().replace(/\,/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	}
}

cura.action.booking_accept = {
	url: '/cd/booking_accept',
	submit: 'booking_accept',
	confirm: 'Have you added all your expences?',
	confirm_button: 'Yes, Accept Booking',
	validate:{"rules":{"discount":{"digits":true,"min":0},"additional_people":{"digits":true,"min":0}},"messages":{"discount":{"digits":"Invalid value","min":"Invalid discount amount"},"additional_people":{"digits":"Invalid value","min":"Invalid number of people"}}},
}


cura.action.booking_reject = {
	url: '/cd/booking_reject',
	confirm: 'Are you sure you want to REJECT this booking?',
	confirm_button: 'REJECT'
}


cura.action.booking_confirm = {
	url:'/cd/booking_confirm',
	submit:'booking_confirm',
	callback : function () {
		$('#booking_changes_error').css({display:'none'});
		
		if(!$('#booking_conditions')[0].checked){
			notify('Please accept booking changes');
			$('#booking_conditions_error').css({display:'block'});
			return false;
			
		}
		
	}
}


cura.action.booking_cancel = {
	url:'/cd/booking_cancel',	
}

cura.action.booking_update = {
	url:'/cd/booking_update',
	submit : 'booking_update',
	callback: function (dt, e){
		if(cura.location && cura.location.place_id){			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
		}
	},
	validate: {"rules":{"reason":{"required":true},'booking_discount':{min:0}},"messages":{"reason":{"required":"Please select a reason"}, "reason":{"min":"Invalid discount amount"}}}	
}

cura.action.booking_change_cancel = {
	url: '/cd/booking_change_cancel',
	submit : 'booking_update',
	confirm: 'Are you sure you want to cancel this booking?',
	validate: {"rules":{"reason":{"required":true}},"messages":{"reason":{"required":"Please select a reason"}}}	,
	confirm_button:'Cancel Booking'
}


cura.action.switch_payment_methods = {
	callback : function (dt) {
		$('.payment-methods').slideUp();
		$('#'+dt.id).slideDown();	
	}
}

cura.action.set_card_type = {
	card_length:null,
	callback: function(dt, ele, e) {
		//console.log(e.which);	
		if(e.which != 8 && e.which != 46){	
			isCardValid('payment_card_num');
		}
	}
}

cura.action.card_submit = {
	url: '/cd/card_submit',
	submit: 'payment_form',
	validate:{"rules":{"payment_card_name":{"required":true},"payment_card_num":{"required":true,"minlength":13,"maxlength":20},"payment_card_cvv":{"required":true,"digits":true,"minlength":3,"maxlength":4}},"messages":{"payment_card_name":{"required":"Enter card holder name"},"payment_card_num":{"required":"Enter card number","minlength":"Card number is too short","maxlength":"Card number is too long"},"payment_card_cvv":{"required":"Enter CVV","digits":"Invalid CVV","minlength":"Invalid CVV","maxlength":"Invalid CVV"}}}
}


cura.action.bank_submit = {
	url: '/cd/bank_submit',
	submit: 'transfer_form',
	validate:{"rules":{"payment_transaction_id":{"required":true,"minlength":4}},"messages":{"payment_transaction_id":{"required":"Please enter bank transfer ID","minlength":"Transfer ID is too short"}}}
}


cura.action.booking_note = {
	url: '/cd/booking_note',
	message: 'Updating....',
	mask:false
}
cura.action.booking_name = {
	url: '/cd/booking_name',
	message: 'Updating....',
	mask:false
}

cura.action.booking_change = {
	url: '/cd/booking_change',
	message: 'Updating....',
	mask:false
}

cura.action.booking_change_list = {
	url: '/cd/booking_changes',
}

cura.action.change_booking_package = {
	callback : function (dt, e) {
		$('.packages').removeClass('selected-package');
		$('#pkg'+dt.id).addClass('selected-package');
		$('#package_id').val(dt.id);
	}
}

cura.action.booking_change_respond = {
	url: '/cd/booking_change_respond',
	callback: function (dt, e){
		if(dt.cancel){
			cura.action.booking_change_respond.confirm = "Are you sure you want to cancel the booking?";
			cura.action.booking_change_respond.confirm_button = "Cancel Booking";
			
		}
		else if(dt.reply == 'accept'){
			cura.action.booking_change_respond.confirm = "Booking will be updated with these changes";
			cura.action.booking_change_respond.confirm_button = "Accept";
		}
		else{
			cura.action.booking_change_respond.confirm = "Are you sure you want to decline this change request?";
			cura.action.booking_change_respond.confirm_button = "Decline";
		}
			
	}
}



cura.action.booking_event_edit = {
	url: '/ev/booking_event_edit'	
}

cura.action.booking_event_update = {
	url : '/ev/booking_event_update',
	submit: 'event_update',
	callback: function () {
		
		if(cura.location && cura.location.place_id){			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
		}
		else{
			notify('Please select a location');
			return false;	
		}
		
	},
	validate: {"rules":{"event_name":{"required":true},"event_begins_date":{"required":true},"event_venue":{"required":true}},"messages":{"event_name":{"required":"Lets give it a name"},"event_begins_date":{"required":"Please select event date"},"event_venue":{"required":"Please select venue or city"}}}
}

cura.action.message_compose = {
	callback: function (dt, e){
		
		if(dt.scroll){
			$(window).scrollTop($('#message-panel').offset().top);	
		}
		
		$('#message-field').val('');
		$('#message_editor').slideDown();	
	}
}

cura.action.message_send = {
	url: '/cd/message_send',
	data:{},
	callback : function (dt) {
		val = $('#message-field').val();
		val = $.trim(val);
		
		if(!val || val == ''){
			notify("Message is empty");
			return false;
		}
			
		dt.value = $('#message-field').val();	
		$('#message_editor').slideUp();	
	},
	message: 'Sending message...'
}

cura.action.message_cancel = {
	callback: function (){
		$('#message_editor').slideUp();	
	}
}

cura.action.show_messages = {
	callback: function (dt, e, res) {

		$('.messages:eq(0)').prepend(res.html);
	}		
}

cura.action.appointment_search = {
	url:'/cd/appointment_search',
	submit:'appointment-search',
	callback: function(dt, e){
		
		if(dt.ref) {
			$('#timespan').val(dt.ref);	
		}
		console.log(dt);	
	},
	mask:false
}

cura.action.switch_day = {
	callback:function (data, e, d) {
		
		var dv = $('#filter_date').val();
		
		if(dv && dv != ''){
			
			var dt = new Date(Date.parse(dv));
			if(dt){
				dt.setDate(dt.getDate() + d);
				df = cura.data.days[dt.getDay()]+' '+dt.getDate()+' '+cura.data.months[dt.getMonth()]+' '+dt.getFullYear();
				
				
				$('#filter_date').val(df);

				//cura.execute('appointment_search');
				if(data.callback) {
					
					cura.execute(data.callback);
				}
			}
			else{
				notify("Invalid date");	
			}
			
		}
		else{
			notify("Invalid date");	
		}
	}
}

cura.action.switch_apppointment_week = {
	callback:function (dt, e, d) {
		console.log([dt, e, d]);
		
		var tw = parseInt(dt.week)+d;
		e.data('week', tw);
		
		var abst = new Date();
		abst.setMonth(0);
		abst.setDate(1);
		
		var ft = abst.getTime();		
		var tt = ft+ tw*7*24*60*60*1000;
		
		var dt = new Date(tt);
		dt.setDate(1);
		var mt = dt.getTime();
		
		wn = Math.ceil((tt-mt)/(7*24*60*60*1000));
		
		$('#filter_week').val(dt.getFullYear()+'-'+tw);
		$('#week_name').html(cura.data.months[dt.getMonth()]+'. '+wn+'<span>'+cura.data.st[wn]+'</span> week');
		
		cura.execute('appointment_search');
	}
}

cura.action.switch_month = {
	callback:function (dt, e, d) {
		
		var mo = parseInt($('#filter_month').val());
		var yr = parseInt($('#filter_year').val());
		
		mo += d;
		
		if(mo < 1){
			yr -= 1;
			
			if(yr < 2021){				
				return;	
			}
			
			$('#filter_month').val(12);	
			
			
			
			$('#filter_year').val(yr);	
		}		
		else if(mo > 12){
			$('#filter_month').val(1);	
			
			yr += 1;
			$('#filter_year').val(yr);	
		}
		else{
			$('#filter_month').val(mo);	
		}
		
		if(dt.callback) {
			cura.execute(dt.callback);
		}
		
	}
}


cura.action.appointment_update = {
	url: '/cd/appointment_update',
	submit:'appointment_update',
	validate:{"rules":{"appointment_description":{"required":true},"appointment_begins_date":{"required":true},"appointment_begins_time":{"required":function(){ return !document.getElementById('appointment_all_day').checked}},"appointment_duration":{"required":function(){ return !document.getElementById('appointment_all_day').checked}}},"messages":{"appointment_description":{"required":"Please add a title"},"appointment_begins_date":{"required":"Please select a date"},"appointment_begins_time":{"required":"Please select begin time"},"appointment_duration":{"required":"Please enter time span"}}}	
}

cura.action.appointment_full_day = {
	callback: function (dt, e) {
		if(e.checked){
			$('.non-ful-day').css({display:'none'});
		}
		else{
			$('.non-ful-day').css({display:'block'});
		}
	}
}

cura.action.appointment_list = {
	callback : function (dt) {
		$('#filter_date').val(dt.date);	
		
		$('.tabs a:eq(0)').click();
	}
}

cura.action.view_my_appointments = {
	url:'/cd/appointment_search',
	callback : function (dt, e){
		dt.action = "appointment_search";
		dt.id = 'appointment_day';
		dt.timespan = 'day';
		dt.ref = 'day';
		
		$('#appointments').removeClass('hidden');
	}
}

cura.action.appointment_options = {
	url: '/cd/appointment_options',
	menu: {'view':'View Appointment', 'edit':'Edit Appointment'},
	callback: function(){
		hideContextMenu();
	}
}

cura.action.appointment_delete = {
	url:'/cd/appointment_delete',
	mask:false,
	confirm: 'Are you sure you want to cancel this appointment?',
	confirm_button: 'Remove'
}

cura.action.appointment_confirm = {
	url:'/cd/appointment_confirm'
}

cura.action.subscription_add = {
	url:'/cd/subscription_add',	
}

cura.action.subscription_category = {
	url : '/cd/subscription_category',
	submit: 'subscription-form',
	callback : function( dt) {
		
		if($('input.category').length > 0 && $('input.category:checked').length == 0){
			$('#category-error').css({display:'block'});
			return false;
		}
		else{
			$('#category-error').css({display:'none'});
		}		
	}
}

cura.action.subscription_package = {
	url : '/cd/subscription_package',
	submit: 'subscription-form',	
}

cura.action.update_package_price = {
	callback: function (dt, e) {
		
		if($('#display_amount').length > 0) {
			$('.amount').html($('#display_amount').val());
		}
		else {
			if(e.checked) {
				val = $(e).parents('.custom-option:eq(0)').find('.package-amount').text();
				$('.amount').html(val);
				console.log('price:'+val);
			}
		}
	}
}

cura.action.subscription_payment = {
	url : '/cd/subscription_payment',
	submit: 'subscription-payment',
	callback : function (dt, e) {
			
	},
	validate: {"rules":{"term":{"required":true},"payment_card_name":{"required":true},"payment_card_num":{"required":true,"creditcard":true},"payment_card_cvv":{"required":true,"digits":true}},"messages":{"term":{"required":"Term is required"},"payment_card_name":{"required":"Please enter card holder name"},"payment_card_num":{"required":"Please enter card number","creditcard":"Invalid card number"},"payment_card_cvv":{"required":"Please enter CVV","digits":"Invalid CVV"}}}
}

cura.action.subscription_view = {
	url:'/cd/subscription_view'
}

cura.action.subscription_change = {
	url: '/cd/subscription_edit',
	callback: function () {
		$(window).scrollTop($('#package-change').offset().top-60);
	},
	loading: 'package-change',
	mask:false	
}
cura.action.subscription_change_cancel = {
	url:'/cd/subscription_change_cancel',
	confirm:"Are you sure you want to stay on current plan?<br>We'll cancel you plan change now",
	confirm_button:"Yes"
}

cura.action.inline_load_cancel = {
	callback : function(dt, e) {
		$('.dash-info-row').removeClass('selected-block');	
		$(e).parents('div.inline_load:eq(0)').html('');	
		
	}
}

cura.action.subscription_package_update = {
	'url' : '/cd/subscription_package_update',
	submit:'subscription_package_update',
	callback: function () {
		$(window).scrollTop($('#package-change').offset().top-60);
	},
}
/*
cura.action.subscription_package_commit = {
	'url' : '/cd/subscription_package_commit',
	submit:'subscription_package_commit'	
}
*/
cura.action.subscription_paymentmethod = {
	url: '/cd/subscription_paymentmethod',	
	loading: 'payment-change',
	mask:false	
}

cura.action.renewal_method_update = {
	url: '/cd/renewal_method_update',
	submit: 'renewal_method_update'
}


cura.action.add_payment_method = {
	url: '/cd/payment_method_add'	
}

cura.action.payment_method_update = {
	url : '/cd/payment_method_update',
	submit: 'new_payment_method',
	callback : function (dt, e){
		
	},
	validate: {"rules":{"payment_card_num":{"required":true,"creditcard":true},"payment_card_cvv":{"required":true,"digits":true}},"messages":{"payment_card_num":{"required":"Please enter card number","creditcard":"Invalid card number"},"payment_card_cvv":{"required":"Please enter CVV","digits":"Invalid CVV"}}}
}

cura.action.refresh_payment_methods = {
	callback : function (dt, e, res) {
		closeDialog();
		if($('#subscription_paymentmethod').length > 0) {
			$('#subscription_paymentmethod').click();
		}
	}
}

cura.action.subscription_cancel = {
	url: '/cd/subscription_cancel',
	confirm: 'Are you sure you want to cancel this subscription?<br><br>',
	confirm_button:  'Yes, Cancel Subscription'	
}

cura.action.subscription_close = {
	url: '/cd/subscription_close',
}

cura.action.payment_history = {
	url: '/cd/plan_payment_history'
	
}

cura.action.switch_work_hours = {
	url: '/cd/switch_working_schedule',
	callback: function(dt, e){
		
		if($('#limited_hours_1')[0].checked){
			$('#week_schedule').css({display:'block'});	
		}
		else{
			$('#week_schedule').css({display:'none'});		
		}
	},
	mask:false
}

cura.action.working_time_edit = {
	callback : function (dt, e) {
		
		$(e).parents('div:eq(0)').find('input').css({display:'block'})[0].focus();	
		$(e).css({display:'none'});
	}
}

cura.action.working_time_updated = {
	url: '/cd/working_time_update',
	callback : function (dt, e, val) {
		
		dt.name = e.name;
		dt.value = val;
		
		if(val && val.indexOf(':') > 0) {
			$(e).parents('div:eq(0)').find('a:eq(0)').css({display:''}).html($(e).val());	
				
		}
		else{
			$(e).parents('div:eq(0)').find('a:eq(0)').css({display:''}).html('NA');
		}
		$(e).css({display:'none'});
		
		delete dt.changed;
		delete dt.close;
	},
	mask:false
}

cura.action.working_day_switch = {
	url: '/cd/switch_working_days',
	callback : function (dt, e) {
		
		dt.id = e.name;
					
		if(e.checked){
			$(e).parents('div.list-select').removeClass('off-day');
			dt.working = 1;
		}
		else{
			$(e).parents('div.list-select').addClass('off-day');
			dt.working = 0;
		}
	},
	mask:false
}

cura.action.handler_edit = {
	url: '/cd/handler_edit'
}

cura.action.handler_update = {
	url: '/cd/handler_update',
	submit:'handler_update',
	callback : function () {
		
		$('#_server_error').html('');
		var val = $('#page_handle').val();
		
		if(val && !isNaN(val+1)){
			$('#_server_error').html('<div class="_server_error">Handle should have at least one non-numeric character.</div>');
			return false;	
		}
		
	},
	validate: {"rules":{"page_handle":{"required":true, 'minlength':5, 'maxlength' : 40}},"messages":{"page_handle":{"required":"Please enter a handle", 'minlength':'Handle should have at least 5 characters', 'maxlength' : 'Handle should not exceed 40 characters'}}},
}

cura.action.handler_cleanup = {
	callback: function (dt, e) {
		e.value = e.value.replace(' ','-').replace(/[\+\!\#\$\~%\^\&\*\(\)\{\}\[\]\'\"\|\/\<\>\,`\=\\@\:\;\?]/g,'');
	}
}

cura.action.talent_request =  {
	url:'/ad/talent_request',
	loading:'talent_request',
	mask:false	
}

cura.action.talent_request_find = {
	url: '/ad/talent_request_find',
	submit: 'talent_request_find',
	loading:'talent_results',
	callback : function () {		
		$('#_server_error').html('');		
	},
	message: 'Searching...',
	validate: {"rules":{"name_handle":{"required":true,  'maxlength' : 40}},"messages":{"name_handle":{"required":"Please enter talent name or handle", 'maxlength' : 'Handle should not exceed 40 characters'}}},	
}

cura.action.talent_request_access = {
	url: '/ad/talent_request_access',
	callback: function(dt, e){
		$('#talent_request_'+dt.id).html('<div class="inline-loader"></div>');
	},
	message: 'Requesting...',
	mask:false
}

cura.action.talent_request_cancel = {
	url : '/ad/talent_request_cancel',
	callback: function(dt, e){
		$('#talent_request_'+dt.id).html('<div class="inline-loader"></div>');
	},
	message: 'Canceling...',
	mask:false	
}

cura.action.talent_remove_access = {
	url:'/ad/talent_remove_access',
	callback: function(dt, e){
		
		if(!$('#talent_'+dt.talent).hasClass('selected-block')){
			$('.dash-info-row').removeClass('selected-block');	
			$('#talent_'+dt.talent).addClass('selected-block')
		}
		
		$('#talent_manage_'+dt.talent).html('<div class="inline-loader"></div>');		
	},
	mask:false	
}
cura.action.talent_remove_access2 = {
	url:'/cd/talent_remove_access',
	confirm: "Are you sure you want to remove this talent?",
		
}

cura.action.agency_request_respond = {
	url:'/ad/agency_request_respond',
	callback: function(dt, e){
		$('.inline_load').html('');
		$('#agency_request__'+dt.id).html('<div class="inline-loader"></div>');		
	},
	mask:false	
	
}

cura.action.agency_request_accept = {
	callback: function(dt, e){
		
		if($('input.additional_permission:checked').length > 0) {
			
			new Dialog( {
				title: 'Confirm',
				message : '<div> You may want to sign  a contract with the agency. Contact them if you don\'t have a contract.<div class="sep"></div>Are you sure your want to grant additional permissions for the agency? </div>',
				mask : true,
				width: 400,
				data:dt,
				buttons: {
						ok : {label : 'Yes, Accept Request', 'class' : '', action: 'agency_request_accept_confirm'},
						cancel : {label : 'Cancel', 'class' : 'button-alt'}
					}
			});
		
		}
		else{
			cura.execute('agency_request_accept_confirm', e);	
		}
	}
}

cura.action.agency_request_accept_confirm = {
	url:'/ad/agency_request_accept',
	submit:'agency_request_accept',
	callback: function(dt, e, p){
		dt = p;
		closeDialog();
	},
	mask:false		
}

cura.action.agency_request_reject = {
	url:'/ad/agency_request_reject',
	submit:'agency_request_reject',
	mask:false		
}

cura.action.agency_pending_requests = {
	url:'/ad/agency_pending_requests'
}

cura.action.sync_agency_permission_model = {
	callback : function (dt, e){
		
		if(e.id == 'manage_bookings'){
			
			if( e.checked) {
				$('#view_booking_detail')[0].checked = true;
				$('#view_bookings')[0].checked = true;
			}					
		}
		
		if(e.id == 'view_booking_detail' ){
			if( e.checked) {
				$('#view_bookings')[0].checked = true;
			}
			else{
				$('#manage_bookings')[0].checked = false;	
			}
		}
		
		if(e.id == 'view_bookings' ){
			if(!e.checked) {
				$('#view_booking_detail')[0].checked = false;
				$('#manage_bookings')[0].checked = false;
			}
		}
		
	}
}


cura.action.agency_access_manage = {
	url:'/ad/agency_access_manage',
	callback: function(dt, e){
		$('.inline_load').html('');
		$('#agency_access_'+dt.id).html('<div class="inline-loader"></div>');		
	},
	mask:false	
}

cura.action.agency_access_update = {
	url: '/ad/agency_access_update',
	submit: 'agency_access_update'
}

cura.action.agency_access_remove = {
	url: '/ad/agency_access_remove',
	confirm: 'Are you sure you want to remove this Agency?',
	confirm_button : 'Yes, Remove'
}

cura.action.agency_talent = {
	url : '/ad/agency_talent',
	callback: function (dt, e){
		
		if($('#filter_date').length > 0){
			dt.date = 	$('#filter_date').val();
		}
		
		if(!$('#talent_'+dt.id).hasClass('selected-block')){
			$('.dash-info-row').removeClass('selected-block');	
			$('#talent_'+dt.id).addClass('selected-block')
		}
		
		$('.inline_load').html('');
		$('#talent_manage_'+dt.id).html('<div class="inline-loader"></div>');
		
		
	},
	mask:false
}

cura.action.agency_appointment_update = {
	url: '/ad/agency_appointment_update',
	submit: 'agency_appointment_update',
	validate: {"rules":{"appointment_begins_date":{"required":true},"appointment_begins_time":{"required":true},"appointment_duration":{"required":true},"appointment_description":{"required":true}},"messages":{"appointment_begins_date":{"required":"Please select a date"},"appointment_begins_time":{"required":"Please select a time"},"appointment_duration":{"required":"Please enter duration"},"appointment_description":{"required":"Please add a title"}}}	
}

cura.action.agency_appointment_remove = {
	url: '/ad/agency_appointment_remove',
	confirm: 'Are you sure you want to remove appointment?'
}

cura.action.agency_booking_update = {
	url:'/ad/agency_booking_update',	
	submit : 'agency_booking_update',
	validate: {"rules":{"b_date":{"required":true},"b_time":{"required":true},"b_place":{"required":true},"session_id":{"required":true}},"messages":{"b_date":{"required":"B Date is required"},"b_time":{"required":"B Time is required"},"b_place":{"required":"B Place is required"},"session_id":{"required":"Session Id is required"}}}
}



cura.action.agency_booking_view = {
	url:'/ad/agency_booking_view',		
}

cura.action.attach_event = {
	url:'/cd/agency_event_attach',
	menu: {'new': 'Create new event', 'existing' : 'Select existing event'}
}

cura.action.switch_event_list = {
	callback : function (dt, e) {
	
		if($(e).val() == '0'){
			$('#events_month').css({display:'block'});
		}
		else{
			$('#events_month').css({display:'none'});
		}
		
		cura.execute('event_search', dt, e);
	}
}

cura.action.event_search = {
	url: '/ev/event_search',
	submit: 'event_search',
	mask:false,
	message: 'Searching...'	
}

cura.action.event_edit = {
	url: '/ev/event_edit'	
}

cura.action.event_update = {
	url:'/ev/event_update',
	callback:function (dt, e) {
		
		if(cura.location && cura.location.place_id){
			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
			
		}
		else if ($('#event_venue').data('place')) {
			// already selected
			return true;
		}
		else{
			notify('Please select a location');
			return false;	
		}
		
	},
	submit: 'event_update',
	validate:{"rules":{"event_name":{"required":true},"event_event_type":{"required":true},"event_venue":{"required":true},"event_begins_date":{"required":true},"event_begins_time":{"required":true},"event_budget":{"number":true},"event_num_attendees":{"digits":true}},"messages":{"event_name":{"required":"Please enter event name"},"event_event_type":{"required":"Selct type of the event"},"event_venue":{"required":"Please select event venue"},"event_begins_date":{"required":"Select event date"},"event_begins_time":{"required":"Select begin time"},"event_budget":{"number":"Invalid amount"},"event_num_attendees":{"digits":"Invalid number"}}}
}

cura.action.event_view = {
	url:'/ev/event_view'
}

cura.action.event_item_update = {
	url:'/ev/event_item_update',
	submit:'event_item_update',
	validate:{"rules":{"item_begins_time":{"required":true},"item_ends_time":{"required":true},"item_title":{"required":true}},"messages":{"item_begins_time":{"required":"Please select begin time"},"item_ends_time":{"required":"Please select an end time"},"item_title":{"required":"Pleae enter a title/name"}}}
}

cura.action.agenda_option = {
	url:'/ev/agenda_option',
	menu:{'edit':'Edit','delete':'Delete'},
	callback: function (dt, e){
		hideContextMenu();
		
		if(dt.action == 'delete'){
			cura.action.agenda_option.confirm = 'Are you sure you want to delete this item?';
		}
		else{
			cura.action.agenda_option.confirm = null;
		}
	}
}

cura.action.agenda_share = {
	url: '/ev/agenda_share',
	loading:'agenda_share',
	mask:false
}

cura.action.agenda_share_update = {
	url: '/ev/agenda_share_update',
	submit:'agenda_share_update'
}

cura.action.agenda_share_cancel = {
	url:'/ev/agenda_share_cancel',
	loading:'agenda_share',
	mask:false,
}

cura.action.agency_talent_select = {
	url:'/ad/agency_talent_select',
	mask:false
}

cura.action.agency_event_booking = {
	url:'/ev/agency_event_booking',
	mask:false	
}

cura.action.event_booking_update = {
	url:'/ev/event_booking_update',
	submit : 'agency_booking_update',
	validate: {"rules":{"b_date":{"required":true},"b_time":{"required":true},"b_place":{"required":true},"session_id":{"required":true}},"messages":{"b_date":{"required":"B Date is required"},"b_time":{"required":"B Time is required"},"b_place":{"required":"B Place is required"},"session_id":{"required":"Session Id is required"}}}	
}

cura.action.switch_appointment_list = {
	callback : function (dt, e) {
	
		if($(e).val() == '0'){
			$('#appointment_day').css({display:'block'});
		}
		else{
			$('#appointment_day').css({display:'none'});
		}
		
		cura.execute('agency_talent_appointments', dt, e);
	}
}

cura.action.agency_talent_appointments = {
	url:'/ad/agency_talent_appointments',
	submit:'appointment-search',
	mask:false
}


cura.action.agency_event_appointment = {
	url:'/ad/agency_event_appointment',
	mask:false	
}

cura.action.browser_events = {
	url: '/ev/browser_events',	
	mask:false	
}


cura.action.clear_event = {
	callback: function(dt, e){
		$('#event_list').html('<div class="vpadding"><a href="javascript:void(0)" cura="browser_events">Add an event</a></div>');
		cura.bind();
	}
}

cura.action.get_event_block = {
	url: '/ev/get_event_block',
	mask:false	
}

cura.action.event_appointment_update = {
	url:'/ev/event_appointment_update',
	submit:	'agency_appointment_update',
	validate: {"rules":{"appointment_begins_date":{"required":true},"appointment_begins_time":{"required":true},"appointment_duration":{"required":true},"appointment_description":{"required":true}},"messages":{"appointment_begins_date":{"required":"Please select a date"},"appointment_begins_time":{"required":"Please select a time"},"appointment_duration":{"required":"Please enter duration"},"appointment_description":{"required":"Please add a title"}}}
		
}

cura.action.add_expence = {
	url:'/ev/expence_add',
	loading:'add_expences',
	mask:false	
}

cura.action.save_expence = {
	url:'/ev/expence_save',	
	submit:'expence_form',
	validate:{"rules":{"expence_type":{"required":true},"expence_amount":{"required":true,"number":true,"min":0}},"messages":{"expence_type":{"required":"Expence type is required"},"expence_amount":{"required":"Pleae enter amount","number":"Invalid amount","min":"Invalid amount"}}}
}

cura.action.expence_changed = {
	callback: function (dt, e, res) {
		$('#add_expences').html('');
		
		if(res.html){
			$('#expences').html(res.html);
			cura.bind();	
		}
		
		console.log(res);
	}
}

cura.action.expence_delete = {
	url : '/ev/expence_remove',
	confirm:'Do you want to remove this expence?',
	confirm_button: 'Remove',
	mask:false	
}


cura.action.setting_changed = {
	callback: function (dt, e){
		$('#setting_update input').change(function(){
			$('#setting_update .button-container').css({display:'flex'});
		});
	}
}

cura.action.setting_update = {
	url : '/cd/setting_update',
	submit:'setting_update'
}

cura.action.remove_device = {
	url : '/cd/device_remove',	
	confirm:'Are you sure you want to remove this device?',
}

cura.action.agenda_create = {
	url : '/ev/agenda_create'
}

cura.action.add_agenda_item = {
	url : '/ev/agenda_item_add'	
}

cura.action.agenda_linking = {
	url : '/ev/agenda_linking'		
	
}

cura.action.expand_block = {
	callback: function (dt, e ){
		
		if($(e).hasClass('expanded')){
			if(dt.id){
				$('#'+dt.id).slideUp('slow');
			}
			$(e).removeClass('expanded');
			
		}
		else{
			if(dt.id){
				$('#'+dt.id).slideDown('slow');
			}
			$(e).addClass('expanded');
		}		
	}
}

cura.action.packages_public = {
	url:'/cd/packages_public',
	callback: function (dt, e){
		dt.enable = e.checked ? 1 : 0;
	},
	mask:false
}

cura.action.simultaneous_change = {
	url:'/cd/simultaneous_change',
	loader:'simultaneous-change',
	mask:false,
}

cura.action.simultaneous_update = {
	url:'/cd/simultaneous_update',
	submit:'simultaneous_update',
	loader:'simultaneous-change',
	mask:false,
	validate: {"rules":{"simultaneous":{"required":true,"digits":true}},"messages":{"simultaneous":{"required":"Please enter number","digits":"Invalid number"}}}
}

cura.action.invoice_pay = {
	url:'/ipg/invoice_pay'	
}

cura.action.stripe_payment = {
	callback: function () {
		if(window['Stripe']){
			options = {};
			const elements = Stripe.elements(options);
			cura.action.stripe_payment.paymentElement = elements.create('card', {style: {base: {fontSize: '16px'}}});
			cura.action.stripe_payment.paymentElement.mount('#card-element');
		}
		else{
			new Dialog({title:'Error', 'message':'Payment processing error.<br>Please contact customer support'});
		}
	}
	
}

cura.action.invoice_process = {
	url:'/ipg/invoice_process',
	submit:'invoice_payment',
	callback: function () {
		
		if(!$('#card-element').hasClass('StripeElement--complete')){
			new Dialog({title:'Error', 'message':'Please enter your card detail'});
			return false;
		}
		
		if(cura.action.payment_confirm.intent){
			cura.execute('payment_confirm');
			return false;
		}
		
	}
}

cura.action.payment_confirm = {
	callback: function (dt, e, res){
		
		if(res && res.data && res.data.intent){
			console.log(res);
			cura.action.payment_confirm.intent = res.data.intent;
		}
		
		if(cura.action.payment_confirm.intent){
			
			$('#payment_form').slideUp(200);
			$('#process_loader').removeClass('hidden');
			$('#process_message').html('Your payment is processing...');
						
			Stripe.confirmCardPayment(cura.action.payment_confirm.intent,{
				payment_method : {card:cura.action.stripe_payment.paymentElement }				
			}).then( function (result){
				console.log(result);
				
				if(result && result.paymentIntent && result.paymentIntent.status == 'succeeded'){
					
					$('#process_message').html('Confirming payment...');
					
					let pd = {
						id : result.paymentIntent.id,
						secret: result.paymentIntent.client_secret,
						method: result.paymentIntent.payment_method
					};
					cura.util.call('/ipg/processed', pd, null, true).post();	
				}
				else{
					$('#payment_form').slideDown(200);
					$('#process_loader').addClass('hidden');
					new Dialog({title:'Error', 'message':'Payment processing error.<br>Please contact customer support'});
				}
				
				cura.action.payment_confirm.intent = null;
			}
			
			);
			
			
		}		
	}
}

cura.action.profile_verify = {
	callback: function(){
		new Dialog({title:'Profile verification', 'message':'We have not started profile verification process yet.<br>We\'ll let you know when it is available.' });
	}
}

cura.action.page_stat = {
	callback: function(){
		new Dialog({title:'Page Statistics', 'message':'This feature will be available soon.' });
	}
}

cura.action.profile_close = {
	url:'/cd/profile_closing',
	confirm:'Are you sure you want to delete profile?',
	confirm_button:'Delete'
}

cura.action.account_close = {
	url:'/cd/account_closing',
	confirm:'Are you sure you want to close your account?',
	confirm_button:'Close Account'
}

cura.action.profile_delete = {
	url:'/cd/profile_delete',
	confirm:'Are you really want to delete profile?',
	confirm_button:'Delete'
}

cura.action.account_delete = {
	url:'/cd/account_delete',
	confirm:'Are you really want to close your account?',
	confirm_button:'Close Account'
}

cura.action.tags_init = {
	callback: function () {
		$('#tag_search').autocomplete({
				source: base+'/tags/',
				minLength : 2,
				select: function(e, ui){
					
					e.preventDefault();
					$('#tag_search').val('');
					$('#no_tags').remove();

					if($('#tag'+ui.item.value).length == 0) {
						$('#tag_list').append('<div  id="tag_'+ui.item.label.replace(/\s/g,'')+'" class="tag" cura="context" data-action="tag_context" data-tag="'+ui.item.label+'">'+ui.item.label+'</div>');
						cura.bind();
						cura.util.call('/cd/tag_add', {tag: ui.item.label}, null, true).post();	
						
					}
					else{
						notify('Tag already added');
					}
					
					// save in server

			}
		});
	}
}

cura.action.tag_add = {
	callback : function (dt, e) {
		$('#no_tags').remove();
		$('#tag_list').append('<div  id="tag_'+dt.tag.replace(/\s/g,'')+'" class="tag" cura="context" data-action="tag_context" data-tag="'+dt.tag+'">'+dt.tag+'</div>');
		cura.bind();
		cura.util.call('/cd/tag_add', {tag: dt.tag}, null, true).post();
		$(e).fadeOut();
	}
}

cura.action.tag_context = {
	url:'/cd/tag_remove',
	menu: {search:'Search', 'delete':'Delete'},
	callback: function (dt, e) {
		
		if(dt.action == 'search'){
			window.open(base+'/search/tag/'+dt.tag);
			hideContextMenu();
			
			return false;
		}
		$('#tag_'+dt.tag.replace(/\s/g,'')).animate({width:0}, 400, function(){ $(this).remove()});
		hideContextMenu();
	},
	mask:false
}

cura.action.switch_profile_type = {
	callback:function (dt) {
		$('#'+dt.id+'_profile').slideDown(400);
		$('#profile_type').slideUp(400);
	}
}

/*************** Took from signup js **********************/
cura.action.submit_signup_profile = {
	url:'/sd/signup_profile',
	submit: 'signup_profile_form',
	callback: function() {
		
		if($('input.gender:checked').length === 0){
			$('#gender-error').css({display:'block'});
			
			$('input.gender').unbind().change(function(){
					$('#gender-error').css({display:'none'});
				})
			
			notify("Please select your gender");
			return false;	
		}		
			
	},
	
}

cura.action.submit_agency_profile = {
	url:'/sd/signup_agency',
	submit: 'signup_agency_form',
	callback: function(dt, e) {
		if(cura.location && cura.location.place_id){			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
		}
		else{
			notify('Please select a location');
			return false;	
		}
	},
	validate:{"rules":{"agency_name":{"required":true},"address":{"required":true},"office_city":{"required":true}},"messages":{"agency_name":{"required":"Please enter agency name"},"address":{"required":"Please enter block number or street name"},"office_city":{"required":"Please select nearest city"}}}
}


/*
{"rules":{"payment_card_name":{"required":true},"payment_card_num":{"required":true,"digits":true,"minlength":15,"maxlength":25,  normalizer:function(value){ return value.replace(/\s/g, '') } },"payment_card_cvv":{"required":true,"digits":true}},"messages":{"payment_card_name":{"required":"Please enter name on the card"},"payment_card_num":{"required":"Please enter your card number","digits":"Invalid card number","minlength":"Card number is too short","maxlength":"Card number is too long"},"payment_card_cvv":{"required":"Please enter 3 or 4 digit code at back of your card","digits":"Invalid CVV"}}}*/

/*************** function to validate date of birth *************/
function validateDOB () {
	
	var limits = {1:31, 2:29, 3:31, 4:30, 5:31, 6:30, 7:31, 8:31,9:30, 10:31,11:30,12:31};

	dd = parseInt($('#dob_day').val());
	mm = parseInt($('#dob_month').val());
	yy = parseInt($('#dob_year').val());
	
	if(mm != 2){
		return dd <= limits[mm];					
	}
	else{
		if(yy%4 == 0){
			return dd <= 29;	
		}
		else{
			return dd <= 28;	
		}
	}
}
