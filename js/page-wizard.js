
var wizard = {
	steps : [
	{id: null, html:'<div class="txt-m txt-bold">This is your public page</div><br>Wizard will guide you through step by step to set up your page.', label:'NEXT'},
	{id: 'publish-tools', html:'<div ><div  class="txt-m txt-bold">Page tool bar</div><div class="sep"></div><div class="flex"><img src="{base}/images/w-wand.png"><div class="gap"></div><div>Re-launch this wizard</div></div><div class="sep"></div><div class="flex"><img src="{base}/images/w-edit.png"><div class="gap"></div><div>Switch to editing mode</div></div><div class="sep"></div><div class="flex"><img src="{base}/images/w-rocket.png"><div class="gap"></div><div>Publish page</div></div><div class="sep"></div><div class="flex"><img src="{base}/images/w-close.png"><div class="gap"></div><div>Exit from editing mode</div></div></div>', label:'NEXT'},
	{id:'publish-tools', html:'Click on page edit button to update page elements. ', label:'NEXT'},
	{id: 'profile_changer', html:'', label:'NEXT'},
	{id:null, html:'<div class="txt-m txt-bold">You are in page edit mode</div><br>Lets start updating your page ', label:'NEXT'},
	{id: 'banner_changer', html:'You may change your page banner.<br>Leave it if you like it.', label:'NEXT'},
	{id: 'profile_changer', html:'Change your profile image', label:'NEXT'},
	{id: 'about_title', html:'Click to change title', label:'NEXT'},
	{id: 'about_text', html:'Click to change text', label:'NEXT'},
	{id: 'about_image', html:'Click to change the image', label:'NEXT'},
	{id: 'about_image', html:'This section is important as it will be shown when  someone going to make a booking', label:'NEXT'},
	{id: 'service_switch', html:'You may turn on/off some page sections according to your requirements', label:'NEXT'},
	{id: 'services_add', html:'You may also add more parts into your page', label:'NEXT'},
	{id: 'page_video_edit', html:'You can add videos from Youtube, Vimeo or Daily Motion sites.<br>Just copy and paste embeded codes here.', label:'NEXT'},
	{id: 'portfolio_add', html:'Add your portfolio items. Pictures, Stories or your projects, pretty much anything you want to showcase. ', label:'NEXT'},
	{id: 'social_link_add', html:'Add your all social pages, where people can learn about you and reach you.', label:'NEXT'},
	{id:'publish-tools', html:'When you updated your page, make sure to publish it<br>Your profile will not be listed in search results until your page has published.', label:'NEXT'},
	{id:null, html:'If you have any question, visit support section or shoot us an email<br><br>Wish you all the best!', label:'CLOSE'},
	],
	step : 0,
	width:300
};

function showWizard (step){
	
	btn = '<div class="sep"></div><div class="button-container nopadding"><div class="center"></div><a class="button" onClick="advanceWizard()" >'+step.label+'</a> </div>';
	
	$('#popup_text').html((step.html.replace(/\{base\}/g, base))+btn);
	
 	// position popup
	/* if there is enough space, show crrot in the middle, 
	otherwise , more it to left or right
	*/
	
	if(step.id) {
		
		
		
		var cnt = $('.container:eq(0)');
		var target = $('#'+step.id);
		
		if(step.id && target.length == 0){
			$('#popup').fadeOut('fast');
			return;	
		}
		
		var t_offset = target.offset();
		var t_width = target.outerWidth();
		var t_height = target.outerHeight();
		var c_offset = cnt.offset();
		var c_width = cnt.width();
		
		var x = t_offset.left + Math.round(t_width/2) - c_offset.left;
		var y = t_offset.top + t_height +5;
		var sc = $(window).scrollTop();
		var wh = $(window).height();
		
		if(step.id == 'publish-tools'){
			
			$('#anchor').css({left:20}).css({display:'block'});
			$('#popup').animate({left:20, top:120});
			$(window).scrollTop(1);	
			
			return;
		}
		
		if(x < wizard.width/2){
			
			$('#anchor').css({left:x-8}).css({display:'block'});
			x = 0;	
		}
		else if(x > c_width -(wizard.width/2)){
			$('#anchor').css({left:(x - (c_width - wizard.width)-8)}).css({display:'block'});
			x = c_width - wizard.width;
		}
		else{
			$('#anchor').css({left:(wizard.width/2)-8}).css({display:'block'});;
			x -= Math.round(wizard.width/2);
		}
		
		$('#popup').animate({left:(x+c_offset.left), top:y});
		
		
		if(sc == 0 && y < (wh/3)*2){
			// nothing to scroll
			delta = 0;
		}
		else{
			delta = y - (wh/3)*2;
		}
		
		$(window).scrollTop(delta);	
	}
	else{
		
		$('#anchor').css({display:'none'});
		var sc = $(window).scrollTop();
		var wh = $(window).height();
		var ww = $(window).width();
		var ph = $('#popup').height();
		
		var y = sc + Math.round((wh - ph)/2) - 15;
		var x = Math.round((ww - wizard.width)/2);
		$('#popup').css({display:'block'}).animate({left:x, top:y});
		
	}
}

function advanceWizard (){
	
	if(window['wizardStep'+wizard.step]){
		window['wizardStep'+wizard.step]();
	}
	
	wizard.step++;
	
	if(wizard.steps.length > wizard.step) {
	
		showWizard(wizard.steps[wizard.step]);
	
	}
	else{
		$('#popup').fadeOut();
	}
}

$(document).ready(function(e) {
	
	if(auto_show_wizard) {
		if(edit_mode) {
			wizard.step = 4;
			showWizard(wizard.steps[4]);
		}
		else {
			wizard.step = 0;
   	 		showWizard(wizard.steps[0]);
		}
	}
	$('.popup-close').click(function(){$('#popup').fadeOut();});
});

cura.action.show_wizard = {
	callback: function() {
		if(edit_mode) {
			wizard.step = 4;
			showWizard(wizard.steps[4]);
		}
		else {
			wizard.step = 0;
   	 		showWizard(wizard.steps[0]);
		}
	}
}