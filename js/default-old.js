/* Talent Market Functions */
var mapService;
var autocomplete; 

$(document).ready(function(e) {
    
addPageListeners();
	
	
	
});

function addPageListeners () {
	bindFormFields();	
	initWidgets();
	initContextMenu();
	initPlaceSearch();
}

function bindFormFields () {
	
	$('.field-wrapper input, .field-wrapper select, .field-wrapper textarea').off('focus',moveLabelAway).on('focus',moveLabelAway)
	
	$.each($('.field-wrapper input:last-of-type, .field-wrapper select:last-of-type, .field-wrapper textarea'), function(){
		if($(this).val()){
			$(this).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		
		})
	
	
	
	$('form').each(function(index, element) {
        
		if($(this).data('submit')){
			$(this).off('submit').on('submit', genFormHandle);
		}
		
		if($(this).data('init')){
			var cf = $(this).data('init');			
			try{
				
				if(window[fc]){
					window[cf](this);	
				}
				else{
					console.log(cf+ ' is not a valid function');
				}
			}
			catch(ex){
				console.log(cf+ ' is not a valid function');	
			}
		}
		
    });
}

function genFormHandle (){

	var cf = $(this).data('submit');
	try{
				
		if(window[cf]){
			return window[cf](this);	
		}
		else{
			console.log(cf+ ' is not a valid function');
			notify("Application error!", false);
		}
	}
	catch(ex){
		console.log(ex);
		notify("Application error!", false);	
	}
	
	return false;
}

function submitForm (form_id, block){

	is_valid = true;
	
	form = $('#'+form_id);
	var vld = form.data('validator');
	
	if(!is_form_valid(form_id)){
		return;	
	}
	
	action = form.attr('action');
	data = form.serialize();
	
	cl = new call(action, data, null, true);
	cl.post();
	
	if(block){
		block();
	}	
	
}


function is_form_valid (form_id) {
	
	is_valid = true;
	
	form = $('#'+form_id);
	var vld = form.data('validator');
	
	if(vld){
		
		var fields = form.find('input,select,textarea');
		
		for(var f = 0; f < fields.length; f++){
			
			
			try {
				if(fields[f].id) {
					if(!vld.element('#'+fields[f].id)){
						is_valid = false;	
					}
				}
			}
			catch(ex){
				console.log(fields[f]);
				console.log("Error found");
			}					
		}		
		
		
		if(!is_valid){
			notify('Please fix errors');	
			return false;
		}
		
		
	}
	
	return true;
	
}

function moveLabelAway () {
	
	$(this).off('focus', moveLabelAway);
	
	$(this).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
	
}

function showCustomOptions () {
	
	if($(this)[0].checked){
		$(this).parents('.custom-option:eq(0)').addClass('custom-option-selected');
	}
	else{
		$(this).parents('.custom-option:eq(0)').removeClass('custom-option-selected');
	}
}



/** Ajax request wrapper   **/

var Tcall = function (url, data, element, json, msg){
	
	// call status, -1 : failed,  0: not sent, 1: in progress, 2:completed
	this.status = 0;
	// url to call
	this.url = base+url;
	// request method
	this.method = 'GET';
	// data to be sent, if POST, post data else, URL parameters
	this.data = data;
	// is this call return json response
	this.json = json ? true : false;
	
	// success callback function
	this.success = null;
	// failur callback function
	this.fail = null;
	
	// DOM element to add data
	this.element = element;
	// whether add data or append 
	this.append = false;

	// message to show while loading
	this.message = msg;	
	// notification id
	this.msgid = null;
	
	// what to retry sending request
	this.retry = false;
	// number of attempts made to resend request
	this.attempts = 0;	
	
	 
}


Tcall.prototype.post = function () {
	
	this.method = 'POST';
	this.send();
}
Tcall.prototype.get = function () {
	this.method = 'GET';
	this.send();
}

Tcall.prototype.load = function () {
	this.method = this.data ? 'POST' : 'GET';
	this.send();
}

Tcall.prototype.send = function () {
	
	this.attempts++;
	
	if(this.success){
		callback = wrap(this, this.success);
	}
	else {
		callback = wrap(this, callComplete);	
	}
	
	if(this.fail){
		faliur = wrap2(this, this.fail);	
	}
	else{
		failur = wrap2(this, callError);	
	}
	
	o = {
		method: this.method,
		url: this.url,
		error: failur,
		success: callback	
	}
	
	if(this.json){
		o.dataType = 'json';
	}
	
	if(this.data){
		
		if(typeof this.data == 'object'){
			qs = [];
			for(k in this.data){
				qs.push(encodeURIComponent(k)+'='+encodeURIComponent(this.data[k]));	
			}
			o.data = qs.join('&');	
		}
		else{
			o.data = this.data;	
		}
	}
	
	
	if(this.message){
		this.msgid = notify(this.message);
	}
	
	$.ajax(o);

}


function call (url, data, element, json, msg) {
	return 	new Tcall(url, data, element, json, msg)
}

function callComplete (res, obj) {
	
	unblock();	
	
	if(obj.json){
		
		if(res.message){
			obj.msgid = notify(res.message, res.status);
		}
		else{
			clearNotify();	
		}
		
		if(res.trigger){
			
			if(res.trigger == 'alert') {
				new Dialog( {
					title: 'Alert',
					message : res.message,
					mask : true,
					buttons: {
							ok : {label : 'Ok'}
						}
				});	
			}
			else if(res.trigger == 'redirect') {
				window.location.href = base+res.data;
			}
			else {
				if(window[res.trigger]) {
					window[res.trigger](res, obj);
					//return;
				}
				else{
					console.log(res.trigger+' trigger undefined');	
				}
			}
		}
		
		if(res.html && obj.element){
							
			if(obj.append){
				$(obj.element).append(res.html);
			}
			else{
				$(obj.element).html(res.html);
			}
			
			addPageListeners();			
		}
		
		if(res.nodes){
			
			for(p = 0; p < res.nodes.length; p++){
				
				if(	res.nodes[p].id || res.nodes[p].class ){
					
					tele = res.nodes[p].class ? '.'+res.nodes[p].class : '#'+res.nodes[p].id;
					
					if(res.nodes[p].hasOwnProperty('append')){
						$(tele).append(res.nodes[p].append);	
					}
					
					if(res.nodes[p].hasOwnProperty('html')){
						$(tele).html(res.nodes[p].html);	
					}
					
					if(res.nodes[p].hasOwnProperty('value')){
						$(tele).val(res.nodes[p].value);	
					}
					
					if(res.nodes[p].options){
						ov = '';
						for(k in res.nodes[p].options){
							ov += '<option value="'+k+'">'+res.nodes[p].options[k]+'</option>';						}
						$(tele).html(ov);	
					}
					
					if(res.nodes[p].trigger && window[res.nodes[p].trigger]){
						window[res.nodes[p].trigger](res, obj);
					}
				}
				else{
					console.log("Node id undefined");	
				}
			}			
			
			addPageListeners();
		}
		
		if(res.error){
			
			p = $('#'+res.error.id).parents('div.panel:eq(0)');
			
			if(!p.hasClass('active-panel')){
				$('.active-panel').css({display:'none'}).removeClass('active-panel');
				p.addClass('active-panel').fadeIn('fast');
			}
			
			$('#'+res.error.id+'_server_error').remove();
			r = $('#'+res.error.id).parents('.field-wrapper:eq(0)');			
			r.append('<div class="server_error" id="'+res.error.id+'_server_error'+'">'+res.error.message+'</div>');
			
			try {
				if(Signup){
					Signup.panel = p.data('id');	
				}
			}
			catch (e){}
			
			try {
				if(Register){
					Register.panel = p.data('id');	
				}
			}
			catch (e){}
		}
								
	}
	else{
		
		if(obj.element) {
			
			if(obj.append){
				$(obj.element).append(res);
			}
			else{
				$(obj.element).html(res);
			}
		}
		
	}
	
	
}

function callError (e, obj) {
	
	if(obj.msgid){
		notificationManager.remove(obj.msgid, true);
	}
	
	if(e == 'error'){
		obj.msgid = notify("Something went wrong. Please try again.", false);
	}
	else if(e == 'timeout'){
		obj.msgid = notify("Server not reachable. Check your internet connection.", false);
	}
	else if(e == "parsererror"){
		obj.msgid = notify("Oops! Something went wrong. Please try again.", false);	
	}
		
	if(obj.retry && obj.attempt < 3){
		obj.send();	
	}
	
	unblock();
	
}




function wrap (obj, callback){
	return function (res) {		
		var data = obj;		
		callback(res, data);			
	}	
}

function wrap2 (obj, callback){
	return function (a, b, c) {		
		var data = obj;		
		callback(b, data);			
	}	
}


/**************** Notification manager ********************/
var notificationManager = {i: 0, ns: {}};

notificationManager.isShow = function (id){
	return notificationManager.ns[id];	
}

notificationManager.add = function (nf) {	
	notificationManager.ns[nf.id] = nf;	
}

notificationManager.remove = function (i, f) {
	if(notificationManager.ns[i]) {
		notificationManager.ns[i].close(f);
	}
}

notificationManager.clicked = function () {
	i = $(this).data('index');
	
	if(notificationManager.ns[i]){
		
		if(notificationManager.ns[i].callback){
			notificationManager.ns[i].callback();
		}
		
		notificationManager.ns[i].close();
	}
}

var notification  = function (msg, type, callback) {
	
	notificationManager.i++;
	
	this.callback = callback;
	this.id = notificationManager.i+'';
	
	ac = '';	
	
	if( type === true){
		ac = 'nfsuccess';
	}
	else if(type === false){
		ac = 'nferror';
	}	
	
	txt = '<div class="notification '+ac+'" id="n'+this.id+'" data-index="'+this.id+'">';
	txt += '<div class="notification-icon"></div>';
    txt += '<div class="notification-msg">'+msg+'</div>';
	txt += '</div>';
	
	$('#notifications').prepend(txt);
	
	$('#n'+this.id).click( notificationManager.clicked).css({display:'none'}).slideDown('fast');
	
	if(callback) {
	setTimeout('notificationManager.remove('+this.id+')', 8000);
	}
	else {
		setTimeout('notificationManager.remove('+this.id+')', 5000);
	}
	
}

notification.prototype.close = function (f) {
	
	delete notificationManager.ns[this.id];
	
	if(f) {
		$('#n'+this.id).remove();
	}
	else {
	
		$('#n'+this.id).slideUp('slow', function(){
			$(this).remove();
			
		});
	}
}


function notify (msg, type, callback) {
	
	nn =  new notification(msg, type, callback);
	notificationManager.add(nn); 
	return nn.id;
}

function block () {
	$('#blocker').fadeIn('fast');
}

function unblock () {
	$('#blocker').fadeOut('slow');
}

/*
function rp () {

	c = Math.floor(Math.random() * 100);
	
	if(c%3 == 0){
		a = null;	
	}
	else{
		a = c%2 == 0 ? true : false;	
	}
	
	notify("my "+c +" message", a);
	
}
*/

/************************* Time picker *****************************/

var TimePicker = function (jele) {
	
	this.active_class = 'tpa';
	
	uid = jele.id+'_time';
	
	$(jele).after ('<div class="timepicker" id="'+uid+'" data-id="'+jele.id+'" style="display:none"> <div class="tp-outer"> <div class="tp-left"><div class="tp-header">hours</div><div class="tp-wrapper tp-hour"><div class="tp-row"> <a data-val="1">1</a> <a data-val="2">2</a> <a data-val="3">3</a> <a data-val="4">4</a> </div><div class="tp-row"> <a data-val="5">5</a> <a data-val="6">6</a> <a data-val="7">7</a> <a data-val="8">8</a> </div><div class="tp-row"> <a data-val="9">9</a> <a data-val="10">10</a> <a data-val="11">11</a> <a data-val="12">12</a> </div></div><div class="tp-header">AM/PM</div><div class="tp-wrapper"><div class="tp-row tp-ampm"> <a data-val="AM">AM</a> <a data-val="PM" >PM</a> </div></div> </div> <div class="tp-right"><div class="tp-header">Minutes</div><div class="tp-row tp-minutes tp-wrapper"> <a data-val="00">:00</a> <a  data-val="15">:15</a> <a data-val="30">:30</a> <a data-val="45">:45</a> </div> </div> </div> </div>');
	
	$('.timepicker a').unbind().click(function (){
		pe = $(this).parents('div.timepicker:eq(0)');
		be = pe.data('id');
		
		$(this).parents('.tp-wrapper:eq(0)').find('a.selected').removeClass('selected');
		$(this).addClass('selected');
		
		sh = pe.find('.tp-hour a.selected');
		sm = pe.find('.tp-minutes a.selected');
		sa = pe.find('.tp-ampm a.selected');
		
		if(!pe.hasClass('focused')){

			pe.addClass('focused');
		}
		
		if(sh.length > 0 && sm.length > 0 && sa.length > 0){
			tpv = sh.data('val')+':'+sm.data('val')+' '+sa.data('val');	
			$('#'+be).val(tpv)[0].focus();
			cb = $('#'+be).data('changed');	
			if(cb){
				window[cb](tpv);
			}
			
			setTimeout("pe.removeClass('focused'); ",250);
			
		}
		
	});
	
	tv = $(jele).val();
	
	if(tv){
		// populate time
		tv = tv.split(/[: ]/);
		$('.tp-hour a').each(function(){
			if($(this).data('val') == tv[0]){
				$(this).addClass('selected');	
			}
		});	
		$('.tp-minutes a').each(function(){
			if($(this).data('val') == tv[1]){
				$(this).addClass('selected');	
			}
		});
		$('.tp-ampm a').each(function(){
			if($(this).data('val') == tv[2]){
				$(this).addClass('selected');	
			}
		});
	}
	
	$(jele).focus(function(){
		$('#'+this.id+'_time').css({display:'block'});	
	});
	
	$(jele).blur(function(){
		
		setTimeout("hideTimePicker('"+this.id+"')", 200)
	});
}



function initWidgets () {
	
	$.each($('.timepick'), function (){
		new TimePicker(this);
	});
	
	$.each($('.datepick'), function (){
		
		var eid = this.id;
		var fv = $(this).val();
		$(this).after('<div class="vanilla-calendar" id="'+eid+'_cal" data-id="'+eid+'"></div>');
						
		var mcl =  new VanillaCalendar({
		selector: '#'+eid+"_cal",
		pastDates: false,
		onSelect: function (dt, ele){
			
				dt = new Date(dt.date);

				var tid = $(ele).parents('div.vanilla-calendar:eq(0)').data('id');
				var dtxt =  ToDate(dt);
				$('#'+tid).val(dtxt);
				
				cb = $('#'+tid).data('changed');	
				if(cb){
					window[cb](dtxt);
				}
				$('#'+tid+'_cal').css({display:'none'});	
			}
		});
		
		
		
		$(this).focus(function () {
			$('#'+eid+"_cal").css({display:'block'}).removeClass('focused');	
		});
		$(this).blur(function () {
			
			//if(this.value || this.value != ''){
				setTimeout("hideCalendar('"+this.id+"')", 200);	
			//}
		});
		
		$('#'+eid+"_cal").click(function () {
			if(!$(this).hasClass('focused')){
				$(this).addClass('focused');	
			}
		});
		
		
			
			//mcl.set({date: strToDate(fv)});	
			$('#'+eid+"_cal").css({display:'none'});
		
	
	});
	
	$('.custom-option input').off('change', showCustomOptions).on('change', showCustomOptions);	
	$.each($('.custom-option input'), showCustomOptions);
	
	$('.inline-edit').each(function(index, element) {
		
		if($(this).hasClass('-inline-edit')){
			return;	
		}
        		
		$(this).addClass('-inline-edit')
		
		$(this).find('.field-holder').append('<div class="sep"></div><div class="button-container"> <div class="center" > <a class="button btn-next" href="javascript:void(0)" onClick="update_inline(this)">DONE</a> <div class="gap"></div>    <a class="button button-alt" href="javascript:void(0)" onClick="cancel_inline(this)">CANCEL</a> </div> </div>');
		
		$(this).find('.edit-trigger').click(function(){
			
			pn = $(this).parents('div.inline-edit:eq(0)')
			pn.addClass('is-editing');
			pn.find('input')[0].focus();
		});
		
    });
	
}

function hideCalendar (cid) {
	
	if(!$('#'+cid+'_cal').hasClass('focused')){
		$('#'+cid+'_cal').css({display:'none'});
	}
}

function hideTimePicker (tid) {

	if(!$('#'+tid+'_time').hasClass('focused')){
		$('#'+tid+'_time').css({display:'none'});
	}	
}

function cancel_inline (e) {
	pn = $(e).parents('div.inline-edit:eq(0)');
	pn.removeClass('is-editing');
}

function update_inline (e) {
	
	pn = $(e).parents('div.inline-edit:eq(0)');
	inp = pn.find('.field-holder input');
	val = '';
	label = '';
	name = '';
	
	if(inp.length == 1){
		val = inp.eq(0).val();
		name = inp[0].name;
		label = val;	
	}
	else{
		inp = pn.find('.field-holder input:checked');	
		
		if(inp.length == 1){
			val = inp.eq(0).val();
			name = inp[0].name;
			label = inp.eq(0).data('value');		
		}
		else{
			val = [];
			name = inp[0].name;
			for(i = 0; i < inp.length; i++){
				val[i] = inp.eq(i).val();
				label += inp.eq(i).data('value')+'<br>';
			}				
		}
	}
	
	ph = pn.find('.place-holder:eq(0)');

	
	dt = pn.data();
	
	if(dt.submit){
		
		window[dt.submit](val, dt, pn);
		
	}
	else{	

		pd = {
			id: dt.id,
			name:name,
			value: val
		}
		
		cl = new call(dt.url, pd, null, true, "Updating..." );
		cl.html = ph.html();
		cl.ele = pn;
		cl.post();
	}
	
	pn.find('.place-holder').html(label);
	
	cancel_inline(e);
}


function ToDate (dt){
	
	var cm = ['Jan', 'Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var cd = ['Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	
	return cd[dt.getDay()]+' '+dt.getDate()+' '+cm[dt.getMonth()]+' '+dt.getFullYear();
}

function strToDate (str) {
	
	var cm = ['Jan', 'Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	
	var dp = str.split(' ');
	mi = cm.indexOf(dp[2]);

	return new Date(dp[3], mi, dp[1]);
}


var manualbuffer = function  (c_id, b_id, t, key) {
	
	this.c = c_id;
	this.b = b_id;
	this.key = key;
	this.t = t;
	this.page = 1;
	this.mid;
	this.prg = false;
	
	$('#'+this.b).click(wrap (this, function(e, o){
		
		if(o.prg){
			return;	
		}
		o.prg = true;
		
		if($('#'+o.b).data('theme') == 'orange'){
			$('#'+o.b).addClass('button-loading-orange').html('&nbsp;');	
		}
		else {
			$('#'+o.b).addClass('button-loading-blue').html('&nbsp;');	
		}
		
		o.page++;
		bc = call('/sd/bff','key='+encodeURIComponent(o.key)+'&page='+o.page, null, true, "Loading..");
		bc.success = wrap(o, function (res, o) {
			$('#'+o.c).append(res.html);

			if(res.data.loaded >= o.t){
				$('#'+o.b).css({display: 'none'});
			}
			else {
				$('#'+o.b).removeClass('loading').html($('#'+o.b).data('label'));
			}
			
			o.page = res.data.page;
			if(o.mid){
				notificationManager.remove(o.mid, true);
			}
			o.prg = false;
		});
		bc.post();
		o.mid = bc.msgid;
	}));
		
	
}

/******************** Context Menu ********************/
function initContextMenu () {
	
	cmenus = $('.context-menu');
	if(cmenus.length > 0){
		
		cmenus.each( function() {
			
			if(!$(this).hasClass('ctx')){
				new ContextMenu(this);
			}	
		});
	
		$(document.body).append('<div id="contextmenu"></div>');
	
	}
	
	
}

var ContextMenu = function (ele) {
	
	var dt = $(ele).data();
	
	this.id = dt.id;
	this.show = false;
	this.menu = dt.menu;
	this.callback = dt.callback;

	$(ele).addClass('ctx');
	$(ele).click(this, function (e){
		e.stopPropagation();
		obj = window[e.data.menu];
		os  = $(this).offset();
		
		var cx = '';
		for(k in obj){
			
			cx += '<a href="javascript:void(0)" data-value="'+k+'" >'+obj[k]+'</a>';
			
		}
		
		$('#contextmenu').html(cx).css({display:'block'});
		$('#contextmenu a').click(e.data.id, window[e.data.callback]);
		
		$('#contextmenu').css({top:os.top+32, left:os.left-$('#contextmenu').width()+32});
		setTimeout("$(document.body).on('click', hideContextMenu)",200);
	});
	
}

function hideContextMenu () {
	$(document.body).off('click', hideContextMenu);
	$('#contextmenu').css({display:'none'});
}

/**
new Dialog( {
	title: 'Confirm',
	message : 'Are you sure?',
	mask : true,
	width: 300,
	buttons: {
			ok : {label : 'Ok', 'class' : '', callback: runCommand},
			cancel : {label : 'Cancel', 'class' : '', callback: closeDialog}
		}
});
 
	callback ( value, data, dialog);
*/

var Dialog = function (opt) {
	
	this.id = Math.round(Math.random()*100000);
	this.title = (opt && opt.title) ? opt.title : '&nbsp;';
	this.message = (opt && opt.message) ? opt.message : '&nbsp;';
	this.mask = (opt && opt.mask) ? true : false;
	this.width = (opt && opt.width) ? opt.width : null;
	this.height = (opt && opt.height) ? opt.height : 'auto';
	this.data = (opt && opt.data) ? opt.data : {};
	
	this.close = function(){
		
		if(this.mask){
			$('#mask').fadeOut('fast');
		}
		
		$('#d'+this.id).remove();
	}
	
	this.buttons = (opt && opt.buttons) ? opt.buttons : {'ok' : {label : 'Ok', 'class' : '', callback :null}};
	
	// render
	if(this.mask){
		if($('#mask').length == 0){
			$(document.body).append('<div id="mask" class="mask"></div>');
			
		}
		$('#mask').fadeIn('fast');
	}
	
	dc = '<div class="dialog" id="d'+this.id+'" '+(this.width ? 'style="width:'+this.width+'px"' : '')+'>';
	dc += '<div class="dialog-header"><div class="dialog-title">'+this.title+'</div><a href="javascript:void(0)" class="dialog-close" >X</a></div>';
    dc += '<div class="dialog-body">'+this.message+'</div>';
    dc += '<div class="dialog-buttons">';
	
	for(b in this.buttons){
		dc += '<a class="button '+(this.buttons[b].class ? this.buttons[b].class : '')+'" data-value="'+b+'" id="d'+this.id+'_'+b+'">'+this.buttons[b].label+'</a>';
	}
	dc += '</div></div>';
	
	$(document.body).append(dc);
	
	$('#d'+this.id+' a.dialog-close').click(closeDialog);
	
	for(b in this.buttons){
		$('#d'+this.id+'_'+b).click(this, function(e){
			dv = $(this).data('value');
			cb = e.data.buttons[dv].callback;
			if(cb) {
				cb(dv, e.data.data, e.data);
			}
			else {
				closeDialog();	
			}
		});
	}
	
	this.position = function () {
		
		ww = $(window).width();
		wh = $(window).height();
		dw = (this.width) ? this.width : $('#d'+this.id).width();
		dh = $('#d'+this.id).height();
		
		if(dw > ww){
			dw = ww;	
		}
		
		if(dh > wh){
			dh = wh;
			$('#d'+this.id+' .dialog-body').css({overflow:'auto'});	
		}
		dt = Math.round((wh-dh)/2);
		dt = dt > 50 ? dt -50 : dt;
		$('#d'+this.id).css({top: dt, left:Math.round((ww-dw)/2), width:dw, height:dh });
	}
	
	$(window).resize(this, function(e){
		e.data.position();
	});
	this.position();
	DialogManager.add(this);
}


var DialogManager = { dialogs : []};

DialogManager.add = function (dlg) {
	DialogManager.dialogs.push(dlg);
}

DialogManager.get = function () {
	
	if(DialogManager.dialogs.length > 0){
		return DialogManager.dialogs.pop();	
	}
	
	return null;
}
DialogManager.getAll = function () {
	return 	DialogManager.dialogs;
}

DialogManager.close = function () {
	
	if(DialogManager.dialogs.length > 0){
		dlg = DialogManager.dialogs.pop();
		dlg.close();	
	}
	
	
}

DialogManager.closeAll = function () {
	
	if(DialogManager.dialogs.length > 0){
		for(var i = 0; i < DialogManager.dialogs.length; i++) {
			dlg = DialogManager.dialogs.pop();
			dlg.close();
		}
	}
}

function closeDialog () {
	DialogManager.close();
}

/**************** Place search *******************/

function initPlaceSearch () {

	$('.addresspick').each(function(index, element) {
		
		mapService = new google.maps.places.Autocomplete(this, {});
		google.maps.event.addListener(mapService, 'place_changed', function() {
			place = mapService.getPlace();
			console.log(place);
			address = AddressPicker.parseAddress(place);
			console.log(address);
			AddressPicker.populate(address);
		});
		
		
    });
	console.log('attached');

	

}


var AddressPicker = {};

AddressPicker.parseAddress = function ( address ) {
		
		var comp = {'place' : address.name};
		
		for (c = 0; c < address.address_components.length ; c++){
		
			var ac = address.address_components[c];
			
					
			if(ac['types'][0] == 'route'){
				comp['route'] = ac.long_name;
			}
			else if(ac['types'][0] == 'neighborhood'){
				comp['neighborhood'] = ac.long_name;
			}
			else if(ac['types'][0] == 'political'){
				comp['political'] = ac.long_name;
			}
			else if(ac['types'][0] == 'locality'){
				comp['locality'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_3'){
				comp['subcity'] = ac.long_name;
			}
			else if(ac['types'][0] == 'postal_code'){
				comp['postal'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_2'){
				comp['city'] = ac.long_name;
			}
			else if(ac['types'][0] == 'administrative_area_level_1'){
				comp['region'] = (/^[a-zA-Z0-9 \.]+$/.test(ac.short_name)) ? ac.short_name : ac.long_name ;
			}
			else if(ac['types'][0] == 'country'){
				comp['country'] = ac.short_name;
			}
			else if(ac['types'][0] == 'establishment'){
				comp['place'] = (/^[a-zA-Z0-9 \.]+$/.test(ac.short_name)) ? ac.short_name : ac.long_name;
			}
		
		}
		
		// compose address
		comp['address'] = [];
		/*if(comp['place']){
			comp['address'].push(comp['place']);
			delete comp['place'];
		}*/
		if(comp['route']){
			comp['address'].push(comp['route']);
			delete comp['route'];
		}
		if(comp['neighborhood']){
			comp['address'].push(comp['neighborhood']);
			delete comp['neighborhood'];
		}
		if(comp['locality']){
			if(comp['city'] != comp['locality']) {
				comp['address'].push(comp['locality']);
			}
			delete comp['locality'];
		}
		if(comp['political']){
			if(comp['city'] != comp['political']) {
				comp['address'].push(comp['political']);
			}
			delete comp['political'];
		}
		if(comp['subcity']){
			if(comp['subcity'] != comp['city']) {
				comp['address'].push(comp['subcity']);
			}
			delete comp['subcity'];
		}
		
		comp['address'] = comp['address'].join(', ');
		
		if((!comp['city'] || comp['city'] == '') && (comp['region'] && comp['region'] != '')) {
			comp['city'] = comp['region'];
			comp['region'] = '';
		}
		
		return comp;
		
	}

AddressPicker.populate = function (dt){
	
		if(dt.place) {
			$('._place').val(dt.place).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._place').val('');	
		}
		if(dt.address) {
			$('._address').val(dt.address).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._address').val('');	
		}
		if(dt.city) {
			$('._city').val(dt.city).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._address').val('');
		}
		if(dt.region) {
			$('._region').val(dt.region).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._region').val('');	
		}
		if(dt.postal) {
			$('._postal').val(dt.postal).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._postal').val('');	
		}
		
		if(dt.country) {
			$('._country').val(dt.country).parents('.field-wrapper:eq(0)').find('label.dynamic-label').addClass('dynamic-label-fold');
		}
		else{
			$('._country').val('');	
		}
				
}
	
	




function AddressCall (res, obj){
			console.log(res);
			console.log(obj);
	if(res && res.results && res.results.length > 0){
		obj.setValues(res.results);
	}
	else{
		// close autofill
		obj.clear();
	}
	
}
	
