/*********************** Seach talent and booking related function ************/

cura.action.search_talent = {
	url:'/sd/search_talents',
	callback: function(dt, e){
		$('#list_id').val('');
		dt.width = $(window).width();
		
		if(cura.location && cura.location.place_id){			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
		}
		else{
			$('#b_place').val('');	
		}
		
		cura.action.paging_talent.page = 2;
	},
	submit:'search_form',
	message: 'Searching...',
	mask:false	
}

cura.action.paging_talent = {
	url:'/sd/search_talents',
	page: 1,
	busy: false,
	data:{},
	callback: function(dt, e){
	
		if(cura.action.paging_talent.busy){
			return false;	
		}
		
		dt.width = $(window).width();
		dt.page = cura.action.paging_talent.page;
		cura.action.paging_talent.busy = true;
		
		
	},
	submit:'search_form',
	message: false,
	mask:false	
}

cura.action.explore_list = {
	url:'/sd/search_talents',
	submit:'search_form',
	callback:function(dt, e) {
		cura.action.paging_talent.page = 2;
		$('#list_id').val(dt.list);
		$(window).scrollTop($('.talent-search-bar').offset().top-40);
	},
	message: 'Loading...',
	mask:false	
}

cura.action.paging_done = {
	callback:function(dt, e){
		cura.action.paging_talent.page++;
		cura.action.paging_talent.busy = false;		
	}
}

cura.action.init_search = {
	callback: function(dt, e){
		
		cura.execute('get_tags', e);
	
		$(".talent-search-bar input[type=text]").keyup(function (e){
			
			var val = $(this).val();
			
			if(val && val.length > 0){
				$('#'+this.id+'_').css({display:'block'});	
			}
			else{
				$('#'+this.id+'_').css({display:'none'});
				$('#agency').val('');
			}
			
			
			
			
			if($('#search_complete a').length > 0){
				
				if(e.which == 13){
			/*
					if($('#search_complete a.selected').length == 1){
						
						setCategory($('#search_complete a.selected'));
					}
					
					return false;
					*/
				}
				else if(e.which == 38){
					if($('#search_complete a.selected').length == 1){
						
						let sel = $('#search_complete a.selected');
						
						if(sel.prev('a').length > 0){
							sel.prev('a').addClass('selected');
							setCategory(sel.prev('a'), true);
							sel.removeClass();
						}
						
						
					}
					else{
							$('#search_complete a:eq(0)').addClass('selected');
							setCategory($('#search_complete a:eq(0)'), true);
						}
					return;
				}
				else if(e.which == 40){
					
					if($('#search_complete a.selected').length == 1){
						
						let sel = $('#search_complete a.selected');
						
						if(sel.next('a').length > 0){
							sel.next('a').addClass('selected');
							setCategory(sel.next('a'), true);
							sel.removeClass();
						}
						
					}
					else{
							$('#search_complete a:eq(0)').addClass('selected');
							setCategory($('#search_complete a:eq(0)'), true);
						}
					return;
				}
			}
			
			let matches = [];
			
			if(val.length >= 2 ){
			
				let original  = val.toLowerCase();
				
				//match the whole string for cat names
				for(let k in skills){
					
					let sn1 = skills[k].name.toLowerCase();
					//console.log(sn1, original)
					if(original && sn1.indexOf(original) == 0){
						matches.push('<a onClick="setCategory(this)" data-id="'+skills[k].id+'" data-agency="'+((skills[k].agency) ? 1 : 0)+'" >'+skills[k].name+'</a>');
						//console.log(skills[k].name,'A', sn1.indexOf(original));
						//original = '';
					}
					/*else if(original && original.indexOf(sn1) >= 0){
						matches.push('<a onClick="setCategory(this)" data-id="'+skills[k].id+'" data-agency="'+((skills[k].agency) ? 1 : 0)+'" >'+skills[k].name+'</a>');
						//console.log('B', original.indexOf(sn1));
						original = original.replace(sn,'');
					}*/
				}
				
				for (let k in cura.tags){
					let tg = cura.tags[k].toLowerCase();
					
					if(original && tg.indexOf(original) == 0){
						matches.push('<a onClick="setCategory(this)" data-id="'+k+'" data-agency="0" >'+cura.tags[k]+'</a>');
					}
				}
				
				
				//console.log(original);		
				// match partial string
				
				let vparts = original.split(' ');
			
				for(let p = 0; p < vparts.length; p++){
					
					if(vparts[p].length < 2 || vparts[p] == 'in' || vparts[p] == 'on' || vparts[p] == 'at'){
						continue;
					}

					for(let k in skills){
						let sn = skills[k].name.toLowerCase();
						if(sn != vparts[p] &&  sn.indexOf(vparts[p]) > -1){
							matches.push('<a onClick="setCategory(this)" data-id="'+skills[k].id+'" data-agency="'+((skills[k].agency) ? 1 : 0)+'" >'+skills[k].name+'</a>');
						}
					}							
				}
			}
			
			let txt = '';
			
			if(matches.length > 0) {
				
				matches = matches.filter(function(value, index, self) { return self.indexOf(value) === index;});
				
				txt = '<div>';
				txt += matches.slice(0, 5).join('');
				txt += '</div>';
			}			

			$('#search_complete').html(txt);
			
			
		})
		$(document.body).click(function (){$('#search_complete').html('');});
		
		$(window).scroll(function(e){
			
			pg = document.getElementById('search-paging');
			if(pg) {
				
				 if($(window).scrollTop() +500 > $(document).height() - $(window).height()) {
					if(!cura.action.paging_talent.busy){
						
						cura.execute('paging_talent',pg);	
					}
				}
			}
		});	
	
	}
}

cura.action.search_by_tag = {
	callback: function (dt, e) {
		$('#b_search').val(dt.tag);
		cura.execute('search_talent',e);
	}
}


cura.action.category_select = {
	callback: function (dt, e){
			
		if(dt.agency){
			$('#b_cat').val($(e).data('name')+' agency');
			$('#agency').val(1);
		}
		else{
			$('#b_search').val($(e).data('name'));
			$('#agency').val(0);
		}
		
		//$('#cat_id').val(dt.id);

		$(window).scrollTop($('.talent-search-bar').offset().top-40);

		cura.execute('search_talent',e);
		//$('#b_cat_').css({display:'block'});
		$('#search-categories').slideUp(200).removeClass('category_open');
		$('#category_toggle').removeClass('open');
	}	
}

cura.action.category_toggle = {
	callback: function () {
		
		if($('#search-categories').hasClass('category_open')) {
			$('#search-categories').slideUp(200).removeClass('category_open');
			$('#category_toggle').removeClass('open');
		}
		else {
			$('#search-categories').slideDown(200).addClass('category_open');
			$('#category_toggle').addClass('open');
		}
		
	}
}

cura.action.category_hide = {
	callback: function () {
				
	}
}

cura.action.clear_field = {
	callback: function(dt, e){
		$('#'+dt.id).val('');
		$(e).css({display:'none'});
		
		if(dt.id == 'b_cat'){
			$('#cat_id').val('');
			$('#agency').val('');
		}
		else if(dt.id == 'b_place'){
			cura.location = {};
		}
		cura.execute('search_talent',e);
	}
}

cura.action.show_more_events = {	
	callback: function(dt, e) {
		
		if(dt.hide) {
			$('.active-event').addClass('hidden');
		}
		
		$('.all-events').removeClass('hidden');
		$(e).css({display:'none'})
	}
}

cura.action.unset_active_event = {
	url:'/sd/event_clear',
	callback: function () {
		$('.manual-selection').removeClass('hidden');
		$('.event-selection').addClass('hidden');
		$(window).scrollTop(0);
		
		//$('#date')[0].focus();
	},
	mask:false,
	message:false
}

cura.action.set_active_event = {
	url:'/sd/event_set',
	message:'Checking available packages'
}

cura.action.check_availability = {
	url:'/sd/check_availability',
	submit:'booking-form',
	callback:function (dt, e) {
		
		if(cura.location && cura.location.place_id){
			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
			
		}
		else if ($('#place').data('place')) {
			// already selected
			return true;
		}
		else{
			notify('Please select a location');
			return false;	
		}
		
	},
	validate:{"rules":{"date":{"required":true},"time":{"required":true},"place":{"required":true}},"messages":{"date":{"required":"Please select a date"},"time":{"required":"Please select a time"},"place":{"required":"Please select the location"}}},
	message:'Checking...'
}

cura.action.select_package = {
	url:'/sd/select_package',
}

cura.action.select_package_time = {
	url:'/sd/select_package',
	callback: function(dt, e){
		dt.date = schedule[needle][0];
		dt.time = schedule[needle][1];
		dt.package = package_id;	
	}
}

cura.action.change_booking = {
	url:'/sd/booking_form'
}

cura.action.submit_booking = {	
	url:'/sd/submit_booking',
	callback: function(){
		
		if(!$('#conditions')[0].checked){
			notify('Please accept conditions and terms');
			$('#condition_error').removeClass('hidden');
			return false;	
		}
	},
	message:'Submitting...'
}

cura.action.condition_changed = {
	callback:function (dt, e){
		if(e.checked){
			$('#condition_error').addClass('hidden');	
		}
	}
}

cura.action.booking_summery = {
	url:'/sd/booking_summery'	
}



cura.action.show_event_form = {
	callback: function () {
		
		if($('#new_event_option')[0].checked) {
			$('#new_event_block').slideDown();
		}
		else{
			$('#new_event_block').slideUp();
		}
		
	}
}

cura.action.submit_event = {
	url: '/sd/event_submit',
	submit:'booking-event',
	validate:{"rules":{"event_name":{"required":(!document.getElementById('new_event_option') || document.getElementById('new_event_option').checked )}},"messages":{"event_name":{"required":"Event Name is required"}}}	
}


cura.action.booking_confirmation = {
	url: '/sd/booking_confirmation',
	mask:false
}



cura.action.package_availability = {
	callback:function (dt, e) {
		
		package_id = dt.id;
		slotwidth = dt.slots; 
		$('.packages').css({display:'none'});
		$('#sliding_timepicker').css({display:'block'});
		$('#pkg'+dt.id).css({display:'block'});
		$('.slider').css({height:slotwidth*5});
		setTimeout("setSliderSlot(96)",250);
	}
}

cura.action.package_all = {
	callback:function (dt, e) {
		$('#sliding_timepicker').css({display:'none'});
		$('.packages').css({display:'block'});
	}
}

cura.action.timeslide = {
	callback : function(dt, e){
		$( ".slidingbox" ).draggable({
			handle:'.timebox',  
			axis: "y", 
			grid:[5,5], 
			scroll:true,
			containment: "parent",
			drag:timeslide_update,
			stop:timeslide_update
		 });
		 
	}
}


cura.action.booking_package = {
	callback: function () {
		$(window).scrollTop($('.profile-booking:eq(0)').offset().top-50);
		$('#date').click();
		//$(window).animate({scrollTop:$('.profile-booking:eq(0)').offset().top},1000);
	}
}

cura.action.expend_top = {
	callback: function (dt, e) {
		$('.top-result').css({display:'none'});
		$('.top-expand').css({display:'block'});
	}
}


function setSliderSlot (index){
	dis = index*5;
	$('.slidingbox').animate({top:dis+15},500,function(){ 
			position = $('.timebar').offset().top
		 });
	$('#timedisplay').html(schedule[index][1]);
	$('#timeslider_btn').css({visibility:'hidden'});
}

function timeslide_update (e,ui) {
	var delta = Math.ceil((ui.offset.top - position)/5);
	var occ = false;
	
	for(var i = 0; i < slotwidth; i++){
		if(!schedule[delta+1] || schedule[delta+i][2] == 1){
			occ = true;
		}
	}
	if(schedule[delta]) {
		document.getElementById('timedisplay').innerHTML = schedule[delta][1];
		needle = delta;
	}
	if(!occ){
		document.getElementById('timeslider_btn').style.visibility= 'visible';	
	}
	else{
		document.getElementById('timeslider_btn').style.visibility ='hidden';
	}
}

function respositionCitySearch () {
	
	ox = document.getElementById('b_cat').offsetWidth;
	oh = document.getElementById('b_place').offsetHeight;
	ow = document.getElementById('b_place').offsetWidth;
	
	$('#place_search').css({top:oh, left:ox, right:2});
}

function setCategory (e, cls){
	
	$('#b_search').val($(e).text());
	
	if($(e).data('agency')){
		$('#agency').val(1);
	}
	else{
		$('#agency').val(0);
	}
	
	if(!cls){
		$('#search_complete').html('');
	}
	
	cura.execute('search_talent', e);
}




