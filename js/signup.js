/****  Signup form related functionality ************/

cura.action.signup_init = {
	callback: function (){
		
	}
}

cura.action.signup_advance = {
		url:'/sd/signup_country',
		callback: function (dt) {
			$('#country-container').css({display:'none'});
			$('#signup-form-container').css({display:'block'});
			dt.country = $('#country').val();
			document.getElementById('user_firstname').focus();
		},
	mask:false,
	message: false
}

cura.action.social_login = {
	callback:function (dt){
		
		if(dt.site == 'google'){
			
			 google.accounts.id.initialize({
			  client_id: '393731355064-sb420ssr6j5opq4gh114cp97bojm59o1.apps.googleusercontent.com',
			  callback: function(res){ 
			  	
				  if(res && res.credential){
					  let payload = decodeJWT(res.credential);
					  $('#user_firstname').val(payload.given_name).addClass('filled')[0].focus();
					  $('#user_lastname').val(payload.family_name).addClass('filled')[0].focus();
					  $('#user_email').val(payload.email).addClass('filled')[0].focus();
					  $('#picture').val(payload.picture);
					  $('#user_city')[0].focus();
					  console.log(payload)
					  notify('Please enter your city and mobile number to continue');
				  }
				  else{
					  new Dialog({
						title:'Signup with Google',
						message:"This feature is not available at the moment. Please try with different method"
					});
				  }
			  
			  },
			  context: 'signup',
			  ux_mode: 'popup'
			});
			
			google.accounts.id.prompt((notification) => {
				if (notification.isNotDisplayed() || notification.isSkippedMoment()) {
					new Dialog({
						title:'Signup with Google',
						message:"This feature is not available.<br>Error: <span class='txt-red'>"+notification.getNotDisplayedReason()+'</span>'
					});
				}
				else if(notification.getMomentType() == 'display'){
					notify("Select your google account");					
				}
				else if(notification.getMomentType() == 'skipped'){
					notify("Please fill to register");					
				}
				
			});

		}
		else if(dt.site == 'facebook'){
		
				FB.login(function(response) {
				  // handle the response
					
					if(response && response.authResponse && response.authResponse.accessToken){
						FB.api('/me?fields=id,email,first_name,last_name,picture.width(400).height(400)', function (payload){
							
							 $('#user_firstname').val(payload.first_name).addClass('filled')[0].focus();
							  $('#user_lastname').val(payload.last_name).addClass('filled')[0].focus();
							  $('#user_email').val(payload.email).addClass('filled')[0].focus();
							
							if(payload.picture && payload.picture.url) {
								$('#picture').val(payload.picture.url);
							}
							  
							  $('#user_city')[0].focus();
			
							  notify('Please enter your city and mobile number to continue');
							
						});
					}
					else{
						new Dialog({
							title:'Signup with Google',
							message:"This feature is not available at the moment. Please try with different method"
						});
					}
					
					
				}, {scope: 'public_profile,email'});
		}
		
	}
}

cura.action.signup_validate = {
	url: '/sd/create_account',
	submit:'signup_form',
	callback : function (dt){
		
		if(cura.location && cura.location.place_id){			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
		}
		else{
			//notify('Please select a location');
			//return false;	
			let city = $('#user_city').val();
			if(city && $.trim(city) != ''){
				dt.city = city;
				dt.coutry = $('#country').val();
				dt.region = '';
				dt.place_id = 1;
			}
			else{
				notify('Please select a location');
			}
		}
		
		cura.action.signup_validate.confirm = "We'll send you account verification code into your mobile.<p>"+$('#user_phone').val()+"</p>Is this number correct?";
	},
	validate:{"rules":{"country":{"required":true},"user_firstname":{"required":true},"user_lastname":{"required":true},"user_city":{"required":true},"user_phone":{"required":true,"number":true, minlength:6},"user_email":{"required":true,"email":true}},"messages":{"country":{"required":"Country is required"},"user_firstname":{"required":"Please entre your first name"},"user_lastname":{"required":"Please entre your last name"},"user_city":{"required":"Please select your nearest city"},"user_phone":{"required":"Mobile number is required to verify your account","number":"Enter the number without spaces or dashes", minlength:"Number seems like too short"}, "user_email":{"required":"Please enter your email address","email":"Invalid email address"}}},
	
	
	confirm: "We'll send you account verification code into your mobile.<p></p>Is this number correct?",
	confirm_button: 'Yes, Send the code',
	message : 'Creating your account...'
}


cura.action.submit_signup_profile = {
	url:'/sd/signup_profile',
	submit: 'signup_profile_form',
	callback: function() {
		
		if($('input.gender:checked').length === 0){
			$('#gender-error').css({display:'block'});
			
			$('input.gender').unbind().change(function(){
					$('#gender-error').css({display:'none'});
				})
			
			notify("Please select your gender");
			return false;	
		}		
			
	},
	
}

cura.action.submit_agency_profile = {
	url:'/sd/signup_agency',
	submit: 'signup_agency_form',
	callback: function(dt, e) {
		if(cura.location && cura.location.place_id){			
			for(var k in cura.location){
				dt[k] = cura.location[k];	
			}
		}
		else{
			notify('Please select a location');
			return false;	
		}
	},
	validate:{"rules":{"agency_name":{"required":true},"address":{"required":true},"office_city":{"required":true}},"messages":{"agency_name":{"required":"Please enter agency name"},"address":{"required":"Please enter block number or street name"},"office_city":{"required":"Please select nearest city"}}}
}

cura.action.browse_banner_image = {
	callback: function(dt, e){
		ct_editing = $('.page_banner_image:eq(0)');
		$('#image_browse').off('change').on('change',uploadCuraImage);
		$('#image_browse').click();
	}
}

cura.action.browse_profile_image = {
	callback: function(dt, e){
		$('#image_browse').off('change').on('change',uploadCuraImage);
		$('#image_browse').click();
	}
}

cura.action.submit_signup_page = {
	url:'/sd/signup_page',
	submit:'signup_page_form',
	validate:{"rules":{"page_handle":{"required":true},"section_title":{"required":true},"section_body":{"required":true}},"messages":{"page_handle":{"required":"Page Handle is required"},"section_title":{"required":"Section Title is required"},"section_body":{"required":"Section Body is required"}}}
}


var categories = null;
var category_types = {
	1: ['Artist/Performer','Group of artists/performers'],
	2: ['Arrangement/Decoration', 'Arrangement/Decoration business'],
	3: ['Beautician','Beauty Salons/Spa'],
	4 : ['Photographer'],
	5:['Event/Activity Planners', 'Event/Activity Planning Business'],
	6:['Service Provider','Service Business'],
	7:[null,'Suppliers'],
	8:[null,'Event Venues']
};

var bio = {};

cura.action.bio_begin = {
	url:'/sd/get_categories',
	message:'Collecting some data...',
	mask:false
}

cura.action.bio_start = {
	callback : function(dt, e, res){
		
		categories = res.data;
		
		$('#bio').css({display:'block'});
		
		cura.action.bio1.callback();
		
		console.log(res);
	}	
}

cura.action.bio1 ={
	
	callback:function() {
		
		let txt = '<div class=" txt-m">Answer few simple questions to build your description.</div><div class="vpadding txt-m">Select an option best match with you</div><div class="bio-option panel linked" cura="bio2" data-value="I" ><div>Individual/Professional</div><span>Describe as  "I am ..."</span></div>';
		txt += '<div class="bio-option panel linked" cura="bio2" data-value="We" ><div>A team or group</div><span>Describe as  "we are ..."</span></div>';
		txt += '<div class="bio-option panel linked" cura="bio2" data-value="business" ><div>Event service business</div></div>';
		txt += '<div class="bio-option panel linked" cura="bio2" data-value="company" ><div>Agency or Company</div></div>';
		txt += '<div class="bio-option panel linked" cura="bio2" data-value="place" ><div>Event venue like Hotel, Reception Hall</div></div>';
		$('#bio').html(txt);
		cura.bind();
	}
}

cura.action.bio2 = {
	callback: function(dt,e) {

		bio.type = dt.value;

		
		let txt = '<div class="txt-m">Select your category.</div><div class="sep"></div>';
		
		if(bio.type == 'I'){
			let cats = [1,2,3,4,5,6];
			bio.lookup = 0;
			for(let c in cats){
				console.log(c);
				txt += '<div class="bio-option panel linked" cura="bio3" data-value="'+cats[c]+'" ><div>'+category_types[cats[c]][0]+'</div></div>';
			}
			
		}		
		else if(bio.type == 'We'){
			let cats = [1,3,5,6];
			bio.lookup = 1;
			for(let c in cats){
				txt += '<div class="bio-option panel linked" cura="bio3" data-value="'+cats[c]+'" ><div>'+category_types[cats[c]][1]+'</div></div>';
			}			
		}
		else if(bio.type == 'business'){
			let cats = [2,3,5,6,7];
			bio.lookup = 1;
			for(let c in cats){
				txt += '<div class="bio-option panel linked" cura="bio3" data-value="'+cats[c]+'" ><div>'+category_types[cats[c]][1]+'</div></div>';
			}			
		}
		else if(bio.type == 'place'){
			bio.cat = 8;
			bio.lookup = 1;
				
			for(let c in categories['skills']){
				
				if(categories['skills'][c].category == bio.cat){
				txt += '<div class="bio-option panel linked" cura="bio4" data-value="'+categories['skills'][c].id+'" ><div>'+categories['skills'][c].name+'</div></div>';
				}
			}			
		}
		else if(bio.type == 'company'){
			bio.lookup = 0;
			
			for(let c in categories['agencies']){
				txt += '<div class="bio-option panel linked" cura="bio4" data-value="'+c+'" ><div>'+categories['agencies'][c]+'</div></div>';
			}			
		}
		
		
		$('#bio').html(txt);
		cura.bind();
	}
}

cura.action.bio3 = {

	callback: function(dt,e) {
		
		bio.cat = parseInt(dt.value);
					
		let txt = '<div class="txt-m">Select your skill</div><div class="sep"></div>';
		let regex = /team|group|band/;
		
		console.log(bio);

		for(let c in categories['skills']){
			
			if(categories['skills'][c].category == bio.cat){
	
				if(bio.lookup == 1 && (parseInt(categories['skills'][c].is_business) == 1 || categories['skills'][c].name.toLowerCase().match(regex))){
					
				   txt += '<div class="bio-option panel linked" cura="bio4" data-value="'+categories['skills'][c].id+'" ><div>'+categories['skills'][c].name+'</div></div>';
				}
				else if(bio.lookup == 0 && parseInt(categories['skills'][c].is_business) == 0 && !categories['skills'][c].name.toLowerCase().match(regex)) {
				   	
				   txt += '<div class="bio-option panel linked" cura="bio4" data-value="'+categories['skills'][c].id+'" ><div>'+categories['skills'][c].name+'</div></div>';
				 }
								
			}

			
		}		
		$('#bio').html(txt);
		cura.bind();
		
	}	
}


cura.action.bio4 = {
	callback: function(dt){
		
		bio.part = dt.value;
		
		let txt = '<div class="txt-m">When you got started?</div><div class="sep"></div>';
		
		if(bio.type == 'We' || bio.type == 'I'){
			txt = '<div class="field-wrapper"><label>Which year you started your profession?</label><input type="text" pattern="[0-9]*" inputmode="numeric" id="year" class="input-short" ></div>';			
		}
		else if(bio.type == 'business' || bio.type == 'company'){
			txt = '<div class="field-wrapper"><label>Which year you found your business?</label><input type="text" pattern="[0-9]*" inputmode="numeric" id="year" class="input-short" ></div>';			
		}
		else if(bio.type == 'place'){
			txt = '<div class="field-wrapper"><label>When do you started renting this property?</label><input type="text" pattern="[0-9]*" inputmode="numeric" id="year" class="input-short" ></div>';			
		}
		
		txt += '<div class="button-container"> <div class="center"></div> <a class="button" cura="bio5">Next</a></div>';
				
		$('#bio').html(txt);
		cura.bind();
	}
}

cura.action.bio5 = {
	callback: function(dt){
		
		bio.found = $('#year').val();
		
		if(!parseInt(bio.found) || bio.found > new Date().getFullYear()){
			notify('Please enter a valid year');
			return false;
		}
		
		let txt = '<div class="txt-m">Won any awards?</div><div class="sep"></div>';
		
		if(bio.type == 'We' || bio.type == 'I'){
			txt = '<div>Did you win any special awards or recognitions so far?</div>';			
		}
		else if(bio.type == 'business' || bio.type == 'company'){
			txt = '<div>Did your business win any special awards or recognitions so far?</div>';		
		}
		else if(bio.type == 'place'){
			txt = '<div>Did your property get any specil awards or recognitions from tourism websites like Agoda,booking.com ?</div>';			
		}
		
		
		
		
		txt += '<div class="field-wrapper"><label>Award name</label><input type="text"  id="award" class="input-short" ></div>';
		txt += '<div class="field-wrapper"><label>Organization name</label><input type="text"  id="organization" class="input-short" ></div>';
		
		txt += '<div class="sep2"></div><div class="bio-option panel linked" cura="bio6" data-value="No" ><div>No. I didn\'t get any awards</div></div>';
		
		txt += '<div class="button-container"> <div class="center"></div> <a class="button" cura="bio6" data-value="Yes">Create Description</a></div>';
				
		$('#bio').html(txt);
		cura.bind();
	}
}

cura.action.bio6 = {
	callback: function(dt){
		
		bio.award = dt.value == 'Yes' ? 1 : 0;
		
		if(bio.award == 1){
			bio.award_name = $.trim($('#award').val());
			bio.org = $.trim($('#organization').val());
			console.log(bio);
			if((!bio.award_name  || bio.award_name == "" ) && (!bio.org || bio.org == "")){
				notify('Please enter awarad name or organization name');
				return false;
			}
		}
		
		$('#bio').html('').css({display:'none'});
		
			// creating bio
		let desc = '';
		
		if(bio.type == 'I'){
			
			desc = 'I am a '+getCategoryName(bio.part)+' in '+$('#city').val()+'. '+' Since '+bio.found+' I have entertain many clients.';
		
			if(bio.award){
				
				desc += ' With my commitment to the profession and hard earned  '+(new Date().getFullYear() - bio.found)+' years experience, ';
				
				if(bio.award_name && bio.org ){
					desc += ' I was awarded '+bio.award_name+' from '+bio.org+' as a  distinction to my dedication.';
				}
				else if( bio.org ){
					desc += ' I got a special recognition from '+bio.org+' as a  distinction to my dedication.';
				}
				else if(bio.award_name ){
					desc += ' I was awarded '+bio.award_name+' as a  distinction to my dedication.';
				}
			}
		
			desc += ' My goal is to become the best '+getCategoryName(bio.part)+' in the country. ';
			
			
		}
		else if(bio.type == 'We' ){
			
			desc = 'We are a '+getCategoryName(bio.part)+' in '+$('#city').val()+'. '+' Since '+bio.found+', We have entertain many clients.';
		
			if(bio.award){
				
				desc += ' With our commitment to the field and hard earned  '+(new Date().getFullYear() - bio.found)+' years experience, ';
				
				if(bio.award_name && bio.org ){
					desc += ' We were awarded '+bio.award_name+' from '+bio.org+' as a  distinction to our dedication.';
				}
				else if( bio.org ){
					desc += ' We got a special recognition from '+bio.org+' as a  distinction to our dedication.';
				}
				else if(bio.award_name ){
					desc += ' We were awarded '+bio.award_name+' as a  distinction to our dedication.';
				}
			}
		
			desc += ' Our goal is to become the best '+getCategoryName(bio.part)+' in the country. ';
						
		}
		else if(bio.type == 'business' || bio.type == 'company' || bio.type == 'place'){
			
			let catname = (bio.type == 'company') ? categories['agencies'][bio.part] : getCategoryName(bio.part);
			
			desc = $('#section_title').val()+' is a '+catname+' '+(bio.type )+' in '+$('#city').val()+'. '+' Since '+bio.found+' we have entertain many clients.';
		
			if(bio.award){
				
				desc += ' With our commitment to the business and hard earned  '+(new Date().getFullYear() - bio.found)+' years experience, ';
				
				if(bio.award_name && bio.org ){
					desc += $('#section_title').val()+' were awarded '+bio.award_name+' from '+bio.org+' as a  distinction to our dedication.';
				}
				else if( bio.org ){
					desc += $('#section_title').val()+'  got a special recognition from '+bio.org+' as a  distinction to our dedication.';
				}
				else if(bio.award_name ){
					desc += $('#section_title').val()+' were awarded '+bio.award_name+' as a  distinction to our dedication.';
				}
			}
		
			desc += ' '+$('#section_title').val()+' mission is to become the best '+catname+' '+bio.type+' in the country. ';		
		}
		
		
		$('#section_body').val(desc);
		console.log(bio);
	}
	
}

/*************** Signup function *************/
	
function getCategoryName (id){
	
	for(let c in categories['skills']){
	
		if(categories['skills'][c].id == id){
			return categories['skills'][c].name;
		}
	}
	
	return '';
}
			

function singnupProfile () {
	
	$('input.category').change(monitorGenderField);
	$('#gender-box').css({display:'none'});
	
}

function monitorGenderField (){
	
	if($('input.category:checked').length > 3){
		this.checked = false;
		notify("Maximum 3 categories only");	
	}
	
	if($('input.category:checked').length == 0){
		$('#category-error').css({display:'block'});
	}
	else {
		$('#category-error').css({display:'none'});
	}
	
	$('#gender-box').css({display:'none'});
	
	if($('input.sex:checked').length > 0){
		$('#gender-box').css({display:'block'});
	}	
}

