<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo !empty($title) ? $title :  Config::$site_name  ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo Util::mapURL('css/talents.css') ?>">
<script type="text/javascript" src="<?php echo Util::mapURL('/js/jquery-3.4.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Util::mapURL('/js/cura.js'); ?>"></script>
<?php
if(!empty($__files)){
	echo implode("\n", $__files);
	echo "\n";
}

?>
</head>
<body>
<div class="strip header-strip">
  <div class="container header">
  	<div class="dash-logo-wrapper"><a href="<?php echo Util::mapURL('/') ?>"><img src="<?php echo Util::mapURL('images/talent-logo.png') ?>"></a></div>
    <div class="flex-space"></div>
    <div class="dash-menu">
    	<a class="icon-menu-home" href="<?php echo Util::mapURL('/dashboard') ?>"><span>Dashboard</span></a>
        <a class="icon-menu-booking" href="<?php echo Util::mapURL('/join') ?>" ><span>Bookings</span></a>
        <a class="icon-menu-payment" href="<?php echo Util::mapURL('/about') ?>"><span>Payments</span></a>
        <a class="icon-menu-mail" href="<?php echo Util::mapURL('/about') ?>"><span>Messages</span></a>
        <a class="icon-menu-notify" href="<?php echo Util::mapURL('/contact') ?>"><span>Alerts</span><label>3</label></a>
    </div>
     <div class="user-info" >
    	<a href="<?php echo ($auth->isAuthenticated()) ? Util::mapURL('/logout') : Util::mapURL('/login') ?>">
        <?php 
		if($auth->isAuthenticated()){
			echo '<img src="'.Util::mapURL('images/test-user-img.jpg').'"> ';
		}
		else{
			echo '<img src="'.Util::mapURL('images/user-blank-small.png').'"> ';
		}
		
		?>
        
        <span><?php  echo (empty($user)) ? 'Login' : $user->firstname; ?></span>
        </a>
    </div>
  </div>
</div>
<div class="page-container">
  <?php
echo $contents;
?>
</div>
<div class="strip footer-strip">
	<div class="container">
    	<div class="columns">
        	<div class="column-3"></div>
            <div class="column-3"></div>
            <div class="column-3"></div>
        </div>
        <div class="vpadding">
       &copy; Curatelant <?php echo date('Y'); ?> 
        </div>
    </div>
</div>

<div id="notifications">

</div>
<div id="blocker"></div>
<script>
var base = '<?php echo Util::mapURL(''); ?>';
var user_id = '<?php echo $user->id; ?>';
</script>
</body>
</html>