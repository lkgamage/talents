<?php
$base = Config::$base_url;

$site_description = !empty($description) ? $description :  "Curatalent is the premier online entertainment industry platform which seeks to bridge the gap between customers, agencies, and talents/services.";
/*Our platform featuring wide range of tools including talent and service marketplace, event planning, talent and service booking, appointment scheduling and message boards. We make the process of finding and hiring talent/services for events simple and pain-free.*/

$site_image = "https://curatalent.com/curatalent-logo-large.png";

$site_keywords = 'event,skill,events,talent,professionals,businsses,event planning,event management,event service,event marketplace,event organizers,event booking,event venue,event business,event agency,party,party organizer,party service,party venue,talent booking,talent search,talent marketplace,happening,hire talent,talent agency,talent curation,hire skill,Singer,Dancer,Dancing Group,Orchestra,Comedian,Agency,Musician,Flowerist,DJ Artist,Flashmob Team,Music Band,Pet Groomer,EventPalnner,Wedding Palnner,Photographer,Videographer,Beacuty Salon,Beautician,Dress Maker,Spa Therapist,SPA,Waiter,Waitress,Bartender,Steward,Stewardess,Cheff,Character Development Trainer,Magician,Announcer,Presenter,MC,Cheerleader Team,Escort Boy,Escort girl,Gardner,Art Director,Activity Planner,Activity Instructor,Activity Guide,Usher,Attendant,Story Teller,Event manager,Sound and Light Equipment Rental,Indoor/Outdoor Decorators,Cake Supplier,Reception Hall,Conference Hall,Auditorium,Outdoor Garden,Swimming Pool,Beach Garden,Cleaning Service,Activity Planing Company,Kids Playground Equipment Supplier,Clothing Supplier,Car Rent,Taxi Service,Drama Team,Celebrity,Speakers';

$site_url = Config::$base_url;

/** customize for pages */
if(!empty($page)){
	
	$title = ucwords($page->name);
	$site_description = $page->description;
	$site_keywords = !$page->isEmpty('skills') ? $page->skills : $site_keywords;
	$site_image = 'https:'.$page->image;
	$site_url .= '/'.$page->handle;
	
}

// final formatting
$title = !empty($title) ? $title.' - '.Config::$site_name :  Config::$site_name.' - '.'Hire Artists, Performers And Event Services';
//Premier Entertainment Industry Platform
if(!empty($url)){
	$site_url .= '/'.$url;
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#ef6c00">
	
<meta property="og:title" content="<?php echo $title;  ?>">
<meta property="og:description" content="<?php echo $site_description; ?>">
<meta property="og:image" content="<?php echo $site_image; ?>">
<meta property="og:url" content="<?php echo $site_url; ?>">
<meta property="og:type" content="website">
<meta property="fb:app_id" content="442476957536899">
<meta name="facebook-domain-verification" content="fo0bfft0ji5ee9786z0fukff4qwfp0" />

<meta property="twitter:card" content="website">
<meta property="twitter:title" content="<?php echo $title;  ?>">
<meta property="twitter:description" content="<?php echo $site_description; ?>">
<meta property="twitter:site" content="@curatalent">
<meta property="twitter:image" content="<?php echo $site_image; ?>">
	
<meta name="description" content="<?php echo $site_description; ?>">
<link rel="icon"  type="image/png"  href="https://curatalent.com/favicon.png">
 <meta name="keywords" content="<?php echo $site_keywords; ?>">
<script type="application/ld+json">{
    "@context": "https:\/\/schema.org",
    "@type": "Website",
    "name": "<?php echo $site_description; ?>",
    "url": "<?php echo $site_url; ?>"
}</script>
	
<title><?php echo $title;  ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo $base.'/css/talents.css?ctvar=_'.Version::$build; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo $base.'/css/chat.css?ctvar=_'.Version::$build; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo $base.'/css/jquery-ui.min.css'; ?>">
<script type="text/javascript" src="<?php echo $base.'/js/jquery.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $base.'/js/jquery.validate.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $base.'/js/jquery-ui.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $base.'/js/jquery.ui.touch-punch.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $base.'/js/client.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $base.'/js/cura.js?ctvar=_'.Version::$build; ?>"></script>
<script type="text/javascript" src="<?php echo $base.'/js/chat.js?ctvar=_'.Version::$build; ?>"></script>
	
<?php

if(!empty($__files)){
	echo implode("\n", $__files);
	echo "\n";
}

?>
<link rel="stylesheet" type="text/css" href="<?php echo $base.'/css/colors.css?version='.Version::$build; ?>">
<meta name="p:domain_verify" content="62ec86be54d8c8e1303c542cae6e36c7"/>

<!-- Initilaize FB SDk ----------->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '442476957536899',
      cookie     : true,
      xfbml      : true,
      version    : 'v12.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
	
<?php if(!Config::$debug){ ?>
<!-- Global site tag (gtag.js) - Google Analytics -->	
<script async src="https://www.googletagmanager.com/gtag/js?id=G-7K57GJPHLN"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-7K57GJPHLN');
</script>
	
<!-- Global site tag (gtag.js) - Google Ads: 984473448 -->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-984473448');
</script>

	<!-- Hotjar Tracking Code for https://curatalent.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2711564,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
	<?php } ?>
</head>
<body>
<?php if(!isset($maximized)) { ?>
<div class="strip header-strip">
  <div class="container header" id="header">
    <?php component('menu', $_data); ?>
  </div>
</div>
<div class="page-container">
<?php } else { ?>
<div class="page-container miximized">
  <?php } ?>
  <?php
echo $contents;
?>
</div>
<?php if(!isset($maximized)) { ?>
<div class="strip footer-strip">
<div class="sep2"></div>
  <div class="container">
	  <div><?php component('currency') ?></div>
	  <div class="sep"></div>
	  <div class="dash-sep"></div>
	  <div class="sep2"></div>
    <div class="columns">
      <div class="column-3">
      <h4>Event Organizers</h4>
      <div>
      	<a href="<?php echo $base; ?>/search">Find Talents/Businesses</a>
        <a href="<?php echo $base; ?>/search/type=agencies">Find Agencies</a>
        <a href="<?php echo $base; ?>/dashboard/events">Event Planining</a>
        <a href="<?php echo $base; ?>/dashboard/bookings">Manage Bookings</a>
      </div>
      </div>
      <div class="column-3">
       <h4>Talents/Businesses/Agencies</h4>
      <div>
      	<a href="<?php echo $base; ?>/join">Join</a>
        <a href="<?php echo $base; ?>/support/categories">Talent/Business Categories</a>
        <a href="<?php echo $base; ?>/dashboard/bookings">Manage Bookings</a>
        <a href="<?php echo $base; ?>/dashboard/appointments">Manage Schedule</a>
        <a href="<?php echo $base; ?>/dashboard/events">Event Planining</a>
        
      </div>
      </div>
      <div class="column-3">
       <h4>Support</h4>
      <div>
      	<a href="<?php echo $base; ?>/support/about">About</a>
        <a href="<?php echo $base; ?>/support/conditions-terms">Conditions and Terms</a>       
        <a href="<?php echo $base; ?>/support/privacy-policy">Privacy Policy</a>
		  <a href="<?php echo $base; ?>/support/faq">FAQ</a> 
        <a href="<?php echo $base; ?>/support/contact">Contact</a>
      </div>
      </div>
    </div>
   <div class="sep2"></div>
    <div class="vpadding bottom-row">
    <div class="columns">
    	<div class="resizable company-name">
        &copy;  <?php echo date('Y'); ?> Curatelant private limited.
        </div>
        <div >
        <div class="flex social-row">
        <div class="resizable"></div>
        	<a href="https://www.facebook.com/curatalent" target="_blank"><img src="<?php echo $base.'/images/social/facebook.png'; ?>" alt="Facebook"></a>
            <a href="https://twitter.com/curatalent" target="_blank"><img src="<?php echo $base.'/images/social/twitter.png'; ?>" alt="Twitter"></a>
            <a href="https://www.linkedin.com/company/curatalent" target="_blank"><img src="<?php echo $base.'/images/social/linkedin.png'; ?>" alt="LinkedIn"></a>
            <a  href="https://www.pinterest.com/curatalent" target="_blank"><img src="<?php echo $base.'/images/social/instagram.png'; ?>" alt="Instagram"></a>
            <a  href="https://www.instagram.com/curatalent" target="_blank"><img src="<?php echo $base.'/images/social/pinterest.png'; ?>" alt="Pinterest"></a>
			<div class="resizable"></div>
        </div>
        
        </div>
    </div>
    <div class="sep"></div>
    
     </div>
  </div>
</div>
<?php } ?>
<div id="notifications"></div>
<div id="blocker"></div>
<div id="notify_pane"></div>
<div id="gdpr" class="bg-orange">
<div class="columns">
	<div class="column-41 txt-s"><div class="txt-bold upper">Consent for Cookies</div>
	<div class="sep"></div>
		<div>We use cookies to store personalized data which required to operate the platform. Please refer our privacy policy statement for further details. By clicking Accept, you agree to Curatalent platform <a href="<?php echo Util::mapURL('/support/privacy-policy#cookie'); ?>" target="_blank">cookie policy</a>. </div>
	</div>
	<div class="column-4 center middle"><a class="button button-act" cura="gdpr_accept">Accept</a></div>
	</div>	
</div>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
	var Stripe = Stripe('<?php echo Config::$stripe_public_key; ?>');
</script>
<?php if(!Config::$debug){ ?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-619936f7f8a205a3"></script> 
<?php } ?>

<script src="https://www.gstatic.com/firebasejs/9.1.2/firebase-app-compat.js"></script> 
<script src="https://www.gstatic.com/firebasejs/9.1.2/firebase-messaging-compat.js"></script> 
<script src="https://www.gstatic.com/firebasejs/9.1.2/firebase-auth-compat.js"></script> 
<script>
const firebaseConfig = { apiKey: "AIzaSyBxtwQlN0SpbIg_BpdFAQJZhUV3kzUw9OI", authDomain: "talents-250604.firebaseapp.com", projectId: "talents-250604", storageBucket: "talents-250604.appspot.com", messagingSenderId: "393731355064", appId: "1:393731355064:web:54a94968dfb2d39f921f4d", measurementId: "G-NL81RZ4837" };
const firebaseApp = firebase.initializeApp(firebaseConfig);
const pushMessaging =  firebase.messaging();
pushMessaging.onMessage(onPushMessage);
var base = '<?php echo $base; ?>';
</script>
</body>
<!---- <?php echo Config::$server; ?>  -->
</html>