<?php
header('Content-Type: application/json');


$_out = array();

$_out['status'] = !empty($status) ? true : ((!empty($data) || !empty($id) || !empty($action)) ? true : false);

if(!empty($message)) {
	$_out['message'] = $message;
}

if(!empty($id)){
	$_out['id'] = $id;	
}

if(!empty($data)){
	$_out['data'] = $data;
}

if(!empty($html)){
	$_out['html'] = $html;	
}
if(!empty($append)){
	$_out['append'] = $append;	
}

if(!empty($trigger)){
	$_out['trigger'] = $trigger;	
}

if(!empty($action)){
	$_out['action'] = $action;	
}


if(!empty($nodes)){
	$_out['nodes'] = $nodes;	
}

if(!empty($error)){
	$_out['error'] = $error;	
}

if(!empty($page) && is_array($page)){
	$page['url'] = Util::mapURL($page['url']);
	$_out['page'] = $page;	
}


echo json_encode(utf8ize($_out));
?>