<div class="strip dash-strip dashboard-strip">
  <div class="container">
    <div class="dash-info"></div>
    <div class="dash-body">
      <div class="dash-body-inner">
        <div class="dash-side">
          <?php component('dash-side', $_data); ?>
        </div>
        <div class="dash-contents" id="dash-contents">
        
        <?php 
		
		if(!empty($action)) {
			
			echo '<div class="inline-loader" ';
			
			foreach ($action as $k => $val){
				
				if($k == 'cura'){
					echo 'cura="'.$val.'"';	
				}
				else{
					echo 'data-'.$k.'="'.$val.'"';	
				}
				
			}
			
			echo 'data-type="load" ></div>';
			
		}
		else {
		
			component('dashboard/dashboard', array('user' => $user));
		
		}
		
		 ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="hidden">
  <input type="file" id="image_browse" accept="image/*" multiple>
</div>