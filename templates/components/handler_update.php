<div class="padding bg-gray panel">
  <form cura="handler_update" id="handler_update">
  <input type="hidden" name="afteredit" value="<?php echo (!empty($redirect)) ? 1 : 0 ?>">
  <input type="hidden" name="page" value="<?php echo !empty($page) ? $page->id : '' ?>">
    <div class="field-wrapper">
      <label class="dynamic-label" for="page_handle" >Handle</label>
      <input type="text" value="<?php echo !empty($page) ? $page->handle : '' ?>" name="page_handle" id="page_handle" maxlength="20" cura="handler_cleanup" data-type="keyup">
    </div>
    <div class="error"  id="_server_error"></div>
  </form>
  <div class="sep"></div>
  <div class="button-container">
    <div class="center"></div>
	  <div class="button-wrapper">
	  <?php if(empty($redirect)) { ?>
    <a class="button btn-next " cura="handler_update">UPDATE</a>
    <div class="gap"></div>
    <a class="button btn-next button-alt " cura="inline_load_cancel">CANCEL</a> 
    <?php } else { ?>
    <a class="button btn-next " cura="handler_update">CREATE PAGE</a>
    <?php } ?>
	  </div>
    
    </div>
</div>
