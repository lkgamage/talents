<div id="search-results" >
  <?php
	
	component('tag_helper', array('engine' => $engine, 'top' => true));
	
if(empty($engine->term) && empty($engine->category) && empty($engine->city) && empty($_POST['list']) && empty($engine->tags)){
		
		// landing page
		
		// newset talents
		$_data['data'] = $engine->newTalents();
		if(!empty($_data['data'])) {
			echo '<div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Raising Stars</div><div><a class="more-link" cura="explore_list" data-list="nt">Explore All</a></div> </div>';
		
			component('search_paging', $_data);
		}
		
		// newest agencies
		$_data['data'] = $engine->newAgencies();
		if(!empty($_data['data'])) {
		echo '<div class="sep full-width"></div><div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">New Players</div><div><a class="more-link" cura="explore_list" data-list="na">Explore All</a></div> </div>';
		component('search_paging', $_data);
		}
		
		// populr talents
		$_data['data'] = $engine->populerTalents();
		if(!empty($_data['data'])) {
		echo '<div class="sep full-width"></div><div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Rocking On The Stage</div><div><a class="more-link" cura="explore_list" data-list="pt">Explore All</a></div> </div>';
		
		
			component('search_paging', $_data);
		}
		
		// populer agencies
		$_data['data'] = $engine->populerAgencies();
		
		if(!empty($_data['data'])) {
		echo '<div class="sep full-width"></div><div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Best In The Business</div><div><a class="more-link" cura="explore_list" data-list="pa">Explore All</a></div> </div>';
		
		
		component('search_paging', $_data);
		}
		
		// talents in town
		$_data['data'] = $engine->results();
		if(!empty($_data['data'])) {
		echo '<div class="sep full-width"></div><div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Close To You</div><div><a class="more-link" cura="explore_list" data-list="tt">Explore All</a></div> </div>';
		
		
			component('search_paging', $_data);
		}
	
		echo '<div class="padding center full-width "><a class="more-link txt-m" cura="explore_list" data-list=na">See All Talents &amp; Service Providers</a></div>';
		
}
else{
	$data = $engine->results();
	
	if(!empty($data)){	
	
			if(!empty($_POST['list'])) {
			
				if($_POST['list'] == 'nt'){
					// new talents 
					echo '<div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Raising Stars</div> </div>';		
				}
				elseif($_POST['list'] == 'na'){
					// new agencies
					echo '<div class="sep full-width"></div><div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">New Players</div> </div>';			
				}
				elseif($_POST['list'] == 'pt'){
					// populer talents
					echo '<div class="sep full-width"></div><div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Rocking On The Stage</div></div>';
				}
				elseif($_POST['list'] == 'pa'){
					// popler agencies
					echo '<div class="sep full-width"></div><div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Best In The Business</div> </div>';	
				}
				
			}		
			elseif(!empty($engine->category)){
				
				if(!empty($engine->agency)){
					$label = DataType::$AGENCY_TYEPS[$engine->category].' Agencies';
				}
				else {
					$skills = $engine->getSkills();
					$skills = Util::groupArray($skills, 'id','name');
					
					$label = str_replace('|', '/',$skills[$engine->category]);	
					$label = Util::pluralize($label);
				}
				
				if(!empty($engine->city)){
					$label .= " in ".ucfirst($engine->city);
				}
				else{
				//	$label .= " in your area";
				}
				
				echo '<div class=" full-width"><h1 class="resizable nopadding txt-xl txt-bold">'.$label.'</h1></div>';
			}
		
	
			$_data['data'] = $data;
			component('search_paging', $_data);
		
		
			if(!empty($engine->tags)) {
			?>
	
	<?php
			}
			
		
	}
	elseif($engine->page == 1){
	// no results in page 1 *****************************************************************
		
		$skills = $engine->getSkills();
		$skills = Util::groupArray($skills, 'id','name');
		
		if(!empty($engine->category)){
				
			if(!empty($engine->agency)){
				$label = DataType::$AGENCY_TYEPS[$engine->category].' Agencies';
			}
			else {

				$label = str_replace('|', '/',$skills[$engine->category]);	
				$label = Util::pluralize($label);
			}

			if(!empty($engine->city)){
				$label .= " in ".ucfirst($engine->city);
			}
			else{
				//$label .= " in your area";
			}

			echo '<div class=" full-width"><h1 class="resizable nopadding txt-xl txt-bold hidden">'.$label.'</h1></div>';
		}
		
		echo '<div class="sep"></div><div class="panel padding center bg-white"><div class="padding txt-m">We could not found ';
		
		
		if(!empty($engine->term)){
			
			if(!empty($engine->category)){
				echo (!empty($engine->agency)) ? DataType::$AGENCY_TYEPS[$engine->category] : $skills[$engine->category];
				
			}
			
			echo ' <span class="txt-bold">'.$engine->term.'</span>';
							
		}
		elseif(!empty($engine->category)){
				echo (!empty($engine->agency)) ? DataType::$AGENCY_TYEPS[$engine->category].' agency' : $skills[$engine->category];
				
		}
		else{
			echo " people or services for your search";
		}
		
		
		
		if(!empty($engine->city)){
			echo ' in ';
			
			echo ucfirst($engine->city); //.' city';
			/*
			if(!empty($engine->region)){
				echo $engine->region.', ';	
			}*/
		}
		
		//echo DataType::$countries[$engine->country][0];
		
		echo '.';
		
		echo '</div></div>';
		
		// show new talents
		$_data['data'] = $engine->newTalents();
		if(!empty($_data['data'])) {
			echo '<div class="vpadding flex full-width"><div class="resizable txt-xl txt-bold">Recently joined Talents</div><div><a class="more-link" cura="explore_list" data-list="nt">Explore All</a></div> </div>';
		
			component('search_paging', $_data);
		}
	}

}
/******************************************* Tag hints **********************************/
	if(!empty($_data['data']) && count($_data['data']) > 5)
component('tag_helper', $_data);
?>
</div>

<?php
if(!empty($data) && count($data) == $engine->pagesize){
	
	echo '<div id="search-paging" ></div>';
	
}

//Util::debug($engine);
?>
