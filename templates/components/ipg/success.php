<?php
$info = $invoice->getInfo();
?>
<div>
  <div class="panel">
    <div class="success-banner">
      <div class="upper">
        <?php


          switch ( $info[ 'invoice' ][ 'type' ] ) {
            case DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION:
              echo ( $info[ 'invoice' ][ 'amount' ] > 0 ) ? "Payment Complete" : "Subscription Activated";
              break;
            case DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION:
            case DataType::$INVOICE_TYPE_UP_SUBSCRIPTION:
              echo "Subscription Updated.";
              break;
            case DataType::$INVOICE_TYPE_CREDIT:
              echo "Payment Complete.";
              break;
              // ....
          }

        

        ?>
      </div>
    </div>
    <div class="sep"></div>
    <div class="section padding">
      <div>
        <p class="center">
          <?php

          switch ( $info[ 'invoice' ][ 'type' ] ) {
            case DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION:
              echo "Your <span class=\"txt-bold\">{$info['skill']['name']} - {$info['package']['name']}</span> subscription successfully activated.";
              break;
            case DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION:
            case DataType::$INVOICE_TYPE_UP_SUBSCRIPTION:
              echo "Your subscription plan successfully changed to <span class=\"txt-bold\">{$info['skill']['name']} - {$info['package']['name']}</span>.";
              break;
            case DataType::$INVOICE_TYPE_CREDIT:
              echo "Your booking credit has been added.";
              break;
              // ....
          }

          ?>
        </p>
        <div >
          <div class="max-600 is-centered">
            <?php
            if ( $info[ 'invoice' ][ 'type' ] == DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION || $info[ 'invoice' ][ 'type' ] == DataType::$INVOICE_TYPE_UP_SUBSCRIPTION ) {

              echo "<div>New plan will take effect from next billing date. <br>You will be charged " . currency( $info[ 'package' ][ 'price' ] ) . " from " . Util::ToDate( $info[ 'subscription' ][ 'begin_date' ] ) . ".</div>
			<div class=\"sep\"></div>";
            }
            ?>
            <div>We have emailed your receipt.</div>
            <div class="sep"></div>
            <div>Thank you!</div>
          </div>
        </div>
        <p></p>
        <div class="button-container">
          <div class="center"> <a class="button" href="<?php echo Util::mapURL('/dashboard') ?>">Continue</a> </div>
        </div>
        <p></p>
      </div>
    </div>
  </div>
</div>
