<div>
  <div class="panel">
    <div class="error-banner">
      <div class="upper">Problem!</div>
    </div>
    <div class="sep"></div>
    <div class="section padding">
      <div>
        <p class="center">We found a problem when we try to fulfil your request. 
          
        </p>
        <div >
          <div class="max-600 is-centered">
			 <div><?php echo $message; ?></div> 
			 <div class="sep"></div>
          
            <div class="sep"></div>
            <div>You may try again later. if problem still exists, please contact our support team </div>
          </div>
        </div>
        <p></p>
        <div class="button-container">
          <div class="center"> <a class="button" href="<?php echo Util::mapURL('/dashboard') ?>">Continue</a> </div>
        </div>
        <p></p>
      </div>
    </div>
  </div>
</div>