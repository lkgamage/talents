
<div>
  <div class="panel">
    <div class="success-banner">
      <div class="upper">Processing...</div>
    </div>
    <div class="sep"></div>
    <div class="section padding">
      <div>
        <p class="center">Payment processed. 
          <?php

          switch ( $invoice->type ) {
            case DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION:
              echo "Activating your <span class=\"txt-bold\">{$info['skill']['name']} - {$info['package']['name']}</span> subscription.";
              break;
            case DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION:
            case DataType::$INVOICE_TYPE_UP_SUBSCRIPTION:
              echo "Updating your subscription plan to <span class=\"txt-bold\">{$info['skill']['name']} - {$info['package']['name']}</span>.";
              break;
            case DataType::$INVOICE_TYPE_CREDIT:
              echo "Adding your booking credits.";
              break;
              // ....
          }

          ?>
        </p>
        <div >
          <div class="max-600 is-centered">
			 <div>It may takes few minutes to confirm your payment. We will notify by SMS and email when it is completed.</div> 
			 <div class="sep"></div>
          
            <div class="sep"></div>
            <div>Thank you!</div>
          </div>
        </div>
        <p></p>
        <div class="button-container">
          <div class="center"> <a class="button" href="<?php echo Util::mapURL('/dashboard') ?>">Continue</a> </div>
        </div>
        <p></p>
      </div>
    </div>
  </div>
</div>