<?php
$url = Util::mapURL( '/images/cards' );

$info = $invoice->getinfo();
?>
<div class="panel padding">
  <div class="columns">
    <div class="column-2">
      <div class="vpadding txt-xl">Order Summary</div>
      <div class="flex">
        <div class="resizable">
          <?php
          if ( $invoice->type == DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION ) {
            echo "New Subscription Payment";
          } elseif ( $invoice->type == DataType::$INVOICE_TYPE_PROFILE ) {
            echo "Profile Activation Payment";
          }
          elseif ( $invoice->type == DataType::$INVOICE_TYPE_CREDIT ) {
            echo "Single Booking Credit";
          }
          elseif ( $invoice->type == DataType::$INVOICE_TYPE_REG ) {
            echo "Registration Payment";
          }
          elseif ( $invoice->type == DataType::$INVOICE_TYPE_ADVERT ) {
            echo "Advertisement Payment";
          }

          ?>
        </div>
      </div>
      <div class="sep"></div>
		<div id="payment_form">
      <form id="invoice_payment" cura="invoice_process">
		  <input type="hidden" name="id" value="<?php echo $invoice->id; ?>">
        <div class="panel">
          <div class="padding">
			 <div class="sep"></div>
			 <div class="flex">
        <div class="resizable">
			<div></div>
          <?php
			
			// label in the payment panel
			
          if ( $invoice->type == DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION ) {
            echo '<div>'.$info['skill']['name'].' - '.$info['package']['name'].'</div><div class="txt-light">'.$info['package']['jobs'].' credits/'.($info['package']['validity'] == 365  ? 'Yearly' : 'Monthly').'</div>';
          } elseif ( $invoice->type == DataType::$INVOICE_TYPE_PROFILE ) {
            echo "Profile Activation Payment";
          }
          elseif ( $invoice->type == DataType::$INVOICE_TYPE_CREDIT ) {
            echo "Single Booking Credit";
          }
          elseif ( $invoice->type == DataType::$INVOICE_TYPE_REG ) {
            echo "Registration Payment";
          }
          elseif ( $invoice->type == DataType::$INVOICE_TYPE_ADVERT ) {
            echo "Advertisement Payment";
          }

          ?>
        </div>
        <div class="lpadding txt-xl middle"><?php echo currency($invoice->amount); ?></div>
      </div> 
			  
            <div class="sep2"></div>
			  
			  
            <div class="field-wrapper" cura="stripe_payment" data-type="load">
              <div id="card-element"> 
                <!-- Elements will create input elements here --> 
              </div>
              <div id="card-errors" role="alert"></div>
            </div>
            <div class="button-container">
              <div class="center"> </div>
              <a class="button txt-m" cura="invoice_process" data-id="<?php echo $invoice->id ?>" >
              <?php
              if ( $invoice->type == DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION ) {
                echo "Activate Subscription";
              } elseif ( $invoice->type == DataType::$INVOICE_TYPE_PROFILE ) {
                echo "Activate Profile";
              }
              elseif ( $invoice->type == DataType::$INVOICE_TYPE_CREDIT ) {
                echo "Activate Booking Credit";
              }
              elseif ( $invoice->type == DataType::$INVOICE_TYPE_REG ) {
                echo "Activate Registration";
              }
              elseif ( $invoice->type == DataType::$INVOICE_TYPE_ADVERT ) {
                echo "Pay &amp; Confirm";
              }
              ?>
              <span class="amount"></span></a> </div>
          </div>
          <div class="bg-gray">
            <div class="flex">
              <div class="padding"><img src="<?php echo $url.'/padlock.png' ?>"></div>
              <div class="resizable middle">Safe &amp; Secure Checkout</div>
              <div class="padding"><img src="<?php echo $url.'/stripe.png' ?>"></div>
            </div>
            <div class="hpadding">
              <div class="flex checkout-cards">
                <div><img src="<?php echo $url.'/visa.png' ?>"></div>
                <div><img src="<?php echo $url.'/master.png' ?>"></div>
                <div><img src="<?php echo $url.'/amex.png' ?>"></div>
                <div><img src="<?php echo $url.'/discover.png' ?>"></div>
                <div><img src="<?php echo $url.'/jcb.png' ?>"></div>
                <div><img src="<?php echo $url.'/diners.png' ?>"></div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <?php
      $currency = Session::currency();
      if ( $currency != 'USD' ) {
        echo '<div class="sep2"></div>';
        echo '<div class="txt-light">You will be billed US$ ' . number_format( $invoice->amount, 2 );
        echo '.&nbsp;&nbsp;(US$ 1 = ' . DataType::$csymbols[ $currency ] . Currencies::$$currency . ')';
        echo '</div>';
      }

      ?>
		</div>
		<div class="padding panel hidden" id="process_loader">
    <div class="flex">
      <div class="rpadding"><img src="<?php echo Util::mapURL('/images/rolling-blue.gif'); ?>"></div>
      <div class="middle txt-m" id="process_message">Your payment is processing...</div>
    </div>
  </div>
		
    </div>
    <div class="column-2 right-content middle">
      <?php
      if ( $invoice->type == DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION ) {


        ?>
      <div class="txt-xl txt-blue vpadding"><?php echo $info['skill']['name'].' - '.$info['package']['name'];?> </div>
      <div><?php echo nl2br($info['package']['description']); ?></div>
      <div class="sep"></div>
      <div class="flex">
        <div class="col2">Term</div>
        <div class="col2 txt-bold"><?php echo $info['package']['validity'] == 365  ? 'Yearly' : 'Monthly'?></div>
      </div>
      <div class="flex">
        <div class="col2">Booking Credits</div>
        <div class="col2 txt-bold"><?php echo $info['package']['jobs']?></div>
      </div>
      <div class="sep2"></div>
      <div class="txt-bold txt-gray">INVOICE</div>
      <div class="sep"></div>
      <?php

      foreach ( $info[ 'items' ] as $item ) {

        echo '<div class="flex padding">
				<div class="resizable">' . $item[ 'description' ] . '</div>
				<div class=" txt-bold">' . currency( $item[ 'amount' ] ) . '</div>
			</div><div class="dash-sep"></div>';

      }

      echo '<div class="flex padding bg-gray">
				<div class="resizable txt-bold">Total Amount</div>
				<div class=" txt-bold">' . currency( $info[ 'invoice' ][ 'amount' ] ) . '</div></div>
			<div class="dash-sep"></div>';
      ?>
      <div class="sep2"></div>
      <div class="txt-light txt-s"><span class="txt-black">Want to change your subscription plan?</span><br>
        You may manage your subscriptions in your profile.</div>
      <?php
      //Util::debug($info);
      } elseif ( $invoice->type == DataType::$INVOICE_TYPE_PROFILE ) {
        echo "Profile Activation Payment";
      }
      elseif ( $invoice->type == DataType::$INVOICE_TYPE_CREDIT ) {
        echo "Single Booking Credit";
      }
      elseif ( $invoice->type == DataType::$INVOICE_TYPE_REG ) {
        echo "Registration Payment";
      }
      elseif ( $invoice->type == DataType::$INVOICE_TYPE_ADVERT ) {
        echo "Advertisement Payment";
      }

      ?>
    </div>
  </div>
  
</div>
<?php
//Util::debug( $info );
?>
