<div class="logo-wrapper desktop-only"> <a href="<?php echo Util::mapURL('/') ?>"><img src="<?php echo Util::mapURL('images/curatalent-logo.png') ?>" alt="Curatalent"></a> </div>
    <div class="logo-wrapper mobile-only"> <a href="<?php echo Util::mapURL('/') ?>"><img src="<?php echo Util::mapURL('images/curatalent-logo-mobile.png') ?>" alt="Curatalent"></a> </div>
    <div class="flex-space"></div>
<nav class="menu">
  <?php
	
		echo '<a  href="'.Util::mapURL('/search').'">Find</a>';
		
		if($auth->isAuthenticated()){
			echo '<a  href="'.Util::mapURL('/dashboard').'">Dashboard</a>';
		}
		else{
			echo '<a  href="'.Util::mapURL('/join').'">Join</a>';
		}
		
		echo '<a  href="'.Util::mapURL('/support').'">Support</a>';
		
        echo '<a class="icon-menu-notify" id="notify"></a>';
	?>
</nav>
<div class="user-info"  > <a <?php echo !$auth->isAuthenticated() ? 'href="'.Util::mapURL('/login').'"' : '';  ?>>
  <?php 
		if($auth->isAuthenticated()){
			$customer = Session::getCustomer();
			if(!empty($customer)) {
				echo '<img alt="'.((empty($user)) ? 'Login' : $user->firstname).'" src="'.$customer['dp'].'">';
			}
			else{
				$img = 	$user->getDP();
				
				if(empty($img)){
					echo '<img alt="User" src="'.Util::mapURL('images/test-user-img.jpg').'"> ';
				}
				else{
					echo '<img alt="'.((empty($user)) ? 'Login' : $user->firstname).'" src="'.$img->i400.'"> ';
				}
			}
			
		}
		else{
			echo '<img alt="'.((empty($user)) ? 'Login' : $user->firstname).'" src="'.Util::mapURL('images/user-blank-small.png').'"> ';
		}
		
		?>
  <span>
  <?php  echo (empty($user)) ? 'Login' : $user->firstname; ?>
  </span> </a> </div>
<?php if ($auth->isAuthenticated()) { ?>
<div class="user-menu"> 
	<a href="<?php echo Util::mapURL('/dashboard/messages'); ?>">Messages</a>
    <a href="<?php echo Util::mapURL('/dashboard/bookings'); ?>">Bookings</a>
    <a href="<?php echo Util::mapURL('/dashboard'); ?>">Dashboard</a>
    <a href="<?php echo Util::mapURL('/dashboard/account'); ?>">Account</a>
  <div class="dash-sep"></div>
  <a href="<?php echo Util::mapURL('/feedback'); ?>">Feedback</a>
  <div class="dash-sep"></div>
  <a href="<?php echo Util::mapURL('/logout'); ?>">Logout</a> </div>
<?php } ?>
