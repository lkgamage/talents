<?php

$categories = $object->getCategories( true );

if ( count( $categories ) > 1 ) {
  $categories = Util::groupArray( $categories, 'skill_id' );
}

if ( empty( $categories ) ) {
 // component( 'subscription/add', $_data );
	component('dashboard/wizard_category', array('profile' => $profile->asArray()));
} else {

  ?>
<form cura="package_update" id="package_update">
  <div class="panel">
    <div class="dash-header-wrapper section padding">
      <div class="dash-sub-header txt-orange resizable"><?php echo (empty($package)) ? 'Add a Package' : 'Update Package'; ?></div>
      <input type="hidden" name="view_id" value="">
      <span class="middle ">
      <div >
        <div class="onoffswitch">
          <input type="checkbox" id="package_active" <?php echo  (empty($package) || (!empty($package) && $package->active)) == 1 ? 'checked' : ''  ?> class="onoffswitch-checkbox cura" cura="page_section_switch" name="package_active">
          <label class="onoffswitch-label" for="package_active"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
        </div>
      </div>
      </span> </div>
    <div class="hpadding">
      <div class=" ">
        <input type="hidden" value="<?php echo !empty($package) ? $package->id : '' ?>" name="package_id" id="package_id" >
        <div class="field-wrapper">
          <label class="dynamic-label dynamic-label-fold" for="package_skill_id" >Category</label>
          <?php

          if ( count( $categories ) == 1 ) {
            echo '<input type="hidden" value="' . ( !empty( $package ) ? $package->category_id : ( $categories[ 0 ][ 'id' ] ) ) . '" name="package_category_id" id="package_category_id" >';

            echo '<div class="txt-m">' . $categories[ 0 ][ 'name' ] . '</div>';
          } elseif ( !empty( $skill_id ) && isset( $categories[ $skill_id ] ) ) {

            foreach ( $categories as $i => $sk ) {

              if ( !empty( $skill_id ) && $sk[ 'skill_id' ] == $skill_id ) {

                echo '<input type="hidden" value="' . ( !empty( $package ) ? $package->category_id : ( $sk[ 'id' ] ) ) . '" name="package_category_id" id="package_category_id" >';

                echo '<div>' . $sk[ 'name' ] . '</div>';

              }
            }
          }
          else {

            ?>
          <select name="package_category_id" id="package_category_id" class="input-short">
            <?php

            foreach ( $categories as $sk ) {

              if ( !empty( $package ) && $package->category_id == $sk[ 'id' ] ) {
                echo '<option selected value="' . $sk[ 'id' ] . '">' . $sk[ 'name' ] . '</option>';
              } else {
                echo '<option value="' . $sk[ 'id' ] . '">' . $sk[ 'name' ] . '</option>';
              }
            }

            ?>
          </select>
          <?php } ?>
        </div>
        <div class="field-wrapper">
          <label class="dynamic-label" for="package_name" >Package Name</label>
          <input type="text" value="<?php echo !empty($package) ? $package->name : '' ?>" name="package_name" id="package_name" maxlength="100">
          <div class="anote">Ex: basic, Premium, Party for 25... </div>
          <a class="tooltip"><span>Add a short meaningfull package name such as,
          <div class="txt-bold">Basic, Delux or Premium</div>
          based on time, price &amp; purpose</span></a> </div>
        <div class="field-wrapper">
          <label class="dynamic-label" for="package_description" >Description</label>
          <textarea  name="package_description" id="package_description" maxlength="1000"><?php echo !empty($package) ? $package->description : '' ?></textarea>
          <div class="anote">Ex: We perform 3 dances for this package  OR 3 songs included ...</div>
          <a class="tooltip"><span>Brief description about package. Incude detail regarding what you offer for the tim and price.</span></a> </div>
        <div class="columns" >
          <div class="column col2" >
            <div class="field-wrapper">
              <label class="dynamic-label" for="package_timespan" >Session duration</label>
              <input type="text" class="input-short" value="<?php echo !empty($package) ? $package->timespan : '' ?>" name="package_timespan" id="package_timespan" maxlength="12" cura="convert_time" data-type="blur">
              <div class="anote">Ex: 30m, 2h or 1d</div>
            </div>
          </div>
          <div class="column col2" >
            <div class="field-wrapper">
              <label class="dynamic-label" for="package_items" >Number of Items (Optional)</label>
              <input type="text" class="input-short" value="<?php echo !empty($package) ? $package->items : '' ?>" name="package_items" id="package_items" maxlength="5">
              <div class="anote">No. of songs or dances (if applicable)</div>
            </div>
          </div>
        </div>
        <div class="field-wrapper">
          <label class="dynamic-label" for="package_interval" >Interval between two sessions</label>
          <input type="text"  cura="convert_time" data-type="blur"  class="input-short" value="<?php echo !empty($package) ? $package->interval : '' ?>" name="package_interval" id="package_interval" maxlength="6">
        </div>
        <div class="field-wrapper">
          <label class="dynamic-label" for="package_fee" >Price (<?php echo Session::currencyCode(); ?>)</label>
          <input  class="input-short"  type="text" value="<?php echo !empty($package) ? str_replace(',','', number_format($package->fee_local,2)) : '' ?>" name="package_fee" id="package_fee" maxlength="12">
        </div>
        <div class="field-wrapper ">
          <label class="custom-checkbox col2">
            <input <?php echo  !empty($package) && $package->fullday == 1 ? 'checked' : ''  ?> type="checkbox"  name="package_fullday" id="package_fullday" value="1" >
            Full day session<span class="checkmark"></span> </label>
          <div class="anote">If you select this option, customers can make only one booking per a day. </div>
        </div>
        <div class="field-wrapper" style="display:none">
          <label class="custom-checkbox">
            <input <?php echo  !empty($package) && $package->is_private == 1 ? 'checked' : ''  ?> type="checkbox"  name="package_is_private" id="package_is_private" value="1"  >
            Private Package<span class="checkmark"></span> </label>
          <div class="anote">Private packages are NOT available in public. But you can offer them when someone make a booking.</div>
        </div>
        <div class="button-container">
          <?php

          if ( !empty( $package ) ) {
            echo '<a href="#" cura="package_delete" data-id="' . $package->id . '">Remove package</a>';
          }

          ?>
          <div class="center"></div>
          <a class="button btn-next" cura="package_update" data-id="<?php echo !empty($package) ? $package->id : '' ?>"> <?php echo empty($package) ? 'Add Package' : 'Update' ?></a>
          <div class="gap"></div>
          <a class="button button-alt" cura="history" data-id="<?php echo !empty($package) ? $package->id : '' ?>" >Cancel</a> </div>
      </div>
    </div>
  </div>
</form>
<?php } ?>
