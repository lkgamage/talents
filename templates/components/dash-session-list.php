<?php


if(!empty($sessions)){
	
	foreach ($sessions as $s){
	?>

<div class="dash-info-box <?php echo ($s['active'] == 1) ? 'box-active' : 'box-inactive'; ?>">
  <div class="dash-info-left" >
    <div class="dash-info-name"><?php echo $s['name']; ?></div>
    <div class="dash-info-place"><?php echo $s['timespan']; ?> <span>$<?php echo $s['fee']; ?></span></div>
    <div class="dash-info-desc"><?php echo $s['description']; ?></div>
  </div>
  <a class="dash-info-menu context-menu" data-id="<?php echo $s['id']; ?>" data-menu="<?php echo $s['active'] == 1 ? 'session_menu_active' : 'session_menu_inactive' ?>" data-callback="packageMenu"></a> </div>
<?php
	}
	
}
else{
	echo "No Sessions Found";	
}


?>
