<?php


if(!empty($expences)){
	
	$total = 0;
	
	foreach ($expences as $exp){
		
	
		echo '<div class="list-select '.((!empty($exp['expences']) || (isset($exp['discount']) && $exp['discount'] > 0)) ? 'noborder' : '').'" >';
		echo '<div class="resizable">';
		
		if(!empty($exp['vendor'])){
			echo $exp['vendor']." (".$exp['skill'].")";	
			echo '<span class="min-info-desc">'.$exp['description'].' package charges</span>';
		}
		else {
			echo DataType::$EXPENCES_TYPE[$exp['expence_type']];
			echo '<span class="min-info-desc">'.$exp['description'].'</span>';
		}
		
		echo '</div>';
		echo '<div>'.currency($exp['amount']).'</div>';
		
		if($exp['type'] == 'expence' && !empty($owner)) {
			echo '<div class="trash"><a title="Remove" class="icon-trash" cura="expence_delete" data-id="'.$exp['id'].'" ></a></div>';
			
		}
		else {
			echo '<div class="trash"></div>';
		}
		echo '</div>';
		
		
		if(!empty($exp['expences'])){
			
			echo '<div class="lpadding">';
			
			$subtotal = $exp['amount'];
			
			foreach ($exp['expences'] as $bexp){
				
				
				echo '<div class="list-select noborder" >';
				echo '<div class="resizable">';
				
				echo DataType::$EXPENCES_TYPE[$bexp['expence_type']];
				echo '<span class="min-info-desc">'.$bexp['description'].'</span>';
				
				
				echo '</div>';
				echo '<div class="txt-gray">'.currency($bexp['amount']).'</div>';
				echo '<div class="trash"></div>';
				echo '</div>';
				
				$subtotal += $bexp['amount'];
		
			}
			
			if($exp['discount'] > 0){
				echo '<div class="list-select  ">
				<div class="resizable">Discount</div>
				<div class="txt-gray">('.currency($exp['discount']).')</div>
				<div class="trash"></div>
				</div>';
				
				$subtotal -= $exp['discount'];
			}
			
			echo '</div>';
			
			
			
			echo '<div class="list-select  ">
			<div class="resizable">Total Charges for '.$exp['vendor'].'</div>
			<div class="txt-bold">'.currency($subtotal).'</div>
			<div class="trash"></div>
			</div>';
			
			
			
			$total += $subtotal;			
			
		}
		else{
			
			if(isset($exp['discount']) && $exp['discount'] > 0){
				echo '<div class="list-select  ">
				<div class="resizable">Discount</div>
				<div class="txt-gray">('.currency($exp['discount']).')</div>
				<div class="trash"></div>
				</div>';
				
				$total -= $exp['discount'];
			}
			
			
			$total += $exp['amount'];	
		}
		
		
		
				
		
	}
	
	echo '<div class="list-select bg-gray txt-bold">
			<div class="resizable">Total Expences</div>
			<div class="">'.currency($total).'</div>
			<div class="trash"></div>
			</div>';
			
	if(!empty($budget)){
		
		if($budget - $total >= 0) {
			echo '<div class="list-select bg-green">';
			echo '<div class="resizable">Remaining budget</div>';
			echo '<div class="hpadding txt-light">('.round((($budget - $total)/$budget)*100).'%)</div>';
			echo '<div class="">'.currency($budget - $total).'</div><div class="trash"></div>';
		}
		else {
			echo '<div class="list-select bg-orange txt-red">';
			echo '<div class="resizable">Overdue budget</div>';
			echo '<div>'.currency($total - $budget).'</div><div class="trash"></div>';
		}
		
		
		
		
		
		echo '</div>';	
	}
			
	echo '<div class="sep" ></div>';
	
//Util::debug($expences);	
		
}
else{
	echo '<div class="padding center">No expences added.</div>';
}

?>

