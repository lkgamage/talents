<?php

$data = $bookings->getData();

if(!empty($associates)){
	$associates = Util::groupArray($associates, 'id');
}

if(!empty($data)){
	
	echo '<div class="panel">';
	
	foreach ($data as $item){
					
					
			echo ' <div class="box-row ">
						
						<div class="box-cell bg-gray stretch flex"><label class="custom-checkbox middle">
                  <input type="checkbox"  '.(isset($associates[$item['id']]) ? 'checked=""' : '').'  name="bookings['.$item['id'].']"  value="'.$item['id'].'"><span class="checkmark"></span> </label></div>
						
          <div class="box-cell cell-width-60"><div class="flex">';
		 
			 if(!empty($item['talent_id'])){
				echo  '<img src="'.$item['talent_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$item['talent_name'].'</div><div class="txt-light-gray txt-s">'.$item['skill_name'].'</div></div>';
			 }
			elseif(!empty($item['agency_id'])){
				echo  '<img src="'.$item['agency_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$item['agency_name'].'</div><div class="txt-light-gray txt-s">'.DataType::$AGENCY_TYEPS[$item['agency_type']].'</div></div>';
			}
			else{
				echo $item['booking_name'];
			}
		 
		 echo '</div></div>
          <div class="box-cell cell-width-20 ">'.Util::ToDate($item['session_start'], true).'</div>
          
		  <div class="box-cell cell-width-20 center '.DataType::$BOOKING_CLASSES[$item['status']].'"><label class="row-status">'.DataType::$BOOKING_STATUS[$item['status']].'</label></div>
        </div>';
		
		//echo '</div>';				
	}
	
	echo '</div>';
	
}
else {
	echo '<div class="vpadding">There are no bookings made for this event</div> ';	
}


?>