<?php
$profile = Session::getProfile();
$customer = Session::getCustomer();
$info = $event->getinfo();

$event_type = $event->getEventType();
$stack = Util::makeArray($info['location']['venue'], $info['location']['address'], $info['location']['city'], $info['location']['region']);

$datetime = Util::ToDate($info['event']['begins'], 1);

if(!empty($info['event']['ends'])){
	$endtime = Util::ToDate($info['event']['ends'], 1);
}

$bookings = App::getBookings(NULL, NULL, $event->id);
$appointments = $event->getAppoinments();

?>

<div>
  <div class="panel">
    <div class="padding section">
      <div class="user-header-name"><?php echo ucfirst($event->name);?></div>
      <div class="user-header-desc"><?php echo $event_type->name; ?></div>
    </div>
    <div class="flex padding">
      <div class="col2">
        <div class="block-header">Place</div>
        <div class="info-body"><?php echo array_shift($stack); ?>
          <div class="txt-light"><?php echo implode(", ", $stack); ?></div>
          <div class="txt-light"><?php echo DataType::$countries[$info['location']['country']][0]; ?></div>
        </div>
      </div>
      <div class="col2">
        <div class="block-header">Date/Time</div>
        <div class="info-body">
          <?php
	  
	  if(empty($endtime)){
			echo $datetime[0]; 
			echo '<div class="txt-light">'.$datetime[1].'</div>'; 
	  }
	  elseif($endtime[0] == $datetime[0]){
		  echo $datetime[0]; 
		  echo '<div class="txt-light">'.$datetime[1].' -  '.$endtime[1].'</div>'; 
	  }
	  else{
		  echo '<div class="flex">
      <div>'.$datetime[0].'
      <div class="txt-light">'.$datetime[1].'</div>
      </div>
      <div class="gap center middle"> - </div>
      <div>'.$endtime[0].'      
      <div class="txt-light">'.$endtime[1].'</div>
      </div>
      </div>';
	  
	  }
	  
	  
	  ?>
        </div>
      </div>
    </div>
    <div class="flex padding">
      <div class="col2">
        <div class="block-header">Budget</div>
        <div >
		<?php
		
		if(!empty($info['event']['budget'])){
			echo currency($info['event']['budget']);	
		}
		
		?>
		
        
        </div>
      </div>
      <div class="col2">
        <div class="block-header">Attendees</div>
        <div >
          <?php
		
		if(!empty($info['event']['attendees'])){
			echo number_format($info['event']['attendees']);	
		}
		
		?>
        </div>
      </div>
    </div>
    <?php if(!empty($info['event']['description'])) { ?>
    <div class="section">
      <div class="sep"></div>
      <div> <?php echo nl2br($info['event']['description']); ?> </div>
    </div>
    <div class="sep"></div>
    <?php } ?>
    <div class="section">
      <div class="button-container">
        <div class="center"> </div>
        <div class="button-wrapper">
        <a href="javascript:void(0)" cura="event_edit" data-id="<?php echo $event->id; ?>">Edit Event</a>
        <div class="gap"></div>
        <a  class="more-link right" cura="history"  >Back</a> </div></div>
    </div>
  </div>
<div class="hidden">
<div class="sep"></div>
<div class="sep"></div>
<div class="panel padding">
time line
</div>
</div>

<div class="sep"></div>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Agenda</div>
  <?php
	if($event->isEmpty('agenda')){
		echo '<a class="button" cura="agenda_create" data-id="'.$event->id.'">Create Agenda</a>';	
	}
?>
  
  </div><div id="add_agenda" class="inline_load "></div>
<div class="panel padding" id="agenda">

<?php

if(!$event->isEmpty('agenda')){
	
	component('event/agenda', array('event' => $event));
	
}
else{
	echo '<div class="center">You have not created an agenda</div>';
}

//

?>

</div>

<div class="sep"></div>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Expences</div>
  <a class="button" cura="add_expence"  data-event="<?php echo $event->id; ?>">Add Expences</a></div><div id="add_expences" class="inline_load "></div>
<div class="panel padding" id="expences">

<?php
component('event/expences', array('expences' => $event->getExpences(), 'budget' => $event->budget, 'owner' => ($customer['id'] == $event->customer_id)));

?>

</div>
<div class="sep"></div>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Bookings</div>
  <?php
  
  if(!empty($profile) && $profile['type'] == 'a'){
	  echo '<a class="button" cura="agency_talent_select" data-id="new_booking" data-cura="agency_event_booking"  data-event="'.$event->id.'">New Booking</a> ';
  }
  else{
	  echo '<a class="button" href="'.Util::mapURL('/search').'">New Booking</a> ';
  }
  
  ?>
  </div>

<div class="inline_load" id="new_booking"></div>
<div class="sep"></div>
<div class="vpadding" id="bookings">
  <?php
  if($bookings->getTotal() > 0){
	  $bookings->_pageSize = 100;
	  $bookings->view_id = 4;
	  
	  echo $bookings->getPage();
  }
  else{
	  
	  echo '<div class="center panel padding">No Bookings Found</div>';
  }
  
  
  ?>
</div>


<div class="sep"></div>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Appointments</div>
  <a class="button" cura="agency_talent_select" data-id="new_appointment" data-cura="agency_event_appointment"  data-event="<?php echo $event->id; ?>">New Appointment</a> </div>
<div id="new_appointment" class="inline_load"></div>
<div  class="vpadding" id="appointments">
  <?php
  
  if(!empty($appointments)){
	  
	  foreach ($appointments as $ap) {
		  
	 $datetime = Util::calcAppointmentTime ($ap['begins'], $ap['duration'], $ap['rest_time']);

?>
  <div cura="context" data-action="appointment_options" data-id="<?php echo $ap['id']; ?>" class="dash-info-row with-picture">
    <div class="dash-info-top">
      <div class="dash-info-pic"> <img src="<?php 
		if(!empty($ap['talent_image'])){
				echo $ap['talent_image'];
			}
			elseif($ap['talent_gender'] == 0){					
				echo Util::mapURL('/images/placeholder-girl.png');
			}
			else{
				echo Util::mapURL('/images/placeholder-boy.png');	
			}
	 ?>"> </div>
      <div class="dash-info-left middle">
        <div class="dash-info-name "><?php echo $ap['talent_name']; ?></div>
        <div class="dash-info-place"><?php echo $ap['description']; ?></div>
        <?php
	
	if($ap['confirmed'] == 1 || !empty($ap['agency_id'])){
		
		echo '<div class="'.($ap['confirmed'] == 1 ? 'appointment-confirmed' : 'appointment-scheduled').'">';
		
		if($ap['confirmed'] == 1){
			echo '<label>Confirmed</label>';
		}
		else{
			echo '<label>Scheduled</label>';	
		}
		
		if(!empty($ap['agency_id'])){
			 echo '<label class="agency">Agency</label>';	
		}
		
		echo '</div>';
		
	}
	else{
		echo '<div class="dash-info-middle "><div class="sep"></div></div>';
	}
	
	
	
	?>
        <div class="sep"></div>
      </div>
      <div class="dash-info-right middle">
        <?php
	  	echo '<div class="dash-info-date">'.$datetime['begin']['date'].'</div>';
		
		if($datetime['begin']['date'] == $datetime['end']['date']){
			echo '<div class="dash-info-time">'.$datetime['begin']['time'].' - '.$datetime['end']['time'].'</div>';
		}
	  else{
		 
		  echo '<div class="dash-info-time">'.$datetime['begin']['time'].' - </div>';
		   echo '<div class="dash-info-date">'.$datetime['end']['date'].'</div>';
		  echo '<div class="dash-info-time">'.$datetime['end']['time'].' - </div>';
	  }
	  
	  ?>
      </div>
    </div>
  </div>
  <?php	 
	  }	  
	  
  }
  else{
	  echo '<div class="center panel padding">No Appointments Found</div>';
  }
  
//Util::debug($info);
  ?>
</div>
</div>
</div>