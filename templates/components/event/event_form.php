<?php
	$begins = array('','');
	$ends = array('','');
	
	if(!empty($event)){
		
		$begins = Util::ToDate($event->begins, 1);
		$ends = !$event->isEmpty('ends') ? Util::ToDate($event->ends, 1) : array('','');
		
		$location = $event->getLocation();
	}
	
?>

<div class="panel">
  <div class="padding section">
    <div class="dash-sub-header txt-orange"><?php echo !empty($event) ? 'Update' : 'Create' ?> Event</div>
    <form cura="event_update" id="event_update">
      <input type="hidden" value="<?php echo !empty($event) ? $event->id : '' ?>" name="event_id" id="event_id" >
      <div class="field-wrapper">
        <label class="dynamic-label" for="event_name" >Name</label>
        <input type="text" value="<?php echo !empty($event) ? $event->name : '' ?>" name="event_name" id="event_name" maxlength="400">
      </div>
      <div class="field-wrapper">
        <label class="dynamic-label-fold" for="event_event_type" >Event Type</label>
        <select name="event_event_type" id="event_event_type" class="input-short" >
          <?php
		$types = App::getEventTypes();
		
		foreach ($types as $id => $name){
			
			if(!empty($event) && $event->event_type == $id){
				echo '<option selected value="'.$id.'">'.$name.'</option>';
			}
			else{
				echo '<option value="'.$id.'">'.$name.'</option>';
			}
			
		}
		
	?>
        </select>
      </div>
      <div class="field-wrapper">
        <label class="dynamic-label" for="event_venue" >Hotel, Reception hall, Auditorium or City</label>
        <input class="addresspick" type="text" data-place="<?php echo !empty($location) ? $location->place_id : '' ?>" value="<?php echo !empty($location) ? $location->display : '' ?>" name="event_venue" id="event_venue" maxlength="400" placeholder="">
      </div>
     <div class="sep"></div>
        <div class="flex">
          <div class="col2">
           <div class="field-wrapper">
            <label class="dynamic-label-fold">Event Start</label>
            <input type="text" class="datepick input-short" id="event_begins_date" name="event_begins_date" value="<?php echo $begins[0];   ?>" data-value="<?php echo $begins[0];   ?>" placeholder="Date">
            <div class="sep"></div>
            <input type="text" class="timepick input-short" id="event_begins_time" name="event_begins_time"   value="<?php echo $begins[1];   ?>" data-value="<?php echo $begins[1];   ?>" placeholder="Time">
            </div>
          </div>
          <div class="col2 non-ful-day">
           <div class="field-wrapper">
            <label class="dynamic-label-fold">Event End</label>
            <input type="text" class="datepick input-short" id="event_ends_date" name="event_ends_date" value="<?php echo $ends[0];   ?>" data-value="<?php echo $ends[0];   ?>" placeholder="Date">
            <div class="sep"></div>
            <input type="text" class="timepick input-short" id="event_ends_time" name="event_ends_time"   value="<?php echo $ends[1];   ?>" data-value="<?php echo $ends[1];   ?>" placeholder="Time">
            </div>
          </div>
        </div>
      <div class="field-wrapper">
	<label class="dynamic-label" for="event_budget" >Budget (<?php echo Session::currencyCode()  ?>)</label>
	<input type="text" class="input-short" value="<?php echo !empty($event) ? toCurrency($event->budget) : '' ?>" name="event_budget" id="event_budget" maxlength="15">
</div>
<div class="field-wrapper">
	<label class="dynamic-label" for="event_num_attendees" >Number of  Attendees</label>
	<input type="text" class="input-short" value="<?php echo !empty($event) ? $event->num_attendees : '' ?>" name="event_num_attendees" id="event_num_attendees" maxlength="4">
</div>
      
      <div class="field-wrapper">
        <label class="dynamic-label" for="event_description" >Description</label>
        <textarea class="input-notes" name="event_description" id="event_description" maxlength="1000"><?php echo !empty($event) ? $event->description : '' ?></textarea>
      </div>
      <div class="field-wrapper">
        <div>
          <label class="custom-checkbox">
            <input <?php echo (!empty($event) && $event->charitable == 1) ? 'checked=""' : '' ?> type="checkbox" name="event_charitable" id="event_charitable" value="1"  >
            This is a charitable event<span class="checkmark"></span> </label>
        </div>
      </div>
      
    </form>
    <div class="sep"></div>
    <div class="button-container">
      <?php if(!empty($booking) && !empty($request) && $request->agency_id == $booking->agency_id && $booking->status == DataType::$BOOKING_PENDING){
		echo '<a cura="event_remove" data-id="'.$request->id.'"  data-booking="'.$booking->id.'">Remove Event</a>';
	}?>
      <div class="center"></div>
      <div class="button-wrapper">
      <a class="button btn-next" cura="event_update" > <?php echo empty($event) ? 'Create' : 'Update' ?> Event</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="<?php  echo (!empty($cura) ? $cura : 'history') ?>" data-id="<?php echo (!empty($cura_id)) ? $cura_id : 'events' ?>"  >Cancel</a> </div></div>
  </div>
</div>
