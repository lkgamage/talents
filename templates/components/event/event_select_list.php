<div class="panel padding">
<div class="block-header txt-gray">Select an event</div>

<?php
$uid = uniqid();

echo '<div id="'.$uid.'">';

if($events->getTotal() > 0) {
	
	$events->_pagesize = 50;
	$events->_component = 'event/event_select';
	$events->no_date = true;

	$events->cura = 'get_event_block';
	
	echo $events->getPage();
	
}

echo '</div>';
echo $events->manualPaginate($uid,'more_'.$uid);
?>
<div><a href="javascript:void(0)" id="more_<?php echo $uid; ?>">More Events</a></div>
</div>
<div class="sep"></div>
<div><a  href="javascript:void(0)" cura="clear_event"  >Remove event from the booking</a></div>
