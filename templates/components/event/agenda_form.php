<?php


if(empty($item)) {

	$begins = Util::ToDate($event->begins, 1);
	$ends = !$event->isEmpty('ends') ? Util::ToDate($event->ends, 1) : array('','');
}
else{
	$begins = Util::ToDate($item->begins, 1);
	$ends = Util::ToDate($item->ends, 1) ;
}

// capture post data
if(!empty($_POST['begins']) && !empty($_POST['ends'])){
	
	$pbegins = Util::ToSysDate($_POST['begins']);
	$pends = Util::ToSysDate($_POST['ends']);

	if(!empty($pbegins)){
		$begins = Util::ToDate($pbegins, 1);
	}
	
	if(!empty($pends)){
		$ends = Util::ToDate($pends, 1);
	}
}



?>
<div class="panel padding section" cura="scroll" data-type="load">
  <div class="sep"></div>
  <div class="section-header txt-black nopadding"><?php echo empty($item) ? 'Add New Item' : 'Update Item'; ?></div>
  <div class="sep2"></div>
  <form cura="event_item_update" id="event_item_update">
    <input type="hidden" value="<?php echo !empty($item) ? $item->id : '' ?>" name="item_id" id="item_id" >
    <input type="hidden" value="<?php echo !empty($item) ? $item->event_id : $event->id ?>" name="item_event_id" id="item_event_id" >
    <div class="flex">
      <div class="col2">
        <div class="field-wrapper">
          <label class="dynamic-label-fold">From</label>
          <input type="text" class="timepick input-short" id="item_begins_time" name="item_begins_time"   value="<?php echo $begins[1];   ?>" data-value="<?php echo $begins[1];   ?>" placeholder="Time">
          <?php if(empty($ends[0]) || $begins[0] != $ends[0]){  ?>
          <div class="sep"></div>
          <input type="text" class="datepick input-short" id="item_begins_date" name="item_begins_date" value="<?php echo $begins[0];   ?>" data-value="<?php echo $begins[0];   ?>" placeholder="Date">
          <?php } else{ ?>
          <input type="hidden" name="item_begins_date" value="<?php echo $begins[0]; ?>">
          <?php  } ?>
          
        </div>
      </div>
      <div class="col2 non-ful-day">
        <div class="field-wrapper">
          <label class="dynamic-label-fold">To</label>
          <input type="text" class="timepick input-short" id="item_ends_time" name="item_ends_time"   value="<?php echo $ends[1];   ?>" data-value="<?php echo $ends[1];   ?>" placeholder="Time">
           <?php if(empty($ends[0]) || $begins[0] != $ends[0]){  ?>
          <div class="sep"></div>
          <input type="text" class="datepick input-short" id="item_ends_date" name="item_ends_date" value="<?php echo !empty($ends[0]) ? $ends[0] :  $begins[0]  ?>" data-value="<?php echo !empty($ends[0]) ? $ends[0] :  $begins[0]  ?>" placeholder="Date">
          
           <?php } else{ ?>
          <input type="hidden" name="item_ends_date" value="<?php echo $begins[0]; ?>">
          <?php  } ?>
        </div>
      </div>
    </div>
    <div class="field-wrapper">
      <label class="dynamic-label" for="item_title" >Title</label>
      <input type="text" value="<?php echo !empty($item) ? $item->title : '' ?>" name="item_title" id="item_title" maxlength="400">
    </div>
    <div class="field-wrapper">
      <label class="dynamic-label" for="item_description" >Description</label>
      <textarea name="item_description" id="item_description" maxlength="1000" ><?php echo !empty($item) ? $item->description : '' ?></textarea>
      
    </div>
    <div class="field-wrapper">
      <label class="dynamic-label" for="item_note" >Note</label>
      <textarea name="item_note" id="item_note" maxlength="1000"><?php echo !empty($item) ? $item->note : '' ?></textarea>

      <div class="anote">Only you can see this note</div>
    </div>
    
    <div class="field-wrapper">
    <?php
	
	if(!empty($item)){
		
		$bookings = $item->getBookings();
		echo '<div class="txt-m">Bookings</div>';		
		echo '<div class="sep"></div><div id="agenda_linking">';
		
		if(!empty($bookings)) {
			
			echo '<div class="panel">';
	
			foreach ($bookings as $booking){
					
			
			echo ' <div class="box-row ">
						

						
          <div class="box-cell cell-width-60"><div class="flex">';
		 
			 if(!empty($booking['talent_id'])){
				echo  '<img src="'.$booking['talent_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$booking['talent_name'].'</div><div class="txt-light-gray txt-s">'.$booking['skill_name'].'</div></div>';
			 }
			elseif(!empty($booking['agency_id'])){
				echo  '<img src="'.$booking['agency_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$booking['agency_name'].'</div><div class="txt-light-gray txt-s">'.DataType::$AGENCY_TYEPS[$booking['agency_type']].'</div></div>';
			}
			else{
				echo $booking['booking_name'];
			}
		 
		 echo '</div></div>
          <div class="box-cell cell-width-20 ">'.Util::ToDate($booking['session_start'], true).'</div>
          
		  <div class="box-cell cell-width-20 center '.DataType::$BOOKING_CLASSES[$booking['status']].'"><label class="row-status">'.DataType::$BOOKING_STATUS[$booking['status']].'</label></div>
        </div>';
							
	}
	
			echo '</div>';
			
			echo '<div class="sep"></div><a class="more-link" href="javascript:void(0)" cura="agenda_linking" data-event="'.(!empty($item) ? $item->event_id : $event->id).'"  data-id="'.(!empty($item) ? $item->id : '' ).'" >Change</a>
		</div>';
		}
		else{
			echo '<a href="javascript:void(0)" cura="agenda_linking" data-event="'.(!empty($item) ? $item->event_id : $event->id).'"  data-id="'.(!empty($item) ? $item->id : '' ).'" >Link to a booking</a><div id="agenda_linking"></div>';
		}
	
		
	}
	else{
		echo '<a href="javascript:void(0)" cura="agenda_linking" data-event="'.(!empty($item) ? $item->event_id : $event->id).'"  data-id="'.(!empty($item) ? $item->id : '' ).'" >Link to a booking</a><div id="agenda_linking"></div>';
	}
	
	?>

    </div>
    <div class="button-container">
      <div class="center"></div>
      <div class="button-wrapper"> <a class="button btn-next" cura="event_item_update" > <?php echo empty($event) ? 'Create' : 'Update' ?> Agenda</a>
        <div class="gap"></div>
        <a class="button btn-next button-alt" cura="inline_load_cancel">Cancel</a> </div>
    </div>
  </form>
</div>
<div class="sep"></div>
