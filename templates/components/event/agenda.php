<div class="flex">
  <div class="resizable"></div>
  <a href="javascript:void(0)" data-event="<?php echo $event->id; ?>" cura="add_agenda_item">Add Item</a></div>
<div class="agenda">
  <?php
$customer = Session::getCustomer();
$agenda = $event->getAgenda();
$schedule = $agenda->getSchedule();

$ev_begin = Util::ToDate($event->begins, 1);
$ev_ends = Util::ToDate($event->ends, 1);

//print_r($ev_begin);
//print_r($ev_ends);
$show_date =  !empty($ev_ends) ? ($ev_begin[0] != $ev_ends[0]) :  true;

if($show_date){
	$beforehours = Util::ToDate(date("Y-m-d H:i:s", strtotime($event->ends) - 3660), 1);

	if($beforehours[0] == $ev_begin[0]){
		$show_date = false;
	}
}

//var_dump($show_date);

if(!empty($schedule)) { 

	foreach ($schedule as $item) {
		
		$begin = Util::ToDate($item['begins'], 1);
		$end = Util::ToDate($item['ends'], 1);
		
		if(!empty($item['id'])) {
?>
  <div class="agenda-row">
    <div class="agenda-time">
      <div class="flex">
        <div class="column-2 no padding">
          <?php 
			echo $begin[1];
			
			if($show_date){
				$d = explode(',', $begin[0]);
				echo '<span>'.$d[0].'</span>';
			}
			
			 ?>
        </div>
        <div class="gap center midddle">-</div>
        <div class="column-2  padding">
          <?php 
				echo $end[1];
				
				if($show_date){
					$d = explode(',', $end[0]);
					echo '<span>'.$d[0].'</span>';
				}
			 ?>
          <span></span></div>
      </div>
    </div>
    <div class="agenda-desc resizable max-600">
      <div class="txt-bold title"><?php echo $item['title']; ?></div>
      <div class="txt-gray desc"><?php echo nl2br($item['description']); ?></div>
      <?php 
		   		if(!empty($item['note']) && $customer['id'] == $event->customer_id){ 
		   			echo '<div class="txt-light note">'.nl2br($item['note']).'</div>';
				}
		    
            ?>
    </div>

    
      <?php
		if(!empty($item['bookings'])){
			
			echo '<div class="agenda-talents resizable">';
						
			foreach ($item['bookings'] as $booking) {
				
				echo ' <div class="box-row ">
					<div class="box-cell">
					  <div class="flex">';
		  
			  if(!empty($booking['talent_id'])){
					echo  '<div><img src="'.$booking['talent_image'].'"></div><div class="gap1"></div>';
					
					echo '<div class="middle"><div>'.$booking['talent_name'].'</div><div class="txt-light-gray">'.$booking['skill_name'].'</div>';
					
					if(empty($print)) {
						echo '<div class="'.DataType::$BOOKING_CLASSES[$booking['status']].'"><label class="row-status">'.DataType::$BOOKING_STATUS[$booking['status']].'</label></div>';
					}
					
					echo '</div>';
				 }
				elseif(!empty($booking['agency_id'])){
					echo  '<img src="'.$booking['agency_image'].'"><div class="gap1"></div>';
					
					echo '<div class="middle"><div>'.$booking['agency_name'].'</div><div class="txt-light-gray txt-s">'.DataType::$AGENCY_TYEPS[$booking['agency_type']].'</div>';
					
					if(empty($print)) {
						echo '<div class="'.DataType::$BOOKING_CLASSES[$booking['status']].'"><label class="row-status">'.DataType::$BOOKING_STATUS[$booking['status']].'</label></div>';
					}
					
					echo '</div>';
				}
				else{
					echo $booking['booking_name'];
				}
				
				 echo '</div> </div> </div>';
		  
			}
         
		 echo '</div>';
			
		}
		
		?>
     
    
    <a class="context-menu" cura="context" data-action="agenda_option" data-id="<?php echo $item['id']; ?>"></a> </div>
  <?php

		}
		elseif($item['width'] >= 5) {
			?>
  <div class="agenda-row-free">
    <div class="agenda-time">
      <div class="flex">
        <div class="column-2 no padding">
          <?php 
			echo $begin[1];
			
			if($show_date){
				$d = explode(',', $begin[0]);
				echo '<span>'.$d[0].'</span>';
			}
			
			 ?>
        </div>
        <div class="gap center midddle">-</div>
        <div class="column-2  padding">
          <?php 
				echo $end[1];
				
				if($show_date){
					$d = explode(',', $end[0]);
					echo '<span>'.$d[0].'</span>';
				}
			 ?>
        </div>
      </div>
    </div>
    <div class="agenda-desc resizable max-600">
      <div><a  href="javascript:void(0)" cura="add_agenda_item" data-event="<?php echo $event->id; ?>" data-begins="<?php echo $begin[0].' '.$begin[1] ?>" data-ends="<?php echo $end[0].' '.$end[1] ?>" >Add an item to <?php echo Util::ToTime($item['width']) ?> free slot</a></div>
    </div>
  </div>
  <?php 
		}
	}

}
else{
	
	echo '<div>No items in agenda. <a cura="add_agenda_item" data-id="'.$event->id.'">Add an item</a> </div>';
}


//Util::debug($schedule);
//Util::debug($agenda->timeline);
?>
</div>
<?php if(!empty($schedule)) {
echo '<div class="sep"></div><div id="agenda_share">'; 
	
component('event/agenda_share_names', array('event' => $event));

echo '</div>';
 } ?>
