<form cura="agenda_share_update" id="agenda_share_update">
<input type="hidden" name="event_id" value="<?php echo $event->id; ?>">
  <div class="sep"></div>
  <?php

$data = $bookings->getData();

$shares = $event->getAgendaShares();

if(!empty($shares)){
	$shares = Util::groupArray($shares, 'customer_id');	
}


if(!empty($data)){
	
	echo '<div class="txt-m">Select partners to share the agenda</div>
	<div class="sep"></div><div class="panel">';
	
	foreach ($data as $item){
					
					
			echo ' <div class="box-row ">
						
						<div class="box-cell bg-gray stretch flex"><label class="custom-checkbox middle">
                  <input type="checkbox"  '.( (isset($shares[$item['talent_customer_id']]) || isset($shares[$item['agency_customer_id']]) ) ? 'checked=""' : '').'  name="bookings['.$item['id'].']"  value="'.$item['id'].'"><span class="checkmark"></span> </label></div>
						
          <div class="box-cell "><div class="flex">';
		 
			 if(!empty($item['talent_id'])){
				echo  '<img src="'.$item['talent_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$item['talent_name'].'</div><div class="txt-light-gray txt-s">'.$item['skill_name'].'</div></div>';
			 }
			elseif(!empty($item['agency_id'])){
				echo  '<img src="'.$item['agency_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$item['agency_name'].'</div><div class="txt-light-gray txt-s">'.DataType::$AGENCY_TYEPS[$item['agency_type']].'</div></div>';
			}
			else{
				echo $item['booking_name'];
			}
		 
		 echo '</div></div>
          
          
		  
        </div>';
		
		//echo '</div>';				
	}
	
	echo '</div>';
	
	echo ' <div class="button-container">
      <div class="center"></div>
      <div class="button-wrapper"> <a class="button btn-next" cura="agenda_share_update" >Share</a>
        <div class="gap"></div>
        <a class="button btn-next button-alt" data-event_id="'.$event->id.'" cura="agenda_share_cancel">Cancel</a> </div>
    </div>';
	
}
else {
	echo '<div class="vpadding">No event partners found. Make bookings for this event.</div> ';	
}


?>
  <div class="sep"></div>
</form>
