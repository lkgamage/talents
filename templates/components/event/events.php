<?php

$cn = Session::getCountry();

if(!empty($data)) {
	
	$data = Util::groupMultiArray($data, 'event_date');
	
	foreach ($data as $date => $items) {
		
		if(empty($no_date)) {
			echo '<div class="center txt-l">'.Util::ToDate($date, NULL, NULL, NULL, true).'</div><div class="sep"></div>';
		}
		
		
		foreach ($items as $item) {
			
		$begintime = Util::ToDate($item['begins'], 1);
		$endtime = (!empty($item['ends'])) ? Util::ToDate($item['ends'], 1) : NULL;
	?>
<div  cura="<?php echo isset($cura) ? $cura :'event_view'; ?>" data-id="<?php echo $item['id']; ?>" class="dash-info-row hover">
  <div class="dash-info-top">
    <div class="dash-info-left">
      <div class="dash-info-name txt-blue"><?php echo $item['name']; ?></div>
      <div class="dash-info-desc "><span><?php echo DataType::$EVENT_CATEGORIES[$item['event_type']][0]; ?></span><span>
      <?php
	  
	  if(!isset($item['bookings'])) {
		  
		  
	  }
	 elseif( $item['bookings'] == 0){
		 echo 'No bookings';
	  }
	  else if($item['bookings'] == 1){
		echo '1 booking';  
	  }
	  else{
		echo $item['bookings'].' bookings';	  
	  }
	  
	  ?>
      </span></div>
      <div class="sep"></div>
      <?php
	  if(!empty($item['venue'])){
		 echo ' <div class="dash-info-desc">'.$item['venue'].'</div>'; 
	  }
	  ?>
      <div class="dash-info-place"><?php 
	  
	   echo '<span>';
	  
	  echo Util::makeString($item['city'], $item['region']);
	  
	  if($cn != $item['country']) {
	 	echo  ', '.DataType::$countries[$item['country']][0];
	  }
	   echo '</span>';
	   
	  if(!empty($item['agency_id'])){
		  echo '<span>Organized by '.$item['agency_name'].'</span>';
	  }
	   ?></div>
    </div>
    <div class="dash-info-right">
   
      <div class="dash-info-date"><?php echo $begintime[0]; ?></div>
      <div class="dash-info-time"><?php echo $begintime[1]; ?></div>
     
      <?php
	  if(!empty($endtime)){
		
		  if($begintime[0] != $endtime[0]){
			  echo '<div>';
			  echo '<div class="dash-info-date">- '.$endtime[0].'</div>';
			  echo '<div class="dash-info-time">'.$endtime[1].'</div>';
			   echo '</div>';
		  }
		  else{
		 	 echo '<div class="dash-info-time">- '.$endtime[1].'</div>';
		  }
	  }
	  
	  ?>
    </div>
  </div>
  <div class="dash-info-middle "><input type="hidden" name="event_id" value="<?php echo $item['id']; ?>">
  </div>
</div>

<?php	
		}
	}	
}

//Util::debug($data);
?>
