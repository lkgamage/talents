
<?php
$shares = $event->getAgendaShares();

if(!empty($shares)){
	
	$contacts = array();
	
	foreach ($shares as $s){
		
		if(!empty($s['talent_id'])){
		/*	$contacts[] = array(
				'name' => $s['talent_name'],
				'image' => $s['talent_image']
			);*/
			$contacts[]	=  $s['talent_name'];	
		}
		elseif(!empty($s['agency_id'])){
			/*$contacts[] = array(
				'name' => $s['agency_name'],
				'image' => $s['agency_image']
			);*/
			$contacts[]	=  $s['agency_name'];	
		}		
	}
	
	if(count($contacts) > 1){
		
		$last = array_pop($contacts);
		echo "Shared with <span>".implode('</span>, <span>', $contacts)."</span> and <span>".$last."</span>.";	
		
	}
	else{
		echo "Shared with <span>".$contacts[0].".</span>";	
	}
	
	echo '&nbsp;&nbsp;&nbsp;<a class="dash-link" href="javascript:void(0)" cura="agenda_share" data-event="'.$event->id.'">Change</a>';
}
else{
	echo '<a  href="javascript:void(0)" cura="agenda_share" data-event="'.$event->id.'" >Share event  agenda</a>';
}
?>