<?php



	if(!empty($data)) {
		
		$data = Util::groupMultiArray($data, 'event_date');
		
		foreach ($data as $date => $items) {
			
			if(empty($no_date)) {
				echo '<div class="center txt-l">'.Util::ToDate($date, NULL, NULL, NULL, true).'</div><div class="sep"></div>';
			}
			
			
			foreach ($items as $item) {
				
			$begintime = Util::ToDate($item['begins'], 1);
			$endtime = (!empty($item['ends'])) ? Util::ToDate($item['ends'], 1) : NULL;
		?>
	<div cura="<?php echo isset($cura) ? $cura :'event_view'; ?>" data-id="<?php echo $item['id']; ?>" class="dash-info-row selectable">
	  <div class="dash-info-top">
		<div class="dash-info-left">
		  <div class="dash-info-name txt-blue"><?php echo $item['name']; ?></div>
		  <div class="dash-info-place"><span><?php echo $item['type_name']; ?></span>
		  <?php
		   if(!empty($item['venue'])){
			 echo '<span>'.$item['venue'].'</span>'; 
		  }
		  ?>
		  </div>
		  <div class="dash-info-place"><?php echo Util::makeString($item['city'], $item['region'],  DataType::$countries[$item['country']][0]); ?></div>
		</div>
		<div class="dash-info-right center">

		  
		  <?php
          echo '<div class="dash-info-date">'.$begintime[0].'</div>';
		  
		  if(!empty($endtime)){
			
			  if($begintime[0] != $endtime[0]){
				  
				  echo '<div class="dash-info-date">- '.$endtime[0].'</div>';
				  echo '<div class="dash-info-time">'.$endtime[1].'</div>';
			  }
			  else{
				 echo '<div class="dash-info-time nowrap">'.$begintime[1].'- '.$endtime[1].'</div>';
			  }
		  }
		  else{
			 
		  	 echo '<div class="dash-info-time">'.$begintime[1].'</div>'; 
		  }
		  
		  ?>
		</div>
	  </div>
	  <div class="dash-info-middle ">
	  </div>
	
	</div>
	<?php	
			}
		}	
	}


//Util::debug($data);
?>
