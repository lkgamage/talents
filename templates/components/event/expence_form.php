<div class="panel padding">
<div  class=" max-600 hpadding">
<form id="expence_form" cura="save_expence">

<input type="hidden" value="<?php echo !empty($expence) ? $expence->id : '' ?>" name="expence_id" id="expence_id" >

<input type="hidden" value="<?php echo (!empty($_POST['event'])  && is_numeric($_POST['event'])) ? $_POST['event'] : '' ?>" name="expence_event_id" id="expence_event_id" >

<input type="hidden" value="<?php echo (!empty($_POST['booking'])  && is_numeric($_POST['booking'])) ? $_POST['booking'] : '' ?>" name="expence_booking_id" id="expence_booking_id" >

<div class="field-wrapper">
	<label class="dynamic-label" for="expence_type" >Expence Type</label>
    <select name="expence_type" id="expence_type">
    <?php
	foreach (DataType::$EXPENCES_TYPE as $id => $name){
		
		echo ' <option value="'.$id.'">'.$name.'</option>';
		
	}
	
	?>
    </select>
	
</div>

<div class="field-wrapper">
	<label class="dynamic-label" for="expence_description" >Description</label>
    <textarea name="expence_description" id="expence_description" maxlength="400" ><?php echo !empty($expence) ? $expence->description : '' ?></textarea>	
</div>

<div class="field-wrapper">
	<label class="dynamic-label" for="expence_amount" >Amount (<?php echo Session::currencyCode()  ?>)</label>
	<input type="text" value="<?php echo !empty($expence) ? $expence->amount : '' ?>" name="expence_amount" id="expence_amount" maxlength="12">
</div>
<div class="button-container">
          <div class="center"> </div>
          <a class="button" cura="save_expence" >Add Expence</a>
          <div class="gap"></div>
          <a class="button btn-next button-alt" cura="inline_load_cancel" >Cancel</a> </div>

</form></div></div>
<div class="sep"></div>