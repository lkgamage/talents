<?php
$skill = new Skill($category['skill_id']);
$packages = $skill->getPackages();
?>
<div class="panel padding">
<div class="txt-l txt-orange">Please select a subscription plan</div>
	<div class="vpadding">Select a plan for <span class="txt-blue"><?php echo $skill->name ?></span> category. You may change your plan later.</div>
	<div class="sep"></div>
<?php
	
	if ( $profile['is_agency'] == 0 ) {
		
		//echo '<div class="txt-m">Select a plan</div>';
		
		component( 'subscription/packages', array(
			'skill' => $skill,
			'packages' => $packages,
			'subscriptions' => array()
		  ) );
		

	} else {
		  echo '<div class="txt-m">Select a subscription package</div>';
		  $skill = App::getAgencyCategory();
		  $packages = $skill->getPackages();
		  component( 'subscription/packages', array(
			'skill' => $skill,
			'packages' => $packages,
			'subscriptions' => array()
		  ) );
		}
?>
	
</div>
<div class="sep"></div>