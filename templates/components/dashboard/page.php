<?php
$parts = array(
	'a' => 'About',
	'b' => 'Service',
	'c' => 'Packages',
	'd' => 'Achivements',
	'e' => 'Showcase',
	'f' => 'Social'
);


?><div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Public Page</div>
</div>
<div id="contents">
  <div class="panel">
    <div id="page_parts">
      <?php for($i = 0; $i < 5; $i++) { ?>
      <div class="page-part">
        <div class="part-header flex">
          <div class="part-handle"></div>
          <div class="resizable part-name">Part name</div>
          <div class="part-enable">
            <div class="onoffswitch">
              <input type="checkbox" name="donorfee_charge" class="onoffswitch-checkbox" id="donorfee_charge"  value="1">
              <label class="onoffswitch-label" for="donorfee_charge"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
            </div>
          </div>
        </div>
        <div class="part-body  padding section columns">
          <div class="part-form column-41">
            <input type="hidden" value="<?php echo !empty($part) ? $part->order : '' ?>" name="part_order" class="part_order" >
            <div class="field-wrapper">
              <label class="dynamic-label" for="part_title" >Title</label>
              <input type="text" value="<?php echo !empty($part) ? $part->title : '' ?>" name="part_title" class="part_title" maxlength="1000">
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="part_body" >Content</label>
              <textarea name="part_body" class="part_body" maxlength="10000"><?php echo !empty($part) ? $part->body : '' ?></textarea>
            </div>
          </div>
          <div class="part-image column-4">
            <input type="hidden" value="<?php echo !empty($part) ? $part->image_id : '' ?>" name="part_image_id" id="part_image_id" >
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
