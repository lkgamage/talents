<?php
$buffer = $agency->getPendingRequests();
?>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Talents</div>
  <a class="button" cura="dash_menu" data-id="talents">VIEW AGENCY TALENTS</a></div>
<div id="contents">
  <div class="panel fullheight">
    <div class="sep"></div>
    <div class="padding section" >
      <div class="flex">
        <div class="resizable info-header txt-orange txt-m">Talent Requests</div>
        <div></div>
      </div>
      <div class="sep"></div>
<?php
$uid = uniqid();

if($buffer->getTotal() > 0) {

	echo '<div id="'.$uid.'">';

	echo $buffer->getPage();

	echo '</div>';
	?>
<div class="button-container">
  <div class="center"> <a class="button btn-next" id="more_<?php echo $uid ?>" data-label="VIEW MORE " > VIEW MORE </a> </div>
</div>
<div>
<?php
echo $buffer->manualPaginate($uid,'more_'.$uid);
?>
</div>
<?php
}
else{
	echo '<div class="padding center">No requests found</div>';	
}

//Util::debug( $buffer->_datasql);
?>
      
    </div>
  </div>
</div>


