<?php
$profile_count = $user->getActiveProfilesCount();
?>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange notoppadding">New Profile</div>
</div>
<div>
  <?php if($profile_count > 0) { ?>
  <div class="panel padding">
    <div class="flex section-header ">
      <div class="rpadding"><a class="crown"></a></div>
      <div class="txt-black resizable middle">Premium Profile </div>
    </div>
    <div>You are going to create a premium profile.  You may subscribe for paid plans only. </div>
    <div class="sep"></div>
    <div class="flex">
      <div class="resizable"></div>
      <a class="more-link dash-link">Read more about premium profiles </a> </div>
  </div>
  <div class="sep"></div>
  <?php } ?>
  <div class="panel padding" id="profile_type">
    <div class="section-header txt-black">Profile Type</div>
    <div class="columns">
      <div class="column-2 left-content">
        <div class="info-body">Artist, Performer, Performing band or group, Event/party service provider or business, Clothing/Foods/Equipment supplier, Photography, Salons, Activity Planners</div>
        <div class="sep"></div>
        <div class="center"><a class="button" cura="switch_profile_type" data-id="talent">Talent Profile</a></div>
      </div>
      <div class="column-2 right-content">
        <div class="info-body">Event management, Activity planning,  Advertising ,  Fashion Modeling, Marketing/ Promotional, Public relations and Social Media agency</div>
        <div class="sep"></div>
        <div class="center"><a class="button"  cura="switch_profile_type" data-id="agency" >Agency Profile</a></div>
      </div>
    </div>
  </div>
  <div id="talent_profile" class="hidden">
    <form id="signup_profile_form" cura="submit_signup_profile" >
      <div class="panel padding">
        <div class="section-header txt-black">Talent Profile</div>
        <div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="talent_name" >Public Name/Business Name</label>
            <input class="input-short" type="text" value="" name="talent_name" id="talent_name" maxlength="400">
            <div class="anote">This is displaying in your public page.<br>
              Your well-known nickname, band name, dancing group  name or business name</div>
          </div>
          <div id="gender-box-">
            <div class="field-wrapper">
              <label  >Gender</label>
              <div class="sep" ></div>
              <div class="flex option-list wrap">
                <div class="col4" style="padding-bottom:0;">
                  <label class="custom-checkbox" title="Male">
                    <input type="radio" name="gender" class="gender" value="1" >
                    Male <span class="checkmark"></span> </label>
                </div>
                <div class="col4">
                  <label class="custom-checkbox" title="Female">
                    <input type="radio" name="gender" class="gender" value="0" >
                    Female <span class="checkmark"></span> </label>
                </div>
                <div class="col4" style="padding-bottom:0; display: none">
                  <label class="custom-checkbox"  title="Transgender Male">
                    <input type="radio" name="gender" class="gender" value="3"  >
                    T-Male <span class="checkmark"></span> </label>
                </div>
                <div class="col4" style="display: none">
                  <label class="custom-checkbox" title="Transgender Female">
                    <input type="radio" name="gender" class="gender" value="2" >
                    T-Female <span class="checkmark"></span> </label>
                </div>
              </div>
              <label style="display:none" id="gender-error" class="error" >Please select your gender</label>
            </div>
            <div class="field-wrapper max-600">
              <label  for="user_dob"  >Date of Birth</label>
              <div class="flex">
                <?php
                $dob = $user->dob;
                $dobp = array( 0, 0, 0 );

                if ( !empty( $dob ) ) {
                  $dobp = explode( '-', $dob );

                  $dobp[ 1 ] = ( int )$dobp[ 1 ];
                  $dobp[ 2 ] = ( int )$dobp[ 2 ];
                }
                ?>
                <select name="dob_month" id="dob_month">
                  <?php
                  foreach ( DataType::$months as $i => $name ) {

                    echo '<option value="' . $i . '" ' . ( $dobp[ 1 ] == $i ? 'selected' : '' ) . ' >' . $name . '</option>';

                  }

                  ?>
                </select>
                <select name="dob_day" id="dob_day">
                  <?php

                  for ( $i = 1; $i <= 31; $i++ ) {
                    echo '<option value="' . $i . '" ' . ( $dobp[ 2 ] == $i ? 'selected' : '' ) . ' >' . $i . '</option>';
                  }

                  ?>
                </select>
                <select name="dob_year" id="dob_year">
                  <?php
                  $dy = date( 'Y' );
                  for ( $i = ( $dy - 5 ); $i >= $dy - 76; $i-- ) {
                    echo '<option value="' . $i . '" ' . ( $dobp[ 0 ] == $i ? 'selected' : ( $dobp[ 0 ] == 0 && $dy - 25 == $i ? 'selected' : '' ) ) . ' >' . $i . '</option>';
                  }


                  ?>
                </select>
              </div>
              <label style="display:none" id="dob-error" class="error" >Date is not valid</label>
            </div>
            <div class="field-wrapper">
              <label  for="user_dob"  >Currency</label>
              <select name="currency" class="input-short">
                <?php
                foreach ( DataType::$currencies as $code => $name ) {
                  echo '<option value="' . $code . '" ' . ( ( $country == 'LK' && $code == 'LKR' ) ? 'selected' : '' ) . ' >' . $name . '</option>';
                }

                ?>
              </select>
            </div>
          </div>
          <div class="button-container">
            <div class="center"> </div>
            <div class="button-wrapper"> <a class="button" cura="submit_signup_profile">Create</a>
              <div class="gap"></div>
              <a class="button btn-next button-alt"  cura="dash_menu" data-id="account" >Cancel</a></div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div id="agency_profile" class="hidden">
    <form id="signup_agency_form" cura="submit_agency_profile" >
      <div class="panel padding">
        <div class="section-header txt-black">Agency Profile</div>
        <div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="agency_name" >Agency Name</label>
            <input class="input-short" type="text" value="" name="agency_name" id="agency_name" maxlength="400">
          </div>
          <div class="field-wrapper">
            <label class="dynamic-label dynamic-label-fold" for="agency_type" >Agency Type</label>
            <select name="agency_type" id="agency_type"  class="input-short">
              <?php

              foreach ( DataType::$AGENCY_TYEPS as $id => $name ) {

                echo '<option value="' . $id . '">' . $name . '</option>';

              }

              ?>
            </select>
          </div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="address" >Office Address</label>
            <input type="text" value="" name="address" class="input-short" id="address input-short" maxlength="100" placeholder=""  >
          </div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="office_city" >City</label>
            <input  type="text" value="" name="office_city" class="citypick input-short" id="office_city" maxlength="100" placeholder=""  >
          </div>
          <div class="button-container">
            <div class="center"> </div>
            <div class="button-wrapper"> <a class="button" cura="submit_agency_profile"  >Create</a>
              <div class="gap"></div>
              <a class="button btn-next button-alt"  cura="dash_menu" data-id="account" >Cancel</a></div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
