<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Talents</div>
  <a class="button" cura="dash_menu" data-id="talents">VIEW AGENCY TALENTS</a> </div>
</div>
<div id="contents">
<div class="panel  fullheight">
  
  <div class="section bg-gray">
  <div class="sep"></div>

    <form cura="talent_request_find" id="talent_request_find">
      <div class="field-wrapper">
        <label class="dynamic-label" for="name_handle" >Talent Name or Handle</label>
        <input type="text" value="" name="name_handle" id="name_handle" maxlength="50">
      </div>
    </form>
    <div class="button-container">
      <div class="center"></div>
      <a class="button  " cura="talent_request_find">SEARCH</a> </div>
  </div>
  <div class="dash-sep"></div>
  <div class="sep"></div>
  <div class="sep"></div>
  <div class="section">
     <div id="talent_results">
        <div class="resizable txt-l txt-bold center">Talents and businesses close to you</div>
        <div class="sep"></div>
        <div>
        <?php
			$nearby = $engine->results();
			$_data['talents'] = $nearby;
			component('talent_request_result', $_data);
		
		?>        
        </div>  
     </div>
  </div>
   <div class="sep"></div>
</div>
