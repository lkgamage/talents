<div>
<div class="panel bg-red padding">
  <div class="panel bg-white">
	  <div class="padding">
    <div class=" txt-bold  txt-red txt-l">You are about to close your account!</div>
    <div class="sep"></div>
    <div>When you close your account,</div>
    <div class="sep"></div>
    <div>1. You'll loose your public profile page(s).</div>
    <div>2. You'll loose your subscriptions. Remaining credits (if any) will be lost, refund will not be processed.</div>
    <div>3. You'll loose your appointments.</div>
    <div>4. You'll loose your received bookings.</div>	
    <div class="txt-red">5. If you wish to re-activate account later, You may have one year commitment on a paid plan.</div>
	   <div class="sep"></div>
	  <div class="txt-red">You will not be abel to login to your account.</div>
	  <div>Contact customer support, if you want to  re-activate your account within 30 days. Your data will be removed from online-database after 30 days.</div>
    <div class="sep"></div>
    If you have any problem, we strongly recommend you to contact our support team to resolve it.
    <div class="button-container">
      <div class="center"> </div>
      <div class="button_wrapper"> <a class="button button-alt" cura="account_delete" data-id="confirm" >I want to close account</a>
        <div class="gap"></div>
        <a class="button button-alt"  cura="dash_menu" data-id="account" >Cancel</a></div>
    </div>
	 </div>
  </div>
</div>
</div>
<div class="sep"></div>
<div>Customer care hotline <a href="tel:<?php echo Config::$support_phone;  ?>"><?php echo Config::$support_phone;  ?></a></div>
<div class="sep"></div>
<div>Customer care email <a href="mailto:<?php echo Config::$support_email;  ?>"><?php echo Config::$support_email;  ?></a></div>
