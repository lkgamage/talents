<?php
if ( empty( $packages ) ) {
	$_data['object'] = $profile;
  component( 'package/package_form', $_data );
} else {

  $packs = [];

  foreach ( $packages as $p ) {
    $packs[ $p[ 'skill_id' ] ][] = $p;
  }

  $skills = App::getSkills();
  $skills = Util::groupArray( $skills, 'id', 'name' );

  ?>
<style>
.onoffswitch-inner:before {
	content: 'SHOW '
}
.onoffswitch-inner:after {
	content: 'HIDE '
}
</style>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange notoppadding">Packages</div>
  <a class="button" href="javascript:void(0)" cura="package_edit">ADD</a> </div>
<div class="sep"></div>
<div id="contents">
<div class="panel padding">
  <div class="flex">
    <div class="onoffswitch" >
      <input type="checkbox"  id="packages_public" checked="" class="onoffswitch-checkbox" cura="packages_public"  data-type="change">
      <label class="onoffswitch-label" for="packages_public"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
    </div>
    <div class="lpadding middle" > Packages on my public page</div>
  </div>
</div>
<?php
if ( !empty( $packages ) ) {
  foreach ( $packs as $skill_id => $packages ) {

?>
<div class="sep"></div>
<div class="panel hpadding">
  <div class="sep"></div>
  <div class="section-header txt-black resizable"><?php echo $profile->is_agency == 0 ?$skills[$skill_id] : 'Agency' ?></div>
  <div class="min-info">
    <?php
    foreach ( $packages as $item ) {
      if ( empty( $item[ 'id' ] ) ) {
        continue;
      }
      ?>
    <div class="min-info-row">
      <div class="flex">
        <div class="resizable"> <a class="txt-m" cura="package_view" data-id="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></a> </div>
        <div class="lpadding"><a class="dash-link ic-icon ic-edit" cura="package_edit" data-id="<?php echo $item['id']; ?>">Manage</a></div>
      </div>
      <div class="sep"></div>
      <div class="flex column-seperated">
        <div >
          <div class="txt-light txt-s">Status</div>
          <div><?php echo !empty($item['active']) ? '<div class="txt-green">Active</div>' : '<div class="txt-red">Disabled</div>'; ?></div>
        </div>
        <div >
          <div class="txt-light txt-s">Time</div>
          <div ><?php echo Util::ToTime( $item[ 'timespan' ] ); ?></div>
        </div>
        <div >
          <div class="txt-light txt-s">Price</div>
          <div><?php echo Session::currencyCode().number_format($item['fee_local'],2); ?></div>
        </div>
        <div >
          <div class="txt-light txt-s">Bookings</div>
          <?php echo $item['bookings'] ?> </div>
      </div>
    </div>
    <?php
    }

    if ( count( $packages ) < 3 ) {
		
		if($profile->is_agency == 1) {
			 echo '<div class="vpadding"><a class="more-link txt-s" cura="package_edit" data-skill="' . $item[ 'skill_id' ] . '">Add new package</a></div>
    </div>';
		}
		else {
			 echo '<div class="vpadding"><a class="more-link txt-s" cura="package_edit" data-skill="' . $item[ 'skill_id' ] . '">Add new package in ' . $skills[ $skill_id ] . ' category</a></div>
    </div>';
		}
     
    }

    echo '</div>';

    }

    }
    else {
      ?>
    <div class="padding">
      <div class="hpadding">
        <div class="sep"></div>
        You don't have any packages. Add your first package.</div>
    </div>
    <?php

    component( 'talent/package', $_data );
    echo '<div class="sep"></div>';

    }
    ?>
  </div>
</div>
<?php } ?>
<?php

//Util::debug( $packs );
//	Util::debug($profile);
?>
