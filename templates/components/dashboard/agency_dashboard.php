<?php

$engine = new DashboardEngine($user, $info);
$engine->is_agency = true;

?>
<div class="sep"></div>
<div class="columns">
  <div class="column-31 left-content">
    <div class="dash-box">
      <div class="dash-box-header">
        <div>Bookings - Received </div>
        <div></div>
      </div>
      <div class="dash-box-body ">
        <?php
	  
	  		$bookings = $engine->getRecivedBookings();
			$bookings->_pagesize = 10;
			$count = $bookings->count();
			
			if($count > 0){
				
				echo ' <div class="box-row txt-bold txt-light-gray">
          <div class="box-cell cell-width-40">&nbsp;</div>
          <div class="box-cell cell-width-20">Date/Time</div>
          <div class="box-cell cell-width-20">Due</div>
		  <div class="box-cell cell-width-20">Status</div>
        </div>';
				
				$data = $bookings->getData();

				
				foreach ($data as $item){
					
					
						echo ' <div class="box-row linked" cura="get_booking" data-id="'.$item['id'].'">
          <div class="box-cell cell-width-40">'.$item['booking_name'].'</div>
          <div class="box-cell cell-width-20 txt-s">'.Util::ToDate($item['session_start'], true).'</div>
          <div class="box-cell cell-width-20 txt-s '.(!empty($item['overdue']) ? 'txt-red' : '').'">'.Util::ToDate($item['due_date'], true).'</div>
		  <div class="box-cell cell-width-20 '.DataType::$BOOKING_CLASSES[$item['status']].'"><label class="row-status">'.DataType::$BOOKING_STATUS[$item['status']].'</label></div>
        </div>';
						
				}
				
				echo ' <div class="sep"></div><div class="box-row">
          <div class="box-cell resizable txt-s txt-gray">'.$count.' up coming bookings</div>
          <div class="box-cell"><a class="more-link" cura="dash_menu" data-id="bookings">View all bookings</a></div>
        </div>';
				
			}
			else{
				echo '<div class="padding">No bookings found</div>';
			}
	  
	  ?>
      </div>
    </div>
     <div class="sep2"></div>
    <div class="dash-box">
      <div class="dash-box-header">
        <div>Bookings - Made </div>
        <div></div>
      </div>
      <div class="dash-box-body ">
        <?php
	  
	  		$bookings = $engine->getRequestedBookings();
			$bookings->_pagesize = 10;
			$count = $bookings->count();
			
			if($count > 0){
				
				echo ' <div class="box-row txt-bold txt-light-gray">
          <div class="box-cell cell-width-40">&nbsp;</div>
          <div class="box-cell cell-width-20">Date/Time</div>
          <div class="box-cell cell-width-20">Due</div>
		  <div class="box-cell cell-width-20">Status</div>
        </div>';
				
				$data = $bookings->getData();
				
				
				foreach ($data as $item){
					
					
						echo ' <div class="box-row linked" cura="get_booking" data-id="'.$item['id'].'">
          <div class="box-cell cell-width-40"><div class="flex">';
		 
			 if(!empty($item['talent_id'])){
				echo  '<img src="'.$item['talent_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$item['talent_name'].'</div><div class="txt-light-gray">'.$item['booking_name'].'</div></div>';
			 }
			elseif(!empty($item['agency_id'])){
				echo  '<img src="'.$item['agency_image'].'"><div class="gap1"></div>';
				
				echo '<div class="middle"><div>'.$item['agency_name'].'</div><div class="txt-light-gray">'.$item['booking_name'].'</div></div>';
			}
			else{
				echo $item['booking_name'];
			}
		 
		 echo '</div></div>
          <div class="box-cell cell-width-20 txt-s">'.Util::ToDate($item['session_start'], true).'</div>
          <div class="box-cell cell-width-20 txt-s '.(!empty($item['overdue']) ? 'txt-red' : '').'">'.Util::ToDate($item['due_date'], true).'</div>
		  <div class="box-cell cell-width-20 '.DataType::$BOOKING_CLASSES[$item['status']].'"><label class="row-status">'.DataType::$BOOKING_STATUS[$item['status']].'</label></div>
        </div>';
						
				}
				
				echo ' <div class="sep"></div><div class="box-row">
          <div class="box-cell resizable txt-s txt-gray">'.$count.' up coming bookings</div>
          <div class="box-cell"><a class="more-link" cura="dash_menu" data-id="bookings">View all bookings</a></div>
        </div>';
				
			}
			else{
				echo '<div class="padding">No bookings found</div>';
			}
	  
	  ?>
      </div>
    </div>
    <div class="sep2"></div>
    <div class="dash-box ">
      <div class="dash-box-header">
        <div>Events</div>
        <div></div>
      </div>
      <div class="dash-box-body">
      <?php
	  $events = $engine->getEvents();
	  
	  	$events->_pagesize = 10;
		$count =  $events->count();
		
		if($count > 0){
			
			$data = $events->getData();
			
			foreach ($data as $e) {
			
				echo '<div class="box-row linked" cura="event_view" data-id="'.$e['id'].'">
          <div class="box-cell cell-width-50">
		  	<div>'.$e['name'].'</div>
		  	<div class="txt-light-gray txt-s">'.$e['type_name'].'</div>
		  </div>
          <div class="box-cell cell-width-30 txt-s">'.$e['city'].'<br>'.DataType::$countries[$e['country']][0].'</div>
          <div class="box-cell cell-width-20 txt-s">'.Util::ToDate($e['begins'], true).'</div>
        </div>';
		
			}
			
			echo ' <div class="sep"></div><div class="box-row">
          <div class="box-cell resizable txt-s txt-gray">'.$count.' up coming events</div>
          <div class="box-cell"><a class="more-link" cura="dash_menu" data-id="bookings">View all events</a></div>
        </div>';
		}
		else{
			echo '<div class="padding">No events found</div>';
		}
	  
	//  Util::debug($data);
	  ?>
        
      </div>
    </div>
    <div class="sep2"></div>
    <div class="dash-box">
      <div class="dash-box-header">
        <div>Messages</div>
        <div></div>
      </div>
      <div class="dash-box-body">
        <?php
	  $boards = $engine->getMessages();
	  $img_event = Util::mapURL('images/img-party.png');
	  
	  
	  if($boards->getTotal() > 0){
	  	
		echo $boards->getPage();
		
		 echo ' <div class="sep"></div><div class="box-row">
	  <div class="box-cell resizable txt-s txt-gray"></div>
	  <div class="box-cell"><a class="more-link" cura="dash_menu" data-id="messages">View all messages</a></div>
	</div>';
	  
	  }
	  else{
		 echo '<div class="padding">No messages found</div>'; 
	  }
	  
	  ?>
      </div>
    </div>
  </div>
  <div class="column-3">
    <div class="dash-box">
      <div class="dash-box-header">
        <div>Today Appointments</div>
        <div></div>
      </div>
      <div class="dash-box-body txt-s">
       <?php
	   $appointments = $engine->getAppointments();
	   
	   if(!empty($appointments)){
		   echo '<div class="dash-sep"></div>';
		   component('appointment/appointments-day', array('appointments' => $appointments, 'date' => date('Y-m-d')));
	   }
	   else{
		  echo '<div class="padding">No appointments today</div>';  
	   }
	  
	   ?>
      </div>
    </div>
    <div class="sep2"></div>
    
   
    <div class="dash-box">
      <div class="dash-box-header">
        <div>Subscriptions</div>
        <div></div>
      </div>
      <div class="dash-box-body">
      <?php
	  
		  $subs = $engine->getSubscriptions();
		  
		  if(!empty($subs)) {
			  foreach($subs as $s) {
				  
				echo '<div class="box-row linked" cura="subscription_view" data-id="'.$s['subscription_id'].'">
				  <div class="box-cell resizable"><div>'.$s['skill_name'].' <span class="txt-light-gray"> - '.$s['package_name'].'</span></div><div class="txt-s txt-gray">'.($s['jobs'] -$s['consumed'] ).' of '.$s['jobs'].' booking credits  remaining</div></div> 
				  <div class="box-cell  txt-green">ACTIVE</div>        
				</div>';
			  }
		  }
	  // Util::debug($subs);
	  ?>
      
        

      </div>
    </div>
    <div class="sep2"></div>
     <div class="dash-box">
      <div class="dash-box-header">
        <div>Public Page</div>
        <div></div>
      </div>
      <div class="dash-box-body">
      <?php
	  $page = $engine->pageInsight();
	  
	  $handle = Util::mapURL($page['handle']);
	  
	  echo ' <div class="box-row">
          <div class="box-cell resizable"><a href="'.$handle.'" target="_blank">@'.$page['handle'].'</a></div>
          <div class="box-cell ">'.(!empty($page['active']) ? '<span class="txt-green">ACTIVE</span>' : '<span>OFFLINE</span>').'</div>
        </div>
		<div class="box-row  txt-s">
          <div class="box-cell resizable">Total Page Views</div>
          <div class="box-cell ">'.number_format($page['views']).'</div>
        </div>
		<div class="box-row  txt-s">
          <div class="box-cell resizable">Published</div>
          <div class="box-cell ">'.Util::ToDate($page['published']).'</div>
        </div>
		<div class="box-row  txt-s">
          <div class="box-cell resizable">Content Updated</div>
          <div class="box-cell ">'.Util::ToDate($page['updated']).'</div>
        </div>';
	  
	  	if(strtotime($page['published']) < strtotime($page['updated'])){
			echo '<div class="box-row  txt-s txt-gray">
          <div class="box-cell resizable">
		  <div class=" txt-red">Your updates has not published yet. <a class="button" data-id="'.$page['id'].'" cura="publish_page">Publish Now</a></div>
		  <div class="sep"></div><div>View <a class="txt-s"  href="'.$handle.'?cache=true" target="_blank">published version of your page.</a></div></div>
        </div>';
		}
	  
	  ?>
       
        
      </div>
    </div>

  </div>
</div>
<?php
//Util::debug($info);
?>
