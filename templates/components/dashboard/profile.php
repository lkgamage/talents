<?php

$info = $profile->getInfo();
$session = Session::getProfile();

$page = new Page();
$page->populate( $info[ 'page' ] );

?>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange notoppadding">Talent Profile</div>
</div>
<div>
  <div class="panel">
    <div class="user-header">
      <div class="columns ">
        <div class="column-41 nopadding">
          <div class="user-header-left">
            <div class="user-header-img"> <img class="profile_<?php echo $profile->id; ?>" src="<?php 

			
			if(!empty($session) && $session['id'] == $profile->id){
				echo $session['dp'];	
			}
			else{
				$img = $profile->getDP();
				echo $img->i400;	
			}
			 ?>"> <a id="profile_changer" class="icon-capture" title="Change Picture" cura="update_profile_picture" data-ref="profile" data-id="<?php echo $profile->id?>"></a> </div>
            <div class="user-header-info resizable">
              <div class="user-header-name"><?php echo ucwords($profile->name); ?></div>
              <div>
                <?php
                echo( $profile->is_agency == 0 ) ? $info[ 'profile' ][ 'skills' ] : DataType::$AGENCY_TYEPS[ $profile->agency_type ] . ' Agency';
                ?>
              </div>
              <div class="user-header-desc">
                <div class="txt-light"><?php echo Util::makeString($info['location']['city'], $info['location']['region'], DataType::$countries[$info['location']['country']][0]); ?></div>
              </div>
            </div>
          </div>
        </div>
        <div class="column-4 middle left-border lpadding">
          <div class="sep"></div>
          <div class="">
            <div id="profile_handle">
              <?php

              if ( !$page->isEmpty( 'handle' ) ) {
                echo '<a href="' . Util::mapURL( '/' ) . urlencode( $page->handle ) . '" target="blank">@' . $page->handle . '</a>';
              } else {
                echo '<a class="dash-link" cura="handler_edit">Create handle</a>';
              }

              ?>
            </div>
          </div>
          <div class="sep"></div>
          <div class="txt-s">Update public page</div>
          <a class="dash-link ic-icon ic-edit txt-s " target="_blank" href="<?php echo Util::mapURL($info['page']['handle']).'?edit=1'; ?>">Update</a>
          <div class="sep"></div>
          <a class="more-link txt-s upper" cura="page_stat" data-id="<?php echo $info['page']['id'] ?>">Performance</a> </div>
      </div>
    </div>
  </div>
  <!----------- Tags --------->
  <div class="sep"></div>
  <div class="panel padding">
    <div class="flex">
      <div class="section-header txt-black resizable">Tags</div>
      <div class="field-wrapper nopadding">
        <input type="text" class="search-bar" id="tag_search" >
      </div>
    </div>
    <div class="txt-gray">Tags help people to find you.</div>
    <div class="sep"></div>
    <div id="tag_list" cura="tags_init" data-type="load">
      <?php

      if ( !empty( $info[ 'profile' ][ 'tags' ] ) ) {

        $tags = App::getTags();

        $pt = explode( ',', $info[ 'profile' ][ 'tags' ] );
        foreach ( $pt as $id ) {
          echo '<div  id="tag_' . str_replace( ' ', '', $tags[ $id ] ) . '" class="tag" cura="context" data-action="tag_context" data-tag="' . $tags[ $id ] . '">' . $tags[ $id ] . '</div>';
        }

      } else {

        echo '<span id="no_tags">You have not added any tags. Add tags related to your services.</span><div class="sep"></div>';

      }

      ?>
    </div>
    <?php
    if ( empty( $pt ) || count( $pt ) < 3 ) {

      $possible = $profile->getPossibleTags();

      if ( !empty( $possible ) ) {

        echo '<div class="dash-sep"></div>
		  <div class="sep"></div>
		  <div class="upper">Suggested Tags</div>';

        echo '<div class="vpadding suggest_tag">';

        foreach ( $possible as $key => $value ) {

          echo '<a cura="tag_add" data-tag="' . $value . '" >' . $value . '</a>';
        }


        echo '</div>';
      }


      // show suggested tags

    }

    ?>
  </div>
  <!------------ Pending invoices ------------------>
  <?php

  $invoices = $user->getPendingInvoices();
  if ( !empty( $invoices ) ) {
    //Util::debug($invoices);
    echo '<div class="sep"></div>';
    component( 'dashboard/invoice_pending', [ 'invoices' => $invoices ] );
  }


  ?>
  <!----------- Subscription Categories --------->
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Subscriptions</div>
    <div  class="min-info">
      <?php
      if ( !empty( $info[ 'categories' ] ) ) {
        foreach ( $info[ 'categories' ] as $item ) {
          ?>
      <div class="min-info-row">
        <div class="flex">
          <div class="resizable"> <a class="txt-m" cura="subscription_view" data-id="<?php echo $item['category_id']; ?>"><?php echo $item['skill_name'].' - '.(!empty($item['package_name']) ? $item['package_name'] : '<span class="txt-red">No plan selected</span>'); ?></a>
            <div class="txt-gray"><?php echo ($item['validity'] == 365) ? 'Yearly' : 'Monthly' ?></div>
          </div>
          <div class="lpadding">
            <?php if(empty( $item[ 'package_id' ])) { ?>
            <a class="button" cura="subscription_view" data-id="<?php echo $item['category_id']; ?>">Activate</a>
            <?php } else { ?>
            <a class="dash-link ic-icon ic-edit" cura="subscription_view" data-id="<?php echo $item['category_id']; ?>">Manage</a>
            <?php } ?>
          </div>
        </div>
        <div class="sep"></div>
        <div class="flex column-seperated">
          <div >
            <div class="txt-light txt-s">Bookings</div>
            <div><?php echo $item['jobs']; ?></div>
          </div>
          <div >
            <div class="txt-light txt-s">Remaining</div>
            <div class="txt-green"><?php echo $item['jobs'] - $item['used']; ?></div>
          </div>
          <div >
            <div class="txt-light txt-s">Renew</div>
            <div>
              <?php
              echo( $item[ 'renewal' ] == 1 ) ? Util::ToShortDate( $item[ 'exp_date' ] ) : 'Canceled';
              ?>
            </div>
          </div>
          <div >
            <div class="txt-light txt-s">Status</div>
            <?php
            if ( empty( $item[ 'package_id' ] ) ) {
              echo '<div class="">Not activated</div>';
            } elseif ( $item[ 'renewal' ] == 1 ) {
              echo '<div class="txt-green">Active</div>';
            }
            else {
              echo '<div class="">Active till ' . Util::ToShortDate( $item[ 'exp_date' ] ) . '</div>';
            }
            ?>
          </div>
        </div>
      </div>
      <?php
      }
      } else {
        echo '<div class="vpadding">You have no active subscriptions</div>';
      }

      if ( empty( $info[ 'profile' ][ 'is_agency' ] ) || empty( $info[ 'categories' ] ) ) {
        echo '<div class="vpadding"><a class="more-link" cura="subscription_add">Get new subscription</a></div>';
      }

      ?>
    </div>
  </div>
  <?php if($profile->is_agency == 0 ) {  ?>
  <!----------- Agency/talents --------->
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Agencies</div>
  </div>
  <!----------- work schedule --------->
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Work Schedule</div>
    <div class="sep"></div>
    <div>
      <div class="options">
        <div>
          <label class="custom-checkbox">
            <input <?php echo ($profile->limited_hours == 0) ? 'checked=""' : '' ?> type="radio" name="limited_hours" id="limited_hours_0" value="0" cura="switch_work_hours" data-type="change" data-value="0" >
            Always available<span class="checkmark"></span> </label>
        </div>
        <div class="sep"></div>
        <div>
          <label class="custom-checkbox">
            <input <?php echo ($profile->limited_hours == 1) ? 'checked=""' : '' ?> type="radio" name="limited_hours" id="limited_hours_1" value="1" cura="switch_work_hours" data-type="change" data-value="1" >
            Formal working hours<span class="checkmark" ></span> </label>
        </div>
      </div>
      <div  id="week_schedule" style="<?php echo ($profile->limited_hours == 0) ? 'display:none' : '' ?>">
        <div class="sep"></div>
        <div class="sep"></div>
        <div class="panel field-wrapper work-time">
          <?php
          $schedule = $profile->getSchedule();
          $schedule = $schedule->toArray();

          foreach ( DataType::$days as $day ) {

            $abbr = substr( strtolower( $day ), 0, 3 );

            ?>
          <div class="list-select <?php echo ($schedule[$abbr.'_avl'] == 0) ? 'off-day' : '' ?>">
            <div class="col3">
              <label class="custom-checkbox">
                <input type="checkbox"  name="<?php echo $abbr; ?>_avl" id="<?php echo $abbr; ?>_avl_1" value="1" <?php echo ($schedule[$abbr.'_avl'] == 1) ? 'checked' : '' ?> cura="working_day_switch" data-type="change"  data-day="<?php echo $abbr; ?>" >
                <?php echo $day; ?><span class="checkmark" ></span> </label>
            </div>
            <div> <a cura="working_time_edit" ><?php echo !empty($schedule[$abbr.'_start'] ) ? Util::stampToTime($schedule[$abbr.'_start']) : 'NA' ?></a>
              <input type="text" value="<?php echo !empty($schedule[$abbr.'_start']) ? Util::stampToTime($schedule[$abbr.'_start']) : '' ?>" name="<?php echo $abbr; ?>_start" id="<?php echo $abbr; ?>_start" maxlength="10" class="timepick" data-changed="working_time_updated" data-close="working_time_updated">
            </div>
            <div> - </div>
            <div> <a cura="working_time_edit"><?php echo !empty($schedule[$abbr.'_ends']) ? Util::stampToTime($schedule[$abbr.'_ends']) : 'NA' ?></a>
              <input type="text" value="<?php echo !empty($schedule[$abbr.'_ends']) ? Util::stampToTime($schedule[$abbr.'_ends']) : '' ?>" name="<?php echo $abbr; ?>_ends" id="<?php echo $abbr; ?>_ends" maxlength="10" class="timepick" data-changed="working_time_updated"  data-close="working_time_updated">
            </div>
            <div class="resizable"></div>
          </div>
          <?php
          }

          ?>
        </div>
      </div>
    </div>
  </div>
  <?php
  }
  /** not showing work schedle for agencies **/
  ?>
  <!----------- Basic Settings --------->
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Basic Settings</div>
    <div class="sep"></div>
    <div class="vadding">
      <div class="columns column-seperated">
        <div class="column-3 ">
          <div class="info-header">current city</div>
          <div class="info-body">
            <?php

            if ( !empty( $info[ 'location' ][ 'address' ] ) ) {
              echo $info[ 'location' ][ 'address' ] . ", ";
            }

            echo $info[ 'location' ][ 'city' ];

            if ( !empty( $info[ 'location' ][ 'region' ] ) ) {
              echo ', ' . $info[ 'location' ][ 'region' ];
            }

            echo "<br>";
            echo DataType::$countries[ $info[ 'location' ][ 'country' ] ][ 0 ];

            ?>
            <br>
          </div>
          <a class="dash-link" cura="customer_city_edit" data-profile="1">Change City</a>
          <div id="customer_location"  class="inline_load"  ></div>
        </div>
        <div class="column-3 lpadding">
          <div class="info-header">Time Zone</div>
          <div class="info-body">
            <?php

            if ( !$user->isEmpty( 'timezone' ) ) {
              echo DataType::$timezones[ $profile->timezone ][ 0 ];
              echo '<br><span class="txt-light">UTC' . DataType::$timezones[ $profile->timezone ][ 1 ] . '</span>';
            } else {
              $offset = $user->timeoffset;

              foreach ( DataType::$timezones as $code => $zone ) {

                if ( $offset == $zone[ 2 ] ) {
                  echo $zone[ 0 ];
                  echo '<br><span class="txt-light">UTC' . $zone[ 1 ] . '</span>';
                }
              }
            }


            ?>
          </div>
          <a class="dash-link" cura="customer_tz_edit" data-profile="1">Change Time Zone</a>
          <div id="customer_timezone"  class="inline_load"  ></div>
        </div>
        <div class="column-3 lpadding">
          <div class="info-header">Currency</div>
          <div class="info-body">
            <?php

            $crr = $profile->currency;

            if ( empty( $crr ) ) {
              $crr = 'USD';
            }

            echo DataType::$currencies[ $crr ];
            echo '<br><span class="txt-light">(' . $crr . ')</span>';


            ?>
          </div>
          <a class="dash-link" cura="customer_currency_edit" data-profile="1">Change Currency</a>
          <div id="customer_currency" class="inline_load" ></div>
        </div>
      </div>
      <div id="customer_setting" class="hpadding"></div>
    </div>
  </div>
  <!----------- manage parts --------->
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Manage</div>
    <div class="sep"></div>
    <div class="vadding">
      <div class="columns column-seperated">
        <div class="column-3 lpadding">
          <div >Verify profile identity</div>
          <div><a class="more-link txt-s" cura="profile_verify">Begin verification</a></div>
          <div class="sep"></div>
          <div >Change profile name</div>
          <?php if(!empty($info['profile']['verified'])) { ?>
          <div class="txt-s txt-light"><span class="txt-bold txt-green">Your profile verified.</span><br>
            You may contact customer support to change profile name</div>
          <?php } elseif(empty($info['profile']['name_changed'])) { ?>
          <div><a class="more-link txt-s"cura="customer_info_edit" data-profile="1" >Update name</a></div>
          <div id="customer_info_edit"> </div>
          <?php } else { ?>
          <div class="txt-s txt-light">You have updated profile once. You may contact customer support to change profile name.</div>
          <?php } ?>
        </div>
        <div class="column-3 lpadding">
          <div >Change page url</div>
          <div><a class="more-link txt-s" cura="handler_edit">Update url</a></div>
          <div class="inline_load" id="handler_edit"></div>
          <div class="sep"></div>
        </div>
        <div class="column-3 lpadding">
          <div >Close profile &amp; page</div>
          <div><a class="more-link txt-s" cura="profile_close">Close profile</a></div>
          <div class="sep"></div>
          <div class="txt-s txt-red"> This can not be un-done. You'll loose your profile, public page, appointments and all bookings received to this profile. </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="sep"></div>
<div id="profile_action"> </div>
<div class="sep"></div>
<?php
//Util::debug( $info );
?>
