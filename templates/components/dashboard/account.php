<?php
//$info = $user->getinfo();
?>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange  notoppadding">Account</div>
</div>
<div id="contents">
  <div class="panel ">
    <div class="user-header">
      <div class="columns ">
        <div class="column-41 nopadding">
          <div class="user-header-left">
            <div class="user-header-img"> <img class="customer_<?php echo $user->id; ?>" src="<?php 
			$cache = Session::getCustomer();
			
			if(!empty($cache) && $cache['id'] == $user->id){
				echo $cache['dp'];	
			}
			else{
				$img = $user->getDP();
				echo $img->i400;	
			}
			 ?>"> <a id="profile_changer" class="icon-capture" title="Change Picture" cura="update_profile_picture" data-ref="customer" data-id="<?php echo $user->id?>"></a> </div>
            <div class="user-header-info resizable">
              <div class="user-header-name"><?php echo $user->firstname.' '.$user->lastname; ?></div>
              <div class="user-header-desc hidden">This information shows some details</div>
              <div id="customer_info_edit"> </div>
            </div>
          </div>
        </div>
        <div class="column-4 middle left-border lpadding">
          <div class="sep"></div>
          <div class="txt-light">Member since<br>
            <?php echo Util::ToDate($user->created, false, true); ?></div>
          <?php  if($user->name_changed ==  0) { ?>
          <div class="sep"></div>
          <a class="dash-link ic-icon ic-edit" cura="customer_info_edit">Update Info</a>
          <?php } ?>
          <div class="sep"></div>
        </div>
      </div>
    </div>
  </div>
  <!----------  Profile --------------->
  
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Profiles</div>
    <div class="sep"></div>
    <div class="vadding">
      <?php

      $profiles = $user->getProfiles( true );
      if ( !empty( $profiles ) ) {

        foreach ( $profiles as $profile ) {
          if ( empty( $profile[ 'id' ] ) ) {
            continue;
          }
          ?>
      <div class="min-info-row flex">
        <div class="box-cell nopadding"><img src="<?php echo fiximage($profile['thumb'], $profile['gender'], $profile['is_agency']) ?>"></div>
        <div class="resizable lpadding  middle">
          <div class="columns">
            <div class="resizable">
              <div class="flex" ><a class="txt-m" target="_blank" href="<?php echo Util::mapURL($profile['handle']) ?>">
                <?php
                echo ucwords( $profile[ 'name' ] );
                if ( !empty( $profile[ 'commitment' ] ) ) {
                  echo '<a class="crown-mini middle lpadding"></a>';
                }
                ?>
                </a></div>
              <div class="txt-light">
                <?php
                if ( empty( $profile[ 'is_agency' ] ) ) {
                  echo $profile[ 'skills' ];
                } else {
                  echo DataType::$AGENCY_TYEPS[ $profile[ 'agency_type' ] ] . ' Agency';
                }
                ?>
              </div>
            </div>
            <div class="lpadding"><a class="dash-link ic-icon ic-edit" cura="dash_menu" data-id="profile">Manage</a></div>
          </div>
        </div>
      </div>
      <?php
      }
      } else {
        echo "<div>You have no business profiles</div>";
      }

      //Util::debug($profiles);
      ?>
      <div class="vpadding"><a class="more-link" cura="dash_menu" data-id="newprofile" >Create new profile</a></div>
    </div>
  </div>
  
  <!--------- Contact ----------------->
  
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Contact Info</div>
    <div class="sep"></div>
    <div class="">
      <div class="flex">
        <div class="info-header resizable">Phones</div>
        <div><a class="more-link" cura="customer_phone_add">Add Phone</a></div>
      </div>
      <?php

      $phones = $user->getPhones();

      if ( !empty( $phones ) ) {

        foreach ( $phones as $phone ) {

          ?>
      <div class="min-info-row">
        <div class="flex">
          <div class="resizable"><?php echo "(+".DataType::$countries[$phone->country_code][1] .") ".$phone->phone ."&nbsp;&nbsp;".($phone->is_primary == 1 ? ' <span class="txt-green txt-s">primary</span>' : '') ?></div>
          <div class="lpadding"><a class="dash-link ic-icon ic-edit" cura="customer_phone_edit" data-id="<?php echo $phone->id; ?>">Manage</a></div>
        </div>
        <div id="phone_edit_<?php echo $phone->id ?>">
          <?php

          if ( $phone->is_primary == 0 ) {
            echo '<div class=""><a class="dash-link" cura="make_primary" data-id="' . $phone->id . '" data-ref="p" >Make primary</a></div>';
          }

          ?>
        </div>
      </div>
      <?php
      }
      }
      ?>
    </div>
    <div class="sep2"></div>
    <div class="">
      <div class="flex">
        <div class="info-header resizable">Emails</div>
        <div><a class="more-link" cura="customer_email_add">Add Email</a></div>
      </div>
      <?php

      $emails = $user->getEmails();

      if ( !empty( $emails ) ) {

        foreach ( $emails as $email ) {

          ?>
      <div class="min-info-row">
        <div class="flex">
          <div class="resizable"><?php echo $email->email ."&nbsp;&nbsp;".($email->is_primary == 1 ? ' <span class="txt-green txt-s">primary</span>' : '') ?></div>
          <div class="lpadding"><a class="dash-link ic-icon ic-edit" cura="email_edit" data-id="<?php echo $email->id; ?>">Manage</a></div>
        </div>
        <div id="email_edit_<?php echo $email->id ?>">
          <?php

          if ( $email->is_primary == 0 ) {
            echo '<div class=""><a class="dash-link" cura="make_primary" data-id="' . $email->id . '" data-ref="e" >Make primary</a></div>';
          }

          ?>
        </div>
      </div>
      <?php
      }
      }
      ?>
    </div>
  </div>
  <!----------- Basic Settings --------->
  <div class="sep"></div>
  <?php
  component( 'customer/basic_setting', array( 'user' => $user ) );
  ?>
  <!----------- Manage ---------> 
  <!----------- manage parts --------->
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black">Manage</div>
    <div class="sep"></div>
    <div class="vadding">
      <div class="columns column-seperated">
        <div class="column-3 lpadding">
          <div >Billing &amp; Payments</div>
          <div><a class="more-link txt-s" cura="dash_menu" data-id="billing">View Billing</a></div>
        </div>
        <div class="column-3 lpadding">
          <div >Notification Settings</div>
          <div><a class="more-link txt-s" cura="dash_menu" data-id="settings">Update settings</a></div>
          <div class="inline_load" id="handler_edit"></div>
          <div class="sep"></div>
        </div>
        <div class="column-3 lpadding">
          <div >Close profile &amp; page</div>
          <div><a class="more-link txt-s" cura="account_close">Close Account</a></div>
          <div class="sep"></div>
          <div class="txt-s txt-red"> This can not be un-done. You'll loose your profiles, public page, appointments and all received bookings . </div>
        </div>
      </div>
    </div>
  </div>
  <!---------------------------> 
  
</div>
