<div>
  <div class="panel bg-red padding">
    <div class="panel bg-white">
      <div class="padding">
        <div class=" txt-bold  txt-red txt-l">You are about to close your profile!</div>
        <div class="sep"></div>
        <div>When you close your profile,</div>
        <div class="sep"></div>
        <div>1. You'll loose your public profile page.</div>
        <div>2. You'll loose your subscriptions. Remaining credits (if any) will be lost, refund will not be processed.</div>
        <div>3. You'll loose your appointments attached to this profile.</div>
        <div>4. You'll loose your bookings received to this profile.</div>
        <div class="txt-red">5. If you wish to re-activate profile later, You may have one year commitment on a paid plan.</div>
        <div class="sep"></div>
        If you have any problem, we strongly recommend you to contact our support team to resolve it.
        <div class="button-container">
          <div class="center"> </div>
          <div class="button_wrapper"> <a class="button button-alt" cura="profile_delete" data-id="confirm" >I want to delete profile</a>
            <div class="gap"></div>
            <a class="button button-alt"  cura="dash_menu" data-id="profile"  >Cancel</a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="sep"></div>
<div>Customer care hotline <a href="tel:<?php echo Config::$support_phone;  ?>"><?php echo Config::$support_phone;  ?></a></div>
<div class="sep"></div>
<div>Customer care email <a href="mailto:<?php echo Config::$support_email;  ?>"><?php echo Config::$support_email;  ?></a></div>
