<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Messages</div>
</div>
<div>
  <div class="panel">
    <div class="columns">
      <div class="resizable">
        <?php
$uid = uniqid();

$boards = $user->getMessageBoards();

if($boards->getTotal() > 0) {

	echo '<div id="'.$uid.'">';

	echo $boards->getPage();

	echo '</div>';
	?>
        <div class="button-container">
          <div class="center"> <a class="button btn-next" id="more_<?php echo $uid ?>" data-label="VIEW MORE " > VIEW MORE </a> </div>
        </div>
        <div>
          <?php
echo $boards->manualPaginate($uid,'more_'.$uid);
?>
        </div>
        <?php
}
else{
	echo '<div class="padding center">No messages found</div>';	
}

//Util::debug( $bookings->_datasql);
?>
        <?php


?>
      </div>

    </div>
  </div>
</div>
