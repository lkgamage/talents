<div class="hpadding">
  <div class="panel padding">
    <div class="section-header nopadding">create service Packages</div>
    <div class="vpadding">You have no packages for <span class="txt-blue"><?php echo $category['name'] ?></span> category. Your customers <span class="txt-red">CAN NOT</span> make bookings for you.</div>
    <div>Packages are service blocks, that your clients can purchase. <a>Learn more about packages.</a></div>
    <div class="sep"></div>
    <div class="sep"></div>
    <a class="button" href="javascript:void(0)" cura="package_edit">ADD PACKGES</a>
    <div class="sep"></div>
  </div>
  <div class="sep"></div>
</div>
