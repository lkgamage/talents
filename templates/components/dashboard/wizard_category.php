<div class="hpadding">
<div class="panel padding">
<div class="sep"></div>
<div class="section-header nopadding">Let's select a category for <span class="txt-black"><?php echo $profile['name'] ?></span> profile</div>
<div class="sep"></div>
<form id="subscription-form" cura="subscription_category">
<?php

if ( $profile[ 'is_agency' ] == 0 ) {
  echo '<div class="txt-m">Select a category</div><div class="sep"></div>';
  $categories = App::getSkills( 't' );
  component( 'subscription/categories', array( 'categories' => $categories ) );
  ?>
<div class="sep"></div>
<div class="button-container">
  <div class="center"></div>
  <a class="button btn-next" cura="subscription_category">Next</a>
  <div class="gap"></div>
  <a class="dash-link" cura="dash_menu" data-id="profile">Cancel</a> </div>
<?php
} else {
  echo '<div class="txt-m">Select a subscription package</div>';
  $skill = App::getAgencyCategory();
  $packages = $skill->getPackages();
  component( 'subscription/packages', array(
    'skill' => $skill,
    'packages' => $packages,
    'subscriptions' => array(),
    'profile' => $profile
  ) );
}
//	Util::debug($profile)
?>

</div>
<div class="sep"></div>
</div>
