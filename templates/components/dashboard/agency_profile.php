<?php
$cache = Session::getProfile();
//$page = $agency->getPage();
$subscriptions = $agency->getSubscriptionDetail();

$info = $agency->getinfo();

?><div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange notoppadding">Agency Profile</div>
</div>
<div id="contents">
  <div class="panel ">
    <div class="user-header">
      <div class="columns ">
        <div class="column-41 nopadding">
          <div class="user-header-left">
            <div class="user-header-img"> <img class="agency_<?php echo $agency->id; ?>" src="<?php 			
			if(!empty($cache) && $cache['id'] == $agency->id){
				echo $cache['dp'];	
			}
			else{
				$img = $agency->getDP();
				echo $img->i400;	
			}
			 ?>"> <a id="profile_changer" class="icon-capture" title="Change Picture" cura="update_profile_picture" data-ref="agency" data-id="<?php echo $agency->id?>"></a> </div>
            <div class="user-header-info resizable">
              <div class="user-header-name"><?php echo $agency->name; ?></div>
              <div class="user-header-desc">
                <div class="flex">
                  <div id="profile_handle">
                    <?php
			  
			  if(!empty($info['page']['handle'])){
				  echo '<a href="'.Util::mapURL('/').urlencode($info['page']['handle']).'" target="blank">@'.$info['page']['handle'].'</a>';
			  }
			  else{
				  echo '<a class="dash-link" cura="handler_edit">Create handle</a>';
			  }
			  
			  ?>
                  </div>
                 
                  <div class="gap"></div>
                  <div><a class="dash-link ic-icon ic-edit txt-light " cura="handler_edit"></a></div>
                 
                </div>
                <div class="inline_load" id="handler_edit"></div>
              </div>
             
            </div>
          </div>
        </div>
        <div class="column-4 middle left-border lpadding">
          <div class="sep"></div>
          <div ><span class="txt-light">Category</span><br>
            <?php echo DataType::$AGENCY_TYEPS[$agency->agency_type]; ?></div>
          
          <div class="sep"></div>
          <a class="dash-link ic-icon ic-edit" cura="customer_info_edit" data-profile="1">Update Settings</a>
         
          <div class="sep"></div>
        </div>
      </div>
      
    </div>
     <div id="customer_info_edit"> </div>
    <div class="sep"></div>
    <div class="sep"></div>
     <div class="section-header txt-orange">Subscription</div>
    <div class="padding section">
      <div class="panel min-info">
        <?php
	$paid = false;
	if(!empty($subscriptions)){
		
		foreach ($subscriptions as $item) {
			
			if($item['subscription']['fees'] > 0){
				$paid = true;	
			}
		
			
		?>
        <div class="min-info-row">
          <div class="mini-info-name"><?php echo $item['skill']['name'].' - '.$item['package']['name']; ?></div>
          <div class="mini-info-detail">
            <div class=" nopadding "><?php echo $item['subscription']['term'] == 'm' ? 'Monthly' : 'Annual' ?></div>
            <div class=" nopadding resizable">Valid till <?php echo Util::ToDate($item['subscription']['exp_date'])?></div>
            <div class="right"><a class="dash-link ic-icon ic-edit" cura="subscription_view" data-id="<?php echo $item['subscription']['id']; ?>">Manage</a></div>
          </div>
        </div>
        <?php
		
		}
		
		//Util::debug($subscriptions);
	}
	else{
		echo "<div class=\"padding\">You have no active subscriptions.</div>";
	}
	
	//Util::debug($subscriptions);
	
	?>
      </div>
      <div class="sep"></div>
      <?php if(!$paid) { ?>
      <div class=""> <a href="javascript:void(0)" class="<?php empty($subscriptions) ? 'button' : '' ?>"   cura="subscription_add" >New Subscription</a> </div>
      <?php } ?>
    </div>
    <div class="sep"></div>

     <div class="sep"></div>
    <div class="section-header txt-orange">Basic settings</div>
    <div class="padding">
      <div class="columns column-seperated">
        <div class="column-3">
          <div class="info-header">current city</div>
          <div class="info-body">
            <?php
         
		  echo $info['location']['city'];
		  
		  if(!empty($info['location']['region'])){
			 echo ', '.$info['location']['region'];
		  }
		  
		  echo "<br>";
		  echo DataType::$countries[$info['location']['country']][0];
		  
		  
		  ?>
            <br>
          </div>
          <a class="dash-link" cura="customer_city_edit" data-profile="1">Change City</a> </div>
        <div class="column-3 lpadding">
          <div class="info-header">Time Zone</div>
          <div class="info-body">
            <?php
	
			if(!$agency->isEmpty('timezone')){
				echo DataType::$timezones[$agency->timezone][0];
				echo '<br><span class="txt-light">UTC'.DataType::$timezones[$agency->timezone][1].'</span>';
			}
			else {
				$offset = $agency->timeoffset;
				
				foreach (DataType::$timezones as $code => $zone){
					
					if($offset == $zone[2]) {
						echo $zone[0];
						echo '<br><span class="txt-light">UTC'.$zone[1].'</span>';
					}
				}			
			}
			
				
			
			?>
          </div>
          <a class="dash-link" cura="customer_tz_edit" data-profile="1">Change Time Zone</a> </div>
        <div class="column-3 lpadding">
          <div class="info-header">Currency</div>
          <div class="info-body">
            <?php
			
			$crr = $agency->currency;
			

			if(empty($crr)) {
				$crr = 'USD';
			}
			
			echo DataType::$currencies[$crr];
			echo '<br><span class="txt-light">('.$crr.')</span>';
			?>
          </div>
          <a class="dash-link" cura="customer_currency_edit" data-profile="1">Change Currency</a> </div>
      </div>
      <div id="customer_setting" class="hpadding"></div>
    </div>
   
    
    <div class="sep"></div>
 
    <div class="sep"></div>
    <div class="dash-sep"></div>
    <div class="padding">
      <div class="padding">
        <div class="button-container">
          <div class="center"></div>
          <a class="dash-link">Close agency account</a> </div>
      </div>
    </div>
    <div class="sep"></div>
  </div>
</div>
<?php
//Util::debug($info);
?>