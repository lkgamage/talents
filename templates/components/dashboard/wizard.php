
<div class="panel padding">
<div class="padding">
  <?php

if($target == 'subscription'){
?>
  There are few things to complete before your <?php echo $type ?> profile go live.
  <div class="sep"></div>
  
  <form id="subscription-form" cura="subscription_category">
    <?php  
	
	if($type == 'talent'){
		echo '<div class="txt-m">Select a subscription category</div>';
		$categories = App::getSkills('t');
		component('subscription/categories', array('categories' => $categories));
		?>
         <div class="sep"></div>
  <div class="button-container">
    <div class="center"></div>
    <a class="button btn-next" cura="subscription_category">Next</a>
    <div class="gap"></div>
    <a class="dash-link" cura="dash_menu" data-id="profile">Cancel</a> </div>
        <?php
	}
	else{
		echo '<div class="txt-m">Select a subscription package</div>';
		$skill = App::getAgencyCategory() ;
		$packages = $skill->getPackages();
		component('subscription/packages', array(
			'skill' => $skill,
			'packages' => $packages,
			'subscriptions' => array()
		));
	}
	
	?>
  </form>
 
  <?php	
}
elseif($target == 'packages'){
	
	$esubs = array();
	foreach ($info[$type]['subscriptions'] as $s){
		
		if(empty($info[$type]['subscriptions']['packages'])){
			$esubs[] = $info[$type]['subscriptions'];	
		}
	}
	
	
	echo '<div class="txt-m">Lets create packages for your subscription'.(count($esubs) > 1 ? 's' : '').'.</div>';
	
	
?>
  <div class="sep"></div>
  <div>Packages are service blocks, that your clients can purchase. <a>Learn more about packages.</a></div>
  <div class="sep"></div>
  <div class="sep"></div>
  <div class="panel min-info">
    <?php
		
		$categories = array();
	

		
		foreach ($info[$type]['subscriptions'] as $item) {
			
		
		?>
    <div class="min-info-row">
      <div class="flex">
        <div class="resizable">
          <div class="mini-info-name"><?php echo $item['skill_name'].' - '.$item['package_name']; ?></div>
          <div class="mini-info-detail">
            <div class="nopadding resizable">
              <?php 
			
			if(!empty($item['packages'])) {
				echo count($item['packages']). (count($item['packages']) != 1 ? ' packages'  : ' package');
			}
			else{
				echo '<span class="txt-red">No packages</span>';
			}
			
			?>
            </div>
          </div>
        </div>
        <div class="middle"><a class="button " cura="package_edit" data-skill="<?php echo $item['skill_id']; ?>">Add Package</a></div>
      </div>
    </div>
    
    <?php		
		}
	?>
  </div>
   <div class="sep"></div>
    <div class="pro-tips"><span class="txt-bold">Pro Tips:</span> Add 3 or more packages in each category, with different price levels. You'll see the magic.</div>
 
  <div class="sep"></div>
  <?php	
}
if($target == 'page'){
?>
  <div class="sep"></div>
  <div>You are almost ready to start your journey. </div>
  <div class="sep"></div>
  <div>Lets create a public page for <span class="txt-blue txt-bold"><?php echo $info[$type]['name'] ?></span>  </div>
  <div class="sep"></div>
   <div class="sep"></div>
  
    <div class="sep"></div>
    <div class="max-600 is-centered">
     <div class="txt-m txt-orange">Choose a meaningful username  that match with your brand</div>
     <div class="sep"></div>
    <?php 
	$page = new Page();
	$page->populate($info[$type]['page']);
	component('handler_update', array('redirect' => 1, 'page' => $page));
	
	 ?></div>
<?php	
}
if($target == 'publish'){
	
	if(empty($info[$type]['page']['updated']) || $info[$type]['page']['created'] == $info[$type]['page']['updated']){
		// page not updated
		?>
<div class="sep"></div>
  <div>You have successfully created a page for <a href="<?php echo Util::mapURL($info[$type]['page']['handle']) ?>"><?php echo $info[$type]['name'] ?></a>. </div>
  <div class="sep"></div>
  <div>It's time for make it awesome! Add few image and update content to match with your business. </div>
    <div class="sep"></div>
      <div class="sep"></div>
  <div class="vpadding"><a class="button"  href="<?php echo Util::mapURL($info[$type]['page']['handle']) ?>">Update Page</a></div>
<?php
	}
	else{
		// page updated, not published
		?>
<div class="sep"></div>
  <div>Just one more step to get your awesome page live. </div>
   <div class="sep"></div>
  <div>If you done editing the page, you may publish it now. </div> 
  <div class="sep">
  </div>
  <div class="vpadding"><a class="button button-act" cura="publish_page" data-id="<?php echo $info[$type]['page']['id']; ?>" data-ref="<?php echo empty($info[$type]['page']['published']) ? 1 : 0 ?>">Publish Page Now</a><div class="gap"></div><a class="button "  href="<?php echo Util::mapURL($info[$type]['page']['handle']) ?>">Update Page</a></div>
  <div class="sep"></div>
  <div class="pro-tips"><span class="txt-bold">Pro Tips:</span> Keep your page always up to date. Visitors get bored with outdated content.</div>
<?php
	}
	
}

//Util::debug($info);

?>
</div>
</div>
