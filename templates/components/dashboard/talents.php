<?php
$subscription = $agency->getActiveSubscription();
?>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Talents</div>
  <a class="button" cura="dash_menu" data-id="find_talents">FIND TALENTS</a> </div>
<div id="contents">

	<div>
    <div class="" >
      <div class="flex">
        <div class="resizable info-header txt-gray">Talents Working With The Agency</div>
        <div><a class="dash-link" cura="dash_menu" data-id="agency_pending_requests">See Requests</a></div>
      </div>
      <div class="sep"></div>
      <?php

	  
	  if(!empty($talents)){
		  
		  $skills = App::getSkillArray();
		  
		  foreach ($talents as $info) {
			  
			  ?>
      
<!------------>      
      
      <div  class="dash-info-row with-picture" id="talent_<?php echo $info['request_id']; ?>">
        <div class="dash-info-top">
          <div class="dash-info-pic" cura="redirect" data-url="<?php echo $info['handle'] ?>"> <img src="<?php 
			
			if(!empty($info['i400'])){
				echo $info['i400'];
			}
			elseif($info['gender'] == 0){
				
				echo Util::mapURL('/images/placeholder-girl.png');
			}
			else{
				echo Util::mapURL('/images/placeholder-boy.png');	
			}
			 ?>"> </div>
          <div class="dash-info-left">
            <div class="dash-info-name" cura="redirect" data-url="<?php echo $info['handle'] ?>"><?php echo $info['name']; ?></div>
            <div class="dash-info-place"><?php 
			 
			  echo util::makeString($info['city'], $info['region'],DataType::$countries[$info['country']][0] );
			  
			   ?>
            </div>
            <div class="dash-info-status txt-light">
              <?php
				
				$t1 = !empty($info['t1']) ? $skills[$info['t1']]['name'] : '';
				$t2 = !empty($info['t2']) ? $skills[$info['t2']]['name'] : '';
				$t3 = !empty($info['t3']) ? $skills[$info['t3']]['name'] : '';
				
				if(empty($t1) && empty($t2) && empty($t3)) {
                
					echo '<label class="txt-red ">No Subscriptions</label>';
				}
				else{
					echo Util::makeString($t1, $t2, $t3);	
				}
				
				?></div>
          </div>
          <div class="dash-info-right">
            <a class="dash-link" cura="talent_remove_access" data-talent="<?php echo $info['request_id']; ?>" >Manage</a>
          </div>
        </div>
        <div class="dash-info-middle dash-info-desc">
          
        </div>
        <div class="dash-info-bottom nopadding bg-white">
          <a cura="agency_talent" data-id="<?php echo $info['request_id']; ?>" data-action="appointments" >Appointments</a>
         <a cura="agency_talent" data-id="<?php echo $info['request_id']; ?>" data-action="bookings">Bookings</a>
       
        </div>
        <div class="inline_load" id="talent_manage_<?php echo $info['request_id']; ?>"></div>
      </div>
      <?php
			  
		  }
		  
	  }
		else{
			echo '<div class="panel padding">There are no talents associated with the agency.<br>You may manage up to '.$subscription->manage.' talents. <a href="javascript:void(0)" cura="dash_menu" data-id="find_talents">Add talents now</a></div>';
		}
	  
	  ?>
    </div>
    </div>

</div>

