<form cura="event_search" id="event_search">
  <div class="dash-header-wrapper">
    <div class="dash-sub-header txt-orange resizable">Events</div>
    <a class="button" href="javascript:void(0)" cura="event_edit">CREATE</a>
     </div>
    
    
    <div class="panel bg-white hpadding "   >
  <div class="button-container ">
	<span class="middle filter-bar">
    <select name="status" cura="switch_event_list" data-type="change" >
      <option value="1" > Upcoming Events</option>
      <option value="2" > Previous Events</option>
      <option value="0" > Browse All Events </option>  
    </select>
    </span>
    <div class="center"></div>
    <div id="events_month" class="events_month hidden" >
            <div class="jumper" data-action="switch_month" data-callback="event_search"> <a></a>
              <div class="flex filter-bar">
                <div>
                  <select name="filter_year" id="filter_year" cura="event_search" data-type="change">
                    <?php
			$a = date('Y');
			for($i = 2021; $i <= $a+5; $i++){
				
				if($a == $i){
					echo '<option value="'.$i.'" selected>'.$i.'</option>';
				}
				else {
					echo '<option value="'.$i.'">'.$i.'</option>';
				}
			}			
			?>
                  </select>
                </div>
                <div>
                  <select name="filter_month" id="filter_month" cura="event_search" data-type="change">
                    <?php
			$a = date('n');
			for($i = 1; $i <= 12; $i++){
				
				if($a == $i){
					echo '<option value="'.$i.'" selected>'.DataType::$months[$i].'</option>';
				}
				else {
					echo '<option value="'.$i.'">'.DataType::$months[$i].'</option>';
				}
			}			
			?>
                  </select>
                </div>
              </div>
              <a></a> </div>
          </div>
     
    
     </div>
</div>
    
    <div class="flex">
    <div class="center"></div>
    
    
    </div>
    
</form>
<div class="sep"></div>
<div id="event-list">
<?php
$profile = Session::getProfile();

if(empty($profile)) {
	$events = $user->searchEvents(1, NULL, NULL);
	component('event/event_list', array('events' => $events));	
}
elseif($profile['type'] == 'a'){
	$events = $user->searchEvents(1, NULL, NULL, $profile['id']);
	component('event/event_list', array('events' => $events));
}
else{
	echo "No events found";
}



?>
</div>
