
<?php
if(!empty($invoices)){
	
	echo '<div >
<div class=" bg-yellow panel">';
	
	foreach ($invoices as $i => $invoice) {
		
		echo '<div class="padding">';
		
		// analize
		
		if($invoice['invoice']['type'] ==  DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION || 
		  	$invoice['invoice']['type'] == DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION ||
		    $invoice['invoice']['type'] ==  DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION ||
		    $invoice['invoice']['type'] == DataType::$INVOICE_TYPE_UP_SUBSCRIPTION
		  ){
			// subscription payment
			echo "<span class=\"txt-bold\">{$invoice['skill']['name']}</span> subscription is not active. You may <span class=\"txt-red\">NOT</span> receive bookings. <a cura=\"invoice_pay\" data-id=\"{$invoice['invoice']['id']}\">Activate Subscription</a>.";
		}
		elseif($invoice['invoice']['type'] ==  DataType::$INVOICE_TYPE_CREDIT){
			// single credit payment
			echo "Your single booking credit is not activated. You may <span class=\"txt-red\">NOT</span> accept bookings. <a cura=\"invoice_pay\" data-id=\"{$invoice['invoice']['id']}\">Activate booking credit</a>.";
		}
		elseif($invoice['invoice']['type'] == DataType::$INVOICE_TYPE_PROFILE){
			// new profile page payment
			echo "Your profile <span class=\"txt-bold\">{$invoice['profile']['name']}</span> is not active. <a cura=\"invoice_pay\" data-id=\"{$invoice['invoice']['id']}\">Activate profile</a>.";
		}
		
		echo '</div>';
		
		if($i < count($invoices) - 1){
			echo '<div class="dash-sep"></div>';
		}
	}
	
	
	echo '</div>
</div>';
	
}

//Util::debug($invoice);
?>
