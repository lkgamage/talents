<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange">Appointments</div>
  <a class="button" href="javascript:void(0)"  cura="appointment_options" data-action="edit">ADD</a> </div>
<div class="sep"></div>
<div class="panel">
  <div class="padding section">
    <div class="sep"></div>
    <div class="columns">
      <div class="resizable">
        <div class="tabs" data-id="tab-body"> <a class="active" cura="tabs" data-id="appointment_day" data-ref="day" data-action="appointment_search">Day</a> <a class="" cura="tabs" data-id="appointment_week" data-ref="week" data-action="appointment_search">Week</a> <a class="" cura="tabs" data-id="appointment_month" data-ref="month" data-action="appointment_search">Month</a> </div>
      </div>
      <form id="appointment-search">
        <input type="hidden" name="timespan" id="timespan" value="day">
        <input type="hidden" name="filter_week" id="filter_week" value="<?php echo date('Y-W'); ?>">
        <div id="tab-body">
          <div id="appointment_day"><span class="middle ">
            <div class="jumper" data-action="switch_day" data-callback="appointment_search"> <a></a>
              <div class="filter-bar">
                <input type="text" placeholder="Date" name="filter_date" class="datepick past" id="filter_date" data-changed="appointment_search" value="<?php echo  Util::ToDate(date("Y-m-d H:i:s")); ?>">
              </div>
              <a></a> </div>
            </span> </div>
          <div id="appointment_week"  class="hidden">
            <div class="jumper" data-action="switch_apppointment_week" data-week="<?php echo date("W"); ?>"> <a></a>
              <div class="jump-label" id="week_name"><?php
              
			  $m = date('M');
			  $w = Util::weekIndex();
			  
			  $abb = array('','st','nd','rd','th','th');
			  
			  echo $m.'. '.$w.'<span>'.$abb[$w].'</span> week'
			  
			  ?></div>
              <a></a> </div>
          </div>
          <div id="appointment_month" class="hidden">
            <div class="jumper" data-action="switch_month" data-callback="appointment_search"> <a></a>
              <div class="flex filter-bar filter-month">
                <div>
                  <select name="filter_year" id="filter_year" cura="appointment_search" data-type="change">
                    <?php
			$a = date('Y');
			for($i = 2021; $i <= $a+5; $i++){
				
				if($a == $i){
					echo '<option value="'.$i.'" selected>'.$i.'</option>';
				}
				else {
					echo '<option value="'.$i.'">'.$i.'</option>';
				}
			}			
			?>
                  </select>
                </div>
                <div>
                  <select name="filter_month" id="filter_month" cura="appointment_search" data-type="change">
                    <?php
			$a = date('n');
			for($i = 1; $i <= 12; $i++){
				
				if($a == $i){
					echo '<option value="'.$i.'" selected>'.DataType::$months[$i].'</option>';
				}
				else {
					echo '<option value="'.$i.'">'.DataType::$months[$i].'</option>';
				}
			}			
			?>
                  </select>
                </div>
              </div>
              <a></a> </div>
          </div>
        </div>
      </form>
    </div>
    <div class="dash-sep-"></div>
    <div class="sep"></div>
    <div class="sep"></div>
    <div  class="panel" id="appointments">
      <div   >
     <?php
	 $profile = Session::getProfile();
	 
	 if(!empty( $profile)) {
		 
		 $date = date('Y-m-d', time()+Session::timezone()*60);
		 
		 $object = new Profile($profile['id']);
		 $appointments = $object->getAppoinments($date, NULL, true);
		 
		 
		 
		 component('appointment/appointments-day' , array('appointments' => $appointments, 'date' => $date));
	 }
	 
	 else{
			echo "Please go to your Talent or Agency dashboard to view appointments"; 
	 }
	 ?> 
      </div>
    </div>
  </div>
  <div class="sep"></div>
  <div class="sep"></div>
  <div class="sep"></div>
  <div></div>
</div>
