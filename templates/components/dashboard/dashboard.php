<?php
$customer = Session::getCustomer();
$session = Session::getProfile();
$info = $user->getInfo();
$error = false;
?>
<?php


if(empty($info['customer']['ct_confirmed'])) {
	
	echo '<div>';
	echo '<div class="txt-l">Confirm your city, time zone and currency</div>';
	echo '<div class="sep"></div>';
	component( 'customer/basic_setting', array( 'user' => $user ) );
		echo '</div>';
	?>
	     <div class="button-container">
          <div class="center"> </div>

          <a class="button btn-next " cura="ctl_confirm" >All set, Ready to Go!</a> </div>
 	<?php
}
else {
// pening invoice vertification
if(!empty($info['customer']['invoices'])){
	
	$invoices = $user->getPendingInvoices();
	//Util::debug($invoices);
	component('dashboard/invoice_pending', ['invoices' => $invoices]);
	echo '<div class="sep"></div>';
}
elseif(!empty($info['profiles'])){
	
	foreach ($info['profiles'] as $profile) {
		
		if(!empty($session) && $session['id'] != $profile['id']){
			continue;
		}
	
		// subscription category
		if(empty($profile['categories'])){
			component('dashboard/wizard_category', array('profile' => $profile));			
		  	$error = true;
			
			break;
		}
		
		// packages		
		elseif(!empty($profile['categories'])){
			
			foreach ($profile['categories'] as $category){
				
				// if there are any pending invoices, we do not show 
				// subscription or package screen, instead, showing a warning!
				
				// check subscription
				if(empty($category['subscriptions'])){
					component('dashboard/wizard_subscriptions', array('profile' => $profile, 'category' => $category));					
					$error = true;			
					break;
				}
				elseif(empty($category['packages'])){
					component('dashboard/wizard_package', array('profile' => $profile, 'category' => $category));					
					$error = true;			
					break;
				} 
				
				
			}
						
			if($error){
			//	break;
			}
		}

		// page
		
		if(empty($info['profiles']['page']) ){
			// page not created
			
		}
		elseif(empty($info['profiles']['published']) || empty($info['profiles']['updated'])){
			// page content required
			
		}
		
	}
	
}


  
  // email and phone verification
  
 // if(!$error) {
	  
	 $_data['info'] = $info;
	  
	if(empty($session)){
		component('dashboard/customer_dashboard', $_data); 
	}
	elseif($session['type'] == 'a'){
		component('dashboard/agency_dashboard', $_data); 
	}
	else{
		component('dashboard/talent_dashboard', $_data); 
	} 
   
 }
  
  ?>
  
  

<?php
//Util::debug($info);

?>
