<?php

$info = $talent->getInfo();

$profile = Session::getProfile();
$subscriptions = $talent->getSubscriptionDetail();
$schedule = $talent->getSchedule();
$page = $talent->getPage();
$agencies = $talent->getAgencies();


$requests = $talent->getAgencyRequests( true, DataType::$AGENCY_REQEST_PENDING );
$url = Util::mapURL( '/' );
?>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange notoppadding">Talent Profile</div>
</div>
<div id="contents">
  <div class="panel">
    <div class="user-header">
      <div class="columns ">
        <div class="column-41 nopadding">
          <div class="user-header-left">
            <div class="user-header-img"> <img class="talent_<?php echo $talent->id; ?>" src="<?php 

			
			if(!empty($profile) && $profile['id'] == $talent->id){
				echo $profile['dp'];	
			}
			else{
				$img = $talent->getDP();
				echo $img->i400;	
			}
			 ?>"> <a id="profile_changer" class="icon-capture" title="Change Picture" cura="update_profile_picture" data-ref="talent" data-id="<?php echo $talent->id?>"></a> </div>
            <div class="user-header-info resizable">
              <div class="user-header-name"><?php echo $talent->name; ?></div>
              <div class="user-header-desc">
                <div class="flex">
                  <div id="profile_handle">
                    <?php

                    if ( !$page->isEmpty( 'handle' ) ) {
                      echo '<a href="' . Util::mapURL( '/' ) . urlencode( $page->handle ) . '" target="blank">@' . $page->handle . '</a>';
                    } else {
                      echo '<a class="dash-link" cura="handler_edit">Create handle</a>';
                    }

                    ?>
                  </div>
                  <div class="gap"></div>
                  <div><a class="dash-link ic-icon ic-edit txt-light " cura="handler_edit"></a></div>
                </div>
                <div class="inline_load" id="handler_edit"></div>
              </div>
              <div id="customer_info_edit"> </div>
            </div>
          </div>
        </div>
        <div class="column-4 middle left-border lpadding">
          <div class="sep"></div>
          <div class="txt-light">Member since<br>
            <?php echo Util::ToDate($talent->created, false, true); ?></div>
          <?php  if($talent->name_changed ==  0) { ?>
          <div class="sep"></div>
          <a class="dash-link ic-icon ic-edit" cura="customer_info_edit" data-profile="1">Update Name</a>
          <?php } ?>
          <div class="sep"></div>
        </div>
      </div>
    </div>
    <div class="sep"></div>
    <div class="sep"></div>
    <?php
    if ( !empty( $requests ) ) {

      ?>
    <div class="section-header txt-orange">Requests</div>
    <div class="section">You have recieved <?php echo count($requests) == 1 ? 'a request from an Agency' : ' requests from Agencies' ?><br>
      <a class="dash-link">Learn more about Agency requests</a></div>
    <div class="padding">
      <div class="hpadding">
        <?php component('talent/agency_requests', array('requests' => $requests)); ?>
      </div>
    </div>
    <?php
    }
    ?>
    <div class="section-header txt-orange">Subscriptions</div>
    <div class="padding section">
      <div class="panel min-info">
        <?php

        $categories = array();

        if ( !empty( $subscriptions ) ) {

          foreach ( $subscriptions as $item ) {

            $categories[ $item[ 'subscription' ][ 'skill_id' ] ] = true;

            ?>
        <div class="min-info-row">
          <div class="mini-info-name"><?php echo $item['skill']['name'].' - '.$item['package']['name']; ?></div>
          <div class="mini-info-detail">
            <div class=" nopadding "><?php echo $item['subscription']['term'] == 'm' ? 'Monthly' : 'Annual' ?></div>
            <div class=" nopadding resizable">Valid till <?php echo Util::ToDate($item['subscription']['exp_date'])?></div>
            <div class="right"><a class="dash-link ic-icon ic-edit" cura="subscription_view" data-id="<?php echo $item['subscription']['id']; ?>">Manage</a></div>
          </div>
        </div>
        <?php

        }

        //Util::debug($subscriptions);
        } else {
          echo "<div class=\"padding\">You have no active subscriptions.</div>";
        }

        //Util::debug($subscriptions);

        ?>
      </div>
      <?php if(count($categories) < 3) { ?>
      <div class="sep"></div>
      <div class=""> <a href="javascript:void(0)" class="<?php empty($subscriptions) ? 'button' : '' ?>"  cura="subscription_add" >New Subscription</a> </div>
      <?php } ?>
    </div>
    <div class="sep"></div>
    <div class="sep"></div>
    <div class="section-header txt-orange">Working Schedule</div>
    <div class="padding section">
      <div class="options">
        <div>
          <label class="custom-checkbox">
            <input <?php echo ($talent->limited_hours == 0) ? 'checked=""' : '' ?> type="radio" name="limited_hours" id="limited_hours_0" value="0" cura="switch_work_hours" data-type="change" data-value="0" >
            Always available<span class="checkmark"></span> </label>
        </div>
        <div class="sep"></div>
        <div>
          <label class="custom-checkbox">
            <input <?php echo ($talent->limited_hours == 1) ? 'checked=""' : '' ?> type="radio" name="limited_hours" id="limited_hours_1" value="1" cura="switch_work_hours" data-type="change" data-value="1" >
            Formal working hours<span class="checkmark" ></span> </label>
        </div>
      </div>
      <div  id="week_schedule" style="<?php echo ($talent->limited_hours == 0) ? 'display:none' : '' ?>">
        <div class="sep"></div>
        <div class="sep"></div>
        <div class="panel field-wrapper work-time">
          <?php

          $schedule = $schedule->toArray();

          foreach ( DataType::$days as $day ) {

            $abbr = substr( strtolower( $day ), 0, 3 );

            ?>
          <div class="list-select <?php echo ($schedule[$abbr.'_avl'] == 0) ? 'off-day' : '' ?>">
            <div class="col3">
              <label class="custom-checkbox">
                <input type="checkbox"  name="<?php echo $abbr; ?>_avl" id="<?php echo $abbr; ?>_avl_1" value="1" <?php echo ($schedule[$abbr.'_avl'] == 1) ? 'checked' : '' ?> cura="working_day_switch" data-type="change"  data-day="<?php echo $abbr; ?>" >
                <?php echo $day; ?><span class="checkmark" ></span> </label>
            </div>
            <div> <a cura="working_time_edit" ><?php echo !empty($schedule[$abbr.'_start'] ) ? Util::stampToTime($schedule[$abbr.'_start']) : 'NA' ?></a>
              <input type="text" value="<?php echo !empty($schedule[$abbr.'_start']) ? Util::stampToTime($schedule[$abbr.'_start']) : '' ?>" name="<?php echo $abbr; ?>_start" id="<?php echo $abbr; ?>_start" maxlength="10" class="timepick" data-changed="working_time_updated" data-close="working_time_updated">
            </div>
            <div> - </div>
            <div> <a cura="working_time_edit"><?php echo !empty($schedule[$abbr.'_ends']) ? Util::stampToTime($schedule[$abbr.'_ends']) : 'NA' ?></a>
              <input type="text" value="<?php echo !empty($schedule[$abbr.'_ends']) ? Util::stampToTime($schedule[$abbr.'_ends']) : '' ?>" name="<?php echo $abbr; ?>_ends" id="<?php echo $abbr; ?>_ends" maxlength="10" class="timepick" data-changed="working_time_updated"  data-close="working_time_updated">
            </div>
            <div class="resizable"></div>
          </div>
          <?php
          }

          ?>
        </div>
      </div>
    </div>
    <?php if(!empty($agencies) ) { ?>
    <div class="sep"></div>
    <div class="sep"></div>
    <div class="section-header txt-orange"><?php echo (count($agencies) == 1) ? 'Agency' : 'Agencies' ?></div>
    <div class="padding">
      <div class="hpadding">
        <?php foreach ($agencies as $agency) { ?>
        <div class="dash-info-row with-picture">
          <div class="dash-info-top">
            <div class="dash-info-pic"> <img src="<?php 
			
			if(!empty($agency['i400'])){
				echo $agency['i400'];
			}
			else{
				echo Util::mapURL('/images/placeholder-business');	
			}
			 ?>"> </div>
            <div class="dash-info-left middle">
              <div class="dash-info-name"><?php echo $agency['name']; ?></div>
              <div class="dash-info-place"><?php echo DataType::$AGENCY_TYEPS[$agency['agency_type']].' Agency'  ?></div>
            </div>
            <div class="dash-info-right"> <a class="button_" href="<?php echo Util::mapURL($agency['handle']); ?>"> View Page</a>
              <div class="sep"></div>
              <a class="button_" cura="agency_access_manage" data-id="<?php echo $agency['request_id']; ?>" >Manage</a> </div>
          </div>
          <div class="dash-info-place">&nbsp;</div>
          <div class="dash-info-middle dash-info-desc"></div>
          <div id="agency_access_<?php echo $agency['request_id']; ?>" class="inline_load"></div>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <div class="sep"></div>
    <div class="section-header txt-orange">Basic settings</div>
    <div class="padding">
      <div class="columns column-seperated">
        <div class="column-3 ">
          <div class="info-header">current city</div>
          <div class="info-body">
            <?php

            echo $info[ 'location' ][ 'city' ];

            if ( !empty( $info[ 'location' ][ 'region' ] ) ) {
              echo ', ' . $info[ 'location' ][ 'region' ];
            }

            echo "<br>";
            echo DataType::$countries[ $info[ 'location' ][ 'country' ] ][ 0 ];

            ?>
            <br>
          </div>
          <a class="dash-link" cura="customer_city_edit" data-profile="1">Change City</a> 
		  <div id="customer_location" class="hpadding"></div>
		  
		  
		  </div>
        <div class="column-3 lpadding">
          <div class="info-header">Time Zone</div>
          <div class="info-body">
            <?php

            if ( !$user->isEmpty( 'timezone' ) ) {
              echo DataType::$timezones[ $user->timezone ][ 0 ];
              echo '<br><span class="txt-light">UTC' . DataType::$timezones[ $user->timezone ][ 1 ] . '</span>';
            } else {
              $offset = $user->timeoffset;

              foreach ( DataType::$timezones as $code => $zone ) {

                if ( $offset == $zone[ 2 ] ) {
                  echo $zone[ 0 ];
                  echo '<br><span class="txt-light">UTC' . $zone[ 1 ] . '</span>';
                }
              }
            }


            ?>
          </div>
          <a class="dash-link" cura="customer_tz_edit" data-profile="1">Change Time Zone</a> 
		   <div id="customer_timezone" class="hpadding"></div></div>
        <div class="column-3 lpadding">
          <div class="info-header">Currency</div>
          <div class="info-body">
            <?php

            $crr = $talent->currency;

            if ( empty( $crr ) ) {
              $crr = 'USD';
            }

            echo DataType::$currencies[ $crr ];
            echo '<br><span class="txt-light">(' . $crr . ')</span>';


            ?>
          </div>
          <a class="dash-link" cura="customer_currency_edit" data-profile="1">Change Currency</a> 
		   <div id="customer_currency" class="hpadding"></div></div>
      </div>
      <div id="customer_setting" class="hpadding"></div>
    </div>
    <div class="sep"></div>
    <div class="sep"></div>
    <div class="dash-sep"></div>
    <div class="padding">
      <div class="padding">
        <div class="button-container">
          <div class="center"></div>
          <a class="dash-link">Download Data</a>
          <div class="gap1"></div>
          |
          <div class="gap1"></div>
          <a class="dash-link">Close Talent Profile</a> </div>
      </div>
    </div>
    <div class="sep"></div>
  </div>
</div>
<?php
//Util::debug($info);
?>
