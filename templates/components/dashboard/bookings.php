<form cura="search_booking" id="search_booking">
  <div class="dash-header-wrapper">
    <div class="dash-sub-header txt-orange resizable">Bookings</div>
    <input type="hidden" name="view_id" value="<?php  ?>">
    <span class="middle filter-bar">
    <select name="status" cura="search_booking" data-type="change" >
      <option value="1" > - Active - </option>
   
      <?php
	foreach (DataType::$BOOKING_STATUS as $code => $name) {
		echo '<option value="'.$code.'">'.$name.'</option>';	
	}
	
	?>
      <option value="" > - All - </option>
   
    </select>
    </span> </div>
</form>
<div class="sep"></div>
<div id="booking-list">
<div class="inline-loader" cura="search_booking" data-type="load"><span>Loading...</span></div>
</div>
