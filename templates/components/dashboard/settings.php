<?php
	$settings = $user->getCustomerSettings()->toArray();
?>

<div class="dash-header-wrapper ">
  <div class="dash-sub-header txt-orange notoppadding">Account Settings</div>
</div>
<div id="contents">
  <form cura="setting_update" id="setting_update">
    <div class="panel padding">
      <div class="sep"></div>
      <div class="section-header txt-black">Email/SMS Notifications</div>
      <div class="section">
        <div >
          <?php 
	foreach (DataType::$NOTIFICATION_TYPES as $index => $name) {
		
		$key = 'alert'.$index.'_';
	?>
          <div class="flex vpadding">
            <div class="col2"><?php echo $name; ?></div>
            <div class="resizable flex">
              <div class="col2">
                <label class="custom-checkbox nomargin">
                  <input type="checkbox" <?php echo (!empty($settings[$key.'email']) ? 'checked' : '')  ?> name="<?php echo $key; ?>email" id="<?php echo $key; ?>email" value="1">
                  Email<span class="checkmark"></span> </label>
              </div>
              <div class="col2">
                <label class="custom-checkbox nomargin">
                  <input type="checkbox"  <?php echo (!empty($settings[$key.'sms']) ? 'checked' : '')  ?> name="<?php echo $key; ?>sms" id="<?php echo $key; ?>sms" value="1">
                  SMS<span class="checkmark"></span> </label>
              </div>
            </div>
          </div>
          <?php 
	  	
			if($index != 10){
				echo '<div class="dash-sep"></div>';	
			}
	  
	  	}
      ?>
          <div class="button-container" style="display:none" cura="setting_changed" data-type="load">
            <div class="center"> </div>
            <a class="button" cura="setting_update">Update</a> </div>
        </div>
        <div class="anote">* SMS notifications available  on paid subscripts</div>
      </div>

      <div class="sep"></div>
      <div class="sep"></div>
      <div class="hidden">
        <div class="section-header txt-black">Social Logins</div>
        <div class="section"></div>
        <div class="sep"></div>
        <div class="sep"></div>
      </div>
     
      <div class="sep"></div>
      <div class="sep"></div>
    </div>
	   <div class="sep"></div>
	     <div class="panel padding">
	   <div class="section-header txt-black">Messaging</div>
      <div class="section">
        <label class="custom-checkbox nomargin">
          <input type="checkbox"  <?php echo (!empty($settings['direct']) ? 'checked' : '')  ?> name="direct" id="direct" value="1" cura="setting_update" data-type="change">
          Customers may contact me directly<span class="checkmark"></span> </label>
        <div class="anote">Paid subscriptions only. Contact button will be shown on your talent and agency pages. </div>
      </div>
	  
	  </div>
	  
	   <div class="sep"></div>
	     <div class="panel padding">
	  <div class="section-header txt-black">Push Notifications</div>
      <div class="section">
        <div class="max-800">
          <div class="min-info">
            <?php
	$devices = $user->getCustomerDevices();
	
	foreach ($devices as $dev){
		
		echo '<div class="min-info-row hpadding flex">
    	<div class="resizable">'.$dev->name;
		
		if($auth->fingureprint == $dev->fingureprint){
			echo ' <span class="txt-s txt-blue">(This device)</span>';	
		}
		
		echo '</div>';
		
		if(!$dev->isEmpty('push_id')){
			echo '<div id="push_enable_'.$dev->fingureprint.'"><span class="txt-green">Enabled</span>';
			
			if($auth->fingureprint == $dev->fingureprint){ 
			echo  '&nbsp;&nbsp;&nbsp;<a class="button button-mini" onClick="push_permission()">Test</a>';
			}
			
			echo '</div>';	
		}
		elseif($auth->fingureprint == $dev->fingureprint){ 
			echo '<div id="push_enable"><a class="button" onClick="push_permission()">Enable</a></div>';
		}
		
		else{
			echo '<div><span class="txt-gray">Not Enabled</span></div>';		
		}
		
		echo '<div class="gap"></div><div><a cura="remove_device" data-id="'.$dev->id.'" class="icon-trash"></a></div>';
		
		echo '</div>';
		
	}
	
	//Util::debug($devices);
	?>
          </div>
        </div>
      </div>
	  
	  </div>
     
  </form>
</div>
