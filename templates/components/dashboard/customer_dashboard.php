<?php

$engine = new DashboardEngine($user, $info);
//$engine->is_agency = true;

?>
<div class="sep"></div>
<div class="columns">
  <div class="resizable">
    <div class="dash-box">
      <div class="dash-box-header">
        <div>Bookings</div>
        <div></div>
      </div>
      <div class="dash-box-body ">
        <?php
	  
	  		$bookings = $engine->getRequestedBookings();
			$bookings->_pagesize = 10;
			$count = $bookings->count();
			//Util::debug($bookings);
			if($count > 0){
				
				echo ' <div class="box-row txt-bold txt-light-gray">
          <div class="box-cell cell-width-40">&nbsp;</div>
          <div class="box-cell cell-width-20">Date/Time</div>
          <div class="box-cell cell-width-20">Due</div>
		  <div class="box-cell cell-width-20">Status</div>
        </div>';
				
				$data = $bookings->getData();
				//Util::debug($data);
				
				foreach ($data as $item){
					
					
						echo ' <div class="box-row linked" cura="get_booking" data-id="'.$item['id'].'">
         <div class="box-cell cell-width-40"><div class="flex">';
		 
		 if(!empty($item['talent_id'])){
			echo  '<img src="'.$item['talent_image'].'"><div class="gap1"></div>';
			
			echo '<div class="middle"><div>'.$item['talent_name'].'</div><div class="txt-light-gray">'.$item['booking_name'].'</div></div>';
		 }
		elseif(!empty($item['agency_id'])){
			echo  '<img src="'.$item['agency_image'].'"><div class="gap1"></div>';
			
			echo '<div class="middle"><div>'.$item['agency_name'].'</div><div class="txt-light-gray">'.$item['booking_name'].'</div></div>';
		}
		else{
			echo $item['booking_name'];
		}
		 
		 echo '</div></div>
          <div class="box-cell cell-width-20 txt-s">'.Util::ToDate($item['session_start'], true).'</div>
          <div class="box-cell cell-width-20 txt-s '.(!empty($item['overdue']) ? 'txt-red' : '').'">'.Util::ToDate($item['due_date'], true).'</div>
		  <div class="box-cell cell-width-20 '.DataType::$BOOKING_CLASSES[$item['status']].'"><label class="row-status">'.DataType::$BOOKING_STATUS[$item['status']].'</label></div>
        </div>';
						
				}
				
				echo ' <div class="sep"></div><div class="box-row">
          <div class="box-cell resizable txt-s txt-gray">'.$count.' up coming bookings</div>
          <div class="box-cell"><a class="more-link" cura="dash_menu" data-id="bookings">View all bookings</a></div>
        </div>';
				
			}
			else{
				echo '<div class="padding">No bookings found</div>';
			}
	  
	  ?>
      </div>
    </div>
    <div class="sep2"></div>
    <div class="dash-box ">
      <div class="dash-box-header">
        <div>Events</div>
        <div></div>
      </div>
      <div class="dash-box-body">
      <?php
	  $events = $engine->getEvents();
	  
	  	$events->_pagesize = 10;
		$count =  $events->count();
		
		if($count > 0){
			
			$data = $events->getData();
			
			foreach ($data as $e) {
			
				echo '<div class="box-row linked" cura="event_view" data-id="'.$e['id'].'">
          <div class="box-cell cell-width-50">
		  	<div>'.$e['name'].'</div>
		  	<div class="txt-light-gray txt-s">'.$e['type_name'].'</div>
		  </div>
          <div class="box-cell cell-width-30 txt-s">'.$e['city'].'<br>'.DataType::$countries[$e['country']][0].'</div>
          <div class="box-cell cell-width-20 txt-s">'.Util::ToDate($e['begins'], true).'</div>
        </div>';
		
			}
			
			echo ' <div class="sep"></div><div class="box-row">
          <div class="box-cell resizable txt-s txt-gray">'.$count.' up coming events</div>
          <div class="box-cell"><a class="more-link" cura="dash_menu" data-id="bookings">View all events</a></div>
        </div>';
		}
		else{
			echo '<div class="padding">No events found</div>';
		}
	  
	//  Util::debug($data);
	  ?>
        
      </div>
    </div>
    <div class="sep2"></div>
    <div class="dash-box">
      <div class="dash-box-header">
        <div>Messages</div>
        <div></div>
      </div>
      <div class="dash-box-body">
        <?php
	  $boards = $engine->getMessages();
	  $img_event = Util::mapURL('images/img-party.png');
	  
	  
	  if($boards->getTotal() > 0){
	  	
		echo $boards->getPage();
		
		 echo ' <div class="sep"></div><div class="box-row">
	  <div class="box-cell resizable txt-s txt-gray"></div>
	  <div class="box-cell"><a class="more-link" cura="dash_menu" data-id="messages">View all messages</a></div>
	</div>';
	  
	  }
	  else{
		 echo '<div class="padding">No messages found</div>'; 
	  }
	  
	  ?>
      </div>
    </div>
  </div>
  <div class="column">
   <!---  messages and instructions -->

  </div>
</div>
<?php
//Util::debug($info);
?>
