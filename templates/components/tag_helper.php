<?php
$alltags = App::getTags();

if ( !empty( $top ) && (!empty( $engine->tags ) || !empty( $engine->category ) ) ) {
	
	echo '<div class="full-width top-result" id="tag_list">';

  if ( !empty( $engine->tags ) ) {

	  $tagdata = $engine->getTagsCategories();
	  
	  if(!empty($tagdata)) {
		  
		  	$tags = [];
		  
		  foreach ( $tagdata as  $i => $cat ) {
			   $t = explode( ',', $cat[ 'tags' ] );
			  
			  if(!empty($t)){
				  $tags = array_merge($tags, $t);
			  }
		  }
		  
		  shuffle($tags);
		  
		  
		  $i = 0;
		   foreach ( $tags as  $tid ) {
			   
			   if ( isset( $alltags[ $tid ] ) ) {

				 echo '<div  class="tag" cura="search_by_tag"  data-tag="' . $alltags[ $tid ] . '">' . $alltags[ $tid ] . '</div>';
			   
				  if($i == 2){
					  echo '<a cura="expend_top" class="more-link middle">More</a>';
					  break;
				  }
			   
			   	 $i++;
			   }

			}
		  
			
  		}

  } elseif ( !empty( $engine->category ) ) {
	  
	  	$tags = $engine->getCategoryTags();
	  
	  	if(!empty($tags)) {
			
			$tags = Util::groupArray( $tags, 'id', 'id' );
			shuffle($tags);
			$i = 0;

			  foreach ( $tags as  $tid ) {

				if ( isset( $alltags[ $tid ] ) ) {
				   echo '<div   class="tag" cura="search_by_tag"  data-tag="' . $alltags[ $tid ] . '">' . $alltags[ $tid ] . '</div>';
				
				 
				   if($i == 2){
					  echo '<a cura="expend_top" class="more-link middle">More</a>';
					  break;
				   }
				  
				  $i++;
				}
				  
			  }
			
			
  		}

  }	
	 
	echo '</div>';
	  

} 

/******************************/
if ( !empty( $engine->tags ) ) {

    $tagdata = $engine->getTagsCategories();

    if ( !empty( $tagdata ) ) {

        // bottom tags
        ?>
<div class="panel hpadding  bg-white <?php echo (!empty($top) ? 'hidden top-expand' : '') ?>">
  <div class="vpadding txt-bold">You may also search</div>
  <div>
    <?php
    foreach ( $tagdata as $cat ) {

      echo '<div  clss="suggest-category"><a cura="search_by_tag"  data-tag="' . $cat[ 'skill_name' ] . '">' . $cat[ 'skill_name' ] . '</a></div>';

    }

    ?>
  </div>
  <div class="sep"></div>
  <div>Find specific service</div>
  <div class="vpadding" id="tag_list">
    <?php
    $tags = explode( ',', $cat[ 'tags' ] );


    foreach ( $tags as $tid ) {

      if ( isset( $alltags[ $tid ] ) ) {
        echo '<div  class="tag" cura="search_by_tag"  data-tag="' . $alltags[ $tid ] . '">' . $alltags[ $tid ] . '</div>';
      }
    }


    ?>
  </div>
</div>
<?php


}
} elseif ( !empty( $engine->category ) ) {

    $tags = $engine->getCategoryTags();

    if ( !empty( $tags ) ) {

      ?>
<div class="panel padding  bg-white <?php echo (!empty($top) ? 'hidden top-expand' : '') ?>">
  <?php echo empty($top) ? '<div>Find specific service</div>' : ''; ?>
  <div class="vpadding" id="tag_list">
    <?php

    $tags = Util::groupArray( $tags, 'id', 'id' );

    $alltags = App::getTags();

    foreach ( $tags as $tid ) {
      if ( isset( $alltags[ $tid ] ) ) {
        echo '<div  class="tag" cura="search_by_tag"  data-tag="' . $alltags[ $tid ] . '">' . $alltags[ $tid ] . '</div>';
      }
    }


    ?>
  </div>
</div>
<?php
}
}
?>
