<form id="session-form" data-submit="savePackage">
<input type="hidden" name="s_talent" value="<?php echo (!empty($package)) ? $package->talent_id : $user->id;  ?>">
<input type="hidden" name="s_id" value="<?php echo (!empty($package)) ? $package->id : ''  ?>">
  <div class="sub-header ">
    <?php

echo (!empty($package)) ? 'Update Session' : 'New Session';

?>
  </div>
  <div class="field-wrapper">
    <label class="dynamic-label" for="s_name">Session Name</label>
    <input type="text" name="s_name" id="s_name" maxlength="400" value="<?php echo (!empty($package)) ? $package->name : ''  ?>">
  </div>
  <div class="field-wrapper">
    <label class="dynamic-label " for="s_desc" >Description</label>
    <textarea style="min-height:100px" name="s_desc" id="s_desc" maxlength="1000" ><?php echo (!empty($package)) ? $package->description : ''  ?></textarea>
  </div>
  <div class="field-wrapper">
    <label class="static-label" for="s_type">Billing Schedule</label>
    <select class="input-short" name="s_type" id="s_type" >
      <?php
			foreach (DataType::$SSTypes as $a => $b){
				
				if(!empty($package) && $package->session_type == $a) {
					echo '<option value="'.$a.'" selected >'.$b.'</option>';
				}
				else {
				
					echo '<option value="'.$a.'" >'.$b.'</option>';
				}					
			}			
			?>
    </select>
  </div>
  <div class="field-wrapper">
    <label class="static-label" for="s_timespan">Session Duration</label>
    <select class="input-short" name="s_timespan" id="s_timespan" >
      <?php
			
			foreach (Util::$TimeMap as $a => $b){
				
				if(!empty($package) && $package->timespan == $a) {
					echo '<option selected value="'.$a.'" >'.$b.'</option>';
				}
				else{
					echo '<option value="'.$a.'" >'.$b.'</option>';
				}
						
			}			
			
			?>
    </select>
  </div>
  <div class="field-wrapper">
    <label class="dynamic-label " for="s_fee" style="left:125px;">Fee</label>
    <select style="width:100px" name="s_currency" >
      <option value="USD">USD</option>
    </select>
    <input type="text" class="input-short" name="s_fee" id="s_fee" maxlength="10" value="<?php echo (!empty($package)) ? $package->fee : ''  ?>">
  </div>
  <div class="field-wrapper">
    <label class="dynamic-label " for="s_advance_percentage" >Advance Percentage</label>
    <input type="text" class="input-short" name="s_advance_percentage" id="s_advance_percentage" maxlength="3" value="<?php echo (!empty($package)) ? $package->advance_percentage : ''  ?>">
    <span class="txt-large">%</span> </div>
  <div class="field-wrapper">
    <div class="flex option-list">
      <div class="col2" style="padding-bottom:0;">
        <label class="custom-checkbox" style="margin:0">
          <input type="checkbox" name="s_is_private" class="s_is_private" <?php echo (!empty($package) && $package->is_private == 1) ? 'checked' : ''  ?> value="1" >
          Hide this session from public<span class="checkmark"></span> </label>
      </div>
    </div>
    <p class="anote">Only you can offer this session</p>
  </div>
  <div class="field-wrapper">
    <div class="flex option-list">
      <div class="col2" style="padding-bottom:0;">
        <label class="custom-checkbox" style="margin:0">
          <input type="checkbox" name="location_required" <?php echo (!empty($package) && $package->location_required == 1) ? 'checked' : ''  ?> class="location_required" <?php echo (empty($package) || $package->location_required == 1) ? 'checked' : ''  ?> >
          Location required<span class="checkmark"></span> </label>
      </div>
    </div>
    <p class="anote">Hotel, Reception hall, Auditorium or city name is required to make this booking</p>
  </div>
  
  <div class="button-container">
    <div class="center"></div>
    <input type="submit" value="SAVE" class="button">

    <div class="gap"></div>
    <a class="button button-alt" href="javascript:void(0)" onClick="getPackages()">CANCEL</a> </div>
    <div class="sep"></div>
</form>
