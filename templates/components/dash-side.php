<?php
$cache = Session::getProfile();
$customer = Session::getCustomer();

$profiles = $user->getProfiles(true);

 if(!empty($cache)){
	
	 foreach ($profiles as $p){
		if($p['id'] == $cache['id']) {
			$profile = $p;
			break;
		}
	 }
 }

?>
<div class="dash-menu" >

  <div class="dash-profile-outer" <?php echo !empty($profiles) ? 'cura="toggle_profiles"' : '' ?> >
  <?php
  // if profile selected, display dashboard for the profile

   if(!empty($cache)) { 
  ?>
    <div class="dash-profile">
      <div class="dash-profile-image"> <img src="<?php echo $cache['dp']; ?>" class="profile_<?php echo $cache['id']; ?>"> </div>
      <div class="dash-profile-name"> <span><?php echo ucwords($cache['name']); ?></span><?php echo (empty($profile['is_agency'])) ? str_replace(',', ', ', $profile['skills']) : 'Agency';  ?> </div>
    </div>
    <?php } else {
		// display dashboard for customer
	?>
    <div class="dash-profile">
      <div class="dash-profile-image"> <img src="<?php echo $customer['dp']; ?>" class="customer_<?php echo $customer['id']; ?>"> </div>
      <div class="dash-profile-name"> <span><?php echo  $customer['name']; ?></span>Customer Dashboard </div>
    </div>
    <?php } ?>
    
    <?php if(!empty($profiles) ){ ?>
    
    <div class="dash-profile-change"><a cura="toggle_profiles"><span>Switch Profile</span></a></div>

  
    <div class="profile-selection">
    <?php
		// display customer line
		if(!empty($cache)) {
			echo '<a cura="switch_profile" data-customer="'.$customer['id'].'"><img class="customer_'.$customer['dp'].'" src="'.$customer['dp'].'"><span>'.$customer['name'].'<label>Customer</label></span></a>';
		}
	
		foreach ($profiles as  $p){
			
				if(empty($p['id']) || (!empty($cache) && $p['id'] == $cache['id'] )){
					continue;
				}
			
			$img = fixImage($p['thumb'], (isset($p['gender']) ? $p['gender'] : NULL), $p['is_agency'] == 1);
			
						
			echo '<a cura="switch_profile" data-profile="'.$p['id'].'"><img class="profile_" src="'.$img.'"><span>'.$p['name'].'<label>'.($p['is_agency'] == 1 ? 'Agency' : 'Talent').'</label></span></a>';
		}
	?>    	
    </div>
    <div class="dash-sep"></div>
     <?php } ?>
    

  </div>
   
    <div class="sep"></div>    
  
  <div class="dash-menu-wrapper">
   
    <?php
	if(empty($cache['type'])){
	// General customer/booker
	?>
    <a class="menu-link" cura="dash_menu" data-id="dashboard">Dashboard</a> 
    <a class="menu-link" cura="dash_menu" data-id="bookings">Bookings</a> 
    <a class="menu-link" cura="dash_menu" data-id="events">Events</a> 
 
    
    <?php	
	}
	elseif( $cache['type'] == 't') {
	// talent
	?>
    <a class="menu-link" cura="dash_menu" data-id="dashboard">Dashboard</a> 
    <a class="menu-link" cura="dash_menu" data-id="bookings">Bookings</a> 
    <a class="menu-link" cura="dash_menu" data-id="appointments">Appointments</a> 
    
    <a class="menu-link" cura="dash_menu" data-id="packages">Packages</a> 
     <a class="menu-link" cura="dash_menu" data-id="profile">Profile</a> 
    <a class="menu-link" cura="dash_menu" data-id="insight" style="display:none">Insight</a>
    
    <?php	
	}
	elseif($cache['type'] == 'a') {
	// An agency
	?>
    <a class="menu-link" cura="dash_menu" data-id="dashboard">Dashboard</a> 
    <a class="menu-link" cura="dash_menu" data-id="bookings">Bookings</a> 
     <a class="menu-link" cura="dash_menu" data-id="talents">Talents</a>
    <a class="menu-link" cura="dash_menu" data-id="appointments">Appointments</a> 
    <a class="menu-link" cura="dash_menu" data-id="events">Events</a>
    <a class="menu-link" cura="dash_menu" data-id="packages">Packages</a> 
     <a class="menu-link" cura="dash_menu" data-id="profile">Profile</a> 
    <a class="menu-link" cura="dash_menu" data-id="insight" style="display:none">Insight</a>
    
    <?php	
	}
	
	?>
   
    

    <div class="dash-sep"></div>

    <a class="menu-link" cura="dash_menu" data-id="messages">Messages</a>
    <a class="menu-link" cura="dash_menu" data-id="favourite">Favorite</a>
    <a class="menu-link" cura="dash_menu" data-id="account">Account</a>
	<a class="menu-link" cura="dash_menu" data-id="billing">Billing</a>
    <a class="menu-link" cura="dash_menu" data-id="settings">Notifications</a>
   
  
  </div>
  <div class="dash-menu-button" cura="activate_menu"></div>
 
</div>