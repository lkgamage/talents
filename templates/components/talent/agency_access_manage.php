<form cura="agency_access_update" id="agency_access_update">
<input type="hidden" name="id" value="<?php echo $request->id; ?>">
<div class="dash-sep"></div>
<div class="padding">
  <div>Manage Additional Permissions</div>
  <?php component('talent/agency_manage', array('request' => $request)); ?>
  <div class="sep"></div>
  <div class="right"> <a class="dash-link">Learn more about joining with an Agency</a></div>
</div>
<div class="hpadding bg-gray">
  <div class="button-container "> <a cura="agency_access_remove" data-id="<?php echo $request->id; ?>" >Remove access to agency</a>
    <div class="center"></div>
    <a class="button " cura="agency_access_update">UPDATE PERMISSION</a>
    <div class="gap"></div>
    <a class="button  button-alt" cura="inline_load_cancel">CLOSE</a> </div>
</div>
</form>