<?php foreach ($requests as $req) {  ?>
<div class="dash-info-row with-picture bg-blue" >
  <div class="dash-info-top">
    <div class="dash-info-pic"  cura="redirect_to" data-url="/<?php echo $req['handle'] ?>"><img src="<?php 
			
			if(!empty($req['i400'])){
				echo $req['i400'];
			}
			else{
				echo Util::mapURL('/images/placeholder-business');	
			}
			 ?>"> </div>
    <div class="dash-info-left middle">
      <div class="dash-info-name "><a href="<?php echo $url.$req['handle']; ?>"><?php echo $req['agency_name']; ?></a></div>
      <div class="dash-info-place">
        <?php
        echo ucwords( util::makeString( $req[ 'city' ], $req[ 'region' ], DataType::$countries[ $req[ 'country' ] ][ 0 ] ) );
        ?>
      </div>
      <div class="dash-info-place"><?php echo DataType::$AGENCY_TYEPS[$req['agency_type']]; ?> Agency</div>
    </div>
    <div class="dash-info-right">
		<div class="button-container nopadding">
		<div class="button-wrapper">
		<a class="button" cura="agency_request_respond" data-id="<?php echo $req['id'] ?>" data-action="accept" > Accept </a>
      <div class="gap"></div>
      <a class="button button-alt" cura="agency_request_respond" data-id="<?php echo $req['id'] ?>" data-action="reject" >Reject&nbsp;</a> </div>
		</div>
	  </div>
  </div>
 
  <div class="inline_load" id="agency_request_<?php echo $req['id'] ?>" ></div>
</div>
<?php 
//Util::debug($req);							  
								  } ?>
