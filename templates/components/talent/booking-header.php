<?php
$stack = Util::makeArray($info['event']['name'], $info['event']['type_name'], $info['booking']['venue'], $info['location']['address'], $info['location']['city'], $info['location']['region'], DataType::$countries[$info['location']['country']][0]);
?>
<div class="padding">
  <div class="columns coulumn-seperated">
    <div class="column-31 ">
      <div class="user-header-name"><?php echo array_shift($stack); ?></div>
      <?php if(count($stack) > 3) { ?>
      <div class="user-header-desc"><?php echo array_shift($stack); ?></div>
      <?php } ?>
      <div class="user-header-desc"><?php echo implode(', ', $stack); ?></div>
      <div class="sep"></div>
      <div>
       <div class="txt-bold <?php 
		  echo ($info['booking']['status'] ==    DataType::$BOOKING_PENDING ) ? 'txt-orange' :($info['booking']['status'] == DataType::$BOOKING_REJECT ? '' : 'txt-blue');
		  
		   ?>"><?php echo DataType::$BOOKING_STATUS[$info['booking']['status']]; ?></div>
      </div>
    </div>
    <div class="column-3 left-border">
      <div class="info-header">BOOKING ID: <?php echo $info['booking']['id']; ?></div>
      <div><?php echo $info['customer']['firstname'].' '.$info['customer']['lastname']; ?></div>
     
      <?php if($info['booking']['status'] ==  DataType::$BOOKING_CONFIRM) { ?>
      <div class="user-header-desc txt-light"><?php echo Util::makeString( $info['location']['city'], $info['location']['region'], DataType::$countries[$info['location']['country']][0]); ?></div>
      <?php } ?>
    </div>
  </div>
</div>
