<div class="hpadding">
  <div class="sep"></div>
  <div>
    <label class="custom-checkbox">
      <input class="additional_permission" name="confirm_appointments" id="confirm_appointments" type="checkbox" <?php echo (!empty($request) && $request->confirm_appointments == 1) ? 'checked' : '' ?> >
      Confirm appointments behalf of you<span class="checkmark"></span> </label>
  </div>
  <div class="sep"></div>
  <div>
    <label class="custom-checkbox">
      <input class="additional_permission" name="view_bookings" id="view_bookings" type="checkbox"  cura="sync_agency_permission_model" data-type="change"  <?php echo (!empty($request) && $request->view_bookings == 1) ? 'checked' : '' ?>  >
      View your bookings<span class="checkmark"></span> </label>
  </div>
  <div class="sep"></div>
  <div>
    <label class="custom-checkbox">
      <input class="additional_permission" name="view_booking_detail" id="view_booking_detail" type="checkbox" cura="sync_agency_permission_model" data-type="change" <?php echo (!empty($request) && $request->view_booking_detail == 1) ? 'checked' : '' ?> >
      View your booking detail<span class="checkmark"></span> </label>
  </div>
  <div class="sep"></div>
  <div>
    <label class="custom-checkbox">
      <input class="additional_permission" name="manage_bookings" id="manage_bookings" type="checkbox" cura="sync_agency_permission_model" data-type="change" <?php echo (!empty($request) && $request->manage_bookings == 1) ? 'checked' : '' ?> >
      Manage your bookings<span class="checkmark"></span> </label>
  </div>
  <div class="sep"></div>
  <div>
    <label class="custom-checkbox">
      <input class="additional_permission" name="manage_page" id="manage_page" type="checkbox" <?php echo (!empty($request) && $request->manage_page == 1) ? 'checked' : '' ?> >
      Manage public page contents<span class="checkmark"></span> </label>
  </div>
  
</div>
