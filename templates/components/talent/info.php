<div class="inline-editor-panel strip-light-gray">
<?php  if($object->name_changed ==  0) { ?>
<p class="nopadding">Display name can be edited only once.</p>
<div class="vpadding txt-light">If you want to change any of this information later, you have to contact customer support.</div>
  <form cura="customer_info_update" id="customer_info_update">
        <input type="hidden" name="profile" value="1">  
      <div class="field-wrapper">
      <label class="dynamic-label" for="display_name" >Display Name</label>
      <input  class="input-short" type="text" value="<?php echo !empty($object) ? $object->name : '' ?>" name="display_name" id="display_name" maxlength="100">
    </div>
    <div class="sep"></div>
    
    
   
    <div class="button-container">
      <div class="center"></div>
		<div class="button-wrapper">
		<a class="button btn-next" cura="customer_info_update" >Update</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="customer_info_close" >Cancel</a>
		</div>
       </div>
  </form>
  <?php } else { ?>
  <div>You have already updated your personal details. <br>Please contact customer support to make any changes.</div>
  <?php } ?>
</div>
