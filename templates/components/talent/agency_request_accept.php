<?php

$agency = $request->getAgency();


?><form  cura="agency_request_accept" id="agency_request_accept">
<input type="hidden" name="request_id" value="<?php echo $request->id ?>">
  <div>
    <div class="dash-sep"></div>
    <div class="bg-white noborder padding">
      <div class="txt-m txt-blue">You are about to join with <?php echo $agency->name; ?></div>
      <div class="sep"></div>
      <div > When you join with an Agency, they'll have following permissions by default.
        <div class="hpadding">
          <div class="sep"></div>
          <div>
            <label class="custom-checkbox">
              <input checked="" type="radio">
              Read  your appointments<span class="checkmark"></span> </label>
          </div>
          <div class="sep"></div>
          <div>
            <label class="custom-checkbox">
              <input checked="" type="radio">
              Create pencil appointments for you<span class="checkmark"></span> </label>
          </div>
        </div>
      </div>
      <div class="sep"></div>
  <?php    
     // 
     // <div class="vpadding"> You may grant additional permissions if nesasasry.
     //   component('talent/agency_manage'); 
	 // </div>
 ?>      
       
     
      
      <a class="dash-link">Learn more about joining with an Agency</a> </div>
    <div class="hpadding">
      <div class="button-container ">
      <a cura="chat_open" data-agency="<?php echo $request->agency_id ?>">Send a message</a>
        <div class="center"></div>
		  <div class="button-wrapper">
        <a class="button button-act" cura="agency_request_accept">ACCEPT REQUEST</a>
        <div class="gap"></div>
        <a class="button  button-alt" cura="inline_load_cancel">CLOSE</a> </div></div>
    </div>
  </div>
</form>
