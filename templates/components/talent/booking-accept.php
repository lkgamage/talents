<?php
// Talent/agancy view
$info = $booking->getInfo();

if(empty($talent)){
	$talent = $booking->getTalent();
}

// display title and other data


$date = Util::ToDate($info['booking']['session_start'], 1);

$dt = explode(' ', $info['booking']['session_start']);
$appointments = $talent->getAppoinments($dt[0]);

$timestamp = strtotime($info['booking']['session_start']);
?>

<div class="panel">
  <?php component('talent/booking-header', array('info' => $info)); ?>
  <div class="dash-sep"></div>
  <div class="" style="display:none">Important info</div>
  <div class="sep"></div>
  <div class="padding">
    <div class="columns">
      <div class="column-2 ">
        <form cura="booking_accept" id="booking_accept">
          <input type="hidden" name="booking" value="<?php echo $booking->id; ?>" >
          <div class="block-header txt-orange">package</div>
          <div class="info-body"> 
            <!---- left column ---->
            <div class="txt-m"><?php echo $info['package']['name']; ?></div>
            <div class="txt-light"><?php echo $info['skill']['name']; ?></div>
            <div><?php echo currency($info['package']['fee'], $info['talent']['currency']); ?></div>
            <div class="sep"></div>
            <a class="dash-link" cura="package_change" data-booking="<?php echo $booking->id; ?>">Offer diffrent package</a>
            <div id="package_change"></div>
            <div class="dash-body-standalong">
              <div class="block-header txt-orange">date/time</div>
              <div class="info-body txt-m"> <?php echo $date[0]; ?>
                <div class="txt-light"><?php echo $date[1]; ?></div>
              </div>
            </div>
            <div class="dash-body-standalong">
              <div class="block-header txt-orange">Additional expences</div>
              <div class="info-body" id="additional_exp" data-label="Ex: Travel, Accomodation or Meals">
                <div class="txt-light"> Ex: Travel, Accomodation or Meals</div>
                <div class="sep"></div>
              </div>
              <a class="button" cura="expence_add">Add an expence</a> </div>
            <div class="dash-body-standalong">
              <div class="block-header txt-orange">Discount</div>
              <div class="info-body" >
                <div class="field-wrapper nopadding">
                  <label>You may give a discount for this booking (<?php echo $info['talent']['currency']; ?>)</label>
                  <input type="text" class="input-short" name="discount" id="discount" max="<?php echo $info['booking']['talent_fee'];   ?>">
                </div>
              </div>
              <div class="sep"></div>
            </div>
            <div class="dash-body-standalong">
              <div class="block-header txt-orange">People/assistants</div>
              <div class="info-body" >
                <div class="field-wrapper nopadding">
                  <label>How many additional people/assistants participating?</label>
                  <input type="text" class="input-short" max="100" name="additional_people" id="additional_people">
                  <div class="anote">Leave blank if none.</div>
                </div>
              </div>
              <div class="sep"></div>
            </div>
            
            <!---- left column ends----> 
          </div>
        </form>
      </div>
      <div class="column-2"> 
        <!---- right column ---->
        <div class="block-header txt-orange">Appointmens </div>
        <div class="info-body">
          <div class="txt-bold txt-m" ><?php echo $date[0]; ?></div>
          <div class="sep"></div>
          <div class="panel" id="appoinments">
            <?php
		  
		  $slots = array();
		  $inserted = false;
		  
		  if(!empty($appointments)){
			
				foreach ($appointments as $a){
				
					$k = strtotime($a->begins);
					
					if($k > $timestamp && !$inserted){
						$slots[] = true;
						$inserted = true;
					}
					
					$slots[] = $a;					
				}
				
				
				if(!$inserted){
					$slots[] = true;
				}
			  
		  }
		  
		  // display slots
		  if(!empty($slots)){
			
			foreach ($slots as $a){
				
				if($a === true){
					
					echo '<div>';
					echo '  <div class="appointment-row bg-orange" >';
					echo '    <div class="appointment-date this-appointment">';
					echo '      <div>'.$date[1].'</div>';
					echo '    </div>';
					echo '    <div class="appointment-desc txt-orange txt-bold">';
					echo '      <div>THIS BOOKING</div>';
					echo '    </div>';
					echo '    <div class="appointment-duration txt-orange">';
					echo '      <div>'.Util::ToTime($info['package']['timespan']).'</div>';
					echo '    </div>';
					echo '  </div>';
				}
				else {
			
					$times = $a->getTimes();	
					
					//echo '<div>';
					echo '  <div class="appointment-row '.($a->confirmed == 1 ? 'appointment-confirmed' : 'appointment-scheduled').'" cura="appointment_brief" data-id="'.$a->id.'" data-nooption="1">';
					echo '    <div class="appointment-date">';
					echo '      <div>'.$times['begin']['time'].'</div>';
					echo '    </div>';
					echo '    <div class="appointment-desc">';
					echo '      <div>'.(strlen($a->description) > 40 ? substr($a->description,0,40).'...' : $a->description);
					echo '        <div></div>';
					echo '      </div>';
					echo '    </div>';
					echo '    <div class="appointment-duration">';
					echo '      <div>'.$times['duration'].'</div>';
					echo '    </div>';
					echo '  </div>';
					
					if(!empty($times['interval'])) { 
					
					echo '  <div class="appointment-row rest-time '.($a->confirmed == 1 ? 'appointment-confirmed' : 'appointment-scheduled').'" cura="appointment_brief" data-id="'.$a->id.'" data-nooption="1">';
					echo '    <div class="appointment-date">';
					echo '      <div>'.$times['interval']['begin'].'</div>';
					echo '    </div>';
					echo '    <div class="appointment-desc">';
					echo '      <div>Rest time</div>';
					echo '    </div>';
					echo '    <div class="appointment-notes">';
					echo '      <div></div>';
					echo '    </div>';
					echo '    <div class="appointment-duration">';
					echo '      <div>'.$times['interval']['duration'].'</div>';
					echo '    </div>';
					echo '  </div>';
					}
					//echo '</div>';
				}
				
			}			  
		  }
	
		  ?>
          </div>
        </div>
        <div class="sep"></div>
        
        <!---- right column end ----> 
      </div>
    </div>
    
  </div>
  <div class="hpadding">
      <div class="button-container"> 
       <div class="center"> </div>
      <a class="button button-act txt-m" cura="booking_accept">ACCEPT BOOKING</a>
       <div class="gap"></div>
        <a class="button btn-next button-alt txt-m" cura="booking_reject" data-id="<?php echo $booking->id; ?>">DECLINE</a> </div>
      <div class="sep"></div>
      <div>When you ACCEPT the booking, we shall send an invoice to the customer. </div>
      <div class="txt-light">You'll be notified when customer confirmed the booking.</div>
    </div>
  <div class="sep"></div>
</div>
<script>

var expence_types = <?php echo json_encode(DataType::$EXPENCES_TYPE); ?>;
var currency = '<?php echo $booking->currency; ?>';
</script> 
