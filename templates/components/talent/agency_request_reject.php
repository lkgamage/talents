<form  cura="agency_request_reject" id="agency_request_reject">
  <input type="hidden" name="request_id" value="<?php echo $request->id ?>">
  <div class="dash-sep"></div>
  <div class="bg-white noborder padding">
    <div> You may contact the agency and discuss about the contract.
      <div class="sep"></div>
      Are you sure you want to reject the request?</div>
  </div>
  <div class="hpadding">
    <div class="button-container "> <a>Send a message</a>
      <div class="center"></div>
      <a class="button button-act" cura="agency_request_reject">REJECT REQUEST</a>
      <div class="gap"></div>
      <a class="button  button-alt" cura="inline_load_cancel">CLOSE</a> </div>
  </div>
  </div>
</form>
