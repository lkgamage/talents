
<form id="otp_form" cura="validate_otp" >
  <input type="hidden" name="purpose" value="<?php echo isset($purpose) ? $purpose : 'login' ?>">
   <input type="hidden" name="code_id" id="code_id" value="<?php echo $code->id; ?>">
 
  <div >
    <div class="sep"></div>
    <h1 class="page-header txt-bold nopadding">Hi <?php echo $user->firstname; ?>,</h1>
    <div class="sep"></div>
    <p>One time passcode has sent to your
      <?php 
	  echo ($type == 'p') ? 'mobile ' : 'email ';
	  echo $username; ?>
      .</p>
    <div class="center">Please enter OTP to continue.</div>
    <div class="field-wrapper" style="text-align:center">
      <input type="text" class="input-short is-centered vcode" name="vcode" id="vcode" maxlength="6" >
    </div>
    <div id="_server_error"></div>
    <div class="button-container">
      <div class="center"><a class="button btn-next" cura="validate_otp">Verify OTP</a></div>
    </div>
    <div class="sep"></div>
    <div class="sep" cura="otp_timer" data-type="load"></div>
    <div>Haven't received OTP yet?</diV>
    <div  id="otp_resend">You may request to re-send OTP in <span id="otp_delay">2:00</span></div>
    <div class="sep"></div>
    <?php

		echo '<div>Have a problem with receiving OTP?</div>';
		echo '<a href="'.Util::mapURL('/login').'" class="txt-light">Login with your '.($type == 'p' ? 'email address' : 'mobile number').'</a>';
		

	?>
  </div>
</form>
