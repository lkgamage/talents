<?php

$times = $appointment->getTimes();

?>

<div class="appointment-brief">
  <div class="padding txt-b <?php echo ($appointment->confirmed == 1) ? 'bg-orange' : 'bg-gray'?>"><?php echo ucfirst($appointment->description);?></div>
  <div class="dash-sep"></div>
  <div >
    <div class="hpadding">
      <div class="compact-row">
        <div>STATUS</div>
        <div>
          <?php 
		  
		  if($appointment->confirmed == 1){
			 echo '<span class="txt-orange">Confirmed</span>'; 
		  }
		  else{
			  echo $appointment->getStatus();
		  }
		  ?>
        </div>
        <span class="txt-orange"></span> </div>
    </div>
    <?php if($appointment->confirmed == 0 && !$appointment->isPast()){ 
	
		echo ' <div class="padding txt-light bg-gray txt-s">';
		
		if(!$appointment->isEmpty('expired') ){
			echo 'If not confirmed on or before '.Util::ToDate($appointment->expired, true).', this will be canceled.';
		}
		else{
			echo 'This time slot is still available for booking.';	
		}
		
		echo '</div>';
	}
	?>
    <?php if($appointment->all_day == 1) { ?>
    <div class="dash-sep hpadding">
      <div class="compact-row">
        <div>DATE</div>
        <div><?php echo $times['begin']['date'].'<br><span class="txt-blue">All day<span>';  ?></div>
      </div>
    </div>
    <?php } else { ?>
    <div class="dash-sep hpadding">
      <div class="compact-row">
        <div>BEGINS</div>
        <div><?php echo $times['begin']['date'].'<br><span class="txt-blue">'.$times['begin']['time'].'<span>';  ?></div>
      </div>
    </div>
    <div class="dash-sep hpadding">
      <div class="compact-row">
        <div>ENDS</div>
        <div><?php echo ($times['begin']['date'] == $times['end']['date'] ? '' : $times['end']['date']).' <span class="txt-blue">'.$times['end']['time'].'<span>';  ?></div>
      </div>
    </div>
    <div class="dash-sep hpadding">
      <div class="compact-row">
        <div>DURATION</div>
        <div><?php echo $times['duration'];  ?></div>
      </div>
    </div>
    <?php if(!empty($times['interval'])) { ?>
    <div class="dash-sep hpadding">
      <div class="compact-row">
        <div>REST TIME</div>
        <div><?php echo $times['interval']['begin'].' - '.$times['interval']['end'].'<br>'.$times['interval']['duration'];  ?></div>
      </div>
    </div>
    <?php } } ?>
    <?php if(!$appointment->isEmpty('note')) { ?>
    <div class="padding">
      <div class="scroll brief-note"> <?php echo nl2br($appointment->note); ?> </div>
    </div>
    <?php } ?>
  </div>
  <div class="sep"></div>
  <div class="sep"></div>
  <div class="button-container hpadding"> 
    <div class="center"></div> 
    <?php if(!isset($_POST['nooption'])) { ?>
    <a class="button " cura="appointment_view" data-id="<?php echo $appointment->id; ?>">DETAILS</a>
    <div class="gap1"></div>
    <a class="button " cura="appointment_edit" data-id="<?php echo $appointment->id; ?>"> CHANGE </a> 
    <div class="gap1"></div>
    <?php } ?>
    <a class="button button-alt " cura="close_dialog">CLOSE</a> </div>
  <div></div>
</div>
<?php
//Util::debug($appointment);
//Util::debug($times);
?>
