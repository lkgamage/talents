<?php
$datettime = (!empty($event)) ? Util::ToDate($event->begins, 1) : array('','');
$endtime = (!empty($event) && !$event->isEmpty('ends')) ? Util::ToDate($event->ends, 1) : array('','');

?>
<div class="inline-editor-panel strip-light-gray">
<form id="event_update" cura="event_update">
  <input type="hidden" name="address" class="_address" value="<?php echo Util::postValue('address') ?>">
  <input type="hidden" name="place" class="_place" value="<?php echo Util::postValue('place') ?>">
  <input type="hidden" name="postal" class="_postal" value="<?php echo Util::postValue('postal') ?>">
  <input type="hidden" name="country" class="_country" value="<?php echo Util::postValue('country') ?>">
  <input type="hidden" name="city" class="_city" value="<?php echo Util::postValue('city') ?>">
  <input type="hidden" name="region" class="_region" value="<?php echo Util::postValue('region') ?>">
  <input type="hidden" value="<?php echo !empty($event) ? $event->id : '' ?>" name="event_id" id="event_id" >
  <input type="hidden" value="<?php echo !empty($booking) ? $booking->id : '' ?>" name="booking_id" id="booking_id" >
  <input type="hidden" value="<?php echo !empty($event) ? $event->location_id : '' ?>" name="event_location_id" id="event_location_id" >
  <div class="field-wrapper">
    <label class="dynamic-label dynamic-label-fold" for="event_event_type" >Event Type</label>
    <select name="event_event_type" id="event_event_type"  class="input-short" >
      <?php	
	foreach(DataType::$EVENT_CATEGORIES as $id => $name) {
	
		if(!empty($event) && $event->event_type == $id) {
			echo '<option value="'.$id.'" selected>'.$name[0].'</option>';
		}
		else{
			echo '<option value="'.$id.'">'.$name[0].'</option>';
		}
	}	
	?>
      <option value="0"> - Other - </option>
    </select>
  </div>
  <div class="field-wrapper">
    <label class="dynamic-label" for="event_name" >Event Name</label>
    <input type="text" value="<?php echo !empty($event) ? $event->name : '' ?>" name="event_name" id="event_name" maxlength="400">
    <div class="anote">Ex: My 21st birthday or My promotion party</div>
  </div>
  <div class="field-wrapper">
    <label class="dynamic-label" for="event_description" >Description</label>
    <textarea name="event_description" id="event_description" maxlength="1000" ><?php echo !empty($event) ? $event->description : '' ?></textarea>
  </div>
  <div class="field-wrapper col2">
    <label class="dynamic-label dynamic-label-fold" for="event_begins" >Event Begins</label>
    <div class="flex">
      <div class="col2">
        <input placeholder="Date" type="text" class="datepick" id="event_begins_date" name="event_begins_date" value="<?php echo $datettime[0];  ?>">
      </div>
      <div class="col2">
        <input placeholder="Time" type="text" class="timepick" id="event_begins_time" name="event_begins_time"   value="<?php echo $datettime[1];   ?>" >
      </div>
    </div>
  </div>
  <div class="field-wrapper col2">
    <label class="dynamic-label  dynamic-label-fold" for="event_ends" >Event Ends</label>
    <div class="flex">
      <div class="col2">
        <input placeholder="Date" type="text" class="datepick" id="event_ends_date" name="event_ends_date" value="<?php echo $endtime[0];   ?>">
      </div>
      <div class="col2">
        <input placeholder="Time" type="text" class="timepick" id="event_ends_time" name="event_ends_time"   value="<?php echo $endtime[1];   ?>" >
      </div>
    </div>
  </div>
  <div class="field-wrapper ">
    <label class="custom-checkbox">
      <input type="checkbox" name="event_charitable" value="1" <?php echo (!empty($event) && $event->charitable == 1) ? 'checked' : ''; ?> >
      This is a charitable event. <span class="checkmark"></span> </label>
  </div>
  <div class="field-wrapper">
    <label class="dynamic-label  dynamic-label-fold" for="event_venue" >Venue/Place</label>
    <input type="text" value="<?php echo !empty($event) ? $event->venue : '' ?>" name="event_venue" id="event_venue" maxlength="400">
  </div>
  <div class="button-container">
    <div class="center"> </div>
    <a class="button " cura="booking_event_update" >Update</a> </div>
</form>
</div>