<?php
if (!empty( $profiles ) ) {

		foreach ($profiles as $pf) {
			
		  echo '<div cura="switch_profile" data-profile="'.$pf['id'].'" >
		  <div class="flex linked ">
			<div class="dash-profile"> <img src="'.fixImage($pf['thumb'], $pf['gender'], $pf['is_agency']).'" > </div>
			<div class="lpadding resizable middle">
			  <div class="dash-profile-name"> <span>'.ucwords($pf['name']).'</span>'.$pf['skills'].'</div>
			</div>
		  </div>
		</div>
		<div class="dash-sep"></div>';
			
	}

}

//Util::debug( $profiles );
?>
