<?php
$info = $booking->getInfo();

if(empty($contacts)){
	$contacts = $booking->getContacts();	
}
?>
<div class="splash">
  <div class="panel">
    <div class="success-banner">SUCCESS!</div>
    <div class="sep"></div>
    <div class="section padding">
      <div>
        <p class="center">Your booking has confirmed!</p>
        <div>Thank you! <br>
          Booking confirmation has emailed to <span class="txt-bold"><?php echo $contacts['customer']['email']; ?></span>.</div>
        <div class="sep"></div>
        <div class="list-select">
  <div class="resizable">Booking ID </div>
  <div><?php echo $booking->id; ?></div>
</div>

<div class="list-select">
  <div class="resizable">Payment ID </div>
  <div><?php echo $info['payment']['id']; ?></div>
</div>
<p></p>
<div>You may make arrangements and discuss about changes with <?php echo $info['talent']['name']; ?> on the message board. </div>
<p></p>
<div class="center txt-l">Enjoy!</div>

<div class="button-container">
      
      <a class="txt-m" href="javascript:void(0)" cura="get_booking" data-id="<?php echo $booking->id; ?>">View My Booking</a> <div class="center"> </div></div>
 <div class="button-container">
      <div class="center"> 
      <a class="txt-m button" href="<?php echo Util::mapURL('/search'); ?>">Make Another Booking</a> </div></div>       
      </div>
    </div>
  </div>
</div>

