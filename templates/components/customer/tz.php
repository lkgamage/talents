<div class="inline-editor-panel strip-light-gray">
  <form cura="customer_setting_update" id="customer_setting_update">
  <input type="hidden" name="profile" value="<?php echo (!empty($_POST['profile'])) ? 1 : 0 ?>">
    <input type="hidden" name="country" value="" class="_country">
    <div>Update time zone</div>
    <div class="sep"></div>
    <div class="columns">
    <div class="resizable">
    <div class="field-wrapper">
      <label class="dynamic-label dynamic-label-fold" for="timezone" >Time Zone</label>
      <select name="timezone">
        <?php
			$tz = $object->timezone;
			$offset = $object->timeoffset;
			
			foreach (DataType::$timezones as $code => $zone){
				
				if(empty($tz) && !empty($offset) && $offset == $zone[2]){
					echo '<option selected value="'.$code.'">'.$zone[0].'</option>';
				}
				elseif(!empty($tz) && $tz == $code){
					echo '<option selected value="'.$code.'">'.$zone[0].' ('.$zone[1].')</option>';
				}
				else{
					echo '<option value="'.$code.'">'.$zone[0].' ('.$zone[1].')</option>';
				}
				
			}		
			
			?>
      </select>
    </div>
    <div class="button-container">
      <div class="center"></div>
		<div class="button-wrapper">
		 <a class="button btn-next" cura="customer_setting_update" >Update</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="inline_load_cancel" >Cancel</a>
		</div>
      </div>
  </form>
</div>
