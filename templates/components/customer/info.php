<div class="inline-editor-panel strip-light-gray">
<?php  if($user->name_changed ==  0) { ?>
<p class="nopadding">Personal detail can be edited only once.</p>
<div class="vpadding txt-light">If you want to change any of this information later, you have to contact customer support.</div>
  <form cura="customer_info_update" id="customer_info_update">
    <div class="field-wrapper">
      <label class="dynamic-label" for="user_firstname" >First Name</label>
      <input   class="input-short" type="text" value="<?php echo !empty($user) ? $user->firstname : '' ?>" name="user_firstname" id="user_firstname" maxlength="100">
    </div>
    <div class="field-wrapper">
      <label class="dynamic-label" for="user_lastname" >Last Name</label>
      <input  class="input-short" type="text" value="<?php echo !empty($user) ? $user->lastname : '' ?>" name="user_lastname" id="user_lastname" maxlength="100">
    </div>
    <div class="field-wrapper">
      <label  for="user_dob" class="dynamic-label dynamic-label-fold" >Date of Birth</label>
      <div class="flex">
        <?php
	  	$dob = $user->dob;
		$dobp = array(0,0,0);
		
		if(!empty($dob)){
			$dobp = explode('-', $dob);	
			
			$dobp[1] = (int)$dobp[1];
			$dobp[2] = (int)$dobp[2];
		}
	  ?>
        <select name="dob_month" id="dob_month">
          <?php
	  	foreach (DataType::$months as $i => $name) {
			
			echo '<option value="'.$i.'" '.($dobp[1] == $i ? 'selected' : '').' >'.$name.'</option>';
			
		}
	  
	  ?>
        </select>
        <select name="dob_day" id="dob_day">
          <?php
	
	for($i = 1; $i <= 31; $i++){
		echo '<option value="'.$i.'" '.($dobp[2] == $i ? 'selected' : '').' >'.$i.'</option>';
	}
	
	?>
        </select>
        <select name="dob_year" id="dob_year">
          <?php
	$dy = date('Y');
	for($i = ($dy-5); $i >= $dy - 76; $i--){
		echo '<option value="'.$i.'" '.($dobp[0] == $i ? 'selected' : ($dobp[0] == 0 && $dy -25 == $i ? 'selected' : '')).' >'.$i.'</option>';
	}
	
	?>
        </select>
         
      </div>
      <label style="display:none" id="dob-error" class="error" >Date is not valid</label>
    </div>
    <div class="field-wrapper">
      <label class="static-label" >Gender</label>
      <div class="sep"></div>
      <div class=" option-list">
        <div class="flex">
          <div class="col2" style="padding-bottom:0;">
            <label class="custom-checkbox" title="Male">
              <input type="radio" name="gender" class="gender" value="1" <?php echo ($user->gender == 1 && $user->is_trans == 0) ? 'checked' : ''; ?> >
              Male <span class="checkmark"></span> </label>
          </div>
          <div class="col2">
            <label class="custom-checkbox" title="Female">
              <input type="radio" name="gender" class="gender" value="2" <?php echo ($user->gender == 0 && $user->is_trans == 0) ? 'checked' : ''; ?> >
              Female <span class="checkmark"></span> </label>
          </div>
        </div>
        <div class="flex">
          <div class="col2" style="padding-bottom:0;">
            <label class="custom-checkbox"  title="Transgender Male">
              <input type="radio" name="gender" class="gender" value="3" <?php echo ($user->gender == 1 && $user->is_trans == 1) ? 'checked' : ''; ?> >
              T - Male <span class="checkmark"></span> </label>
          </div>
          <div class="col2">
            <label class="custom-checkbox" title="Transgender Female">
              <input type="radio" name="gender" class="gender" value="4" <?php echo ($user->gender == 0 && $user->is_trans == 1) ? 'checked' : ''; ?> >
              T - Female <span class="checkmark"></span> </label>
          </div>
        </div>
        
      </div>
       <label style="display:none" id="gender-error" class="error" >Please select your gender</label>
    </div>
   
    <div class="button-container">
      <div class="center"></div>
      <a class="button btn-next" cura="customer_info_update" >Update</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="customer_info_close" >Cancel</a> </div>
  </form>
  <?php } else { ?>
  <div>You have already updated your personal details. <br>Please contact customer support to make any changes.</div>
  <?php } ?>
</div>
