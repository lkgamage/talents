<div class="inline-editor-panel strip-light-gray">
  <form cura="phone_preferences_update" id="phone_preferences_update">
 
 <div class="columns">
 	<div class="column col2">
    Primary private number
    <div class="field-wrapper">
    <?php
   if(!empty($phones)){
	   
	    echo '<select name="private" class="input_short">';
   	
		foreach ($phones as $p) {
			
			if($p->is_primary == 1) {
				echo '<option selected value="'.$p->id.'">'.$p->phone.'</option>';
			}
			else {
				echo '<option value="'.$p->id.'">'.$p->phone.'</option>';
			}
		}
  
  		echo '</select>';
	   
   }
   
   ?>
   </div>
   <div class="padding txt-light">We'll use this to send you updates and notifications regarding your bookings.</div>
    </div>
    <div class="gap"></div>
    <div class="column col2">
    Primary public number
    <div class="field-wrapper">
    <?php
    if(!empty($phones)){
	   
	    echo '<select name="public" class="input_short">';
		
		echo '<option value="0">-</option>';
   	
		foreach ($phones as $p) {
			
			if($p->is_public == 1 && $p->is_primary == 1) {
				echo '<option selected value="'.$p->id.'">'.$p->phone.'</option>';
			}
			else {
				echo '<option value="'.$p->id.'">'.$p->phone.'</option>';
			}
		}
  
  		echo '</select>';
	   
   }
   else{
		echo "You don't have any public numbers added";   
   }
   
   ?>
   </div>
   <div class="padding txt-light">We'll share this number with the other party when booking is confirmed.</div>
    </div>
 </div>
 <div class="button-container">
      <div class="center">
      <a class="button btn-next" cura="phone_preferences_update" data-id="<?php echo !empty($phone) ? $phone->id : '' ?>"> Update Preference</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="phone_preferences_close" data-id="<?php echo !empty($phone) ? $phone->id : '' ?>" >Cancel</a> </div></div>
  </form>
  </div>