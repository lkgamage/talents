<div class="panel padding">
    <div class="section-header txt-black">Basic Settings</div>
    <div class="sep"></div>
    <div class="vadding">
      <div class="columns column-seperated">
        <div class="column-3 ">
          <div class="info-header">current city</div>
          <div class="info-body">
            <?php
            $location = $user->getLocation();
			  
			if ( !$location->isEmpty( 'address' ) ) {
              echo  $location->address.", ";
            }
			  
            echo $location->city;

            if ( !$location->isEmpty( 'region' ) ) {
              echo ', ' . $location->region;
            }

            echo "<br>";
            echo DataType::$countries[ $location->country ][ 0 ];

            ?>
            <br>
          </div>
          <a class="dash-link" cura="customer_city_edit" data-profile="0">Change City</a>
          <div id="customer_location"  class="inline_load"  ></div>
        </div>
        <div class="column-3 lpadding">
          <div class="info-header">Time Zone</div>
          <div class="info-body">
            <?php

            if ( !$user->isEmpty( 'timezone' ) ) {
              echo DataType::$timezones[ $user->timezone ][ 0 ];
              echo '<br><span class="txt-light">UTC' . DataType::$timezones[ $user->timezone ][ 1 ] . '</span>';
            } else {
              $offset = ( !empty( $_POST[ 'tz' ] ) && is_numeric( $_POST[ 'tz' ] ) ) ? $_POST[ 'tz' ] : $user->timeoffset;

              foreach ( DataType::$timezones as $code => $zone ) {

                if ( $offset == $zone[ 2 ] ) {
                  echo $zone[ 0 ];
                  echo '<br><span class="txt-light">UTC' . $zone[ 1 ] . '</span>';
                  break;
                }
              }
            }


            ?>
          </div>
          <a class="dash-link" cura="customer_tz_edit" data-profile="0">Change Time Zone</a>
          <div id="customer_timezone"  class="inline_load"  ></div>
        </div>
        <div class="column-3 lpadding">
          <div class="info-header">Currency</div>
          <div class="info-body">
            <?php

            $crr = $user->currency;

            if ( empty( $crr ) ) {
              $location = $user->getLocation();
              $crr = DataType::$countries[ $location->country ][ 2 ];
            }

            echo DataType::$currencies[ $crr ];
            echo '<br><span class="txt-light">(' . $crr . ')</span>';


            ?>
          </div>
          <a class="dash-link" cura="customer_currency_edit" data-profile="0">Change Currency</a>
          <div id="customer_currency" class="inline_load" ></div>
        </div>
      </div>
      <div id="customer_setting" class="hpadding"></div>
    </div>
  </div>