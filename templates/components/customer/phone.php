<div class="inline-editor-panel strip-light-gray">
  <form cura="phone_update" id="phone_update">
    <input type="hidden" value="<?php echo !empty($phone) ? $phone->id : '' ?>" name="phone_id" id="phone_id" >
    <div class="field-wrapper">
      <label class="dynamic-label" for="phone_phone" >Phone Number</label>
      <input  class="input-short" type="text" value="<?php echo !empty($phone) ? $phone->phone : '' ?>" name="phone_phone" id="phone_phone" maxlength="14" <?php echo (!empty($phone) && $phone->is_verified == 1) ? 'readonly="1"' : '' ?>>
    </div>
    <div class="button-container">
      <?php

      if ( !empty( $phone ) && $phone->is_primary == 0 ) {
        echo '<a href="#" cura="phone_delete" data-id="' . $phone->id . '">Remove Phone</a>';
      }

      ?> <div class="center"></div>
      <div class="button-wrapper">
       
        <a class="button btn-next" cura="phone_update" data-id="<?php echo !empty($phone) ? $phone->id : '' ?>"> <?php echo empty($phone) ? 'Add Phone' : 'Update' ?></a>
        <div class="gap"></div>
        <a class="button button-alt" cura="phone_close" data-id="<?php echo !empty($phone) ? $phone->id : '' ?>" >Cancel</a> </div>
    </div>
  </form>
</div>
