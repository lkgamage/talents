<div class="inline-editor-panel strip-light-gray">
<form cura="customer_setting_update" id="customer_setting_update">
	 <input type="hidden" name="country" id="country" value="<?php echo Session::getCountry() ?>">
<input type="hidden" name="profile" value="<?php echo (!empty($_POST['profile'])) ? 1 : 0 ?>">
    <div>Update current city</div>
    <div class="sep"></div>
    <div class="columns">
    <div class="resizable">
   

     <div class="field-wrapper">
            <label class="dynamic-label" for="user_city" >City</label>
            <input type="text" value="" name="user_city" class="citypick" id="user_city" maxlength="100" placeholder=""  >
          </div>
    <div class="button-container">
      <div class="center"></div>
		<div class="button-wrapper">
		 <a class="button btn-next" cura="customer_setting_update" >Update</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="inline_load_cancel" >Cancel</a>
		</div>
      </div>

</form>
</div>