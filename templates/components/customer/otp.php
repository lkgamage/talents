<form id="otp_form" cura="<?php echo (!empty($cura) ? $cura : '') ?>" >
  <div class="sep"></div>
	<input type="hidden" name="code_id" value="<?php echo $code->id; ?>">
	<div>We have sent you an OTP to your <?php echo $type == 'e' ? 'email address' : 'mobile number'; ?>.</div>
	<div>Please enter code.</div>
     <div class="field-wrapper">      
      <input  class="input-short" type="text" value="" name="code" id="code" maxlength="6" >
    </div>
  <div class="sep"></div>
</form>