<?php

$img_event = Util::mapURL('images/img-party.png');

if(!empty($data)){
		  
	  foreach ($data as $b) {
		  
		  $type = '';
		  
		  if(!empty($b['event_id'])){
			$name = $b['event_name'];
			$type = 'Event';  
			$img = $img_event;
		  }
		  else {
				if(!empty($b['booking_id'])){						
					$type = "Booking";						
				}
				
				if(!empty($b['agency_id'])){
					$img = fixImage($b['agency_image'], NULL, 1);
					$name = $b['agency_name'];
					if(empty($type)){
						$type = 'Agency';	
					}
				}
				elseif(!empty($b['talent_id'])){
					$img = fixImage($b['talent_image'], $b['talent_gender']);
					$name = $b['talent_name'];
					if(empty($type)){
						$type = 'Talent';	
					}	
				}
				else{
					$img = fixImage($b['customer_image'], 1);
					$name = $b['customer_name'];
					if(empty($type)){
						$type = 'Customer';	
					}
				}
		  }
	  
		  echo '<div class="box-row linked flex" cura="chat_open" data-board="'.$b['board_id'].'">
		  <div class="box-cell"><img src="'.$img.'"></div>
		  <div class="box-cell cell-width-50"><div class="txt-blue txt-m">'.$name.'</div><div class="txt-light-gray txt-s">'.$type.'</div></div>
		  <div class="box-cell resizable"></div>
		  <div class="box-cell cell-width-30 right"><div class="txt-orange">'.(!empty($b['unseen']) ? $b['unseen'].' new messages' : '').'</div><div class="txt-light-gray txt-s">'.Util::ToDate($b['lastsend'], true).'</div></div>
		</div> ';
	
	  }
	  
	 // Util::debug($data);
	 
	  
  }


?>