<form cura="currency_update" id="currency_update" >
<div class="field-wrapper">
            <label class="dynamic-label dynamic-label-fold" for="currency" >Currency</label>
            <select name="currency">
            <?php
			
			$crr = Session::currency();

			foreach (DataType::$currencies as $code => $name){
				
				if(!empty($crr) && $crr == $code){
					echo '<option selected value="'.$code.'">'.$name.' ('.DataType::$csymbols[$code].')</option>';
				}
				else{
					echo '<option value="'.$code.'">'.$name.' ('.DataType::$csymbols[$code].')</option>';
				}
				
			}		
			
			?>            
            </select>
          </div>
</form>