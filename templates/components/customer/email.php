<div class="inline-editor-panel strip-light-gray">
  <form cura="email_update" id="email_update">
    <input type="hidden" value="<?php echo !empty($email) ? $email->id : '' ?>" name="email_id" id="email_id" >
    
    <div class="field-wrapper">
      <label class="dynamic-label" for="email_email" >Email Address</label>
      <input  class="input-short" type="text" value="<?php echo !empty($email) ? $email->email : '' ?>" name="email_email" id="email_email" maxlength="100" <?php echo (!empty($email) && $email->is_verified == 1) ? 'readonly="1"' : '' ?>>
    </div>

    
    <div class="button-container">
    <?php 
    	
		if(!empty($email) && $email->is_primary == 0) { 
			echo '<a href="#" cura="email_delete" data-id="'.$email->id.'">Remove email</a>';
		}
	
	?>
      <div class="center"></div>
      <a class="button btn-next" cura="email_update" data-id="<?php echo !empty($email) ? $email->id : '' ?>"> <?php echo empty($email) ? 'Add Email' : 'Update' ?></a>
      <?php if(empty($required)) { ?>
      <div class="gap"></div>
      <a class="button button-alt" cura="email_close" data-id="<?php echo !empty($email) ? $email->id : '' ?>" >Cancel</a> 
      <?php } ?></div>
  </form>
</div>
