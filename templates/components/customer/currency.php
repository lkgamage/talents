<div class="inline-editor-panel strip-light-gray">
<form cura="customer_setting_update" id="customer_setting_update">
<input type="hidden" name="profile" value="<?php echo (!empty($_POST['profile'])) ? 1 : 0 ?>">
<input type="hidden" name="country" value="" class="_country">
   
    <div>Update currency</div>
    <div class="sep"></div>
    <div class="columns">
    <div class="resizable">
   

     <div class="field-wrapper">
            <label class="dynamic-label dynamic-label-fold" for="currency" >Currency</label>
            <select name="currency">
            <?php
			
			$crr = $object->currency;
			
			if(empty($crr)){
				$location = $object->getLocation();
				$crr = DataType::$countries[$location->country][2];	
			}
			
			foreach (DataType::$currencies as $code => $name){
				
				if(!empty($crr) && $crr == $code){
					echo '<option selected value="'.$code.'">'.$name.' ('.DataType::$csymbols[$code].')</option>';
				}
				else{
					echo '<option value="'.$code.'">'.$name.' ('.DataType::$csymbols[$code].')</option>';
				}
				
			}		
			
			?>            
            </select>
          </div>
    <div class="button-container">
      <div class="center"></div>
		<div class="button-wrapper">
		<a class="button btn-next" cura="customer_setting_update" >Update</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="inline_load_cancel" >Cancel</a> 
		</div>
      </div>

</form>
</div>