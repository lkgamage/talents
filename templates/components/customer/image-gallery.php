<div id="gallery_stage">
<?php

if($buffer->getTotal() > 0) {
	
	echo $buffer->getPage();

}
else {
	echo '<div class="padding center">No images found</div>';
}
?>
</div>
<div class="button-container">
  <div class="center"> <a  class="txt-blue" href="javascript:void(0)" id="more_images" data-label="More Images" > More Images </a> </div>
</div>
<?php
echo $buffer->manualPaginate('gallery_stage','more_images');
?>

