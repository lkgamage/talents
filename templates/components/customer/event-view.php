<div>
  <div class="txt-m"><?php echo $event->name; ?></div>
  <div class="txt-light"><?php echo DataType::$EVENT_CATEGORIES[$event->event_type][0]; ?></div>
  <div class="sep"></div>
  <?php 
  
  			$begins = Util::ToDate($event->begins, 1);
			$ends =  Util::ToDate($event->ends, 1);
		
			if(!$event->isEmpty('venue')) { 
			 echo '<div class="txt-m">'.$event->venue.'</div><div class="sep"></div>';
			}
			
			if(empty($ends)){
				echo '<div>'.$begins[0].'  <span class="txt-light">from</span> '.$begins[1].'</div>';
			}
			else{
				if($ends[0] == $begins[0]){
					echo '<div>'.$begins[0].'   '.$begins[1].'  -  '.$ends[1].'</div>';
				}
				else{
					echo '<div><span class="txt-light">from</span> '.$begins[0].' '.$begins[1].' <span class="txt-light">to</span> '.$ends[0].'  '.$ends[1].'</div>';	
				}
				
			}
			
			
			
			if(!$event->isEmpty('description')) { 
			echo '<div class="sep"></div>';
				echo '<div class="max-600 txt-light vpadding">'.nl2br($event->description).'</div>';
				
			}
				
		?>
  <div class="sep"></div>
  <a class="dash-link" cura="booking_event_edit" data-target="<?php echo (!empty($_POST['target'])) ?  $_POST['target'] : 'booking_event'; ?>" data-id="<?php echo $event->id; ?>">Edit event info</a>
  <div class="sep"></div>
</div>
