<?php
// packages

$list = $profile->getPackageDetail(true);
$pacakgelist = [];

if(!empty($list)){
	foreach ($list as $p){
		$pacakgelist[$p['skill']['name']][] = $p;
	}
}

foreach ($pacakgelist as $skillname => $packages ){
    ?>
	  <h2 class="package-headers"><?php echo $skillname; ?> </h2>
    <div class="profile-packages ">
      <?php 
	  
	  if(!empty($packages)) { 
	  
	  $count = 0;
	  
	   foreach ($packages as $pkg) {
		   
		   if($pkg['package']['active'] == 0 || $pkg['package']['is_private'] == 1) {
			   continue;
		   }
	  
	  		$count++;
	  ?>
      <div class="profile-package-wraper fade">
        <div class="profile-package">
          <div class="profile-package-name vbox-header"><?php echo ucwords($pkg['package']['name'])  ?>
            <div class="profile-package-cat"><?php echo $pkg['skill']['name']  ?></div>
			  <div class="profile-package-price "><?php echo currency($pkg['package']['fee']); ?></div>
          </div>
          <div class="profile-package-desc ">
			<?php 
	
	if(strlen($pkg['package']['description']) > 200) {
		
		echo nl2br(substr($pkg['package']['description'],0,200));
		
		echo '<span class="hidden" id="desc_'.$pkg['package']['id'].'">'.nl2br(substr($pkg['package']['description'],201)).'</span> <a cura="expand_text" data-id="desc_'.$pkg['package']['id'].'">view more...</a>';
	}
	else{
		echo nl2br($pkg['package']['description']);
	}
	
	 ?>
			</div>
          
          <div class="profile-package-action "> <a class="button" cura="booking_package" data-id="<?php echo $pkg['package']['id']; ?>">BOOK</a> </div>
        </div>
      </div>
      <?php 
	  	}
	  }
	   ?>
    </div>
<?php } ?>


<?php
//Util::debug($pacakgelist);
?>