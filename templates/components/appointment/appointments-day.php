<div>
  <?php
$is_past = time() > strtotime($date)+86400;
$profile = Session::getProfile();

if(!empty($appointments)) {
	//print_r($appointments);
	
	foreach($appointments as $i => $a){
		
		$times = Util::calcAppointmentTime ($a['begins'], $a['duration'], $a['rest_time']);
		
		
		echo '<div class="'.(!empty($block) ? 'appointment-block' : 'appointment-row').' '.($a['confirmed'] == 1 ? 'appointment-confirmed' : 'appointment-scheduled').'" ';
		
		
		
		if(empty($is_agency)){
			//echo 'cura="appointment_brief"';
			echo 'cura="context" data-action="appointment_options"';
			echo ' data-id="'.$a['id'].'">';
		}
		elseif($a['agency_id'] == $request->agency_id){
			echo 'cura="agency_talent" data-action="edit_appointment" ';
			echo ' data-id="'.$request->id.'" data-appointment="'.$a['id'].'" >';
		}
		else {
			echo '">';	
		}
		
		
		
		echo '<div class="appointment-date"><div>'.$times['begin']['time'].'</div></div>';
		
		echo ' <div class="appointment-desc"><div>';
		
		if(!empty($block)){
		
			echo strlen($a['description']) > 40 ? substr($a['description'],0,40).'...' : $a['description'];
		
		}
		else{
			echo $a['description'];
		}
		
		if($a['confirmed'] == 1){
			echo '<div><label>Confirmed</label>';
		}
		else{
			echo '<div><label>Scheduled</label>';
			
			if(!empty($a['expired'])) {
				
				if(!empty($block)){
					echo '<div class="appointment-exp">Exp: '.str_replace(', '.date('Y'), '', Util::Todate($a['expired'])).'</div>';
				}
				else {
					echo '<div class="appointment-exp">Expires '.Util::Todate($a['expired']).'</div>';
				}
			}
		}
		
		if(!empty($a['agency_id'])  ){
			
			/*if(!empty($request) && $request->agency_id == $a['agency_id']) {
				echo '<label class="agency">Agency</label>';
			}*/
			if(!empty($profile) && $profile['type'] == 'a' &&  $profile['id'] == $a['agency_id']) {
				echo '<label class="agency">Agency</label>';
			}
			elseif(empty($is_agency)){
				echo '<label class="agency">'.$a['with_name'].'</label>';
			}
		}
		
		// closing bottom row
		echo '</div>';
		
		echo '</div></div>'; // desc
		
		if(empty($block) && empty($is_agency)) {
		//echo '<div class="appointment-notes"><div><a class="icon-note"></a></div></div>';
		}
	
	echo '<div class="appointment-duration"><div>'.$times['duration'].'</div></div>';
		
		echo '</div>';
		
		
		
		if($a['rest_time'] > 0){
			
			echo '<div class="'.(!empty($block) ? 'appointment-block' : 'appointment-row').' rest-time '.($a['confirmed'] == 1 ? '' : 'appointment-scheduled').'" cura="appointment_brief" data-id="'.$a['id'].'">';
		
		echo '<div class="appointment-date"><div>'.$times['interval']['begin'].'</div></div>';
		
		echo ' <div class="appointment-desc"><div>Rest time</div></div>'; // desc
		
		if(empty($block) && empty($is_agency)) {
			echo '<div class="appointment-notes"><div></div></div>';
		}
	
	echo '<div class="appointment-duration"><div>'.$times['interval']['duration'].'</div></div>';
		
		echo '</div>';	
			
		
		}

		// show interval
		if(count($appointments) > $i+1 && !$is_past){
			$int = Util::interval($a['begins'], $a['duration'] + $a['rest_time'],$appointments[$i+1]['begins']); 
			
			if(!empty($int)){
				echo '<div class="'.(!empty($block) ? 'appointment-block' : 'appointment-row').' available-time"> '.$int[0].' - '.$int[1].' ('.Util::ToTime($int[2]).') Available. ';
				
				if($int[2] >= 30  ) {
					
					if(!empty($is_agency)) {
						// agency creating appointmnet
						echo '<br><a href="javascript:void(0)" cura="agency_talent" data-date="'.Util::Todate($date).'" data-time="'.$int[0].'" data-action="new_appointment"  data-id="'.$request->id.'">Add an appointment</a>';
					}
					else {
						// talent creating appointment
						echo '<br><a href="javascript:void(0)" cura="appointment_options"  data-action="edit" data-date="'.Util::Todate($date).'" data-time="'.$int[0].'">Add an appointment</a>';
					}
				}
				echo '</div>';
			}
		}
		
	}
}
else{
	echo '<div class="'.(!empty($block) ? 'appointment-block' : 'appointment-row').' available-time">No appointments. ';
	
	if(!$is_past ) {
		
		if(!empty($is_agency)) {
			echo '<br><a cura="agency_talent" href="javascript:void(0)" data-date="'.Util::Todate($date).'" data-action="new_appointment" data-id="'.$request->id.'">Add an appointment</a>';
		
		}
		else {
			echo '<br><a cura="appointment_options"  data-action="edit" href="javascript:void(0)" data-date="'.Util::Todate($date).'" >Add an appointment</a>';
		}
	}
	
	echo '</div>';

}
 



?>
</div>
<?php
//Util::debug($appointments);
?>
