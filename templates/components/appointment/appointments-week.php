<div class="appointment-week columns column-seperated">
  <?php
  
  	// format appointments
	$items = array();
	
	if(!empty($appointments)){
		
		
		foreach ($appointments as $a){
			
			$ad = explode(' ',$a['begins']);
			$ad[0] = Util::ToZone($ad[0]);
			
			if(!isset($items[$ad[0]])){
				$items[$ad[0]] = array();	
			}
			
			$items[$ad[0]][] = $a;
		}
		
	}
	
  
 	 $dt = strtotime($begin_date);
	 $dx = $dt;
	 $t = time();
		
	for($i = 0; $i < 7; $i++){
		
		$dx = $dt + (60*60*24*$i);
		$date = date("Y-m-d", $dx);
		
		echo '<div class="appointment-day">';
    	echo '<div class="appointment-day-name">'.DataType::$days[$i].'<span>'.date('M j',$dt + (60*60*24*$i) ).'</span></div>';
    
		echo '<div class="appointment-day-body">';
		
		if(!empty($items[$date])) {
			
     		component('/appointment/appointments-day', array(
			'block' => true, 
			'appointments' => $items[$date],
			'date' => $date
			 ));
		}
		else{
			echo '<div class="appointment-row available-time">No appointments';
			
			if($dx >= $t) {
			echo '<br><a cura="appointment_options" data-action="edit" href="javascript:void(0)" data-date="'.Util::ToDate($date).'" >Add</a>';
			}
			else{
				echo '<br>&nbsp;';
			}
			
			echo '</div>';	
		}
    
		echo '</div>';
  		echo '</div>';
		
	
		}
		
		?>
</div>
<?php
//echo $begin_date.' '. $end_date;
//Util::debug($appointments);
?>