<?php
	$begins = array('','');
	
	if(!empty($appointment)){
		$begins = Util::ToDate($appointment->begins, 1);
		$times = $appointment->getTimes();
	}
	elseif(!empty($_POST['date'])){
		$begins[0] = $_POST['date'];
		
		if(!empty($_POST['time'])){
			$begins[1] = $_POST['time'];
		}
	}
	
?>
<div class="panel">
  <div class="padding section">
    <div class="dash-sub-header txt-orange"><?php echo !empty($appointment) ? 'Update' : 'Create' ?> Appointment</div>
    <form id="appointment_update" cura="appointment_update">
      <input type="hidden" value="<?php echo !empty($appointment) ? $appointment->id : '' ?>" name="appointment_id" id="appointment_id" >
      <div class="field-wrapper">
        <label class="dynamic-label" for="appointment_description" >Title</label>
        <input type="text" value="<?php echo !empty($appointment) ? $appointment->description : '' ?>" name="appointment_description" id="appointment_description" maxlength="400">
      </div>
      <div class="field-wrapper">
        <label class="dynamic-label" for="appointment_note" >Note</label>
        <textarea class="input-notes" name="appointment_note" id="appointment_note" maxlength="4000"><?php echo !empty($appointment) ? $appointment->note : '' ?></textarea>
      </div>
      <?php if(empty($appointment) || !$appointment->isBooking()){  ?>
      <div class="columns" >
        <div class="column col2" >
          <div class="field-wrapper">
            <div class="options">
              <div>
                <label class="custom-checkbox">
                  <input type="checkbox"  name="appointment_all_day" id="appointment_all_day" value="1" <?php echo (!empty($appointment) && $appointment->all_day == 1) ? 'checked' : '' ?> cura="appointment_full_day" data-type="change,load" >
                  Full day appointment<span class="checkmark"></span> </label>
              </div>
            </div>
          </div>
        </div>
        <div class="column col2" >
          <div class="field-wrapper">
            <div class="options">
              <div>
                <label class="custom-checkbox">
                  <input type="checkbox"  name="appointment_time_off" id="appointment_time_off_1" value="1" <?php echo (!empty($appointment) && $appointment->time_off == 1) ? 'checked' : '' ?> >
                  Personal time off<span class="checkmark"></span> </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="field-wrapper">
        <div class="flex">
          <div class="col2">
            <label>Date</label>
            <input type="text" class="datepick " id="appointment_begins_date" name="appointment_begins_date" value="<?php echo $begins[0];   ?>" data-value="<?php echo $begins[0];   ?>">
          </div>
          <div class="col2 non-ful-day">
            <label>Time</label>
            <input type="text" class="timepick " id="appointment_begins_time" name="appointment_begins_time"   value="<?php echo $begins[1];   ?>" data-value="<?php echo $begins[1];   ?>">
          </div>
        </div>
      </div>
      <div class="field-wrapper non-ful-day">
        <label class="dynamic-label" for="appointment_duration" >Duration</label>
        <input type="text" class="input-short" value="<?php echo !empty($appointment) ? $appointment->duration : '' ?>" name="appointment_duration" id="appointment_duration"  maxlength="10" cura="convert_time" data-type="blur">
        <div class="anote">Ex: 30m, 1h</div>
      </div>
      <?php } else { ?>
      <div class="sep"></div>
      <div class="columns">
        <div class="column-3">
          <div class="info-header">Begins</div>
          <div class="info-body"> <?php echo $times['begin']['date']; ?>
            <div class="txt-blue"><?php echo $times['begin']['time']; ?></div>
          </div>
        </div>
        <div class="column-3">
          <div class="info-header">Ends</div>
          <div class="info-body"> <?php echo $times['end']['date']; ?>
            <div class="txt-blue"><?php echo $times['end']['time']; ?></div>
          </div>
        </div>
        <div class="column-3">
          <div class="info-header">Duration</div>
          <div class="info-body">
            <div class="txt-blue"><?php echo $times['duration']; ?></div>
          </div>
        </div>
      </div>
      <?php  } ?>
      <div class="field-wrapper non-ful-day">
        <label class="dynamic-label" for="appointment_rest_time" >Free time before next appointment</label>
        <input type="text" class="input-short"   value="<?php echo !empty($appointment) ? $appointment->rest_time : '' ?>" name="appointment_rest_time" id="appointment_rest_time"  maxlength="10" cura="convert_time" data-type="blur">
        <div class="anote">Ex: 30m, 1h</div>
      </div>
      
      <div class="button-container">
        <div class="center"></div>
        <?php
		
		if(empty($appointment)){
			echo '<a class="button btn-next" cura="appointment_update" data-confirm="1" >Confirm</a>';
			echo ' <div class="gap"></div>';
			echo '<a class="button btn-next" cura="appointment_update" >Schedule</a>';
		}
		else{
			echo '<a class="button btn-next" cura="appointment_update" >Update</a>';
		}
		
		?>
        
        
        <div class="gap"></div>
        <a class="button button-alt" cura="history" >Cancel</a> </div>
    </form>
    <div class="sep"></div>
    <?php if(empty($appointment) || !$appointment->isBooking()){  ?>
    <div class="txt-light txt-s">* When you select <span class="txt-bold">Personal time off</span> option, It will be shown as<span class="txt-bold"> NOT AVAILBALE </span>in public.</div>
    <?php } ?>
  </div>
</div>
