<?php
$begins = array( '', '' );

if ( !empty( $appointment ) ) {
  $begins = Util::ToDate( $appointment->begins, 1 );
} elseif ( !empty( $_POST[ 'date' ] ) ) {
  $begins[ 0 ] = $_POST[ 'date' ];

  if ( !empty( $_POST[ 'time' ] ) ) {
    $begins[ 1 ] = $_POST[ 'time' ];
  }
}

$times = $appointment->getTimes();

$info = $appointment->getinfo();
$profile = Session::getProfile();

?>
<div class="panel">
  <div class="dash-header-wrapper section padding">
    <div class="resizable">
      <div class="dash-sub-header txt-orange ">Appointment</div>
    </div>
    <input type="hidden" name="view_id" value="">
    <span class="middle left-content">
    <div class="center">
       <?php

          if ( $appointment->confirmed == 1 ) {
            echo '<label class="status-bubble status-confirmed">CONFIRMED</label>';
          } else {
            echo '<label class="status-bubble status-pending">SCHEDULED</label>';
          }

          ?>
   
    </div>
    </span> </div>
  <div class="padding">
    <div class="columns hpadding">
      <div class="resizable">
        <div class="dash-sub-header"><?php echo ucfirst($appointment->description);?></div>
      </div>
      <div class="middle">
        <div class="center">
         
        </div>
      </div>
    </div>
  </div>
  <?php

  if ( $appointment->confirmed != 1 && $appointment->isOwner() && !$appointment->isPast() ) {

    echo '<div class="booking-status-message section padding bg-yellow flex" ><div class="resizable">';

    if ( !$appointment->isEmpty( 'expired' ) ) {
      echo 'If not confirmed on or before ' . Util::ToDate( $appointment->expired, true ) . ', appointment will be canceled.';
    } else {

      if ( $appointment->time_off == 1 ) {
        echo 'You have marked this as personal time off, but haven\'t confirmed yet.<br>This time slot is still available for booking.';
      } else {
        echo 'This time slot is still available for booking untill you confirm appointment';
      }
    }

    echo '</div><div>';
    if ( $appointment->confirmed != 1 && $appointment->isOwner() && !$appointment->isPast() ) {


      echo '<a cura="appointment_confirm" data-id="' . $appointment->id . '" class="button button-act">CONFIRM</a>';
    }

    echo '</div></div>';
  }

  ?>
  <div class="padding ">
    
    <?php if($appointment->all_day == 1) { ?>
    <div class="info-body"> <?php echo $times['begin']['date']; ?>
      <div class="txt-blue">This is a full day appointment</div>
    </div>
    <?php } else { ?>
    <div class="columns">
      <div class="column-3">
        <div class="block-header">Begin</div>
        <div class="info-body"> <?php echo $times['begin']['date']; ?>
          <div class="txt-blue"><?php echo $times['begin']['time']; ?></div>
        </div>
      </div>
      <div class="column-3">
        <div class="block-header">Ends</div>
        <div class="info-body"> <?php echo $times['end']['date']; ?>
          <div class="txt-blue"><?php echo $times['end']['time']; ?></div>
        </div>
      </div>
      <div class="column-3">
        <div class="block-header">Duration</div>
        <div class="info-body">&nbsp;
          <div class="txt-blue"><?php echo $times['duration']; ?></div>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <?php if(!empty($times['interval'])) { ?>
  <div class="dash-sep"></div>
  <div class="padding ">
    <div class="hpadding">
      <div class="block-header">Rest Time</div>
    </div>
    <div class="columns">
      <div class="column-3">
        <div class="info-header">Begins</div>
        <div class="info-body"> <?php echo $times['interval']['begin']; ?> </div>
      </div>
      <div class="column-3">
        <div class="info-header">Ends</div>
        <div class="info-body"> <?php echo $times['interval']['end']; ?> </div>
      </div>
      <div class="column-3">
        <div class="info-header">Duration</div>
        <div class="info-body">
          <div class="txt-blue"><?php echo $times['interval']['duration']; ?></div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php if(!$appointment->isEmpty('note')) { ?>
  <div class="section">
    <div class="block-header">NOTES</div>
    <div class="max-600" ><?php echo nl2br($appointment->note); ?></div>
  </div>
  <div class="sep"></div>
  <?php } ?>
  <div class="sep"></div>
  <?php
  if ( !empty( $info[ 'event' ][ 'id' ] ) ) {
    $begintime = Util::ToDate( $info[ 'event' ][ 'begins' ], 1 );
    $endtime = ( !empty( $info[ 'event' ][ 'ends' ] ) ) ? Util::ToDate( $info[ 'event' ][ 'ends' ], 1 ) : NULL;
    ?>
  <div class="section">
    <div class="block-header">Event</div>
    <div  data-id="<?php echo $info['event']['id']; ?>" class="dash-info-row nolink">
      <div class="dash-info-top">
        <div class="dash-info-left">
          <div class="dash-info-name txt-blue"><?php echo $info['event']['name']; ?></div>
          <div class="dash-info-place"><?php echo $info['event']['type_name']; ?></div>
          <div class="sep"></div>
          <div class="dash-info-desc">
            <?php

            if ( !empty( $info[ 'event' ][ 'venue' ] ) ) {
              echo $info[ 'event' ][ 'venue' ];
              echo '</div><div class="dash-info-place">';
            }


            echo Util::MakeString( $info[ 'event' ][ 'city' ], $info[ 'event' ][ 'region' ], DataType::$countries[ $info[ 'event' ][ 'country' ] ][ 0 ] );

            ?>
          </div>
        </div>
        <div class="dash-info-right">
          <div class="dash-info-date"><?php echo $begintime[0]; ?></div>
          <div class="dash-info-time"><?php echo $begintime[1]; ?></div>
          <?php
          if ( !empty( $endtime ) ) {

            if ( $begintime[ 0 ] != $endtime[ 0 ] ) {

              echo '<div class="dash-info-date">- ' . $endtime[ 0 ] . '</div>';
              echo '<div class="dash-info-time">' . $endtime[ 1 ] . '</div>';
            } else {
              echo '<div class="dash-info-time">- ' . $endtime[ 1 ] . '</div>';
            }
          }

          ?>
        </div>
      </div>
      <div class="dash-info-middle ">
        <input type="hidden" name="event_id" value="1">
      </div>
    </div>
  </div>
  <?php } ?>
  <?php
  if ( !empty( $info[ 'booking' ][ 'id' ] ) ) {

    $time = Util::ToDate( $info[ 'booking' ][ 'session_start' ], 1 );

    ?>
  <div class="section">
    <div class="block-header">Booking</div>
    <div class="dash-info-row nolink">
      <div class="dash-info-top">
        <div class="dash-info-left">
          <div class="dash-info-name txt-blue"><?php echo $info['booking']['package_name']; ?></div>
          <div class="dash-info-place"> <span><?php echo $info['booking']['skill_name']; ?></span> <span><?php echo Util::ToTime($info['booking']['package_timespan']); ?></span> <span><?php echo currency($info['booking']['total']); ?></span> </div>
          <div class="sep"></div>
          <div class="dash-info-desc">
            <?php

            if ( !empty( $info[ 'booking' ][ 'venue' ] ) ) {
              echo $info[ 'booking' ][ 'venue' ];
              echo '</div><div class="dash-info-place">';
            }


            echo Util::MakeString( $info[ 'booking' ][ 'city' ], $info[ 'booking' ][ 'region' ], DataType::$countries[ $info[ 'booking' ][ 'country' ] ][ 0 ] );

            ?>
          </div>
        </div>
        <div class="dash-info-right">
          <div class="dash-info-date"><?php echo $time[0]; ?></div>
          <div class="dash-info-time"><?php echo $time[1]; ?></div>
          <div class="sep"></div>
          <div class="txt-bold">
            <?php
            echo DataType::$BOOKING_STATUS[ $info[ 'booking' ][ 'status' ] ];
            ?>
          </div>
        </div>
      </div>
      <div class="sep"></div>
    </div>
  </div>
  <?php } ?>
  <?php if(!empty($info['agency']['id']) && !empty($profile) && $profile['type'] == 't'  ) { ?>
  <div class="section">
    <div class="block-header">Agency</div>
    <div class="dash-info-row nolink">
      <div class="dash-info-top">
        <div class="dash-info-pic"> <img src="<?php
  
  	if(!empty($info['agency']['i400'])){
		echo $info['agency']['i400'];
	}
	else{
		echo Util::mapURL('/images/placeholder-business.png');;
	}
  
  ?>"> </div>
        <div class="dash-info-left middle">
          <div class="dash-info-name txt-blue"><?php echo $info['agency']['name'];  ?></div>
          <div class="dash-info-place"><?php echo $info['agency']['type_name'];  ?></div>
        </div>
        <div class="dash-info-right"> </div>
      </div>
      <div class="sep"></div>
    </div>
  </div>
  <?php } ?>
  <?php
  // If agency created appointment with a talent, show talent profile link
  if ( !empty( $info[ 'agency' ][ 'id' ] ) && !empty( $info[ 'talent' ][ 'id' ] ) && !empty( $profile ) && $profile[ 'type' ] == 'a' ) {
    ?>
  <div class="section">
    <div class="block-header">Talent</div>
    <div  class="dash-info-row nolink">
      <div class="dash-info-top">
        <div class="dash-info-pic"> <img src="<?php
		
		if(!empty($info['talent']['i400'])){
				echo $info['talent']['i400'];
		}
		elseif($item['talent']['gender'] == 0){					
			$img = Util::mapURL('/images/placeholder-girl.png');
		}
		else{
			$img = Util::mapURL('/images/placeholder-boy.png');	
		}
  
  	
  
  ?>"> </div>
        <div class="dash-info-left middle">
          <div class="dash-info-name txt-blue"><?php echo $info['talent']['name'];  ?></div>
          <div class="dash-info-place"><?php echo !empty($info['booking']['skill_name']) ? $info['booking']['skill_name'] : '';  ?></div>
        </div>
        <div class="dash-info-right"> </div>
      </div>
      <div class="sep"></div>
    </div>
  </div>
  <?php } ?>
  <div class="section">
    <div class="compact-row">
      <div>Created:</div>
      <div><?php echo Util::ToDate($appointment->created, true);  ?></div>
    </div>
    <div class="compact-row">
      <div>Last updated:</div>
      <div><?php echo Util::ToDate($appointment->modified, true);  ?></div>
    </div>
  </div>
  <div class="sep"></div>
  <div class="padding section">
    <div class="button-container">
      <?php

      if ( $appointment->canDelete() && !$appointment->isPast() ) {
        echo '<a class="" href="javascript:void(0)" cura="appointment_delete" data-id="' . $appointment->id . '">Cancel Appointment</a>';
      }
      ?>
      <div class="center"></div>
      <a class="button " cura="appointment_options" data-action="edit" data-id="<?php echo $appointment->id; ?>"> EDIT </a>
      <div class="gap"></div>
      <a class="button button-alt "  cura="history" >CLOSE</a> </div>
    <div class="sep"></div>
  </div>
</div>
<?php

//Util::debug( $info );
?>
