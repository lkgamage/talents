<div class="appointment-month ">

<?php

$today = Util::ToZone(date("Y-m-d"));


echo '<div class="columns column-seperated day-names">';

for($i = 0; $i < 7; $i++){

	echo '<div class="appointment-day">';
    echo '<div class="appointment-day-name">'.DataType::$days[$i].'</div>';
	echo '</div>';
	
}

echo '</div>';
echo '<div class="columns column-seperated wrap">';
foreach ($appointments as $d => $a){
	
	$userdate = Util::ToDate($d);
	
	echo '<div class="appointment-day calendar-day ">';
	echo '<div class="calendar-day-name '.($today == $d ? 'today' : '').'">'.date('M j', strtotime($d)).'</div>';
	
	
	if(!empty($a)) {
		
		echo '<div class="calendar-body" cura="appointment_list" data-date="'.$userdate.'">';
		
		if(!empty($a['confirmed'])) {
			echo '<div class="bg-orange flex"><div class="resizable desktop-only">Confirmed</div><div>'.$a['confirmed'].'</div></div><div  class="calendar-time">'.Util::ToTime($a['confirmed_time']).'</div>';
		}
		
		if(!empty($a['shadow'])) {
			echo '<div class="bg-gray flex"><div class="resizable desktop-only">Scheduled</div><div>'.$a['shadow'].'</div></div><div class="calendar-time">'.Util::ToTime($a['shadow_time']).'</div>';
		}
			
		echo '</div>'; // body
		
	}
	else{
		
		echo '<div class="calendar-body" cura="appointment_edit" data-date="'.$userdate.'"  ></div>';
	}


	echo '</div>'; // day
	
}
echo '</div>';

//Util::debug($appointments);
?>
</div>