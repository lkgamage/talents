<?php
		  
	
	
if(!empty($data)) {
		
	$skills = App::getSkillArray();	
		  
	foreach ($data as $info) {
			  
			  ?>

<!------------>

<div  class="dash-info-row with-picture " id="talent_<?php echo $info['request_id']; ?>">
  <div class="dash-info-top">
    <div class="dash-info-pic"> <img src="<?php 
			
			if(!empty($info['i400'])){
				echo $info['i400'];
			}
			elseif($info['gender'] == 0){
				
				echo Util::mapURL('/images/placeholder-girl.png');
			}
			else{
				echo Util::mapURL('/images/placeholder-boy.png');	
			}
			 ?>"> </div>
    <div class="dash-info-left">
      <div class="dash-info-name"><?php echo $info['name']; ?></div>
      <div class="dash-info-place">
        <?php 
			 
			  echo util::makeString($info['city'], $info['region'],DataType::$countries[$info['country']][0] );
			  
			   ?>
      </div>
      <div class=" txt-light">
        <?php
				
				$t1 = !empty($info['t1']) ? $skills[$info['t1']]['name'] : '';
				$t2 = !empty($info['t2']) ? $skills[$info['t2']]['name'] : '';
				$t3 = !empty($info['t3']) ? $skills[$info['t3']]['name'] : '';
				
				if(empty($t1) && empty($t2) && empty($t3)) {
                
					echo '<label class="txt-red ">No Subscriptions</label>';
				}
				else{
					echo Util::makeString($t1, $t2, $t3);	
				}
				
				?>
      </div>
    </div>
    <div class="dash-info-right"> 
    <div><?php echo Util::ToDate($info['created']) ?></div>
    <div class="sep"></div>
    <div id="talent_request_<?php echo $info['request_id']; ?>">
    <a class="button button-alt" cura="talent_request_cancel" data-id="<?php echo $info['request_id']; ?>">Cancel</a> </div></div>
  </div>
  <div class="dash-info-middle dash-info-desc"> </div>
  
  
</div>
<?php
			  
	  }
	}
		  
?>
