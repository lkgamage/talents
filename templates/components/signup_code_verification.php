<form id="otp_form" cura="validate_otp" >
  <input type="hidden" name="purpose" value="signup">
  <input type="hidden" name="email_code_id" id="email_code_id" value="<?php echo $emailcode->id; ?>">
  <input type="hidden" name="code_id" id="code_id" value="<?php echo $code->id; ?>">

      <div style="display:none"><img src="<?php echo Util::mapURL('images/talent-logo-16i.png') ?>"></div>
      <div class="page-header nopadding">Hello <?php echo $user->firstname; ?>,</div>
       <div class="sep"></div>
      <div class="nopadding" >
        <p class="center">We have sent OTP into your mobile number and email address.</p>
        <div>Please enter OTP to continue.</div>
        <div class="field-wrapper" style="text-align:center">
          <input type="text" class="input-short is-centered vcode" name="vcode" id="vcode" maxlength="6" >
        </div>
        <div id="_server_error"></div>
        <div class="button-container">
          <div class="center"><a class="button btn-next" cura="validate_otp" >Verify OTP</a></div>
        </div>
        <div class="sep"></div>
        <div class="sep"></div>
        <diV>Haven't received OTP yet?</diV>
        <div class="txt-light" id="otp_resend">You may request to re-send OTP in <span id="otp_delay">2:00</span></div>
      </div>

</form>
