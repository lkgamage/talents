<div class="vapdding">
<?php

if(!empty($talents)) { 

	foreach ($talents as $item ) {
		
		if(!empty($item['agency_id'])){
			continue;	
		}
		
?><div class="result-block">
        <div class="result-block-inner"> <a  href="<?php echo Util::mapURL('/'.$item['handle']) ?>" target="_blank"><img src="<?php 
		
		if(empty($item['image'])){
			if($item['gender'] == 1) {
				echo Util::mapURL('images/placeholder-boy.png');
			}
			else {
				echo Util::mapURL('images/placeholder-girl.png');
			}
		}
		else{
			echo $item['image'];	
		}
		
		 ?>"></a>
          <div class="block-info middle">
          <div class="resizable">
            <div class="block-name"><a  target="_blank" href="<?php echo Util::mapURL('/'.$item['handle']) ?>"><?php echo $item['name']; ?></a></div>
            <div class="block-categoty"><?php echo $item['skills']; ?>
            </div>
            <div class="block-popularity txt-gray"><?php echo Util::makeString($item['city'], $item['region'], DataType::$countries[$item['country']][0]); ?></div>
            
            </div>
            <?php
			
				if(!empty($item['request_modified'])) {
					echo '<div class="txt-s txt-orange">';
					
					switch ($item['request_status']){
						case DataType::$AGENCY_REQEST_REJECTED : echo 'Previous request has rejected';
						break;
						case DataType::$AGENCY_REQEST_AGENCY_REMOVED : echo "You have removed this talent on ".Util::ToDate($item['request_modified']);
						break;
						case DataType::$AGENCY_REQEST_TALENT_REMOVED : echo "Talent has quitt from agency on ".Util::ToDate($item['request_modified']);
						break;
					}
					
					echo '</div>';
				}
			
			?>
            <div class="block-button-bar" id="talent_request_<?php echo $item['talent_id']; ?>">
            <?php
			
			if(isset($item['request_status'])) {
			
				switch ($item['request_status']){
					case DataType::$AGENCY_REQEST_PENDING : echo '<div>Requested</div> <a class="button-alt" cura="talent_request_cancel" data-id="'.$item['id'].'" >Cancel</a>';
					break;
					case DataType::$AGENCY_REQEST_ACCEPTED : echo "Accepted";
					break;
					default: 
						echo ' <a class="button" title="Request Access" cura="talent_request_access" data-id="'.$item['talent_id'].'" >Request Access</a>';	
				}
			
			}
			else{
				echo ' <a class="button" title="Request Access" cura="talent_request_access" data-id="'.$item['talent_id'].'" >Request Access</a>';	
			}
			
			?>
           
            
            </div>
          </div>
        </div>
      </div><?php
	  
	//  Util::debug($item);
	}
}

//Util::debug($talents);

?>