<?php

if(!empty($data)) {
//	Util::debug($data);
	$data = Util::groupMultiArray($data, 'begin_date');
	//Util::debug($data);
	
	foreach ($data as $date => $items) {
		
		echo '<div class="center txt-l">'.Util::ToDate($date, NULL, NULL, NULL, true).'</div><div class="sep"></div><div class="panel">';
		
		
		foreach ($items as $item) {
			
		$datetime = Util::Todate($item['begins'], 1);

?>
<!-- appointment_brief -->
<div class="appointment-row <?php echo $item['confirmed'] == 1 ? 'appointment-confirmed' : 'appointment-scheduled' ?>" cura="context" data-action="appointment_options"  data-id="<?php echo $item['id']; ?>">
  <div class="appointment-date">
    <div><?php echo $datetime[1]; ?></div>
  </div>
  <div class="appointment-desc">
    <div><?php echo nl2br($item['description']) ?>
      <div>
      <?php
	  if($item['confirmed'] == 1){
			echo '<label>Confirmed</label>';
		}
		else{
			echo '<label>Scheduled</label>';
			
			if(!empty($item['expired'])) {
				echo '<div class="appointment-exp">Expires '.Util::Todate($item['expired']).'</div>';
				
			}
		}
		
		if(!empty($item['agency_id'])){
			 echo '<label class="agency">Agency</label>';
		}
	  
	  ?>
      </div>
    </div>
  </div>
  <div class="appointment-notes">
    <div></div>
  </div>
  <div class="appointment-duration">
    <div>3hr</div>
  </div>
</div>

<?php } 
		echo '</div><div class="sep"></div><div class="sep"></div>';
	}
}


	?>