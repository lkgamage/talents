<div class="dash-sep"></div>
<div class="padding bg-white">
  <div class="flex">
    <div class="resizable info-header txt-orange">Bookings</div>
    <div><a class="button" cura="agency_talent" data-id="<?php echo $request->id; ?>" data-action="new_booking">New</a></div>
  </div>

  <form cura="search_booking" id="search_booking">
  <input type="hidden" name="view_id" value="5">
  <input type="hidden" name="request_id" value="<?php echo $request->id; ?>">
<div>
<div class="field-wrapper nopadding">
    <select name="status" cura="search_booking" data-type="change" class="input-short" >
      <option value="1" > - Active - </option>
      <?php
	foreach (DataType::$BOOKING_STATUS as $code => $name) {
		echo '<option value="'.$code.'">'.$name.'</option>';	
	}
	
	?>
      <option value="" > - All - </option>
   
    </select></div>
    </div>
</form>
  <div class="sep"></div>
   <div class="sep"></div>
  <div id="booking-list" cura="search_booking" data-type="load"></div>
     <div class="sep"></div>
</div>
<div class="hpadding strip-light-gray"  >
  <div class="button-container ">

    <div class="center"></div>
    <a class="button button-alt" cura="inline_load_cancel" >Close</a> </div>
</div>