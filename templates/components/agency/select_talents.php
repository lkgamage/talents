<?php if(!empty($talents)) { ?>

<div class="block-header txt-black">Select a talent</div>
<div class="auto-warp">
  <?php
  
    $skills = App::getSkillArray();

foreach ($talents as $info) {		
				
	$t1 = !empty($info['t1']) ? $skills[$info['t1']]['name'] : '';
	$t2 = !empty($info['t2']) ? $skills[$info['t2']]['name'] : '';
	$t3 = !empty($info['t3']) ? $skills[$info['t3']]['name'] : '';	
	
		
	if(!empty($info['i400'])){
		$img =  $info['i400'];
	}
	elseif($info['gender'] == 0){
		
		$img = Util::mapURL('/images/placeholder-girl.png');
	}
	else{
		$img = Util::mapURL('/images/placeholder-boy.png');	
	}

			
	
	echo '<div data-talent="'.$info['id'].'" cura="'.(!empty($cura) ? $cura : '').'"';
	
	foreach (array('action', 'event','ref') as $i) {
		echo (isset($_POST[$i]))  ? 'data-'.$i.'="'.$_POST[$i].'"' : '';
	}
	echo ' >';
	echo '<div class="item-selector">';
	echo '<img src="'.$img.'">';
	echo '<div >'.$info['name'].'<span>'.Util::makeString($t1, $t2, $t3).'</span></div>';
	echo '</div>';
	echo '</div>';
	
}

if(!empty($cura) && $cura == 'agency_event_booking'){
	
	echo '<div cura="redirect" data-url="search">
    <div class="item-selector  padding">
    <div class="is-centrered middle vpadding center">Search for  talent or business</div>
    </div>
  </div>';
		
	
}

?>
  
</div>
<?php } else { 
	// nothing here
} ?>
<div class="sep"></div>
<?php
//UtiL::debug($talents);
?>
