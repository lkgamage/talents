<div class="dash-sep"></div>
<div class="padding bg-white">
  <div class="flex">
    <div class="resizable info-header txt-orange">Appointments</div>
    <div><a class="button" cura="agency_talent" data-id="<?php echo $request->id; ?>" data-action="new_appointment">New</a></div>
  </div>
  <div class="sep"></div>
  <form id="appointment-search" cura="agency_talent_appointments">
  <div class="flex">
    <div class="resizable">
      <div class="field-wrapper nopadding">
        <select name="status" id="appointment_filter" cura="switch_appointment_list" data-type="change" class="input-short">
          <option value="1"> Upcoming </option>
          <option value="2"> Previous </option>
          <option value="0"> Browse </option>
        </select>
      </div>
    </div>
    <div id="appointment_day" class="hidden"><span class="middle ">
      <div class="jumper" data-action="switch_day" data-callback="agency_talent_appointments"> <a></a>
        <div class="filter-bar">
          
            <input type="hidden" name="timespan" id="timespan" value="day">
            <input type="hidden" name="talent" value="<?php echo $request->id; ?>">
            <input type="text" placeholder="Date" name="filter_date" class="datepick past" id="filter_date" data-changed="agency_talent_appointments" value="<?php  echo (!empty($date)) ? Util::ToDate($date) : Util::ToDate(date("Y-m-d"));
	   ?>">
         
        </div>
        <a></a> </div>
      </span> </div>
  </div>
   </form>
  <div class="sep"></div>
  <div class="sep"></div>

    <div id="appointments"  >
      <?php
	  
	  component('/agency/talent_appointment_list', array('appointments' => $appointments));
	  
   //   component('/talent/appointments-day', array('date' => $date, 'appointments' => $appointments,'is_agency' => true, 'request' => $request ));
	  ?>
    </div>

  <div class="sep"></div>
</div>
<div class="hpadding strip-light-gray"  >
  <div class="button-container ">
    <div class="center"></div>
    <a class="button button-alt" cura="inline_load_cancel" >Close</a> </div>
</div>
