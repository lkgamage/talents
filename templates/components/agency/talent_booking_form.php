<?php
$packages = $talent->getPackageDetail();
$event_types =  App::getEventTypes();

if(!empty($booking)){
	
	//$info = $booking->getinfo();
	
	$datetime = Util::ToDate($booking->session_start, 1);
	$location = $booking->getLocation();
	
	$address = Util::makeArray($booking->venue, $location->city, $location->region, DataType::$countries[$location->country][0]);
	
	$venue = $booking->venue;
}
elseif (!empty($event)){
	
	$datetime = Util::ToDate($event->begins, 1);
	$location = $event->getLocation();
	
	$address = Util::makeArray($event->venue, $location->city, $location->region, DataType::$countries[$location->country][0]);
	
	$venue = $event->venue;
}

if(!empty($with_header) && !empty($talent)) {
		
?>

<div  class="panel">
<?php
component('agency/talent_header', array('talent' => $talent));
?>
<div class="dash-sep"></div>
<div class="padding bg-white">
<?php } else { ?>
<div class="dash-sep"></div>
<div class="padding bg-white">
  <div class="flex">
    <div class="resizable info-header txt-orange"><?php echo !empty($booking) ? 'Update' : 'New' ?> Booking</div>
  </div>
  <?php } ?>
  <div class="hpadding">
    <form id="agency_booking_update" cura="<?php echo (!empty($event)) ? 'event_booking_update' : 'agency_booking_update' ?>">
      <input type="hidden" name="request_id" value="<?php echo (!empty($request)) ? $request->id : ''; ?>">
      <input type="hidden" value="<?php echo !empty($booking) ? $booking->id : '' ?>" name="booking_id" id="booking_id" >
      <input type="hidden" value="<?php echo !empty($talent) ? $talent->id : '' ?>" name="talent_id" id="talent_id" >
      <input type="hidden" name="address" class="_address" value="<?php echo (!empty($location)) ? $location->address : ''; ?>">
      <input type="hidden" name="place" class="_place" value="<?php echo (!empty($venue)) ? $venue : ''; ?>">
      <input type="hidden" name="postal" class="_postal" value="<?php echo (!empty($location)) ? $location->postalcode : ''; ?>">
      <input type="hidden" name="country" class="_country" value="<?php echo (!empty($location)) ? $location->country : ''; ?>">
      <input type="hidden" name="city" class="_city" value="<?php echo (!empty($location)) ? $location->city : ''; ?>">
      <input type="hidden" name="region" class="_region" value="<?php echo (!empty($location)) ? $location->region : ''; ?>">
      <div class="field-wrapper compact">
        <div class="columns" >
          <div class="column col2" >
            <label>Date</label>
            <input type="text" class="input-short datepick " name="b_date" id="b_date" maxlength="400" value="<?php echo (!empty($datetime)) ? $datetime[0] : ''; ?>">
          </div>
          <div class="column col2" >
            <label>Time</label>
            <input type="text" class="input-short timepick" name="b_time" id="b_time" maxlength="400" value="<?php echo (!empty($datetime)) ? $datetime[1] : ''; ?>">
          </div>
        </div>
      </div>
      <?php if(empty($event)) { ?>
      <div id="auto_address" >
        <div class="field-wrapper" >
          <label class="" for="b_place">Hotel, Reception hall, Auditorium or City</label>
          <input type="text" class="addresspick" name="b_place" id="b_place" maxlength="200" value="<?php echo (!empty($address)) ? implode(', ',$address) : ''; ?>">
        </div>
        <div class="sep"></div>
      </div>
      <?php } ?>
      Package
      <div class="sep"></div>
      <div>
        <?php
	  
	  if(!empty($packages)) {

	   foreach ($packages as $pkg) {
			 
			echo '<label class="block-select" >
					  <input type="radio" '.((count($packages) == 1 || (!empty($booking) && $booking->package_id == $pkg['package']['id']) ) ?  'checked' : '').' value="'.$pkg['package']['id'].'" name="session_id" '.((!empty($pvs) && $pvs == $pkg['package']['id']) ? 'checked' : '').'  >
					  <span>
					  <span class="bs-name">'.$pkg['package']['name'].'<span class="bs-desc">'.$pkg['package']['description'].'</span></span> 
					  <span class="bs-price">'.currency($pkg['package']['fee']).'<span class="bs-desc">'.(!empty($pkg['package']['timespan']) ? Util::ToTime($pkg['package']['timespan']) : '').'</span></span>
					  </span>
					  </label>';
		 }
	  }
	 else{
			echo '<div class="booking-status-message padding bg-yellow" >'.$talent->name.' has not created any packages or does not have an active subscription.<br>Booking is not possible without selecting a package.</div><div class="sep">';
	 }
		 
		// Util::debug($packages);
	  ?>
      </div>
      <div class="field-wrapper compact">
        <label>Event</label>
        <div id="event_list">
          <?php
			if(!empty($event)){
				
				component('event/events', array('data' => $event->brief(), 'no_date' => true, 'cura' => ''));
				
				
				echo '<div class="right"><a href="javascript:void(0)" cura="browser_events" >Change</a></div> ';
			}
			elseif(!empty($booking) && !$booking->isEmpty('event_id')){
				
				$event = $booking->getEvent();
				
				component('event/events', array('data' => $event->brief(), 'no_date' => true, 'cura' => ''));
				
				echo '<div class="right"><a href="javascript:void(0)" cura="browser_events"  >Change</a></div> ';
				unset($event);
			}
			else{
				echo ' <div class="vpadding"><a href="javascript:void(0)" cura="browser_events">Create Event</a></div>';
			}
		?>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="section <?php echo (empty($with_header)) ? 'strip-light-gray' : ''; ?>"  >
  <div class="button-container">
    <?php if(!empty($booking) && !empty($request) && $request->agency_id == $booking->agency_id && $booking->status == DataType::$BOOKING_PENDING){
		echo '<a cura="agency_booking_remove" data-id="'.$request->id.'"  data-booking="'.$booking->id.'">Cancel Booking</a>';
	}?>
    <div class="center"></div>
    <?php if(!empty($packages)) { ?>
    <a class="button btn-next" cura="<?php echo (!empty($event)) ? 'event_booking_update' : 'agency_booking_update' ?>" > <?php echo empty($booking) ? 'Create Booking' : 'Update' ?></a>
    <?php } ?>
    <div class="gap"></div>
    <a class="button button-alt" cura="<?php echo (!empty($request)) ? 'agency_talent' : 'inline_load_cancel' ?>" data-id="<?php echo (!empty($request)) ? $request->id : '' ?>"  >Cancel</a> </div>
  <div class="sep"></div>
</div>
<?php
echo (!empty($with_header)) ? '</div>' : '';
?>