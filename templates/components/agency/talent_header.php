<?php
	$talentinfo = $talent->getinfo();
	$skills = App::getSkillArray();
	
	if(!empty($talentinfo['image']['id'])){
		$img = $talentinfo['image']['thumb'];
	}
	elseif($talentinfo['talent']['gender'] == 0){					
		$img = Util::mapURL('/images/placeholder-girl.png');
	}
	else{
		$img = Util::mapURL('/images/placeholder-boy.png');	
	}
	
	$t1 = !empty($talentinfo['skills'][0]) ? $skills[$talentinfo['skills'][0]]['name'] : '';
	$t2 = !empty($talentinfo['skills'][1]) ? $skills[$talentinfo['skills'][1]]['name'] : '';
	$t3 = !empty($talentinfo['skills'][2]) ? $skills[$talentinfo['skills'][2]]['name'] : '';

?><div class="dash-info-row with-picture noborder hpadding noshadow" id="talent_11">
  <div class="dash-info-top">
    <div class="dash-info-pic"> <img src="<?php echo $img; ?>"> </div>
    <div class="dash-info-left">
      <div class="dash-info-name"><?php echo $talentinfo['talent']['name']; ?></div>
      <div class="dash-info-place"><?php echo Util::makeString($talentinfo['location']['city'], $talentinfo['location']['region'], DataType::$countries[$talentinfo['location']['country']][0]) ?></div>
      <div class="dash-info-status txt-light"> <?php echo Util::makeString($t1, $t2, $t3) ?>&nbsp; </div>
    </div>
    <div class="dash-info-right"> </div>
  </div>
</div>
