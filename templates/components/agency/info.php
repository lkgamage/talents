<div class="padding section strip-light-gray">
<div class="sep"></div>
<div class="section-header txt-gray nopadding">Update Agency Settings</div>
<div class="sep"></div>
  <form cura="customer_info_update" id="customer_info_update">
  <input type="hidden" name="profile" value="1">
    <div class="field-wrapper">
      <label class="dynamic-label" for="agency_name" >Agency Name</label>
      <input   class="input-short" type="text" value="<?php echo !empty($object) ? $object->name : '' ?>" name="agency_name" id="agency_name" maxlength="100">
    </div>
    
    <div class="field-wrapper">
      <label class="dynamic-label" for="agency_type" >Agency Type</label>
      <select name="agency_type" id="agency_type"  class="input-short">
      
      <?php
	  
	  	foreach ( DataType::$AGENCY_TYEPS as $id => $name) {
			
			if($id == $object->agency_type){
				echo '<option selcted value="'.$id.'">'.$name.'</option>';
			}
			else {
				echo '<option value="'.$id.'">'.$name.'</option>';
			}
							
				
		}
	  
	  ?>
      </select>
     
    </div>
    
   
    <div class="button-container">
      <div class="center"></div>
      <a class="button btn-next" cura="customer_info_update" >Update</a>
      <div class="gap"></div>
      <a class="button button-alt" cura="customer_info_close" >Cancel</a> </div>
  </form>

</div>
<div class="dash-sep"></div>
