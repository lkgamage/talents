<div class="dash-sep"></div>
<div class="padding bg-white">
  <div class="txt-m">Remove <?php echo $talent->name; ?>?</div>
  <div class="sep"></div>
  <div class="txt-orange">When you remove <?php echo $talent->name; ?> from your agency, <span class="txt-orange">you'll not be abel to manage</span> <?php echo ($talent->gender == 1 ? 'his' : 'her')?> bookings, appoinments, work schedule, profile information and public page content from agency.</div>
  <?php if($talent->customer_id == $user->id) { ?>
  <div class="sep"></div>
  <div>But you can still manage <?php echo ($talent->gender == 1 ? 'his' : 'her')?> data from your account.</div>
  <?php }  ?>
  <div class="sep"></div>
  <div>Are you sure you want to remove <?php echo $talent->name; ?> from your agency?</div>
</div>
<div class="hpadding strip-light-gray"  >
  <div class="button-container ">
    <div class="center"></div>
    <a class="button btn-next"  cura="talent_remove_access2" data-talent="<?php echo $talent->id; ?>" data-agency_confirmed="1" >Remove Talent</a>
    <div class="gap"></div>
    <a class="button button-alt" cura="inline_load_cancel" >Cancel</a> </div>
</div>
