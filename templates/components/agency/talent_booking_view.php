<?php
$info = $booking->getinfo();
$timestamp = Util::ToDate($info['booking']['session_start'], 1);


$stack = Util::makeArray($info['booking']['venue'], $info['location']['address'], $info['location']['city'], $info['location']['region']);

?>

<div class="dash-sep"></div>
<div class="bg-white" >
  <div class="padding">
    <div class="hpadding">
      <div class="info-header txt-orange ">Booking Detail</div>
    </div>
    <div class="hpadding">
      <div class="columns panel vpadding ">
        
        <div class="column-3">
        <div class="info-header nopadding">Status</div>
        <div class=" <?php 
		  echo ($info['booking']['status'] ==   DataType::$BOOKING_PAYMENT || $info['booking']['status'] == DataType::$BOOKING_ACCEPTED ) ? 'txt-orange' :($info['booking']['status'] == DataType::$BOOKING_REJECT ? '' : 'txt-blue');
		  
		   ?>"><?php echo DataType::$BOOKING_STATUS[$info['booking']['status']]; ?></div>
        </div>
        
        <div class="column-3">
          <div class="info-header nopadding">BOOKING ID</div>
          <?php echo $booking->id; ?> </div>
          <div class="column-3">
          <div class="info-header nopadding">DUE DATE</div>
          <?php echo Util::ToDate($booking->due_date, true); ?> </div>
      </div>
    </div>
    <div class="sep"></div>
    <div class="sep"></div>
    <div class="columns">
      <div class="column-2 ">
        <div class="info-header txt-orange ">Date/Time</div>
        <div class="info-body "> <?php echo $timestamp[0]; ?>
          <div class="txt-light"><?php echo $timestamp[1]; ?></div>
        </div>
      </div>
      <div class="column-2 ">
        <div class="info-header txt-orange ">Place</div>
        <div class="info-body ">
          <div><?php echo array_shift($stack); ?></div>
          <div class="txt-light"><?php echo implode(", ", $stack); ?></div>
          <div class="txt-light"><?php echo DataType::$countries[$info['location']['country']][0]; ?></div>
          <div class="txt-light"></div>
        </div>
      </div>
    </div>
  
  </div>
  <div class="padding">
    <div class="columns <?php echo (!empty($info['package']['suggested_id']))? 'bg-blue vpadding' : ''; ?>">
      <div class="column-2 ">
        <div class="info-header txt-orange ">package</div>
        <div class="info-body">
          <div class=""><?php echo $info['package']['name']; ?></div>
          <div class="txt-light"><?php echo currency($info['package']['fee'], $info['talent']['currency']); ?></div>
          <div class="txt-light"><?php echo Util::ToTime($info['package']['timespan']); ?></div>
        </div>
      </div>
      <div class="column-2 "> </div>
    </div>
  </div>
    <div class="padding section">
    <div class="info-header txt-orange ">Event</div>
    <div class="info-body">
    <?php
	
	if(!empty($info['event']['id'])){
				
		
	}
	else{
		echo '<a cura="context" data-action="attach_event" data-ref="booking" data-id="'.$booking->id.'" >Add Event</a> </div>';
	}
	?>
    </div>
    </div>

</div>
<div class="hpadding strip-light-gray"  >
  <div class="button-container">
    <?php if(!empty($booking) && !empty($request) && $request->agency_id == $booking->agency_id && $booking->status == DataType::$BOOKING_PENDING){
		echo '<a cura="agency_booking_remove" data-id="'.$request->id.'"  data-booking="'.$booking->id.'">Cancel Booking</a>';
	}?>
    <div class="center"></div>
     <?php if(!empty($booking) && !empty($request) && $request->agency_id == $booking->agency_id && $booking->status == DataType::$BOOKING_PENDING){
		echo '<a class="button" cura="agency_talent" data-id="'.$request->id.'"  data-booking="'.$booking->id.'" data-action="edit_booking">Edit Booking</a>';
	}?>

    <div class="gap"></div>
    <a class="button button-alt" cura="agency_talent" data-id="<?php echo $request->id ?>" data-action="bookings" >Close</a> </div>
</div>
