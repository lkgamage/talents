<?php
$event_types =  App::getEventTypes();
$begins = array('','');
	
if(!empty($appointment)){
	$begins = Util::ToDate($appointment->begins, 1);

}
elseif (!empty($event)){
	
	$begins = Util::ToDate($event->begins, 1);
}

if(!empty($with_header) && !empty($talent)) {
?>

<div  class="panel">
<?php
component('agency/talent_header', array('talent' => $talent));
?>
<div class="dash-sep"></div>
<div class="padding bg-white">
<?php } else { ?>
<div class="dash-sep"></div>
<div class="padding bg-white">
  <div class="flex padding">
    <div class="resizable info-header txt-orange"><?php echo !empty($appointment) ? 'Update' : 'New' ?> Appointment</div>
  </div>
  <?php } ?>
  <div class="hpadding">
    <form id="agency_appointment_update" cura="<?php echo (!empty($event)) ? 'event_appointment_update' : 'agency_appointment_update' ?>">
      <input type="hidden" value="<?php echo !empty($appointment) ? $appointment->id : '' ?>" name="appointment_id" id="appointment_id" >
      <input type="hidden" name="talent_id" value="<?php echo (!empty($talent)) ? $talent->id : '' ?>"> 
      <input type="hidden" name="request_id" id="request_id" value="<?php echo (!empty($request)) ? $request->id : '' ?>">
      <?php if(empty($appointment) || !$appointment->isBooking()){  ?>
      <div class="field-wrapper compact">
        <div class="flex">
          <div class="col2">
            <label>Date</label>
            <input type="text" class="datepick " id="appointment_begins_date" name="appointment_begins_date" value="<?php echo $begins[0];   ?>" data-value="<?php echo $begins[0];   ?>">
          </div>
          <div class="col2 non-ful-day">
            <label>Time</label>
            <input type="text" class="timepick " id="appointment_begins_time" name="appointment_begins_time"   value="<?php echo $begins[1];   ?>" data-value="<?php echo $begins[1];   ?>">
          </div>
        </div>
      </div>
      <div class="field-wrapper non-ful-day compact">
        <label >Duration</label>
        <input autofocus type="text" class="input-short" value="<?php echo !empty($appointment) ? $appointment->duration : '' ?>" name="appointment_duration" id="appointment_duration"  maxlength="10" cura="convert_time" data-type="blur" placeholder="30m or 1h">
      </div>
      <?php } ?>
      <div class="field-wrapper compact">
        <label>Title</label>
        <input type="text" value="<?php echo !empty($appointment) ? $appointment->description : '' ?>" name="appointment_description" id="appointment_description" maxlength="400">
      </div>
      <div class="field-wrapper compact">
        <label>Event</label>
        <div id="event_list">
          <?php
			if(!empty($event)){
				
				component('event/events', array('data' => $event->brief(), 'no_date' => true, 'cura' => ''));
				
				
				echo '<div class="right"><a href="javascript:void(0)" cura="browser_events" >Change</a></div> ';
			}
			elseif(!empty($booking) && !$booking->isEmpty('event_id')){
				
				$event = $booking->getEvent();
				
				component('event/events', array('data' => $event->brief(), 'no_date' => true, 'cura' => ''));
				unset($event);
				echo '<div class="right"><a href="javascript:void(0)"  cura="browser_events"  >Change</a></div> ';
			}
			else{
				echo ' <div class="vpadding"><a href="javascript:void(0)" cura="browser_events">Add an event</a></div>';
			}
		?>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="hpadding <?php echo (empty($with_header)) ? 'strip-light-gray' : ''; ?>"  >
  <div class="button-container">
    <?php if(!empty($appointment) && !empty($request) && $request->agency_id == $appointment->agency_id){
		echo '<a cura="agency_appointment_remove" data-id="'.$request->id.'"  data-appointment="'.$appointment->id.'">Remove Appointment</a>';
	}?>
    <div class="center"></div>
    <a class="button btn-next" cura="<?php echo (!empty($event)) ? 'event_appointment_update' : 'agency_appointment_update' ?>" > <?php echo empty($appointment) ? 'Add Appointment' : 'Update' ?></a>
    <div class="gap"></div>
    <a class="button button-alt" cura="<?php echo (!empty($request)) ? 'agency_talent' : 'inline_load_cancel' ?>" data-id="<?php echo (!empty($request)) ? $request->id : '' ?>" data-action="appointments"  >Cancel</a>  </div>
</div>
<?php
echo (!empty($with_header)) ? '</div>' : '';
?>