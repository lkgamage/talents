<?php
if(!isset($location)) {
	$location = $talent->getLocation();
}
?><div class="talent-booking-profile">
  <div class="talent-booking-image"><img src="<?php echo $talent->getImage()->i400; ?>"></div>
  <div class="talent-booking-detail">
    <div class="talent-booking-name"><?php echo $talent->name; ?></div>
    <div class="talent-booking-skills"><?php echo $talent->getSkillString(); ?></div>
    <div class="talent-booking-place"><?php echo Util::makeString($location->city, $location->region, DataType::$countries[$location->country][0]);  ?></div>
  </div>
  <div class="talent-booking-link"><a class="link-button-" href="<?php echo Util::mapURL('/t/'.$talent->id); ?>">More...</a></div>
</div>
