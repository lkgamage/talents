<?php

$profile = Session::getProfile();
$customer = Session::getCustomer();

if(!empty($data)){
	
	foreach ($data as $item) {
		
		$image = '';
			
		echo '<div class="message-block '.(($customer['id'] != $item['customer_id']) ? 'inward' : '').'">';
		echo '  <div class="message-body">';
		echo '    <div class="message-info">';
		echo '      <div class="message-from">';
		
		if($item['originated'] == 'c'){
			echo $item['customer_name'];
			$image = fixImage($item['customer_image'], $item['customer_gender'], false);	
		}
		elseif($item['originated'] == 't'){
			echo $item['talent_name'];	
			$image = fixImage($item['talent_image'], $item['talent_gender'], false);
		}
		elseif($item['originated'] == 'a'){
			echo $item['agency_name'];	
			$image = fixImage($item['agency_image'], NULL, true);
		}
		
		echo '</div>';
		echo '      <div class="message-date">'.Util::ToReadableDate($item['created']).'</div>';
		echo '    </div>';
		echo '    <div class="message-contents">'.nl2br($item['message']).'</div>';
		echo '  </div>';
		echo '  <div class="message-header"><img src="'.$image.'"></div>';
		echo '</div>';	
		
	}
	
}

//Util::debug($data);




?>