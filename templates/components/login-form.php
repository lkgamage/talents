<div class="middle center-panel " id="login_block">
	<div class="padding">
  <form id="login_form" cura="validate_login">
    <div class="sep"></div>
    <h1 class="page-header txt-bold nopadding center" cura="greet" data-type="load">Welcome Back!</h1>
    <div class="sep"></div>
    <div class="field-wrapper">
      <input type="text" placeholder="Email or Mobile Number" name="username" id="username" >
      <label style="display:none" id="username-error" for="username" class="error center">Please enter email address or mobile number</label>
    </div>
    <div class="field-wrapper" id="login_country_select">
      <select name="country" id="country" class="input-short" >
        <?php
			$country = Session::getCountry();
			foreach (DataType::$countries as $a => $c){
				
				if($country == $a){
					echo '<option selected="selected" value="'.$a.'">'.$c[0].'</option>';
				}
				else {
					echo '<option value="'.$a.'">'.$c[0].'</option>';
				}
					
			}			
			
			?>
      </select>
    </div>
    <div id="_server_error"></div>
    <div class="button-container">
      <div class="center"></div>
      <a class="button btn-next" cura="validate_login">Log In</a> </div>
    <div class="sep"></div>
    <div class="sep"></div>
    <div style="display:none">
      <div class="sub-header txt-gray">Login with Social Media</div>
      <div class="social-login"> <a class="social-login-button social-fb"><span>Login with Facebook</span></a> <a class="social-login-button social-tw"><span>Login with Twitter</span></a> <a class="social-login-button social-gl"><span>Login with Google</span></a> <a class="social-login-button social-ln"><span>Login with LinkedIn</span></a> </div>
      <div class="sep"></div>
      <div class="sep"></div>
    </div>
    <p>If you do not have an account, <a href="<?php echo Util::mapURL('/join'); ?>" >create an account</a> now. </p>
	  <div>Need login support? See <a href="<?php echo Util::mapURL('/support/login') ?>">login support page</a></div>
    <div class="sep"></div>
  </form>
		</div>
</div>
