<?php

$packages = $talent->getPackages();

if(!empty($_POST['date'])) { 
?>

<div>
  <div class="padding bg-orange">
    <p class="single txt-orange txt-xl"> Elisha is available on xxx. </p>
    <div class="txt-light">There may be some bookings to be confirmed. Make your booking now to avoid disapoinments.</div>
  </div>
  <div class="box-body">
    <div class="sep"></div>
    <div class="field-wrapper inline-edit" style="padding:0;" data-submit="chcek_talent" data-id="" title="Change Date">
      <label >Date</label>
      <div class="dateinput txt-left place-holder edit-trigger"><?php echo $_POST['date']; ?></div>
      <div class="field-holder">
        <input type="text" class="input-short datepick dateinput" name="b_date" id="b_date" maxlength="400"   placeholder="Select Date" data-id="<?php echo $talent->id; ?>" value="<?php echo $_POST['date']; ?>">
      </div>
    </div>
    <div class="field-wrapper" id="session-header">
      <label >Session</label>
    </div>
    <div class="booking-sessions">
      <?php
		  
		  $pvs = Util::postValue('session_id');
		  
		  foreach ($packages as $pkg){
			  

			  if($pkg->active == 0){
				continue;  
			  }
			
			echo '<label class="block-select">
              <input type="radio" '.(count($packages) == 1 ?  'checked' : '').' value="'.$pkg->id.'" name="session_id" '.((!empty($pvs) && $pvs == $pkg->id) ? 'checked' : '').' '.($pkg->location_required == 1 ? 'data-location="1"' : '').' >
              <span><span class="bs-name">'.$pkg->name.'<span class="bs-desc">'.$pkg->description.'</span></span> <span class="bs-price">$'.$pkg->fee.'<span class="bs-desc">'.(!$pkg->isEmpty('timespan') ? Util::$TimeMap[$pkg->timespan] : '').'</span></span></span></label>';
             
		  }
		  
		  ?>
    </div>
    <div class="field-wrapper">
      <label>Time</label>
    </div>
    <div class="field-wrapper nopadding">
      <input type="text" class="input-short timepick" name="b_time" id="b_time" maxlength="400" value="<?php echo Util::postValue('b_time') ?>">
      <div class="anote">Expected session start time. This can be changed later.</div>
    </div>
    <div id="auto_address" >
      <div class="field-wrapper">
        <label>Place</label>
      </div>
      <div class="field-wrapper nopadding" >
        <input type="text" class="addresspick" name="address_search" id="address_search" maxlength="200" value="<?php echo Util::postValue('address_search') ?>">
        <div class="anote">Hotel, Reception hall, Auditorium or City</div>
      </div>
      <div class="sep"></div>
    </div>
    <div id="manual_address" style="display:none">
      <div class="field-wrapper">
        <label class="dynamic-label" for="b_venue">Venue</label>
        <input type="text" name="b_venue" class="_place" id="b_venue" maxlength="200" value="<?php echo Util::postValue('b_venue') ?>">
      </div>
      <div class="field-wrapper">
        <label class="dynamic-label" for="b_address">Address</label>
        <input type="text"  name="b_address" id="b_address" maxlength="200" value="<?php echo Util::postValue('b_address') ?>" class="_address">
      </div>
      <div class="field-wrapper">
        <label class="dynamic-label" for="b_city">City</label>
        <input type="text" name="b_city" id="b_city" maxlength="100" value="<?php echo Util::postValue('b_city') ?>" class="_city">
      </div>
      <div class="field-wrapper">
        <label class="dynamic-label" for="b_region">Region/State</label>
        <input type="text" name="b_region" id="b_region" maxlength="100" value="<?php echo Util::postValue('b_region') ?>"class="_region">
      </div>
      <div class="field-wrapper">
        <label class="dynamic-label " for="b_postal">Postal Code</label>
        <input type="text" class="input-short _postal" name="b_postal" id="b_postal" maxlength="10" value="<?php echo Util::postValue('b_postal') ?>">
      </div>
      <div class="field-wrapper">
        <label class="" for="b_country">Country</label>
        <select name="b_country" id="b_country" class="_country" >
          <?php
			
			$cnt = Util::postValue('b_country');
			
			foreach (DataType::$countries as $a => $c){
				
				if(!empty($cnt) && $cnt == $a) {
					echo '<option selected value="'.$a.'" data-code="'.$c[1].'">'.$c[0].'</option>';
				}
				else {
				
					echo '<option value="'.$a.'" data-code="'.$c[1].'">'.$c[0].'</option>';
				}
					
			}
			
			
			?>
        </select>
      </div>
    </div>
    <div class="button-container">
            <div class="center"></div>
            <a class="button btn-next" href="javascript:void(0)" onClick="review_booking()">REVIEW</a> </div>
  </div>
</div>
<?php 

} else { 
// no date selection
?>
<div class="box-header bg-blue">Check Availability</div>
<div class="box-body">
  <div>Check if <strong><?php echo $talent->firstname; ?></strong> is available on your dates </div>
  <div class="sep"></div>
  <div class="field-wrapper" style="padding:0;">
    <input type="text" class="input-short datepick dateinput" name="b_date" id="b_date" maxlength="400" value="" data-changed="chcek_talent" placeholder="Select Date" data-id="<?php echo $talent->id; ?>">
  </div>
  <div class="button-container">
    <div class="center" id="check_status"> <a class="button btn-next" href="javascript:void(0)" onClick="chcek_talent()">CHECK</a> </div>
  </div>
</div>
<?php } ?>
