<div class="client-packages">
<?php

if(!empty($packages)) {
	

	
	foreach ($packages as $package) {

?>
<label class="client-package" <?php 
	echo !empty($time) ? 'data-time="'.$time.'" ' : ' ';
	echo !empty($date) ? 'data-date="'.$date.'" ' : ' ';
 ?> >
<input type="radio" name="session_id" value="<?php echo $package['id']; ?>" <?php echo count($packages) == 1 ? 'checked' : '' ?> >
<span class="client-package-wrapper">
<span class="client-package-name"><?php echo $package['name'] ?></span>
	<span class="client-package-header">
    	
        <span class="client-package-skill"><?php echo $package['skill_name'] ?></span>
        <span class="client-package-price"><?php echo currency($package['fee']) ?></span>
        <span class="client-package-time"><?php 
		echo (!empty($package['timespan']) ? Util::ToTime($package['timespan']) : '');
		 
		if(!empty($package['items'])){
			echo ' / '.$package['items'].' items';	
		}
		?></span>
    </span>
    <?php
	
	if(!empty($package['description'])) {
		echo '<span class="client-package-body">'.$package['description'].'</span>';
	}
	
	?>
    <span class="client-package-button"></span>
</span>
</label>

<?php
	}
}



?>
</div>
<?php
//Util::debug($packages);
?>