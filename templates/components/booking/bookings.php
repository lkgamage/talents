<?php

/*
* Depending on who's view this, infornation displaying will be different
* view_id 1 : Talents main display (event/location/ agency(if any)
* view id 2 : Customer's main display (Talent or agency/ location)
* view id 3 : Agency main view (smart) (Event/location for recived and Talent/Location for sent)
* View id 4 : Agency -> event view (talent only)
* View id 5 : Agency -> talent view (event only)
*/

$customer = Session::getCustomer();
$profile = Session::getProfile();

if(!isset($view_id)){
	
	if(isset($_POST['view_id']) && is_numeric($_POST['view_id'])){
		$view_id = $_POST['view_id'];
	}
	elseif(!empty($profile)){
		$view_id = $profile['type'] == 't' ? 1 : 3;	
	}
	else {
		$view_id = 2;
	}
			
}

$sender = false;

//print_r($data);

if(!empty($data)) {
	
	foreach ($data as $item){
		
		
		if(empty($profile) && $customer['id'] == $item['customer_id']){
			$sender = true;				
		}
		elseif(!empty($profile) && $profile['type'] == 'a'){
			
			$sender = !empty($item['talent_id']);		
		}

		$img = $show_agency = NULL;
				
		// image
		if(($view_id == 2 && !empty($item['talent_id'])) || ($view_id == 3 && !empty( $item['talent_id'])) || $view_id == 4){
			// talent image
			$img = fixImage($item['talent_image'], $item['talent_gender'], false);
		}
		elseif(($view_id == 1 || $view_id == 2) && !empty($item['agency_id'])){
			//$img = fixImage($item['agency_image'], NULL, true);
			$show_agency = true;
		}
			
		// time
		$date = Util::ToDate($item['session_start'], 1);
			
		echo '<div cura="'.(!empty($cura) ? $cura : 'get_booking').'" data-id="'.$item['id'].'" class="dash-info-row '.DataType::$BOOKING_CLASSES[$item['status']].' '.(!empty($img) ? 'with-picture' : '').'">';
		echo '  <div class="dash-info-top">';
		
		echo '<label class="row-status">'.DataType::$BOOKING_STATUS[$item['status']].'</label>';
		
		
			
		// show image 
		if(!empty($img)) {
			echo '<div class="dash-info-pic"> <img  src="'.$img.'"> </div>';
		}
		
		echo '<div class="dash-info-left">';
			
		$labels = array();	
		
		if($view_id == 1 || $view_id == 5 || ($view_id == 3 && empty( $item['talent_id']))){
			
			if(!empty($item['event_name'])){
				$labels[] = $item['event_name'];
			}
			
			if(!empty($item['venue'])){
				$labels[] = $item['venue'];
			}
				
		}
		elseif(($view_id == 2 && !empty($item['talent_id'])) || ($view_id == 3 && !empty( $item['talent_id'])) || $view_id == 4 ){
			$labels[] = $item['talent_name'];
			$labels[] = $item['skill_name'];
		}
		/*elseif ($view_id == 2 && !empty($item['agency_id'])) {
			$labels[] = $item['agency_name'];
			$labels[] = $item['agency_type'];
		}*/
		
		if($view_id == 4) {	
			$labels[] = Util::ToTime($item['duration']);
			$labels[] = currency($item['total']);
		}
		else {
			$labels[] = Util::makeString($item['city'], $item['region'],  DataType::$countries[$item['country']][0]);
		}
			
		echo '      <div class="dash-info-name">'.array_shift($labels).'</div>';
		
		if(!empty($show_agency)){
			echo '      <div class="dash-info-place">Booked by <span class="txt-black">'.$item['agency_name'].'</span></div>';
		}
		
			echo '      <div class="dash-info-place">';
			
			foreach ($labels as $i => $lbl) {				
				echo '<span>'.$labels[$i].'</span>';				
			}
			
			
			echo '</div>';
			echo '    </div>';
			echo '    <div class="dash-info-right">';
			echo '      <div class="dash-info-date">'.$date[0].'</div>';
			echo '      <div class="dash-info-time">'.$date[1].'</div>';
			echo '    </div>';
			echo '  </div>';
			echo '  <div class="dash-info-middle dash-info-desc">';
			
			/*********************** Description messages ******************/
			
			if(!$sender){
				// for talent
				if($item['status'] == DataType::$BOOKING_PENDING){
					echo '<div class="flex"><div class="resizable txt-orange">'.(!empty($item['agency_id']) ? 'Agency' : 'Client').' is wating for the response.</div><div><a class="button">Respond</a></div></div>';	
				}
				elseif($item['status'] == DataType::$BOOKING_ACCEPTED){
					
				}				
				elseif($item['status'] == DataType::$BOOKING_PAYMENT){
					echo '<div class="txt-blue">You accepted the booking. Waiting for  payment from customer.</div>';	
				}
				elseif($item['status'] == DataType::$BOOKING_CONFIRM){
					
				}				
				elseif($item['status'] == DataType::$BOOKING_CANCEL){
					
				}
				elseif($item['status'] == DataType::$BOOKING_REJECT){
					
				}
				elseif($item['status'] == DataType::$BOOKING_BLOCKED){
					
				}
			}
			else{
				// for booker
				if($item['status'] == DataType::$BOOKING_PENDING){
					echo '<div class="txt-blue">Wating for '.$item['talent_name']."'s confirmation</div>";	
				}
				elseif($item['status'] == DataType::$BOOKING_ACCEPTED){
					echo '<div class="flex"><div class="resizable txt-orange">Booking accepted. Please review and confirm.</div><div><a class="button button-act">Review</a></div></div>';
				}
				elseif($item['status'] == DataType::$BOOKING_PAYMENT){
					echo '<div class="flex"><div class="resizable txt-orange">Advance payment due. Pay and confirm.</div><div><a class="button button-act">Pay Now</a></div></div>';	
				}
				elseif($item['status'] == DataType::$BOOKING_CONFIRM){
					
				}				
				elseif($item['status'] == DataType::$BOOKING_CANCEL){
					
				}
				elseif($item['status'] == DataType::$BOOKING_REJECT){
					
				}
				elseif($item['status'] == DataType::$BOOKING_BLOCKED){
					
				}
			}
			
			/************************ Description messages end ***************/
			echo '</div>';
			
			
			
			
			/*
			echo '  <div class="dash-info-bottom">';
			echo '    <div><span>Status</span>'.DataType::$BOOKING_STATUS[$item['status']].'</div>';
			echo '    <div><span>Package</span>'.$item['package_name'].'</div>';
			//echo '    <div><span>Advance</span>'.currency($item['advance'] ,$item['currency']).'</div>';
			echo '    <div><span>Due</span>'.Util::ToDate($item['due_date'], true).'</div>';
			echo '  </div>';
			*/
			echo '</div>';
			
			
		
		
	}
	
}


//Util::debug($data);
?>