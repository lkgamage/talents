<?php
$info = $booking->getInfo();
$changes = $booking->getChangeRequests();
$customer = Session::getCustomer();
?>

<div>
  <div class="panel">
    <div class="section">
      <div class="button-container">
        <div class="center"> </div>
        <a class="more-link right" cura="get_booking" data-id="<?php echo $info['booking']['id'] ?>" >View Booking</a> </div>
    </div>
    <div class="dash-sep"></div>
    <?php   component('booking/change_request', array('info'  => $info, 'changes' => $changes, 'customer' => $customer));	?>
    <div class="section">
       <div class="button-container">
        <div class="center"> </div>
        <a class="more-link right" cura="get_booking" data-id="<?php echo $info['booking']['id'] ?>" >View Booking</a> </div>
    </div>
  </div>
</div>
