<?php

	$object = $engine->getObject();

?>
<div class="max-600">
  <form id="booking-form" cura="check_availability" >
    <div class="event-selection">
      <?php
		  // if there is an active event, show active event selection option
		  $active_event = $engine->getActiveEvent();
		  $all_events = (!empty($user)) ? $user->getEvents(true) : array();
		  
		  if(!empty($all_events)){
			  echo '<p>Are you making this booking for an event? Pick an event.</p>';
		  }

		  if(!empty($active_event)){
		
			 ?>
      <?php 
			 // active event
			 echo '<div class="active-event">';			 
			 component('event/events', array('data' => array($active_event), 'no_date' => true, 'cura' => 'set_active_event'));
			 echo '</div>';
			 
			 // all events
			 echo '<div class="all-events hidden" >';
			 component('event/events', array('data' => $all_events, 'cura' => 'set_active_event'));
			 echo '</div>';
			 
			 echo '<div class="active-event">';	
			 echo '<a class="more-link" cura="show_more_events" data-hide="1">Show all my events</a>';
			 echo '</div>';
			 
			 echo '<div class="button-container">';	
			 echo '<div><a href="'.Util::mapURL('/dashboard/event/create').'">Create an event</a></div>';		 
			 echo '<div class="center"></div>';
			 echo '<a class="button" cura="unset_active_event">No, Continue without an event</a>';
			 echo '</div>';
			 ?>
      <?php
			 
		  }
		   // if there are more upcommiing events for this customer, show event pick option
		  elseif(!empty($all_events)){
			  
			  if(count($all_events) > 3){
				 
				 $a = array_slice($all_events, 0, 3);
				 $b = array_slice($all_events, 3);

				 echo '<div class="active-event">';			 
				 component('event/events', array('data' => $a, 'no_date' => true, 'cura' => 'set_active_event'));
				 echo '</div>';
				 
				 echo '<div class="all-events hidden" >';
				 component('event/events', array('data' => $b, 'cura' => 'set_active_event'));
				 echo '</div>';
				  echo '<div class="center"><a class="more-link" cura="show_more_events">Show all my events</a></div><div class="sep"></div>';
					  
			  }
			  else{
				  	echo '<div class="all-events" >';
			 		component('event/events', array('data' => $all_events, 'cura' => 'set_active_event'));
			 		echo '</div>';
			  }
			  
			   	echo '<div class="button-container">';
				echo '<div><a href="'.Util::mapURL('/dashboard/event/create').'">Create an event</a></div>';			 
				echo '<div class="center"></div>';
				echo '<a class="button" cura="unset_active_event">No, Continue without an event</a>';
				 echo '</div>';
		  }
		  // default booking form
		  
		  ?>
    </div>
    <div class="manual-selection <?php echo (!empty($all_events)) ? 'hidden' :'' ?>">
      <p class="nopadding sec-header txt-orange">Booking Date</p>
      <div class="field-wrapper">
        <input type="text" class="input-short datepick dateinput" name="date" id="date" maxlength="40" value="<?php echo !empty($engine) ? $engine->getDate(true) : '' ?>">
      </div>
      <div class="sep"></div>
      <p class="nopadding sec-header txt-orange" id="session-header">Time</p>
      <div class="field-wrapper">
        <input type="text" class="input-short timepick" name="time" id="time" maxlength="40" value="<?php echo !empty($engine) ? $engine->getTime(true) : '' ?>">
      </div>
      <div class="sep"></div>
      <p class="nopadding sec-header txt-orange">Place</p>
      <div id="auto_address" >
        <div class="field-wrapper" >
          <label class="" for="place">Hotel, Reception hall, Auditorium or City</label>
          <input type="text" class="addresspick" name="place" id="place" maxlength="200" value="<?php echo !empty($engine) ? $engine->getPlace() : '' ?>" data-place="<?php echo $engine->getPlaceID(); ?>">
        </div>
        <div class="sep"></div>
      </div>
      <div class="button-container">
        <div class="center"> <a class="button" cura="check_availability" href="javascript:void(0)">Check Availability</a></div>
      </div>
    </div>
  </form>
</div>
<?php
//Util::debug($engine->data);
?>
