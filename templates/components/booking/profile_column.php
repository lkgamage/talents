<?php
$info = $engine->getViewData();

?><div class="talent-booking-profile">
  <div class="talent-booking-image"><img src="<?php echo $info['image']; ?>">
  <div class="talent-booking-name"><?php echo $info['name']; ?></div>
  </div>
  <div class="talent-booking-detail">
   
    <div class="talent-booking-skills"><?php echo $info['skills']; ?></div>
    <div class="talent-booking-place"><?php echo $engine->getObjectAddress()  ?></div>
	<div class="talent-booking-place txt-orange txt-m">Price: <?php
	
	if(empty($info['min_fee'])){
		echo "N/A";
	}
	elseif($info['min_fee'] == $info['max_fee'] ){
		echo currency($info['min_fee']);
	}
	else{
		echo currency($info['min_fee']). ' - '.currency($info['max_fee']);	
	}
		
	
	?> </div>
    <div class="talent-booking-desc"><?php echo nl2br($info['body']) ?></div>
  </div>
  <div class="vpadding right">
  <a class="more-link" href="<?php echo Util::mapURL('/'.$info['handle']); ?>">Visit Page</a>
  </div>

</div>
