<?php



?>
<!--------------------------->
<?php

	component('booking/illustrator', array('booking' => $booking));
?>

<div class="sep"></div>
<div class="sep"></div>
<div class="panel">
  <div class="pm-header pm-success">Booking Submitted</div>
  <div class="padding">
    <div class="sep"></div>
    <p><?php echo $user->firstname; ?>, Your booking request has submitted to <span class="txt-bold"><?php echo  $object->name;  ?></span>. </p>
    <div>You'll recieve a response form <?php echo  $object->name; ?> before <?php echo Util::ToDate($booking->due_date, true) ?>.</div>
    <div class="button-container">
    <a class="button" href="<?php echo Util::mapURL('/dashboard/booking/').$booking->id; ?>">View Booking</a>
      <div class="center"></div>
      <a class="button"  href="<?php echo Util::mapURL('/search'); ?>">Make Another Booking</a></div>
  </div>
</div>
<!---------------------------> 

