<div class="panel padding"><?php

if(!empty($booking)) {
	
	$due_date = $booking->due_date;
	$res_date = $booking->responded_date;
	$con_date = $booking->confirmed_date;
	
	$status = array( 
			array(
				'status' => 'SUBMIT',
				'date' => Util::ToShortDate($booking->created),
				'class' => 'ils-complete'
				)
			 );
	$progress = array();
		
	if($booking->status == DataType::$BOOKING_PENDING){
		
		if(time() > strtotime($due_date)){
			// expired
			$progress[] = 100;
			$status[] = array(
				'status' => 'EXPIRED',
				'date' => Util::ToShortDate($due_date),
				'class' => 'ils-close'
				);
		}
		else{
			// clculate days left
			$delat = time() - strtotime($booking->created);
			$total = strtotime($due_date) - strtotime($booking->created);
			$percent = round(($delat/$total)*100);
			
			$progress[] = $percent;
			
			$status[] = array(
				'status' => 'ACCEPT',
				'date' => Util::ToShortDate($due_date),
				'class' => ''
				);
			 
			// confirm && complete
			$progress[] = 0;
			$progress[] = 0;
			
			$status[] = array(
				'status' => 'CONFIRM',
				'date' => '',
				'class' => ''
				); 
				
			$status[] = array(
				'status' => 'COMPLETE',
				'date' => Util::ToShortDate($booking->session_start),
				'class' => '',
				); 
			 
		}
		
			
	}
	elseif($booking->status == DataType::$BOOKING_ACCEPTED){
		
		$progress[] = 100;
		$status[] = array(
			'status' => 'ACCEPTED',
			'date' => Util::ToShortDate($res_date),
			'class' => 'ils-complete'
			);
		
		if(time() > strtotime($due_date)){
			// expired
			$progress[] = 100;
			$status[] = array(
				'status' => 'EXPIRED',
				'date' => Util::ToShortDate($due_date),
				'class' => 'ils-close'
				);
		}
		else{
			// clculate days left
			$delat = time() - strtotime($res_date);
			$total = strtotime($due_date) - strtotime($res_date);
			$percent = round(($delat/$total)*100);
			
			$progress[] = $percent;
			
			$status[] = array(
				'status' => 'CONFIRM',
				'date' => Util::ToShortDate($due_date),
				'class' => ''
				);
			 
			// confirm && complete
			$progress[] = 0;			
			$status[] = array(
				'status' => 'COMPLETE',
				'date' => Util::ToShortDate($booking->session_start),
				'class' => '',
				); 
			 
		}
				
	}
	elseif($booking->status == DataType::$BOOKING_REJECT){
		
		$progress[] = 100;
		$status[] = array(
			'status' => 'REJECTED',
			'date' => Util::ToShortDate($res_date),
			'class' => 'ils-close'
			);
		
	}
	elseif($booking->status == DataType::$BOOKING_CONFIRM){
		
		$progress[] = 100;
		$progress[] = 100;
		
		$status[] = array(
			'status' => 'ACCEPTED',
			'date' => Util::ToShortDate($res_date),
			'class' => 'ils-complete'
			);

		$status[] = array(
				'status' => 'CONFIRMED',
				'date' => Util::ToShortDate($con_date),
				'class' => 'ils-complete'
				);
		
		$delat = time() - strtotime($con_date);
		$total = strtotime($booking->session_start) - strtotime($con_date);
		$percent = round(($delat/$total)*100);		
				
		$progress[] = $percent > 100 ? 100 : $percent;	
		$status[] = array(
			'status' => 'COMPLETE',
			'date' => Util::ToShortDate($booking->session_start),
			'class' => '',
			);
		
	}
	elseif ( $booking->status == DataType::$BOOKING_COMPLETE){
		
		
		$progress[] = 100;
		$progress[] = 100;
		$progress[] = 100;
		
		$status[] = array(
			'status' => 'ACCEPTED',
			'date' => Util::ToShortDate($res_date),
			'class' => 'ils-complete'
			);
			
		$status[] = array(
				'status' => 'CONFIRMED',
				'date' => Util::ToShortDate($con_date),
				'class' => 'ils-complete'
				);
		
		$status[] = array(
			'status' => 'COMPLETED',
			'date' => Util::ToShortDate($booking->session_start),
			'class' => '',
			);
		
	}
	elseif ($booking->status == DataType::$BOOKING_CANCEL ) {
	
		if(!empty($res_date)){
			
			$progress[] = 100;
			$status[] = array(
				'status' => 'ACCEPTED',
				'date' => Util::ToShortDate($res_date),
				'class' => 'ils-complete'
				);				
		}
		
		if(!empty($con_date)){
			
			$progress[] = 100;
			$status[] = array(
				'status' => 'CONFIRMED',
				'date' => Util::ToShortDate($con_date),
				'class' => 'ils-complete'
				);				
		}
		
		$progress[] = 100;
		$status[] = array(
			'status' => 'CANCELED',
			'date' => Util::ToShortDate($booking->status_changed),
			'class' => 'ils-close'
			);
			
	}
	 
?>
<div class="status-illustrator">
<?php

foreach ($status as $i => $s){
	
	echo '<div class="status-box">
    <div class="status-name">'.$s['status'].'</div>
    <div class="status-wrapper '.$s['class'].'"></div>
    <div class="status-date">';
	
	echo (!empty($s['date'])) ? $s['date'] :'';

    echo '</div></div>';
	
	if(isset($progress[$i])){
		echo '<div class="transition-box">
    <div class="transition-progress" style="width:'.$progress[$i].'%"></div>
  </div>';	
	}
	
}


?>

</div>
<?php
}

?>
</div>