<div class="txt-l">Select a package</div>
<div class="sep"></div>
<div class="panel min-info">
  <?php

$timepicker = false;

if(!empty($packages) && !empty($packages['packages'])){
	
	foreach ($packages['packages'] as $package) {
?>
  <div class="min-info-row packages" id="pkg<?php echo $package['id'] ?>">
    <div class="flex">
      <div class="resizable">
        <div class="txt-l txt-bold txt-blue"><?php echo $package['name']; ?></div>
        <div class=""><?php echo $package['skill_name']; ?></div>
      </div>
      <div class="right">
        <div class="txt-m txt-bold"><?php echo currency($package['fee']); ?></div>
        <div class=""> <?php echo Util::ToTime($package['timespan']); ?></div>
      </div>
    </div>
    <div class="sep"></div>
    <div class="txt-gray">
      <?php 
	
	if(strlen($package['description']) > 200) {
		
		echo nl2br(substr($package['description'],0,200));
		
		echo '<span class="hidden" id="desc_'.$package['id'].'">'.nl2br(substr($package['description'],201)).'</span> <a cura="expand_text" data-id="desc_'.$package['id'].'">view more...</a>';
	}
	else{
		echo nl2br($package['description']);
	}
	
	 ?>
    </div>
    <div class="sep"></div>
    <div class="columns">
      <?php 
	  
	  if(!empty($package['available'])){
		  echo '<div class="resizable"></div><div><a class="button" cura="select_package" data-package="'.$package['id'].'">SELECT PACKAGE</a></div>';
	  }
	  else {
		  
		  $slots = ($package['timespan']%15 == 0) ? $package['timespan']/15 : ceil($package['timespan']/15) ;
		  
		  echo '<div class="resizable">This package is not available at '.$engine->getTime(true).'.</div><div>';
		  
		  if(!empty($package['alt'])){
		  
		 	 echo '<div class="gap1"></div><a class="more-link" data-id="'.$package['id'].'" data-slots="'.$slots.'" cura="package_availability" href="javascript:void(0)">Check available times</a>';
		 	 $timepicker = true;
		  }
		  echo '</div>';
	  }
	  
	  ?>
    </div>
  </div>
  <?php	
	}
	
}

?>
</div>
<?php

if($timepicker === true) {

$schedule = $engine->timeline->getSchedule();	
	
	?>
<div id="sliding_timepicker" class="hidden">
  <div class="sep"></div>
  <a href="javascript:void(0)" cura="package_all">View all packages</a>
  <div class="sep"></div>
  <div class="sep"></div>
  <div>Slide to select a time</div>
  <div class="panel padding">
    <div class="timeslider" cura="timeslide" data-type="load">
      <div class="timebar">
        <?php 
	
	
	foreach ($schedule as $i => $s){
		echo '<div id="s'.$i.'" class="'.($s[2] == 0 ? 'free' : 'busy').'" ></div>';
	}
	
	?>
      </div>
      <div class="datebar">
        <?php 
	
	
	$c = 0;
	foreach ($schedule as $i => $s){
		
		
		
		if($i == 0 || $d != $s[0] ){
			$d = $s[0];
			$c = 0;
			echo '<div class="sdate">'.$d.'</div>';
		}
		elseif(strpos($s[1],':00') !== false  ){
			$t = explode(':',$s[1]);
			
			if(($t[0]%2) == 0 && $c > 3) {
				echo '<div class="stime" >'.$s[1].'</div>';
			}
			else{
				echo '<div ></div>';	
			}
		}
		else{
			echo '<div ></div>';	
		}
		
		$c++;
		//echo '<div id="s'.$i.'" class="'.($s[2] == 0 ? 'free' : 'busy').'" ></div>';
	}
	
	?>
      </div>
      <div class="slidingbox">
        <div class="slider"></div>
        <div class="timebox">
          <div class="timedisplay" id="timedisplay"></div>
          <div class="selectbtn"> <a cura="select_package_time" class="button" id="timeslider_btn">Select</a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php


$packages = Util::groupArray($packages, 'id');

echo '<script>
var schedule = '.json_encode($schedule).';
var position = 0;
var slotwidth = 2;
var package_id = null;
var needle = 0;
</script>';
}
?>
<div class="sep"></div>
<div class="sep"></div>
<a href="javascript:void(0)"  cura="change_booking">Change  Date/Time</a>
