<?php
$profile = Session::getProfile();
$customer = Session::getCustomer();

$info = $booking->getInfo();

$stack = Util::makeArray( $info[ 'location' ][ 'venue' ], $info[ 'location' ][ 'address' ], $info[ 'location' ][ 'city' ], $info[ 'location' ][ 'region' ] );

$datatime = Util::ToDate( $info[ 'booking' ][ 'session_start' ], 1 );

$changes = $booking->getChangeRequests();

?>
<div>
<div class="panel">
  <?php

  if ( $booking->status == DataType::$BOOKING_PENDING && $booking->isRecipient() ) {
    // booking is pending, let talent/agency add expences and accept it
    echo '<div class="padding bg-yellow action-required">Please review and ACCEPT/DECLINE. Due on <span class="txt-bold">' . Util::ToDate( $info[ 'booking' ][ 'due_date' ], true ) . '</span></div><div class="dash-sep"></div>';
  } elseif ( $booking->status == DataType::$BOOKING_ACCEPTED && $booking->isSender() ) {
    echo '<div class="padding bg-yellow action-required">Please review and CONFIRM/CANCEL. Due on <span class="txt-bold">' . Util::ToDate( $info[ 'booking' ][ 'due_date' ], true ) . '</span></div><div class="dash-sep"></div>';
  }
  ?>
  <!------------------- Change request --------------->
  <?php

  if ( !empty( $changes ) ) {

    $pendings = array();

    foreach ( $changes as $change ) {

      if ( $change[ 'status' ] == 'p' && $change[ 'created_by' ] != $customer[ 'id' ] ) {
        $pendings[] = $change;
      }
    }

    if ( !empty( $pendings ) ) {
      component( 'booking/change_request', array( 'info' => $info, 'changes' => $pendings, 'customer' => $customer ) );
    }

  }
  ?>
  <!------------------- Change request ends ---------------> 
  <!-------------- Booking action -------------------->
  <?php

  if ( $booking->status == DataType::$BOOKING_PENDING && $booking->isRecipient() ) {
    // booking is pending, let talent/agency add expences and accept it
    ?>
  <div class="section bg-blue padding">
    <form cura="booking_accept" id="booking_accept">
      <input type="hidden" name="booking" value="<?php echo $booking->id; ?>" >
      <div class="txt-m txt-bold">RESPOND TO BOOKING REQUEST</div>
		<?php if($booking->free_quota == 1) { ?> 
		<div class="txt-green">Free Allowance</div>
		<?php } ?>
		<div></div>
      <div class="dash-body-standalong">
        <div class="block-header txt-orange">Charges</div>
        <div class="info-body max-600 ">
          <?php
          component( 'booking/expences', array( 'info' => $info ) );
          ?>
        </div>
      </div>
      <div class="dash-body-standalong">
        <div class="block-header txt-orange">Discount</div>
        <div class="info-body" >
          <div class="field-wrapper nopadding">
            <label>You may give a discount for this booking (<?php echo Session::currencyCode() ?>)</label>
            <br>
            <input type="text" class="input-short" name="discount" id="discount" max="<?php echo toCurrency($info['booking']['talent_fee']);   ?>" cura="recalculate_booking_total" data-type="keyup">
          </div>
        </div>
        <div class="sep"></div>
      </div>
      <div class="">
        <div class="block-header txt-orange">Staff/Assistants</div>
        <div class="info-body" >
          <div class="field-wrapper nopadding">
            <label>How many additional staff members/assistants participating?</label>
            <br>
            <input type="text" class="input-short" max="3" name="additional_people" id="additional_people">
            <div class="anote">Leave blank if none.</div>
          </div>
        </div>
        <div class="sep"></div>
      </div>
      <div class="button-container"> <a href="javascript:void(0)" cura="view_my_appointments" data-filter_date="<?php echo $datatime[0]; ?>">View my schedule</a>
        <div class="center"> </div>
        <div class="button-wrapper"> <a class="button button-act" cura="booking_accept">ACCEPT BOOKING</a>
          <div class="gap"></div>
          <a class="button btn-next button-alt" cura="booking_reject" data-id="<?php echo $booking->id; ?>">DECLINE</a> </div>
      </div>
    </form>
  </div>
  <div class="dash-sep"></div>
	<div id="appointments" class="bg-white hidden panel"></div>
  <?php
  } elseif ( $booking->status == DataType::$BOOKING_ACCEPTED && $booking->isSender() ) {
      // talent or agency accepted the booking, let customer confirm it
      ?>
  <div class="section bg-blue padding">
    <form cura="booking_confirm" id="booking_confirm">
      <input type="hidden" name="booking" value="<?php echo $booking->id; ?>" >
      <div class="txt-m txt-bold">CONFIRM BOOKING</div>
      <div class="dash-body-standalong">
        <div class="block-header txt-orange">Charges</div>
        <div class="info-body max-600 ">
          <?php
          component( 'booking/expences', array( 'info' => $info ) );
          ?>
        </div>
      </div>
      <div class="sep"></div>
      <div class="sep"></div>
      <div >
        <label class="custom-checkbox">
          <input type="checkbox" name="booking_conditions" id="booking_conditions" value="1" >
          I accept  booking <a>conditions and terms</a> <span class="checkmark"></span> </label>
      </div>
      <label class="error" id="booking_conditions_error" style="display:none">Please accept booking conditions and terms</label>
      <div class="sep"></div>
      <div class="sep"></div>
      <div class="button-container"> <a cura="message_compose" data-scroll="1" >Send a message</a> &nbsp;to <?php echo $booking->getRecipientName($info); ?>
        <div class="center"> </div>
        <div class="button-wrapper"> <a class="button button-act" cura="booking_confirm" data-id="<?php echo $booking->id; ?>">CONFIRM BOOKING</a>
          <div class="gap"></div>
          <a class="button btn-next button-alt" cura="booking_cancel" data-id="<?php echo $booking->id; ?>">CANCEL BOOKING</a> </div>
      </div>
    </form>
  </div>
  <div class="dash-sep"></div>
  <div class="sep"></div>
  <?php } ?>
  <!----------------  Booking actions ends -------------------> 
  <!------------- booking header ------------------->
  <div class="columns section padding">
	  
	  
    <div class="resizable">
		<div id="booking_name" class="user-header-name is-relative">
      <div class="inline-contents"><?php echo $info['booking']['name'] ?></div>
      <a class="inline-editor" cura="inline_edit" data-action="booking_name" data-id="<?php echo $booking->id; ?>">Edit</a> </div>
      
    </div>
    <input type="hidden" name="view_id" value="">
    <span class="middle right-content">
    <div class="center">
      <label class="status-bubble <?php echo DataType::$BOOKING_CLASSES[$info['booking']['status']]; ?>"><?php echo DataType::$BOOKING_STATUS[$info['booking']['status']]; ?></label>
      <?php

      if ( $booking->status == DataType::$BOOKING_PENDING || $booking->status == DataType::$BOOKING_ACCEPTED || DataType::$BOOKING_ACCEPTED == DataType::$BOOKING_PAYMENT ) {
        echo ' <div class="sep"></div>
      <div class="txt-s"><span class="txt-bold txt-gray">DUE:</span> ' . Util::ToShortDate( $info[ 'booking' ][ 'due_date' ], true ) . '</div>';

      }
		else{
			echo '<div class="sep"></div>';
		}

      ?>
		<?php if($booking->free_quota == 1) { ?> 
		<div class="txt-green">Free Allowance</div>
		<?php } ?>
    </div>
    </span> </div>
  <div class="section">
    
  </div>
  <div class="sep"></div>
  <div class="section">
    <?php
    component( 'booking/illustrator', array( 'booking' => $booking ) )
    ?>
  </div>
  <div class="sep"></div>
  <div class="section">
    <div class="columns">
		<div class="column-31 flex nopadding">
		 <div class="col2">
        <div class="block-header">Date/Time</div>
        <div class="info-body"> <?php echo $datatime[0]; ?>
          <div class="txt-light"><?php echo $datatime[1]; ?></div>
        </div>
      </div>
		<div class="col2">
        <div class="block-header">Place</div>
        <div class="info-body">
          <div><?php echo array_shift($stack); ?></div>
          <div class="txt-light"><?php echo implode(", ", $stack); ?></div>
          <div class="txt-light"><?php echo DataType::$countries[$info['location']['country']][0]; ?></div>
        </div>
      </div>	
			
		</div>     
      
      <div class="column-3 nopadding">
        <div class="block-header">Package</div>
        <div class="info-body"> <?php echo $info['package']['name']; ?>
          <div class="txt-light"><?php echo currency($info['package']['fee'], $info['talent']['currency']); ?></div>
          <div class="txt-light"><?php echo Util::ToTime($info['package']['timespan']); ?></div>
        </div>
      </div>
    </div>
  </div>
  <!------------- booking header ends ------------------->

  <div class="sep"></div>
  <div class="section">
    <div class="block-header">Notes</div>
    <div id="booking_notes" class="is-relative max-600" >
      <div class="inline-contents">
        <?php

        if ( $booking->isRecipient() ) {

          echo!empty( $info[ 'booking' ][ 'talent_note' ] ) ? nl2br( $info[ 'booking' ][ 'talent_note' ] ) : '<span class="txt-gray">Add your notes</span>';
        } elseif ( $booking->isSender() ) {

          echo!empty( $info[ 'booking' ][ 'customer_note' ] ) ? nl2br( $info[ 'booking' ][ 'customer_note' ] ) : '<span  class="txt-gray">Add your notes</span>';
        }

        ?>
      </div>
      <a class="inline-editor" cura="inline_edit" data-action="booking_note" data-id="<?php echo $booking->id; ?>">Edit</a> </div>
  </div>
  <div class="sep"></div>
  <div class="sep"></div>
  <?php if($booking->isCustomerCreated() && !$booking->isSender() ){   ?>
  <div class="section">
    <div class="block-header">Client</div>
    <div class="dash-info-row nolink">
      <div class="dash-info-top">
        <div class="dash-info-pic"> <img src="<?php echo fixImage($info['customer']['thumb'], $info['customer']['gender']); ?>"> </div>
        <div class="dash-info-left middle">
          <div class="dash-info-name txt-blue"><?php echo $info['customer']['firstname'].' '.$info['customer']['lastname']; ?></div>
          <div class="dash-info-place"><?php echo ucwords(Util::makeString($info['customer']['city'], $info['customer']['region'], DataType::$countries[$info['customer']['country']][0])); ?></div>
        </div>
        <div class="dash-info-right"> </div>
      </div>
      <div class="sep"></div>
    </div>
  </div>
  <?php } ?>
  <?php if(($booking->isAgencyCreated() && !$booking->isSender()) || (!$booking->isEmpty('agency_id') && $booking->isCustomerCreated() && $booking->isSender()) ){   ?>
  <div class="section">
    <div class="block-header">Agency</div>
    <div class="dash-info-row nolink">
      <div class="dash-info-top">
        <div class="dash-info-pic"> <img src="<?php echo fixImage($info['agency']['thumb'], NULL, 1); ?>"> </div>
        <div class="dash-info-left middle">
          <div class="dash-info-name txt-blue"><?php echo $info['agency']['name']; ?></div>
          <div class="dash-info-place"><?php echo $info['agency']['type_name']; ?></div>
          <div class="dash-info-place"><?php echo ucwords( Util::makeString($info['agency']['city'], $info['agency']['region'], DataType::$countries[$info['agency']['country']][0])); ?></div>
        </div>
        <div class="dash-info-right"> </div>
      </div>
      <div class="sep"></div>
    </div>
  </div>
  <?php } ?>
  <?php if($booking->isTalentRecipient() && !$booking->isRecipient() ){   ?>
  <div class="section">
    <div class="block-header">Talent</div>
    <div class="dash-info-row nolink">
      <div class="dash-info-top">
        <div class="dash-info-pic"> <img src="<?php echo fixImage($info['talent']['thumb'], NULL, 1); ?>"> </div>
        <div class="dash-info-left middle">
          <div class="dash-info-name txt-blue"><?php echo $info['talent']['name']; ?></div>
          <div class="dash-info-place"><?php echo $info['talent']['skill_name']; ?></div>
          <div class="dash-info-place"><?php echo ucwords(Util::makeString($info['talent']['city'], $info['talent']['region'], DataType::$countries[$info['talent']['country']][0])); ?></div>
        </div>
        <div class="dash-info-right"> </div>
      </div>
      <div class="sep"></div>
    </div>
  </div>
  <?php } ?>
  <div class="section">
    <div class="button-container">
      <div class="center"> </div>
      <div class="button-wrapper">
        <?php  if($booking->status != DataType::$BOOKING_PENDING ||  !$booking->isRecipient() ) { ?>
        <a href="javascript:void(0)" cura="booking_change" data-id="<?php echo $booking->id; ?>" >Change Booking</a>
        <?php } ?>
        <div class="gap"></div>
        <a class="more-link right" cura="history">Back</a> </div>
    </div>
  </div>
</div>
</div>
<div class="sep"></div>
<!-------------------------------- Change requests --------------------->
<?php
if ( !empty( $changes ) ) {

  echo '<div>
  <div class="panel">
    <div class="dash-header-wrapper section padding">
      <div class="dash-sub-header txt-orange resizable">Change Requests</div>
    </div>
    <div  ><div class="box-row txt-bold txt-light-gray desktop-only">
          <div class="box-cell cell-width-40">&nbsp;</div>
          <div class="box-cell cell-width-20">Created</div>
          <div class="box-cell cell-width-20">Responded</div>
		  <div class="box-cell cell-width-20">Status</div>
        </div>';

  $mapping = array(
    'p' => array( 'PENDING', 'status-pending' ),
    'a' => array( 'ACCEPTED', 'status-confirmed' ),
    'r' => array( 'DECLINED', 'status-reject' ),
  );

  foreach ( $changes as $change ) {

    // find name

    if ( $change[ 'created_by' ] == $customer[ 'id' ] ) {
      $sender = 'You';
    } else {
      if ( $info[ 'talent' ][ 'customer_id' ] == $change[ 'created_by' ] ) {
        $sender = $info[ 'talent' ][ 'name' ];
      } elseif ( $info[ 'agency' ][ 'customer_id' ] == $change[ 'created_by' ] ) {
        $sender = $info[ 'agency' ][ 'name' ];
      }
      elseif ( $info[ 'customer' ][ 'id' ] == $change[ 'created_by' ] ) {
        $sender = $info[ 'customer' ][ 'firstname' ];
      }
    }
    ?>
<div class="box-row linked" cura="booking_change_list" data-id="<?php echo $booking->id ?>" >
  <div class="box-cell cell-width-40">
	  
    <div>

      <?php
      if ( !empty( $change[ 'is_cancel' ] ) ) {
        echo "Cancellation request";
      } else {
        echo ucwords( Util::getChangeRequestDesc( $change ) );
      }
      ?>
    </div>
    <div class="txt-light-gray txt-s"><?php echo $sender; ?></div>
  </div>
  <div class="box-cell cell-width-20 txt-s"><?php echo Util::ToDate($change['created'], true); ?></div>
  <div class="box-cell cell-width-20 txt-s"><?php echo !empty($change['approved_date']) ? Util::ToDate($change['approved_date'], true) : ''; ?></div>
  <div class="box-cell cell-width-20 <?php echo $mapping[$change['status']][1]; ?>">
    <label class="row-status"><?php echo $mapping[$change['status']][0]; ?></label>
  </div>
</div>
<?php
}

echo '<div class="sep2"></div>
    </div>
  </div>
</div>
<div class="sep"></div>';

}


?>
<!---------------------------------------- Expences ----------------------->
<?php if(!(($booking->status ==  DataType::$BOOKING_PENDING && $booking->isRecipient()) || ($booking->status ==  DataType::$BOOKING_ACCEPTED && $booking->isSender())) ) { ?>
<div>
  <div class="panel">
    <div class="dash-header-wrapper section padding">
      <div class="dash-sub-header txt-orange resizable">Expences</div>
    </div>
    <div class="section">
      <?php
      component( 'booking/expences', array( 'info' => $info ) );
      ?>
      <div class="sep2"></div>
    </div>
  </div>
</div>
<div class="sep"></div>
<?php } ?>
<!----------------------------- Messages -------------------------->
<?php

if ( $booking->isRecipient() || $booking->isSender() && ( !empty( $info[ 'booking' ][ 'messages' ] ) || $booking->status != DataType::$BOOKING_PENDING ) ) {
  $board = $booking->getMessageBoard();
  ?>
<div>
  <div class="panel">
    <div class="dash-header-wrapper section padding" id="message-panel">
      <div class="dash-sub-header txt-orange resizable">Message Board</div>
      <span class="middle "><a class="button" cura="message_compose">Send a message</a> </span> </div>
    <div class="section">
      <div id="message_editor" >
        <div class="inline-editor-panel  strip-light-gray">
          <div>
            <textarea id="message-field"></textarea>
          </div>
          <div class="button-container">
            <div class="center"> </div>
			  <div class="button-wrapper">
			  <a class="button" cura="message_send"  data-id="<?php echo $booking->id; ?>">Send</a>
            <div class="gap"></div>
            <a class="button btn-next button-alt" cura="message_cancel" >Cancel</a> 
			  </div>
            </div>
        </div>
        <div class="sep"></div>
        <div class="sep"></div>
      </div>
    </div>
    <div id="booking_msg_board" data-id="<?php echo $booking->id; ?>" data-board="<?php echo $board->id; ?>">
      <?php component('booking/message_board', array('booking' => $booking)); ?>
    </div>
  </div>
</div>
<?php } ?>
<script>

var expence_types = <?php echo json_encode(DataType::$EXPENCES_TYPE); ?>;
var currency = '<?php echo Session::currencyCode(); ?>';

</script>
<?php
//Util::debug($info);

?>
