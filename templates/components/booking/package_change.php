<?php
$selected = NULL;
$filtered = array();

	foreach ($packages as $p){
		
		if($p->id == $booking->package_id){
			$selected = $p;
			break;	
		}
			
	}

	foreach ($packages as $p){
		
		if($p->skill_id == $selected->skill_id  && $p->id != $selected->id){
			$filtered[] = $p;	
		}
	}


?>
<div class="selection-wrapper">
  <div class="sep"></div>
  <div class="panel">
    <?php
	
	if(!empty($filtered)) {
	
		foreach ($filtered as $pak){
					
			echo '<label class="list-select" >
			<input type="radio" name="suggested_package" value="'.$pak->id.'" >	<div class="resizable">'.$pak->name.' '.($booking->suggested_package == $pak->id ? ' (Previous Offer)' : '').'</div>
		<div class="">'.currency($pak->fee, $booking->currency).'</div>
	</label>';
	
		}
		
	}
	else{
		echo '<div class="padding">No similer packages found.</div>';	
	}
?>
  </div>
  <div class="sep"></div>
  <div class="button-container">
    <div class="center"></div>
    <a class="button btn-next" cura="package_offer">UPDATE</a>
    <div class="gap"></div>
    <a class="button btn-next button-alt" cura="package_offer_cancel">CANCEL</a> </div>
</div>
