<?php

if(!empty($changes)) {
	
	foreach ($changes as $change) {

		// find names and ids		 
		 if($info['talent']['customer_id'] == $change['created_by']){
			$sender = $info['talent']['name'].' has '; 
			
			if(!empty($info['agency']['id'])){
				$receipient = $info['agency']['name'];
				$receipient_id = $info['agency']['customer_id'];
			}
			else{
				$receipient = $info['customer']['firstname'];
				$receipient_id = $info['customer']['id'];	
			}
			
		 }
		 elseif($info['agency']['customer_id'] == $change['created_by']){
			$sender = $info['agency']['name'].' has '; 
			
			if(!empty($info['talent']['id'])){
				$receipient = $info['talent']['name'];
				$receipient_id = $info['talent']['customer_id'];
			}
			else{
				$receipient = $info['customer']['firstname'];
				$receipient_id = $info['customer']['id'];	
			}
		 }
		 elseif($info['customer']['id'] == $change['created_by']){
			$sender = $info['customer']['firstname'].' has ';
			
			if(!empty($info['talent']['id'])){
				$receipient = $info['talent']['name'];
				$receipient_id = $info['talent']['customer_id'];
			}
			elseif(!empty($info['agency']['id'])){
				$receipient = $info['agency']['name'];
				$receipient_id = $info['agency']['customer_id'];
			} 
		 }
			 
			 
		if($change['created_by'] == $customer['id']){
			$sender = 'You have';			
		}
		
		if($receipient_id == $customer['id']){
			$receipient = "You";	
		}
		
		 
		if($receipient_id == $customer['id'] && $change['status'] == 'p') {
			echo '<div class="bg-blue">';
		}
		else{
			echo '<div>';
		}

?>

<div class="padding section">
  <div class="flex">
    <div class="dash-sub-header  nopadding resizable"><?php echo !empty($change['is_cancel']) ? 'Cancellation' : 'Change'; ?> Request</div>
    <div><?php echo Util::ToDate($change['created'], true);  ?></div>
  </div>
  <div class="sep"></div>
  <div><?php 
  
  if(!empty($change['is_cancel'])){
	  echo  $sender.' made a <span class="txt-bold">booking cancellation</span> request. If you want more information, you may contact via booking message board.';
  }
  else{
	  echo $sender.' made a booking change request to update the booking '. Util::getChangeRequestDesc($change).'.';
  }
   ?></div>
  <div class="sep"></div>
</div>
<div class="section">
  <?php 
  if(empty($change['is_cancel'])){
	  
 			 if(!empty($change['session_start'])){
					//$modified[] = "date/time";
					echo '<div class="block-header">Date/Time</div>';

					echo '<div class="strike">'.Util::ToDate($change['ex_start'], true).'</div>';
					echo '<div class="sep1"></div>';
					echo '<div class="txt-blue" >'.Util::ToDate($change['session_start'], true).'</div>';
					echo '<div class="sep2"></div>';
					
			 }
			 if(!empty($change['location_id'])){
					echo '<div class="block-header">Place</div>';

					echo '<div class="strike">'.$change['old_location'].'</div>';
					echo '<div class="sep1"></div>';
					echo '<div class="txt-blue" >'.$change['new_location'].'</div>';
					echo '<div class="sep2"></div>';
			 }
			 
			 if(!empty($change['package'])){
					echo '<div class="block-header">Package</div>';

					echo '<div class="strike">'.$change['old_package'].' - '.Util::ToTime($change['old_timespan']).' ('.currency($change['old_fee']).')</div>';
					echo '<div class="sep1"></div>';
					echo '<div  class="txt-blue" >'.$change['new_package'].' - '.Util::ToTime($change['new_timespan']).' ('.currency($change['new_fee']).')</div>';
					echo '<div class="sep2"></div>';
			 }
			 
			  if($change['discount'] != $change['ex_discount']){
				  echo '<div class="block-header">Discount</div>';
				echo '<div class="strike">'.currency($change['ex_discount']).'</div>';
					echo '<div class="sep1"></div>';
					echo '<div class="txt-blue" >'.currency($change['discount']).'</div>';
					echo '<div class="sep2"></div>';
			 }
	
  	}
  //	 Util::debug($change);
  
  ?>
</div>

<?php if($receipient_id == $customer['id'] && $change['status'] == 'p') { ?>
<div class="padding section">
  <div class="button-container">
    <div class="center"> </div>
    <?php if(!empty($change['is_cancel'])) { ?>
    	<a class="button" cura="booking_change_respond" data-id="<?php echo $change['id']; ?>" data-reply="accept" data-cancel="1">CANCEL BOOKING</a>
    <?php } else { ?>
    	<a class="button" cura="booking_change_respond" data-id="<?php echo $change['id']; ?>" data-reply="accept">ACCEPT</a>
    <?php } ?>
   
    <div class="gap"></div>
    <a class="button btn-next button-alt"  cura="booking_change_respond" data-id="<?php echo $change['id']; ?>" data-reply="decline" >DECLINE</a> </div>
    </div>
<?php } else { 

	echo '<div class="button-container bg-yellow padding section">';
	
	if($change['status'] == 'p'){
		echo $receipient." has not responded yet.";
	}
	else {
		echo $receipient.'&nbsp;<span class="txt-bold">'.($change['status'] == 'a' ? 'accepted' : 'declined' ). '</span>&nbsp;  this request on '.Util::ToDate($change['approved_date'], true);
	}
	
	echo '</div>';
 } ?>

</div>
<div class="dash-sep"></div>
<?php
	}
}

//Util::debug($change);
?>
