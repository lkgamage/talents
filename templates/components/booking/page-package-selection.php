<div class="strip strip-light-gray">
      <div class="sep"></div>
      <div class="sep"></div>
      <div class="container" id="booking" >
        <div class="panel padding bg-white">
          <div class="padding" id="booking-panel">
          
          <form id="booking-form" cura="review_booking" >
            <input type="hidden" name="talent_id" value="<?php echo !empty($talent) ? $talent->id : ''; ?>">
            <input type="hidden" name="agency_id" value="<?php echo !empty($agency) ? $agency->id : ''; ?>">
            <input type="hidden" name="address" class="_address" value="<?php echo Util::postValue('address') ?>">
            <input type="hidden" name="place" class="_place" value="<?php echo Util::postValue('place') ?>">
            <input type="hidden" name="postal" class="_postal" value="<?php echo Util::postValue('postal') ?>">
            <input type="hidden" name="country" class="_country" value="<?php echo Util::postValue('country') ?>">
            <input type="hidden" name="city" class="_city" value="<?php echo Util::postValue('city') ?>">
            <input type="hidden" name="region" class="_region" value="<?php echo Util::postValue('region') ?>">
            <?php
	$_data['no_cache'] = true;
	component('booking/package-selection', $_data);

?>
  </form>
  
          </div>
        </div>
      </div>
    </div>


