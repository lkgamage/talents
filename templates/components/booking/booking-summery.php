<div class="txt-l">Boooking Summery</div>
<div class="sep"></div>
<div class="sep"></div>
<div class="min-info max-600">
  <div class="min-info-row ">
    <div class="flex">
      <div class="resizable">
        <div class="txt-light-gray txt-bold">DATE</div>
      </div>
      <div class="txt-m"><?php echo $engine->getDate(true) ?></div>
    </div>
  </div>
  <div class="min-info-row ">
    <div class="flex">
      <div class="resizable">
        <div class="txt-light-gray txt-bold">TIME</div>
      </div>
      <div class="txt-m"><?php echo $engine->getTime(true); ?></div>
    </div>
  </div>
  <div class="min-info-row ">
    <div class="flex">
      <div class="resizable">
        <div class="txt-light-gray txt-bold">PLACE</div>
      </div>
      <div class="txt-m right"><?php echo $engine->getPlace(true); ?></div>
    </div>
  </div>
  <div class="min-info-row ">
    <div class="flex">
      <div class="resizable">
        <div class="txt-light-gray txt-bold">PACKAGE</div>
      </div>
      <div class="txt-m right">
        <?php 
	  
	  $package = $engine->data['package'];
	  
	 // if(!empty($package['id'])){
		 echo '<div>'.$package['skill_name'].' - '.$package['name'].'</div>'; 
		 echo '<div class="txt-m txt-light">'.Util::ToTime($package['timespan']).'</div>'; 
		 echo '<div>'.currency($package['fee']).'</div>'; 
	 // }
	  
	  
	  
	   ?>
      </div>
    </div>
  </div>
  <?php if(!empty($engine->data['event']['id'])){ ?>
  
   <div class="min-info-row ">
    <div class="flex">
      <div class="resizable">
        <div class="txt-light-gray txt-bold">EVENT</div>
      </div>
      <div >
	  <div class="txt-m"><?php echo $engine->data['event']['name']; ?></div>
      <div class="right txt-gray"><?php echo DataType::$EVENT_CATEGORIES[$engine->data['event']['event_type']][0]; ?></div>
      </div>
    </div>
  </div>
  
  <?php } ?>
</div>
<div class="sep"></div>
<div class="max-600">
<div class="sep"></div>
  <div>
    <label class="custom-checkbox">
      <input type="checkbox" name="conditions" id="conditions" value="1" cura="condition_changed" data-type="change" >
      I accept Curatalent <a href="<?php echo Util::mapURL('/support/booking-conditions'); ?>" target="_blank">booking condition &amp; terms.</a><span class="checkmark"></span> </label>
  </div>
  <div id="condition_error" class="txt-red hidden">Please accept conditions and terms</div>
  <div class="sep"></div>
  <div class="button-container "> <a href="javascript:void(0)"  cura="change_booking">Change  Booking</a>
    <div class="center"></div>
    <a class="button" cura="submit_booking">Submit Booking</a> </div>
</div>
<?php
//Util::debug($engine->data);
?>