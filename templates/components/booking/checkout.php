<?php
//Customer view
$info = $booking->getInfo();

// display title and other data
$stack = Util::makeArray($info['booking']['venue'], $info['location']['address'], $info['location']['city'], $info['location']['region']);

$timestamp = strtotime($info['booking']['session_start']);



?>

<div class="panel">
  <?php component('customer/booking-header', array('info' => $info)); ?>
  <div class="padding">
    <div class="columns">
      <div class="column-3">
        <div class="info-header">Package</div>
        <div class="info-body"> <?php echo $info['package']['name']; ?>
          <div class="txt-light"><?php echo currency($info['package']['fee'], $info['talent']['currency']); ?></div>
          <div class="txt-light"><?php echo Util::ToTime($info['package']['timespan']); ?></div>
        </div>
      </div>
      <div class="column-3">
        <div class="info-header">Date/Time</div>
        <div class="info-body"> <?php echo Util::toDate($info['booking']['session_start']); ?>
          <div class="txt-light"><?php echo date("h:ia",$timestamp); ?></div>
        </div>
      </div>
      <div class="column-3">
        <div class="info-header">Place</div>
        <div class="info-body">
          <div><?php echo array_shift($stack); ?></div>
          <div class="txt-light"><?php echo implode(", ", $stack); ?></div>
          <div class="txt-light"><?php echo DataType::$countries[$info['location']['country']][0]; ?></div>
        </div>
      </div>
    </div>
  </div>
  <div class="sep"></div>
  <div class="padding section max-600"> 
    
    <!--------- invoice ------>
    <div >
      <div class="block-header ">Invoice</div>
      <div><span class="txt-light">Invoice #:</span> <?php echo $info['invoice']['id']; ?></div>
      <div><span class="txt-light">Date:</span> <?php echo Util::ToDate($info['invoice']['created']); ?></div>
    </div>
    <div class="sep"></div>
    <div class="panel" >
      <div class="list-select">
        <div class="resizable"><?php echo $info['package']['name']; ?> <span class="min-info-desc">
          <?php 
	  		echo $info['package']['description'];
	  		//echo currency($info['package']['fee'], $info['booking']['currency']);

	  ?>
          </span> </div>
        <div><?php echo currency($info['invoice']['talent_fee'], $info['booking']['currency']); ?></div>
      </div>
      <?php
	
	if(!empty($info['agency']['id'])){
		
		echo '<div class="list-select">
      <div class="resizable">Agency Fee</div>
	  <span class="min-info-desc">'.$info['agency']['name'].'</span>
      <div>'.currency($info['invoice']['agency_fee'], $info['booking']['currency']).'</div></div>';
		
	}
	
	
	if(!empty($info['expences'])) {
		
		echo '<div class="list-select noborder">
      <div class="resizable">Other Expences</div>
      <div></div></div>';
    
		
	  foreach ($info['expences'] as $exp){
		
		echo '<div class="list-select txt-light noborder">';
		echo '<div class="resizable">';
		echo DataType::$BOOKING_EXPENCES[$exp['expence_type']];
		echo '<span class="min-info-desc">'.$exp['description'].'</span>';
		
		echo '</div>';
		echo '<div>'.currency($exp['amount'], $info['booking']['currency']).'</div>';
		echo '</div>';

		  
	  }
	  
	  echo '<div class="list-select">
      <div class="resizable">Total Other Expences</div>
      <div>'.currency($info['invoice']['expences'], $info['booking']['currency']).'</div></div>';
	  
	}
	
	/*
	echo '<div class="list-select">
      	<div class="resizable">Charges</div>
      <div>'.currency($info['invoice']['charges'], $info['booking']['currency']).'</div></div>';
	*/  
	 if(!empty($info['invoice']['discount'])){
		  echo '<div class="list-select">
			<div class="resizable">Discount</div>
		  <div>('.currency($info['invoice']['discount'], $info['booking']['currency']).')</div></div>';		
	 }
	 
	 echo '<div class="list-select bg-blue txt-bold">';
		echo '<div class="resizable">Total Amount</div>';
		echo '<div>'.currency($info['invoice']['amount'], $info['booking']['currency']).'</div>';
		echo '</div>';
	  
	?>
    </div>
    <!--------- invoice ------>
    <div class="vpadding txt-s">Invoice has been sent to your email. You may make payment arrangement with <?php echo $info['talent']['name']; ?>.</div>
    <div class="sep"></div>
    
    <!---------payment ---->
    <div class="panel strip-light-gray">
      <div class="txt-l padding center">Booking Charges</div>
      <div class="dash-sep"></div>
      <div>
      <label class="list-select strip-white"  for="payment_method_card" >
      <input type="radio" name="payment_method" id="payment_method_card" checked cura="switch_payment_methods" data-id="payment_cards" data-type="change" >
      <div class="resizable txt-bold">CREDIT/DEBIT CARDS</div>
      <div class="image-line"> <img src="<?php echo Util::mapURL('/images/card-visa.png'); ?>"> <img src="<?php echo Util::mapURL('/images/card-master.png'); ?>"> <img src="<?php echo Util::mapURL('/images/card-amex.png'); ?>"> </div>
      </label>
      <form id="payment_form" cura="card_submit"  >
        <div class="padding payment-methods" id="payment_cards">
          <div> Amount : <span class="txt-m txt-blue"><?php echo  currency($info['booking']['charges'], $info['booking']['currency']); ?></span> </div>
          <div class="sep"></div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="payment_card_name" >Card Holder Name</label>
            <input type="text" value="" name="payment_card_name" id="payment_card_name" maxlength="100">
          </div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="payment_card_num" >Card Number</label>
            <input type="text" value="" name="payment_card_num" id="payment_card_num" maxlength="24"  cura="set_card_type" data-type="keyup">
            <label style="display:none" id="payment_card_num-error" class="error" ></label>
          </div>
          <div class="flex">
            <div class="resizable" >
              <div class="field-wrapper">
                <label class="dynamic-label" for="payment_card_exp" >Card Exp. Date</label>
                <select class="input-tiny" name="card_exp_month" id="card_exp_month" >
                  <?php
	
	for($i = 1; $i <= 12; $i++){
		echo '<option value="'.($i < 10 ? '0'.$i : $i).'">'.($i < 10 ? '0'.$i : $i).'</option>';	
	}
	
	?>
                </select>
                /
                <select class="input-tiny" name="card_exp_year" id="card_exp_year" >
                  <?php
	$year = date('y');
	for($i = $year; $i <= $year+10; $i++){
		echo '<option value="'.$i.'">20'.$i.'</option>';	
	}
	
	?>
                </select>
              </div>
            </div>
            <div >
              <div class="field-wrapper">
                <label class="dynamic-label" for="payment_card_cvv" >CVV</label>
                <input class="input-tiny" type="text"  value="" name="payment_card_cvv" id="payment_card_cvv" maxlength="4">
              </div>
            </div>
          </div>
          <div class="button-container">
            <div><img style="display:block" src="<?php echo Util::mapURL('/images/secure-payment.png'); ?>"></div>
            <div class="center"> </div>
            <a class="button button-act txt-m" cura="card_submit" data-id="<?php echo $booking->id; ?>" data-invoice="<?php echo $info['invoice']['id']; ?>">PAY &amp; CONFIRM</a> </div>
        </div>
      </form>
      <label class="list-select strip-white noborder"  for="payment_method_bank" >
      <input type="radio" name="payment_method" id="payment_method_bank" cura="switch_payment_methods" data-id="payment_bank" data-type="change">
      <div class="resizable txt-bold">BANK DEPOSIT/ACH</div>
      <div class="image-line"> <img src="<?php echo Util::mapURL('/images/card-bank.png'); ?>"> <img src="<?php echo Util::mapURL('/images/icon-bank.png'); ?>"> </div>
      </label>
      <form cura="bank_submit" id="transfer_form">
        <div class="padding payment-methods" id="payment_bank" style="display:none">
          <div class="list-select">
            <div class="min-info-desc">BANK</div>
            <div class="resizable"> The Bank PLC<br>
              Colombo, SRI LANKA</div>
          </div>
          <div class="list-select">
            <div class="min-info-desc">A/C NAME</div>
            <div class="resizable txt-s"> TALENT MARKET PVT LTD</div>
          </div>
          <div class="list-select">
            <div class="min-info-desc">A/C NUMBER</div>
            <div class="resizable"> 1245 5487 5684</div>
          </div>
          <div class="list-select">
            <div class="min-info-desc">SWIFT</div>
            <div class="resizable txt-s"> CMBLKLX</div>
          </div>
          <div class="sep"></div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="payment_transaction_id" >Bank Transfer ID</label>
            <input type="text" value="" name="payment_transaction_id" id="payment_transaction_id" maxlength="100">
          </div>
          <div class="txt-s ">Enter bank transfre number given to you by the bank aftere you made wire/ACH/TT transfer.</div>
          <div class="button-container">
            <div class="center"> </div>
            <a class="button button-act txt-m" cura="bank_submit" data-id="<?php echo $booking->id; ?>" data-invoice="<?php echo $info['invoice']['id']; ?>" >SUBMIT</a> </div>
        </div>
      </form>
      </div>
    </div>
  </div>
    <div class="sep"></div>
  <p class="section "> When you complete the booking charges, we’ll confirm your booking with <?php echo $info['talent']['name']; ?>.  <br><br>
Message board will be open and your contact information (public) will be shared with <?php echo $info['talent']['name']; ?> to make further arrangements. 

</p>
  <div class="sep"></div>
</div>
<script>

var currency = '<?php echo $booking->currency; ?>';
</script> 