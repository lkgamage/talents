<?php
$total = $info['package']['fee'];
?><div class="bg-white panel">
  <div class="list-select expence" data-value="<?php echo toCurrency($info['package']['fee']); ?>">
    <div class="resizable"><?php echo $info['package']['name']; ?><span class="min-info-desc">Package</span></div>
    <div><?php echo currency($info['package']['fee']); ?></div>
  </div>
  <div  id="additional_exp" data-label="Ex: Travel, Accomodation or Meals">
    <?php
		  foreach ($info['expences'] as $exp){
			
			echo '<div class="list-select expence" data-value="'.toCurrency($exp['amount']).'">';
			echo '<div class="resizable">';
			echo DataType::$EXPENCES_TYPE[$exp['expence_type']];
			echo '<span class="min-info-desc">'.$exp['description'].'</span>';
			
			echo '</div>';
			echo '<div>'.currency($exp['amount']).'</div>';
			echo '</div>';

			$total +=  $exp['amount'];
		  }
		  /*
		  	echo '<div class="list-select bg-gray">';
			echo '<div class="resizable">Total additional expences</div>';
			echo '<div>'.currency($info['booking']['other_exp'], $info['booking']['currency']).'</div>';
			echo '</div>';*/
			
			if(!empty($info['booking']['discount']) && $info['booking']['discount'] != '0.00'){
				
				echo ' <div class="list-select expence discount-row" data-value="-'.toCurrency($info['booking']['discount']).'">
						<div class="resizable">Discount</div>
						<div id="discount_amount">'.currency($info['booking']['discount']*-1).'</div>
					  </div>';
				$total -= $info['booking']['discount'];	
			}
		  
		  ?>
  </div>
  <?php 
  $profile = Session::getProfile();
  //if owner and booking is pending
  if($info['booking']['status'] == DataType::$BOOKING_PENDING && ( ($profile['type'] == 't' && !empty($info['booking']['talent_id']) && $info['booking']['talent_id'] = $profile['id']) || ($profile['type'] == 'a' && empty($info['booking']['talent_id']) && $info['booking']['agency_id'] == $profile['id'])) ) {
	   ?>
  <div class="list-select total-row">
    <div class="resizable"><a  href="javascript:void(0)" cura="expence_add">Add an expence</a></div>
    <div></div>
  </div>
  <?php } ?>
  <div class="list-select bg-gray">
    <div class="resizable">Total Amount</div>
    <div id="total_expence"><?php echo currency($total); ?></div>
  </div>
</div>
