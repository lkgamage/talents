<div class="popup-message">
<div class="bg-orange padding">Sorry! You just missed it!</div>
<div class="padding">
	Someone has already confirmed a booking for<br><span class="txt-blue"><?php echo Util::postValue('b_date').' '.Util::postValue('b_time') ;  ?></span>.
    
    <div class="sep"></div>
    You may pick another available date/time.
    <div class="sep"></div>
</div>

      <div class="button-container hpadding">
        <div class="center"> </div>
        <a class="button" cura="refresh">CLOSE</a></div>

</div>