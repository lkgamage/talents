<?php
$info = $booking->getInfo();

$stack = Util::makeArray($info['location']['venue'], $info['location']['address'], $info['location']['city'], $info['location']['region']);

$datatime = Util::ToDate($info['booking']['session_start'], 1);

?>

<form cura="booking_update" id="booking_update">
  <input type="hidden" value="<?php echo !empty($booking) ? $booking->id : '' ?>" name="booking_id" id="booking_id" >
  <input type="hidden" value="" name="package_id" id="package_id" >
  <div class="panel">
    <div class="dash-header-wrapper section padding">
      <div class="resizable">
        <div class="dash-sub-header txt-orange ">Change Booking</div>
      </div>
      <input type="hidden" name="view_id" value="">
      <span class="middle left-content">
      <div class="center">
        <label class="status-bubble <?php echo DataType::$BOOKING_CLASSES[$info['booking']['status']]; ?>"><?php echo DataType::$BOOKING_STATUS[$info['booking']['status']]; ?></label>
      </div>
      </span> </div>
    <div class="section">
      <div id="booking_name" class="user-header-name "><?php echo $info['booking']['name'] ?> </div>
      <div class="sep"></div>
      <div class="field-wrapper">
        <div class="block-header">Date/Time</div>
        <div> <?php echo $datatime[0]." ".$datatime[1]; ?></div>
        <div><a href="javascript:void(0)" cura="reveal" data-id="booking_date_change">Change date/time</a></div>
        <div id="booking_date_change" class="hidden">
          <div class="sep"></div>
          <div class="flex max-600">
            <div class="col2">
              <input type="text" class="input-short datepick" name="date" id="date" maxlength="40" value="<?php echo $datatime[0] ?>">
            </div>
            <div class="col2">
              <input type="text" class="input-short timepick" name="time" id="time" maxlength="40" value="<?php echo $datatime[1] ?>">
            </div>
          </div>
        </div>
      </div>
      <div class="field-wrapper">
        <div class="block-header">Place</div>
        <div> <?php echo implode(", ", $stack); ?>
          <div class="txt-light"><?php echo DataType::$countries[$info['location']['country']][0]; ?></div>
        </div>
        <div><a href="javascript:void(0)" cura="reveal" data-id="booking_place_change">Change place</a></div>
        <div id="booking_place_change" class="hidden">
          <div class="sep"></div>
          <div class="max-600">
            <input type="text" class="addresspick" name="place" id="place" maxlength="200" placeholder="Enter a location or leave blank" data-place="<?php echo $booking->location_id; ?>">
          </div>
        </div>
      </div>
      <div class="field-wrapper">
        <div class="block-header">Package</div>
        <div> <?php echo $info['package']['name']; ?>
          <div class="txt-light"><?php echo currency($info['package']['fee'], $info['talent']['currency']).' / '. Util::ToTime($info['package']['timespan']); ?></div>
        </div>
        <div><a href="javascript:void(0)" cura="reveal" data-id="booking_package_change">Change package</a></div>
        <div id="booking_package_change" class="hidden">
          <div class="sep"></div>
          <div class="panel min-info">
            <?php
		
		if($booking->isTalentRecipient()){
			
			$object = $booking->getTalent();
		}
		else{
			$object = $booking->getAgency();
		}


$packages = $object->getPackages(true);

if(!empty($packages)){
	
	foreach ($packages as $package) {
?>
            <div class="min-info-row packages <?php echo ($package['id'] == $info['package']['id']) ? 'selected-package' : '' ?>" id="pkg<?php echo $package['id'] ?>">
              <div class="flex">
                <div class="resizable">
                  <div class="txt-l txt-bold txt-blue"><?php echo $package['name']; ?></div>
                  <div class=""><?php echo $package['skill_name']; ?></div>
                </div>
                <div class="right">
                  <div class="txt-m txt-bold"><?php echo currency($package['fee']); ?></div>
                  <div class=""> <?php echo Util::ToTime($package['timespan']); ?></div>
                </div>
              </div>
              <div class="sep"></div>
              <div class="columns">
                <?php
		
		if($package['id'] == $info['package']['id']){
			echo '<div class="resizable txt-orange">Selected Package</div><a cura="change_booking_package" data-id="'.$package['id'].'" class="button">SELECT</a>';	
		}
		else {
			echo '<div class="resizable"></div><a cura="change_booking_package" data-id="'.$package['id'].'" class="button">SELECT</a>';
		}
		?>
              </div>
            </div>
            <?php	
	}
	
}

?>
          </div>
        </div>
      </div>
      <?php if($booking->status != DataType::$BOOKING_PENDING) { ?>
      <?php if($booking->isRecipient()) { ?>
      <div class="field-wrapper">
        <label class="dynamic-label" for="booking_discount" >Discount (<?php echo Session::currencyCode() ?>)</label>
        <input  class="input-short" type="text" value="<?php echo !empty($booking) ? toCurrency($booking->discount) : '' ?>" name="booking_discount" id="booking_discount" maxlength="12">
      </div>
      <?php } ?>
      <div class="field-wrapper">
        <div>Reason for change the booking</div>
        <select name="reason" id="reason" class="input-short">
          <option value=""> - Please select - </option>
          <?php
	
		foreach (DataType::$BOOKING_CHANGE_REASONS as $id => $resaon) {
			echo '<option value="'.$id.'">'.$resaon.'</option>';
		}
	
	?>
        </select>
      </div>
      <?php }  ?>
      <div class="button-container"> <a href="javascript:void(0)" cura="booking_change_cancel">Cancel Booking</a>
        <div class="center"></div>
		  <div class="button-wrapper">
		  <a class="button" cura="booking_update">Update Booking</a>
        <div class="gap"></div>
        <a class="button button-alt" cura="history">Back</a>
		  </div>
         </div>
    </div>
  </div>
</form>
