<div class="hpadding">
  <form cura="payment_method_update" id="new_payment_method">
    <div class="field-wrapper">
      <label class="dynamic-label" for="payment_card_num" >Card Number</label>
      <input type="text" value="" name="payment_card_num" id="payment_card_num" maxlength="24"  cura="set_card_type" data-type="keyup">
      <label style="display:none" id="payment_card_num-error" class="error" ></label>
    </div>
    <div class="flex">
      <div class="resizable" >
        <div class="field-wrapper">
          <label class="dynamic-label" for="payment_card_exp" >Card Exp. Date</label>
          <select class="input-tiny" name="card_exp_month" id="card_exp_month" >
            <?php
	
	for($i = 1; $i <= 12; $i++){
		echo '<option value="'.($i < 10 ? '0'.$i : $i).'">'.($i < 10 ? '0'.$i : $i).'</option>';	
	}
	
	?>
          </select>
          /
          <select class="input-tiny" name="card_exp_year" id="card_exp_year" >
            <?php
	$year = date('y');
	for($i = $year; $i <= $year+10; $i++){
		echo '<option value="'.$i.'">20'.$i.'</option>';	
	}
	
	?>
          </select>
        </div>
      </div>
      <div >
        <div class="field-wrapper">
          <label class="dynamic-label" for="payment_card_cvv" >CVV</label>
          <input class="input-tiny" type="text"  value="" name="payment_card_cvv" id="payment_card_cvv" maxlength="4">
        </div>
      </div>
    </div>
    <label id="payment_card_cvv-error" class="error" for="payment_card_cvv" style="display:none">Please enter CVV</label>
  </form>
</div>
