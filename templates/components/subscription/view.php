<?php

$info = $category->getinfo();
$next = $category->nextSubscription();

$pending = $category->getPendingInvoices();


?>
<div class="dash-header-wrapper">
  <div class="dash-sub-header txt-orange notoppadding">Subscription</div>
</div>
<div>
  <?php
  if (!empty( $pending ) && empty($info['category']['deleted']) && empty( $info[ 'category' ][ 'package_id' ] )   ) {

    $inv = $pending[ 0 ]->getInfo();

    ?>
  <div class="panel ">
    <div class="padding">
      <div class="txt-l resizable"><?php echo $inv['skill']['name'].' - '.$inv['package']['name'] ?></div>
		<div class="sep"></div>
		<div class="">You chose this plan but have not activated yet. </div>
    </div>
	  <div class="dash-sep"></div>
	  <div class="padding">
      <div class="columns column-seperated">
        
         <div class="column-2 lpadding right-column">
          <div class="info-header">Activate</div>
          <a cura="invoice_pay" data-id="<?php echo  $inv[ 'invoice' ][ 'id' ] ?>" class="button" >Activate Subscription</a>
        </div>
        <div class="column-2 lpadding left-column">
          <div class="info-header">cancel</div>
          <?php
            echo '<a class="more-link" cura="subscription_cancel" data-id="' . $info[ 'category' ][ 'id' ] . '">Cancel subscription</a>';
          ?>
        </div>
		
      </div>
    </div>
  </div>
	<?php } elseif(empty( $info[ 'category' ][ 'package_id' ] )) { ?>
	
	<div class="panel padding">
		<div class="padding">
	 <div class="txt-l"><?php echo $info['skill']['name']; ?></div>
	<div class="txt-m txt-gray">Select a subscription plan</div>
	<?php
		$skill = new Skill();
		$skill->populate($info['skill']);
		$packages = $skill->getPackages();
		$_data['packages'] = $packages;			  
		component('subscription/packages', $_data);			  
	?></div>
		
		
		<?php
			if(empty($info['category']['deleted'])){
				 echo '<div class="dash-sep"></div><div class="padding"><a class="more-link" cura="subscription_cancel" data-id="' . $info[ 'category' ][ 'id' ] . '">Cancel subscription</a></div>';
			}													  
           
          ?>
	</div>

  <?php } else { ?>
  <div class="panel">
    <div class="padding">
      <div class="flex">
        <div class="txt-l resizable"><?php echo $info['skill']['name'].' - '.$info['package']['name'] ?></div>
        <div class=" upper lpadding">
          <?php

          if ( !empty( $info[ 'category' ][ 'deleted' ] ) ) {
            echo '<div class="txt-red">Canceled</div>';
          } elseif ( empty( $info[ 'category' ][ 'package_id' ] ) ) {
            echo '<div class="txt-red">Not Activated</div>';
          } elseif ( empty( $info[ 'category' ][ 'renewal' ] ) ) {

            $cdate = strtotime( $info[ 'category' ][ 'last_renewal' ] );
            $cdate = strtotime( "+" . $info[ 'package' ][ 'validity' ] . " days", $cdate );

            echo '<div class="txt-gray">canceled</div>';
            echo '<div class="txt-txt-gray">Ends on ' . Util::ToDate( date( 'Y-m-d', $cdate ) ) . '</div>';
          }
          else {
            echo '<div class="txt-green">Active</div>';
          }

          ?>
        </div>
      </div>
      <div class="txt-m txt-gray"><?php echo currency($info['package']['price']).'/'.($info['package']['validity'] == 365 ? 'Year':'Month')  ?></div>
    </div>
    <div class="padding">
      <div class="min-info">
        <div class="min-info-row flex">
          <div class="col2 upper txt-s txt-light-gray">booking credit</div>
          <div class="col2">
            <?php

            if ( $info[ 'subscription' ][ 'jobs' ] === '0' ) {
              echo "Unlimited";
            } else {
              echo $info[ 'subscription' ][ 'jobs' ];
            }

            echo "&nbsp;&nbsp;<span class=\"txt-gray\">(" . $info[ 'subscription' ][ 'used' ] . " used)</span>";

            ?>
          </div>
        </div>
        <div class="min-info-row flex">
          <div class="col2 upper txt-s  txt-light-gray">Billing Cycle</div>
          <div class="col2">
            <?php

            if ( !empty( $info[ 'subscription' ][ 'id' ] ) ) {
              echo Util::ToShortDate( $info[ 'subscription' ][ 'begin_date' ] ) . ' - ' . Util::ToShortDate( $info[ 'subscription' ][ 'exp_date' ] );
            } else {
              echo "N/A";
            }
            ?>
          </div>
        </div>
        <div class="min-info-row flex">
          <div class="col2 upper txt-s  txt-light-gray">Bookings At a Time</div>
          <div class="col2">
            <?php


            if ( $info[ 'package' ][ 'price' ] > 0 ) {

              echo '<span id="simultaneous-bookings">' . $info[ 'category' ][ 'simultaneous' ] . ( $info[ 'category' ][ 'simultaneous' ] > 1 ? " Bookings" : ' Booking' ) . '</span>';
              echo '<div class="sep"></div><a class="dash-link" cura="simultaneous_change" data-id="' . $info[ 'category' ][ 'id' ] . '" >Change</a>
				  ';
            } else {
              echo '1 Booking';
            }
            ?>
          </div>
        </div>
        <div class="inline_load" id="simultaneous-change"></div>
      </div>
    </div>
    <?php

    if ( !empty( $next ) ) {
      echo "<div class=\"padding bg-cream\">Plan will be changed to {$info['skill']['name']} - {$next['name']} from " . Util::ToDate( $next[ 'begin_date' ] ) . "<div><a cura=\"subscription_change_cancel\" data-id=\"{$category->id}\" class=\"dash-link\">Cancel</a></div></div>";
    }
    ?>
    <div class="dash-sep-"></div>
    <div class="padding">
      <div class="columns column-seperated">
        <div class="column-3 lpadding">
          <div class="info-header">Bookings</div>
          <a class="more-link">View bookings in this category</a> </div>
        <div class="column-3 lpadding">
          <div class="info-header">Change</div>
          <?php

          if ( !empty( $info[ 'category' ][ 'deleted' ] ) ) {
            echo '<div > Canceled on ' . Util::ToDate( $info[ 'category' ][ 'deleted' ] ) . '</div>';
          } elseif ( empty( $info[ 'category' ][ 'renewal' ] ) ) {

            $cdate = strtotime( $info[ 'category' ][ 'last_renewal' ] );
            $cdate = strtotime( "+" . $info[ 'package' ][ 'validity' ] . " days", $cdate );

            echo '<div class="txt-gray">Subscription schedule to be canceled</div>';
            echo '<div class="txt-txt-gray">Ends on ' . Util::ToDate( date( 'Y-m-d', $cdate ) ) . '</div>';
          }
          else {
            echo '<a class="more-link" cura="subscription_change" data-id="' . $info[ 'category' ][ 'id' ] . '">Change current plan</a>';
          }

          ?>
        </div>
        <div class="column-3 lpadding">
          <div class="info-header">cancel</div>
          <?php

          if ( !empty( $info[ 'category' ][ 'deleted' ] ) ) {
            echo '<div > Canceled on ' . Util::ToDate( $info[ 'category' ][ 'deleted' ] ) . '</div>';
          } elseif ( empty( $info[ 'category' ][ 'renewal' ] ) ) {

            $cdate = strtotime( $info[ 'category' ][ 'last_renewal' ] );
            $cdate = strtotime( "+" . $info[ 'package' ][ 'validity' ] . " days", $cdate );

            echo '<div class="txt-gray">Subscription schedule to be canceled</div>';
            echo '<div class="txt-txt-gray">Ends on ' . Util::ToDate( date( 'Y-m-d', $cdate ) ) . '</div>';
          }
          else {
            echo '<a class="more-link" cura="subscription_cancel" data-id="' . $info[ 'category' ][ 'id' ] . '">Cancel subscription</a>';
          }

          ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <div id="package-change" class="inline_load"></div>
  <div class="sep"></div>
  <div class="panel padding">
    <div class="section-header txt-black resizable">Payments</div>
    <div id="payments"  >
      <?php
      $uid = uniqid();
      $payments = $category->getPayments();


      if ( $payments->getTotal() > 0 ) {

        echo '<div id="' . $uid . '">';

        echo $payments->getPage();

        echo '</div>';
        ?>
      <div class="button-container">
        <div class="center"> <a class="button btn-next" id="more_<?php echo $uid ?>" data-label="VIEW MORE " > VIEW MORE </a> </div>
      </div>
      <div>
        <?php
        echo $payments->manualPaginate( $uid, 'more_' . $uid );
        ?>
      </div>
      <?php
      } else {
        echo '<div class="padding center">Payments found</div>';
      }

      //Util::debug( $bookings->_datasql);
      ?>
    </div>
  </div>
</div>
<?php
//Util::debug( $info );
?>
