<?php

$skill = $category->getSkill();
$current = $category->getPackage();
$packages = $skill->getPackages();
$_data['packages'] = $packages;
$_data['cura'] = 'subscription_package_update';
?>

<div class="sep"></div>
<div class="padding panel">
  <div class="txt-l center txt-bold">Choose your new subscription plan</div>

  <form id="subscription_package_update" cura="subscription_package_update" >
   
    <div>
      <?php
	  component('subscription/packages', $_data);
	  ?>
    </div>
  </form>
  <div class="sep"></div>
	<?php
	
	if(!empty($current) && $current->price > 0){
		echo '<div class="anote">* When you <span class="txt-bold">upgrade</span> or <span class="txt-bold">downgrade</span> your subscription plan, it will be switched to the new plan at the end of the current billing cycle.
</div> <div class="sep"></div>';
	}
	
	?>
  
	
 
       <div class="button-container">
          <div class="center"> </div>
<div class="button_wrapper">

          <a class="button btn-next button-alt" cura="inline_load_cancel" >Cancel</a></div> </div>
</div>
<?php
//Util::debug($current);
?>