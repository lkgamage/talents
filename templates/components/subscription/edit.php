<?php
$info = $subscription->getinfo();
$info[ 'subscription' ][ 'fees' ] = $info[ 'subscription' ][ 'fees' ] + 0;

?>
<div id="contents">
  <div class="panel ">
    <div id="subscription-message">
      <?php

      if ( !empty( $info[ 'subscription' ][ 'fees' ] ) && empty( $info[ 'reneval' ][ 'active' ] ) ) {

        if ( strtotime( $info[ 'reneval' ][ 'next_reneval' ] ) < time() ) {
          echo '<div class="bg-yellow padding">Subscription will be canceled on ' . Util::ToDate( $info[ 'reneval' ][ 'next_reneval' ] ) . '.</div>';
        } else {
          echo '<div class="bg-yellow padding">Subscription canceled on ' . Util::ToDate( $info[ 'reneval' ][ 'next_reneval' ] ) . '.</div>';
        }
      }


      ?>
    </div>
    <div class="padding ">
      <div >
        <div class="dash-header-wrapper">
          <div class="dash-sub-header txt-orange">Subscription</div>
			<?php if ( empty( $info[ 'subscription' ][ 'fees' ] ) ) { ?>
			<div><a class="button">Upgrade</a></div>
			<?php } ?>
        </div>
      </div>
      <div class="sep"></div>
      <div class="flex">
        <div class="col2">
          <div class="block-header txt-orange">plan</div>
          <div class="info-body">
            <div ><?php echo $info['package']['name']; ?></div>
            <div class="txt-light">
              <?php

              if ( empty( $info[ 'subscription' ][ 'fees' ] ) ) {
                echo 'Free';
              } elseif ( $info[ 'subscription' ][ 'term' ] == 'm' ) {
                echo currency( $info[ 'package' ][ 'price_monthly' ] ) . '/Month';
              }
              else {
                echo currency( $info[ 'package' ][ 'price_annualy' ] ) . '/Year';
              }


              ?>
            </div>
            <div class="sep"></div>
            <?php if(!empty($info['reneval']['active'])  ){ ?>
            <div><a cura="subscription_change" data-id="<?php echo $info['subscription']['id']; ?>" class="dash-link">Change Plan</a></div>
            <?php } ?>
          </div>
          <div class="inline_load" id="package-change">
            <?php

            if ( $info[ 'subscription' ][ 'package_id' ] != $info[ 'reneval' ][ 'package_id' ] ) {

              $package = new Package( $info[ 'reneval' ][ 'package_id' ] );

              if ( !empty( $info[ 'reneval' ][ 'active' ] ) ) {
                echo '<div class="sep"></div><div class="bg-blue panel padding ">Package will be switched to <span class="txt-bold txt-blue">' . $package->name . '</span> on next renewal date.';
                if ( $package->price_monthly > $info[ 'package' ][ 'price_monthly' ] ) {
                  echo '<div class="sep"></div><a cura="subscription_package" data-id="' . $package->id . '" data-category="' . $package->skill_id . '" >Switch immidiately.</a>';
                }

                echo '</div>';
              }

            }


            ?>
          </div>
        </div>
        <div class="col2">
          <div class="block-header txt-orange">Category</div>
          <div ><?php echo $info['skill']['name']; ?></div>
        </div>
      </div>
      <div class="sep"></div>
      <div class="flex">
        <div class="col2">
          <div class="block-header txt-orange">Renewal</div>
          <div class="info-body">
            <?php
            if ( empty( $info[ 'subscription' ][ 'fees' ] ) ) {
              echo '<div>' . Util::ToDate( $info[ 'subscription' ][ 'exp_date' ] ) . '</div>';
              echo ' <div class="txt-light"></div>';
            } elseif ( $info[ 'reneval' ][ 'active' ] == 1 ) {
              // active
              echo '<div class="txt-light">Next renewal date</div>';
              echo '<div>' . Util::ToDate( $info[ 'reneval' ][ 'next_reneval' ] ) . '</div>';
              echo ' <div class="sep"></div>';
              echo '<div><a class="dash-link"  cura="billing_cycle_change" data-id="' . $info[ 'subscription' ][ 'id' ] . '" >Change billing cycle</a></div>';

            }
            else {
              echo '<div>Canceled</div>';
              echo ' <div class="sep"></div>';
            }
            /*	else{
				// cancelled
				
				
				if(strtotime($info['reneval']['next_reneval']) > time()){
            		echo '<div><a class="dash-link" cura="subscription_activate" data-id="'.$info['subscription']['id'].'">Re-activate Subscription</a></div>';
				}
			}*/

            ?>
          </div>
          <div class="inline_load" id="billing-change">
            <?php

            if ( !empty( $info[ 'reneval' ][ 'term' ] ) && $info[ 'subscription' ][ 'term' ] != $info[ 'reneval' ][ 'term' ] ) {

              if ( $info[ 'reneval' ][ 'term' ] == 'm' ) {
                echo '<div class="sep"></div><div class="bg-blue panel padding ">You\'ll be billed monthly from next renewal date.</div>';
              } else {
                echo '<div class="sep"></div><div class="bg-blue panel padding ">You\'ll be billed anually from next renewal date.</div>';
              }

            }

            ?>
          </div>
        </div>
        <div class="col2">
          <div class="block-header txt-orange">Payment Method</div>
          <?php
          if ( empty( $info[ 'subscription' ][ 'fees' ] ) ) {
            echo ' <div class="">Not Available</div>';
          } else {
            ?>
          <div id="payment_method_name"><?php echo $info['reneval_method']['name']; ?></div>
          <div class="sep"></div>
          <?php if(!empty($info['reneval']['active'])  ){ ?>
          <div><a class="dash-link" id="subscription_paymentmethod" cura="subscription_paymentmethod" data-id="<?php echo $info['subscription']['id']; ?>">Change</a></div>
          <?php } ?>
          <div class="inline_load" id="payment-change"></div>
          <?php } ?>
        </div>
      </div>

		<div class="sep"></div>
      <div class="flex">
		  <div class="col2">
          <div class="block-header txt-orange">Max Booings At a Time</div>
          <div class="info-body">
            
			  <?php
			  if ( empty( $info[ 'subscription' ][ 'fees' ] ) ) {
				  echo '1 Booking at a time<div class="sep"></div><a class="button">Upgrade</a>';
			  }
			  else{
				  echo '<span id="simultaneous-bookings">'.$info['subscription']['simultaneous']. ($info['subscription']['simultaneous'] > 1  ? " Bookings" : ' Booking').'</span>';
				  echo '<div class="sep"></div><a class="dash-link" cura="simultaneous_change" data-id="'.$info['subscription']['id'].'" >Change</a>
				  <div class="inline_load" id="simultaneous-change"></div>';
			  }
			  
			  ?>
          </div>
        </div>
        
      </div>

      <div class="sep"></div>
		<div class="flex">
		  
        <div class="col2">
          <div class="block-header txt-orange">Usage</div>
          <div class="info-body">
            <?php
            echo '<div class="flex"><div class="txt-gray col2">BOOKINGS</div> <div></div>' . $info[ 'usage' ][ 'jobs' ] . ' of ' . $info[ 'subscription' ][ 'jobs' ] . '</div>';

            if ( !empty( $info[ 'subscription' ][ 'agency_id' ] ) ) {
              echo '<div class="sep"></div><div class="flex"><div class="txt-gray col2">MANAGE TALENTS</div> <div></div>' . $info[ 'usage' ][ 'manage' ] . ' of ' . $info[ 'subscription' ][ 'manage' ] . '</div>';
            }

            ?>
          </div>
        </div>
      </div>
      <div class="sep"></div>
      <div class="sep"></div>
      <div class="">
        <div class="block-header txt-orange">Payments</div>
        <div class="info-grid">
          <div class="columns min-info-row">
            <div class="col3">Date</div>
            <div class="col3">Method</div>
            <div class="col3 right">Amount</div>
          </div>
          <?php
          if ( empty( $info[ 'subscription' ][ 'fees' ] ) ) {

            echo '<div class="columns min-info-row txt-gray">
            <div class="col3">' . Util::ToDate( $info[ 'subscription' ][ 'created' ] ) . '</div>
            <div class="col3">N/A</div>
            <div class="col3 right">Free</div>
          </div>';
          } else {
            ?>
          <div class="columns min-info-row  txt-gray">
            <div class="col3"><?php echo'<div>'.Util::ToDate($info['payment']['created']).'</div>'; ?></div>
            <div class="col3"><?php echo strtoupper($info['payment']['card_type']).' - '.Util::decodeCardNumber($info['payment']['card_num'], $user->created) ?></div>
            <div class="col3  right"><?php echo currency($info['payment']['amount']); ?></div>
          </div>
          <div id="payment-history">
            <div class="sep"></div>
            <a class="dash-link" cura="payment_history" data-id="<?php echo $info['subscription']['id']; ?>">View payment history...</a> </div>
          <?php } ?>
          <div class="sep"> </div>
        </div>
      </div>
    </div>
    <div class="sep"></div>
    <div class="section">
      <div class="button-container">
        <?php if(empty($info['subscription']['fees']) || !empty($info['reneval']['active'])  ){ ?>
        <a href="javascript:void(0)" cura="subscription_cancel" data-id="<?php echo $info['subscription']['id']; ?>" >Cancel Subscription</a>
        <?php } ?>
        <div class="center"></div>
        <a class="button button-alt" cura="history" >Close</a> </div>
    </div>
  </div>
</div>
<?php
//Util::debug($info);
?>
