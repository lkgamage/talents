<?php
$info = $category->getinfo();
?>
<div class="sep"></div>
  <div class="panel padding">
    <div class="txt-l">Cancel Subscription</div>
	<div class="sep"></div>
	<div class=" ">You are about to cancel the subscription.</div>
	  <div class="sep"></div>
	  <?php
	  
	  if($info['package']['price'] > 0){
		  
		  echo '<div class=" ">Your subscription will be cancelled at the <span class="txt-red">end of the current billing cycle</span>. You may receive bookings until '.$info['subscription']['exp_date'].'.</div>';
		  
	  }
	  else{
		  echo '<div>Your subscription will close <span class="txt-red">immediately</span>.  You will not receive bookings in this category. </div>';
	  }
	  
	  ?>
	  <div class="sep"></div>
	<div class="">Are you sure you want to cancel this subscription?</div>
	      <div class="button-container">
          <div class="center"> </div>
<div class="button-wrapper">
          <a class="button" cura="subscription_close" data-id="<?php echo $category->id; ?>">Cancel Subscription</a>
          <div class="gap"></div>
          <a class="button btn-next button-alt" cura="inline_load_cancel" >Dismiss</a></div> </div>
</div>
<?php
//Util::debug($info);
?>