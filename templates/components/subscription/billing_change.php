<?php

$renewal = $subscription->getAutoReneval();

?>

<div class="sep"></div>
<div class="padding panel bg-gray">
  <div class="txt-m">Billing cycle change</div>
  <div class="sep"></div>
  <form id="billing_cycle_update" cura="billing_cycle_update" >
    <input type="hidden" name="id" value="<?php echo $subscription->id; ?>">
    
    <div class="options flex">
            <div class="col2">
              <label class="custom-checkbox">
                <input <?php echo ($renewal->term == 'm' ? 'checked=""' : ''); ?> type="radio" name="billing_cycle" id="billing_cycle_m" value="m"> Monthly<span class="checkmark"></span> </label>
            </div>
            <div class="col2">
              <label class="custom-checkbox">
                <input type="radio" <?php echo ($renewal->term == 'y' ? 'checked=""' : ''); ?> name="billing_cycle" id="billing_cycle_y" value="y">
               Annually<span class="checkmark"></span> </label>
            </div>
          </div>
  </form>
  <div class="sep"></div>
  <div class="button-container">
    <div class="center"></div>
    <a class="button btn-next " cura="billing_cycle_update">UPDATE</a>
    <div class="gap"></div>
    <a class="button btn-next button-alt " cura="inline_load_cancel">CANCEL</a> </div>

</div>
