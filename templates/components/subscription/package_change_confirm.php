<?php

$info = $category->getinfo();

?>
<div class="sep"></div>
<div class="padding panel">
  <div class="txt-l center txt-bold">Subscription Plan <?php echo $info['subscription']['jobs'] > $package->jobs ? 'Downgrade' : 'Upgrade' ?></div>
  <div class="sep2"></div>
  <div class="columns">
    <div class="column-2">
      <div class="section-header txt-gray">Current Plan</div>
      <div class="txt-m"><?php echo $info['skill']['name'].' - '. $info['package']['name'];  ?></div>
      <div class="txt-gray">
        <?php        
			echo $info[ 'subscription' ][ 'jobs' ].' Bookngs / ';
			echo $info[ 'package' ][ 'validity' ] == 365 ? 'Yearly' : 'Monthly';
        ?>
      </div>
		<div><?php echo currency($info['package']['price']); ?></div>
    </div>
    <div class="column-2">
      <div class="section-header ">New Palan</div>
      <div class="txt-m"><?php echo $info['skill']['name'].' - '. $package->name;  ?></div>
      <div class="txt-gray">
        <?php
		if ( empty($package->price*1)) {
			echo DataType::$FREE_BOOKINGS;
		}
		 else {
          echo $package->jobs;
        }

        echo ' Bookngs / ';

        echo $package->validity == 365 ? 'Yearly' : 'Monthly';
        ?>
      </div>
		<div><?php echo currency($package->price); ?></div>
    </div>
  </div>
  <div class="sep"></div>
  <div class="dash-sep"></div>
  <div class="sep2"></div>
  <?php

  $payable = $package->price ;

  if ( $info[ 'package' ][ 'price' ] < $package->price ) {
    // upgrade
    ?>
  <div class="txt-l">Your Invoice</div>
  <div class="sep"></div>
  <div class="max-600 panel">    
    <div class="box-row flex">
      <div class="resizable box-cell txt-gray">New plan price</div>
      <div class="box-cell"><?php echo currency($package->price); ?></div>
    </div>
    <div class="box-row flex bg-cream txt-bold">
      <div class="resizable box-cell ">Total Payable</div>
      <div class="box-cell"><?php echo currency($package->price ); ?></div>
    </div>
  </div>
	
	<div class="sep"></div>
	<?php  
	  echo '<div>You will be charged '.currency($package->price ).' from your next belling cycle, begins from '.Util::ToDate($info['subscription']['exp_date']).'.</div>';
	 
	  ?>
	
  <?php
  } elseif ( empty( $package->price * 1 ) ) {
    // switching to free package
    echo "<div>Your plan will be switched into <span class=\"txt-green txt-m\">{$info['skill']['name']} - {$package->name} </span> from <span class=\"txt-green txt-m\">" . Util::ToDate( $info[ 'subscription' ][ 'exp_date' ] ) . "</span>. <br>You may consume remaining booking credits until end of the current billing cycle.</div>";
  }
  else {
    // downgrade
     echo "<div>Your plan will be switched into <span class=\"txt-green txt-m\">{$info['skill']['name']} - {$package->name} </span> from <span class=\"txt-green txt-m\">" . Util::ToDate( $info[ 'subscription' ][ 'exp_date' ] ) . "</span>. <br>You may consume remaining booking credits until end of the current billing cycle.</div>";
	  
	  echo '<div class="sep"></div><div>You DO NOT have to pay now. Authorize your payment method to charge '.currency($package->price).' from '. Util::ToDate( $info[ 'subscription' ][ 'exp_date' ] ) .'</div>';
  }
  ?>
	<div class="sep"></div>
	<div>Confirm new plan to switch your subscription.</div>
  <div class="sep2"></div>
  <div class="button-container">
    <div class="center"> </div>
    <div class="button-wrapper"> 
		<a class="button" cura="subscription_package" data-id="<?php echo $package->id; ?>">Confirm New Plan</a>
      <div class="gap"></div>
      <a class="button btn-next button-alt" cura="inline_load_cancel" >Cancel</a></div>
  </div>
</div>
<?php
///Util::debug( $balance );
//Util::debug( $info );
?>
