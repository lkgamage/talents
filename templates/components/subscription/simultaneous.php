<div class="sep"></div>
<div class="padding panel bg-gray">
  <div class="txt-m">Maximum number of bookings you can accept at a time</div>
  <div class="sep"></div>
  <form id="simultaneous_update" cura="simultaneous_update" >
    <input type="hidden" name="id" value="<?php echo $subscription->id; ?>">
    <div class="field-wrapper">
	<label class="dynamic-label" for="simultaneous" >Number of bookings</label>
	<input type="text" class="input-short" value="<?php echo !empty($subscription) ? $subscription->simultaneous : '' ?>" name="simultaneous" id="simultaneous" maxlength="4">
</div>
  </form>
  <div class="sep"></div>

  <div class="button-container">
    <div class="center"></div>
	  <div class="button-wrapper">
	   <a class="button btn-next " cura="simultaneous_update">UPDATE</a>
    <div class="gap"></div>
    <a class="button btn-next button-alt " cura="inline_load_cancel">CANCEL</a>
	  </div>
    </div>

    
</div>
