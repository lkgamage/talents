<div class="field-wrapper">
  <div class="option-wrapper package-list">
<?php		  
	$classes = array('bg-blue', 'bg-green', 'bg-orange');
	$texts = array('txt-blue', 'txt-green', 'txt-orange');

	$cura = (!empty($cura)) ? $cura : 'subscription_package';	

	$k = 0;
	  
	if(empty($profile)){
		$session = Session::getProfile();
		
		if(!empty($session)){
			$profile = new Profile($session['id']);
			$profile = $profile->asArray();
		}
		
	}

	foreach ($packages as $i => $pkg){

	   if(!empty($subscriptions) && $pkg->isEmpty('price')){
			continue;   
	   }

	   if($pkg->active == 0){
			continue;   
	   }
		
		if(!empty($profile) && !empty($profile['commitment']) && $pkg->isEmpty('price') ){
			continue;
		}

	   if($k > 2 && $k%4 == 0){
			echo '</div><div class="option-wrapper package-list">';   
	   }

	   $k++;

	   $class =  $text ='';
	   if($pkg->price > 0){

			if($i == count($packages) - 1){
				$class = array_pop($classes);
				$text = array_pop($texts); 	
			}
			 else{
				$class = array_shift($classes); 
				$text = array_shift($texts); 	 
			 }
	   }


	   echo '<div class="subscription-plan">';
		
	 if($pkg->selected == 1){
		 echo '<div class="selected-plan"></div>';
	 }

	   echo '<div class="plan-name '.$text.'">'.$pkg->name.'</div>';

	   if($pkg->isEmpty('price')){

		  echo '<div class="plan-price txt-gray txt-light"><span>&nbsp;</span>Free';

		   echo '</div>';
		   echo '<div class="plan-price-annual"><label>&nbsp;</label></div>';

	   }
	   else {

		   echo '<div class="plan-price '.$class.'">
			'.currency($pkg->price).'<span>'.($pkg->validity == 365 ? 'Year' : 'Month').'</span>';

		   echo '</div>';
		   echo '<div class="plan-price-annual '.$class.'"><label>'.$pkg->tagline.'</label></div>';

	   }

	   echo '<div class="plan-features">';

	   if($pkg->agency_only == 1){
			  echo '<div><span>'.(($pkg->manage == 0) ? 'UNLIMITED' : $pkg->manage).'</span> Talents</div>'; 
	   }


	   if($pkg->price == 0){
			echo '<div><span>'.DataType::$FREE_BOOKINGS.'</span> <div>Bookings</div><div class="txt-green"><span >&nbsp;</span> &nbsp;</div></div>';
	   }
	   else {
			echo '<div><span>'.(($pkg->jobs == 0) ? 'UNLIMITED' : $pkg->jobs).'</span> <div>Bookings</div><div class="txt-green"><span >+'.DataType::$FREE_BOOKINGS.' </span> Free Bookings Monthly</div></div>';
	   }


	   echo '</div>';	

	   if(!empty($category) && $pkg->id ==  $category->package_id) {	   
		 echo '<a class="button button-alt" >CURRENT PLAN</a>';
	   }
	   else{
		   echo '<a class="button" cura="'.$cura .'" data-id="'.$pkg->id.'" data-category="'.$pkg->skill_id.'">SELECT PLAN</a>';
	   }

	   echo '<div class="plan-desc">'.nl2br($pkg->description).'</div>';
	  // echo '<div class="button-container">';

	 //  echo '</div>';

	   echo '</div>';

	}


?>
  </div>
</div>
<?php
//Util::debug($packages);

?>