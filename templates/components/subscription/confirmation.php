<?php

$skill = $engine->package->getSkill();
$profile = Session::getProfile();

if($user->isNew()){
	// redirect new users to dashboard
	$profile = NULL;
}

?>

<div class="panel">
  <div class="success-banner">SUBSCRIPTION ADDED!</div>
  <div class="sep"></div>
  <div class="section padding">
    <div>
      <p class="center">Your new subscription successfully activated.</p>
      <div >

        <div class="max-600 panel autocenter">
          <div class="list-select ">
            <div class="resizable info-header">Subscription </div>
            <div>
              <?php 
		  echo $skill->name.' - '.$engine->package->name;
		  echo '<br>';
		  ?>
            </div>
          </div>
          <div class="list-select">
            <div class="resizable info-header">Price </div>
            <div><?php echo !empty($engine->invoice->amount > 0) ?  currency($engine->invoice->amount) : 'Free'; ?></div>
          </div>
          <div class="list-select">
            <div class="resizable info-header">Renewal date</div>
            <div>
              <?php 
		  if($engine->category->renewal == 1) {
		  		echo Util::toDate($engine->subscription->exp_date);
		  }
		  else{
				echo   'N/A';
		  }
		   ?>
            </div>
          </div>
        </div>

      </div>
      <p></p>
      <div>Create packages for customers to make bookings.</div>
      <div class="button-container"> You may also want to update your public page.
        <div class="center"> </div>
      </div>
      <div class="button-container">
        <div class="center"> 
        <?php if(!empty($profile)) { ?> 
        <a class="button" href="<?php echo Util::mapURL('/dashboard/profile') ?>">Continue to profile</a>
        <?php } else { ?>
        <a class="button" href="<?php echo Util::mapURL('/dashboard') ?>">Continue to Dashboard</a>
        <?php } ?>
        <div class="gap"></div>
        <a class="button"  href="<?php echo Util::mapURL('/dashboard/packages') ?>">Create Packages</a>
         </div>
      </div>
      <p></p>
    </div>
  </div>
</div>
