<form id="subscription-form" cura="subscription_package">
<input type="hidden" name="skill" value="<?php echo $skill->id; ?>">

<div id="contents">
  <div class="panel ">
    <div class="padding section">
      <div class="dash-header-wrapper">
        <div class="dash-sub-header txt-orange">New Subscription</div>
      </div>
      <div class="sep"></div>
      <div class="txt-l center txt-bold">Choose your subscription plan</div>
     <?php
	  component('subscription/packages', $_data);
	  ?>
      <div class="sep"></div>
<?php
$profile = Session::getProfile();

if(!empty($profile) && $profile['type'] == 't') {
?>
      <div class="button-container">
      <a class="dash-link" cura="subscription_add" >Change Category</a> 
        <div class="center"></div>
        </div>
<?php } ?>
    </div>
  </div>
</div>
</form>