<?php

$renewal = $subscription->getAutoReneval();
$methods = $user->getPaymentMethods();

?>

<div class="sep"></div>
<div class="padding panel bg-gray">
  <div class="txt-m">Payment Method</div>
  <div class="sep"></div>
  <form id="renewal_method_update" cura="renewal_method_update" >
    <input type="hidden" name="id" value="<?php echo $subscription->id; ?>">
    <div class="option-wrapper option1 padding">
      <?php 
	
	if(!empty($methods)) { 
	
		foreach ($methods as $method) {
			
			if($method->isExpired()){
				continue;	
			}
	
			echo '<label class="custom-option bg-white" >';
			echo '<input type="radio" class="category " name="payment_method" value="'.$method->id.'" '.(($renewal->method_id == $method->id || count($methods) == 1) ? 'checked' : '' ).'>';
			echo '<div class="flex"> ';
			echo '<img src="'.Util::getCardTypeIcon($method->type).'">';
			echo '<div class="gap"></div>';
			echo ' <div class="middle">'.$method->name.'</div>';
			echo '</div>';
			echo '<span class="checkmark"></span>';
			echo '</label>';
		

		}
	
	}
	
	?>
      
    </div>
  </form>
  <div class="sep"></div>
  <?php if(count($methods) > 0) { ?>
  <div class="button-container">
    <div class="center"></div>
    <a class="button btn-next " cura="renewal_method_update">UPDATE</a>
    <div class="gap"></div>
    <a class="button btn-next button-alt " cura="inline_load_cancel">CANCEL</a> </div>
    <div class="sep"></div>
    <?php } ?>
    
    <a href="javascript:void(0)" cura="add_payment_method" data-subscription="<?php echo $subscription->id ?>" >Add new card</a>
</div>
