<div>
  <div>
    <?php
		if(empty($categories)) {
		  $categories = App::getSkills();
		}
		  $categories = Util::groupMultiArray($categories, 'category');
		  
		 // Util::debug($categories);
		  
		  foreach ($categories as $cat => $skills){
			  
			  echo ' <div class="txt-m txt-blue txt-bold vpadding expandable" cura="expand_block" data-id="cat_'.$cat.'" >'. DataType::$SKILL_CATEGORIES[$cat].'</div>';
			  echo '<div class="expandable-body" id="cat_'.$cat.'">';
			   echo '<div class="option-wrapper option2" >';
			  
			  foreach ($skills as $c) {
			  
				  echo '<label class="custom-option '.(!empty($c['is_business']) ? 'business' : '').'">
					  <input type="radio" class="category" name="category" value="'.$c['id'].'" >'.$c['name'].'<span class="checkmark"></span> </label>';
				  
			  }
			  
			  echo '</div></div>';
			  
		  }
		  
		  ?>
  </div>
  <label style="display:none" id="category-error" class="error" >Please select a role/skill category</label>
</div>
