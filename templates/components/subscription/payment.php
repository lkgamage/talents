<?php

$packages = $skill->getPackages();

if(!empty($previous)){
	$previous = $previous[0];
	$reneval = $previous->getAutoReneval();
	$balance = $previous->getBalance();
	$amount = ($reneval->term == 'm') ? currency($package->price_monthly) : currency($package->price_annualy);
}

?>
<form id="subscription-payment" cura="subscription_payment">
  <input type="hidden" name="category" value="<?php echo $skill->id; ?>">
  <input type="hidden" name="package" value="<?php echo $package->id; ?>">
  <input type="hidden" name="discount" id="package_discount" value="<?php echo $package->id; ?>">
  <div id="contents">
  <div class="panel ">
    <div class="padding section">
      <div class="dash-header-wrapper">
        <div class="dash-sub-header txt-orange">New Subscription</div>
      </div>
      <div class="sep"></div>
      <div class="columns">
        <div class="column-2  nopadding">
          <div class="section-header txt-gray nopadding">Subscription</div>
          <div class="sep"></div>
          <div class="txt-m txt-blue"><?php echo $skill->name.' - '.$package->name; ?></div>
          <div class="sep"></div>
          <?php if(empty($reneval)) { ?>
          <div class="field-wrapper">
            <div class="option-wrapper" style="display:block">
              <label class="custom-option custom-option-selected">
                <input type="radio" class="term"  name="term" value="m" checked cura="update_package_price" data-type="change"   >
                <span class="flex"><span class="resizable middle">Monthly</span><span class="txt-l package-amount"><?php echo currency($package->price_monthly); ?></span></span><span class="checkmark"></span> </label>
              <?php if(!$package->isEmpty('price_annualy')) { ?>
              <div class="sep"></div>
              <label class="custom-option">
                <input type="radio" class="term"  name="term" value="y" cura="update_package_price" data-type="change" >
                <span class="flex"><span class="resizable middle">Anually</span><span  class="txt-l package-amount"><?php echo currency($package->price_annualy); ?></span></span><span class="checkmark"></span> </label>
              <?php
			$anu = $package->price_monthly*12;
			$diff = $anu - $package->price_annualy;
			$percent = round(($diff/$anu)*100);
			
			if($percent > 0) {
				echo '<div class="txt-orange">Save '.$percent.'% on annual subscription!</div>';
				
			}
			?>
              <?php } ?>
            </div>
          </div>
          <?php } else {
			  
			  	
			  
			   ?>
           <div class="field-wrapper">
            <div class="option-wrapper" style="display:block">
              <label class="custom-option custom-option-selected">
                <input type="radio" class="term"  name="term" value="<?php echo $reneval->term; ?>" checked cura="update_package_price" data-type="change"   >
                <span class="flex"><span class="resizable middle"><?php echo ($reneval->term == 'm') ? 'Monthly' : 'Anually' ?></span><span class="txt-l package-amount"><?php echo $amount; ?></span></span><span class="checkmark"></span> </label>
              
            </div>
          </div>
          <?php } ?>
          <?php if (!empty($balance) && $balance['balance'] > 0){
			  
			$amount = ($reneval->term == 'm') ? $package->price_monthly : $package->price_annualy;
			$amount -=  $balance['balance'];
			$amount = currency($amount); 
			echo '<input type="hidden" id="display_amount" value="'.$amount.'">';;  
			  ?>
              
          <div class="sep"></div>
          <div class="section-header txt-gray nopadding">Balance transfer</div>
          <div class="sep"></div>
          <div><?php echo 'Remaining balance of <span class="txt-orange">'.currency($balance['balance'])."</span> will be credited to your new subscription." ?></div>
          <?php } ?>
        </div>
        <div class="gap"></div>
        <div class="column-2 nopadding">
          <div class="panel">
            <label class="list-select strip-white"  for="payment_method_card" >
            <input type="radio" name="payment_method" id="payment_method_card" checked cura="switch_payment_methods" data-id="payment_cards" data-type="change" >
            <div class="resizable txt-bold">CREDIT/DEBIT CARDS</div>
            <div class="image-line"> <img src="<?php echo Util::mapURL('/images/card-visa.png'); ?>"> <img src="<?php echo Util::mapURL('/images/card-master.png'); ?>"> <img src="<?php echo Util::mapURL('/images/card-amex.png'); ?>"> </div>
            </label>
            <div class="padding payment-methods" id="payment_cards">
              <div> Amount : <span class="txt-l txt-blue amount"><?php echo $amount; ?></span> </div>
              <div class="sep"></div>
              <div class="field-wrapper">
                <label class="dynamic-label" for="payment_card_name" >Card Holder Name</label>
                <input type="text" value="" name="payment_card_name" id="payment_card_name" maxlength="100">
              </div>
              <div class="field-wrapper">
                <label class="dynamic-label" for="payment_card_num" >Card Number</label>
                <input type="text" value="" name="payment_card_num" id="payment_card_num" maxlength="24"  cura="set_card_type" data-type="keyup">
                <label style="display:none" id="payment_card_num-error" class="error" ></label>
              </div>
              <div class="flex">
                <div class="resizable" >
                  <div class="field-wrapper">
                    <label class="dynamic-label" for="payment_card_exp" >Card Exp. Date</label>
                    <select class="input-tiny" name="card_exp_month" id="card_exp_month" >
                      <?php
	
	for($i = 1; $i <= 12; $i++){
		echo '<option value="'.($i < 10 ? '0'.$i : $i).'">'.($i < 10 ? '0'.$i : $i).'</option>';	
	}
	
	?>
                    </select>
                    /
                    <select class="input-tiny" name="card_exp_year" id="card_exp_year" >
                      <?php
	$year = date('y');
	for($i = $year; $i <= $year+10; $i++){
		echo '<option value="'.$i.'">20'.$i.'</option>';	
	}
	
	?>
                    </select>
                  </div>
                </div>
                <div >
                  <div class="field-wrapper">
                    <label class="dynamic-label" for="payment_card_cvv" >CVV</label>
                    <input class="input-tiny" type="text"  value="" name="payment_card_cvv" id="payment_card_cvv" maxlength="4">
                  </div>
                </div>
              </div>
              <label id="payment_card_cvv-error" class="error" for="payment_card_cvv" style="display:none">Please enter CVV</label>
              <div class="sep"></div>
              <div class="button-container">
                <div><img style="display:block" src="<?php echo Util::mapURL('/images/secure-payment.png'); ?>"></div>
                <div class="center"> </div>
                <a class="button button-act txt-m" cura="subscription_payment" >Pay <span class="amount"><?php echo $amount; ?></span></a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="sep"></div>
    <?php if(!empty($reneval)) { ?>
    <div class="section">
    <div class="button-container">              
                <div class="center"> </div>
                <a class="" cura="history" >Cancel</span></a> </div></div>
     <?php } ?>
    <div class="sep"></div>
    <div class="sep"></div>
  </div>
</form>
