<form id="subscription-form" cura="subscription_category">

<div id="contents">
  <div class="panel ">
    <div class="padding section">
      <div class="dash-header-wrapper">
        <div class="dash-sub-header txt-orange">New Subscription</div>
      </div>
      <div class="sep"></div>
      <div class="txt-l">Select a category</div>
      <?php
	  component('subscription/categories');
	  ?>
      <div class="sep"></div>
      <div class="button-container">
        <div class="center"></div>
        <a class="button btn-next" cura="subscription_category">Next</a>
        <div class="gap"></div>
        <a class="dash-link" cura="dash_menu" data-id="profile">Cancel</a> </div>
    </div>
  </div>
</div>
</form>
<?php
//Util::debug($categories);
?>