<div id="dash-contents">
<form id="subscription-form" cura="subscription_package">
<input type="hidden" name="category" value="<?php echo $skill->id; ?>">

<div id="contents">
  <div class="panel padding">
    <div class="padding section">
      <div class="page-header nopadding">Complete your profile</div>
      <div class="sep"></div>
      <div class="txt-m">Select a plan</div>
      <?php
	  component('subscription/packages', $_data);
	  ?>
      <div class="sep"></div>
<?php
$profile = Session::getProfile();

if(!empty($profile) && $profile['type'] == 't') {
?>

<?php } ?>
    </div>
  </div>
</div>
</form>
</div>