<?php
if(!empty($data)) {
	
	echo '<div class="box-row flex bg-gray txt-bold txt-s">
			<div class="box-cell cell-width-20">Date</div>
			<div class="box-cell cell-width-20">Amount</div>
			<div class="box-cell cell-width-40">Period</div>
			<div class="box-cell cell-width-20">Status</div>

		</div>';
	
	foreach ($data as $item){
		echo '<div class="box-row flex linked" cura="invoice_view" data-id="'.$item['id'].'">
			<div class="box-cell cell-width-20">'.( !empty($item['effective_date']) ? Util::ToDate($item['effective_date']) : Util::ToDate($item['created'])).'</div>
			<div class="box-cell cell-width-20">'.currency($item['amount']).'</div>
			<div class="box-cell cell-width-40">'.Util::ToShortDate($item['begin_date']).' - '.Util::ToShortDate($item['exp_date']).'</div>
			<div class="box-cell cell-width-20">';
		
		if(strtotime($item['due_date']) < time()){
			echo '<span class="txt-gray">Expired</span>';			
		}
		elseif(!empty($item['invoice_deleted'])){
			echo '<span class="txt-gray">Canceled</span>';			
		}
		elseif($item['status'] == DataType::$INVOICE_PENDING ){
			 echo  'Pending';
		}
		 else{
			echo '<span class="txt-green">Success</span>';
		 }
		
		echo '</div>
		</div>';
	}
	
}


//Util::debug($data);

?>
<div></div>
