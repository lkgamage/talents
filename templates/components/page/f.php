<?php
// portfolio

?><div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip ">
  <div class="container">
    <?php if(!empty($edit)) { ?>
    <div class="part-enable">
      <div class="onoffswitch">
        <input type="checkbox" id="section_f_enable" <?php echo ($sec['enabled'] == 1) ? 'checked' : ''; ?> class="onoffswitch-checkbox" cura="page_section_switch" data-section="f"  value="1" data-type="change">
        <label class="onoffswitch-label" for="section_f_enable"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
      </div>
    </div>
    <?php } ?>
    <?php 
    if(!empty($edit)) {
			echo '<h2 class="txt-blue notoppadding center" cura="page_edit" data-section="f" data-field="title" >'.$sec['title'].'</h2>';
			
			
		}
		else{
			echo '<h2 class="txt-blue notoppadding center" >'.$sec['title'].'</h2>';
			
			
		}
    ?>
     <div>
     <?php if(!empty($sec['contents']) && count($sec['contents']) >= 8) { ?>
     
     <div id="add_part_d">
      <div class="sep"></div>
      <a class="button"  cura="page_part_add" data-section="f" >ADD NEW</a> 
      </div>
     <?php } ?>
      <div class="sep"></div>
    </div>
    <div class="portfolio" id="portfolio"  >
    <?php
	
	$url = Util::mapURL('/images');
	
	if(!empty($sec['contents'])) {
		
		$sec['contents'] = array_reverse($sec['contents']);
	
		foreach ($sec['contents'] as $i => $item) {
			
			if(!empty($item['image'])){
				$image = $item['image'];	
			}
			else{
				$image = $url.'/static/sunset-ocean.png';	
			}
			
			echo '<a id="pft_'.$item['order'].'" class="portfolio-item noborder slide" cura="portfolio_open" data-index="'.$item['order'].'" data-id="'.$item['id'].'">';
			echo '<img class="f_'.$item['order'].'_img" src="'.$image.'">';
			
			if(!empty($item['title'])) {
				echo '<span class="f_'.$item['order'].'_title">'.$item['title'].'</span>';
			}
			
			echo '</a>';	
		}
	
	}
	
	
	
	
	?>      
    </div>
    <?php
	
	if(!empty($edit)) {
		echo ' <div id="add_part_d">
      <div class="sep"></div>
      <a class="button" id="portfolio_add" cura="page_part_add" data-section="f" >ADD NEW</a> </div>';	
	}
	?>
    <div>
    
    </div>
  </div>
</div>
<div class="portfolio-mask" id="portfolio-mask">
<div id="portfolio-container">
<?php 
if(!empty($sec['contents'])) {
	
		foreach ($sec['contents'] as $i => $item) {
			
			if(!empty($item['image'])){
				$image = $item['image'];	
			}
			else{
				if(!empty($edit)) {
					$image = $url.'/portfolio-upload.png';	
				}
				else{
					$image = $url.'/static/sunset-ocean.png';		
				}
			}
			
			
			
			echo '<div class="portfolio-max" id="pf_'.$item['order'].'">';
			echo (!empty($edit)) ? '<div class="portfolio-image" >' : '<div class="portfolio-image">';
			echo '    <img class="f_'.$item['order'].'_img"  src="'.$image.'">';
			
			if(!empty($edit)) {
				echo '<a href="javascript:void(0)" class="portfolio-delete"  cura="page_part_remove" data-section="f"  data-index="'.$item['order'].'"></a>';
				echo '<a  href="javascript:void(0)" class="portfolio-upload"  cura="page_edit" data-section="f" data-field="image" data-index="'.$item['order'].'"></a>';
			}
			
			
			
			echo '    </div>';
			
			if(!empty($edit) || !empty($item['title']) || !empty($item['body'])) {
				echo '    <div class="portfolio-content">';
				
				echo (!empty($edit)) ? '<div class="portfolio-title f_'.$item['order'].'_title" cura="page_edit"  data-section="f" data-field="title"data-index="'.$item['order'].'" >'.((empty($item['title'])) ? 'Add Title' : $item['title']).'</div>' : '<div class="portfolio-title f_'.$item['order'].'_title">'.$item['title'].'</div>';
				
				echo (!empty($edit)) ? '<div class="portfolio-description" cura="page_edit"  data-section="f" data-field="body"data-index="'.$item['order'].'" >'.((empty($item['body'])) ? 'Add description' : $item['body']).'</div>' : '<div class="portfolio-description">'.$item['body'].'</div>';
				echo '    </div>';
			}
			
			echo '</div>';
			
			
		}
		
}
			
?>
</div>
<a class="portfolio-close" cura="portfolio_close" ></a>
<a class="portfolio-prev" cura="portfolio_prev" ></a>
<a class="portfolio-next" cura="portfolio_next" ></a>
</div>

  
