<?php
// achivements
?>

<div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip ">
  <div class="container">
    <?php if(!empty($edit)) { ?>
    <div class="part-enable">
      <div class="onoffswitch">
        <input type="checkbox" id="section_d_enable" <?php echo ($sec['enabled'] == 1) ? 'checked' : ''; ?> class="onoffswitch-checkbox" cura="page_section_switch" data-section="d"  value="1" data-type="change">
        <label class="onoffswitch-label" for="section_d_enable"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
      </div>
    </div>
    <?php } ?>
    <?php 
    if(!empty($edit)) {
			echo '<h2 class="txt-blue notoppadding" cura="page_edit" id="about_title" data-section="d" data-field="title" >'.$sec['title'].'</h2>';
			
			
		}
		else{
			echo '<h2 class="txt-blue notoppadding" >'.$sec['title'].'</h2>';
			
			
		}
    ?>
    <div>
      <?php 
	  
	  $url = Util::mapURL('/images/static/');
	
		if(!empty($sec['contents'])){
		
		foreach ($sec['contents'] as $i => $item) {
			
				
			$sec['contents'] = Util::groupArray($sec['contents'],'order');
			ksort($sec['contents']);
			$sec['contents'] = array_reverse($sec['contents'], true);
	
		if($i%2 == 0){
			echo '<div class="columns achievement-row">';
			echo '<div class="column-2"><div class="achievement slideleft " data-index="'.$i.'">';
		}
		else{
			echo '<div class="column-2"><div class="achievement slideright " data-index="'.$i.'">';
		}
		
		
		if(!empty($edit)) {
			echo '<a class="achievement-remove" cura="page_part_remove" data-section="d" data-id="'.$item['id'].'" data-index="'.$i.'"></a>';
		}
		
		if(!empty($item['image'])){
			$image = $item['image'];	
		}
		else{
			$image = $url.'award'.($i%4).'.jpg';	
		}
		
		if(!empty($edit)) {
			
			echo ' <div class="award-image"  cura="page_edit" data-section="d" data-field="image" data-index="'.$i.'" data-crop="1" ><img src="'.$image.'"></div>';
		
			echo '<div class="award-title" cura="page_edit" data-section="d" data-index="'.$i.'" data-field="title">'.$item['title'].'</div>';
			
			echo '<div class="award-desc" cura="page_edit" data-section="d" data-index="'.$i.'" data-field="body">'.$item['body'].'</div>';
			
		}
		else {
		
			echo ' <div class="award-image"><img src="'.$image.'"></div>';
		
			echo '<div class="award-title">'.$item['title'].'</div>';
			
			echo '<div class="award-desc">'.$item['body'].'</div>';
		
		}
		
		echo '</div></div>';
		
		if($i%2 == 1 || count($sec['contents']) - 1 == $i){
			echo '</div>'; // rows
		}
	}
		}
?>
    </div>
    <?php	
	if(!empty($edit)){
		echo '<div id="add_part_d">
      <div class="sep"></div>
      <a class="button" cura="page_part_add" data-section="d" >ADD NEW</a> </div>';	
	}
	
	 ?>
  </div>
</div>
