<?php
// social
?>
<div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip social-strip">
  <div class="container">
  <?php if(!empty($edit)) { ?>
    <div class="part-enable">
      <div class="onoffswitch">
        <input type="checkbox" id="section_g_enable" <?php echo ($sec['enabled'] == 1) ? 'checked' : ''; ?> class="onoffswitch-checkbox" cura="page_section_switch" data-section="g"  value="1" data-type="change">
        <label class="onoffswitch-label" for="section_g_enable"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
      </div>
    </div>
    <?php } ?>
    <?php 
    if(!empty($edit)) {
			echo '<h2 class="txt-blue notoppadding txt-center" cura="page_edit" data-section="g" data-field="title" >'.$sec['title'].'</h2>';
			
			
		}
		else{
			echo '<h2 class="txt-blue notoppadding txt-center" >'.$sec['title'].'</h2>';
			
			
		}
    ?>
<div class="social-links social-link-display">
<?php

$urls = json_decode($sec['body'], true);

foreach (DataType::$SOCIAL_CLASSES as $s => $sn) {
	
	if(isset($urls[$s])) {
		echo '<a href="'.$urls[$s].'" target="_blank" class="social-'.$s.'"></a> ';		
	}
	elseif(!empty($edit)){
		
		echo '<a href="javascript:notify(\'You have not added '.$sn.' link.\')" class="social-'.$s.'"></a> ';	
		
	}
	
}


?>
</div>
    <?php if(!empty($edit)) { ?>
    <div class="center social-link-display">
      <div class="sep"></div>
      <a class="button" id="social_link_add" cura="social_link_open" >EDIT</a> </div>
    <div class="panel padding strip-white social-link-editor">
      <form cura="update_social_links" id="update_social_links">
        <div class="social-links ">
         <div class="flex "> <a  class="social-ww"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="ww" placeholder="Add your website URL">
            </div>
            <div class="gap"></div>
          </div>
          <div class="flex "> <a  class="social-fb"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="fb" id="social_fb" placeholder="Add your Facebook page link">
            </div>
            <div class="gap"></div>
          </div>
          <div class="flex "> <a  class="social-tw"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="tw" id="social_tw" placeholder="Add your Twitter profile url">
            </div>
            <div class="gap"></div>
          </div>
          <div class="flex "> <a  class="social-yt"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="yt" id="social_yt" placeholder="Add your Youtube channel URL">
            </div>
            <div class="gap"></div>
          </div>
          <div class="flex "> <a  class="social-ig"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="ig" id="social_ig" placeholder="Add your Instagram profile URL">
            </div>
            <div class="gap"></div>
          </div>
			<div class="flex "> <a  class="social-tk"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="tk" id="social_tk" placeholder="Add your TikTok profile URL">
            </div>
            <div class="gap"></div>
          </div>
          <div class="flex "> <a  class="social-ln"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="ln" id="social_ln" placeholder="Add your LinkedIn profile URL">
            </div>
            <div class="gap"></div>
          </div>
          <div class="flex "> <a  class="social-pn"></a>
            <div class="field-wrapper resizable">
              <input class="middle" type="text" name="pn" id="social_pn" placeholder="Add your Pinterest URL">
            </div>
            <div class="gap"></div>
          </div>
        </div>
        <div class="button-container">
          <div class="center"></div>
          <a class="button btn-next" cura="update_social_links">Update</a>
          <div class="gap"></div>
          <a class="button button-alt" cura="social_link_close">Cancel</a> <div class="gap"></div></div>
      </form>
    </div>
  </div>
  <?php } ?>
</div>
</div>
<?php
  
 // Util::debug($sec);
  
  ?>
