<div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip ">
  <div class="container">
    <?php 
    if(!empty($edit)) {
			echo '<h2 class="txt-blue notoppadding" cura="page_edit" id="about_title" data-section="c" data-field="title" >'.$sec['title'].'</h2>';
		}
		else{
			echo '<h2 class="txt-blue nopadding" >'.$sec['title'].'</h2>';
		}
	  
	 ?> 
	 <div class="noborder" id="package_container" cura="packages_show" data-id="<?php echo $talent->id; ?>" data-type="load"></div>
  </div>
</div>
