<?php
// about me
?><div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip ">

  <div class="container">
    <div class="columns profile slide">
      <?php
		
		echo '<div class="page-description to-right flex">';
		echo '<div class="middle">';
		
		if(!empty($edit)) {
			echo '<h2 class="txt-blue notoppadding center" cura="page_edit" id="about_title" data-section="a" data-field="title" >'.$sec['title'].'</h2><div class="sep"></div>';
			
			echo '<p id="about_text" cura="page_edit" data-section="a" data-field="body" >'.nl2br($sec['body']).'</p>';
		}
		else{
			echo '<h2 class="txt-blue notoppadding center" id="about_title">'.$sec['title'].'</h2>';
			
			echo '<p id="about_text" >'.nl2br($sec['body']).'</p>';
		}
        echo '</div>';
		echo '</div>';
		
		if(!empty($edit)) {
			echo '<div class="page-image to-left" cura="page_edit" data-section="a" data-field="image"><a  class="picture-browser icon-capture" title="Change Banner" id="about_image"></a>';
		}
		else {
			echo '<div class="page-image to-left" > ';
		}
		
		echo '<img src="'.(!empty($sec['image']) ? $sec['image'] : Util::mapURl('images/static/public-page-default.jpg')).'"> ';
		
		
		echo '</div>';
        
        ?>
    </div>
  </div>
</div>

