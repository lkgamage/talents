<?php
//Video
?>
<div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip <?php echo !empty($edit) ? '' : 'nopadding'?>">
  <div class="container">
    <?php if(!empty($edit)) { ?>
    <div class="part-enable">
      <div class="onoffswitch">
        <input type="checkbox" id="section_e_enable" <?php echo ($sec['enabled'] == 1) ? 'checked' : ''; ?> class="onoffswitch-checkbox" cura="page_section_switch" data-section="e"  value="1" data-type="change">
        <label class="onoffswitch-label" for="section_e_enable"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
      </div>
    </div>
    <?php } ?>
    <div class="profile-video">
      <?php
	
	if(!empty($sec['body'])) {
		
		echo '<iframe frameborder="0" type="text/html" src="'.$sec['body'].'" width="100%" height="100%" allowfullscreen allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"> </iframe>';
	}
	else {
		echo '<img src="'.Util::mapURL('/images/static/video-placeholder.jpg').'">';
	}
	?>
     </div>   
<?php	if (!empty($edit)){ ?>
      <div class="video-update-wrapper">
        <div class="video-update-dialog" id="video-update-dialog"> Paste embed codes
          <div class="sep"></div>
          <textarea  class="inline-text"  id="video-embed"></textarea>
          <div class="button-container">
            <div class="center"> <a class="button" cura="page_video_update">Update</a>
              <div class="gap"></div>
              <a class="button button-alt" cura="page_video_cancel">Cancel</a> </div>
          </div>
        </div>
      </div>
      <div class="sep"></div>
      <a class="button" cura="page_video_edit" id="page_video_edit" >Update Video</a>
      <?php	} ?>

    
    
  </div>
</div>
<?php
  
  //Util::debug($sec);
  
  ?>
