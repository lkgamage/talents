<?php
// mission
?>
<div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip ">
  <div class="container">
   <?php if(!empty($edit)) { ?>
    <div class="part-enable">
      <div class="onoffswitch">
        <input type="checkbox" id="section_h_enable" <?php echo ($sec['enabled'] == 1) ? 'checked' : ''; ?> class="onoffswitch-checkbox" cura="page_section_switch" data-section="h"  value="1" data-type="change">
        <label class="onoffswitch-label" for="section_h_enable"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
      </div>
    </div>
    <?php } ?>
    <?php 
    if(!empty($edit)) {
			echo '<h2 class="txt-blue notoppadding center" cura="page_edit" id="about_title" data-section="h" data-field="title" >'.$sec['title'].'</h2>';
		echo '<div class="sep"></div>';	
		echo '<p id="mission_text" cura="page_edit" data-section="h" data-field="body" >'.nl2br($sec['body']).'</p>';
	}
	else{
		echo '<h2 class="txt-blue notoppadding center" id="about_title">'.$sec['title'].'</h2>';
		//echo '<div class="sep"></div>';	
		echo '<p class="txt-l center" ><span class="mission-quote"><span>'.nl2br($sec['body']).'</span></span></p>';
	}
    ?>


</div>
</div>

