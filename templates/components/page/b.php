<?php
// services
?>
<div class="strip <?php echo !empty($is_gray) ? 'strip-light-gray' : 'strip-white'; ?> profile-strip">
  <div class="container">
    <?php if(!empty($edit)) { ?>
    <div class="part-enable">
      <div class="onoffswitch" id="service_switch">
        <input type="checkbox" id="section_b_enable" <?php echo ($sec['enabled'] == 1) ? 'checked' : ''; ?> class="onoffswitch-checkbox" cura="page_section_switch" data-section="b"  value="1" data-type="change">
        <label class="onoffswitch-label" for="section_b_enable"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
      </div>
    </div>
    <?php } ?>
    <?php
	
		$url = Util::mapURL('/images');
	
	
		if(!empty($edit)) {
			echo '<h2 class="txt-blue notoppadding " cura="page_edit" id="about_title" data-section="b" data-field="title" >'.$sec['title'].'</h2>';
			
			echo '<p id="service_text" cura="page_edit" data-section="b" data-field="body" >'.nl2br($sec['body']).'</p>';
			
		}
		else{
			echo '<h2 class="txt-blue notoppadding  center" >'.$sec['title'].'</h2>';
			if(!empty($sec['body'])){
				echo '<p id="about_text" class="center" >'.nl2br($sec['body']).'</p>';
			}
		}
		
		echo '<div class="service-block" >';
		
		if(!empty($sec['contents'])){
			
			$sec['contents'] = Util::groupArray($sec['contents'],'order');
			ksort($sec['contents']);
			
			foreach ($sec['contents'] as $index => $cont){
				
				echo '<div class="service-row slide " data-index="'.$index.'" >';
				echo '<div class="service-inner">';
				if(!empty($edit)) {
	
					echo '<a class="achievement-remove" cura="page_part_remove" data-section="b" data-id="7" data-index="'.$index.'"></a>';
					
						echo '<div class="service-image" cura="page_edit" data-section="b" data-field="image" data-index="'.$index.'" data-crop="1"><img class="b_'.$index.'_img" src="'.(!empty($cont['image']) ? $cont['image'] : $url.'/static/service1.jpg').'"></div>';
					
					echo '<div class="service-title page_part_'.$cont['id'].'"" cura="page_edit" data-section="b" data-field="title" data-index="'.$index.'">'.$cont['title'].'</div>';
					
					
					
					echo '<div class="service-row-desc page_part_'.$cont['id'].'"" cura="page_edit" data-section="b" data-field="body" data-index="'.$index.'">'.$cont['body'].'</div>';
					
				}
				else{
		
					echo '<div class="service-image" ><img src="'.(!empty($cont['image']) ? $cont['image'] : $url.'/static/service1.jpg').'"></div>';
					
					echo '<div class="service-title" >'.$cont['title'].'</div>';
					
					if(!empty($cont['body'])){
						echo '<div class="service-row-desc" >'.$cont['body'].'</div>';
					}
					
					
				}
				echo '</div>';
				echo '</div>';
			}
			
			
		}
		
	echo '</div>';
		
	if(!empty($edit)) {
		echo '<div id="add_part_b">
      <div class="sep"></div>
      <a id="services_add" class="button" cura="page_part_add" data-section="b" >ADD</a> </div>';
	}
	
	
		
	?>
    
  </div>
</div>
</div>
