<div class="panel" style="display:block" >
 <div class="sub-header ">Update your contact detail</div>
  <p>Review your contact detail and update if nessasary.</p>
          <div class="field-wrapper combined-field">
            <label class="dynamic-label " style="left:75px;" for="t_mobile">Mobile Number</label>
            <div class="inner-wrapper">
              <input type="text" style="width:75px;" disabled value="+944" class="t_phonecode" >
              <input type="text" class="input-short" name="t_mobile" id="t_mobile"  maxlength="14">
            </div>
            <label id="t_mobile-error" class="error" for="t_mobile"></label>
            <p class="anote">This will be our primary communication channel. It is private.</p>
          </div>
          <div class="field-wrapper combined-field">
            <label class="dynamic-label " style="left:75px;" for="t_phone">Office Number</label>
            <div class="inner-wrapper">
              <input type="text" style="width:75px;" disabled value="+944" class="t_phonecode">
              <input type="text" class="input-short" name="t_phone" id="t_phone" maxlength="14">
            </div>
            <label id="t_phone-error" class="error" for="t_phone"></label>
          </div>
          <div class="field-wrapper">
            <label class="dynamic-label" for="email">Email Address</label>
            <input type="text" id="email" name="t_email"  maxlength="100">
          </div>
</div>