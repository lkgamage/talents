<?php
$country = Session::getCountry();
?>
<div class="strip space_block bg-white columns nopadding">
  <div class="column-2 flex">
    <div class=" center-panel middle" id="signup_panel">
      <form id="signup_profile_form" >
        <input type="hidden" name="tz" id="tz" value="">
        <div class="strip signup-strip">
          <div style="display:none"><img src="<?php echo Util::mapURL('images/talent-logo-16i.png') ?>"></div>
          <h1 class="page-header nopadding">Complete Talent/Business Profile</h1>
          <div class="sep"></div>
          <div class="nopadding" >
            <div class="field-wrapper">
              <label class="dynamic-label" for="talent_name" >Public Name</label>
              <input type="text" value="<?php echo !empty($talent) ? $talent->name : '' ?>" name="talent_name" id="talent_name" maxlength="400">
              <div class="anote">This is displaying in your public page.<br>
                Your well-known nickname, band name, dancing group  name or business name</div>
            </div>
            <div id="gender-box-">
              <div class="field-wrapper">
                <label  >Gender</label>
                <div class="sep" ></div>
                <div class="flex option-list wrap">
                  <div class="col4" style="padding-bottom:0;">
                    <label class="custom-checkbox" title="Male">
                      <input type="radio" name="gender" class="gender" value="1" >
                      Male <span class="checkmark"></span> </label>
                  </div>
                  <div class="col4">
                    <label class="custom-checkbox" title="Female">
                      <input type="radio" name="gender" class="gender" value="0" >
                      Female <span class="checkmark"></span> </label>
                  </div>
                  <div class="col4" style="padding-bottom:0; display: none">
                    <label class="custom-checkbox"  title="Transgender Male">
                      <input type="radio" name="gender" class="gender" value="3"  >
                      T-Male <span class="checkmark"></span> </label>
                  </div>
                  <div class="col4" style="display: none">
                    <label class="custom-checkbox" title="Transgender Female">
                      <input type="radio" name="gender" class="gender" value="2" >
                      T-Female <span class="checkmark"></span> </label>
                  </div>
                </div>
                <label style="display:none" id="gender-error" class="error" >Please select your gender</label>
              </div>
              <div class="field-wrapper">
                <label  for="user_dob"  >Date of Birth</label>
                <div class="flex">
                  <?php
                  $dob = $user->dob;
                  $dobp = array( 0, 0, 0 );

                  if ( !empty( $dob ) ) {
                    $dobp = explode( '-', $dob );

                    $dobp[ 1 ] = ( int )$dobp[ 1 ];
                    $dobp[ 2 ] = ( int )$dobp[ 2 ];
                  }
                  ?>
                  <select name="dob_month" id="dob_month">
                    <?php
                    foreach ( DataType::$months as $i => $name ) {

                      echo '<option value="' . $i . '" ' . ( $dobp[ 1 ] == $i ? 'selected' : '' ) . ' >' . $name . '</option>';

                    }

                    ?>
                  </select>
                  <select name="dob_day" id="dob_day">
                    <?php

                    for ( $i = 1; $i <= 31; $i++ ) {
                      echo '<option value="' . $i . '" ' . ( $dobp[ 2 ] == $i ? 'selected' : '' ) . ' >' . $i . '</option>';
                    }

                    ?>
                  </select>
                  <select name="dob_year" id="dob_year">
                    <?php
                    $dy = date( 'Y' );
                    for ( $i = ( $dy - 5 ); $i >= $dy - 76; $i-- ) {
                      echo '<option value="' . $i . '" ' . ( $dobp[ 0 ] == $i ? 'selected' : ( $dobp[ 0 ] == 0 && $dy - 25 == $i ? 'selected' : '' ) ) . ' >' . $i . '</option>';
                    }


                    ?>
                  </select>
                </div>
                <label style="display:none" id="dob-error" class="error" >Date is not valid</label>
              </div>
              <div class="field-wrapper">
                <label  for="user_dob"  >Currency</label>
                <select name="currency" class="input-short">
                  <?php
                  foreach ( DataType::$currencies as $code => $name ) {
                    echo '<option value="' . $code . '" ' . ( ( $country == 'LK' && $code == 'LKR' ) ? 'selected' : '' ) . ' >' . $name . '</option>';
                  }

                  ?>
                </select>
              </div>
            </div>
            <div id="_server_error"></div>
            <div class="sep"></div>
            <div class="button-container">
              <div class="center"></div>
              <a class="button" cura="submit_signup_profile">Create Profile</a> </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="column-2 right-banner desktop-only" style="background-image:url(<?php
  	echo Util::mapURL('images/static/right-banner').rand(1,6).'.jpg';
   ?>)"> <img src="<?php echo Util::mapURL('images/curatalent-logo.png'); ?>">
    <div class="padding"></div>
    <h1 class="center txt-white">We have tools<br>
      To<br>
      Make your life easier!</h1>
  </div>
</div>

<!--------------------------------------------> 
