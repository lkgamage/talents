
<div class="strip signup-strip ">
<div class="space_block flex">
  <div class=" max-800 is-centered middle" >
    <div class="page-header">Setting up your account</div>
    <div class="sep"></div>
    <div class="flex padding bg-gray">
      <div class="account-selection"><img src="<?php echo Util::mapURL('images/static/selection-talent.jpg') ?>"></div>
      <div class="resiable">
        <div class="txt-l txt-bold txt-orange">PERFORMER/BUSINESS</div>
        <div class="sep"></div>
        <p class="nopadding">If you an artist, performer or a business related to party/events, register as a talent.</p>
        <div class="sep"></div>
        <a class="txt-blue">See performer/business categories</a>
        <div class="sep"></div>
        <div class="sep"></div>
        <a href="<?php echo Util::mapURL('/join/talent') ?>" class="button">Create Talent Profile</a> </div>
    </div>
    <div class="flex padding ">
      <div class="account-selection"><img src="<?php echo Util::mapURL('images/static/selection-agency.jpg') ?>"></div>
      <div class="resiable">
        <div class="txt-l txt-bold txt-orange">AGENCY</div>
        <div class="sep"></div>
        <p class="nopadding">If you own or manage an event related service agency, register as an agency.</p>
        <div class="sep"></div>
        <a class="txt-blue">See agency categories</a>
        <div class="sep"></div>
        <div class="sep"></div>
        <a href="<?php echo Util::mapURL('/join/agency') ?>" class="button">Create Agency Profile</a> </div>
    </div>
    <div class="flex padding bg-gray">
      <div class="account-selection"><img src="<?php echo Util::mapURL('images/static/selection-booking.jpg') ?>"></div>
      <div class="resiable">
        <div class="txt-l txt-bold txt-orange">BOOKING</div>
        <div class="sep"></div>
        <p class="nopadding">If you are a seasonal event organizer or organizing your promotion party, we have talents, service providers and tools to make it happning. </p>
        <div class="sep"></div>
        <div class="sep"></div>
        <div class="sep"></div>
        <a href="<?php echo Util::mapURL('/book') ?>" class="button">Find Talents</a> </div>
    </div>
  </div>
</div>
<div class="sep"></div>
<div class="sep"></div>
<!---------------------------------------------------> 

