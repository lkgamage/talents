<div class="strip help-strip strip-orange">
  <div class="container">
	  <a href="https://www.tiktok.com/@curatalent/video/7076564765279063322" >
   <img class="promo" src="<?php echo Util::mapURL('/images/promo/tiktok-challenge.jpg'); ?>"></a>
  </div>
</div>

<div class="strip help-strip ">
  <div class="container">
	  <div class="vpadding center">
	  <h1>කියුරාටැලන්ට් ටික්ටොක් චැලේන්ජ්</h1>
	  </div>
	  <p class="max-800 is-centered center txt-l">අපි ඔයාලට අලුත් විදිහෙ චැලේන්ජ් එක්ක අරගෙන ආව. ටික්ටොක් කරන අයට විශේෂ වුනත්, අනිත් අයටත් සහභාගී වෙන්න පුලුවන්. 
ඔයාලට කරන්න තියෙන්නෙ, අපේ ටික්ටොක් වීඩියෝ එකේ මියුසික් එකට, නිර්මාණශීලී ඩාන්ස්  එකක් ඉදිරිපත් කරන එක විතරයි. </p>
	  
	  <div class="sep2"></div>
	  <div class="center"><a class="button" href="https://www.tiktok.com/@curatalent/video/7076564765279063322">Take The Challenge</a></div>
	  <div class="sep2"></div>
	  <p class="max-800 is-centered  txt-l">තෝරගන්න නිර්මාණවලට මුදලින් තෑගි වගේම, හොදම නිර්මාණයේ අයිතිකාරයට ෆොටෝශූට් එකක් අපෙන් තෑගි.  
තරඟය අප්‍රේල් මාසෙ 10 වෙනිද රෑ 12.00 න් ඉවර වෙනව. ඒ නිසා ඉක්මනින් ඔයාලගෙ නිර්මාණත් ටික්ටොක් එකට දාන්න. <br>#curatalent හෑශ්ටෑග් එක දාන්නත් අමතක කරන්න එපා.<br>
ටික්ටොක් කරන්නෙ නැති අයට, ඔයාලගෙ වීඩියෝ එක <a class="txt-l" href="https://www.facebook.com/curatalent">ඉන්බොක්ස් කරන්නත් </a> පුලුවන්.<br>
දැන්ම පටන් ගමු!</p>
  </div>
	<div class="sep2"></div>
</div>

<style>
	.promo {width: 100%;
	display: block;}
</style>