<div class="strip space_block bg-white columns nopadding">
  <div class="column-2 flex">
    <div class="middle center-panel " id="signup_panel">
      <form id="signup_form" cura="signup_validate">
		  <input type="hidden" name="picture" id="picture" value="">
        <div class="signup-logo"><img src="<?php echo Util::mapURL('images/curatalent-logo.png'); ?>"></div>
        <div id="country-container">
          <h1 class="page-header txt-bold nopadding">Select your country</h1>
          <div class="sep"></div>
          <div class="field-wrapper">
            <select name="country" id="country" >
              <?php
              $country = Session::getCountry();

              foreach ( DataType::$countries as $code => $obj ) {

                if ( $code == $country ) {
                  echo '<option selected="" value="' . $code . '">' . $obj[ 0 ] . '.</option>';
                } else {
                  echo '<option value="' . $code . '">' . $obj[ 0 ] . '.</option>';
                }
              }
              ?>
            </select>
          </div>
		
			
          <div class="button-container">
            <div class="center"></div>
            <a class="button btn-next" cura="signup_advance">Next</a> </div>
        </div>
        <div id="signup-form-container" class="hidden">
          <h1 class="page-header txt-bold nopadding">Create Your Account</h1>
			<div>Signup with your Facebook or Google account. </div>
          
          <div class="sep"></div>
          <div >
			   <!----------- Social buttons ------>
           	<div class="columns" cura="signup_init">
				<div class="column-2 nopadding">
				<a class="social-buttons signup-facebook"  cura="social_login" data-site="facebook">Signup with Facebook</a>
				</div>
				<div class="gap1"></div>
				<div class="column-2 nopadding">					
				<a class="social-buttons signup-google" cura="social_login" data-site="google">Signup with Google</a>
				</div>		
			</div>
			  <!----------- Social buttons ------>
			  <div class="sep"></div>
			  <div class="sep"></div>
			  <div class="dash-sep"></div>
			  <div class="sep"></div>
			  <div>Enter your real name. You may create your talent/business page with your well-known name in next step.</div>
			   <div class="sep"></div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="user_firstname" >First Name</label>
              <input type="text" value="<?php echo !empty($talent) ? $talent->firstname : '' ?>" name="user_firstname" id="user_firstname" maxlength="100"  >
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="user_lastname" >Last Name</label>
              <input type="text" value="<?php echo !empty($talent) ? $talent->lastname : '' ?>" name="user_lastname" id="user_lastname" maxlength="100"  >
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="user_city" >City</label>
              <input type="text" value="<?php echo !empty($talent) ? $talent->city : '' ?>" name="user_city" class="citypick " id="user_city" maxlength="100" placeholder=""  >
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label dynamic-label-fold" for="user_phone" >Mobile Number</label>
              <div class="flex">
                <input type="text" class="phone-code" id="phone-code" disabled >
                <input class="phone-number" type="text" pattern="[0-9]*" inputmode="numeric" name="user_phone" id="user_phone" maxlength="14">
              </div>
              <label style="display:none" id="user_phone-error" class="error" for="user_phone">Mobile number is required to verify your account</label>
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="user_email" >Email Address</label>
              <input type="text" value="" name="user_email"  id="user_email" maxlength="100" placeholder=""  >
            </div>
            <div id="_server_error"></div>
            <div class="anote">Mobile number and email address are private.</div>
            <div class="button-container">
              <div class="center"></div>
              <a class="button btn-next" cura="signup_validate">Create Account</a> </div>
            <div class="sep"></div>
            <div>Already have an account? &nbsp;&nbsp;<a href="<?php echo Util::mapURL('/login'); ?>">Sign In</a></div>
            <div class="sep"></div>
            <div > By creating an account, you agree to Curatalent's <a>conditions of use</a> and <a>privacy policy</a>. </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="column-2 right-banner desktop-only" style="background-image:url(<?php
  	echo Util::mapURL('images/static/right-banner').rand(1,6).'.jpg';
   ?>)">
    <div class="padding"></div>
    <h1 class="center txt-white">Lets Make It Happening!</h1>
  </div>
</div>
<script>
function googleSignup (res){
	console.log(res);
}

</script> 
