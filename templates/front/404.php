<?php
$url = Util::mapURL('');
?>
<div class="page-container page-not-found">
<div class="columns">

<div class="column-2 right-column  lost-help middle">
<h1 class="txt-orange">Ooops, It's 404 ! </h1>
<div class="sep"></div>
<div class="txt-xl">We can't seem to find the page you are looking for.</div>
<div class="sep"></div>
<div class="sep"></div>
<div class="txt-l txt-gray txt-bold">Let’s get back into the track: </div>
<div class="sep"></div>
<p>If you were looking for a talent or a business page,<br>page may have been removed.<br> 
<a href="<?php echo $url.'/search'; ?>" class="more-link txt-bold">Search talents and services</a>
</p>
<p>Here are more helpful links</a>
</p>
<div><a href="<?php echo $url.'/'; ?>" >Home</a></div>
<div><a href="<?php echo $url.'/login'; ?>" >Login</a></div>
<div><a href="<?php echo $url.'/dashboard'; ?>" >Your dashboard</a></div>
<div><a href="<?php echo $url.'/join'; ?>">Join with Curatalent</a></div>
<div><a href="<?php echo $url.'/support/startup'; ?>">Startup guide</a></div>
<div><a href="<?php echo $url.'/support'; ?>">Support</a></div>
<div><a href="<?php echo $url.'/contact'; ?>">Contact us</a></div>
<div><a href="<?php echo $url.'/feedback'; ?>">Leave a feedback</a></div>
</div>
<div class="column-2 left-column  fullheight minions"></div>
</div>


</div>
