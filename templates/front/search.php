<?php

$categories = $engine->getSkills();
$category_list = $categories;

if ( !empty( $category ) ) {
  $categories = Util::groupArray( $categories, 'id' );
  $cat_name = $categories[ $category ][ 'name' ];
}

?>
<div class="strip search-strip strip-orange">
  <form id="search_form" cura="search_talent">
    <input type="hidden" name="agency" id="agency" value="">
    <input type="hidden" name="cat_id" id="cat_id" value="<?php echo (!empty($category)) ? $category : ''; ?>">
    <input type="hidden" name="list" id="list_id" value="">
    <div class="container content">
      <div class="sep"></div>
      <div class="page-header notoppadding">Make up your next event</div>
      <p style="margin:0">Find the best performer, entertainer, music band or services</p>
      <div class="talent-search-bar"   cura="init_search" data-type="load">
        <div class="talent-search-a">
          <div class="flex resizable is-relative">
            <input type="text" class="search-field" placeholder="I'm looking for ..." id="b_search" name="query"  value="<?php echo (!empty($term)) ? $term : '' ?>" autocomplete="off">
            <input type="button" class="search-button " value="" id="b_button1" cura="search_talent">
            <a class="clear-field clear-field-first" id="b_search_" cura="clear_field" data-id="b_search" style="<?php echo (!empty($term)) ? 'display:block"' : '' ?>"></a> </div>
          <a class="browse_button" cura="category_toggle" id="category_toggle"></a> </div>
        <div id="search_complete"> </div>
      </div>
		<div class="txt-gray"><?php component('currency') ?></div>
		<div class="sep"></div>
    </div>
  </form>
</div>
<div class="strip strip-gray">
  <div class="container">
    <div class="category-container" >
      <div class="search-categories hidden" id="search-categories">
        <div class="category-wrapper">
          <?php


          $cats = Util::groupMultiArray( $categories, 'category' );
          $i = 1;

          // remianing categories
          foreach ( $cats as $cat => $skills ) {

            if ( $i == 2 ) {
              // add agencies
              echo '<div>';
              echo '<h2>' . DataType::$SKILL_CATEGORIES[ 0 ] . '</h2>';
              echo '<div class="option-wrapper option2 ">';

              foreach ( DataType::$AGENCY_TYEPS as $id => $at ) {

                echo '<a cura="category_select" data-name="'.$at.'" data-id="' . $id . '" data-agency="1" >' . $at . '</a>';
				  $category_list[] = array('id' => $at, 'name' => $at.' Agency', 'agency' => 1);
              }

              echo '</div>';
              echo '</div>';

            }

            echo '<div>';
            echo '<h2>' . DataType::$SKILL_CATEGORIES[ $cat ] . '</h2>';
            echo '<div class="option-wrapper option2 ">';

            foreach ( $skills as $c ) {

              echo '<a cura="category_select" data-name="'.$c[ 'name' ].'" data-id="' . $c[ 'id' ] . '">' . str_replace( "|", '/', $c[ 'name' ] ) . '</a>';

            }

            echo '</div>';
            echo '</div>';

            $i++;
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="container content space_block" id="search-result-container"  >
<?php
	  
	  if(!empty($category)) {
		  component('search_results', $_data);
	  }
	  else{
	?>
<div class="sep"></div>
<div class="sep"></div>
<div class="search-loader" cura="search_talent" data-type="load">
  <div>Searching...</div>
</div>	  
	<?php
	  }
	  
?>
    
	  
  </div>
</div>
<script>
var skills = <?php echo json_encode($category_list); ?>
</script> 
