<?php
$sections = $page->getPageContents();
$section = $sections[ 'a' ];
$profile = $object->getImage();

$class = 'profile_'.$object->id;
$ref = "profile";

?>
<div class="strip space_block bg-white columns nopadding">
  <div class="column-2 flex">
    <div class=" center-panel middle" id="signup_panel">
      <form id="signup_page_form" cura="signup_page" >
        <input type="hidden" name="public-page" id="public-page" data-id="<?php echo $page->id; ?>" value="<?php echo $page->id; ?>">
		  <input type="hidden" id="city" name="city" value="<?php echo $user->getLocation()->city ?>">
        <div class="strip signup-strip">
          <div style="display:none"><img src="<?php echo Util::mapURL('images/talent-logo-16i.png') ?>"></div>
          <h1 class="page-header nopadding">Create Talent/Business Page</h1>
          
			<div>You can use any language to create your page</div>	
			<div class="sep"></div>
			 <div class="sep"></div>
          <div class="nopadding" >
            <div class="field-wrapper">
              <label class="dynamic-label" for="talent_name" >Page handle/url</label>
              <input type="text" value="<?php echo (!empty($page) && !$page->isEmpty('handle')) ? $page->handle :  preg_replace("/[\+\!\#\$\~%\^\&\*\(\)\{\}\[\]\'\"\|\/\<\>\,`\=\\@\:\;\?]/",'', str_replace(' ', '-', strtolower($object->name))) ?>" name="page_handle" id="page_handle" maxlength="400">
              <div class="anote">This is your public page. Choose a handle match with your business name</div>
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="section_title" >Page Title</label>
              <input type="text" value="<?php echo (!empty($section['title']) && $section['title'] != "I'M TALENTED" ) ? $section['title'] : $object->name ?>" name="section_title" id="section_title" maxlength="400">
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="section_body" >Page Description</label>
			<div class="txt-s txt-gray" style="line-height: 1.8rem">Need help? <a href="javascript:void(0)" cura="bio_begin" class="txt-s">Click here autofill</a></div>	
				<div id="bio" class="bg-yellow panel"></div>
              <textarea name="section_body" id="section_body" maxlength="2000" style="min-height: 8rem" ><?php 
	
				if(!empty($section['body']) && $section['body'] != 'Brief description about you/business'){
					echo  $section['body'];
				}
				 else{
					 	echo  $object->isTalent() ? 'I am ' : 'We are';
				 }

				  
				  ?></textarea>
              <div class="anote">Tell us some stroy about you/your business.</div>
				<div style="display: none">Need help? <a href="javascript:void(0)" cura="bio_begin">Click here to write a description for you</a></div>
				
				
            </div>
            <div class="field-wrapper">
              <label class="txt-m">Add a profile picture</label>
              <div >
                <div class="flex">
                  <div class="resizable">
					  <div class="page_profile_image" id="profile_changer" cura="update_profile_picture" data-ref="<?php echo $ref; ?>"  data-id="<?php echo $object->id; ?>">
					  <img src="<?php echo $profile->i400; ?>" class="<?php echo $class; ?>">
					  <a></a></div>
					</div>
                
                </div>
              </div>
            </div>
            <div class="field-wrapper">
              <div class="sep"></div>
              <label class="txt-m">Add a big image for your page</label>
              <div class="anote">Choose a picture which you are in action</div>
              <div class="sep"></div>
              <div class="">
                <div class="flex">
                  <div class="resizable">
					<div class="page_banner_image page-image" cura="browse_banner_image" data-field="image" data-section="a" ><?php
						
						$banner = (!empty($section['image'])) ? $section['image'] : Util::mapURL('images/placeholder-banner.jpg');
						$class = ($object->isTalent()) ? 'talent_'.$object->id : 'agency_'.$object->id;
						echo '<img src="'.$banner.'"  >';
					?><a></a></div>
					</div>
                  
                </div>
              </div>
            </div>
            <div class="sep2"></div>
            <div class="button-container">
              <div class="center"></div>
              <a class="button" cura="submit_signup_page"><?php echo !$page->isEmpty('handle') ? 'Save' : 'Create My Page' ?></a> </div>
          </div>
			
        </div>
      </form>
    </div>
	  
	   
	  
  </div>
  <div class="column-2 right-banner desktop-only" style="background-image:url(<?php
  	echo Util::mapURL('images/static/right-banner').rand(1,6).'.jpg';
   ?>)"> <img src="<?php echo Util::mapURL('images/curatalent-logo.png'); ?>">
    <div class="padding"></div>
    <h1 class="center txt-white">We have tools<br>
      To<br>
      Make your life easier!</h1>
  </div>
</div>
<div class="hidden">
    <form id="image_form">
      <input type="file" id="image_browse" name="image" accept="image/*" >
    </form>
  </div>
<script>
$(document).ready(function(){
var	ct_uploading = $('.page_banner_image');
});
</script>
<?php
//Util::debug($object->getImage());
//Util::debug( $section );
?>
