<?php
$location = $object->getLocation();

if ( empty( $sections ) ) {
  $sections = $page->getPageContents();
}
//$edit = false;
?>
<div id="public-page" data-id="<?php echo $page->id; ?>">
  <div class="strip" style="background-image:url(<?php echo $page->getBanner()->url; ?>); " id="page-header" >
    <form cura="check_date" id="inline-booking-form">
      <input type="hidden" name="profile" value="<?php echo $page->profile_id; ?>">
      <div class="container">
        <div class="profile-top">
          <div class="profile-banner">
            <?php if(!empty($edit)) { ?>
            <a id="banner_changer" class="icon-capture" title="Change Banner"></a> <a id="banner_changer" data-id="<?php echo $page->id; ?>" data-ref="banner" class="icon-capture" title="Change Banner" cura="browse_banner" data-action="browseImage"></a>
            <?php } ?>
          </div>
          <div class="profile-info">
            <div class="profile-pic">
              <div class="profile-pic-inner"> <img class="<?php echo 'profile_'.$object->id; ?>" src="<?php echo $object->getImage()->i600; ?>" alt="<?php echo $object->name; ?>">
				  <?php echo ($object->exclusive == 1) ? '<div class="exclusive"></div>' : '' ?>
				  
                <?php if(!empty($edit)) { ?>
                <a id="profile_changer" data-id="<?php echo $object->id; ?>" data-ref="profile" class="icon-capture" title="Change Picture" cura="context" data-action="browseImage"></a>
                <?php } ?>
              </div>
            </div>
            <div class="profile-info-inner">
              <h1 class="profile-name"><?php echo $object->name; ?></h1>
              <?php
              echo '<div class="profile-cat">' . $object->getSkillString() . '</div>';
              ?>
              <div class="profile-loc">
                <?php
                echo ucwords( $location->city . ', ' . $location->region . ', ' . DataType::$countries[ $location->country ][ 0 ] );
                /** No country if it same as user country ***/
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php if(!empty($edit)) { ?>
      <!----- Banner updater -------->
      
      <div class="bi-crop" id="bi-crop">
        <div id="bi-canvas" data-zoom="1"> </div>
        <div class="zoom-tools"><a class="zoom-in" onClick="zoomBi(1)"></a><a class="zoom-out" onClick="zoomBi(-1)"></a></div>
      </div>
      <div id="bi-buttons"> <a class="button" onClick="cropBannerImage()">Done</a>
        <div class="gap"></div>
        <a class="button button-alt" onClick="closeBannerImage()">Cancel</a> </div>
      
      <!------ Banner updater ends ------>
      <?php } ?>
      <div class="strip strip-light-gray ">
        <div class="container ">
          <?php
          $ps = $object->packageStatus();

          if ( empty( $ps ) || empty( $ps[ 'packages' ] ) ) {
            ?>
          <div class="profile-booking">
            <div class="padding center is-centered txt-l txt-gray">Available soon for bookings!</div>
          </div>
          <?php } else { ?>
          <div class="profile-booking">
            <div class="profile-booking-label">Check Availability</div>
            <div class="relative">
              <div class="profile-booking-fields">
                <input id="date" name="date" type="text" class="date-field profile-booking-date datepick" placeholder="Select Date" data-changed="picktime_open" >
                <input id="time" name="time" type="text" class="date-field hidden timepick" placeholder="Select Time" data-changed="check_date">
                <a class="button button-act profile-booking-button" cura="check_date">Check</a> </div>
              <label id="date-error" class="error" for="date" style="display:none">Please select a date</label>
              <label id+"time-error" class="error" for="time" style="display:none">Please select a time</label>
              <div class="vanilla-calendar" id="date_cal" data-id="date" ></div>
              <div class="timepicker" id="time_time" data-id="time" style="display: none;"> </div>
            </div>
            <div class="resizable"></div>
            <div class="profile-booking-info" style="display: none"> <a id="fav_<?php echo $page->id; ?>" class="favorite" cura="favorite" data-id="<?php echo $page->id; ?>"></a> </div>
          </div>
          <?php }	?>
        </div>
      </div>
    </form>
  </div>
  <div class="strip strip-light-gray">
    <div class="container bg-white">
      <div class="is-centered max-800">
        <div id="page-booking"> </div>
      </div>
    </div>
  </div>
  <?php

  $is_gray = true;

  foreach ( $sections as $section_id => $sec ) {
    // display sections

    if ( empty( $edit ) && empty( $sec[ 'enabled' ] ) ) {
      continue;
    }

    if ( $section_id == 'c' && $page->show_packages == 0 ) {
      continue;
    }

    component( 'page/' . $section_id, array( 'talent' => $object, 'sec' => $sec, 'edit' => !empty( $edit ), 'is_gray' => $is_gray ) );

    $is_gray = !$is_gray;
  }

  ?>
  <div class="hidden">
    <form id="image_form">
      <input type="file" id="image_browse" name="image" accept="image/*" >
    </form>
  </div>
</div>
<script>
var talent_id = <?php echo $object->id; ?>;
var auto_show_wizard = <?php echo (!empty($show_wizard)) ? 'true' : 'false' ?>;
var edit_mode = <?php echo (!empty($edit)) ? 'true' : 'false' ?>;
</script>
<?php
if ( !empty( $tools ) ) {

  if ( !empty( $edit ) ) {
    ?>
<div class="publish-tools" id="publish-tools"> <a class="pt-wizard" title="Wizard" id="wb1" cura="show_wizard"></a> <a class="pt-publish" title="Publish" id="wb3" cura="publish_page" data-id="<?php echo $page->id; ?>" data-ref="<?php echo ($page->isEmpty('published')) ? 1 : 0 ?>"></a> <a class="pt-close" href="<?php echo Util::mapURL('/'.$page->handle); ?>" title="Stop Editing" id="wb4"></a> </div>
<?php
} else {
  ?>
<div class="publish-tools" id="publish-tools"> <a class="pt-wizard" title="Wizard" id="wb1" cura="show_wizard"></a> <a class="pt-edit" href="<?php echo Util::mapURL('/'.$page->handle); ?>?mode=edit" title="Edit Page" id="wb2"></a> </div>
<?php
}
}

?>
<div class="popup" id="popup">
  <div class="popup-inner">
    <div class="anchor" id="anchor"></div>
    <div class="popup-close">x</div>
    <div class="popup-body" id="popup_text"></div>
  </div>
</div>
<?php
//Util::debug($sections);

?>
