<?php
$object = $engine->getObject();

?>
<div id="google_root"></div>

<div class="strip search-strip strip-white">
  <div class="container content">
    <div class="columns">
      <div class="column-2 align-top left-content">
        <div class="page-header txt-bold">Make a Booking for <?php echo $object->name ?></div>
        <div class="main-desc hidden">Please fill following details to make your booking</div>
        <div class="sep"></div>
        <div id="booking-panel" >
			<?php 
			
			if(!empty($summery)) {
		
				component('booking/booking-summery',  $_data);
		
			}
			else{
				component('booking/booking-form', $_data);
			}
			
			?>
			
        </div>
      </div>
      <div class="column-2 desktop-column align-top">
        <div class="sep"></div>
        <?php component('/booking/profile_column', $_data); ?>
      </div>
    </div>
    <div class="sep"></div>
  </div>
</div>





