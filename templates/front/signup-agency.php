<?php
$types = App::getAgencyTypes();
?>
<div class="strip space_block bg-white columns nopadding">
  <div class="column-2 flex">
    <div class="middle center-panel " id="signup_panel">
      <form id="signup_agency_form" cura="submit_agency_profile" >
        <input type="hidden" name="country" id="country" value="<?php echo Session::getCountry() ?>">
        <div class="strip signup-strip">
          <div style="display:none"><img src="<?php echo Util::mapURL('images/talent-logo-16i.png') ?>"></div>
          <h1 class="page-header nopadding">Complete Agency Profile</h1>
          <div class="sep"></div>
          <div class="nopadding" >
            <div class="field-wrapper">
              <label class="dynamic-label" for="agency_name" >Agency Name</label>
              <input type="text" value="" name="agency_name" id="agency_name" maxlength="400">
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label dynamic-label-fold" for="agency_type" >Agency Type</label>
              <select name="agency_type" id="agency_type"  class="input-short">
                <?php

                foreach ( DataType::$AGENCY_TYEPS as $id => $name ) {

                  echo '<option value="' . $id . '">' . $name . '</option>';

                }

                ?>
              </select>
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="address" >Office Address</label>
              <input type="text" value="" name="address" class="" id="address" maxlength="100" placeholder=""  >
            </div>
            <div class="field-wrapper">
              <label class="dynamic-label" for="office_city" >City</label>
              <input type="text" value="" name="office_city" class="citypick " id="office_city" maxlength="100" placeholder=""  >
            </div>
            <div id="_server_error"></div>
            <div class="sep"></div>
            <div class="button-container"> <a  href="<?php echo Util::mapURL('/join/talent'); ?>">I don't have an agency</a>
              <div class="center"></div>
              <a class="button" cura="submit_agency_profile">Create Agency</a> </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="column-2 right-banner desktop-only" style="background-image:url(<?php
  	echo Util::mapURL('images/static/right-banner').rand(1,6).'.jpg';
   ?>)"> <img src="<?php echo Util::mapURL('images/curatalent-logo.png'); ?>">
    <div class="padding"></div>
    <h1 class="center txt-white">Manage your business<br>
      With<br>
      Ease of mind!</h1>
  </div>
</div>
