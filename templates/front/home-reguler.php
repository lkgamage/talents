<div class="strip intro-strip" cura="init_home" data-type="load">
  <div class="container">
    <h1>Let's Make It happening!</h1>
    <div class="main-search-bar">
      <div class="main-search-inner">
        <div class="main-search-text">Search for Artist, Band, Dancing group, Entertainer or service provider </div>
        <form method="get" action="<?php echo Util::mapURL('search/') ?>">
        <div class="main-search-fieldset">
        
          <input type="text" class="main-search-field" placeholder="Search" name="q">
          <input type="submit" class="main-search-button" value="">
        </div></form>
      </div>
    </div>
  </div>
</div>
<div class="strip strip-gray service-strip">
  <div class="container">
    <h2 class="center nopadding txt-blue">EVENT PLANNING TO A NEW DIMENSION</h2>
    <div class="sep"></div>
    <div class="columns">
      <div class="column-3">
        <div class="service-block slide">
          <div class="service-icon"><img src="<?php echo Util::mapURL('images/feature-search.png'); ?>" alt="Find Talents and Services"></div>
          <div class="service-name" >Find Talents &amp; Services</div>
          <div class="service-desc">Just one place to look for whatever you need when planning your next event. No more hunting through websites, emailing or phoning around. Get everything in one place. We make it easy!</div>
        </div>
      </div>
      <div class="column-3">
        <div class="service-block slide">
          <div class="service-icon"><img src="<?php echo Util::mapURL('images/feature-market.png'); ?>" alt="Event Service Marketplace"></div>
          <div class="service-name">Event Service Marketplace</div>
          <div class="service-desc">We help you market your talents and services all in one place. You focus on what you do best, and leave the admin to us.</div>
        </div>
      </div>
      <div class="column-3">
        <div class="service-block slide">
          <div class="service-icon"><img src="<?php echo Util::mapURL('images/feature-manage.png'); ?>" alt="Event & Booking Management" ></div>
          <div class="service-name">Event &amp; Booking Management</div>
          <div class="service-desc">Our events management tool will assist you with all your booking, budget as well as any advice you might need along the way.</div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="strip service-strip">
  <div class="container">
    <div class="columns">
      <div class="column-2 nopadding home-category to-right slideright" id="category_professional"> </div>
      <div class="column-2 middle to-left left-content slideleft">
        <h2 class="txt-orange center">PROFESSIONALS &AMP; BUSINESSES</h2>
        <p>You are good at something, but you can’t get noticed by event organizer? No more hand out your business card to everyone you meet or email everyone who may need your skills.</p>
        <p>Our all in one platform take away the stress. We give you exposure to huge crowd of event organizers that are looking for someone like you. You can market yourself directly on our platform and take advantage of this opportunity.</p>
        <div class="center"><a class="button button-act button-large" href="<?php echo Util::mapURL('/join') ?>">Join Now</a></div>
      </div>
    </div>
    <div class="columns">
      <div class="column-2 nopadding home-category slideleft" id="category_agency"> </div>
      <div class="column-2 middle right-content slideright">
        <h2 class="txt-orange center">AGENCIES</h2>
        <p>Today’s market is more competitive than ever, so you need to be one step ahead. Finding businesses and  right talent at the same time is more stressful. </p>
        <p>Network with the right professionals who are all in one place. Meet event managers, event planners, exhibitors, or creative talents for your next project to solve your problems effectively. You can offer your service and manage your company and contacts through one simple platform.</p>
        <div class="center"><a href="<?php echo Util::mapURL('/join') ?>"  class="button button-act button-large">Join Now</a></div>
      </div>
    </div>
    <div class="columns">
      <div class="column-2 nopadding home-category to-right slideright" id="category_customer"> </div>
      <div class="column-2 middle to-left left-content slideleft">
        <h2 class="txt-orange center">EVENT ORGANIZERS</h2>
        <p>Finding the right balance of talent to rock your event? It’s time consuming when you have to plan an event alone. Your event needs careful planning-balancing the right number of talents, the décor, the beverages, the music and more...</p>
        <p>Our solution makes it a whole lot easier. Find all your service providers under one roof-from the venue to the  decorator and even entertainment all at one place. Thus avoiding dealing with multiple emails, phone calls and spreadsheets. Our software does all the hard work for you.</p>
        <p>If you are still hesitating, take a look at our FAQ section.</p>
        <div class="center"><a href="<?php echo Util::mapURL('/search') ?>"  class="button button-act button-large">Find Talents</a></div>
      </div>
    </div>
  </div>
</div>
<div class="strip event-strip slide">
  <div class="container slide">
    <div class="max-800 is-centered hpadding" >
      <h2 class="center">EVENT MANAGEMENT</h2>
      <p>Plan your event with organizing it step by step, defined  budget and automated communication with service providers.</p>
      <p> Our platform connects you with musicians, photographer, planners  and many more services, so you can book your vendors and plan your event with  ease! With its integrated feature <span class="txt-orange">Curatalent</span> helps cut down on the time and  energy spent organizing events, so you have more to focus on.</p>
      <p> When your guests turn up on the day or night, they&rsquo;ll  discover a party venue with everything they need to rock out all night long.</p>
      <div class="sep"></div>
      <div class="center"> <a href="<?php echo Util::mapURL('/dashboard/events') ?>" class="button button-act button-large">Start Planning</a></div>
    </div>
  </div>
</div>
<div class="strip strip-gray">
  <div class="container">
    <h2>FIND THE ANSWERS YOU NEED </h2>
    <div class="vpadding">
      <div class="columns ">
        <div class="column-2 left-content">
          <div class="faq"> <span>What is Curatalent?</span>
            <div>Curatalent is an online platform that offers a wide range of project management tools for businesses and individuals in the events & entertainment industry, including talent marketing, event management, booking, appointment scheduling  and message boards. </div>
          </div>
          <div class="faq"> <span>Who can use it?</span>
            <div> Curatalent is open for everyone. <br>
              If you are planning a birthday party, wedding or big cooperate event, we have  simplified your works. <br>
              If you are an artist, performer, speaker, music band, dancing group, venue  provider, decorator, beauty salon… You are most welcome to our platform.  <a  href="<?php echo Util::mapURL('/support/categories'); ?>">See all talent and service categories.</a> </div>
          </div>
          <div class="faq"> <span>How it works for talents/service providers?</span>
            <div>Curatalent is a hub for entertainment industry professionals and service providers to create their profile and connect with people looking to book them. We put Talents in the center, giving them the best possible way to promote themselves. Our smart booking and appointment management tools will take care of hard works while you focus on what you do the best.</div>
          </div>
          <div class="faq"> <span>How it works for agencies?</span>
            <div>Finding, hiring, and retaining the best talent is costly. Dedicated teams are needed to manage the process. Curatalent is a powerful, all-in-one solution that does away with the hassle of trying to sync with multiple systems. Finding businesses, gathering talent and vendors in one place. Significantly reducing your time spent on non-event related activities, allowing you to focus more on your core strength.</div>
          </div>
          <div class="faq"> <span>How it works for event organizers?</span>
            <div>Trying to manage and book venues and performers has always been a nightmare. From communicating with talent, to dealing with their managers and agents. We make it painless for you to find the best talent out there for your event. <br>
              Our platform streamlines the entire event talent hiring process so you can find, negotiate and book entertainment and services with just a few clicks in a seamless and stress-free way.</div>
          </div>
        </div>
        <div class="column-2 right-content">
          <div class="faq"> <span>How to book a talent or service provider?</span>
            <div><a  href="<?php echo Util::mapURL('/search'); ?>">Start searching talents and services here.</a><br>
              You may search or browse through categories to find best talents for your budget in your area. Pick the time and date you want to book. Send a booking request.  They’ll get in touch with you to make arrangements. Its simpler than a piece of cake. </div>
          </div>
          <div class="faq"> <span>How to plan an event?</span>
            <div><a  href="<?php echo Util::mapURL('/join'); ?>" >Create an account</a> or <a  href="<?php echo Util::mapURL('/login'); ?>"  >login</a> to your Curatalent account. Go to your dashboard, create an event.  When you select the type of your event, we’ll give you some suggestions and recommendations regarding choosing right balance of talents/services.    You may make booking for your preferred talents and venders. You can negotiate and manage all your bookings, expenses, and arrangements from our event management platform. </div>
          </div>
          <div class="faq"> <span>As a talent, how can I work with an agency?</span>
            <div>If you are interested to work with an agency as a talent or service provider, you may contact the agency through Curatalent messaging board.<br>
              Agency will send you a request to join with them. When you accept the request, Agency will be able to work with you on our all-in-one platform. Direct booking, appointment sharing,  event discussion and many more tools available. </div>
          </div>
          <div class="faq"> <span>Can I hire an agency to plan my event?</span>
            <div>Of course, That’s what agencies do. <br>
              <a  href="<?php echo Util::mapURL('/search'); ?>">Find an agency by search or browse</a> through categories.  Pick a package they offer and make a booking.  They’ll get in touch with you to make arrangements. </div>
          </div>
          <div class="faq"> <span>How much will it cost?</span>
            <div>Just create an account and start planning your next event free.<br>
              Agencies, talents, and services can also join with free of charge. Additional features and tools available on subscription basis.<br>
              <br>
              <a href="<?php echo Util::mapURL('/support/categories'); ?>" >See complete price list for talent/business categories. </a> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="center"><a href="<?php echo Util::mapURL('/support/fqa') ?>" class="more-link" >See all FAQ</a></div>
    <div class="sep"></div>
    <div class="sep"></div>
  </div>
</div>
<div class="strip strip-white">
  <div class="container">
    <div class="columns">
      <div class="column-4 middle">
   
        <div class="center"> <img src="<?php echo Util::mapURL('images/curatalent-logo-large.png'); ?>"></div>
        <p style="display:none"><a cura="chat_open" data-talent="1">Open chat room 1</a></p>
      </div>
      <div class="column-41">
        <h2 class="txt-orange">MAKE IT HAPPENNING!</h2>
        <p><span class="txt-bold">Curatalent</span> is an exclusive network where people who organize events can connect with the best emerging talents worldwide. We're thrilled to present you our platform designed as a two-way tool for curating and discovering new talent as well as connecting organizers with performers and creators from different artistic fields.</p>
        <p>You could be a popular artist, recognized business brand, raising star or startup business. Maybe you are a sessional event planner, or you are organizing your promotion party at the first time. <span class="txt-bold">Curatalent</span> platform leveraging technology to simplify the process of marketing talents to connects with the event organizers seeking them out. </p>
        <p><span class="txt-bold">Curatalent</span> is a marketplace for artists and creatives to meet the global demand for creative talents. Our proprietary technology finds them work. It’ll enable organizers of events and trade shows to find professional creatives quickly and efficiently.</p>
        <div class="center"><a href="<?php echo Util::mapURL('/support/about') ?>" class="more-link" >Read More About Us</a></div>
        <div class="sep"></div>
        <div class="sep"></div>
      </div>
    </div>
  </div>
</div>
