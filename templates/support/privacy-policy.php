


<div class="strip help-strip notoppadding">
  <div class="container space_block">
	  <div class="sep"></div>
	  <h1 class="txt-gray">Curatalent Platform Privacy Policy</h1>
	  <h2>Privacy policy</h2>
	  <div class="sep2"></div>
    <p class="nopadding">
At Curatalent Private Limited your right to privacy is a  concern.<br>
Curatalent is sensitive about the information you provided to  us over the internet, by email or other form of communications. We follows a  transparent policy model. We take our responsibility to protect the personal  information provided to us seriously. Our security guidelines are subject to  change based upon technological developments.</p>
<h2>Who we are</h2><p>
Curatalent Private Limited is a registered limited liability  legal entity in Sri Lanka. We are utilizing technology to develop primer entertainment  industry platform for event industry professional, event related service  agencies, talents, performers, and artist in entertainment industry, industry  service providers as well as a mass public who organize parties and events.</p>
<h2>Why we collect data</h2><p>
We rely upon a number of different legal bases for  processing personal information and special category personal information –  these include processing personal information where it is in our legitimate  interests to do so, where this is necessary for the fulfillment of a contract  or where the processing is necessary to carry out our obligations under  employment law. Where we rely on our legitimate interests, this means that we  use personal information to run our business and to provide the services we  have been asked to provide. We only collect information that has been supplied  voluntarily, you do not have to provide us with personal information. However,  if you do not provide us with information we need by law or require to do work,  we may not be able to offer certain products and services.</p>
<h2>How we collect data</h2>
	  
We may collect your data on,
    <ul>
      <li>Curatalent.com website when you register on Curatalent  platform, subscribe for certain services, customize your pages and dashboard</li>
      <li>Third party supportive services like feedback  modules and bug reports</li>
      <li>Analytical services such as google analytics,  social sharing services</li>
      <li>Public form of communications, emails, messaging  services</li>
      <li>Social media interactions on Facebook,  Instagram, Twitter, LinkedIn, and Pinterest </li>
    </ul>
   <h2> What data we are collecting</h2>
    <ul>
      <li>Your personal information such as name,  location, contact number, email</li>
      <li>Your general data such as country, currency, and  local time</li>
      <li>Your photographs, your product photographs</li>
      <li>Other content you choose to provide to promote  yourself or your practice</li>
      <li>Your event information such as date, location,  and schedule</li>
      <li>Information regarding your profession or  business</li>
      <li>Your personal schedule, working hours </li>
      <li>Your feedback and correspondence with us</li>
      <li>Social media interaction, interaction with  Curatalent platform </li>
    </ul>
   <h2>Where we store data</h2> <p>
      We store these data in a PCI compliance secure private  servers. Data is accessible to authorized Curatalent employees only. </p>
    <h2>How we use your data</h2>
    <p>
      We mainly use your private data to customizes the service  and products we offer you on Curatalent platform.  We also use statistical (non-personal data) collected  form analytic services and social media to promote our services. </p>
    <ul>
      <li>Customize product and service to individual  preferences on Curatalent platform</li>
      <li>Communicate about the products and services we  offer, and respond to requests, inquiries, comments, and suggestions</li>
      <li>Operate, evaluate, and improve our business, the  platform, and other products and services we offer (including to research and  develop new products and services)</li>
      <li>Establish and maintain an individual&rsquo;s profile  on the Site</li>
      <li>Administer surveys and other market research</li>
      <li>Comply with legal requirements, judicial  process, and our company policies</li>
    </ul>

    <h2>Your payment data</h2><p>
      We do not store or use your payment data, including credit card  numbers and validation data within Curatalent servers. All the payment processing  task done by a government authorized payment processing partner. However, we  store your payment method, last 4 digits of your card number and card type,  your IP address in Curatalent data servers for payment references and  verification purposes.  </p>
    <h2>Your data and your  right to be forgotten</h2>
    <p>
      If you are a customer  of Curatalent platform, you may close your account or associated profiles from  your dashboard. We keep this information for 30 days within Curatalent servers,  in case if you change your mind, you may re-activate your account or associated  profiles. After 30 days,  we remove all  your data from our online databases and keep them in offline data store as we  may have to produce the data if any legal requirement arise. Then we&rsquo;ll permanently  delete your data including all the information you provided on Curatalent  platform , any photographs you uploaded, your event and appointment data.</p>
   <h2>Cookie policy</h2> <p>
      Curatalent platform is  highly customizable for individual preferences. Therefore, platform store some piece  of information in your browser. Your country, selected currency, selected time  zone and your logion identification are some of these data. We store them in an  encrypted manner, and no one can read them from your browser except Curatalent platform  software.  These data will be eased as  you log out from the system. </p><p>
      Curatalent has integrated  with trusted third party services to enhance its features. These services may  also create cookies on your browser to temporary store some important information. </p><p>
      By using Curatalent platform, you are  consenting to our use of cookies and other tracking technology in accordance  with this notice. If you do not agree to our use of cookies and other tracking  technology in this way, you should set your browser settings accordingly or not  use the  Curatalent platform. If you  disable cookies that we use, this may impact your user experience while on the Curatalent  platform. </p>
    <h2>Disclaimer</h2>
    <p>The information contained in this website   is for general information purposes only. The information is provided by   Curatalent and while we endeavour to keep the information up to   date and correct, we make no representations or warranties of any kind,   express or implied, about the completeness, accuracy, reliability,   suitability or availability with respect to the website or the   information, products, services, or related graphics contained on the   website for any purpose. Any reliance you place on such information is   therefore strictly at your own risk.</p>
    <p>In no event will we be liable   for any loss or damage including without limitation, indirect or   consequential loss or damage, or any loss or damage whatsoever arising   from loss of data or profits arising out of, or in connection with, the   use of this website.</p>
    <p>Through this website you are able to link to   other websites which are not under the control of Curatalent. We   have no control over the nature, content and availability of those   sites. The inclusion of any links does not necessarily imply a   recommendation or endorse the views expressed within them.</p>
    <p>Every   effort is made to keep the website up and running smoothly. However,   Curatalent  takes no responsibility for, and will not be liable for,   the website being temporarily unavailable due to technical issues   beyond our control</p>

    <p><a class="more-link" href="<?php echo Util::mapURL('/support'); ?>" >Support Home Page</a></p>
  </div>
</div>
