<div class="strip help-strip strip-orange">
  <div class="container">
    <h1 class="txt-orange upper center">Services</h1>
  </div>
</div>
<div class="strip help-strip">
  <div class="container">
	  
	  <div class="max-800 column-31">
	  <p>Our mission is to create the  largest marketplace for life-changing experiences, with the objective of  enabling people all over the world, to follow their passions and accomplish  their goals.  To achieve this, our  platform has several different kinds of users, acting in different ways.  Our platform needs to cater each user, to  provide them with a unique experience, that implements their specific needs.</p>
      <ul>
        <li class="txt-l">Talents </li>
      </ul>
      <p>Artists, performers, bands,  professionals  have busy life, tight  schedules, unique goals, but you don't have a lot of time to manage it all.  Curatalent helps you to make your life easier, your experience more enjoyable  and your goals a reality.</p>
      <ul>
        <li  class="txt-l">Businesses</li>
      </ul>
      <p>It is hard to manage entertainment industry  business. You have to lead your crew to get things done on time while searching  for businesses. Curatalent has made to connect event service industry business  with event organizers.</p>
      <ul>
        <li  class="txt-l">Agencies</li>
      </ul>
      <p>We want to make it easier for agencies,  planners and organizers of events to find and connect with actors, musicians  and other event talents and services. Curatalent platform build with handy  tools to streamline your entire business process from receiving a business to successfully  complete the event. </p>
      <ul>
        <li  class="txt-l">Event organizers</li>
      </ul>
      <p>We  created a platform where you can find and connect with people in the service  industry, such as entertainers or chefs for your special occasion. Curatalent  makes it easier for event planners to find, hire talents and services while  providing the necessary tools to plan your event.</p>
	  </div>
	  <div class="sep2"></div>
<h2 class="center notoppadding">Curatalent Services</h2>
	  <div class="columns">
	
	  	<div class="column-2 left-content">
		  <h3 class="txt-orange">Talents &amp; Businesses</h3>
			<p>Curatalent platform marketplace connect you with thousands  of opportunities for any talent or business in the entertainment industry.  Connect with potential clients and build a successful career. It will  also help you manage your schedule, track your income, connect you to other  professionals for mutual benefit, and allow you to sell your merchandise from  anywhere you may be. Curatalent lets you organize your time more  efficiently. </p> <p>
Integrated schedule management tools let you set your own  working hours, keep track of your appointments and availability.  Our intelligent software let your clients  find your free time to make bookings while protecting your privacy. Platform  featured with messages boards to make arrangements with your clients and  flexible enough to change/modify bookings based on your and clients&rsquo;  requirements.
</p>
			<p class="nopadding">Curatalent platform offers you,</p>
            <ul>
              <li>Public profile page  to promote your talents or business</li>
              <li>Flexible work/time scheduling</li>
              <li>Smart appointment management</li>
              <li>Booking management, change and cancel bookings</li>
              <li>Directly working with Agencies on bookings and  appointments</li>
              <li>Booking message boards, chat rooms to work on  events</li>
              <li>Free subscriptions up to 3 talent or business  categories</li>
            </ul>
            <p> What else can we build to make your professional  life easier? </p>

	    </div>
		 <div class="column-2 right-content">
		  <h3  class="txt-orange">Event Organizers</h3>
			<p>Whether you are a seasonal event  planner or occasionally planning a party, event planning is difficult, time  consuming and always leaves you with one or two unbooked talents or services to  attend in the last minute. We are the premier marketplace platform that brings  together event planners, who need talents and services, to directly connect  with them. This means no more calling around to your friends, asking your  family if they know someone. No more waiting for the phone to ring after sending  out e-mails.</p><p>
With our platform, anyone can find  an up-to-date talent which ranges from celebrity impersonators to DJ's in a  matter of seconds! Lost venue? We help solve that problem for you too. Say no  to email chains, lengthy phone calls negotiating with performers and artists. Planning  event and hiring talents, all in one place.
</p>
			 <p class="nopadding">Curatalent platform offers you,</p>
            <ul>
              <li>Event panning tools including budget and  bookings</li>
              <li>Event agenda planning </li>
              <li>Event message boards/chat room to make plans  with participants</li>
              <li>Intelligent booking management, change and  cancel bookings</li>
              <li>Booking message boards</li>
            </ul>
            <p>and more event planning tools will  be released soon. <br>Most importantly, all these  services are absolutely free for you.</p>

	    </div>
	  </div>
	</div></div>
<?php
include(Config::$base_path.'/templates/support/service_part.php');
?>