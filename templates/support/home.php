<?php
$url = Config::$base_url;
?>

<div class="strip support-banner">
  <div class="container flex fade">
    <div class="middle slide">
      <h1>Support Center</h1>
      <h2 class="nopadding txt-white">Getting most out of your account</h2>
    </div>
  </div>
</div>
<div class="strip help-strip">
  <div class="container">
    <div class="columns">
      <div class="column-3">
        <div class="service-block">
          <div class="service-icon"><img src="<?php echo $url; ?>/images/support/rocket.png" alt="startup guide"></div>
          <div class="service-name txt-orange">Startup Guide</div>
          <div class="service-desc"> If you are new to Curatalent platform, Startup Guide will help you setup your account and profile quickly. </div>
          <div class="center"><a class="button " href="https://www.youtube.com/watch?v=b5SaRuCc2Xs" target="_blank">See Startup Guide</a></div>
        </div>
      </div>
      <div class="column-3">
        <div class="service-block">
          <div class="service-icon"><img src="<?php echo $url; ?>/images/support/services.png" alt="services"></div>
          <div class="service-name txt-orange">Services</div>
          <div class="service-desc"> Explore available platform features for each type of users. </div>
          <div class="center"><a class="button " href="<?php echo $url; ?>/support/services">Explore Services</a></div>
        </div>
      </div>
      <div class="column-3">
        <div class="service-block">
          <div class="service-icon"><img src="<?php echo $url; ?>/images/support/faq.png" alt="FAQ"></div>
          <div class="service-name txt-orange">Frequently Asked Questions </div>
          <div class="service-desc"> Quickly find answers for your questions. </div>
          <div class="center"><a class="button "  href="<?php echo $url; ?>/support/faq">See FAQ</a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="strip ">
  <div class="container help-strip  bg-gray">
    <div class="columns  padding ">
      <div class="column-4">
        <h3 class="center txt-blue upper txt-l">Events</h3>
        <div >Learn how to, create an event, make bookings for the event, budget and expences, creating and sharing agenda and communicate with partners.</div>
        <div class="sep"></div>
        <div><a class="more-link">Learn more about events</a></div>
      </div>
      <div class="gap"></div>
      <div class="column-4">
        <h3 class="center txt-blue upper  txt-l">Bookings</h3>
        <div>Learn how to, find talents and services you need, make booking request, manage and respond to bookings. Change or cancel bookings.</div>
        <div class="sep"></div>
        <div><a class="more-link">Learn more about bookings</a></div>
      </div>
      <div class="gap"></div>
      <div class="column-4">
        <h3 class="center txt-blue upper  txt-l">Appointments</h3>
        <div>Learn how to, manage your schedule. Scheduled &amp; confirmed appointments, Appointments for bookings. Working with agency appoinments.</div>
        <div class="sep"></div>
        <div><a class="more-link">Learn about appointments</a></div>
      </div>
      <div class="gap"></div>
      <div class="column-4">
        <h3 class="center txt-blue upper txt-l">Subscriptions &amp; Packages</h3>
        <div>Learn skill categories and subscriptions. Creating and managing packages (Sellable service blocks)</div>
        <div class="sep"></div>
        <div><a class="more-link">Learn more about packages</a></div>
      </div>
    </div>
  </div>
</div>
<div class="sep"></div>
<div class="strip help-strip">
  <div class="container">
    <div class="columns">
      <div class="column-2 left-content">
        <div class="service-icon"><img src="<?php echo $url; ?>/images/support/business.png"></div>
        <h3  class="center">Talents &amp; Business Categories</h3>
        <div >Talents/Businesses can subscribe upto 3 categories. See full list of available categories.</div>
        <div class="sep"></div>
        <div class="center"><a class="button " href="<?php echo $url; ?>/support/categories">See All Categories</a></div>
      </div>
      <div class="column-2 column-2 right-content">
        <div class="service-icon"><img src="<?php echo $url; ?>/images/support/shopingcart.png"></div>
        <h3 class="center">Pricing Plans</h3>
        <div><span class="txt-bold">Basic subscriptions are free!</span> Additional features are available on paid subscriptions.</div>
        <div class="sep"></div>
        <div class="center"><a class="button " >See Subscription Plans</a></div>
      </div>
    </div>
  </div>
</div>
<div class="strip help-strip">
  <div class="container">
    <div class="columns">
      <div class="column-2">
        <h3>Event Organizers</h3>
        <div class="links"> 
        <a>Finding talents &amp; businesses </a> 
        <a>Finding agencies</a> 
        <a>Planining an event</a>
        <a>Make and managing bookings</a>
        <a>Communicate with talents and services</a>
        </div>
      </div>
      <div class="column-2">
        <h3>Talents  &amp; Businesses</h3>
        <div class="links"> 
        <a>Creating talent/business profile</a> 
        <a>Managing subscriptions</a> 
        <a>Creating and managing packages</a> 
        <a>Responding to bookings</a> 
        <a>Managing schedule/appointments</a>
        <a>Communicate with customers</a>
        <a>Working with agencies</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="strip help-strip">
  <div class="container">
    <div class="columns">
      <div class="column-2">
        <h3>Agencies</h3>
        <div class="links"> 
        <a>Creating agency profile</a> 
        <a>Managing subscriptions</a>
        <a>Creating and managing packages</a>
        <a>Planing events</a> 
        <a>Make bookings</a>  
        <a>Respond to booking requests</a> 
        <a>Managing schedule/appointments</a>
        <a>Working with talents and businesses</a>
        <a>Messageboards and chat rooms</a>
        </div>
      </div>
      <div class="column-2">
        <h3>Talents &amp; Businesses working with Agencies</h3>
        <div class="links"> 
        <a>Agency requests</a> 
        <a>Sending/Responding to agency requests</a>
        <a>Creating bookings</a>
        <a>Creating appointments</a>
        <a>Terminate work contract</a>
       
        </div>
      </div>
    </div>
  </div>
</div>
<div class="strip help-strip">
  <div class="container">
    <div class="columns">
      <div class="column-2">
        <h3>Common Practices</h3>
        <div class="links"> 
         <a>Verifying profile</a>
        <a>Protecting your account</a> 
        <a>Changing country &amp; identification details</a>
       <a>Messaging &amp; notification settings</a>
        </div>
      </div>
      <div class="column-2">
        <h3>Privacy &amp; Legal</h3>
        <div class="links"> 
        <a href="<?php echo $url; ?>/support/privacy-policy">Privacy policy </a> 
        <a href="<?php echo $url; ?>/support/conditions-terms" >Conditions &amp; terms</a>
        <a style="display: none">General Data Protection Regulations (GDPR)</a>
        <a>Subscription payments</a>
        <a href="<?php echo $url; ?>/support/about">About Curatalent (Private) Limited</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="strip help-strip">
  <div class="container">
    <div class="columns ">
      <div class="column-3">
        <div class="service-icon"><img src="<?php echo $url; ?>/images/support/contacts.png"></div>
        <h3 class="center">Help Lines</h3>
     
        <div >Feel free to contact us any time. We'll respond within 72 hours for general informations, and within 24 hours for billing issues. </div>
        <div class="sep"></div>
        <div>
        	General Information : <a>info@curatalent.com</a>
        </div>
         <div>
        	Billing/Payments : <a>billing@curatalent.com</a>
        </div>
        <div class="sep"></div>
        Use contact form for fast response.
        <div class="sep"></div>
        <a class="button">Contact Us</a>
      </div>
      <div class="gap"></div>
      <div class="column-3">
        <div class="service-icon"><img src="<?php echo $url; ?>/images/support/idea.png"></div>
        <h3 class="center">Feedback &amp; Suggestions</h3>
        <div>We always value your feedback, suggestions, and comments. Feel free to send us your thoughts.</div>
         <div class="sep"></div>
          <div class="sep"></div>
        <a class="button">Send Your Feebcak</a>
      </div>
      <div class="gap"></div>
      <div class="column-3">
        <div class="service-icon"><img src="<?php echo $url; ?>/images/support/invest.png"></div>
        <h3 class="center">Investers</h3>
        <div>
        If you would like to invest on Curatalent, please shoot us an email or contact us via our contact form. Our agent will get in touch with you  to discuss further arrangements.
        </div>
        <div class="sep"></div>
        <div><a>info@curatalent.com</a></div>
        <div class="sep"></div>
        <a class="button">Contact Us</a>
      </div>
    </div>
  </div>
</div>
