<div class="strip help-strip notoppadding ">
  <div class="container space_block">
	  <div class="sep"></div>
	   <h1 class="txt-gray">Curatalent Platform Conditions and Terms</h1>
    <div class="sep2"></div>
    <p class="nopadding">
	    Our mission is to enable event organizers to easily find  talents and services they want as well as create a marketplace for talents and  service providers to showcase their skills and products.</p>
     <h2>Definitions</h2> <p class="nopadding">
  "<span class="txt-bold">We</span>", "<span class="txt-bold">Our</span>", "<span class="txt-bold">Company</span>" means, Curatalent private limited, a limited  liability company incorporated under low of Sri Lanka, and having it's  registered address at 21, Vishaka Gardens, Kurunegala 60000.<br><br>
  "<span class="txt-bold">Platform</span>" means Curatalent.com website which the service made  available to customers, owned, controlled, managed, maintained and hosting by  Curatalent private limited.<br><br>

  "<span class="txt-bold">Talent</span>" means the professional who works or provide service   in event management and entertainment industry.<br><br>
  "<span class="txt-bold">Agency</span>" means an agency or company doing business related  to the event management and entertainment sector.<br><br>
  "<span class="txt-bold">Service provider</span>" means the business which serves or provide  services/equipment in events.<br><br>
  "<span class="txt-bold">Event organizers</span>" means the person who want to find service  providers or talents from our platform to fulfil their need while organizing an  event, party , cooperate or social function.<br>
  <br>
  "<span class="txt-bold">You</span>" means the person who utilize one or more of our services  on our platform.</p>
      <h2>Scope &amp; nature of our service </h2>
      <p  class="nopadding">We have developed an online platform where talent and  service providers can register,  showcase  their skills and talents and event organizers can search/browse, make bookings  for talents and service providers. We also offer event planning tools through  our platform for event organizers. Agencies can showcase their products and  make bookings for their events. <br><br>
        By using our service, registering , creating profile and  talent/business pages, make bookings and using our platform event planning tools,  you are legally binding to a relationship with the company and considered as  you have accepted our conditions and terms.<br><br>
        When rendering our service, the information we collected on  our platform as well as other means are utilize according to our <a href="<?php echo Util::mapURL('/support/privacy-policy');  ?>">privacy policy  statement</a>.</p>
	  <h2>General usage policy</h2>
	  <p class="nopadding">
You may not use, or encourage, promote, facilitate, instruct, induce, or otherwise influence or cause others: (1) to use the Services for any activities that violate applicable law or for any other fraudulent or harmful purpose or (2) to transmit, store, display, distribute or otherwise make available content that is illegal, fraudulent or harmful to others.
If we found such incident, we have right to permanently close of your account. If we exercises its right to cancel you account, we shall not liable to any damages or loss of use, profit, revenue or data to you or any third party person. 
</p>
	  <h2>Talent and service provider membership program  (Subscriptions)</h2>
      <p  class="nopadding">
        Each subscription program or package offer certain number of  credits for "Accept" bookings (Booking credits) and number of credits to manage  pool of service providers or talents (Manage credits – applies only for Agencies) <br><br>
        Accept a booking means directly accept a booking request received  from a client or start a conversation/using message boards to contact the  client.<br><br>
        Manage credit means the credits you want to manage a pool of  service providers or talents, one credit per each talent or service provider.<br>
        <br>
        Each type of credit indicates the monthly capacity for you  to accept bookings or manage pool members. Remaining monthly credits will not  be roll over to the next month. It should be consumed within your billing month. <br>
        <br>
        Each subscription package has a price advertise on the  platform. Prices may vary depend on the subscription category and number of  credits. We reserve rights to amend the package prices time to time. We'll  notify registered members (talents, agencies and service providers) in advance on an incident  of changing package prices. <br><br>
        No refunds of fees already paid  will be given. If Curatalent exercises its right to cancel a membership, we  will not refund the membership fee already paid.</p>
	  <h2>Enrolling into talent  and service provider membership program</h2>
      <p  class="nopadding"> 
        Any professional or service  provider can register on our platform and enter our membership program. When you  are enrolling into the program, you are agreed to provide us legal proof  documents/evidence to prove that you have rights to market yourself or your  service under the profile name give to us. We may demand you to provide such  document time to time when we need to authenticate you.<br><br>
        We offer free monthly subscription  program for beginners, and we have rights to increase or decrease credits offer  in free program or cancel free subscription any time.<br><br>
        You can enroll up to 3 (three)  categories and choose one of the available free or paid subscription packages. </p>
	  
	  <h2>Talent and service provider public page</h2>
<p  class="nopadding">  
When you subscribe for the membership program, you may create a business profile for you or your business.  Public page is a part of your business profile.  You may showcase your experiences, achievements, video and portfolio on your public page. You are responsible for the  content you choose to display on your public page.  Company does not take any responsibility or copyright clams arise on the content displaying in your page. On such events, we may disclose owner’s identity to authorities if required.</p>	<br>
<p  class="nopadding"> 
<span class="txt-orange">It is strictly prohibited to display phone numbers and email addresses on your public page.</span> Since we have built a standard and formal method to your customers to reserve/purchase your service, it is not necessary to display your contact information in your public page. If we found emails and phone numbers displaying in your public page, we have right to close your profile without further notice. 
</p>	 
	  <h2>Upgrade, downgrade or cancel membership  programs</h2>
      <p  class="nopadding">
        You can upgrade or downgrade  your membership package any time.<br><br>
        When you are upgrading a package,  we'll cancel your existing package at the same time and calculate the prices  for remaining credits and time, then roll the balance (Money) into your new membership  package.  Then you have to pay the balance  payment for your new package.<br><br>
        When you are downgrading package,  we shall not refund your balance but you can enjoy the existing package up to next  billing date. Your new package will take effect from the next billing date. <br><br>
        When you are cancelling your  membership for a category, we'll not refund your paid membership fee, but you  have an option to continue the membership until the end of the billing  period.  If you decided to immediately  cancel your subscription and join into another category, we shall not roll the  balance credit or funds into new category.</p>
	
	  <h2>Prices and currency conversions</h2>
  <p class="nopadding">All prices displaying in this website has stored in United States Dollars (USD). Unless you are viewing website in service provider’s local currency, we are displaying converted prices based on daily conversion rate.  You may see a different prices than the service provider originally advertised price due to currency conversion rate fluctuation.</p> 

	  <h2>Taxes</h2>
      <p  class="nopadding">
        We have priced listed membership  packages including the taxes applicable in Sri Lanka. If there is any additional  taxes have to pay based on your location or region you work/live, we have  rights to  explicitly add taxes to your  subscription.</p>
	  <h2>Automatic membership renewal</h2>
      <p  class="nopadding"> 
        The Membership billing period begins  the day your enroll into a selected membership category (free) or  the day you subscribed to a paid package or  the day you upgrade your membership  package, whichever occurs last. <br>
        <br>
    We reserve rights to automatically  charge your monthly membership fee using the instrument (credit card or bank account)  you provided while you purchase the subscription package with our payment  service provider.  We do not store your payment  details (see our <a href="<?php echo Util::mapURL('/support/privacy-policy');  ?>">privacy policy</a>)</p>
	  <h2>Rights to change the fee  structure</h2>
      <p  class="nopadding">
        We reserve rights to  increase decrease or re-structure the free  structure. On such events, we'll notify existing members in advance notice. </p>
	  <h2>Refund policy</h2>
      <p  class="nopadding">
        No refunds of fees already paid  will be given. If Curatalent exercises its right to cancel a membership, we  will not refund the membership fee already paid. </p>
	  <h2>Payment issues</h2>
      <p  class="nopadding">
        If you experience any kind of  payment difficulties, contact us on <a href="mailto:billing@curatalent.com">billing@curatalent.com</a> or  alternatively you may contact us on  general customer service +94 70 507 7070 / whatsapp +94 70 507 7070.</p>
	  <h2>Disputes</h2>
    <p class="nopadding">
        If any disputes occurs and we  had to involved into resolve the dispute, we have right to take a decision and  deems it as final.   </p>
<h2>Limitations of liability</h2>
    <p>Company and our employees, agents, shareholders, or directors, shall not be liable for any special, incidental, indirect, punitive, or consequential damages or loss of use, profit, revenue or data to you or any third person arising from your use of the platform. This limitation of liability shall apply regardless of whether (i) you base your claim on contract, tort, statute, or any other legal theory, (ii) we knew or should have known about the possibility of such damages, or (iii) the limited remedies provided in this section fail of their essential purpose. This limitation of liability shall not apply to any damage that Curatalent may cause you intentionally or knowingly in violation of the platform terms and conditions or applicable law.</p>

    <p><a class="more-link" href="<?php echo Util::mapURL('/support'); ?>" >Support Home Page</a></p>
  </div>
</div>
