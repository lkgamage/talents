<?php
$categories = App::getSkills();
$types = Util::groupMultiArray($categories, 'is_business');
?>
<div class="strip help-strip strip-orange">
  <div class="container">
    <h1 class="txt-orange upper center">Talents &amp; Business Categories</h1>
  </div>
</div>

<div class="strip help-strip ">
  <div class="container">
    <h2 class="txt-orange">Talent Categories</h2>
	  <?php
	  $types[0] = Util::groupMultiArray($types[0], 'category');
	  foreach ($types[0] as $cat_id =>  $categories){
		  
		  echo '<h3 class=" category-head">'.DataType::$SKILL_CATEGORIES[$cat_id].'</h3>';
		  echo '<ul class="categories">';
		  
		  foreach ($categories as $cat){
			  
			  echo '<li>'.$cat['name'].'</li>';
		  }
	  		
	  
	  
	   echo '</ul><div class="sep"></div>';
		  
	  } 
	  
	  ?>


    <h2 class="txt-orange">Business Categories</h2>
	  <?php
	  $types[1] = Util::groupMultiArray($types[1], 'category');
	  foreach ($types[1] as $cat_id =>  $categories){
		  
		  echo '<h3 class=" category-head">'.DataType::$SKILL_CATEGORIES[$cat_id].'</h3>';
		  echo '<ul class="categories">';
		  
		  foreach ($categories as $cat){
			  
			  echo '<li>'.$cat['name'].'</li>';
		  }
	  		
	  
	  
	   echo '</ul><div class="sep"></div>';
		  
	  } 
	  
	  ?>
	  
	  <h2 class="txt-orange">Agency Categories</h2>
	  <?php
	  
		 
		  echo '<ul class="categories">';
		  
		  foreach (DataType::$AGENCY_TYEPS as $cat){
			  
			  echo '<li>'.$cat.'</li>';
		  }
	  		
	  
	  
	   echo '</ul><div class="sep"></div>';
		  
	   
	  
	  ?>
	  <p><a class="more-link" href="<?php echo Util::mapURL('/support'); ?>" >Support Home Page</a></p>
  </div>
</div>
