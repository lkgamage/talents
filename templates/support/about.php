<div class="strip help-strip strip-orange">
  <div class="container">
    <h1 class="txt-orange upper center">Let's Make It happening!</h1>
  </div>
</div>
<div class="strip help-strip">
  <div class="container">
	  <div>
    <h3 class=" txt-gray max-800 is-centered center quote"><span>Curatalent is the premier online entertainment industry platform which seeks to bridge the gap between customers, agencies, and talents/services.<br><br>
Our platform featuring wide range of tools including talent and service marketplace, event planning, talent and service booking, appointment scheduling and message boards. We make the process of finding and hiring talent/services for events simple and pain-free.</span></h3></div>
  </div>
</div>


<div class="strip help-strip ">
  <div class="container">
    <h2>Story Behind Curatalent</h2>
	  <div class="columns">
	  <div class="column-2 left-content">
	    <p>I involved into organizing a fundraising program for purchase an interactive smart board for nursery kids in 2014. It was an indoor concert.  We had face into many problems finding and contacting artist, catering services, digital and printing media institutes as none of us (organizers) have contacts with them. That wasn’t our working field. </p><p>
	      Again in 2017 and 2018 I have participated to organize another two programs. One of them is, providing school items and stationaries for primary kids. We have chosen an underprivileged school to provide all their individual needs from school bag, shoes to books and pencils. This photo was taken at the day we offer them those prices. It is one of my favorites. </p>
	    <p> While organizing these events, we found the same problem again. It was hard to find required services for our event.  It shouldn't be hard if we had a central hub to find them. That is how the idea was born.</p>
	    <p>I have met many event organizers, talked to many event management companies, and finally came up with a plan to build the platform.  Lots of event management and industry professionals contribute their ideas to build Curatalent platform. </p>
	    <p>There is another interesting story about Curatalent name.  I had originally named this as &quot;Talent Market&quot;, since we have extended features other than talent marketing, we needed a new name.  One of my business partners brought &quot;curation&quot; word to the discussion table. It was interesting as it is highly related to the business, but we needed an energetic business name. After hours of discussion over finishing many cups of teas, finally we all agreed to Cura + Talent name. 
        </p>
		  <div class="flex person">
		  <img src="<?php echo Util::mapURL('/images/support/lasantha.jpg'); ?>" alt="Lasantha Gamage" >
			  <div>Lasanth Gamage
			  <span>Founder</span>
			  </div>
		  </div>
		  </div>
	  <div class="column-2 figure">
	    <img src="<?php echo Util::mapURL('/images/support/school-boy.jpg'); ?>" alt="School boy impatiently waiting for his gift - Nelumpathwawa, Kurunegala, Sri Lanka" >
		  
	    </div>
	  </div>
  </div>
</div>
<div class="strip help-strip">
  <div class="container bg-gray padding">
	  <div class="padding">
    <p class="nopadding">There are two meaning for <span class="upper txt-bold">happening</span>,</p>

    <ul>
      <li>An event or occurrence </li>
      <li>A partly improvised or spontaneous piece of  theatrical or other artistic performance, typically involving audience  participation. </li>
    </ul>
    <p class="nopadding">We choose it as the company slogan for both reasons.  We want to make our clients’ events happening, for real!  </p>
	  </div>
  </div>
</div>
<?php
include(Config::$base_path.'/templates/support/service_part.php');
?>
