
<?php $base = Util::mapURL(''); ?>

<div class="strip help-strip">
  <div class="container">

	  <div class="columns">
	  
	  	<div class="column-2 left-content">
		  <h3  class="txt-orange">Agencies</h3>
		  <p>Managing an entertainment industry  business is challenging. You must be updated with latest market trends while finding  businesses and organizing talent/services for clients' events, all at the same time.</p><p>
Curatalent has born to supercharge  your business. With our system, you can find businesses as well as the best  talents/services for each event request immediately and keep track of the  entire business process from an initial meeting to a finished project.</p>
          <p>We use cutting edge technology to  streamline the event management process. Our platform allows you to manage your  entertainment industry businesses, communicate with your team, and facilitate  collaboration between all involved.</p>
          <p class="nopadding">Curatalent platform offers you,</p>
          <ul>
            <li>Find talents  and services you need for your projects.</li>
            <li>Intelligent booking management, change and  cancel bookings</li>
            <li>Smart appointment management</li>
            <li>Event planning tools including budget and  bookings</li>
            <li>Event agenda planning </li>
            <li>Maintain a pool of talents and service providers</li>
            <li>Directly working with talents/services  on bookings and appointments</li>
            <li>Booking message boards and event message  boards for collaborative planning </li>
            <li>Free agency subscription</li>
          </ul>
          <p>We are adding more tools to make your business more easier and streamlined  process.</p>
        </div>
		 <div class="column-2 right-content">

			<h3 class="txt-orange">Curatalent is for everyone</h3>
<h4>Are you here looking for your  favorite artist or music band to hire into your next party or event? </h4><p>Welcome aboard!<br>You may search all  talents and services in your area.  Find  your <a href="<?php echo $base; ?>/search">artist or service</a>. <br>If you can&rsquo;t find them,  Share this on social media and tag  him/her/them to invite into your next party. It is a favor for them as well as  you could be their favorite fan!<br>
Meanwhile, you may <a href="<?php echo $base; ?>/dashboard/events">plan your event</a>  and <a href="<?php echo $base; ?>/search">find services</a> for your party. </p>
            <h4>Are you a singer, dancer, comedian,  DJ, photographer, beautician…. (<a href="<?php echo $base; ?>/support/categories">see full list here</a>) or  activity planner or outdoor activity guide ?</h4>
            <p>
              We warmly welcome you to the platform!<br><a href="<?php echo $base; ?>/join">Signup</a>, create your talent profile, subscribe  for your category free!  Curatalent is a  place where the best and sometimes the most unexpected things in life take  place.</p>
            <h4>Are you an event related business  owner,  Such as food supplier, beauty salon,  venue decoration, sound and light equipment renting, cleaning service… (<a href="<?php echo $base; ?>/support/categories">see  full list here</a>) ?</h4><p>
              Welcome to platform! <a href="<?php echo $base; ?>/join">Signup</a> and  create your business page, subscribe for your category  free. <br>
              We are here for giving the best  social exposure to your business and boosting your exposure in a way that  guarantees success in your industry. We will not allow you to waste time doing  things that don't work in this digital age.</p>
            <h4>Do you have a outdoor garden, swimming  pool , beach garden, auditorium or reception hall to short term rent for events?</h4> <p>
              Welcome aboard!<br>
              We were looking  for you. <a href="<?php echo $base; ?>/join">Signup</a>, choose the venue category for your property. Post cool  pictures on your business page. We&rsquo;ll take care of the rest. You just have to  sit back and see how we bring businesses to your property.</p>
            <h4>Are you an agency doing business  in entertainment industry?</h4> <p>
              Welcome to platform!<br>We have  special tools designed and build for boost your business.  You may find clients while hiring talents and  services, planning events and many more. <br>
              <a href="<?php echo $base; ?>/join"><a href="<?php echo $base; ?>/join">Signup</a></a> and create your agency page.  Curatalent's platform will help agencies and  event planners to find and connect with  talents and services more easily, facilitating the exchange of value among  different stakeholders.</p>

	    </div>
	  </div>
	  <p><a class="more-link" href="<?php echo Util::mapURL('/support'); ?>" >Support Home Page</a></p>
	</div></div>
