<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo (!empty($subject)) ? $subject : 'Curatalent'; ?></title>
<style type="text/css">
img {
	max-width: 100%;
}
body {
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: none;
	width: 100% !important;
	height: 100%;
	line-height: 1.6em;
	color: #333;
}
body {
	background-color: #f6f6f6;
}

@media only screen and (max-width: 640px) {
body {
	padding: 0 !important;
}
h1 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h2 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h3 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h4 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h1 {
	font-size: 22px !important;
}
h2 {
	font-size: 18px !important;
}
h3 {
	font-size: 16px !important;
}
.container {
	padding: 0 !important;
	width: 100% !important;
}
.content {
	padding: 0 !important;
}
.content-wrap {
	padding: 10px !important;
}
.invoice {
	width: 100% !important;
}
}
</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
<table class="body-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
  <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
    <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0;" valign="top"></td>
    <td class="container" width="600" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top"><div class="content" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
        <div class="header" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; width: 100%; clear: both; color: #999; margin: 0; padding: 5px;">
          <table width="100%" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
              <td class="aligncenter content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top"><img src="https://www.curatalent.com/images/email/curatalent-logo.png" alt="Curatalet Private Limited" /></td>
            </tr>
          </table>
        </div>
        <?php
			if(!empty($template)) {
				include(Config::$base_path.'/templates/emails/'.$template.'.php');
			}
		?>
        <div class="footer" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px 0;">
          <table width="100%" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
              <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; margin: 0; padding: 0 0 20px;"  valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; width:100%;">
                  <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
                    <td width="60%" style="width:60%; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; padding-left:10px;"><a target="_blank" href="https://curatalent.com" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">&copy; curatalent.com</a></td>
                    <td width="40%" class="aligncenter content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px; widows:40%;" align="center" valign="top"><table width="100%"   border="0" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0; width:100%;">
                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; padding:0">
                          <td  width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.facebook.com/curatalent" target="_blank"><img src="https://www.curatalent.com/images/email/facebook.jpg" alt="Facebook" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://twitter.com/curatalent"  target="_blank"><img src="https://www.curatalent.com/images/email/twitter.jpg" alt="Twitter" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.linkedin.com/company/curatalent"  target="_blank"><img src="https://www.curatalent.com/images/email/linkedin.jpg" alt="LinkedIn" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.pinterest.com/curatalent"  target="_blank"><img src="https://www.curatalent.com/images/email/pinterest.jpg" alt="Pinterest" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.instagram.com/curatalent"  target="_blank"><img src="https://www.curatalent.com/images/email/instagram.jpg" alt="Instagram" /></a></td>
                        </tr>
                      </table></td>
                  </tr>
                  <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0;">
                    <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; padding-left:10px;"><p style="margin:0; padding:0; line-height:1.4em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; color:#999;">Curatalent Private Limited</p>
                      <p style="margin:0; padding:0; line-height:1.4em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; color:#999;">21, Vishaka Gardens, Kurunegala, Sri Lanka.</p></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td align="center" style="text-align:center"><a  target="_blank"  href="https://curatalent.com/dashboard/settings" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">Manage email preferences</a></td>
            </tr>
          </table>
        </div>
      </div></td>
    <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0;" valign="top"></td>
  </tr>
</table>
</body>
</html>
