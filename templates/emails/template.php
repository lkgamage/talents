<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo (!empty($subject)) ? $subject : 'Curatalent'; ?></title>
<style type="text/css">
img {
	max-width: 100%;
}
body {
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: none;
	width: 100% !important;
	height: 100%;
	line-height: 1.6em;
	color: #333;
}
body {
	background-color: #f6f6f6;
}

@media only screen and (max-width: 640px) {
body {
	padding: 0 !important;
}
h1 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h2 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h3 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h4 {
	font-weight: 800 !important;
	margin: 20px 0 5px !important;
}
h1 {
	font-size: 22px !important;
}
h2 {
	font-size: 18px !important;
}
h3 {
	font-size: 16px !important;
}
.container {
	padding: 0 !important;
	width: 100% !important;
}
.content {
	padding: 0 !important;
}
.content-wrap {
	padding: 10px !important;
}
.invoice {
	width: 100% !important;
}
}
</style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
<table class="body-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
  <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
    <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0;" valign="top"></td>
    <td class="container" width="600" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top"><div class="content" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
        <div class="header" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; width: 100%; clear: both; color: #999; margin: 0; padding: 5px;">
          <table width="100%" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
              <td class="aligncenter content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMoAAAAYCAYAAAC7k2KMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3FpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoxYjFiMjM0OC01YmNmLTkxNGQtYTk0Yy1mOTc2NDQ5ODFiYTkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RkI0MjRGOUUxNTM4MTFFQzgzMjE4QURDMTk3NUVCNEUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RkI0MjRGOUQxNTM4MTFFQzgzMjE4QURDMTk3NUVCNEUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjFiMWIyMzQ4LTViY2YtOTE0ZC1hOTRjLWY5NzY0NDk4MWJhOSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxYjFiMjM0OC01YmNmLTkxNGQtYTk0Yy1mOTc2NDQ5ODFiYTkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz53e17JAAAGnElEQVR42uxcT2gcVRh/s5um5CBkVIQGJZtJPPQgbd2AiIo5bPVoBTftMV42RQtWPMxWPESkdgeEKtZiF0SQHox70JOXzkGhBw8ZKniU/KMh9SBM8aCN3ez4zTqbzn755v3Zmczu0P3gNbPz77197/t93+/33ttqTNEKF7eOs1z+7saFIxsqz3mex4Y2tKxaTvWBlsbmWqy5MOy6oT1MNqL6gJbTXvY0bQ4Ol+JUPGltG/CnDKUCxQhdcqFYUGwoTlSCQp8XodQj7l1F7w/fW4JyQ6K5a8EzjeBY1m4EdXSsHtSPzYRSi9GdfpumifPU95uW+A643dVgTGRMtk9ZRLtw3b4PzPbY1rg0ZnHTnKgrZ5TC0vo41HwKqh8vXLpTiAESM3DgGnJi3/Tg/AqUawMSUIygTasKDm2gAWRBYNBTbHdZ8twgWzEIptmgXr7GaI0eWnhAwbx3egRJTcHZKj1Gp4M02ehPOaSeoqNG1VVh2bNaygEmHvVq0669D94CgOddTdNUQFIOHC1sdlDCEaSMUrmpkPrjWjVioDBY6gIKU+acxzTRiai3hLLSGodiUs/qnExnp+hnVYl7XAHoaxG0tZd68XhGjaWjDJSpDzfGvbHR1/ZYH9CvqdofPv3aUIwMYfM5/3zEfSZyzLSAQtVjB1QQO7zFoQtFjgMbaGBsjuNioMj2Q1lwLU2gJDF2lcChnQTq3eeHoEXsRKjX7ujhOQBHV/rw6ZfstG+QTQxChMt8wTQpC4uILA4RmWWddC1lnYD7y0URO22tlCQFG2yNkv9351bOy73UXbTPFKgXjrAOoNjlpGEVx0wLLCrRjxcQDlonVAhqYQ9Q4OnVSv3SWCPueTbJmuxXOB7n3vnnFH3+nLCOf+D9rxzbDxRR6p8dwNkXGU6No3WHVjmhdxy0TigTfY3BUVbQO/0OUEWUVRoCTZN8RtE/ZZv6Faa3GPs58bdr7BaooKP6l+wmkRFclh2jNIej4KQdPZYG/cJtdUNgdQmtlIZ5nCKa1WwQ2dDsG/V67AqbYy12BpreTKRjWuwr/XP2rA/EjKV3kygrhOZoSGgDForatiDzHFQ2qSPApKmVkrJFYoyKqVKvrlG+ypbhz7L7NvsdssEM4f5NgNZve2LeY3+R6GuyD4IskkUTCUbf4eYltYETiuJOADAjBKpSBOCS1CcOym6YflkZGBO/36pobPzjk30Byh5gvmBPA1i+B7CcQlRqBDLFI/519nCaHUS3Nclo3iA+m+j+JIGCs5SL3u8fXyNomnPA/VYVBB4Zs1j3dqdUhX3kOgqA4XX3LXYaMsjX8HEsBJYZ9xz7GwDzZpCBVKKCgWhK1oy314vSMQYChh7h2G6CQMFOaArGoZwCUKwEAfcdyipraQw8d3r4xBPrv5x4fP1oW5R32xg8+W0766jNXmAn4tkqEn1mShMBWqjUCW4s66QdGlQLlYoEVerVKH1koPqpvXWVjAWqBvrOxb4DZXf08NnW2Oj6MxO3v2kLfbwbE6gZZBe3PcWsDpTSpLWtcwbdICIhdcwDHfUeFZBZggyRhMMlJagrCQJskK3aj0q5QNFy3hv+anxOy10+9uTWGSBq/mLKHXTbOEj8dQDMed67Ns0JTFt0jnA2Cee2kVbATmJIvId6VkQXqayCAV6KQSWLCUXFUoKULQvCPlUT7fUqPACN9lww1TtBCH2fplx2z7IVwWyXhcRkx8FtNOAl4rlwJqgT3H8FZYAio6dKVWmbhUSyHrTb4jia6DcUKwgccXUCtaV/njNRUEZcX6SVZEDI61sz5vMiYd8/oDz18fYlYFp711ue9+M+oa+x6+2ZsI7l2Xvw701OVqkD3cK/LygJBsIhKBA1XcjLUHEiUWcjYg0NfGdgdYnZLiqrFVHAiBMlKRHfENRPUTeLAxQRWHir5TJ7tFRX2/3Zx9R+ghFJvTRt9/nOMbCvu/md+x91EdurbBkAc6hL6GvsVVGFAJZFhVmQOicyWwrO5TvGyRiTADjahVeHqYXDhoRTJKkTKor1U0DK2t4vmyW/BtWDRtFyL4Dje5BKfsjf2zm+sTRFrrD7q++hiDEG9OtFCbD4Dj4dOLobEflnmfj3Bz5YHg3udyKuzwcgiTON6BLg7qwOUwt8orqoe+JMBhg96DCb0EqljIGlylLaCkVu/S1c3DrdymvXvZb2ye33Jy5IeVJnc6XHfvKpGb4+/F9YhpZlIzVKM+cdGbl3fyYqi5CZ5X+hr4tmv4Y2tCzafwIMAOyl+LoIA9aYAAAAAElFTkSuQmCC" alt="Curatalet Private Limited" /></td>
            </tr>
          </table>
        </div>
        <?php
			if(!empty($template)) {
				include(Config::$base_path.'/templates/emails/'.$template.'.php');
			}
		?>
        <div class="footer" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px 0;">
          <table width="100%" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
              <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; margin: 0; padding: 0 0 20px;"  valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; width:100%;">
                  <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
                    <td width="60%" style="width:60%; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; padding-left:10px;"><a target="_blank" href="https://curatalent.com" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">&copy; curatalent.com</a></td>
                    <td width="40%" class="aligncenter content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px; widows:40%;" align="center" valign="top"><table width="100%"   border="0" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0; width:100%;">
                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; padding:0">
                          <td  width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.facebook.com/curatalent" target="_blank"><img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QN6aHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6ZTVhZDczYTktNDViOS02ZjQ3LWI2MGEtZTYxNjc1OWVmNzgzIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMzOEFDQzM3MzVEMDExRUNCREIyQ0Y3RTJEMzdCRDQyIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMzOEFDQzM2MzVEMDExRUNCREIyQ0Y3RTJEMzdCRDQyIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDplNWFkNzNhOS00NWI5LTZmNDctYjYwYS1lNjE2NzU5ZWY3ODMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6ZTVhZDczYTktNDViOS02ZjQ3LWI2MGEtZTYxNjc1OWVmNzgzIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAFAAUAwERAAIRAQMRAf/EAGcAAAMBAAAAAAAAAAAAAAAAAAIGBwQBAQAAAAAAAAAAAAAAAAAAAAAQAAAFAwMCAQ0AAAAAAAAAAAERAhIDABMEFAUGITEVUYGRIjJSYkNzwyUHNxEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8AKeebImXPOtUk0ihVJIoTERHqIiI0D7x7iHFIeHo5JyOSZUeQsUxRQiRACxQAEAGKhFIj3IqBP16buhvzeCam7YcPsOJxdnMoM+3z48GfjT5EV/HilQuaESJaEqAVJ6+8HSgtOXyDjSP1/ibnJs6V7TJI2LbSQSFXFgfZvdIj56CJXU6i6xLXvtl6pGbS8lAefotbPoX6N6rF0nsPo4qCh7n/ABXbfr/floET8L4L87xe98Nmy30mdB//2Q==" alt="Facebook" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://twitter.com/curatalent"  target="_blank"><img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QN6aHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6NmMyYjI2ZGUtNmUxOC1mNTRjLWExMjktYTQ0NWY1ODQyZDc3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRBQzA5RDZEMzVEMDExRUM5RkI3OTQ1Rjc0RjhFRkE1IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRBQzA5RDZDMzVEMDExRUM5RkI3OTQ1Rjc0RjhFRkE1IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2YzJiMjZkZS02ZTE4LWY1NGMtYTEyOS1hNDQ1ZjU4NDJkNzciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NmMyYjI2ZGUtNmUxOC1mNTRjLWExMjktYTQ0NWY1ODQyZDc3Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAFAAUAwERAAIRAQMRAf/EAGYAAQEAAwAAAAAAAAAAAAAAAAQFBgcIAQEAAAAAAAAAAAAAAAAAAAAAEAAABAUDAgQHAAAAAAAAAAABEQIDABITBAUhFBVRBkGBQgeRwSIyYkMlEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBmzy2QZu8pTcuW2VpG8uPuFKnZhBSvEhlHWAr4T2+7lzWLVkrFlCmJhS2lawQpwUiQyHpoPUQgJNTJFw1dVGuVCcadU5JukBmXs7kn7XNXFttnHba7QlLjzba3AbWkRlnFOiUiY6jAbH7w7jxfbeCfIW27l1C02dqggFTiz+qUPSAiah+cBzxAOWfJvcJuKUytvKdWmehyQBrndVlbqev66pz+c2sAr+Lwv7uXrfjRoy/EzgP/2Q==" alt="Twitter" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.linkedin.com/company/curatalent"  target="_blank"><img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QN6aHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6NmZjMThhZWQtOTY4ZC1iNjQyLThhZDItYmY4M2Q1ZGQ1MDhhIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjU0NDFCQ0RBMzVEMDExRUNBMDI4QkY1RjE1MDZBODhEIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjU0NDFCQ0Q5MzVEMDExRUNBMDI4QkY1RjE1MDZBODhEIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2ZmMxOGFlZC05NjhkLWI2NDItOGFkMi1iZjgzZDVkZDUwOGEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NmZjMThhZWQtOTY4ZC1iNjQyLThhZDItYmY4M2Q1ZGQ1MDhhIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAFAAUAwERAAIRAQMRAf/EAGwAAAIDAQAAAAAAAAAAAAAAAAIFAwQGBwEBAAAAAAAAAAAAAAAAAAAAABAAAAMHAgMECwAAAAAAAAAAAQISABEDEwQUBSEVMUEiUXEGFoGxQoKyI3NEJQc3EQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBzAoqCvw+SytdkThlYcQBhU4kMeaoQUYT+kWBVa1TjDJO4gKP0joHaOmjAV/W2VjPPZrmyFChbnKcwbrwp/MfEX14frhsG4rs9kYf7MocKQ5Qx1TSGPHhIKImMBYpgETOVogObByKypvPFigLbc5EvlLuEpd3MFOpmLq9rudpmC5T3Jf0zE9L+DAJt7viLub5Py3zJqdeHtO4sE347bvufMNz7qfjWtg//2Q==" alt="LinkedIn" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.pinterest.com/curatalent"  target="_blank"><img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QN6aHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6N2IzNjRjYTYtMzdmNy0wYzQ5LThjMGQtNTRhMzg5MmFiOTdmIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQ1RjExMkIxMzVEMDExRUNBRTU5OTFDREMxODQ3RkJBIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQ1RjExMkIwMzVEMDExRUNBRTU5OTFDREMxODQ3RkJBIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo3YjM2NGNhNi0zN2Y3LTBjNDktOGMwZC01NGEzODkyYWI5N2YiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N2IzNjRjYTYtMzdmNy0wYzQ5LThjMGQtNTRhMzg5MmFiOTdmIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAFAAUAwERAAIRAQMRAf/EAGgAAQADAQAAAAAAAAAAAAAAAAUDBAYHAQEAAAAAAAAAAAAAAAAAAAAAEAAABAUDAgILAAAAAAAAAAABEQIEEgMTFAUAFQYhMUEWYXGBkaFiQ1M0JQcRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AG8JhHGfcvHr14DZm1CvkchONcMY9AAO6lrHsGgWb8T4tlJ44/E5NzKyopFTeU/khLlzhAIiSpImkw6gegzZ5g9jim/kQ2RiVc6ZQ9jPpoG+JZBtMx7vAzAEHGQctFsxBMSVTETgAZa/QIKPQbXN4h9O/oieQO1S22BxtNQvhmIhGgkxQQDFHVNJFoOe7/K86b9ANG/u6fjBVjL1w6CzxO08+MtvO2uRta/ciGCOH2aAPI3l85uzua0yuf3Yhj+Ogm/S7L9bd63y0aMPvM9B/9k=" alt="Pinterest" /></a></td>
                          <td width="20%" align="center" style="padding:0; margin:0; text-align:center;"><a href="https://www.instagram.com/curatalent"  target="_blank"><img src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QN6aHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MDYyYWE3Y2YtM2NkMy0yMTQ5LWFmMDgtZTY3YWNjYzc0YmExIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRGOTVCNTkzMzVEMDExRUM4QzZFRjAwQUYwOEJFRjZEIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRGOTVCNTkyMzVEMDExRUM4QzZFRjAwQUYwOEJFRjZEIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowNjJhYTdjZi0zY2QzLTIxNDktYWYwOC1lNjdhY2NjNzRiYTEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDYyYWE3Y2YtM2NkMy0yMTQ5LWFmMDgtZTY3YWNjYzc0YmExIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAFAAUAwERAAIRAQMRAf/EAGIAAQEBAAAAAAAAAAAAAAAAAAUEAwEBAAAAAAAAAAAAAAAAAAAAABAAAAQFBAAFBQAAAAAAAAAAAQIDBBESExQFACEVFkFRcSJikSNDJQYRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AG2TJq6arZjMLOFQVcUEkkJTLKrGLOYRMeIAAAIeAiIjoFeqYioVF03yOKFUABJw7MgJJhMUkJJUxMICcBEpRmhvDQASZa469WNLdUreYadeanGHroGv5xdZHCtHjduZ2bGZS4coJAInBM6RQKbYBhumMDeeg3aNCgUuMxhXr4756g4UUcICkVEqQm3H3HiYZ/cbYIBoCL1t3e+nC15OvU8KdxNN6Q0EpLjlluvXUsxrelNXpx2jT30FL3u1se95K2h9ytXpw+U20NBH+l4X83L1vjRoy/WMdB//2Q==" alt="Instagram" /></a></td>
                        </tr>
                      </table></td>
                  </tr>
                  <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0;">
                    <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; margin: 0; padding-left:10px;"><p style="margin:0; padding:0; line-height:1.4em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; color:#999;">Curatalent Private Limited</p>
                      <p style="margin:0; padding:0; line-height:1.4em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; color:#999;">21, Vishaka Gardens, Kurunegala, Sri Lanka.</p></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td align="center" style="text-align:center"><a  target="_blank"  href="https://curatalent.com/dashboard/settings" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">Manage email preferences</a></td>
            </tr>
          </table>
        </div>
      </div></td>
    <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0;" valign="top"></td>
  </tr>
</table>
</body>
</html>
