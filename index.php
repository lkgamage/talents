<?php

/**
 * @package Amplify
 * Root request handing class
 */

include_once('lib/autoload.php');

//date_default_timezone_set('Asia/Colombo');
date_default_timezone_set('UTC');
/*
header("content-security-policy:	default-src * data: blob: filesystem: about: ws: wss: 'unsafe-inline' 'unsafe-eval'; script-src * data: blob: 'unsafe-inline' 'unsafe-eval' https://js.stripe.com; connect-src * data: blob: 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; frame-src * data: blob: ; style-src * data: blob: 'unsafe-inline'; font-src * data: blob: 'unsafe-inline';");
*/
if(!empty($_SERVER['HTTP_HOST']) &&  $_SERVER['HTTP_HOST'] == 'curatalent.com'){
	Config::$base_url = 'https://curatalent.com';
}

//header("X-Powered-By: Amplify P2P V3.2");

// set debug level
if (Config::$debug) {
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	//error_reporting(E_ALL);

     //E_ERROR | E_WARNING | E_PARSE); //E_ALL & ~E_STRICT
} else {
    ini_set('display_errors', 0);
	ini_set('display_startup_errors', 0);
}

error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED);

// creating request object for this request
$request = new RequestObject();

// mapping request to correct controller
// if you wish to handover control to another script
// add it to the switch below

if (!empty($request->parameters[0])) {

    switch (strtolower($request->parameters[0])) {

        /*case 'login' 	:
        case 'logout' 	:
        case 'register'	:
        case 'pwreset'	: $handler = new AuthController( $request); // authentication handling (will be replaced soon)
                    break;*/

        /**
         * Dashboard and admin requests follow this pattern:
         * /dashboard|admin/{Page Name}/{Site ID}
         */
       // case 'dashboard':
		case 'dash':
       		$handler = new DashController($request);
            break;
		case 'sd':
       		$handler = new AppDataController($request);
			break;
		case 'cd':
       		$handler = new ClientDataController($request);
            break;
		case 'ev':
       		$handler = new EventDataController($request);
            break;
		case 'ad':
       		$handler = new AgencyDataController($request);
            break;
		case 'ipg':
       		$handler = new IPGController($request);
            break;
		case 'chat':
       		$handler = new ChatController($request);
            break;        
        
        default :
            $handle = new PageController($request);
    }
} else {
    $handle = new PageController($request);
}

?>