<?php

include('../lib/autoload.php');

$term = !empty($_GET['term']) ? filter_var($_GET['term'], FILTER_SANITIZE_STRING) : '';

if(empty($term)){
	$term = isset($_GET['path']) ? filter_var($_GET['path'], FILTER_SANITIZE_STRING) : '';
}

if(!empty($term)){
	
	$db = new Database();
	$term = $db->escape($term);
	
	$data = $db->query("select tag as label from tags where tag like '%".$term."%' order by CHAR_LENGTH(tag) limit 10 ");
	
	header('Content-Type: application/json');
	
	echo json_encode($data);
}


?>