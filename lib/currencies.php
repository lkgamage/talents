<?php 
/**** Auto generated currency exchange class 2021-11-25 19:38:51***/

class Currencies {

public static $USD = 1;
public static $AUD = 1.392408;
public static $CAD = 1.267308;
public static $CNY = 6.386528;
public static $EUR = 0.891027;
public static $INR = 74.49648;
public static $JPY = 115.30785;
public static $LKR = 196;
public static $MYR = 4.229529;
public static $RUB = 74.763878;
public static $GBP = 0.751092;
public static $CHF = 0.933797;
public static $THB = 33.409962;

}
?>