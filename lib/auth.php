<?php

class auth {

/** Session cookie encription key */
    private $session_encript_key = 'T7eG8AN7Pro13ct';

    /** Password encription key */
    public $password_encript_key = "LGQ0NUTGNI9";

    /** Session cookie name **/
    private $session_key = "TLMK";

    /** Session validity period, default 1 month */
    private $session_lifetime = 2592000;

    /** Active session */
    private $session;	
	
	/** Currently logged in user id **/
	public $user_id = NULL;
	
	/** Active login ID */
	public $login_id = NULL;	
	
	/** Device Fingureprint **/
	public $fingureprint = NULL;
	
	/** Login error message */
	public $error = '';
	
	/** Status code **/
	/**
		0 : not authenticated
		1 : authenticated
		2 : Invalid username
		3 : invalid password
		4 : invalid verification code
		5 : code expired 
		6 : account disabled
		7 : login disabled
	*/ 
	public $code = 0;
	
	function __construct () {
        $this->getSession();
		
		//print_r($this->session);
    }
	
	
	/**
	* Authenticate user by given username and password
	*/
	public function pwdlogin ($login, $password){
		
		if($login->isDeleted() || $login->blocked == 1){
			$this->code = 7;
			$this->error = "Login disabled";
			return false;
		}
		
		$customer = $login->getCustomer();
		
		if($customer->isDeleted()){
			$this->code = 6;
			$this->error = "Account disabled";
			return false;
		}
	
		$password = trim($password);		
			
		if($login->is_verified == 1 && $login->password_login == 1){
						
			$enp = $this->encriptPassword($password);
			
			if($login->password == $enp){
				$this->code = 1;
				return $this->authenticate($login);	
			}
			else{
				$this->code = 3;
				$this->error = "Invalid password";
				return false;	
			}
			
		}
		else {
			$this->code = 7;
			$this->error = "Login not verified. Use OTP login";
			return false;	
		}
						
		return false;
	}
	
	
	 /**
     * Create sessoin for given user
     * user : user object
     */
    public function authenticate ($login, $device) {

        // create session
        $session = array();

		$session['uid'] = $login['id'];
		$session['logid'] = $login['login_id'];
		$session['fp'] = $device->fingureprint; 
        $session['login'] = time();
        $this->session = $session;

        $this->setSession();

		return true;

    }
	
	
	 /**
     * Logout from the system
     * Clear all session cookie and redirect to given url or site home
     * Return nothing
     */
    public function logout () {

        $this->session = null;
		Session::clearProfile();
		Session::clearCustomer();

        $this->clearSession();
    }
	
	/**
	* Check if user has authenticated
	*/
	 public function isAuthenticated (){
		
		if (!empty($this->session)) {

            return $this->session['expire'] > time();
			
		}
		
		return false;	 
	 }
	 
	
	
	/**
	* Refresh session
	* It will update cookie exp. date and continuesly logged in 
	*/
	public function refresh () {
		
		if($this->isAuthenticated()){
			$this->setSession();
			return true;	
		}
		
		return false;
	}
	
	/**
	* Get currently logged in user
	*/
	public function getCustomer () {
		
		$customer_data = App::getCustomerSession($this->user_id, $this->fingureprint);		
		
		if(!empty($customer_data)) {
	
			$customer =  new Customer();
			$customer->populate($customer_data);

			if(!$customer->isDeleted() && $customer->active == 1){
				return $customer;
			}
		
		}
		else{
			// login revoked
			$this->logout();	
		}
		
	}
	
	 /**
     * Remove session cookie and clear session
     * User no longer tread as loged in user
     * Return nothing
     */
    private function clearSession () {

        // send expired cookie
        setcookie($this->session_key, "", time() - $this->session_lifetime, "/");
    }
	
	 /**
     * Create session cookie
     */
    private function setSession () {

        if (!empty($this->session)) {

            $this->session['expire'] = time() + $this->session_lifetime;

            $cookie = json_encode($this->session);
            $cookie = Util::encrypt($cookie, $this->session_encript_key);
            $cookie = base64_encode($cookie);
			$cookie = trim($cookie,'=');
			
            // setting cookie
            setcookie($this->session_key, $cookie, $this->session['expire'], "/");
        }
    }
	
	/**
	* Read session cookie
	*/	
	private function getSession () {

        $this->session = null;

        if (isset($_COOKIE[$this->session_key])) {

            $cookie = base64_decode($_COOKIE[$this->session_key]);
            $json = Util::decrypt($cookie, $this->session_encript_key);

            $session = json_decode(trim($json), true);
			
            if ($session['expire'] > time()) {

                $this->session = $session;
				$this->user_id = $session['uid'];
				$this->login_id = $session['logid'];
				$this->fingureprint = $session['fp'];

                return true;
            } else {
                $this->clearSession();
            }
        }

        return false;
    }
	
	/**
     * Get encripted password from plain text password
     * Return encripted password
     */
    public function encriptPassword ($password) {

        $password = Util::encrypt($password, $this->password_encript_key);

        return md5($this->password_encript_key . sha1($password));
    }
	
	
	
	
}

?>