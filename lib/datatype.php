<?php

class DataType {
	
	
	
	/****** Free booking threshold for month (starting from month's 1st date) ******/
	public static $FREE_BOOKINGS = 5;
	
	
	/****** customer types **************/
	
	/** Customer talent (t) *****/
	public static $CUSTOMER_TALENT = 't';
	
	/** Customer agency (a) *****/
	public static $CUSTOMER_AGENCY = 'a';
	
	/** Customer booker (b) *****/
	public static $CUSTOMER_BOOKER = 'b';
	
	
	/******  session types ***************/
	
	/** Session Type: hourly (h)**/
	public static $SSTYPE_HOUR = 'h';
	
	/** Session Type: day (d)**/
	public static $SSTYPE_DAY = 'd';
	
	/** Session Type: show (s)**/
	public static $SSTYPE_SHOW = 's';
	
	/** Session Type: session (t) **/
	public static $SSTYPE_SESSION = 't';
	
	
	/** Get array of session type descriptions **/
	public static $SSTypes  = array(
					'h' => 'Hourly',
					'd' => 'Per Day',
					's' => 'Per Show',
					't' => 'Per Session'
				);
				
	/** Service charge **/
	public static $SERVICE_CHARGE = 1;
	
	/** Booking Statuses **/
	
	/** Talent approval pending: AP **/
	public static $BOOKING_PENDING = 'AP';
	
	/** Talent accepted booking : BA **/
	public static $BOOKING_ACCEPTED = 'BA';
	
	
	/** Booker payment pending: PP **/
	public static $BOOKING_PAYMENT = 'PP';
	
	/** Booking confirmed: CO **/
	public static $BOOKING_CONFIRM = 'CO';
	
	/** Booking completed: CM **/
	public static $BOOKING_COMPLETE = 'CM';
	
	/** Booking cancelled: CN **/
	public static $BOOKING_CANCEL = 'CN'; 
	
	/** Booking Rejected : RJ **/
	public static $BOOKING_REJECT = 'RJ';
	
	/** Booking Blocked on dispute : BK **/
	public static $BOOKING_BLOCKED = 'BK';
	
	/** Booking request expired ***/
	public static $BOOKING_EXPIRED = 'EX';
	
	/** Booking status array */
	public static $BOOKING_STATUS = array(
		'AP' => 'PENDING',
		'BA' => 'ACCEPTED',
		'PP' => 'PAYMENT PENDING',
		'CO' => 'CONFIRMED',
		'CM' => 'COMPLETED',
		'CN' => 'CANCELED',
		'RJ' => 'REJECTED',
		'BK' => 'BLOCKED',
		'EX' => 'EXPIRED'
	);
	
	public static $BOOKING_CLASSES = array(
		'AP' => 'status-pending',
		'BA' => 'status-accepted',
		'PP' => 'status-payment',
		'CO' => 'status-confirmed',
		'CM' => 'status-completed',
		'CN' => 'status-canceled',
		'RJ' => 'status-reject',
		'BK' => 'status-blocked',
		'EX' => 'status-expired'
	);
	
	
	/********** Phone Types ******************/
	public static $PHONE_TYPE_MOBILE = 'M';
	
	public static $PHONE_TYPE_FIXLINE = 'P';
	
	/********** Invoice TYPES ***************/
	
	/** Invoce type : new subscription : nsb **/
	public static $INVOICE_TYPE_NEW_SUBSCRIPTION = 'nsb';
	
	/** Invoce type : renew subscription : rub **/
	public static $INVOICE_TYPE_RENEW_SUBSCRIPTION = 'rsb';
	
	/** Invoce type : downgrade subscription : dsb **/
	public static $INVOICE_TYPE_DOWN_SUBSCRIPTION = 'dsb';
	
	/** Invoce type : upgrade subscription : usb **/
	public static $INVOICE_TYPE_UP_SUBSCRIPTION = 'usb';
		
	/** Invoce type : profile : pro **/
	public static $INVOICE_TYPE_PROFILE = 'pro';
	
	/** Invoce type : credit : crd **/
	public static $INVOICE_TYPE_CREDIT = 'crd';
	
	/** Invoce type : registrtion : reg (not now) **/
	public static $INVOICE_TYPE_REG = 'reg';
	
	/** Invoce type : advertisement : ads **/
	public static $INVOICE_TYPE_ADVERT = 'ads';
	
	/********** Invoice status ***************/
	
	/** Invoice pending : p**/
	public static $INVOICE_PENDING = 'p';
	
	/** Invoce paid : s**/
	public static $INVOICE_PAID = 's';
	
	/** Invoce expired : e**/
	public static $INVOICE_EXPIRED = 'e';
	
	/************* Payment types **************/
	
	/** Card payment : c **/
	public static $PAYMENT_TYPE_CARD = 'c';
		
	/** Bank transfers : b **/
	public static $PAYMENT_TYPE_BANK = 'b';
	
	/************* Payment status *************/
	
	/** Payment pending : p **/
	public static $PAYMENT_PENDING = 'p';
	
	/** Payment success : s **/
	public static $PAYMENT_SUCCESS = 's';
	
	/** Payment in process (bank) : i **/
	public static $PAYMENT_PROCESSING = 'i';
	
	/** Payment in failed : f **/
	public static $PAYMENT_FAILED = 'f';
	
	/** Payment Crd Type ***************************************/
	
	/** Visa card : visa*/
	public static $PAYMENT_CARD_VISA = 'visa';
	
	/** Visa card : master*/
	public static $PAYMENT_CARD_MASTER = 'master';
	
	/** Amex card : amex*/
	public static $PAYMENT_CARD_AMEX = 'amex';
	
	/** Discover card : discover*/
	public static $PAYMENT_CARD_DISCOVER = 'discover';
	
	/** JCB card : jcb*/
	public static $PAYMENT_CARD_JCB = 'jcb';
	
	/** Dinnersclub card : dc*/
	public static $PAYMENT_CARD_DINNERSCLUB = 'diners';
	
	
	/********** Cookie key settings ***********/
	
	/** Security key **/
	public static $COOKIE_SECURITY = "MDSK";
	
	/** Country cookie **/
	public static $COOKIE_COUNTRY = "CTCNT";
	
	/** Money/Currency cookie **/
	public static $COOKIE_CURRENCY = "CTCUR";
	
	/** Time zone cookie */
	public static $COOKIE_TIMEZONE = 'CTTZC';
		
	/** current user */
	// UserID|~|name|~|DP
	public static $COOKIE_USER = "CTCUI";
	
	/** currrent profile **/
	// ProfileID|~|type|~|name|~|DP
	public static $COOKIE_PROFILE = "CTCPI";
		
	/** Last Booking cookie */
	public static $COOKIE_BOOKING = "CTLBT";
	
	/** Last search cookie */
	public static $COOKIE_SEARCH = "CTSRH";
	
	/** last send OTP code **/
	public static $COOKIE_CODE = "CTXCI";
	
	/** Last checked event cookie **/
	public static $COOKIE_EVENT = 'CTLCE';
	
	/** Language **/
	public static $COOKIE_LANGUAGE = 'CTLNG';
	
	/** Redirect url (after login  or signup) */
	public static $COOKIE_REDIRECT = 'CTRDU';
	
	
	/************ List of booking events ********/
	
	/** Creating a booking **/
	public static $EVENT_BOOKING_CREATE = 'CBC';
	
	/** Talent change package **/
	public static $EVENT_TALENT_PKG_CHANGE = 'TPC';
	
	/** Talent change date/time 
	public static $EVENT_TALENT_TIME_CHANGE = 'TTC';
	**/
	/** Talent add charges **/
	public static $EVENT_TALENT_ADD_CHARGES = 'TAC';
	
	/** Talent add charges **/
	public static $EVENT_TALENT_ADD_DISCOUNT = 'TAD';
		
	/** Talent accept booking **/
	public static $EVENT_TALENT_ACCEPT_BOOKING = 'TAB';
	
	/** Quotation sent (system)**/
	public static $EVENT_QUATATION_SENT = 'QTO';
	
	/** Customer accept new package **/
	public static $EVENT_CUSTOMER_PACKAGE_ACCEPT = 'CPA';
	
	/** customer accept new time 
	public static $EVENT_CUSTOMER_TIME_ACCEPT = 'CTA';
	**/
	/** customer accept additional charges **/
	public static $EVENT_CUSTOMER_CHARGES_ACCEPT = 'CAC';
	
	/** customer confirm booking **/
	public static $EVENT_CUSTOMER_CONFIRM_BOOKING = 'CCB';	
	
	/** Invoice generated/sent (system)**/
	public static $EVENT_INVOICE_SENT = 'INV';
	
	/** customer change place **/
	public static $EVENT_CHANGE_PLACE = 'CCP';
	
	/** Advance payment received **/
	public static $EVENT_ADVANCE_PAYMENT_RECEIVED = 'APR';
	
	/** Talent rejected booking **/
	public static $EVENT_TALENT_REJECTED_BOOKING = 'TRB';
	
	/** Customer cancel booking **/
	public static $EVENT_CUSTOMER_CANCEL_BOOKING = 'CXB';
	
	/** Talent accept booking cancellation **/
	public static $EVENT_TALENT_ACCEPT_CANCEL = 'TCA';
	
	/** Talent demand charges **/
	public static $EVENT_TALENT_DEMAND_CHARGES = 'TDC';
	
	/** Creating refund (system)**/
	public static $EVENT_REFUND_CREATE = 'RFC';
	
	/** Refund processed (system)**/
	public static $EVENT_REFUND_PROCESSED = 'RFP';
	
	/** customer released funds to talent**/
	public static $EVENT_CUSTOMER_RELEASED = 'CRF';
	
	/** system released funds to talent**/
	public static $EVENT_SYSTEM_RELEASED = 'SRF';
	
	/** booking confirmed (system) **/
	public static $EVENT_BOOKING_CONFIRMED = 'CON';
	
	/** Talent appoinment created */
	public static $EVENT_TALENT_APPOINMENT_CREATE = 'TAP';
	
	/** Talent appoinment removed on cancellation */
	public static $EVENT_TALENT_APPOINMENT_REMOVE = 'TAR';
	
	/** Customer created a dispute **/
	public static $EVENT_CUSTOMER_DISPUTE = 'CCD';
	
	/** Talent created a dispute */
	public static $EVENT_TALENT_DISPUTE = 'TCD';
	
	/** Acknowladge customer */
	public static $EVENT_CUSTOMER_ACK = 'CAK';
	
	/** Acknowladge talent */
	public static $EVENT_TALENT_ACK = 'TAK';
	
	
	
	/***************** List of Agency Request status ***************/
	
	/** Agency Request status PENINDG : p **/
	public static $AGENCY_REQEST_PENDING = 'p';
	
	/** Agency Request status ACCEPTED : a **/
	public static $AGENCY_REQEST_ACCEPTED = 'a';
	
	/** Agency Request status REJECTED : r **/
	public static $AGENCY_REQEST_REJECTED = 'r';
	
	/** Agency Request status :  Removed by Agency : e **/
	public static $AGENCY_REQEST_AGENCY_REMOVED = 'e';
	
	/** Agency Request status :  Removed by Talent  : f **/
	public static $AGENCY_REQEST_TALENT_REMOVED = 'f';
	
	
	/************************ Notificatio types (reference only )******************/
	
	public static $NOTIFICATIONS = array(
			'b' => 'Booking',
			'm' => 'Message',
			'r' => 'Agency request',
			'e' => 'Event',
	);
	
	
	/** Skill Categories **/
	public static $SKILL_CATEGORIES = array(
		0 => 'Agency',
		1 => 'Artist/Performer',
		2 => 'Arrangement/Decoration',
		3 => 'Beautician',
		4 => 'Photographer',
		5 => 'Event/Activity Planners',
		6 => 'Services',
		7 => 'Suppliers',
		8 => 'Event Venues'
	);
	
	/**
	* List of social site classes 
	* 'ww','fb', 'tw','yt','ig','ln','pn'
	*/
	public static $SOCIAL_CLASSES = array(
				'ww' => 'Website',
				'fb' => 'Facebook', 
				'tw' => 'Twitter',
				'yt' => 'Youtube',
				'ig' => 'Instagram',
				'tk' => 'TikTok',
				'ln' => 'LinkedIn',
				'pn' => 'Pinterest'
				);
	
	/*** Notification types */
	public static $NOTIFICATION_TYPES = array(
		1 => 'New booking requests',
		2 => 'Booking status changes',
		3 => 'Booking due date reminder',
		4 => 'Booking date/time reminder',
		5 => 'Appointment reminder',
		6 => 'Event changes',
		7 => 'New messages',
		8 => 'Agency requests',
		9 => 'Daily Briefing',
		10 => 'News Letters/Promotions'
	);
	
	
	/**
	* Get array of booking events
	*/
	public static $booking_events = array(
		'CBC' => 'New booking created',
		'TPC' => 'Talent change the package',
		'TAC' => 'Tallent added  extra charges',
		'TAD' => 'Talent added discount',
		'TAB' => 'Talent accepted booking',
		'QTO' => 'Quotation sent to customer',
		'CPA' => 'Customer accepted package change',
		'CAC' => 'Customer accepted additional charges',
		'CCB' => 'Customer confirmed booking',
		'INV' => 'Invoice created and sent',
		'CCP' => 'Place changed',
		'APR' => 'Advance payment received',
		'TRB' => 'Talent rejected booking',
		'CBX' => 'Customer cancel booking',
		'TCA' => 'Talent accepted cancellation',
		'TDC' => 'Talent demanded charges for cancellation',
		'RFC' => 'Refund created',
		'RFP' => 'Refund processed',
		'CRF' => 'Customer release funds to talent',
		'SRF' => 'System released funds to talent',
		'CON' => 'Booking confirmed' ,
		'TAP' =>  'Talent appoinment created',
		'TAR' => 'Talent appoinment removed',
		'CCD' => 'Customer created a dispute',
		'TCD' => 'Talent created a dispute',
		'CAK' => 'Acknowladgement sent to customer',
		'TAK' => 'Acknowladgement sent to talent'
	);
	
	
	/** Type of booking expences **/
	public static $EXPENCES_TYPE = array(
			1 => 'Travel',
			2 => 'Transport',
			3 => 'Accommodation',
			4 => 'Meals/Beverages',
			5 => 'Equipment rental',
			6 => 'Labor charges',
			7 => 'Costumes',
			8 => 'Insurance',
			9 => 'Advertisements' ,
			10 => 'Legal Fees' ,
			11 => 'Administration' ,
			12 => 'Supplies',
			13 => 'Taxes' ,
			14 => 'Deposits',
			15 => 'Other'
		);
		
		
	/**** Agency Types ****/
	public static $AGENCY_TYEPS = array(
		1 => 'Activity Management',
		2 => 'Advertising',
		3 => 'Event Management',
		4 => 'Fashion Modeling',
		5 => 'Marketing',
		6 => 'Promotional',
		7 => 'Public Relations',
		8 => 'Social Media'
	);
	
	
	/**
	* Booking change resonsons
	*/
	public static $BOOKING_CHANGE_REASONS = array(
		1 => 'Event rescheduled',
		2 => 'Location changed',
		3 => 'Over budget',
		4 => 'Event canceled',
		5 => 'Too busy schedule',
		6 => 'Other/Personal reason'
	);
		
	
	/**  Types of events ***/
	
	public static $EVENT_CATEGORIES = array(
		1 => array("Anniversary", 0 ),
		2 => array("Bachelor Party", 0 ),
		3 => array("Bachelorette Party", 0 ),
		4 => array("Birthday", 0 ),
		5 => array("Business Party", 1 ),
		6 => array("Conference", 1 ),
		7 => array("Cultural Festival", 0 ),
		8 => array("Family Event", 0 ),
		9 => array("Fashion Show", 1 ),
		10 => array("Get Together", 0 ),
		11 => array("Meeting", 1 ),
		12 => array("Office Party", 1 ),
		13 => array("Private Party", 0 ),
		14 => array("Sport Festival", 1 ),
		15 => array("Theme Party", 0 ),
		16 => array("Wedding", 0 )
	);
	
	
	/** International country codes and dialing codes */
	public static $countries = array(
		'AF' => array('Afghanistan', '93', 'AFN'), 
		'AL' => array('Albania', '355', 'ALL'), 
		'DZ' => array('Algeria', '213', 'DZD'), 
		'AS' => array('American Samoa', '1684', 'USD'), 
		'AD' => array('Andorra', '376', 'EUR'), 
		'AO' => array('Angola', '244', 'AOA'), 
		'AI' => array('Anguilla', '1264', 'XCD'), 
		'AQ' => array('Antarctica', '672', 'USD'), 
		'AG' => array('Antigua and Barbuda', '1268', 'XCD'), 
		'AR' => array('Argentina', '54', 'ARS'), 
		'AM' => array('Armenia', '374', 'AMD'), 
		'AW' => array('Aruba', '297', 'AWG'), 
		'AU' => array('Australia', '61', 'AUD'), 
		'AT' => array('Austria', '43', 'EUR'), 
		'AZ' => array('Azerbaijan', '994', 'AZN'), 
		'BS' => array('Bahamas', '1242', 'BSD'), 
		'BH' => array('Bahrain', '973', 'BHD'), 
		'BD' => array('Bangladesh', '880', 'BDT'), 
		'BB' => array('Barbados', '1246', 'BBD'), 
		'BY' => array('Belarus', '375', 'BYN'), 
		'BE' => array('Belgium', '32', 'EUR'), 
		'BZ' => array('Belize', '501', 'BZD'), 
		'BJ' => array('Benin', '229', 'XOF'), 
		'BM' => array('Bermuda', '1441', 'BMD'), 
		'BT' => array('Bhutan', '975', 'BTN'), 
		'BO' => array('Bolivia', '591', 'BOB'), 
		'BA' => array('Bosnia and Herzegovina', '387', 'BAM'), 
		'BW' => array('Botswana', '267', 'BWP'), 
		'BR' => array('Brazil', '55', 'BRL'), 
		'IO' => array('British Indian Ocean Territory', '246', 'USD'), 
		'VG' => array('British Virgin Islands', '1284', 'USD'), 
		'BN' => array('Brunei', '673', 'BND'), 
		'BG' => array('Bulgaria', '359', 'BGN'), 
		'BF' => array('Burkina Faso', '226', 'XOF'), 
		'BI' => array('Burundi', '257', 'BIF'), 
		'KH' => array('Cambodia', '855', 'KHR'), 
		'CM' => array('Cameroon', '237', 'XAF'), 
		'CA' => array('Canada', '1', 'CAD'), 
		'CV' => array('Cape Verde', '238', 'CVE'), 
		'KY' => array('Cayman Islands', '1345', 'KYD'), 
		'CF' => array('Central African Republic', '236', 'XAF'), 
		'TD' => array('Chad', '235', 'XAF'), 
		'CL' => array('Chile', '56', 'CLP'), 
		'CN' => array('China', '86', 'CNY'), 
		'CX' => array('Christmas Island', '61', 'AUD'), 
		'CC' => array('Cocos Islands', '61', 'AUD'), 
		'CO' => array('Colombia', '57', 'COP'), 
		'KM' => array('Comoros', '269', 'KMF'), 
		'CK' => array('Cook Islands', '682', 'none'), 
		'CR' => array('Costa Rica', '506', 'CRC'), 
		'HR' => array('Croatia', '385', 'HRK'), 
		'CU' => array('Cuba', '53', 'CUP'), 
		'CW' => array('Curacao', '599', 'ANG'), 
		'CY' => array('Cyprus', '357', 'EUR'), 
		'CZ' => array('Czech Republic', '420', 'CZK'), 
		'CD' => array('Democratic Republic of the Congo', '243', 'CDF'), 
		'DK' => array('Denmark', '45', 'DKK'), 
		'DJ' => array('Djibouti', '253', 'DJF'), 
		'DM' => array('Dominica', '1767', 'XCD'), 
		'DO' => array('Dominican Republic', '1809', 'DOP'), 
		'TL' => array('East Timor', '670', 'USD'), 
		'EC' => array('Ecuador', '593', 'USD'), 
		'EG' => array('Egypt', '20', 'EGP'), 
		'SV' => array('El Salvador', '503', 'USD'), 
		'GQ' => array('Equatorial Guinea', '240', 'XAF'), 
		'ER' => array('Eritrea', '291', 'ERN'), 
		'EE' => array('Estonia', '372', 'EUR'), 
		'ET' => array('Ethiopia', '251', 'ETB'), 
		'FK' => array('Falkland Islands', '500', 'FKP'), 
		'FO' => array('Faroe Islands', '298', 'none'), 
		'FJ' => array('Fiji', '679', 'FJD'), 
		'FI' => array('Finland', '358', 'EUR'), 
		'FR' => array('France', '33', 'EUR'), 
		'PF' => array('French Polynesia', '689', 'XPF'), 
		'GA' => array('Gabon', '241', 'XAF'), 
		'GM' => array('Gambia', '220', 'GMD'), 
		'GE' => array('Georgia', '995', 'GEL'), 
		'DE' => array('Germany', '49', 'EUR'), 
		'GH' => array('Ghana', '233', 'GHS'), 
		'GI' => array('Gibraltar', '350', 'GIP'), 
		'GR' => array('Greece', '30', 'EUR'), 
		'GL' => array('Greenland', '299', 'DKK'), 
		'GD' => array('Grenada', '1473', 'XCD'), 
		'GU' => array('Guam', '1671', 'USD'), 
		'GT' => array('Guatemala', '502', 'GTQ'), 
		'GG' => array('Guernsey', '441481', 'GGP'), 
		'GN' => array('Guinea', '224', 'GNF'), 
		'GW' => array('Guinea-Bissau', '245', 'XOF'), 
		'GY' => array('Guyana', '592', 'GYD'), 
		'HT' => array('Haiti', '509', 'HTG'), 
		'HN' => array('Honduras', '504', 'HNL'), 
		'HK' => array('Hong Kong', '852', 'HKD'), 
		'HU' => array('Hungary', '36', 'HUF'), 
		'IS' => array('Iceland', '354', 'ISK'), 
		'IN' => array('India', '91', 'INR'), 
		'ID' => array('Indonesia', '62', 'IDR'), 
		'IR' => array('Iran', '98', 'IRR'), 
		'IQ' => array('Iraq', '964', 'IQD'), 
		'IE' => array('Ireland', '353', 'EUR'), 
		'IM' => array('Isle of Man', '441624', 'IMP'), 
		'IL' => array('Israel', '972', 'ILS'), 
		'IT' => array('Italy', '39', 'EUR'), 
		'CI' => array('Ivory Coast', '225', 'XOF'), 
		'JM' => array('Jamaica', '1876', 'JMD'), 
		'JP' => array('Japan', '81', 'JPY'), 
		'JE' => array('Jersey', '441534', 'JEP'), 
		'JO' => array('Jordan', '962', 'JOD'), 
		'KZ' => array('Kazakhstan', '7', 'KZT'), 
		'KE' => array('Kenya', '254', 'KES'), 
		'KI' => array('Kiribati', '686', 'AUD'), 
		'XK' => array('Kosovo', '383', 'EUR'), 
		'KW' => array('Kuwait', '965', 'KWD'), 
		'KG' => array('Kyrgyzstan', '996', 'KGS'), 
		'LA' => array('Laos', '856', 'LAK'), 
		'LV' => array('Latvia', '371', 'EUR'), 
		'LB' => array('Lebanon', '961', 'LBP'), 
		'LS' => array('Lesotho', '266', 'LSL'), 
		'LR' => array('Liberia', '231', 'LRD'), 
		'LY' => array('Libya', '218', 'LYD'), 
		'LI' => array('Liechtenstein', '423', 'CHF'), 
		'LT' => array('Lithuania', '370', 'EUR'), 
		'LU' => array('Luxembourg', '352', 'EUR'), 
		'MO' => array('Macau', '853', 'MOP'), 
		'MK' => array('Macedonia', '389', 'MKD'), 
		'MG' => array('Madagascar', '261', 'MGA'), 
		'MW' => array('Malawi', '265', 'MWK'), 
		'MY' => array('Malaysia', '60', 'MYR'), 
		'MV' => array('Maldives', '960', 'MVR'), 
		'ML' => array('Mali', '223', 'XOF'), 
		'MT' => array('Malta', '356', 'EUR'), 
		'MH' => array('Marshall Islands', '692', 'USD'), 
		'MR' => array('Mauritania', '222', 'MRU'), 
		'MU' => array('Mauritius', '230', 'MUR'), 
		'YT' => array('Mayotte', '262', 'EUR'), 
		'MX' => array('Mexico', '52', 'MXN'), 
		'FM' => array('Micronesia', '691', 'USD'), 
		'MD' => array('Moldova', '373', 'MDL'), 
		'MC' => array('Monaco', '377', 'EUR'), 
		'MN' => array('Mongolia', '976', 'MNT'), 
		'ME' => array('Montenegro', '382', 'EUR'), 
		'MS' => array('Montserrat', '1664', 'XCD'), 
		'MA' => array('Morocco', '212', 'MAD'), 
		'MZ' => array('Mozambique', '258', 'MZN'), 
		'MM' => array('Myanmar', '95', 'MMK'), 
		'NA' => array('Namibia', '264', 'NAD'), 
		'NR' => array('Nauru', '674', 'AUD'), 
		'NP' => array('Nepal', '977', 'NPR'), 
		'NL' => array('Netherlands', '31', 'EUR'), 
		'AN' => array('Netherlands Antilles', '599', 'ANG'), 
		'NC' => array('New Caledonia', '687', 'XPF'), 
		'NZ' => array('New Zealand', '64', 'NZD'), 
		'NI' => array('Nicaragua', '505', 'NIO'), 
		'NE' => array('Niger', '227', 'XOF'), 
		'NG' => array('Nigeria', '234', 'NGN'), 
		'NU' => array('Niue', '683', 'NZD'), 
		'KP' => array('North Korea', '850', 'KPW'), 
		'MP' => array('Northern Mariana Islands', '1670', 'USD'), 
		'NO' => array('Norway', '47', 'NOK'), 
		'OM' => array('Oman', '968', 'OMR'), 
		'PK' => array('Pakistan', '92', 'PKR'), 
		'PW' => array('Palau', '680', 'USD'), 
		'PS' => array('Palestine', '970', 'ILS'), 
		'PA' => array('Panama', '507', 'USD'), 
		'PG' => array('Papua New Guinea', '675', 'PGK'), 
		'PY' => array('Paraguay', '595', 'PYG'), 
		'PE' => array('Peru', '51', 'PEN'), 
		'PH' => array('Philippines', '63', 'PHP'), 
		'PN' => array('Pitcairn Islands', '64', 'NZD'), 
		'PL' => array('Poland', '48', 'PLN'), 
		'PT' => array('Portugal', '351', 'EUR'), 
		'PR' => array('Puerto Rico', '1787', 'USD'), 
		'QA' => array('Qatar', '974', 'QAR'), 
		'CG' => array('Republic of the Congo', '242', 'XAF'), 
		'RE' => array('Reunion', '262', 'EUR'), 
		'RO' => array('Romania', '40', 'RON'), 
		'RU' => array('Russia', '7', 'RUB'), 
		'RW' => array('Rwanda', '250', 'RWF'), 
		'BL' => array('Saint Barthelemy', '590', 'EUR'), 
		'SH' => array('Saint Helena', '290', 'SHP'), 
		'KN' => array('Saint Kitts and Nevis', '1869', 'XCD'), 
		'LC' => array('Saint Lucia', '1758', 'XCD'), 
		'MF' => array('Saint Martin', '590', 'EUR'), 
		'PM' => array('Saint Pierre and Miquelon', '508', 'EUR'), 
		'VC' => array('Saint Vincent and the Grenadines', '1784', 'XCD'), 
		'WS' => array('Samoa', '685', 'WST'), 
		'SM' => array('San Marino', '378', 'EUR'), 
		'ST' => array('Sao Tome and Principe', '239', 'STN'), 
		'SA' => array('Saudi Arabia', '966', 'SAR'), 
		'SN' => array('Senegal', '221', 'XOF'), 
		'RS' => array('Serbia', '381', 'RSD'), 
		'SC' => array('Seychelles', '248', 'SCR'), 
		'SL' => array('Sierra Leone', '232', 'SLL'), 
		'SG' => array('Singapore', '65', 'SGD'), 
		'SX' => array('Sint Maarten', '1721', 'ANG'), 
		'SK' => array('Slovakia', '421', 'EUR'), 
		'SI' => array('Slovenia', '386', 'EUR'), 
		'SB' => array('Solomon Islands', '677', 'SBD'), 
		'SO' => array('Somalia', '252', 'SOS'), 
		'ZA' => array('South Africa', '27', 'ZAR'), 
		'KR' => array('South Korea', '82', 'KRW'), 
		'SS' => array('South Sudan', '211', 'SSP'), 
		'ES' => array('Spain', '34', 'EUR'), 
		'LK' => array('Sri Lanka', '94', 'LKR'), 
		'SD' => array('Sudan', '249', 'SDG'), 
		'SR' => array('Suriname', '597', 'SRD'), 
		'SJ' => array('Svalbard and Jan Mayen', '47', 'NOK'), 
		'SZ' => array('Eswatini(Swaziland)', '268', 'SZL'), 
		'SE' => array('Sweden', '46', 'SEK'), 
		'CH' => array('Switzerland', '41', 'CHF'), 
		'SY' => array('Syria', '963', 'SYP'), 
		'TW' => array('Taiwan', '886', 'TWD'), 
		'TJ' => array('Tajikistan', '992', 'TJS'), 
		'TZ' => array('Tanzania', '255', 'TZS'), 
		'TH' => array('Thailand', '66', 'THB'), 
		'TG' => array('Togo', '228', 'XOF'), 
		'TK' => array('Tokelau', '690', 'NZD'), 
		'TO' => array('Tonga', '676', 'TOP'), 
		'TT' => array('Trinidad and Tobago', '1868', 'TTD'), 
		'TN' => array('Tunisia', '216', 'TND'), 
		'TR' => array('Turkey', '90', 'TRY'), 
		'TM' => array('Turkmenistan', '993', 'TMT'), 
		'TC' => array('Turks and Caicos Islands', '1649', 'USD'), 
		'TV' => array('Tuvalu', '688', 'AUD'), 
		'VI' => array('U.S. Virgin Islands', '1340', 'USD'), 
		'UG' => array('Uganda', '256', 'UGX'), 
		'UA' => array('Ukraine', '380', 'UAH'), 
		'AE' => array('United Arab Emirates', '971', 'AED'), 
		'GB' => array('United Kingdom', '44', 'GBP'), 
		'US' => array('United States', '1', 'USD'), 
		'UY' => array('Uruguay', '598', 'UYU'), 
		'UZ' => array('Uzbekistan', '998', 'UZS'), 
		'VU' => array('Vanuatu', '678', 'VUV'), 
		'VA' => array('Vatican City', '379', 'EUR'), 
		'VE' => array('Venezuela', '58', 'VES'), 
		'VN' => array('Vietnam', '84', 'VND'), 
		'WF' => array('Wallis and Futuna', '681', 'XPF'), 
		'EH' => array('Western Sahara', '212', 'MAD'), 
		'YE' => array('Yemen', '967', 'YER'), 
		'ZM' => array('Zambia', '260', 'ZMW'), 
		'ZW' => array('Zimbabwe', '263', 'USD'), 
	);
	
	
	/** Currency codes and names **/
	public static $currencies = array (
	  'AUD' => 'Australian dollar',	  
	  'CAD' => 'Canadian dollar',
	  'CNY' => 'Chinese yuan',
	  'EUR' => 'Euro',
	  'INR' => 'Indian rupee',
	  'JPY' => 'Japanese yen',
	 'LKR' => 'Sri Lanka Rupees',
	  'MYR' => 'Malaysian ringgit',	  
	  'RUB' => 'Russian ruble',
	  'GBP' => 'Sterling pound',
	  'CHF' => 'Swiss franc',
	  'THB' => 'Thai baht',
	  'USD' => 'United States dollar'	  
	);
	
	/** Currency symboles **/
	public static $csymbols = array(
		'AUD' => 'A&#36;',	  
		'CAD' => 'CA&#36;',
		'CNY' => '&#20803;',
		'EUR' => '&euro;',
		'INR' => '&#x20B9;',
		'JPY' => '&#165;',
		'LKR' => 'Rs',
		'MYR' => 'RM',
		'RUB' => '&#8381;',
		'GBP' => '&#163;',
		'CHF' => '&#8355;',
		'THB' => '&#3647;',
		'USD' => '&#36;'
	);
	
	/** Time zone data ***/
	public static $timezones = array(
	
//		'ACT' => array('ASEAN Common Time','+06:30 - UTC+09',390),
		'AFT' => array('Afghanistan Time','+04:30',270),
		'AKDT' => array('Alaska Daylight Time','-08',-480),
		'AKST' => array('Alaska Standard Time','-09',-540),
		'AMST' => array('Amazon Summer Time (Brazil)[1]','-03',-180),
		'ART' => array('Argentina Time','-03',-180),
		'AMT' => array('Armenia Time','+04',240),
		'ADT' => array('Atlantic Daylight Time','-03',-180),
		'AST' => array('Atlantic Standard Time','-04',-240),
		'AWST' => array('Australian Western Standard Time','+08',480),
		'AZT' => array('Azerbaijan Time','+04',240),
		'AZOT' => array('Azores Standard Time','-01',-60),
		'AZOST' => array('Azores Summer Time','±00',0),
		'BIT' => array('Baker Island Time','-12',-720),
		'BTT' => array('Bhutan Time','+06',360),
		'BOT' => array('Bolivia Time','-04',-240),
		'BRST' => array('Brasilia Summer Time','-02',-120),
		'BRT' => array('Brasilia Time','-03',-180),
		'BIOT' => array('British Indian Ocean Time','+06',360),
		'BST' => array('British Summer Time (British Standard Time)','+01',60),
		'BDT' => array('Brunei Time','+08',480),
		'CVT' => array('Cape Verde Time','-01',-60),
		'CAT' => array('Central Africa Time','+02',120),
		'CEST' => array('Central European Summer Time (Cf. HAEC)','+02',120),
		'CET' => array('Central European Time','+01',60),
		'CIT' => array('Central Indonesia Time','+08',480),
		'ACST' => array('Central Standard Time (Australia)','+09:30',570),
		'ACDT' => array('Central Summer Time (Australia)','+10:30',630),
		'CWST' => array('Central Western Standard Time (Australia)','+08:45',525),
		'CHST' => array('Chamorro Standard Time','+10',600),
		'CHADT' => array('Chatham Daylight Time','+13:45',825),
		'CHAST' => array('Chatham Standard Time','+12:45',765),
		'CLT' => array('Chile Standard Time','-04',-240),
		'CLST' => array('Chile Summer Time','-03',-180),
		'CT' => array('China time','+08',480),
		'CHOT' => array('Choibalsan Standard Time','+08',480),
		'CHOST' => array('Choibalsan Summer Time','+09',540),
		'CXT' => array('Christmas Island Time','+07',420),
		'CHUT' => array('Chuuk Time','+10',600),
		'CIST' => array('Clipperton Island Standard Time','-08',-480),
		'CCT' => array('Cocos Islands Time','+06:30',390),
		'COST' => array('Colombia Summer Time','-04',-240),
		'COT' => array('Colombia Time','-05',-300),
		'CKT' => array('Cook Island Time','-10',-600),
		'UTC' => array('Coordinated Universal Time','±00',0),
		'CDT' => array('Cuba Daylight Time[4]','-04',-240),
		'CST' => array('Cuba Standard Time','-05',-300),
		'DAVT' => array('Davis Time','+07',420),
		'DDUT' => array('Dumont d\'Urville Time','+10',600),
		'EAT' => array('East Africa Time','+03',180),
		'EAST' => array('Easter Island Standard Time','-06',-360),
		'EASST' => array('Easter Island Summer Time','-05',-300),
		'EDT' => array('Eastern Daylight Time (North America)','-04',-240),
		'EEST' => array('Eastern European Summer Time','+03',180),
		'EET' => array('Eastern European Time','+02',120),
		'EGST' => array('Eastern Greenland Summer Time','±00',0),
		'EGT' => array('Eastern Greenland Time','-01',-60),
		'EIT' => array('Eastern Indonesian Time','+09',540),
		'AEST' => array('Eastern Standard Time (Australia)','+10',600),
		'EST' => array('Eastern Standard Time (North America)','-05',-300),
		'AEDT' => array('Eastern Summer Time (Australia)','+11',660),
		'ECT' => array('Ecuador Time','-05',-300),
		'FKST' => array('Falkland Islands Summer Time','-03',-180),
		'FKT' => array('Falkland Islands Time','-04',-240),
		'FNT' => array('Fernando de Noronha Time','-02',-120),
		'FJT' => array('Fiji Time','+12',720),
		'GFT' => array('French Guiana Time','-03',-180),
		'FET' => array('Further-eastern European Time','+03',180),
		'GALT' => array('Galapagos Time','-06',-360),
		'GIT' => array('Gambier Island Time','-09',-540),
		'GAMT' => array('Gambier Islands','-09',-540),
		'GET' => array('Georgia Standard Time','+04',240),
		'GILT' => array('Gilbert Island Time','+12',720),
		'GMT' => array('Greenwich Mean Time','±00',0),
		'GST' => array('Gulf Standard Time','+04',240),
		'GYT' => array('Guyana Time','-04',-240),
		'HADT' => array('Hawaii-Aleutian Daylight Time','-09',-540),
		'HAST' => array('Hawaii-Aleutian Standard Time','-10',-600),
		'HMT' => array('Heard and McDonald Islands Time','+05',300),
		'HAEC' => array('Heure Avancée d\'Europe Centrale francised name for CEST','+02',120),
		'HKT' => array('Hong Kong Time','+08',480),
		//'IOT' => array('Indian Ocean Time','+03',180),
		'INST' => array('Indian Standard Time','+05:30',330),
		'TFT' => array('Indian/Kerguelen','+05',300),
		'ICT' => array('Indochina Time','+07',420),
		'IRDT' => array('Iran Daylight Time','+04:30',270),
		'IRST' => array('Iran Standard Time','+03:30',210),
		'IRKT' => array('Irkutsk Time','+08',480),
		'IDT' => array('Israel Daylight Time','+03',180),
		'IST' => array('Israel Standard Time','+02',120),
		'JST' => array('Japan Standard Time','+09',540),
		'USZ1' => array('Kaliningrad Time','+02',120),
		'PETT' => array('Kamchatka Time','+12',720),
		'HOVT' => array('Khovd Standard Time','+07',420),
		'HOVST' => array('Khovd Summer Time','+08',480),
		'KST' => array('Korea Standard Time','+09',540),
		'KOST' => array('Kosrae Time','+11',660),
		'KRAT' => array('Krasnoyarsk Time','+07',420),
		'KGT' => array('Kyrgyzstan time','+06',360),
		'LINT' => array('Line Islands Time','+14',840),
		'LHST' => array('Lord Howe Summer Time','+11',660),
		'MIST' => array('Macquarie Island Station Time','+11',660),
		'MAGT' => array('Magadan Time','+12',720),
		'MST' => array('Malaysia Standard Time','+08',480),
		'MYT' => array('Malaysia Time','+08',480),
		'MVT' => array('Maldives Time','+05',300),
		'MART' => array('Marquesas Islands Time','-09:30',-570),
		'MIT' => array('Marquesas Islands Time','-09:30',-570),
		'MHT' => array('Marshall Islands','+12',720),
		'MUT' => array('Mauritius Time','+04',240),
		'MAWT' => array('Mawson Station Time','+05',300),
		'MEST' => array('Middle European Summer Time Same zone as CEST','+02',120),
		'MET' => array('Middle European Time Same zone as CET','+01',60),
		'MSK' => array('Moscow Time','+03',180),
		'MDT' => array('Mountain Daylight Time (North America)','-06',-360),
		'MMT' => array('Myanmar Standard Time','+06:30',390),
		'NPT' => array('Nepal Time','+05:45',345),
		'NCT' => array('New Caledonia Time','+11',660),
		'NZDT' => array('New Zealand Daylight Time','+13',780),
		'NZST' => array('New Zealand Standard Time','+12',720),
		'NDT' => array('Newfoundland Daylight Time','-02:30',-150),
		'NST' => array('Newfoundland Standard Time','-03:30',-210),
		'NT' => array('Newfoundland Time','-03:30',-210),
		'NUT' => array('Niue Time','-11',-660),
		'NFT' => array('Norfolk Time','+11',660),
		'OMST' => array('Omsk Time','+06',360),
		'ORAT' => array('Oral Time','+05',300),
		'PDT' => array('Pacific Daylight Time (North America)','-07',-420),
		'PST' => array('Pacific Standard Time (North America)','-08',-480),
		'PKT' => array('Pakistan Standard Time','+05',300),
		'PGT' => array('Papua New Guinea Time','+10',600),
		'PYST' => array('Paraguay Summer Time (South America)[7]','-03',-180),
		'PYT' => array('Paraguay Time (South America)[8]','-04',-240),
		'PET' => array('Peru Time','-05',-300),
		'PHST' => array('Philippine Standard Time','+08',480),
		'PHT' => array('Philippine Time','+08',480),
		'PHOT' => array('Phoenix Island Time','+13',780),
		'PONT' => array('Pohnpei Standard Time','+11',660),
		'ROTT' => array('Rothera Research Station Time','-03',-180),
		'RET' => array('Réunion Time','+04',240),
		'PMDT' => array('Saint Pierre and Miquelon Daylight time','-02',-120),
		'PMST' => array('Saint Pierre and Miquelon Standard Time','-03',-180),
		'SAKT' => array('Sakhalin Island time','+11',660),
		'SAMT' => array('Samara Time','+04',240),
		'SDT' => array('Samoa Daylight Time','-10',-600),
		'SST' => array('Samoa Standard Time','-11',-660),
		'SCT' => array('Seychelles Time','+04',240),
		'SYOT' => array('Showa Station Time','+03',180),
		'SIST' => array('Singapore Standard Time','+08',480),
		'SGT' => array('Singapore Time','+08',480),
		'SBT' => array('Solomon Islands Time','+11',660),
		'SAST' => array('South African Standard Time','+02',120),
		'SRET' => array('Srednekolymsk Time','+11',660),
		'SLST' => array('Sri Lanka Standard Time','+05:30',330),
		'SRT' => array('Suriname Time','-03',-180),
		'TAHT' => array('Tahiti Time','-10',-600),
		'TJT' => array('Tajikistan Time','+05',300),
		'THA' => array('Thailand Standard Time','+07',420),
		'TLT' => array('Timor Leste Time','+09',540),
		'TKT' => array('Tokelau Time','+13',780),
		'TOT' => array('Tonga Time','+13',780),
		'TRT' => array('Turkey Time','+03',180),
		'TMT' => array('Turkmenistan Time','+05',300),
		'TVT' => array('Tuvalu Time','+12',720),
		'ULAT' => array('Ulaanbaatar Standard Time','+08',480),
		'ULAST' => array('Ulaanbaatar Summer Time','+09',540),
		'UYT' => array('Uruguay Standard Time','-03',-180),
		'UYST' => array('Uruguay Summer Time','-02',-120),
		'UZT' => array('Uzbekistan Time','+05',300),
		'VUT' => array('Vanuatu Time','+11',660),
		'VET' => array('Venezuelan Standard Time','-04',-240),
		'VLAT' => array('Vladivostok Time','+10',600),
		'VOLT' => array('Volgograd Time','+04',240),
		'VOST' => array('Vostok Station Time','+06',360),
		'WAKT' => array('Wake Island Time','+12',720),
		'WAST' => array('West Africa Summer Time','+02',120),
		'WAT' => array('West Africa Time','+01',60),
		'WEST' => array('Western European Summer Time','+01',60),
		'WET' => array('Western European Time','±00',0),
		'WIT' => array('Western Indonesian Time','+07',420),
		'WST' => array('Western Standard Time','+08',480),
		'YAKT' => array('Yakutsk Time','+09',540),
		'YEKT' => array('Yekaterinburg Time','+05',300)
	);
	
	
	/** Month names**/
	public static $months = array (
		1 => 'January',
		2 => 'February',
		3 => 'March',
		4 => 'April',
		5 => 'May',
		6 => 'June',
		7 => 'July',
		8 => 'August',
		9 => 'September',
		10 => 'October',
		11 => 'November',
		12 => 'December'	
	);
	
	public static $days = array(
		'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Satuday'
	);
	
	
	
	/**********************
	* language customization
	*************************/
	public static $languages = array(
		'en' => 'English',
		'si' => 'සිංහල'
	);
	

}


?>