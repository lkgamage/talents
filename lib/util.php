<?php

/**
 * @package Amplify
 * Utility function class
 *
 */
 
 if(!defined('OPENSSL_RAW_DATA')){
	 define('OPENSSL_RAW_DATA', 1);
 }
 
class Util {
    //public static $MONTHS = array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEPT","OCT","NOV","DEC");

    /**
     * Convert DB time stamp to given time zone and return printable date 
     * Assume that System time zone is UTC
	 * IF $withtime == true, return date+time;
	 * IF $with_time == 1 ,return array (date, time);
     */
     public static function ToDate ($stamp, $with_time = false, $target_zone = NULL, $source_zone = 0, $day_name = false) {
		 
		if (empty($stamp)) {
            return "";
        }
		 
		 $dt = strtotime($stamp);
		 
		 if(!$dt){
			 return false;
		 }
		 
		if(empty($target_zone)){
			$target_zone = Session::timezone();
		}
		
		$dt -= ($source_zone - $target_zone)*60;
		
		if($with_time === true){
			
			if($day_name) {
				return date('l M j, Y h:ia',$dt);
			}
			else {
				return date('M j, Y h:ia',$dt);
			}
		}

        if($with_time){
			if($day_name) {
				return array(date('l M j, Y',$dt), date('h:ia', $dt));
			}
			else {
				return array(date('M j, Y',$dt), date('h:ia', $dt));
			}
		}
		
		if($day_name) {
			return date('l M j, Y',$dt);
		}
		else {
        	return date('M j, Y',$dt);
		}
    }
	
	
	/**
	* Convert system timestamp to short date
	* Month date only
	*/
	public static function ToShortDate ($stamp, $with_time = false) {
		
		if (empty($stamp)) {
            return "";
        }
		 
		 $dt = strtotime($stamp);
		 
		 if(!$dt){
			 return false;
		 }
		 
		if(empty($target_zone)){
			$target_zone = Session::timezone();
		}
		$source_zone = 0;
		$dt -= ($source_zone - $target_zone)*60;
		
		if($with_time){
			return date('M j, h:ia',$dt);
		}
		else {
			return date('M j',$dt);
		}
	}
	
	
	/**
	* Convert user entred date and time values to 
	* system date GMT ++ 0)
	*/
	public static function ToSysDate ($d, $t = '00:00'){
		
		if(empty($d)){
			return '';			
		}

		if($t == '00:00' && strpos($d, ':') > 0){
			
			$dt = strtotime($d);
		}
		else{
			$dt = strtotime($d.' '.$t);	
		}
		
		/*
		$e = explode(' ', $d);
		if(count($e) > 1){
			$d = $e[0];	
		}
		*/
		$source_zone = Session::timezone();
				
		if(!empty($dt)) {
		
			$dt -= $source_zone*60;
			return date("Y-m-d H:i:s", $dt);
		
		}
		
		
	}
	
	
	
	/**
	* Convert given system date into clients time zone and give date
	*/
	public static function ToZone ($stamp, $with_time = false, $target_zone = NULL, $source_zone = 0) {
		 
		if (empty($stamp)) {
            return "";
        }
		 
		 $dt = strtotime($stamp);
		 
		if(empty($target_zone)){
			$target_zone = Session::timezone();
		}
		
		$dt -= ($source_zone - $target_zone)*60;
		
		if($with_time === true){
			return date('Y-m-d H:i:s',$dt);
		}

        if($with_time){
			return array(date('Y-m-d',$dt), date('H:i:s', $dt));
		}
		
        return date('Y-m-d',$dt);
	}
	
	/**
	* Convert date/time to given time zone
	* ($date, $time, $source_zone, $target_zone)
	*/
	public static function DateTimeToZone ($d, $t, $sz, $tz){
	
				
		$t = preg_split("/[: ]/", $t);
		
		if($t[2] == 'PM' || $t[2] == 'pm'){
			$t[0] += 12;	
		}

		$dt = strtotime($d);

		// add time
		$dt = $dt + ($t[0]*60*60) + ($t[1]*60);
		// time zone adjestment GMT + time zone difference
		$dt = $dt - ($sz - $tz)*60;
		return date("Y-m-d H:i:s", $dt);
		
	}
	
	/**
	* Convert time span into a minutes
	*/
	public static function ToMinutes ($span) {
		
		$span = strtolower($span);
		
		$parts = explode(' ', $span);
		
		$minutes = 0;
		
		foreach ($parts as $p){
			
			if(empty($p)){
				continue;
			}
			
			$time = 0;
			
			if(strpos($p,'m') > 0){
				$time = (int)str_replace('m', '', $p);
			}
			elseif(strpos($p, 'h') > 0){
				$time = (int)str_replace('h', '', $p);
				$time = $time*60;	
			}
			elseif(strpos($p, 'd') > 0){
				$time = (int)str_replace('d', '', $p);
				$time = $time*60*24;	
			}
			else{
				$time = (int)$p;	
			}
			
			if($time && $time > 0){
				$minutes += $time;
			}
			
		}
					
		return $minutes;
	}
	
	/**
	* Convert time stamp to readable format
	*/
	public static function stampToTime ($time) {
		
		if(empty($time)){
			return;	
		}
		
		$tp = explode(':', $time);
		
		$h = (int)$tp[0];
		
		$ap = $h >= 12 ? 'PM' : 'AM';
		
		if($h == 0){
			$h = 12;	
		}
		
		if($h > 12){
			$h = $h - 12;	
		}
		
		return $h.':'.$tp[1].' '.$ap;
		
	}
	
	/**
	* Convert readable time format to time stamp
	*/
	public static function timeToStamp ($time) {
		
		$time = strtolower($time);
		list($t, $a) = explode(' ', $time);
		list($h, $m) = explode(':', $t);
		
		if($h == 12){
			if($a == 'am'){
				$h = '00';
			}
		}
		elseif($a == 'pm'){
			$h += 12;	
		}
		
		return $h.':'.$m.':00';
	}
	
	
	/**
	* Convert minutes into readable format
	* $query_format is hh:mm:00
	*/
	public static function ToTime ($minutes, $query_format = false){
		
		if($query_format){
			//return 	($a < 0 ? '-' : '').abs(floor($a/60)).':'.abs($a%60).':00';
			return (floor($minutes/60)).':'.($minutes%60).':00';
				
		}	
	
		if($minutes < 60){
			return $minutes.'min';	
		}
		elseif($minutes < 1440){
			$hour = floor($minutes/60);
			$min = $minutes%60;
			
			return (!empty($min)) ? $hour.'hr '.$min.'min' : $hour.'hr';
		}
		else{
			$days = floor($minutes/1440);
			$rem = $minutes%1440;
			$hour = floor($rem/60);
			$min = $rem%60;
			
			if(!empty($min)){
				return 	$days.'d '.$hour.'hr '.$min.'min';
			}
			else{
				if(!empty($hour)){
					return 	$days.'d '.$hour.'hr ';
				}
				else{
					return 	$days.'d ';
				}
			}
		}
		
		
		return '';
	}
	
	/**
	* Get free time between given appounts/date-times 
	* return array(start, end, interval in minutes) OR null
	*/
	public function interval ($dt1, $span, $dt2){
		
		$t1 = strtotime($dt1);
		$t1 += $span*60;
		
		$t2 = strtotime($dt2);
		
		if($t2 - $t1 > 15){
			
			$d1 = Util::ToDate(date('Y-m-d H:i:s',$t1), 1);
			$d2 = Util::ToDate(date('Y-m-d H:i:s',$t2), 1);
			
			return array(
				0 => $d1[1],
				1 => $d2[1],
				2 => round(($t2 - $t1)/60)
			);
			
		}
		
		return NULL;
	}
	
	/**
	* Get month,week index of the given date
	*/
	public static function weekIndex ($date = NULL){
			
		$dt = (!empty($date)) ? strtotime($date) : time();
		$first = date('Y-m-01', $dt);
		
		$first = strtotime($first);

		return  ceil(($dt - $first)/(7*24*60*60));
		
	}
	
	/**
     * Get readable time spans from oracle time stamp
     * Today, Yesterday, X days ago (max 2), date
     */
    public static function ToReadableDate ($datetime, $clear_date = false, $time_zone = null) {

       
		
		if (empty($datetime)) {
            return "";
        }
						 
		if($clear_date){
			return self::ToDate($datetime, true,$time_zone);
		}

		$stamp = strtotime($datetime);
        $now = time();
        $today = strtotime('today');
        $thismonth = strtotime(date('F') . " 1");
        $aday = 86400;

        if ($stamp >= $now - 60) {
            return "Just Now";
        } elseif ($stamp >= $now - 3600) {
            return round(($now - $stamp) / 60) . " minutes ago";
        } elseif ($stamp > $today - $aday) {
            return round(($now - $stamp) / 3600) . " hours ago";
       /* } elseif ($stamp > ($today - $aday)) {
            return "Yesterday";*/
        } elseif ($stamp > ($today - $aday * 2)) {
            return "2 days ago";
        } elseif ($stamp > ($today - $aday * 3)) {
            return "3 days ago";
        }  else {
            return self::ToDate($datetime, true, $time_zone);
        }
    }
	
	/**
	* Get time zone based on time offset
	*/
	public static function findTimeZone ($offset){
		
		foreach (DataType::$timezones as $a => $t){
			
			if($t[2] == $offset){
				return $a;	
			}
			
		}
		
		return 'UTC';
	}
	
	
	/**
	* Cauculate time data for appointment class 
	*/
	public static function calcAppointmentTime ($begin, $duration, $interval) {
		
		
		$dt = strtotime($begin);
		$tz = Session::timezone();
		$dt += $tz*60;
		
		$out = array('begin' => array(), 'end' => array(), 'duration' => '', 'interval' => array());
		
		$out['begin']['date'] = date('M j, Y', $dt);
		$out['begin']['time'] = date('h:ia', $dt);
		
		$dt += $duration*60;
		
		$out['end']['date'] = date('M j, Y', $dt);
		$out['end']['time'] = date('h:ia', $dt);
		
		$out['duration'] = Util::ToTime($duration);
		
		if(!empty($interval)){
			
			$out['interval']['begin'] = $out['end']['time'];
			
			$dt += $interval*60;
			$out['interval']['end'] =date('h:ia', $dt);
			
			$out['interval']['duration'] = Util::ToTime($interval);
		}
		
		return $out;
		
	}
	
	
    public static function sort2d_bycolumn($array, $column, $method, $has_header = null)
        {
        
       // if ($has_header)  $header = array_shift($array);
        $narray = array();
        foreach ($array as $key => $row) {
          $narray[$key]  = $row[$column]; 
          $naarray[$key]  = $row['ID']; 
          }
             array_multisort($narray, SORT_DESC, $naarray, SORT_ASC, $array);
    
        
        return $array;
        }
	
	public static function maskPhone ($num){
		
		return str_repeat('*', strlen($num)-4).substr($num, strlen($num)-4, 4);
		
	}
	
	public static function maskEmail ($email){
		$p = explode('@', $email);
		return substr($email, 0, 2).str_repeat('*', strlen($p[0])-2).'@'.$p[1];
	}
		
    /**
     * Convert oracle time stamp into Unix time stamp
     */
    public static function ToTimeStamp ($stamp, $time_zone = null) {

        if (empty($stamp)) {
	
            return "";
        }

		if(strpos($stamp, ':') == false){
			$date = DateTime::createFromFormat("Y-m-d", $stamp);
		}
		else {
       	 	$date = DateTime::createFromFormat("Y-m-d H:i:s", $stamp);
		}

       /* if (!$date) {
            $date = DateTime::createFromFormat("d-M-y", $oracle_stamp);
        }
		*/
		
        if (!$date) {
            return "";
        }

        if (!empty($time_zone)) {
            $date->setTimezone($time_zone);
        }

        return $date->getTimestamp();
    }

    

    /**
     * Campare dates
     * Campare up to minute
     */
    public static function CompDate ($date1, $date2) {

        $dt1 = DateTime::createFromFormat("d-M-y h.i.s.u A", $date1);
        $dt2 = DateTime::createFromFormat("d-M-y h.i.s.u A", $date2);

        //$d1 = $dt1->format('Y-m-d H:i');
        //$d2 = $dt2->format('Y-m-d H:i');
        $diff = $dt1->diff($dt2);

        return ($diff->y == 0 && $diff->m == 0 && $diff->d == 0 && $diff->h == 0 && $diff->i == 0);
    }

    /**
     * Convert oracle date part into days ago value
     */
    public static function ToDaysAgo ($oracle_stamp) {

        $date = DateTime::createFromFormat("d-M-y", $oracle_stamp);
        $time = $date->getTimestamp();
        $now = time();

        return floor(($now - $time) / 86400);
    }

    /**
     * get unique id for given object
     * Unique id calculate based on created date and object id
     */
    public static function getUniqueObjectID ($object) {

        $ts = self::ToTimeStamp($object->created);

        return $object->id . "-" . date("YmdHis", $ts);
    }
	
	
	
	

    /**
     * Display debug varibale in a readble manner
     */
    public static function debug ($var) {

        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }

    /**
     * Map given short url path into full relative url
     *
     */
    public static function mapURL ($path, $full_qualified = false, $force_ssl = null) {

        if (!empty($path) && $path[0] != '/') {
            $path = "/" . $path;
        }

        if ($full_qualified) {

            if (isset($_SERVER['HTTPS']) || $force_ssl === true) {
                return "https://" . RequestObject::$domain . Config::$base_url . $path;
            } elseif ($force_ssl === false) {
                return "http://" . RequestObject::$domain . Config::$base_url . $path;
            } else {
                //return "http://".Config::$domain.Config::$base_url.$path;
                return "//" . RequestObject::$domain . Config::$base_url . $path;
            }
        }

        return Config::$base_url . $path;
    }

    /**
     * Translate various type of urls into a unified format
     */
    public static function buildURL ($source, $site_url) {

        if (empty($source)) {
            return self::mapURL($site_url, true);
        }

        // absolute urls
        if ($source[0] == '/') {
            return self::mapURL($site_url . $source, true);
        } // full qualified urls
        elseif (strpos($source, 'http') !== false) {

            if (strpos($source, 'https') !== false) {

                if (strpos($source, 'https:') === false) {
                    return str_replace('https', 'https:', $source);
                } else {
                    return $source;
                }
            } elseif (strpos($source, 'http') !== false) {

                if (strpos($source, 'http:') === false) {
                    return str_replace('http', 'http:', $source);
                } else {
                    return $source;
                }
            }

            return $source;
        } // relative urls
        else {
            return self::mapURL($site_url . '/' . $source, true);
        }
    }

    /**
     * Encript given string
     * Return encripted string
     */
    public static function encrypt ($pure_string, $encryption_key) {
		
		//$iv_size = openssl_cipher_iv_length("AES-128-CBC");
		//$iv = openssl_random_pseudo_bytes($iv_size);
		//echo base64_encode($iv);
		

		$encrypted_string = openssl_encrypt( $pure_string, 'AES-128-CBC', $encryption_key, OPENSSL_RAW_DATA, Config::$IV);

        return $encrypted_string;
    }

    /**
     * Decript string
     * Return original string
     */
    public static function decrypt ($encrypted_string, $encryption_key) {
        
		//$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        //$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
       // $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);

		$decrypted_string = openssl_decrypt($encrypted_string, 'AES-128-CBC', $encryption_key, OPENSSL_RAW_DATA, Config::$IV);

        return $decrypted_string;
    }
	
	
	/**
	* Set a cookie for given key => value
	* This stay forever, no exp date
	*/
	public static function setCookie ($key, $val, $days = NULL){
		
		if(empty($days)){
			/** 10 years from now */
			$expire = time() + 315360000; 
		}
		else{
			$expire = time() + 60*60*24*$days; 
		}

		$cookie = Util::encrypt($val, Config::$cookie_encript_key);
		$cookie = base64_encode($cookie);
		$cookie = trim($cookie,'=');
		
		// setting cookie
		@setcookie($key, $cookie,$expire, "/", Config::$domain);
		
	}
	
	/**
	* Clear given cookie
	*/
	public static function clearCookie ($key){
		
		$expire = time() - 86400; 

		// setting cookie
		setcookie($key, '',$expire, "/", Config::$domain);
		
	}
	
	/* Get a given cookie
	*  return null if no cookies set
	*/
	public static function getCookie ($key){
		
		 if (isset($_COOKIE[$key])) {

            $cookie = base64_decode($_COOKIE[$key]);
            $val = Util::decrypt($cookie, Config::$cookie_encript_key);
			return rtrim($val, '=+');
            
        }

		return NULL;
	}
	
	
	

    /**
     * Decript string
     * Return original string
     */
    public static function genGuid ($includeHyphen = false) {
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
//            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));

            $hyphen = '';
            if ($includeHyphen) {
                $hyphen = chr(45);// "-"
            }
            $uuid = substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);

            return $uuid;
        }

    }

    /**
     * Create associative array from indexed array
     * array : array to be re-formatted
     * key : key field name
     * value : if present, return key => value array
     */
    public static function groupArray ($array, $key, $value = null) {

        $output = array();
        $objects = array();

        foreach ($array as $element) {

            if (!isset($element[$key])) {
                continue;
            }

            if (!empty($value)) {
                $output[$element[$key]] = $element[$value];
            } else {

                $output[$element[$key]] = $element;
            }
        }

        return $output;
    }
	
	/**
	* Group result by spliiting array key values
	*/
	public static function groupResult ($data, $multi_row = false) {
		
		$out = array();
		
		if(count($data) == 1 && !$multi_row) {
		
			foreach ($data as $k => $v){
				
				list($t, $f) = explode('__',$k);
				if(!isset($out[$t])){
					$out[$t] = array();
				}
				
				$out[$t][$f] = $v;	
			}
		
		}
		else{
			
			foreach ($data as $item) {
				
				$line = array();
			
				foreach ($item as $k => $v){
					
					list($t, $f) = explode('__',$k);
					if(!isset($line[$t])){
						$line[$t] = array();
					}
					
					$line[$t][$f] = $v;	
				}
			
				$out[] = $line;
			}
			
		}
		
		
		return $out;
		
	}

    /**
     * Create group of associative array form indexed array
     * array : indexd arry to be re-formatted
     * key : key field name
     * value : if present only value will be included
     */
    public static function groupMultiArray ($array, $key, $value = null) {

        $output = array();
        $objects = array();

        foreach ($array as $element) {

            if (!isset($output[$element[$key]])) {
                $output[$element[$key]] = array();
            }

            if (!empty($value)) {
                $output[$element[$key]][] = $element[$value];
            } else {
                $output[$element[$key]][] = $element;
            }
        }

        return $output;
    }
	
	/**
	* Check how many given elemnst found in the array
	*/
	public static function howManyInArray ($array, $keys){
		
		$x = 0;
		
		foreach ($keys as $k){
			
			if(in_array($k, $array)){
				$x++;	
			}
				
		}
		
		return $x;
	}
	
	
	/**
	* Create a string from  given arguments, 
	* empty values will be removed
	*/
	public static function makeString (...$items) {
		
		$arr = array();
		
		foreach ($items as $item){
			
			if(!empty($item)){
				$arr[] = $item;	
			}
		}
		
		return implode(", ", $arr);
	}
	
	/* Create a non empty array from  given arguments, 
	* empty values will be removed
	*/
	public static function makeArray (...$items) {
		
		$arr = array();
		
		foreach ($items as $item){
			
			if(!empty($item)){
				$arr[] = $item;	
			}
		}
		
		return $arr;
	}


    /** Convert array into key=value string */
    public static function array2String ($array, $sep = " ") {

        $str = array();

        foreach ($array as $key => $val) {
            $str[] = $key . "=" . $val;
        }

        return implode($sep, $str);
    }

    /**
     * Execute codes in template file and return output
     * This function doesn't support contextual data, but basic url mapping codes
     * path : part relative to /templates folder
     * vars : array of variables to pass into the template
     */
    public static function executeTemplteCodes ($path, $vars = array()) {

        if (!empty($vars)) {
            extract($vars);
        }

        ob_start();

        include(dirname(__FILE__) . "/../templates" . $path);

        return ob_get_clean();
    }



    /**
     * Trace errors
     */
    public static function trace () {

        if (Config::$debug) {

            $trace = debug_backtrace();

            echo '<style>.dberror{background-color:#999;} .dberror td{background-color:#fff;}</style>
        <table border="0" cellspacing="1" cellpadding="5" class="dberror">';

            foreach ($trace as $t) {

                echo ' <tr>
          <td>File</td>
          <td>' . $t['file'] . ' on line ' . $t['line'] . '</td>
          </tr>';
            }
            echo '  </table>';
        }
    }

   
	
	/**
	* Convert to base 36
	*/
	public static function base36encode ($num){
		
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$val = '';
		
		while(true){
			
			$rem = $num%35;		
			
			$num =  ($num - $rem)/35;
			
			if( $rem > 0) {
				$val .= $chars[$rem];
			}
			
			if($num == 0){
				break;	
			}	
			
		}
		
		return strrev($val);
	}
	
	public static function base36decode ($val){
		
		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$val = strrev($val);
		$a = 0;
		
		for($i = 0; $i < strlen($val); $i++){
			$p = strpos($chars, $val[$i]);
			$a += $p*(pow(35,$i));
		}
		
		return $a;
	}

    /**
     * Build form json based on given array of field names
     */
    public static function buildFormJSON ($fields, $defaults) {

        $defaults = Util::groupArray($defaults, 'name');

        // build form josn
        $json = array(
            'id' => 0,
            'title' => '',
            'desc' => '',
            'css' => '',
            'buttons' => array('next' => 'Next', 'back' => 'Back'),
            'elements' => array()
        );

        // create general donation form fields
        foreach ($fields as $fname => $required) {

            if (empty($defaults[$fname])) {
                continue;
            }

            $field = $defaults[$fname];

            $element = array(
                'type' => 'f',
                'id' => $field['id'],
                'name' => $field['name'],
                'label' => $field['name'],
                'htmlType' => $field['htmlType']
            );

            if (!empty($field['defaultContent']) && !empty($field['defaultContent']['data'])) {

                $element['options'] = $field['defaultContent']['data'];
            }

            if ($required) {

                $element['validators'][] = array('required' => array(
                    'type' => 'required',
                    'name' => 'Required',
                    'message' => $field['name'] . ' required'
                ));
            }

            $json['elements'][] = $element;
        }

        return json_encode(array(0 => $json));
    }

    /**
     * Get post values for all fileds in the form
     */
    public static function getFormPost ($page) {

        //print_r($_POST);
        //print_r($page);

        $data = array();

        if (!empty($page['elements'])) {

            foreach ($page['elements'] as $ele) {

                if ($ele['type'] == 'f' || $ele['type'] == 'x' || $ele['type'] == 'q') {

                    $name = (!empty($ele['columnName'])) ? $ele['columnName'] : str_replace(" ", "_", $ele['name']);

                    if ($ele['type'] == 'q') {
                        if (isset($_POST[$name])) {
                            $data['questions'][$name] = $_POST[$name];
                        } else {
                            $data['questions'][$name] = "";
                        }
                    } else {

                        if (isset($_POST[$name])) {
                            $data[$name] = $_POST[$name];
                        } else {
                            $data[$name] = "";
                        }
                    }
                }
            }
        }

        return $data;
    }
	


    /*************  Word Helper *****************/
    private static $_plural = array(
        '/(s)tatus$/i' => '\1tatuses',
        '/(quiz)$/i' => '\1zes',
        '/^(ox)$/i' => '\1\2en',
        '/([m|l])ouse$/i' => '\1ice',
        '/(matr|vert|ind)(ix|ex)$/i' => '\1ices',
        '/(x|ch|ss|sh)$/i' => '\1es',
        '/([^aeiouy]|qu)y$/i' => '\1ies',
        '/(hive)$/i' => '\1s',
        '/(chef)$/i' => '\1s',
        '/(?:([^f])fe|([lre])f)$/i' => '\1\2ves',
        '/sis$/i' => 'ses',
        '/([ti])um$/i' => '\1a',
        '/(p)erson$/i' => '\1eople',
        '/(?<!u)(m)an$/i' => '\1en',
        '/(c)hild$/i' => '\1hildren',
        '/(buffal|tomat)o$/i' => '\1\2oes',
        '/(alumn|bacill|cact|foc|fung|nucle|radi|stimul|syllab|termin)us$/i' => '\1i',
        '/us$/i' => 'uses',
        '/(alias)$/i' => '\1es',
        '/(ax|cris|test)is$/i' => '\1es',
        '/s$/' => 's',
        '/^$/' => '',
        '/$/' => 's',
    );

    private static $_irregular = array(
        'atlas' => 'atlases',
        'beef' => 'beefs',
        'brief' => 'briefs',
        'brother' => 'brothers',
        'cafe' => 'cafes',
        'child' => 'children',
        'cookie' => 'cookies',
        'corpus' => 'corpuses',
        'cow' => 'cows',
        'criterion' => 'criteria',
        'ganglion' => 'ganglions',
        'genie' => 'genies',
        'genus' => 'genera',
        'graffito' => 'graffiti',
        'hoof' => 'hoofs',
        'loaf' => 'loaves',
        'man' => 'men',
        'money' => 'monies',
        'mongoose' => 'mongooses',
        'move' => 'moves',
        'mythos' => 'mythoi',
        'niche' => 'niches',
        'numen' => 'numina',
        'occiput' => 'occiputs',
        'octopus' => 'octopuses',
        'opus' => 'opuses',
        'ox' => 'oxen',
        'penis' => 'penises',
        'person' => 'people',
        'sex' => 'sexes',
        'soliloquy' => 'soliloquies',
        'testis' => 'testes',
        'trilby' => 'trilbys',
        'turf' => 'turfs',
        'potato' => 'potatoes',
        'hero' => 'heroes',
        'tooth' => 'teeth',
        'goose' => 'geese',
        'foot' => 'feet',
        'foe' => 'foes',
        'sieve' => 'sieves'
    );
	
	 protected static $_singular = array(
        '/(s)tatuses$/i' => '\1\2tatus',
        '/^(.*)(menu)s$/i' => '\1\2',
        '/(quiz)zes$/i' => '\\1',
        '/(matr)ices$/i' => '\1ix',
        '/(vert|ind)ices$/i' => '\1ex',
        '/^(ox)en/i' => '\1',
        '/(alias)(es)*$/i' => '\1',
        '/(alumn|bacill|cact|foc|fung|nucle|radi|stimul|syllab|termin|viri?)i$/i' => '\1us',
        '/([ftw]ax)es/i' => '\1',
        '/(cris|ax|test)es$/i' => '\1is',
        '/(shoe)s$/i' => '\1',
        '/(o)es$/i' => '\1',
        '/ouses$/' => 'ouse',
        '/([^a])uses$/' => '\1us',
        '/([m|l])ice$/i' => '\1ouse',
        '/(x|ch|ss|sh)es$/i' => '\1',
        '/(m)ovies$/i' => '\1\2ovie',
        '/(s)eries$/i' => '\1\2eries',
        '/([^aeiouy]|qu)ies$/i' => '\1y',
        '/(tive)s$/i' => '\1',
        '/(hive)s$/i' => '\1',
        '/(drive)s$/i' => '\1',
        '/([le])ves$/i' => '\1f',
        '/([^rfoa])ves$/i' => '\1fe',
        '/(^analy)ses$/i' => '\1sis',
        '/(analy|diagno|^ba|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\1\2sis',
        '/([ti])a$/i' => '\1um',
        '/(p)eople$/i' => '\1\2erson',
        '/(m)en$/i' => '\1an',
        '/(c)hildren$/i' => '\1\2hild',
        '/(n)ews$/i' => '\1\2ews',
        '/eaus$/' => 'eau',
        '/^(.*us)$/' => '\\1',
        '/s$/i' => ''
    );

    private static $_uninflected = array(
        '.*[nrlm]ese', '.*data', '.*deer', '.*fish', '.*measles', '.*ois',
        '.*pox', '.*sheep', 'people', 'feedback', 'stadia', '.*?media',
        'chassis', 'clippers', 'debris', 'diabetes', 'equipment', 'gallows',
        'graffiti', 'headquarters', 'information', 'innings', 'news', 'nexus',
        'proceedings', 'research', 'sea[- ]bass', 'series', 'species', 'weather'
    );

    private static $_cache = array();

    public static function pluralize ($word) {
		
        if (isset(static::$_cache['pluralize'][$word])) {
            return static::$_cache['pluralize'][$word];
        }
        if (!isset(static::$_cache['irregular']['pluralize'])) {
            static::$_cache['irregular']['pluralize'] = '(?:' . implode('|', array_keys(static::$_irregular)) . ')';
        }
        if (preg_match('/(.*?(?:\\b|_))(' . static::$_cache['irregular']['pluralize'] . ')$/i', $word, $regs)) {
            static::$_cache['pluralize'][$word] = $regs[1] . substr($regs[2], 0, 1) .
                substr(static::$_irregular[strtolower($regs[2])], 1);

            return static::$_cache['pluralize'][$word];
        }
        if (!isset(static::$_cache['uninflected'])) {
            static::$_cache['uninflected'] = '(?:' . implode('|', static::$_uninflected) . ')';
        }
        if (preg_match('/^(' . static::$_cache['uninflected'] . ')$/i', $word, $regs)) {
            static::$_cache['pluralize'][$word] = $word;

            return $word;
        }
        foreach (static::$_plural as $rule => $replacement) {
            if (preg_match($rule, $word)) {
                static::$_cache['pluralize'][$word] = preg_replace($rule, $replacement, $word);

                return static::$_cache['pluralize'][$word];
            }
        }
    }
	
	 public static function singularize($word)
    {
        if (isset(static::$_cache['singularize'][$word])) {
            return static::$_cache['singularize'][$word];
        }
        if (!isset(static::$_cache['irregular']['singular'])) {
            static::$_cache['irregular']['singular'] = '(?:' . implode('|', static::$_irregular) . ')';
        }
        if (preg_match('/(.*?(?:\\b|_))(' . static::$_cache['irregular']['singular'] . ')$/i', $word, $regs)) {
            static::$_cache['singularize'][$word] = $regs[1] . substr($regs[2], 0, 1) .
                substr(array_search(strtolower($regs[2]), static::$_irregular), 1);
            return static::$_cache['singularize'][$word];
        }
        if (!isset(static::$_cache['uninflected'])) {
            static::$_cache['uninflected'] = '(?:' . implode('|', static::$_uninflected) . ')';
        }
        if (preg_match('/^(' . static::$_cache['uninflected'] . ')$/i', $word, $regs)) {
            static::$_cache['pluralize'][$word] = $word;
            return $word;
        }
        foreach (static::$_singular as $rule => $replacement) {
            if (preg_match($rule, $word)) {
                static::$_cache['singularize'][$word] = preg_replace($rule, $replacement, $word);
                return static::$_cache['singularize'][$word];
            }
        }
        static::$_cache['singularize'][$word] = $word;
        return $word;
    }

   

    /**
     * Convert text into camel case
     */
    public static function toCamelCase ($str) {

        $str = ucwords($str);
        $words = explode(' ', $str);

        if (count($words) > 1) {

            $fw = array_shift($words);

            return strtolower($fw) . implode('', $words);
        } else {
            return strtolower($str);
        }
    }
	
	

    /**
     * function to build 15 minute time intervals for the $TIMES_OF_DAY DataType
     */
    static function hoursRange ($lower = 0, $upper = 86400, $step = 3600, $format = '') {
        $times = array();
        $times[''] = '';

        if (empty($format)) {
            $format = 'g:i a';
        }

        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);

            list($hour, $minutes) = explode(':', $increment);

            $date = new DateTime($hour . ':' . $minutes);

            $times[(string)$increment] = $date->format($format);
        }

        return $times;
    }

    /**
     * Take a piece of text and shorten it to the maximum number of sentences below the limit
     * This function will always return at least the first sentence
     * 
     * @param  string  $text         [description]
     * @param  integer $length_limit [description]
     * @return string                [description]
     */
    static function shortenBySentence($text, $length_limit = 110){
        if(strlen($text) > $length_limit){
            $sentences = explode(". ", $text);
            $text = $sentences[0] . ".";

            for($i = 1; $i < count($sentences); $i++){
                if(strlen($text) + strlen($sentences[$i]) > $length_limit){
                    break;
                }
                $text .= " " . $sentences[$i] . ".";
            }
        }

        return $text;
    }
      
	
	/**
	* Remove all special charaters , spaces and html tags
	* Return plain text
	*/
	public static function getText ($html) {
		
		if(empty($html)){
			return $html;	
		}
		
		$html = strip_tags($html);
		$html = str_replace(' ', '', $html);
		return preg_replace('/[^A-Za-z0-9\-]/', '', $html);

	}
	
	/**
	* Read given CSV file and return an associative array
	*/
	public static function readCSV ($path){
	
		$data = array();
		
		if (($handle = fopen($path, "r")) !== FALSE) {
			
			//get first line
			$headers = fgetcsv($handle, 5000, ",");
			
			while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
				
				$item = array();
				
				foreach ($headers as $i => $name){
					$item[$name] = $row[$i];
				}
				
				$data[] = $item;
			}
			
		}
		
		
		return $data;
	}
    
	
	
    
    
	
	/**
	* Time mapping
	*/
	public static $TimeMap = array(
				0 => 'N/A',
				15 => '15 Minutes',
				30 => '30 Minutes',
				45 => '45 Minutes',
				60 => '1 Hour',
				90 => '1.5 Hours',
				120 => '2 Hours',
				150 => '2.5 Hours',
				180 => '3 Hours',
				240 => '4 Hours',
				300 => '5 Hours',
				360 => '6 Hours',
				480 => '8 Hours',
				600 => '10 Hours',
				720 => '12 Hours',
				1440 => '1 Day',
				2880 => '2 Days'
			);
    
    /**
	* Serialise and base 64 encode POST data
	*/
	public static function serializePost (){
		
		$data = array();
		foreach ($_POST as $k=>$v){
			$data[] = $k.'='.urlencode($v);
		}
		
		$data = implode('&', $data);
		return base64_encode($data);
	}
	
	/**
	* Decode base64 data and add as post data
	*/
	public static function deserializePost ($str, $populate_post = true ) {
		
		
		
		if(empty($str)){
			return array();
		}
		
		$data = base64_decode($str);
		
		$data = explode('&', $data);
		$rv = array();
		
		foreach ($data as $d){
			
			list($k, $v) = explode('=', $d);
			
			if($populate_post && !isset($_POST[$k])) {
				$_POST[$k] = urldecode($v);
			}
			$rv[$k] = urldecode($v);
		}
		
		return $rv;
	}
	
	/**
	* Get value form post
	* return value or empty string
	*/
	public static function postValue ($key){
		
		return (isset($_POST[$key])) ? $_POST[$key] : '';
			
	}
	
	
	
	/**
	* Convert DB date time to given zone
	* Return a genaric GMT time stam that can be used with date() 
	* function to format date 
	*/
	public static function DateStampToZone ( $stamp, $sz, $tz){
		
		$dt = strtotime($stamp);	
		$dt = $dt - ($sz - $tz)*60;		
		return $dt;
	}
	
	
	
	/** 
	* Resize and move image
	*/
	public static function resizeMoveImage ( $original, $target, $twidth = 1200){
	
		$stat = getimagesize($original);
        $width = $stat[0];
		
		$local = Config::$base_path.'/'.$target;

        // image alredy in given size

        if ($width <= $twidth) {
			copy($original, $local);
			
			$s = new Storage();
			$res = $s->upload($local, $target,1);		
			//unlink($local);				
			return !empty($res['ObjectURL']) ? $res['ObjectURL'] : false;
        }

        $height = $stat[1];

        $parts = explode('.', $original);
        $ext = strtolower($parts[count($parts) - 1]);
        // load source image
        if ($ext == 'jpg' || $ext == 'jpeg') {
            $src = imagecreatefromjpeg($original);
        } elseif ($ext == 'png') {
            $src = imagecreatefrompng($original);
        } elseif ($ext == 'gif') {
            $src = imagecreatefromgif($original);
        } else {
            return false;
        }

     //   $theight = round(($twidth * $height) / $width);

      //  $theight = round(($twidth * $height) / $width);
	  	$theight = round(($twidth * $height) / $width);

        $image = imagecreatetruecolor($twidth, $theight);
        $white = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $white);

        imagecopyresampled($image, $src, 0, 0, 0, 0, $twidth, $theight, $width, $height);

        if ($ext == 'jpg' || $ext == 'jpeg') {

            imagejpeg($image, $local, 95);
        } elseif ($ext == 'png') {

            imagepng($image, $local, 100);
        } elseif ($ext == 'gif') {
            imagegif($image, $local);
        }

        imagedestroy($image);
        imagedestroy($src);
		
		// upload image to CDN
		$s = new Storage();
		$res = $s->upload($local, $target,1);		
		//unlink($local);				
		return !empty($res['ObjectURL']) ? $res['ObjectURL'] : false;
			
	}
	
	
	/**
	* Generate a thumb image width given width = height 
	*/
	public static function generateThumbImage ($original, $target, $width = 200) {

        // Thumbnail image width
        $thumb_width = $width;

        // Thumbnail image height
        $thumb_height = $width;
		
		$local = Config::$base_path.'/'.$target;

        $parts = explode('.', $original);
        $ext = strtolower($parts[count($parts) - 1]);

        //echo $ext." ".$original;

        // load source image
        if ($ext == 'jpg' || $ext == 'jpeg') {
            $src = @imagecreatefromjpeg($original);
        } elseif ($ext == 'png') {
            $src = imagecreatefrompng($original);
        } elseif ($ext == 'gif') {
            $src = imagecreatefromgif($original);
        } else {
            return false;
        }
		
		if($src === false){
			return;	
		}

        // create thumb image
        $thumb = imagecreatetruecolor($thumb_width, $thumb_height);
        $white = imagecolorallocate($thumb, 255, 255, 255);
        imagefill($thumb, 0, 0, $white);

        // get original height and width
        $stat = getimagesize($original);
        $o_width = $stat[0];
        $o_height = $stat[1];

        // croping
        if ($o_width <= $o_height) {
            // portrait
            $ratio = $o_width / $thumb_width;
            $height = ceil($ratio * $thumb_height);

            imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_width, $thumb_height, $o_width, $height);
        } else {
            // landscape
            $ratio = $o_height / $thumb_height;
            $width = ceil($ratio * $thumb_width);

            $x = floor(($o_width - $width) / 2);
            imagecopyresampled($thumb, $src, 0, 0, $x, 0, $thumb_width, $thumb_height, $width, $o_height);
        }

        // save image
        imagejpeg($thumb, $local, 100);
		imagedestroy($thumb);
        imagedestroy($src);		
		
		$s = new Storage();
		$res = $s->upload($local, $target,1);		
		//unlink($local);				
		return !empty($res['ObjectURL']) ? $res['ObjectURL'] : false;

    }
	
	
	
	/**
	* Get the photo collage layouts
	*/
	public static function collageLayout ($images){
		
		
		$txt = "";
		$total = count($images);
		
		if($total == 1){
			return 	'<div class="f_fw"><img src="'.$images[0]['i600'].'" ></div>';
		}
		
		for ($i = 0; $i < $total; $i++) {
			
			$img  = $images[$i];
			
			if($i == 0 && $total > 2){
				$txt .= '<div class="f_fw"><img src="'.$images[$i]['i600'].'" ></div>';	
				continue;
			}
			
			if($img['orientation'] == 'l'){
				// landscape
				
				
				if(($total - $i) >= 2){
				
					if($images[$i+1]['orientation'] == 'l'){
						$txt .= '<div class="f_lhw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_lhw"><img src="'.$images[$i]['i400'].'" ></div>';	
						continue;
					}
					else{
						$txt .= '<div class="f_lptw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_lpqw"><img src="'.$images[$i]['i400'].'" ></div>';	
						continue;
					}
				}
				
				$txt .= '<div class="f_fw"><img src="'.$images[$i]['i600'].'" ></div>';	
				
				
			}
			else{
				// portrait
				if(($total - $i) >= 3){
					
					if($images[$i+1]['orientation'] == 'p' && $images[$i+2]['orientation'] == 'p'){
						$txt .= '<div class="f_ptw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_ptw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_ptw"><img src="'.$images[$i]['i400'].'" ></div>';	
						continue;
					}
					
					if($images[$i+1]['orientation'] == 'l' && $images[$i+2]['orientation'] == 'l'){
						$txt .= '<div class="f_phw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_phw">';
						$txt .= '<div class="f_lsw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_lsw"><img src="'.$images[$i]['i400'].'" ></div>';
						$txt .= '</div>';	
						continue;
					}
					
											
				}
				
				if(($total - $i) >= 2){
					if($images[$i+1]['orientation'] == 'p'){
						$txt .= '<div class="f_phw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_phw"><img src="'.$images[$i]['i400'].'" ></div>';	
						continue;
					}
					else{
						$txt .= '<div class="f_lpqw"><img src="'.$images[$i]['i400'].'" ></div>';	
						$i++;
						$txt .= '<div class="f_lptw"><img src="'.$images[$i]['i400'].'" ></div>';	
						continue;
					}
				}
				
				
				$txt .= '<div class="f_fw"><img src="'.$images[$i]['i600'].'" ></div>';
			}
		}// each image
		
		return $txt;
	}
	
	public function encodeCardNumber ($number, $salt) {
		
	}
	
	/**
	* Decode and hash the card number
	*/
	public function decodeCardNumber ( $number, $salt, $hash = true ) {
		
		
		if($hash){
			return substr($number,-4);	
		}
		
	}
	
	/**
	* Return payment type icons by its code
	*/
	public function getCardTypeIcon ($type) {
		
		switch ($type){
			case DataType::$PAYMENT_CARD_VISA : return Util::mapURL('/images/card-visa.png');
			case DataType::$PAYMENT_CARD_MASTER : return Util::mapURL('/images/card-master.png');
			case DataType::$PAYMENT_CARD_AMEX : return Util::mapURL('/images/card-amex.png');
			case DataType::$PAYMENT_CARD_DISCOVER : return Util::mapURL('/images/card-discover.png');
			case DataType::$PAYMENT_CARD_JCB : return Util::mapURL('/images/card-jcb.png');
			case DataType::$PAYMENT_CARD_DINNERSCLUB : return Util::mapURL('/images/card-dinners.png');	
		}
		
		return '';
	}
	 
	 
	 /**
	* Log post variables
	*/
	public static function logPost () {
		
		$txt = array();
		foreach ($_POST as $key => $val){
			$txt[] =  $key;
		}
		
		
		file_put_contents(Config::$base_path.'/log.txt', implode("\n",$txt));	
	}
	
	
	/**
	* Create or update location record
	*/
	public static function saveLocation ($location = NULL){
		
		$customer = Session::getCustomer();
		
		if(empty($location)) {
			$location = new Location();
		}
		
		if(!empty($customer)){
			$location->customer_id = $customer['id'];
		}
		
		if(!empty($_POST['place_id'])) {
			$location->place_id = Validator::cleanup($_POST['place_id'], 20);
		}
		
		if(!empty($_POST['display'])) {
			$location->display = Validator::cleanup($_POST['display'], 400);
		}
		
		if(!empty($_POST['name'])) {
			$location->venue = Validator::cleanup($_POST['name'], 200);
		}
		
		if(!empty($_POST['address'])) {
			$location->address = Validator::cleanup($_POST['address'], 400);
		}
		
		if(!empty($_POST['city'])) {
			$location->city = Validator::cleanup($_POST['city'], 100);
		}
		
		if(!empty($_POST['region'])) {
			$location->region = Validator::cleanup($_POST['region'], 100);
		}
		
		if(!empty($_POST['postal'])) {
			$location->postalcode = Validator::cleanup($_POST['postal'], 10);
		}
		
		if(!empty($_POST['country'])){
			$location->country = Validator::cleanup($_POST['country'], 2);
		}
		
		$location->save();
		
		return $location;
	}
	
	/**
	* Extract images from chat messages
	*/
	public static function extractImages ($html){
		
		preg_match_all('/<img[^>]+>/i',$html, $result); 
		print_r($result);
		$img = array();
		foreach( $result[0] as $a => $img_tag)
		{
			preg_match_all('/(src)=("[^"]*")/i',$img_tag, $img[$a]);
		}
		
		print_r($img);
	}
	
	
	/**
	* Get Ip to geocode information
	*/
	public static function IP2Location ($ip){
		
		$url = 'https://ipgeolocation.abstractapi.com/v1/?api_key='.Config::$ip2locaton_key.'&ip_address='.$ip;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);       
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        
        $response = curl_exec($ch);

        curl_close($ch);

		return json_decode($response, true);
	}
	
	/**
	* Capture device informtaion and return a new/unsaved device objct
	*/
	public function getDevice () {
		
		if(!empty($_POST['device']) && !empty($_POST['dfp']) && is_numeric($_POST['dfp'])){
			
			$deviceinfo = json_decode($_POST['device'], true);
			
			if(!empty($deviceinfo)){
				
				$name = '';
				
				if(!empty($deviceinfo['device']) && !empty($deviceinfo['device']['vendor'])){
					$name = $deviceinfo['device']['vendor'].' '.$deviceinfo['device']['model'];
					if(!empty($deviceinfo['browser']) && !empty($deviceinfo['browser']['name'])){				
						$name .= " (".$deviceinfo['browser']['name'].")";
						
					}
				}
				else{
					$name .= $deviceinfo['os']['name'].' '.$deviceinfo['os']['version'].'/'. $deviceinfo['browser']['name'];
				}
			
				$device = new CustomerDevice();
				$device->name = Validator::cleanup($name, 40);
				$device->os = Validator::cleanup($deviceinfo['os']['name'], 10);
				$device->browser = Validator::cleanup($deviceinfo['browser']['name'], 20);
				$device->country = Session::getCountry();
				$device->fingureprint = Validator::cleanup($_POST['dfp'], 12);
				
				return $device;
			}
			
			
			
		}
		
		if(!empty($_POST['dfp'])){
			
			$device = new CustomerDevice();
			$device->fingureprint = Validator::cleanup($_POST['dfp'], 12);
			$device->name = 'Unknown Device';
			return $device;
		}
		
		
		
		return false;
	}
	
	
	/** Get formatted text for booking change request
	*/
	public static function getChangeRequestDesc ($change) {
		
			 $modified = array();
			 
			 if(!empty($change['session_start'])){
					$modified[] = "date/time";
			 }
			 if(!empty($change['location_id'])){
					$modified[] = "place";
			 }
			 
			 if(!empty($change['package'])){
					$modified[] = "package";
			 }
			 
			 if($change['discount'] != $change['ex_discount']){
					$modified[] = "discount";
			 }
			 
			  if(count($modified) == 1){
				  return $modified[0];  
			  }
			  elseif(count($modified) == 2){
				 return $modified[0].' and '.$modified[1];  
			  }
			  else{
				  $last = array_pop($modified);
				  return implode(", ", $modified).' and '.$last;
				  
			  }
		
	}
	
	
	/**
	* Extract fcebook video ID
	*/
	public function FBVideoID ($shorturl) {
		
		
		
		
	}
	
	
	
	
	
} // class Util

?>