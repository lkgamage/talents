<?php

/**
 * @package Amplify
 * Data element validator class
 * Validate certain data elemnts such as email, short url and username
 * against pre=defined set of ruls
 *
 * This is a staic class
 */
class Validator {
    /** Letters, Numbers, minus sign and dot only */
    //public static $ALPHANUMERIC = 1;

    /** Letts only (A-Z and a - z) */
    //public static $CHARACTERS = 2;

    /** Numbers only */
    //public static $NUMBERS = 3;

    /** Numbers, minus sign and dot only */
    //public static $EXTNUMBERS = 4;

    /** Extended set of characters A-Z, a-z, @,#,$,(,),[,],{,},*,&,%,!,?,+,-,/,',",comma, dot, space */
    //public static $EXTCHARACTERS = 5;

    /** Validation error message */
    public static $error = "";

    /** Reserved words, these are not avaialbe in site url, page url or usernames (unless system default page) */
    public static $reserved = array('admin','ping','administrator','agency','company','business','event','events','booking','appointment','package','packages','profile','message','messages','favourite','dashboard', 'system', 'support','help', 'page', 'group',  'search', 'talent', 'talented','curatalent','event','market','available','testing','reserved','please','choose','handle', 'page', 'showpage');

    // check whether given string is a reserved word
    public static function isReservedWord ($str) {
		
		if(method_exists('PageController', $str)){
			return true;
		}

        return in_array(strtolower($str), self::$reserved);
    }

    /** Validate Email address    */
    public static function isEmail ($email) {

        $email = trim($email);

        if (strlen($email) > 100) {
            self::$error = "Email address is too long. Maximum 100 characters allowed.";

            return false;
        }
		
		$parts = preg_split("/[@.]/", $email);
		if(count($parts) < 3){
			self::$error = "Invalid email address";
			return false;	
		}

        self::$error = "Invalid email address";

        $regex = "/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i";

        return preg_match($regex, $email) === 1;
    }

    /** Validate public url  */
    public static function isURL ($url) {

        $url = trim($url);

        self::$error = "Invalid URL";

        $regex = '/^(?!:\/\/)([a-zA-Z0-9]+\.)?[a-zA-Z0-9][a-zA-Z0-9-]+\.[a-zA-Z]{2,18}?$/';
		
		$regex = '#((https?)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i';
        return preg_match($regex, $url) === 1;
    }

    /** Validate site url (alias) for site */
    public static function isSiteURL ($url) {

        $url = trim($url);

        if (in_array(strtolower($url), self::$reserved)) {
            self::$error = "'{$url}' is a reserved word and cannot be used as a url";

            return false;
        }

        if (strlen($url) < 2 || strlen($url) > 200) {
            self::$error = 'Url should have minimum 2 characters and maximum 200 characters';

            return false;
        }

        self::$error = 'Invalid site url';

        $regex = '/^[a-zA-Z0-9\-\._\'\$\(\)\[\]&]+$/';

        return preg_match($regex, $url) === 1;
    }

    public static function isDateTime ($date, $time = null) {
        if (empty($date)) {
            return false;
        }

        if (!empty($time)) {
            try {
                $datetime = new DateTime($date . ' ' . $time);

                return $datetime instanceof DateTime;
            } catch (Exception $e) {
                return false;
            }
        } else {
            try {
                $date = new DateTime($date);

                return $date instanceof DateTime;
            } catch (Exception $e) {
                return false;
            }
        }
    }
	
	/**
	* Check if given date is a valid date
	*/
	public static function isDate ($year, $month, $day) {
		
		$limits = array(1=>31, 3=>31, 4=>30, 5=>31, 6=>30, 7=>31, 8=>31,9=>30, 10=>31,11=>30,12=>31);
		
		if($month == 2){
			
			if($year % 4 === 0){
				return $day <= 29;
			}
			else{
				return $day <= 28;
			}
		}
		else{
			return ($day <= $limits[$month]);	
		}
		
	}

    /** Validate short page url */
    public static function isPageURL ($url) {

        $url = trim($url);

        if (in_array(strtolower($url), self::$reserved)) {
            self::$error = "'{$url}' is a reserved word and cannot be used as a url";

            return false;
        }

        if (strlen($url) < 2 || strlen($url) > 100) {
            self::$error = 'Site url should have minimum 2 characters and maximum 100 characters';

            return false;
        }

        self::$error = "Invalid page url (Allowed characters A-Z,a-z,0-9,(,),-,_ and dot)";
        $regex = '/^[A-Za-z0-9_()-.]+$/';

        //$regex = '/^[a-z0-9\-_\.\(\)]+$/';

        return preg_match($regex, $url) === 1;
    }

    /** Validate username */
    public static function isUsername ($username) {

        $username = trim($username);

        if (in_array(strtolower($username), self::$reserved)) {
            self::$error = "'{$username}' is a reserved word and cannot be used as a username";

            return false;
        }

        // check length
        if (strlen($username) < 6) {
            self::$error = "Username should have minimum 6 characters";

            return false;
        }

        if (strlen($username) > 100) {
            self::$error = "Username is too long. Maximum 100 characters allowed.";

            return false;
        }

        //check reserved words

        self::$error = "Invalid username (Allowed characters A-Z,a-z,0-9,-,_ and dot)";

        $regex = '/^[a-z0-9\-_\.]+$/';

        return preg_match($regex, $username) === 1;
    }

    /** Validate password */
    public static function isPassword ($password) {

        $password = trim($password);

        self::$error = "Password should have minimum 8 characters";

        if (strlen(trim($password)) < 8) {
            return false;
        }

        return true;
    }

    /** Validate phone number */
    public static function isPhoneNumber ($number) {

        $number = trim($number);

        $regex = '/^[0-9\-\+]+$/';

        if (preg_match($regex, $number) === 1 && strlen(trim($number)) >= 10) {
            return true;
        }

        return false;
    }
	
	/** Validate credit crad */
	public static function isCreditCard ( $number ){
		
		if(empty($number) || !preg_match('/^[0-9]*$/', $number)){
			return false;	
		}
		
		$patterns = array (
			'/^4[0-9]{12}(?:[0-9]{3})?$/',
			'/^6(?:011|5[0-9]{2})[0-9]{12}$/',
			'/^3[47][0-9]{13}$/',
			'/^5[1-5][0-9]{14}$/',
			'/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',
			'/^(?:2131|1800|35\d{3})\d{11}$/'
		);
		
		foreach ($patterns as $p){
		
			if(	preg_match($p, $number)){
				return true;	
			}
		}
		
		
		return false;
		
	}
	
	/** Find credit card type by card number **/
	public static function getCardType ($number){
	
		$cards = array(
			'visa' => '/^4/',
			'master' => '/^(51|52|53|54|55|5018|5020|5038|5893|6304|6759|6761|6762|6763|2[2-7])/',
			'amex' => '/^(34|37)/',
			'diners' => '/^(300|301|302|303|304|305|36|54)/',
			'discover' => '/^(6011|622[1-9]|644|645|646|647|648|649|65)/',
			'jcb' => '/^(35[2-8])/',
			
		);
		
		foreach ($cards as $type => $pattern){
			
			if(preg_match($pattern, $number)){
				return $type;	
			}
		}
	
	
		return false;
	
	}
	
	/**
	* Cleanup phone number
	* if country code is present,
	* it will be removed
	*/
	public static function cleanupPhoneNumber ($phone, $co) {
		
		$phone = trim($phone);
		$phone = preg_replace('/[\(\)\-\.]/', '', $phone);
			
		if($phone[0] == '+'){
			$phone = str_replace('+'.$co, '', $phone );
		}
		
		$phone = ltrim($phone, '0');
		return  preg_replace("/[^0-9]/", "", $phone);
	}
	
	
	/**
	* Remove email and phone number form the text
	*/
	public static function removeEmails ($text){
		
		$patterns =	'/([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)/';
		
		return preg_replace($patterns, '*', $text);
	}
	
	/**
	* check if string has a phone number longer than 7 digit
	*/
	public static function hasPhoneNumber ($a){
		
		$b = [];
		preg_match("/\d{6,}/",  preg_replace("/[\- .,]/", "",$a), $b);
		
		return !empty($b);
		
	}
	
	
	
    /**
     * Clean up user input
     * Remove all php tags, trim string to given size
     * str : string to clean up
     * size : max number of charactors allowed in output string. default 2000
     * no_html : remove all html tags
     */
    public static function cleanup ($str, $size = 2000, $no_html = true) {

        $str = trim($str);
			

        // remove php tags
        $str = preg_replace('/<\\?.*(\\?>|$)/Us', '', $str);
		

        // remove html tags
        if ($no_html) {
            $str = strip_tags($str);
        }

        // trim string
        if ($size > 0) {
            if (strlen($str) > $size) {
                $str = substr($str, 0, $size);
            }
        }

        return $str;
    }
}

?>