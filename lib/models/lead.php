<?php

/**********************************************
* Lead Class
**********************************************/

class Lead extends Model{

	public $table = 'leads';

	public $fields = array(
		'id',
		'name',
		'phone',
		'email',
		'categories',
		'description',
		'notes',
		'status',
		'user_id',
		'marketter_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated lead_actions
	* Returns array of lead_action objects or empty array 
	*/

	public function getLeadActions (){

		$data = $this->db->query("SELECT * FROM lead_actions WHERE lead_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new leadAction();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated marketter
	* Returns marketter object or NULL 
	*/

	public function getMarketter (){

		$data = $this->db->query("SELECT * FROM marketters WHERE id = ".$this->data['marketter_id'], NULL, 1);

		if(!empty($data)){
			$obj = new marketter();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated user
	* Returns user object or NULL 
	*/

	public function getUser (){

		$data = $this->db->query("SELECT * FROM users WHERE id = ".$this->data['user_id'], NULL, 1);

		if(!empty($data)){
			$obj = new user();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
