<?php
		
/****************************
* Agency Class
*****************************/

class Agency extends Model{

	public $table = 'agencies';

	public $fields = array(
		'id',
		'customer_id',
		'name',
		'agency_type',
		'verified',
		'title',
		'description',
		'location_id',
		'timezone',
		'timeoffset',
		'multinational',
		'image_id',
		'banner_id',
		'active',
		'name_changed',
		'currency',
		'created',
		'modified',
		'deleted'
		);
	
	
	/**
	* general function to object check type
	*/
	public function isAgency () {
		return true;	
	}
	
	public function isTalent () {
		return false;	
	}
	
	/**
	* Get display picture
	*/
	public function getDP () {
	
		if(!empty($this->data['image_id'])){
			return new Image($this->data['image_id']);
		}
		
		return NULL;
	}

	/**
	* Get associated talents
	* Returns array of talent objects or empty array 
	*/

	public function getTalents ( $as_array = false){

		$data = $this->db->query("SELECT t.* , l.city, l.region, l.country, i.i400, i.thumb, p.id AS page_id, p.handle, p.active AS page_active, r.id as request_id,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id AND active = 1 LIMIT 0,1 ) AS t1,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id AND active = 1 LIMIT 1,1 ) AS t2,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id AND active = 1 LIMIT 2,1 ) AS t3
FROM talents t 
JOIN agency_request r ON (r.talent_id = t.id)
LEFT JOIN images i ON (i.id = t.image_id)
LEFT JOIN locations l ON (l.id = t.location_id)
LEFT JOIN pages p ON (p.talent_id = t.id)
WHERE r.agency_id = ".$this->data['id']." AND ISNULL(t.deleted) AND ISNULL(r.deleted) AND r.status = 'a'  ORDER BY t.name ", NULL);
		
		if($as_array){
			return $data;	
		}

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talent();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get pending or rejected requests sent to talents
	*/
	public function getPendingRequests () {
		
		
		$sql = "SELECT t.* , l.city, l.region, l.country, i.i400, i.thumb, p.id AS page_id, p.handle, p.active AS page_active, r.id as request_id, r.created,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id AND active = 1 LIMIT 0,1 ) AS t1,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id AND active = 1 LIMIT 1,1 ) AS t2,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id AND active = 1 LIMIT 2,1 ) AS t3
FROM talents t 
JOIN agency_request r ON (r.talent_id = t.id)
LEFT JOIN images i ON (i.id = t.image_id)
LEFT JOIN locations l ON (l.id = t.location_id)
LEFT JOIN pages p ON (p.talent_id = t.id)
WHERE r.agency_id = ".$this->data['id']." AND ISNULL(t.deleted) AND ISNULL(r.deleted) AND r.status != 'a'  ORDER BY r.created desc, t.name ";

		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = "select count(*) as total  FROM agency_request WHERE agency_id = ".$this->data['id']." AND  ISNULL(deleted) AND status != 'a'";
		$buffer->_component = "talent_requests";
		$buffer->_pagesize = 50;
		$buffer->_page = 1; 
		return $buffer;
		
		
	}
	
	

	/**
	* Get image 
	*/
	public function getImage () {
		
		if(!empty($this->data['image_id'])) {
			return new Image($this->data['image_id']);
		}
		else{
			
			$image = new image();
			
			$url = Util::mapURL('/images/placeholder-business.png');
						
			$image->i600 = $url;
			$image->i400 = $url;
			$image->thumb = $url;
			$image->url = $url;
			
			return $image;
		}			
	}
	
	
	/**
	* Get agency category names as a string
	*/
	public function getSkillString () {
		
		return DataType::$AGENCY_TYEPS[$this->data['agency_type']].' Agency';
		
	}
	


	/**
	* Get associated ledgers
	* Returns array of ledger objects or empty array 
	*/

	public function getLedgers (){

		$data = $this->db->query("SELECT * FROM ledgers WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new ledger();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated payout_methods
	* Returns array of payout_method objects or empty array 
	*/

	public function getPayoutMethods (){

		$data = $this->db->query("SELECT * FROM payout_methods WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payoutMethod();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated emails
	* Returns array of email objects or empty array 
	*/

	public function getEmails (){

		$data = $this->db->query("SELECT * FROM emails WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new email();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated phones
	* Returns array of phone objects or empty array 
	*/

	public function getPhones (){

		$data = $this->db->query("SELECT * FROM phones WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new phone();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated location
	* Returns location object or NULL 
	*/

	public function getLocation (){

		$data = $this->db->query("SELECT * FROM locations WHERE id = ".$this->data['location_id'], NULL, 1);

		if(!empty($data)){
			$obj = new location();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated usernames
	* Returns array of username objects or empty array 
	*/

	public function getUsernames (){

		$data = $this->db->query("SELECT * FROM usernames WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new username();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



		/**
	* Get associated pages
	* Returnsexisting page or create a new page
	*/

	public function getPage (){

		$data = $this->db->query("SELECT * FROM pages WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL, 1);
		
		$page = new Page();	
		
		if(!empty($data)){
			$page->populate($data);
		}
		else {
			$page->customer_id = $this->data['customer_id'];
			$page->agency_id = $this->data['id'];
			$page->active = 0;
			$page->save();
			
			// creating parts
			$sections = array('a', 'b', 'c', 'd', 'e', 'f', 'g');
			foreach ($sections as $s){
				$page->getSection($s);	
			}
		}
		
		return $page;
		
	}
	
	
	/**
	* Get summery about agency
	* picure,  owner, location 
	*/
	public function summary () {
		
		return $this->db->query("SELECT a.*, c.firstname, c.lastname, l.city, l.region, l.country, i.i400, i.thumb, p.id as page_id, p.handle, p.active as page_active
FROM agencies a
JOIN customers c ON (a.customer_id = c.id)
JOIN locations l ON (a.location_id = l.id)
LEFT JOIN images i ON (a.image_id = i.id)
LEFT JOIN pages p on (p.agency_id = a.id)
WHERE a.id = ".$this->data['id'], NULL, 1);
		
	}



	/**
	* Get associated booking_trail
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrail (){

		$data = $this->db->query("SELECT * FROM booking_trail WHERE agancy_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated booking_trails
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrails (){

		$data = $this->db->query("SELECT * FROM booking_trails WHERE agancy_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}






	/**
	* Get associated pages
	* Returns array of page objects or empty array 
	*/

	public function getPages (){

		$data = $this->db->query("SELECT * FROM pages WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new page();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


	/**
	* Make a reqest to talent to grant access
	*/
	public function requestTalentAccess ($talent_id, $create_new = false) {
		
		if(!$create_new) {
		
			$data = $this->db->query("SELECT * FROM agency_request
	WHERE agency_id = ".$this->data['id']." AND talent_id = ".$this->db->escape($talent_id)." and isnull(deleted)", NULL, 1);
	
			if(!empty($data)){
				
				$request = new AgencyRequest();
				$request->populate($data);
				return $request;
				
			}		
		}
		
		
		$request = new AgencyRequest();
		$request->agency_id = $this->data['id'];
		$request->talent_id = $talent_id;
		$request->status = DataType::$AGENCY_REQEST_PENDING;
		$request->save();
		return $request;	
		
	}
	
	
	/**
	* Get existing talent request
	*/
	public function getTalentRequest ($talent_id){
		
		$data = $this->db->query("select * from agency_request where agency_id = ".$this->data['id']." and talent_id = ".$talent_id." and isnull(deleted) order by id desc", NULL, 1);
		
		if(!empty($data)){
			$req = new AgencyRequest();
			$req->populate($data);
			return $req;	
		}
		
		
		return false;
	}


	/**
	* Get associated agency_request
	* Returns array of agency_request objects or empty array 
	*/

	public function getAgencyRequest (){

		$data = $this->db->query("SELECT * FROM agency_request WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agencyRequest();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated agency_type
	* Returns agency_type object or NULL 
	*/

	public function getAgencyType (){

		$data = $this->db->query("SELECT * FROM agency_types WHERE id = ".$this->data['agency_type'], NULL, 1);

		if(!empty($data)){
			$obj = new agencyType();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	public function getinfo () {
		
		$data = $this->db->query("select
		a.id as agency__id, 
a.customer_id as agency__customer_id, 
a.verified as agency__verified, 
a.name as agency__name, 
a.location_id as agency__location_id, 
a.timeoffset as agency__timeoffset, 
a.currency as agency__currency, 
a.image_id as agency__image_id, 
a.active as agency__active, 
a.created as agency__created, 
a.modified as agency__modified, 
a.deleted as agency__deleted,
c.id as customer__id, 
c.firstname as customer__firstname, 
c.lastname as customer__lastname, 
c.name_changed as customer__name_changed, 
c.dob as customer__dob, 
c.location_id as customer__location_id, 
c.timeoffset as customer__timeoffset, 
c.timezone as customer__timezone, 
c.currency as customer__currency, 
c.image_id as customer__image_id, 
c.active as customer__active, 
c.gender as customer__gender, 
c.is_trans as customer__is_trans, 
c.created as customer__created, 
c.modified as customer__modified, 
c.deleted as customer__deleted,
l.id as location__id, 
l.address as location__address, 
l.city as location__city, 
l.region as location__region, 
l.postalcode as location__postalcode, 
l.country as location__country, 
l.locked as location__locked, 
l.created as location__created, 
l.modified as location__modified, 
l.deleted as location__deleted,
i.id as image__id, 
i.i400 as image__i400, 
i.thumb as image__thumb, 
i.url as image__url, 
i.orientation as image__orientation,
p.id as page__id, 
p.customer_id as page__customer_id, 
p.talent_id as page__talent_id, 
p.agency_id as page__agency_id, 
p.handle as page__handle, 
p.banner_id as page__banner_id, 
p.description as page__description, 
p.active as page__active, 
p.created as page__created, 
p.modified as page__modified, 
p.deleted as page__deleted
from agencies a
join customers c on (c.id = a.customer_id)
join locations l on (l.id = a.location_id)
left join images i on (i.id = a.image_id)
left join pages p on (p.agency_id = a.id)
where a.id = ".$this->data['id'], NULL, 1);

		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		
		
		return $out;
		
		
	}



	/**
	* Get associated appoinments
	* Returns array of appoinment objects or empty array 
	*/

	public function getAppoinments ($from = NULL, $to = NULL, $as_array = false) {

		if(empty($from)){
			//
			$from =  date("Y-m-d");
		}
		else {
			$f = explode(' ', $from);
			
			if(count($f) == 1){
				$from =  Util::ToSysDate($from,'00:00');
			}
		}
		
		if(empty($to)){
			$to = date("Y-m-d H:i:s", strtotime('+1 day', strtotime($from)));		
		}
		else{
			$t = explode(' ', $to);		
			if(count($t) == 1){
				$to =  Util::ToSysDate($to,'11:59PM');	
			}
		}
		
		if($as_array){
			return $this->db->query("SELECT a.*, b.talent_fee, l.venue, l.address, l.city, l.region, l.country, t.name AS with_name, p.id AS package_id, p.name AS package_name, s.name AS skill_name
FROM 
appoinments a
LEFT JOIN talents t ON (t.id = a.talent_id)
LEFT JOIN bookings b ON (b.id = a.booking_id)
LEFT JOIN locations l ON (l.id = b.location_id)
LEFT JOIN packages p ON (p.id = a.package_id)
LEFT JOIN skills s ON (s.id = p.skill_id) 
WHERE a.agency_id = ".$this->data['id']." and a.begins >= '".$from."' and a.begins <= '".$to."' and isnull(a.deleted) and (isnull(a.expired) || a.expired > NOW()) order by a.begins,a.id");
		}

		$data = $this->db->query("SELECT * FROM appoinments WHERE agency_id = ".$this->data['id']." and begins >= '".$from."' and begins <= '".$to."' and isnull(deleted) and (isnull(expired) || expired > NOW()) order by begins,id", NULL);
		
		

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Appoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get monthly appointments, 
	*/
	public function getMontlyAppoinments ($year, $month){
		
		$dayone = strtotime($year."-".$month.'-01');
		$firstday = date('D',$dayone) == 'Sunday' ? $dayone : strtotime("last sunday", $dayone);
		$dayend = strtotime(date("Y-m-t", $dayone));
		$lastday = date('D',$dayend) == 'Saturday' ? $dayend : strtotime("next saturday", $dayend);
		
		$offset = Session::timeDiff();
		
		$appointmnets = $this->db->query("SELECT  DATE(ADDTIME(begins, '".$offset."')) AS dates,COUNT(id) AS total, SUM(duration) AS minutes, confirmed
FROM appoinments
WHERE agency_id = ".$this->data['id']." and DATE(begins) >= '".date('Y-m-d', $firstday)."' and DATE(begins) <= '".date('Y-m-d', $lastday)."' and isnull(deleted)
GROUP BY DATE(begins), confirmed");


		$data = array();
		if(!empty($appointmnets)){
			
			foreach ($appointmnets as $a){
				
				if(!isset($data[$a['dates']])){
					$data[$a['dates']] = array('confirmed' => 0, 'confirmed_time' => 0 ,'shadow' => 0,'shadow_time' => 0, 'minutes' => 0);
				}
				
				if(!empty($a['confirmed'])){
					$data[$a['dates']]['confirmed'] = $a['total'];
					$data[$a['dates']]['confirmed_time'] = $a['minutes'];
				}
				else{
					$data[$a['dates']]['shadow'] = $a['total'];
					$data[$a['dates']]['shadow_time'] = $a['minutes'];
				}
				
				$data[$a['dates']]['minutes'] += $a['minutes'];
				
			}
			
		}
		
	

		$out = array();
		$date = $firstday;
		
		while ($date <= $lastday){
			
			$dt = date('Y-m-d', $date);
			
			$out[$dt] = isset($data[$dt]) ? $data[$dt] : NULL;

	
			$date = strtotime('+1 day', $date);
		}
		
		return $out;
	}
	
	/**
	* Get talent appointments
	* filetre 1 : upcomming
	* filter 2 : previous
	* filter : 0 search by date
	*/
	public function getTalentAppointments ($talent_id, $filter = 1, $date = NULL){
		
		$order = "a.begins,a.id";
		
		if(empty($filter)){
			
			$from =  !empty($date) ? Util::ToSysDate($date, '00:00') : Util::ToSysDate(date("Y-m-d"), '00:00');			
			$to = date("Y-m-d H:i:s", strtotime('+1 day', strtotime($from)));			
			$condition = "a.begins >= '".$from."' and a.begins <= '".$to."'";			
		}
		elseif($filter == 2) {
			$from = Util::ToSysDate(date("Y-m-d"), '00:00');
			$condition = "a.begins <= '".$from."'";	
			$order = "a.begins desc,a.id";	
		}
		else{
			$from = Util::ToSysDate(date("Y-m-d"), '00:00');
			$condition = "a.begins >= '".$from."'";	
		}
		
		$tz = Session::timezone(true);
		//$tz = 0;
		
		$query = "SELECT a.*, b.talent_fee, b.venue, l.address, l.city, l.region, l.country, t.name AS with_name, p.id AS package_id, p.name AS package_name, s.name AS skill_name, date(ADDTIME(a.begins,'".$tz."')) as begin_date
FROM 
appoinments a
LEFT JOIN talents t ON (t.id = a.talent_id)
LEFT JOIN bookings b ON (b.id = a.booking_id)
LEFT JOIN locations l ON (l.id = b.location_id)
LEFT JOIN packages p ON (p.id = a.package_id)
LEFT JOIN skills s ON (s.id = p.skill_id) 
WHERE a.talent_id = ".$talent_id." and a.agency_id = ".$this->data['id']." and ".$condition." and isnull(a.deleted) and (isnull(a.expired) || a.expired > NOW()) order by ".$order;

		$buffer = new Buffer();
		$buffer->_datasql = $query;
		$buffer->_countsql = "select count(*) as total  FROM appoinments a where a.talent_id = ".$talent_id." and a.agency_id = ".$this->data['id']." and ".$condition." and isnull(a.deleted) and (isnull(a.expired) || a.expired > NOW())";
		$buffer->_component = "agency/appointments";
		$buffer->_pagesize = 50;
		$buffer->_page = 1; 
		return $buffer;
		
		
	}



	/**
	* Get associated bookings
	* buffer
	*/

	public function getBookings ($status = NULL, $date = NULL){

		$buffer = App::getBookings(NULL, $this->data['id'], NULL, $status, $date);
		$buffer->_component = "booking/bookings";
		return $buffer;
	}
	
	
	/**
	* Get bookings created for talents
	* buffer
	*/

	public function getTalentBookings ($talent_id , $status = NULL, $date = NULL){

		$buffer = App::getBookings($talent_id, $this->data['id'], NULL, $status, $date);
		$buffer->_component = "booking/bookings";
		$buffer->no_image = true;
		$buffer->cura = "agency_booking_view";
		return $buffer;
	}



	/**
	* Get associated events
	* Returns array of event objects or empty array 
	*/

	public function getEvents (){

		$data = $this->db->query("SELECT * FROM events WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by begins asc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new event();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get agency packages
	* return arry of package objects
	*/
	public function getPackages ($as_array = false) {
		
		if($as_array){
			
			return $this->db->query("SELECT t.id, t.skill_id, s.name as skill_name, t.name, t.description, t.timespan, t.interval, t.fee, t.fee_local, t.items , t.active, t.deleted 
			FROM talent_packages t 
			join skills s on (s.id = t.skill_id) 
			join subscriptions p on (p.agency_id = t.agency_id and p.skill_id = t.skill_id and p.active = 1 && p.begin_date <= NOW() and p.exp_date >= date(NOW()) and isnull(p.deleted)) WHERE t.agency_id = ".$this->data['id']." AND  t.active = 1 AND  isnull(t.deleted) order by t.fee");
			
		}
		
		$data = $this->db->query("select p.* 
		from talent_packages p
		join subscriptions s on (s.agency_id = p.agency_id and s.skill_id =  p.skill_id and s.begin_date < NOW() && s.exp_date >= DATE(NOW()) )
		where p.agency_id = ".$this->data['id']." and isnull(p.deleted) 
		order by name");
		
		$out = array();
		if(!empty($data)){
			
			foreach ($data as $item){
				$p = new Talentpackage();
				$p->populate($item);
				$out[] = $p;	
			}
			
		}
		
		return $out;
	}
	
	/**
	* Get total number of packages
	*/
	public function isNew () {
		
		$total = $this->db->query("SELECT COUNT(*) AS total, TIME_TO_SEC(TIMEDIFF( NOW(), MAX(created))) AS latest FROM talent_packages WHERE agency_id = ".$this->data['id'], NULL , 1);
		
		return $total['total'] < 2 && $total['latest'] < 3600;
	}
	
	/**
	* Get details regarding each packages
	* contain details about all packages
	*/
	public function getPackageDetail ($active_only = true) {
		
		$con = "p.agency_id = ".$this->data['id'];
		
		if($active_only){
			$con .= " and isnull(p.deleted)";	
		}
		
		$data = $this->db->query("SELECT p.id as package__id, p.active as package__active, p.agency_id as package__agency_id, p.skill_id as package__skill_id, p.name as package__name, p.description as package__description, p.items as package__items, p.timespan as package__timespan, p.interval as package__interval, p.location_required as package__location_required, p.fee as package__fee, p.advance_percentage as package__advance_percentage, p.is_private as package__is_private, p.fulday as package__fulday, p.created as package__created, p.modified as package__modified, p.deleted as package__deleted, a.currency as package__currency,
s.id as skill__id, s.parent_id as skill__parent_id, s.name as skill__name, s.genre as skill__genre, s.gender_required as skill__gender_required, s.is_default as skill__is_default, s.created as skill__created, s.modified as skill__modified, s.deleted as skill__deleted
FROM talent_packages as p 
JOIN agencies a on (a.id = p.agency_id)
JOIN skills s ON ( s.id = p.skill_id)
JOIN subscriptions u on (u.agency_id = p.agency_id and u.skill_id = p.skill_id and u.begin_date < NOW() && u.exp_date >= DATE(NOW()) )
WHERE ".$con." order by fee");



		return Util::groupResult($data, true);
		
	}
	
	/**
	* Get skills 
	* retun skill object
	*/
	public function getSkills (){
		
		$data = $this->db->query("SELECT s.* FROM   subscriptions  t join skills s on (s.id = t.skill_id) WHERE t.agency_id = ".$this->data['id']." and isnull(t.deleted) and t.active = 1 order by t.id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Skill();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
		
		
	}

	/**
	* Get current;y active subscription
	*/
	public function getActiveSubscription () {
		
		$data = $this->db->query("SELECT * FROM subscriptions WHERE agency_id = ".$this->data['id']." and isnull(deleted) and begin_date <= date(now()) and  exp_date >= date(now()) order by id desc", NULL, 1);
		
		if(!empty($data)){
			$obj = new subscription();
			$obj->populate($data);
			return $obj;
		}
		
		return false;
	}


	/**
	* Get associated subscriptions
	* Returns array of subscription objects or empty array 
	*/

	public function getsubscriptions (){

		$data = $this->db->query("SELECT * FROM subscriptions WHERE agency_id = ".$this->data['id']." and isnull(deleted) and exp_date >= date(now()) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	public function getSubscriptionDetail () {
		
		$data = $this->db->query("SELECT s.id AS subscription__id, s.agency_id AS subscription__agency_id, s.skill_id AS subscription__skill_id, s.package_id AS subscription__package_id, s.payment_id AS subscription__payment_id, s.reneval_id AS subscription__reneval_id, s.term AS subscription__term, s.sub_date AS subscription__sub_date, s.begin_date AS subscription__begin_date, s.exp_date AS subscription__exp_date, s.active AS subscription__active, s.jobs as subscription__jobs, s.manage as subscription__manage, s.fees as subscription__fees, s.created AS subscription__created, s.modified AS subscription__modified, s.deleted AS subscription__deleted,
p.id AS package__id, p.skill_id AS package__skill_id, p.name AS package__name, p.description AS package__description, p.price_monthly AS package__price_monthly, p.price_annualy AS package__price_annualy, p.jobs AS package__jobs, p.begin_date AS package__begin_date, p.end_date AS package__end_date, p.active AS package__active, p.display_order AS package__display_order, p.selected AS package__selected, p.created AS package__created, p.modified AS package__modified, p.deleted AS package__deleted,
k.id AS skill__id, k.parent_id AS skill__parent_id, k.name AS skill__name, k.genre AS skill__genre, k.gender_required AS skill__gender_required, k.is_default AS skill__is_default, k.created AS skill__created, k.modified AS skill__modified, k.deleted AS skill__deleted,
r.id AS reneval__id, r.agency_id AS reneval__agency_id, r.package_id AS reneval__package_id, r.term AS reneval__term, r.method_id AS reneval__method_id, r.next_reneval AS reneval__next_reneval, r.active AS reneval__active, r.created AS reneval__created, r.modified AS reneval__modified, r.deleted AS reneval__deleted,
rm.id AS reneval_method__id, rm.customer_id AS reneval_method__customer_id, rm.type AS reneval_method__type, rm.name AS reneval_method__name, rm.card_name AS reneval_method__card_name, rm.card_number AS reneval_method__card_number, rm.card_exp AS reneval_method__card_exp, rm.ccvv AS reneval_method__ccvv, rm.token AS reneval_method__token, rm.reference AS reneval_method__reference, rm.created AS reneval_method__created, rm.modified AS reneval_method__modified, rm.deleted AS reneval_method__deleted,
py.id AS payment__id, py.invoice_id AS payment__invoice_id, py.subscription_id AS payment__subscription_id, py.method AS payment__method, py.amount AS payment__amount, py.currency AS payment__currency, py.card_name AS payment__card_name, py.card_type AS payment__card_type, py.card_num AS payment__card_num, py.card_exp AS payment__card_exp, py.card_cvv AS payment__card_cvv, py.payment_id AS payment__payment_id, py.transaction_id AS payment__transaction_id, py.bank_id AS payment__bank_id, py.ip_address AS payment__ip_address, py.status AS payment__status, py.success AS payment__success, py.released AS payment__released, py.created AS payment__created, py.modified AS payment__modified, py.deleted AS payment__deleted
FROM subscriptions s 
JOIN packages p ON ( p.id = s.package_id)
JOIN skills k ON ( k.id = p.skill_id)
LEFT JOIN payments py ON ( py.id = s.payment_id)
LEFT JOIN auto_renevals r ON ( r.id = s.reneval_id)
LEFT JOIN payment_methods rm ON (rm.id = r.method_id)
WHERE s.agency_id = ".$this->data['id']." and s.exp_date >= date(NOW()) and isnull(s.deleted) and s.active = 1");


		
		$subs = array();
		
		foreach ($data as $item) {
			
			$out = array();
			
			foreach ($item as $k => $v){
				
				list($t, $f) = explode('__',$k);
				if(!isset($out[$t])){
					$out[$t] = array();
				}
				
				$out[$t][$f] = $v;	
			}
			
			$subs[] = $out;
		}
		
		
		
		
		return $subs;
			
	}
	
	/**
	* Check if talent has a paid subscription
	*/
	public function hasPaidSubscription () {
		
		$data = $this->db->query("SELECT COUNT(*) AS total FROM subscriptions WHERE fees > 0 AND !ISNULL(payment_id) AND agency_id = ".$this->data['id']." AND ISNULL(deleted) and exp_date > date(NOW())", NULL, 1);
		
		return $data['total'] > 0;
		
	}
	
	
	/**
	* Get talent's time line for given time
	*/
	public function getTimeline ($datetime){
		
		$timeline = new Timeline($datetime, $this->data['timeoffset']);
			
		$range = $timeline->getRange();
					
		$appointments = $this->getAppoinments($range[0],$range[1], true);
		
		if(!empty($appointments)){
			
			foreach ($appointments as $a) {
				
				if(empty($a['confirmed'])){
					continue;
				}
				
				
				if(empty($a['all_day'])){
					$timeline->addToLine($a['begins'], $a['duration']+$a['rest_time'] );	
				}
				else {
					$timeline->addFullDay($a['begins'], $this->data['timeoffset']);
					
				}
				
				
			}
		}
		
		
		return $timeline;
		
	}



	/**
	* Get associated board_participants
	* Returns array of board_participant objects or empty array 
	*/

	public function getBoardParticipants (){

		$data = $this->db->query("SELECT * FROM board_participants WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new boardParticipant();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated likes
	* Returns array of like objects or empty array 
	*/

	public function getLikes (){

		$data = $this->db->query("SELECT * FROM likes WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new like();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated agenda_shares
	* Returns array of agenda_share objects or empty array 
	*/

	public function getAgendaShares (){

		$data = $this->db->query("SELECT * FROM agenda_shares WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agendaShare();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}

?>