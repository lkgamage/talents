<?php

/**********************************************
* AttemptTalent Class
**********************************************/

class AttemptTalent extends Model{

	public $table = 'attempt_talents';

	public $fields = array(
		'id',
		'attempt_id',
		'profile_id',
		'begin_time',
		'package_id',
		'booking_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated booking_attempt
	* Returns booking_attempt object or NULL 
	*/

	public function getBookingAttempt (){

		$data = $this->db->query("SELECT * FROM booking_attempts WHERE id = ".$this->data['attempt_id'], NULL, 1);

		if(!empty($data)){
			$obj = new bookingAttempt();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	/*
	* Duplicate this object 
	*/
	public function duplicate ( ) {
		
		$this->_status = 0;
		unset($this->data['id']);
		unset($this->data['talent_id']);
		unset($this->data['agency_id']);
		unset($this->data['created']);
		unset($this->data['modified']);
		unset($this->data['deleted']);
		
		return $this->create();
		
	}


}
?>
