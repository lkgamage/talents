<?php

/**********************************************
* TalentSchedule Class
**********************************************/

class TalentSchedule extends Model{

	public $table = 'talent_schedules';

	public $fields = array(
		'id',
		'profile_id',
		'sun_avl',
		'sun_start',
		'sun_ends',
		'mon_avl',
		'mon_start',
		'mon_ends',
		'tue_avl',
		'tue_start',
		'tue_ends',
		'wed_avl',
		'wed_start',
		'wed_ends',
		'thu_avl',
		'thu_start',
		'thu_ends',
		'fri_avl',
		'fri_start',
		'fri_ends',
		'sat_avl',
		'sat_start',
		'sat_ends',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
