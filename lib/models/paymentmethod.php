<?php
		
/****************************
* Paymentmethod Class
*****************************/

class Paymentmethod extends Model{

	public $table = 'payment_methods';

	public $fields = array(
		'id',
		'customer_id',
		'method_id',
		'type',
		'card_number',
		'exp_month',
		'exp_year',
		'funding',
		'created',
		'modified',
		'deleted'
		);
	


	/**
	* Check whether if card expired
	*/
	public function isExpired () {
	
		if(!empty($this->data['card_exp'])){
			
			list($month, $year) = explode('/', $this->data['card_exp']);
			
			return !($year > date('y') || ($year == date('y') && $month >= date('m')));
			
		}
		
		return false;
		
	}


	/**
	* Get associated subscription_payments
	* Returns array of subscription_payment objects or empty array 
	*/

	public function getSubscriptionPayments (){

		$data = $this->db->query("SELECT * FROM subscription_payments WHERE method_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscriptionPayment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated auto_renevals
	* Returns array of auto_reneval objects or empty array 
	*/

	public function getAutoRenevals (){

		$data = $this->db->query("SELECT * FROM auto_renevals WHERE method_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new autoReneval();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated payments
	* Returns array of payment objects or empty array 
	*/

	public function getPayments (){

		$data = $this->db->query("SELECT * FROM payments WHERE method_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}

?>