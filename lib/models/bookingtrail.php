<?php

/**********************************************
* BookingTrail Class
**********************************************/

class BookingTrail extends Model{

	public $table = 'booking_trails';

	public $fields = array(
		'id',
		'booking_id',
		'customer_id',
		'profile_id',
		'description',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agancy_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
