<?php
		
/**
* Booking Class
* 
* session_start, session_ends and suggested_date
* are in Talent's time zone
* timeoffset is talent's time offset
**/

class Booking extends Model{

	public $table = 'bookings';

	public $fields = array(
		'id',
		'original_id',
		'customer_id',
		'event_id',
		'talent_id',
		'agency_id',
		'location_id',
		'name',
		'charitable',
		'timeoffset',
		'package_id',
		'session_start',
		'session_ends',
		'status',
		'status_changed',
		'talent_fee',
		'other_exp',
		'charges',
		'tax',
		'discount',
		'total',
		'advance',
		'currency',
		'additional_people',
		'due_date',
		'customer_note',
		'talent_note',
		'responded_date',
		'confirmed_date',
		'subscription_id',
		'free_quota',
		'created',
		'modified',
		'deleted'
		);
	

		
	public function create () {
		
		$this->_calculate();		
		parent::create();
		
		$this->addTrail( DataType::$EVENT_BOOKING_CREATE);
	}
	
	public function update () {
		$this->_calculate();		
		parent::update();
	
	}
	
	public function __set ($name, $value) {
		
		if($name == 'status'){
			// there is another method to do so
			$this->setStatus($value);
			return;
		}
		
		if($this->_status != 0){
			
			if($name == 'suggested_package'){
				$this->addTrail(DataType::$EVENT_TALENT_PKG_CHANGE, $this->data['suggested_package'].','.$value);	
			}
			
			if($name == 'discount'){
				$this->addTrail(DataType::$EVENT_TALENT_ADD_DISCOUNT, $value);	
			}
			
			if($name == 'venue'){
				$this->addTrail( DataType::$EVENT_CHANGE_PLACE, $value);
			}
		}
					
		
		parent::__set ($name, $value);
	}
	
	/**
	* Check if current profile has permission for this booking
	*/
	public function hasPermission (){
		
		$profile = Session::getProfile();
		
		if(empty($profile)){
			return false;	
		}
		
		if($profile['type'] == 't' && $profile['id'] == $this->data['talent_id']){
			return true;	
		}
		
		if($profile['type'] == 'a' && $profile['id'] == $this->data['agency_id']){
			return true;	
		}
		
		return false;
	}
	
	/** Supportive functions to understand booking sender and reciver **/
	
	/**
	* Currently viewing as a sender
	*/
	public function isSender () {
		
		$profile = Session::getProfile();
		$customer = Session::getCustomer();
		
		if(empty($profile)){
			return ($this->data['customer_id'] == $customer['id'] &&  (empty($this->data['talent_id']) || empty($this->data['agency_id'])));
		}
		else{
			
			return ($profile['type'] == 'a' && !empty($this->data['talent_id']) && $this->data['agency_id'] == $profile['id']);
		}
		
		return false;
	}
	
	/**
	* Currently viewing as reciver
	*/
	public function isRecipient() {
		
		$profile = Session::getProfile();

		if(!empty($profile)){
			
			if($profile['type'] == 't' && !empty($this->data['talent_id']) && $this->data['talent_id'] == $profile['id']){
				return true;
			}
			
			if($profile['type'] == 'a' && empty($this->data['talent_id']) && $this->data['agency_id'] == $profile['id']){
				return true;	
			}
		}
		
		return false;	
	}
	
	/**
	* Check if sender is a customer
	*/
	public function isCustomerCreated () {
		
		return empty($this->data['agency_id']);
	}
	
	
	/**
	* Check if agency created the booking
	*/
	public function isAgencyCreated () {
		return (!empty($this->data['agency_id']));
	}
	
	/**
	* Check if Recipient is a talent
	*/
	public function isTalentRecipient () {
		return !empty($this->data['talent_id']);
	}
	
	/**
	* Check if recipent is an agency
	*/
	public function isAgencyRecipient () {
		return (empty($this->data['talent_id']) && !empty($this->data['agency_id']));
	}
	
	/**
	* Get sender name
	*/
	public function getSenderName ($info){
				
		if(!empty($this->data['agency_id']) && !empty($this->data['talent_id'])){
			return $info['agency']['name'];
		}
		
		return $info['customer']['firstname'].' '.$info['customer']['lastname'];
	}
	
	/**
	* Get recipient name
	*/
	public function getRecipientName ($info){
		
		if(!empty($this->data['agency_id']) && empty($this->data['talent_id'])){
			return $info['agency']['name'];
		}
		
		echo $info['talent']['name'];
	}
	
	/**
	* Check if there are messages for this booking
	*/
	public function hasMessages () {
		
		$data = $this->db->query("SELECT COUNT(*) AS total
FROM message_recipients r
JOIN messages m ON ( m.id = r.message_id )
JOIN message_boards b ON (b.id = m.board_id) 
WHERE b.booking_id = ".$this->data['id'], NULL, 1);
		
		return !empty($data['total']);
			
	}
	
	
	
	
	/**
	* Calculate total amount and due date
	*/
	private function _calculate (){
		
		if(empty($this->data['id'])){
			return;	
		}
		
		$sum = $this->db->query('SELECT SUM(amount) AS total FROM expences WHERE booking_id = '.$this->data['id'].' and isnull(deleted)', NULL, 1);
		
		if(!empty($sum['total']) && (float)$this->data['other_exp'] !=  (float)$sum['total']){
			$this->addTrail(DataType::$EVENT_TALENT_ADD_CHARGES, $sum['total']);
		}
		
		$this->data['other_exp'] = !empty($sum['total']) ? $sum['total'] : 0;
		
		// total booking fee
		$total = (!empty($this->data['talent_fee'])) ? $this->data['talent_fee'] : 0;
		$total +=  (!empty($this->data['agency_fee'])) ? $this->data['agency_fee'] : 0;
		$total +=  (!empty($this->data['other_exp'])) ? $this->data['other_exp'] : 0;
	//	$total +=  (!empty($this->data['charges'])) ? $this->data['charges'] : 0;
		$total -=  (!empty($this->data['discount'])) ? $this->data['discount'] : 0;
		
		
		$this->data['total'] = $total;
		
		// round to nearest .25
		/*
		$advance = (!empty($this->data['talent_fee'])) ? ceil((($this->data['talent_fee']*$this->data['advance_percentage'])/100)*4)/4 : 0;
		$advance +=  (!empty($this->data['agency_fee'])) ? $this->data['agency_fee'] : 0;
		$advance +=  (!empty($this->data['other_exp'])) ? $this->data['other_exp'] : 0;
		$advance +=  (!empty($this->data['charges'])) ? $this->data['charges'] : 0;
		$advance -=  (!empty($this->data['discount'])) ? $this->data['discount'] : 0;
		
		
		$this->data['advance'] = $advance;
		*/
	}
	
	/**
	* Set Status 
	* Automatically invoke when you assign new status
	*/
	public function setStatus ($status){
		
		$this->data['status'] = $status;
		$this->data['status_changed'] = date("Y-m-d H:i:00");
		$this->setDueDate();
		$this->save();
		
		if($status == DataType::$BOOKING_ACCEPTED){
			// talent accepted booking
			$this->addTrail(DataType::$EVENT_TALENT_ACCEPT_BOOKING);
			
			
			
			// appoinmnet description
			$info = $this->getinfo();
			$desc = Util::makeString($info['event']['name'], $info['location']['display']);

			// adding shadow appoinment for the talent
			$appoinment = $this->getAppoinment();
		}
		
		if($status == DataType::$BOOKING_ACCEPTED || $status ==  DataType::$BOOKING_REJECT){
			$this->data['responded_date'] = 'NOW';
		}
		elseif($status == DataType::$BOOKING_CONFIRM || $status == DataType::$BOOKING_CANCEL){
			$this->data['confirmed_date'] = 'NOW';	
		}
		
	/*	elseif($status == DataType::$BOOKING_PAYMENT){
			// customer confirmed and get the invoice
			if(!empty($this->data['suggested_package'])){
				$this->data['package_id'] = $this->data['suggested_package'];
				$this->save();
				
				$this->addTrail(DataType::$EVENT_CUSTOMER_PACKAGE_ACCEPT);
			}
			
			if($this->data['other_exp'] > 0){
				$this->addTrail(DataType::$EVENT_CUSTOMER_CHARGES_ACCEPT, $this->data['other_exp']);
			}
			
			$this->addTrail(DataType::$EVENT_CUSTOMER_CONFIRM_BOOKING);
			
			// renew expire date
			$appoinment = $this->getTalentAppoinment();
			
			if(empty($appoinment)) {
				$appoinment->expired = $this->data['due_date'];
				$appoinment->save();
			}
			
				
						
		}*/
		if($status == DataType::$BOOKING_CONFIRM){
			
			// customer pay advance fee
			//$invoice = $this->getInvoice();
			
			//$invoice->status = DataType::$INVOICE_PAID;
			//$invoice->save();
			
			// confirm shadow booking
			$appoinmnet = $this->getAppoinment();
			$appoinmnet->setNull('expired');
			$appoinmnet->confirmed = 1;
			$appoinmnet->save();
			
			//$this->addTrail(DataType::$EVENT_ADVANCE_PAYMENT_RECEIVED);			
			$this->addTrail(DataType::$EVENT_BOOKING_CONFIRMED);
			$this->addTrail(DataType::$EVENT_TALENT_APPOINMENT_CREATE);
				
		}
		elseif($status == DataType::$BOOKING_CANCEL){
			
			$appoinmnet = $this->getAppoinment();
			
			if(!empty($appoinmnet)){
				$appoinmnet->delete();
			}
			
		}
		
		
	}
	
	
	/**
	* get/ Create invoice for this booking
	*/
	public function getInvoice () {
		/*
		$data = $this->db->query("select * from invoices where booking_id = ".$this->data['id']." and isnull(deleted)", NULL, 1);

		if(!empty($data)){
			$invoice = new Invoice();
			$invoice->populate($data);
			return $invoice;
		}
		
		$this->_calculate();
		
		// create new invoice
		$invoice = new Invoice();
		$invoice->booking_id = $this->data['id'];
		$invoice->talent_fee = $this->data['talent_fee'];
		$invoice->agency_fee = $this->data['agency_fee'];
		$invoice->expences = $this->data['other_exp'];
		$invoice->discount = $this->data['discount'];
		$invoice->charges = 0; //$this->data['charges'];
		$invoice->amount = $this->data['total']; // $this->data['advance'];
		$invoice->balance = 0; // $this->data['total'] - $this->data['advance'];
		$invoice->currency = $this->data['currency'];
		$invoice->status = DataType::$INVOICE_PENDING;
		//$invoice->expires = $this->data['due_date'];
		$invoice->tax = 0;
		$invoice->save();
		
		$this->addTrail(DataType::$EVENT_INVOICE_SENT);
		
		return $invoice;
		*/
		return NULL;
	}
	
	
	/**
	* Confirm a booking
	* Check paymnet object and confirm booking
	*/
	public function confirm ($payment) {
		
		$invoice = $this->getInvoice();
		
		if($payment->invoice_id == $invoice->id){
			
			$invoice->status = DataType::$INVOICE_PAID;
			$invoice->save();
			
			$this->setStatus(DataType::$BOOKING_CONFIRM);
			
		}
		
		return false;
		
	}
	
	
	/**
	* Set due date based on status
	*/
	public function setDueDate () {
	
		// calculate due date
		$delta = (strtotime($this->data['session_start']) - time())/3600;
		
		if($delta >= 2){
			
			$modified = strtotime($this->data['status_changed']);
			
			if( $this->data['status'] == DataType::$BOOKING_PENDING || 
				$this->data['status'] == DataType::$BOOKING_PAYMENT ||
				$this->data['status'] == DataType::$BOOKING_ACCEPTED ){
				
				/*if($delta > 720){
					// 30 days
					$due = strtotime('+3 days', $modified);
				}
				elseif ($delta > 168){
					// 7 days
					$due = strtotime('+2 days', $modified);	
				}*/
				if ($delta >= 336){
					// 3 days
					$due = strtotime('+3 days', $modified);	
				}
				elseif ($delta > 168){
					// 7 days
					$due = strtotime('+2 days', $modified);	
				}
				elseif ($delta > 72){
					// 3 days
					$due = strtotime('+1 days', $modified);	
				}
				elseif ($delta > 24){
					// 1 day
					$due = strtotime('+6 hours', $modified);	
				}
				elseif ($delta >= 6){
					// 6 hours
					$due = strtotime('+2 hours', $modified);	
				}
				else{
					// less than 6 hour
					$due = strtotime('+1 hours', $modified);	
				}
				
				$this->data['due_date'] = date('Y-m-d H:i:00', $due);
			}
			else{
				$this->data['due_date'] = 	$this->data['session_start'];
			}
			
			
			
		} // > 0
		
	}


	/**
	* Get associated expences
	* Returns array of booking_expence objects or empty array 
	*/

	public function getBookingExpences (){

		$data = $this->db->query("SELECT * FROM expences WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingExpence();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	public function getExpences () {
		return $this->getBookingExpences();
	}




	/**
	* Get associated complains
	* Returns array of complain objects or empty array 
	*/

	public function getComplains (){

		$data = $this->db->query("SELECT * FROM complains WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new complain();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated event_talents
	* Returns array of event_talent objects or empty array 
	*/

	public function getEventTalents (){

		$data = $this->db->query("SELECT * FROM event_talents WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new eventTalent();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated ratings
	* Returns array of rating objects or empty array 
	*/

	public function getRatings (){

		$data = $this->db->query("SELECT * FROM ratings WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new rating();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	



	/**
	* Get associated appoinments
	* Returns talent_appoinment object or NULL
	*/

	public function getTalentAppoinment (){

		$data = $this->db->query("SELECT * FROM appoinments WHERE booking_id = ".$this->data['id']." and isnull(deleted) ", NULL, 1);

		if(!empty($data)){
			
			$obj = new talentAppoinment();
			$obj->populate($data);
			return $obj;
			
		}

		return NULL;
	}


	/**
	* Get each type of contacts
	* return customer id, primary email and phone 	
	* If exclude (int) is passed, return only one record (opposite party) 
 	*/
	public function getContacts ($exclude = NULL) {

		
		$data = $this->db->query("SELECT c.id AS customer__id, ce.id AS customer__email_id, ce.email AS customer__email,
cp.id AS customer__phone_id, cp.phone AS customer__phone, cp.country_code AS customer__country_code, cp.whatsapp AS customer__whatsapp, c.firstname AS customer__name,
b.talent_id AS talent__id, t.customer_id AS talent__customer_id, te.id AS talent__email_id, te.email AS talent__email, t.name AS talent__name,
tp.id AS talent__phone_id, tp.phone AS talent__phone, tp.country_code AS talent__country_code, tp.whatsapp AS talent__whatsapp, a.name AS agency__name,
b.agency_id AS agency__id, a.customer_id AS agency__customer_id
FROM bookings b 
JOIN customers c ON (c.id = b.customer_id)
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
LEFT JOIN emails ce ON (ce.customer_id = c.id AND ce.is_primary = 1 AND ISNULL(ce.deleted))
LEFT JOIN phones cp ON (cp.customer_id = c.id AND cp.is_primary = 1 AND ISNULL(cp.deleted))
LEFT JOIN emails te ON (te.customer_id = t.customer_id AND te.is_primary = 1 AND ISNULL(te.deleted))
LEFT JOIN phones tp ON (tp.customer_id = t.customer_id AND tp.is_primary = 1 AND ISNULL(tp.deleted))
WHERE b.id =".$this->data['id'], NULL, 1);

		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		$ids = Util::makeArray($out['customer']['id'],$out['talent']['customer_id'] );
		$ids = array_unique($ids);
		
		if(!empty($exclude)){
			$ids = array_diff($ids, array($exclude));
		}
		
		$out['customer']['devices'] = array();
		$out['talent']['devices'] =	 array();
		
		if(!empty($ids)){		
				
			$devices = $this->db->query("SELECT customer_id, fingureprint, push_id FROM customer_devices WHERE customer_id in (".implode(',', $ids).") AND !ISNULL(push_id) AND isnull(deleted)");

			if(!empty($devices)){
				$devices = Util::groupMultiArray($devices,'customer_id');	

				if(isset($devices[$out['customer']['id']])){
					$out['customer']['devices'] = $devices[$out['customer']['id']];
				}
				else{
					$out['customer']['devices'] = array();
				}

				if(isset($devices[$out['talent']['customer_id']])){
					$out['talent']['devices'] = $devices[$out['talent']['customer_id']];
				}
				else{
					$out['talent']['devices'] =	 array();	
				}

				if(isset($devices[$out['agency']['customer_id']])){
					$out['agency']['devices'] = $devices[$out['agency']['customer_id']];
				}
				else{
					$out['agency']['devices'] = array();
				}
				//print_r($devices);
			}
		}
		
		if(!empty($exclude)){
			
			foreach ($out as $a => $contact){
				
				if($a == 'customer' && $contact['id'] != $exclude){
					return $contact;	
				}
				elseif(!empty($contact['customer_id']) && $contact['customer_id'] != $exclude){
					return $contact;	
				}
				
			}
			
			return NULL;	
		}
		
		
		
		return $out;
		
	}


	/**
	* Get associated otus
	* Returns array of otus objects or empty array 
	*/

	public function getOtus (){

		$data = $this->db->query("SELECT * FROM otus WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new otus();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated location
	* Returns location object or NULL 
	*/

	public function getLocation (){

		$data = $this->db->query("SELECT * FROM locations WHERE id = ".$this->data['location_id'], NULL, 1);

		if(!empty($data)){
			$obj = new location();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated talent_package
	* Returns talent_package object or NULL 
	*/

	public function getTalentPackage (){

		$data = $this->db->query("SELECT * FROM talent_packages WHERE id = ".$this->data['package_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talentPackage();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['original_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	/**
	* Get message board for this booking
	* if there is no messageboard, create it
	*/
	public function getMessageBoard () {
		
		$data = $this->db->query("select * from message_boards where booking_id =".$this->data['id'], NULL, 1);
		
		$board = new MessageBoard();
		
		if(!empty($data)){
			$board->populate($data);
		}
		else{
			$customer = Session::getCustomer();
			
			$board->booking_id = $this->data['id'];
			$board->created_by = $customer['id'];
			$board->save();
			
			$partners = $this->getPartners();
			
			if(!empty($partners)){
							
				foreach($partners as $p){
					$board->addParticipant($p['customer_id'], $p['talent_id'], $p['agency_id']);	
				}							
			}
		}
		
		return $board;
		
		
	}



	/**
	* Get associated messages
	* Returns array of message objects or empty array 
	*/

	public function getMessages (){		
	
		$customer = Session::getCustomer();

		$sql = "SELECT b.id AS board_id, b.event_id,  b.booking_id,  p.profile_id, t.name AS talent_name, t.gender AS talent_gender, ti.thumb AS talent_image, m.id AS message_id, s.gender AS customer_gender, t.gender AS talent_gender, m.originated, m.created, s.id AS customer_id,
CONCAT(s.firstname,' ',s.lastname) AS customer_name, si.thumb AS customer_image, m.message, m.deleted
FROM message_recipients r
JOIN messages m ON ( m.id = r.message_id )
JOIN message_boards b ON (b.id = m.board_id) 
JOIN board_participants p ON (p.board_id = m.board_id AND p.customer_id = m.customer_id)
JOIN customers s ON (s.id = p.customer_id)
LEFT JOIN profiles t ON (t.id = p.profile_id)
LEFT JOIN images ti ON (ti.id = t.image_id)
LEFT JOIN images si ON (si.id = s.image_id)
WHERE b.booking_id = ".$this->data['id']." and  r.customer_id = ".$customer['id']." order by m.created desc";
		
		$count = "SELECT COUNT(*) AS total
FROM message_recipients r
JOIN messages m ON ( m.id = r.message_id )
JOIN message_boards b ON (b.id = m.board_id) 
WHERE b.booking_id = ".$this->data['id']." AND r.customer_id = ".$customer['id']."";

		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = $count;
		$buffer->_component = "messages";
		$buffer->_pagesize = 10;
		
		return $buffer;

	}


	/**
	* Get all booking related details at once
	*/
	public function getinfo() {
		
		$sql = "SELECT c.id AS customer__id,
 c.id AS customer__customer_id,
 c.firstname AS customer__firstname,
 c.lastname AS customer__lastname,
 c.firstname AS customer__name,
 c.dob AS customer__dob,
 c.location_id AS customer__location_id,
 c.timeoffset AS customer__timeoffset,
 c.timezone AS customer__timezone,
 c.currency AS customer__currency,
 c.image_id AS customer__image_id,
 c.active AS customer__active,
 c.gender AS customer__gender,
 c.is_trans AS customer__is_trans,
 c.created AS customer__created,
 c.modified AS customer__modified,
 c.deleted AS customer__deleted,
 b.id AS booking__id,
 b.name AS booking__name,
 b.original_id AS booking__original_id,
 b.customer_id AS booking__customer_id,
 b.event_id AS booking__event_id,
 b.talent_id AS booking__talent_id,
 b.agency_id AS booking__agency_id,
 b.location_id AS booking__location_id,
 l.venue AS location__venue,
 l.display AS location__display,
 b.charitable AS booking__charitable,
 b.timeoffset AS booking__timeoffset,
 b.package_id AS booking__package_id,
 b.session_start AS booking__session_start,
 b.session_ends AS booking__session_ends,
 b.status AS booking__status,
 b.status_changed AS booking__status_changed,
 b.talent_fee AS booking__talent_fee,
 b.other_exp AS booking__other_exp,
 b.charges AS booking__charges,
 b.discount AS booking__discount,
 b.total AS booking__total,
 b.advance AS booking__advance,
 b.currency AS booking__currency,
 b.additional_people AS booking__additional_people,
 b.due_date AS booking__due_date,
 b.created AS booking__created,
 b.customer_note AS booking__customer_note,
 b.talent_note AS booking__talent_note,
 b.modified AS booking__modified,
 b.deleted AS booking__deleted,
 l.id AS location__id,
 l.address AS location__address,
 l.city AS location__city,
 l.region AS location__region,
 l.postalcode AS location__postalcode,
 l.country AS location__country,
 l.locked AS location__locked,
 l.created AS location__created,
 l.modified AS location__modified,
 l.deleted AS location__deleted,
 p.id AS package__id,
 p.active AS package__active,
 p.name AS package__name,
 p.description AS package__description,
 p.items AS package__items,
 p.timespan AS package__timespan,
 p.interval AS package__interval,
 p.location_required AS package__location_required,
 p.fee AS package__fee,
 p.advance_percentage AS package__advance_percentage,
 p.is_private AS package__is_private,
 p.created AS package__created,
 p.modified AS package__modified,
 p.deleted AS package__deleted, 
s.name AS talent__skill_name,
s.genre AS talent__skill_genre,
e.id AS event__id,
 e.customer_id AS event__customer_id,
 e.event_type AS event__event_type,
 e.name AS event__name,
 e.description AS event__description,
 e.begins AS event__begins,
 e.ends AS event__ends,
 e.charitable AS event__charitable,
  e.location_id AS event__location_id,
 e.created AS event__created,
 e.modified AS event__modified,
 e.deleted AS event__deleted,
t.id AS talent__id,
 t.customer_id AS talent__customer_id,
  t.verified AS talent__verified,
 t.name AS talent__name,
 t.location_id AS talent__location_id,
 t.timeoffset AS talent__timeoffset,
 t.currency AS talent__currency,
 t.gender AS talent__gender,
 t.is_agency AS talent__is_agency,
t.agency_type AS talent__agency_type,
 t.limited_hours AS talent__limited_hours,
 t.active AS talent__active,
 t.created AS talent__created,
 t.modified AS talent__modified,
 t.deleted AS talent__deleted,
 ti.id AS talent__image_id,
 ti.i600 AS talent__i600,
 ti.i400 AS talent__i400,
 ti.thumb AS talent__thumb,
 ti.url AS talent__url,
 ci.id AS customer__image_id,
 ci.i600 AS customer__i600,
 ci.i400 AS customer__i400,
 ci.thumb AS customer__thumb,
 ci.url AS customer__url,
 et.name AS event__type_name,
 et.corporate AS event__corporate,
 tl.city AS talent__city,
 tl.region AS talent__region,
 tl.country AS talent__country,
ta.id AS appoinment__id,
  ta.description AS appoinment__description,
  ta.all_day AS appoinment__all_day,
  ta.time_off AS appoinment__time_off,
  ta.begins AS appoinment__begins,
  ta.duration AS appoinment__duration,
  ta.rest_time AS appoinment__rest_time,
  ta.confirmed AS appoinment__confirmed,
  ta.expired AS appoinment__expired,
  ta.created AS appoinment__created,
  ta.modified AS appoinment__modified,
  ta.deleted AS appoinment__deleted,
 a.id AS agency__id,
 a.name AS  agency__name,
 a.timeoffset AS agency__timeoffset,
 a.currency AS agency__currency,
 a.agency_type AS  agency__type_id,
 a.agency_type AS agency__type,
 a.verified AS  agency__verified,
 a.active AS  agency__active,
 a.customer_id AS agency__customer_id,
 al.city AS  agency__city,
 al.region AS  agency__region,
 al.country AS  agency__country,
  ai.id AS agency__image_id,
 ai.thumb AS  agency__thumb,
 ai.i400 AS  agency__i400,
 ai.url AS  agency__url,
 ap.handle AS  agency__handle,
 tp.handle AS talent__handle,
 cl.city AS customer__city,
 cl.region AS customer__region,
 cl.country AS customer__country,
 TIME_TO_SEC(TIMEDIFF(b.due_date, NOW())) AS booking__due,
 IF(TIME_TO_SEC(TIMEDIFF(b.session_start, NOW())) > 0, 0, 1) AS booking__past
FROM bookings b
JOIN customers c ON (c.id = b.customer_id)
JOIN locations cl ON (cl.id = c.location_id)
JOIN talent_packages p ON (p.id =  b.package_id)
JOIN categories pc ON (pc.id = p.category_id)
JOIN skills s ON (s.id = pc.skill_id)
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN locations l ON (l.id = b.location_id)
LEFT JOIN locations tl ON (tl.id = t.location_id)
LEFT JOIN pages tp ON (tp.profile_id = t.id)
LEFT JOIN `events` e ON (e.id = b.event_id)
LEFT JOIN images ti ON (ti.id = t.image_id)
LEFT JOIN images ci ON (ci.id = c.image_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
LEFT JOIN locations al ON (al.id = a.location_id)
LEFT JOIN images ai ON (ai.id = a.image_id)
LEFT JOIN pages ap ON (ap.profile_id = a.id)
LEFT JOIN appoinments ta ON (ta.booking_id = b.id)
LEFT JOIN event_types et ON (et.id = e.event_type)
WHERE b.id =".$this->data['id'];

		$data = $this->db->query($sql, NULL, 1);
		
		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		// get expences
		$out['expences'] = $this->db->query("select * from expences where booking_id = ".$this->data['id']." and isnull(deleted)");
		
		return $out;
	}


	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){		
		
		return $this->getProfile();
	}
	
	
	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){
		
		if(empty($this->data['agency_id'])){
			return false;	
		}

		$data = $this->db->query("SELECT * FROM profile WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new Profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


	/**
	* Create record for property changes
	*/
	private function addTrail ($event, $data = '') {

		$profile = Session::getProfile();
		$customer = Session::getCustomer();
		
		$trail = new BookingTrail();
		$trail->booking_id = $this->data['id'];
		$trail->event = $event;
		$trail->event_data = $data;
		
		$trail->customer_id = $customer['id'];

		if(!empty($profile)) {
			
			if($profile['type'] == 'a' && $profile['id'] == $this->data['agency_id']){
				$trail->agency_id = $profile['id'];
			}
			elseif($profile['type'] == 't' && $profile['id'] == $this->data['talent_id']){
				$trail->talent_id = $profile['id'];
			}			
		}

		//$trail->save();
		
	}


	/**
	* Get associated booking_trail
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrail (){

		$data = $this->db->query("SELECT * FROM booking_trail WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	


	/**
	* Get associated invoices
	* Returns array of invoice objects or empty array 
	*/

	public function getInvoices (){

		$data = $this->db->query("SELECT * FROM invoices WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new invoice();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated appoinments
	* Returns array of talent_appoinment objects or empty array 
	*/

	public function getTalentAppoinments (){

		$data = $this->db->query("SELECT * FROM appoinments WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentAppoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated booking_trails
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrails (){

		$data = $this->db->query("SELECT * FROM booking_trails WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated event
	* Returns event object or NULL 
	*/

	public function getEvent (){

		$data = $this->db->query("SELECT * FROM events WHERE id = ".$this->data['event_id'], NULL, 1);

		if(!empty($data)){
			$obj = new event();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	/**
	* Get possible events for this booking
	* based on booking date
	*/ 
	public function getPossibleEvents () {
		
		$location = $this->getLocation();
		
		return $this->db->query("select e.*, l.city, l.region, l.country
		from events e 
		join locations l on (l.id = e.location_id)
		where e.customer_id = ".$this->data['customer_id']." and DATE(e.begins) = DATE('".$this->data['session_start']."') and l.city = '".$location->city."' and l.country = '".$location->country."' and isnull(e.deleted)");
		
			}


	/**
	* Get associated appoinments
	* Returns array of appoinment objects or empty array 
	*/

	public function getAppoinment (){

		$data = $this->db->query("SELECT * FROM appoinments WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL, 1);

		if(!empty($data)){
			
			$obj = new Appoinment();
			$obj->populate($data);
			return $obj;
		}
		
		// create appointment if it is accepted 
		if($this->data['status'] == DataType::$BOOKING_ACCEPTED || $this->data['status'] ==  DataType::$BOOKING_CONFIRM){
			
			
			$info = $this->getinfo();
			
			$title = array();
			
			if(!empty($info['event']['id'])){
				$title[] = $info['event']['name'];
				$title[] = $info['event']['type_name'];
			}
			
			if(!empty($info['booking']['venue'])){
				$title[] = $info['booking']['venue'];
			}
				
			$title[] = $info['location']['city'];	
			
			
			// adding shadow appoinment for the talent
			$appoinment = new Appoinment();
			
			if(!empty($this->data['talent_id'])) {
				$appoinment->talent_id = $this->data['talent_id'];
			}
			if(!empty($this->data['agency_id'])) {
				$appoinment->agency_id = $this->data['agency_id'];
			}
			$appoinment->package_id = $this->data['package_id'];
			$appoinment->booking_id = $this->data['id'];
			
			if(!empty($this->data['agency_id'])){
				$appoinment->agency_id = $this->data['agency_id'];	
			}
			
			$appoinment->description = implode(', ', $title);
			$appoinment->all_day = 0;
			$appoinment->time_off = 0;
			$appoinment->begins = $this->data['session_start'];
			$appoinment->duration = $info['package']['timespan'];
			
			if($this->data['status'] ==  DataType::$BOOKING_CONFIRM){
				$appoinment->confirmed = 1;
			}
			else{
				$appoinment->confirmed = 0;
			}
			
			$appoinment->expired = $this->data['due_date'];
			$appoinment->save();
			return $appoinment;
		}

	}
	





	/**
	* Get associated appoinments
	* Returns array of appoinment objects or empty array 
	*/

	public function getAppoinments (){

		$data = $this->db->query("SELECT * FROM appoinments WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new appoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	
	/**
	* get event partners
	* Get all customer, agency and talent data involved in to the event
	*/
	public function getPartners () {
	
		// get creater and bookings made for the  event
		$item = $this->db->query("SELECT b.customer_id, t.customer_id AS talent_customer, t.id AS talent_id, a.customer_id AS agency_customer, a.id AS agency_id
FROM bookings b
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
WHERE b.id = ".$this->data['id'], NULL, 1);
		
				
		$out = array();
		// event creator
		$out[$this->data['customer_id']] = array(
					'customer_id' => $this->data['customer_id'],
					'talent_id' => NULL,
					'agency_id' => NULL
				);
				
		
		if(!empty($item['talent_customer'])){
				
			$out[$item['talent_customer']] = array(
				'customer_id' => $item['talent_customer'],
				'talent_id' => $item['talent_id'],
				'agency_id' => NULL
			);
		}	
		
		if(!empty($item['agency_customer'])){
				
			$out[$item['agency_customer']] = array(
				'customer_id' => $item['agency_customer'],
				'talent_id' => NULL,
				'agency_id' => $item['agency_id']
			);
		}
		
				
		
		return $out;

	}



	/**
	* Get associated message_boards
	* Returns array of message_board objects or empty array 
	*/

	public function getMessageBoards (){

		$data = $this->db->query("SELECT * FROM message_boards WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new messageBoard();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated event_bookings
	* Returns array of event_booking objects or empty array 
	*/

	public function getEventBookings (){

		$data = $this->db->query("SELECT * FROM event_bookings WHERE booking_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new eventBooking();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated change_requests
	* Returns array of change_request objects or empty array 
	*/

	public function getChangeRequests (){

		return $this->db->query("SELECT cr.*, l.display AS new_location, xl.display AS old_location, p.name AS new_package, xp.name AS old_package, p.fee AS new_fee, xp.fee AS old_fee,
s.name AS new_skill, xs.name AS old_skill, p.timespan AS new_timespan, xp.timespan AS old_timespan
FROM change_requests cr
LEFT JOIN locations l ON (l.id = cr.location_id)
LEFT JOIN locations xl ON (xl.id = cr.ex_location)
LEFT JOIN talent_packages p ON (p.id = cr.package)
left join categories c on (c.id = p.category_id)
LEFT JOIN skills s ON (s.id = c.skill_id)
LEFT JOIN talent_packages xp ON (xp.id = cr.ex_package)
left join categories xc on (xc.id = xp.category_id)
LEFT JOIN skills xs ON (xs.id = xc.skill_id)
WHERE cr.booking_id = ".$this->data['id']." ORDER BY `status` DESC, created desc");

	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}






	/**
	* Get associated subscription
	* Returns subscription object or NULL 
	*/

	public function getSubscription (){

		$data = $this->db->query("SELECT * FROM subscriptions WHERE id = ".$this->data['subscription_id'], NULL, 1);

		if(!empty($data)){
			$obj = new subscription();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	/*
	* Assign a subscription id which consume credits
	*/
	public function assignSubscription () {
		
		$data = $this->db->query("SELECT s.*,
(SELECT COUNT(*) FROM bookings WHERE subscription_id = s.id ) AS consumed
FROM talent_packages t 
JOIN categories c ON (c.id = t.category_id)
JOIN subscriptions s ON (s.category_id = c.id AND begin_date <= NOW() AND exp_date > NOW() AND s.active = 1 AND ISNULL(s.deleted))
WHERE t.id = {$this->data['package_id']}
HAVING consumed < s.jobs", NULL , 1);
		
		if(!empty($data)){
			
			$this->data['subscription_id'] = $data['id'];
			$this->save();
			
			return true;
			
		}
		
		return false;
	}


}

?>