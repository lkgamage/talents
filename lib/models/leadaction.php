<?php

/**********************************************
* LeadAction Class
**********************************************/

class LeadAction extends Model{

	public $table = 'lead_actions';

	public $fields = array(
		'id',
		'lead_id',
		'description',
		'user_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated lead
	* Returns lead object or NULL 
	*/

	public function getLead (){

		$data = $this->db->query("SELECT * FROM leads WHERE id = ".$this->data['lead_id'], NULL, 1);

		if(!empty($data)){
			$obj = new lead();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
