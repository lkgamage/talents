<?php

/**********************************************
* Customer data processing function
**********************************************/


/*** Class to warp customer data and process **/
class CustomerSnap {
	
	public $data = array(); 
	
	function __construct ($data){
		
		$this->data = $data;
	}
	
	
	/** Has talent profile */
	public function hasTalent () {
		
		return !empty($this->data['talent']['id']) && $this->data['talent']['active'] == 1 && empty($this->data['talent']['deleted']);
		
	}
	
	public function hasTalentPage () {
		return !empty($this->data['talent']['page']['handle']);
	}
	
	public function isTalentPageLive () {
		
		return $this->data['talent']['page']['active'] == 1 && !empty($this->data['talent']['page']['published']);
	}
	
	/** Check if talent has an active subscription */
	public function talentHasSubscription () {
		return !empty($this->data['talent']['subscriptions']);
	}
	
	public function talentSubscriptionsHasPackages () {
		
		if($this->talentHasSubscription()){
			
			foreach($this->data['talent']['subscriptions'] as $s){
				if(empty($s['packages'])){
					return false;	
				}
			}
		}
		
		return true;
	}
	
	/** has agency profile **/
	public function hasAgency () {
		
		return !empty($this->data['agency']['id']) && $this->data['agency']['active'] == 1 && empty($this->data['agency']['deleted']);
		
	}
	
	public function hasAgencyPage () {
		return !empty($this->data['agency']['page']['handle']);
	}
	
	public function isAgencyPageLive () {
		
		return $this->data['agency']['page']['active'] == 1 && !empty($this->data['agency']['page']['published']);
	}
	
	
	
	/** Check if talent has an active subscription */
	public function agencyHasSubscription () {
		return !empty($this->data['agency']['subscriptions']);
	}
	
	public function agencySubscriptionsHasPackages () {
		
		if($this->talentHasSubscription()){
			
			foreach($this->data['agency']['subscriptions'] as $s){
				if(empty($s['packages'])){
					return false;	
				}
			}
		}
		
		return true;
	}
	
	
	
	/**
	* Get associated notifications
	* Returns array of notification objects or empty array 
	*/

	public function getNotifications (){

		$data = $this->db->query("SELECT * FROM notifications WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new notification();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}

	
}



?>