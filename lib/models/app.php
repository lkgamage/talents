<?php
		
/****************************
* Application class
* Contains common methods avaialable through the app
*
*****************************/

class App{


	public static $db;
	
	public static $error;
	
	/**
	* Error codes to dientify excat error
	* 
	* 1 : disabled or deleted
	* 2 : short timeout
	* 3 : long timeout
	* 10: undefined object
	*/
	public static $error_code;
	
	
	public static function init(){
	
		if(empty(self::$db)){
			self::$db = new Database();
		}
		
		self::$error = NULL;	
	}
	
	
	/**
	* Get booking charges
	* make it a function to add custom booking rules
	*/
	public static function bookingCharge ($package) {
		
		return 0;
		
		return ($package->fee > 500) ? ceil($package->fee/100) : 5;
		
	}
	
	
	/**
	* Get talent categories
	*/
	public static function getSkills ($type = NULL){
		
		
		if(file_exists(Config::$cache_path.'/system/skills.ccf') && filemtime(Config::$cache_path.'/system/skills.ccf') > time()-3600 ){
	
			$content = file_get_contents(Config::$cache_path.'/system/skills.ccf');
			return json_decode($content, true);
	
		}
		else{
	
			self::init();
			$data = self::$db->query("select * from skills WHERE is_agency = 0 and isnull(deleted) order by category, is_business, name ");	
			$json = json_encode($data);
			file_put_contents(Config::$cache_path.'/system/skills.ccf', $json);
			return $data;
		}
		
		
	}
	
	/**
	* Get talent categories as array
	*/
	public static function getSkillArray (){
		

		if(file_exists(Config::$cache_path.'/system/skills.ccf') && filemtime(Config::$cache_path.'/system/skills.ccf') > time()-3600){
		
			$content = file_get_contents(Config::$cache_path.'/system/skills.ccf');
			$content =  json_decode($content, true);
			return Util::groupArray($content, 'id');
		}
		else{
			
			self::init();
			$data = self::$db->query("select * from skills WHERE is_agency = 0 and isnull(deleted) order by category, is_business, name ");	
			$json = json_encode($data);
			file_put_contents(Config::$cache_path.'/system/skills.ccf', $json);
			return Util::groupArray($data, 'id');
		}		
	
	}
	
	/*
	* Get talent categories
	*/
	public static function getAgencyTypes (){
		return DataType::$AGENCY_TYEPS;	
		
	}
	
	
	
	/**
	* Get event types
	*/
	public static function getEventTypes () {
		
		self::init();
		$data = self::$db->query("select * from event_types where isnull(deleted) order by name");
		
		return Util::groupArray($data, 'id', 'name');
		
	}
	
	
	/**
	* Sending OTP
	*/
	/**
	* Create and send OTP 
	*/
	//public static function sendOTP ($object, $type ='l') {

	public static function sendOTP ($type, $id, $customer_id) {
		
		self::init();
		
		
		//check customer total
		$ctotal = self::$db->query("SELECT COUNT(*) as total FROM codes WHERE customer_id = {$customer_id} AND created > ADDTIME(NOW(),'-2:00:00')",NULL,1);
		
		if($ctotal['total'] >= 6){
			self::$error_code = 3;
			self::$error = "You have reached maximum code limit.<br>Please request a code aftre 2 hours";

			return false;
		}
				
		if($type == 'p'){
			$excodes = self::$db->query("SELECT phone_id as object_id, COUNT(*) AS total, MAX(created) AS lastsend,  TIME_TO_SEC(TIMEDIFF(NOW(), MAX(created))) as diff FROM codes
 WHERE phone_id = ".$id." AND verified = 0 AND created > '".date("Y-m-d H:i:s", strtotime("-6 hours"))."' ", NULL, 1);
		}
		elseif($type == 'e'){
			$excodes = self::$db->query("SELECT email_id as object_id, COUNT(*) AS total, MAX(created) AS lastsend,  TIME_TO_SEC(TIMEDIFF(NOW(), MAX(created))) as diff FROM codes
 WHERE email_id = ".$id." AND verified = 0 AND created > '".date("Y-m-d H:i:s", strtotime("-6 hours"))."' ", NULL, 1);
		}
		
	
		if(empty($excodes['total'])) {
		
			$code = new Code();
			$code->type = $type;
			$code->customer_id = $customer_id;
			
			if($type == 'p') {
				$code->phone_id = $id;
			}
			elseif($type == 'e') {
				$code->email_id = $id;				
			}
			
			$code->attempts = 0;
			$code->verified = 0;
			$code->save();
						
			Util::setCookie(DataType::$COOKIE_CODE, $code->id);		
			
			return $code;
		}
		else{
			
			/* 	analize to see if spaming.
			*	if there are 3 unsuccesfull logins and tring to another
			* 	user should have wait 1 hours before sending another otp
			* 	interval between each is 2 min
			*/
			if($excodes['diff'] < 119 ){
				
				self::$error_code = 2;
				self::$error = "Wait ".(119-$excodes['diff'])." sec before send another code ";
				//.date("H:i:s");
				return false;
			}
			
			if($excodes['total'] >= 3){
				
				
				self::$error_code = 3;
				self::$error = "We have sent 3 OTP codes.<br> Please request new code after 6 hours";

				return false;
			}		

			$code = new Code();
			$code->type = $type;
			$code->customer_id = $customer_id;
			
			if($type == 'p') {
				$code->phone_id = $id;
			}
			elseif($type == 'e') {
				$code->email_id = $id;				
			}
			
			$code->type = $type;
			$code->attempts = 0;
			$code->verified = 0;
			$code->save();
						
			Util::setCookie(DataType::$COOKIE_CODE, $code->id);		
			
			return $code;
				
		}		
		
		return false;
	}
	
	
	
	
	/**
	* check if handle is available
	*/
	public static function isHandlerAvailable ($handler) {
		
		self::init();
		$handler = self::$db->escape($handler);
		
		$data = self::$db->query("select * from pages where handle = '".$handler."' and isnull(deleted)", NULL, 1);
		
		return empty($data);
	}
	

	/**
	* Find page by handle
	*/
	public static function findPage ($handler){
		
		self::init();
		$handler = self::$db->escape($handler);
		$data = self::$db->query("select * from pages where handle = '".$handler."'", NULL, 1);
		
		if(!empty($data)){
			$page = new Page();
			$page->populate($data);
			return $page;	
		}
		
		return false;
	}
	
	
	/**
	* Search talent by given name, handle
	*/
	public static function searchTalentsByName ($keyword = '', $agency_id = '') {
		
		self::init();
		
		if(!empty($keyword)){
			$keyword = str_replace('"','',$keyword);
			$keyword = str_replace('@','',$keyword);
			$keyword = self::$db->escape(trim($keyword,'@'));
			$condition = '(t.name like "%'.$keyword.'%" or p.handle like "'.$keyword.'%")';	
		}
		else{
			$condition = " 1 = 1";	
		}
	
		return self::$db->query('SELECT  t.id as talent_id, t.customer_id, t.name, t.gender, l.city, l.region, l.country,
p.id AS page_id, p.handle,
(SELECT GROUP_CONCAT(skill) FROM talent_skills WHERE talent_id = t.id) AS skills,
i.i400 AS image, r.status AS request_status, r.modified AS request_modified
FROM talents t 
JOIN locations l ON (l.id = t.location_id)
LEFT JOIN pages p ON (p.talent_id = t.id)
LEFT JOIN images i ON (i.id = t.image_id)
LEFT JOIN (select max(id) as id, talent_id, agency_id, deleted from agency_request group by talent_id, agency_id) ar on (ar.talent_id = t.id and ar.agency_id = '.$agency_id.' )
LEFT JOIN agency_request r ON (r.id = ar.id and isnull(r.deleted))
WHERE  t.active = 1 AND ISNULL(t.deleted) and '.$condition.' order by t.name, p.handle');
		
	}
	
	
	/**
	* Universal booking search function
	* talent_id
	* agency_id
	* event_id
	* status
	* date
	* type : (for agencies) r - recevied, m - made
	*/
	public static function getBookings ($customer_id = NULL, $talent_id = NULL, $agency_id = NULL, $event_id = NULL, $status = NULL, $date = NULL, $type = NULL) {
				
		$conditions = array();
		
		if(!empty($customer_id)){
			$conditions[] = "b.customer_id = ".$customer_id;
		}
		
		if(!empty($talent_id) && is_numeric($talent_id)){
			$conditions[] = "b.talent_id = ".$talent_id	;
		}
		
		if(!empty($agency_id) && is_numeric($agency_id)){
			$conditions[] = "b.agency_id = ".$agency_id	;
			
			if(!empty($type)){
				if($type == 'r'){
					$conditions[] = " isnull(b.talent_id) ";
				}
				else{
					$conditions[] = " !isnull(b.talent_id) ";
				}
			}
		}
		
		if(!empty($event_id) && is_numeric($event_id)){
			$conditions[] = "b.event_id = ".$event_id	;
		}
		
		if(empty($date)){
			$conditions[] = "b.session_start >= NOW()";	
		}
				
		
		$order = "FIELD(b.status,'AP','CO','BA','PP','CN','RJ'), b.due_date, b.session_start";
		
		if(!empty($status)){
			
			if($status == 1) {
				$conditions[] = "(b.status in ('".DataType::$BOOKING_PENDING."',  '".DataType::$BOOKING_PAYMENT."', '".DataType::$BOOKING_ACCEPTED."') || (b.status = '".DataType::$BOOKING_CONFIRM."' && date(b.session_ends) >= '".date("Y-m-d")."'))";
			}
			else {
				$conditions[] =  "b.status = '".$status."'";	
			}
		}
		
		if(!empty($date)){
			
			if($date == -1){
				$conditions[] =  "date(b.session_start) < '".$date."'";	
				$order = "b.session_start desc";
			}
			else{
				$conditions[] =  "date(b.session_start) = '".$date."'";	
			}
		}
		else{
			//$conditions[] =  "date(b.session_ends) >= '".date("Y-m-d H:i")."'";		
		}
		
		// If you are updating this, update the following getAgendaBookings() function too
	
		$query = "SELECT  c.firstname AS customer_name, c.id AS customer_id, t.id AS talent_id ,  t.customer_id as talent_customer_id,  t.name AS talent_name, i.i400 AS talent_image, t.gender AS talent_gender,
 e.id AS event_id, b.name as booking_name, e.name AS event_name,l.venue, l.address, l.city,l.region, l.postalcode, l.country, p.id AS package_id, p.name AS package_name, p.timespan as duration, s.name as skill_name, a.id as agency_id, a.name as agency_name, a.customer_id as agency_customer_id, ai.i400 as agency_image, a.agency_type as agency_type, IF(TIME_TO_SEC(TIMEDIFF(session_start,NOW())) > 0, 1, 0 ) AS overdue, b.*
FROM bookings b
JOIN customers c ON (c.id = b.customer_id) 
JOIN locations l ON (l.id = b.location_id)
JOIN talent_packages p ON (p.id =  b.package_id)
join categories cc on (cc.id = p.category_id)
JOIN skills s on (s.id = cc.skill_id)
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN events e ON (e.id = b.event_id)
LEFT JOIN images i ON (i.id = t.image_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
LEFT JOIN images ai ON (ai.id = a.image_id)
WHERE ".implode(" and ", $conditions)." order by ".$order;

//echo $query;

		$buffer = new Buffer();
		$buffer->_datasql = $query;
		$buffer->_countsql = "select count(*) as total  FROM bookings b where ".implode(" and ", $conditions);
		$buffer->_component = "booking/bookings";
		$buffer->_pagesize = 50;
		$buffer->_page = 1; 
		return $buffer;
	}
	
	/**
	* Get bookings for event items/agenda
	*/
	public static function getAgendaBookings ($item_ids){
		
		if(empty($item_ids)){
			return array();	
		}
		
		self::init();
		
		$conditions = array('isnull(eb.deleted)');
		
		if(is_array($item_ids)){
			$conditions[] = "eb.item_id in (".implode(',', $item_ids).")";	
		}
		else{
			$conditions[] = "eb.item_id = ".$item_ids."";	
		}
		
		return self::$db->query("SELECT eb.item_id,  t.id AS talent_id ,  t.customer_id as talent_customer_id,  t.name AS talent_name, i.i400 AS talent_image, t.gender AS talent_gender,
 e.id AS event_id, b.name as booking_name, e.name AS event_name, p.id AS package_id, p.name AS package_name, p.timespan as duration, s.name as skill_name, a.id as agency_id, a.name as agency_name, a.customer_id as agency_customer_id, ai.i400 as agency_image,  at.name as agency_type, IF(TIME_TO_SEC(TIMEDIFF(session_start,NOW())) > 0, 1, 0 ) AS overdue, b.*
FROM event_bookings eb
JOIN bookings b on (b.id = eb.booking_id)
JOIN talent_packages p ON (p.id =  b.package_id)
join categories c on (c.id = p.category_id)
JOIN skills s on (s.id = c.skill_id)
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN events e ON (e.id = b.event_id)
LEFT JOIN images i ON (i.id = t.image_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
LEFT JOIN agency_types at ON (at.id = a.agency_type)
LEFT JOIN images ai ON (ai.id = a.image_id)
WHERE ".implode(" and ", $conditions)." order by eb.created desc", NULL);
		
		
	}
	
	
	/**
	* Create a subscription for current profile 
	* return a subscription object
	* check If there is another paid subscription effective in given period,
	* if this is an upgrade, happen imiditely
	* if this is a downgrade, it will update at next billig cycle
	* Payment (if any) must be made before call this
	* array(
			'subscription' => 
			'reneval' => 
			'method' => 
			);
	*/
	

	
	/**
	* Get agency packages
	*/
	public static function getAgencyCategory () {
		
		self::init();
		
		$data = self::$db->query("SELECT * FROM skills WHERE is_agency = 1 and isnull(deleted)", NULL, 1);

		if(!empty($data)) {
			$skill = new Skill();
			$skill->populate($data);
			return $skill;
		}


		return false;;
		
	}
	
	
	/**
	* Search login records for given username
	*/
	public static function searchLogin ($username) {
		
		self::init();
		
		$username = Validator::cleanup($username, 100);
		$username = self::$db->escape($username);
		return self::$db->query("SELECT * FROM active_logins WHERE username = '".$username."'", NULL, 1);	
		
	}
	
	
	/**
	* Get customer session for given figerprint
	*/
	public static function getCustomerSession ($customer_id, $fingureprint) {
		
		self::init();
		
		return self::$db->query("SELECT c.* FROM 
customer_logins cl 
JOIN customers c ON (c.id = cl.customer_id)
WHERE cl.customer_id = ".$customer_id." AND fingureprint = ".$fingureprint." AND ISNULL(cl.deleted)", NULL, 1);
		
	}
	
	
	/**
	* Get all published pages
	*/
	public static function getPublishedPages () {
		
		$sql = "SELECT handle FROM pages WHERE !ISNULL(published) AND active = 1 AND ISNULL(deleted)";
		
		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = "SELECT count(*) as total FROM pages WHERE !ISNULL(published) AND active = 1 AND ISNULL(deleted)";
		$buffer->_component = "booking/bookings";
		$buffer->_pagesize = 50;
		$buffer->_page = 1; 
		return $buffer;
		
		
		
	}
	
	
	public static function getCustomerContact ($customer_id) {
		
		self::init();
		
		return self::$db->query("SELECT e.id AS email_id, p.id AS phone_id, e.email, p.phone, p.country_code
FROM customers c
LEFT JOIN emails e ON (e.customer_id = c.id AND e.is_primary AND ISNULL(e.deleted))
LEFT JOIN phones p ON (p.customer_id = c.id AND p.is_primary AND ISNULL(p.deleted))
WHERE c.id = ".$customer_id, NULL, 1);
		
	}
	
	
	/**
	* Get free package for given skill
	**/
	public static function getFreePackage ($skill_id) {
		
		self::init();
		
		$data = self::$db->query("SELECT * FROM packages WHERE skill_id = ".$skill_id." AND active = 1 AND ISNULL(deleted) AND price_monthly = 0", NULL, 1);
		
		if(!empty($data)){
			
			$package = new Package();
			$package->populate($data);
			return $package;
			
		}
		
		return false;
		
	}
	
	
	/**	
	* get category object by given stripe secret
	*/
	public static function getInvoiceByIntent ($intent){
		
		self::init();
		
		$data =  self::$db->query("SELECT i.* FROM  payments p JOIN invoices i ON (i.id = p.invoice_id) WHERE p.intent_id = '".self::$db->escape($intent)."'", NULL, 1);
																														
		if(!empty($data)){
			$invoice = new Invoice();
			$invoice->populate($data);
			return $invoice;
		}
		
		return false;
	}
	
	/**
	* Get nessasary details for a profile
	*/
	public function getProfile ($id) {
		
		return $this->db->query("SELECT p.id, p.name, p.is_agency, i.thumb, i.id as dpid, p.gender, pp.handle, GROUP_CONCAT(k.name SEPARATOR ', ') AS skills
	FROM profiles p
	left join pages pp ON (pp.profile_id = p.id)
	LEFT JOIN images i ON (i.id = p.image_id)
	LEFT JOIN categories c ON (c.profile_id = p.id AND ISNULL(c.deleted))
	LEFT JOIN skills k ON (k.id = c.skill_id)
	WHERE  p.customer_id = {$id} AND p.active = 1 AND ISNULL(p.deleted) ", NULL, 1);
	}
	
	/**
	* Get list of tags
	*/
	public static function getTags () {		
		
		if(file_exists(Config::$cache_path.'/system/tags.ccf') && filemtime(Config::$cache_path.'/system/tags.ccf') > time()-3600){
		
			$content = file_get_contents(Config::$cache_path.'/system/tags.ccf');
			return json_decode($content, true);
	
		}
		else{
			self::init();
			$data = self::$db->query("select id, tag from tags WHERE isnull(deleted) order by tag ");
			$data = Util::groupArray($data, 'id', 'tag');
			$json = json_encode($data);
			file_put_contents(Config::$cache_path.'/system/tags.ccf', $json);
			return $data;
		}
		
	}
	
	/**
	* Get tag id by tag
	*/
	public static function getTagID ($tag) {
		
		
		$tags = self::getTags();
		
		foreach ($tags as $id => $name) {
			if($tag == $name) {
				return $id;
			}
		}
		
		return NULL;
	}
	
	/**
	* Get Invoice by stripe id
	*/
	public static function getInvoice ($stripe_id){
		
		self::init();
		
		$data = self::$db->query("select * from invoices where stripe_id = '{$stripe_id}'", NULL, 1);
		
		if(!empty($data)){
			$invoice = new Invoice();
			$invoice->populate($data);
			return $invoice;
		}
		
		return false;
	}
	
	
	/**
	* Get categorysubscription record for given
	* stripe id
	*/
	public static function getCategorySubscription ($stripe_id){		
		
		self::init();
		
		$data = self::$db->query("select * from category_subscriptions where stripe_id = '{$stripe_id}'", NULL, 1);
		
		if(!empty($data)){
			$object = new CategorySubscription();
			$object->populate($data);
			return $object;
		}
		
		return false;			
	}
	
	/**
	* Get package by stripe price id
	*/
	public static function getPackage ($price_id){
		
		self::init();
		
		$data = self::$db->query("select * from packages where price_id = '{$price_id}'", NULL, 1);
		
		if(!empty($data)){
			$object = new Package();
			$object->populate($data);
			return $object;
		}
		
		return false;
		
	}
	
	/**
	* Get customer by stripe id
	*/
	public function getCustomer ($stripe_id){
		
		self::init();
		
		$data = self::$db->query("select * from customers where stripe_id = '{$stripe_id}'", NULL, 1);
		
		if(!empty($data)){
			$object = new Customer();
			$object->populate($data);
			return $object;
		}
		
		return false;
		
	}
	
	/**
	* Search for city
	*/
	public function searchCity ($city){
		
		self::init();
		$city = filter_var($city, FILTER_SANITIZE_STRING);
		
		return self::$db->query("SELECT city, region, country 
		FROM  locations WHERE city like '%".self::$db->escape($city)."%' GROUP BY city, region, country ");		
		
	}
	

}

?>