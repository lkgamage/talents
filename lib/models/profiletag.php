<?php

/**********************************************
* ProfileTag Class
**********************************************/

class ProfileTag extends Model{

	public $table = 'profile_tags';

	public $fields = array(
		'id',
		'profile_id',
		'tag_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated tag
	* Returns tag object or NULL 
	*/

	public function getTag (){

		$data = $this->db->query("SELECT * FROM tags WHERE id = ".$this->data['tag_id'], NULL, 1);

		if(!empty($data)){
			$obj = new tag();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
