<?php

/**********************************************
* Profile Class
**********************************************/

class Profile extends Model{

	public $table = 'profiles';

	public $fields = array(
		'id',
		'customer_id',
		'is_agency',
		'verified',
		'location_id',
		'image_id',
		'agency_type',
		'gender',
		'currency',
		'timezone',
		'timeoffset',
		'name',
		'name_changed',
		'limited_hours',
		'active',
		'commitment',
		'exclusive',
		'notes',
		'exclusive',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Check profile type
	*/
	public function type () {
		
		return $this->data['is_agency'] == 1 ? 'a' : 't';
		
	}
	
	/**
	* general function to object check type
	*/
	public function isAgency () {
		return !empty($this->data['is_agency']) ;	
	}
	
	public function isTalent () {
		return empty($this->data['is_agency']) ;	
	}
	
	/**
	* Get display picture
	*/
	public function getDP () {
	
		if(!empty($this->data['image_id'])){
			return new Image($this->data['image_id']);
		}
		
		return NULL;
	}


	/**
	* Get associated profile_tags
	* Returns array of profile_tag objects or empty array 
	*/

	public function getProfileTags (){

		$data = $this->db->query("SELECT * FROM profile_tags WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new profileTag();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated categories
	* Returns array of category objects or empty array 
	*/

	public function getCategories ($as_array = false){
		
		if($as_array){
			return $this->db->query("SELECT c.id, c.skill_id, s.name, s.category 
FROM categories c 
JOIN skills s ON (s.id = c.skill_id)
WHERE c.profile_id = {$this->data['id']} AND ISNULL(c.deleted) order by s.name");			
			
		}

		$data = $this->db->query("SELECT * FROM categories WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new category();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Check if profile has given category
	* return category object
	*/
	public function hasCategory ($skill_id) {
		
		$data = $this->db->query("SELECT * FROM categories WHERE profile_id = ".$this->data['id']." and skill_id = ".$skill_id." and isnull(deleted)", NULL, 1);
		
		if(!empty($data)){
			$obj = new Category();
			$obj->populate($data);	
			return $obj;
		}
		
		return false;
		
	}
	
	/**
	* Add to given category
	*/
	public function addCategory ($skill_id) {
		
		$data = $this->db->query("SELECT * FROM categories WHERE profile_id = ".$this->data['id']." and skill_id = ".$skill_id." and isnull(deleted)", NULL, 1);
				
		$category = new Category();
		
		if(!empty($data)){
			$category->populate($data);
			
			if($category->isDeleted()){
				$category->undelete();
			}
		}
		else{
			$category->profile_id = $this->data['id'];
			$category->skill_id = $skill_id;
			$category->renewal = 1;
			//$category->package_id = Validator::cleanup($_POST['category_package_id'], 100);
			//$category->term = Validator::cleanup($_POST['category_term'], 1);
			//$category->renewal_date = Validator::cleanup($_POST['category_renewal_date'], 2);
			//$category->last_renewal = date("Y-m-d", strtotime($_POST['category_last_renewal'] ));
			$category->save();
		}
		
		return $category;
		
	}
	
	


	/**
	* Get associated talent_packages
	* Returns array of talent_package objects or empty array 
	*/

	public function getTalentPackages (){

		$data = $this->db->query("SELECT * FROM talent_packages WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentPackage();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get package details
	*/
	public function getPackages ($as_array = false) {
		
		if($as_array){
			
			return $this->db->query("SELECT t.id, c.skill_id, s.name AS skill_name, t.name, t.description, t.timespan, t.interval, t.fee, t.fee_local, t.items, t.active, t.deleted , p.id AS subscription_id,
			(SELECT COUNT(*) FROM bookings WHERE package_id = t.id) AS bookings
FROM categories c
LEFT JOIN talent_packages t ON (t.category_id = c.id)
LEFT JOIN skills s ON (s.id = c.skill_id) 
LEFT JOIN subscriptions p ON (p.category_id = c.id AND  p.active = 1 && p.begin_date <= NOW() AND p.exp_date >= DATE(NOW()) AND ISNULL(p.deleted)) 
WHERE c.profile_id = {$this->data['id']} AND ISNULL(c.deleted) and isnull(t.deleted) ORDER BY t.fee");
			
		}
		
		$data = $this->db->query("SELECT p.* 
		FROM categories c
		LEFT JOIN talent_packages p ON (p.category_id = c.id)
		WHERE c.profile_id = ".$this->data['id']." and isnull(p.deleted) 
		order by name");
		
		$out = array();
		if(!empty($data)){
			
			foreach ($data as $item){
				$p = new Talentpackage();
				$p->populate($item);
				$out[] = $p;	
			}
			
		}
		
		return $out;
	}
	
	/**
	* Get details regarding each packages
	* contain details about all packages
	*/
	public function getPackageDetail ($active_only = true) {
		
		$con = "c.profile_id = ".$this->data['id'];
		
		if($active_only){
			$con .= " and p.active = 1 and isnull(p.deleted)";	
		}
		
		$data = $this->db->query("SELECT p.id AS package__id, p.active AS package__active,  c.skill_id AS package__skill_id, p.name AS package__name, p.description AS package__description, p.items AS package__items, p.timespan AS package__timespan, p.interval AS package__interval, p.location_required AS package__location_required, p.fee AS package__fee, p.advance_percentage AS package__advance_percentage, p.is_private AS package__is_private, p.fulday AS package__fulday, p.created AS package__created, p.modified AS package__modified, p.deleted AS package__deleted, t.currency AS package__currency,
s.id AS skill__id, s.parent_id AS skill__parent_id, s.name AS skill__name, s.genre AS skill__genre, s.gender_required AS skill__gender_required, s.is_default AS skill__is_default, s.created AS skill__created, s.modified AS skill__modified, s.deleted AS skill__deleted
FROM categories c
JOIN talent_packages  p ON (p.category_id = c.id)
JOIN profiles t ON (t.id = c.profile_id)
JOIN skills s ON ( s.id = c.skill_id)
WHERE ".$con." order by fee");



		return Util::groupResult($data, true);
		
	}
 

	/**
	* Get associated ledgers
	* Returns array of ledger objects or empty array 
	*/

	public function getLedgers (){

		$data = $this->db->query("SELECT * FROM ledgers WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new ledger();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get associated pages
	* Returnsexisting page or create a new page
	*/

	public function getPage (){

		$data = $this->db->query("SELECT * FROM pages WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL, 1);
		
		$page = new Page();	
		
		if(!empty($data)){
			$page->populate($data);
		}
		else {
			$page->customer_id = $this->data['customer_id'];
			$page->profile_id = $this->data['id'];
			$page->active = 0;
			$page->save();
			
			// creating parts
			$sections = array('a', 'b', 'h', 'c', 'd', 'e', 'f', 'g');
			foreach ($sections as $s){
				$page->getSection($s);	
			}
		}
		
		return $page;
		
	}



	/**
	* Get associated pages
	* Returns array of page objects or empty array 
	*/

	public function getPages (){

		return $this->getPage();
	}



	/**
	* Get associated bookings
	* Returns array of booking objects or empty array 
	*/

	public function getBookings (){

		$data = $this->db->query("SELECT * FROM bookings WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new booking();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}






	/**
	* Get associated agency_request
	* Returns array of agency_request objects or empty array 
	*/

	public function getAgencyRequest (){

		$data = $this->db->query("SELECT * FROM agency_request WHERE agency_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agencyRequest();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}






	/**
	* Get associated talent_schedules
	* Returns array of talent_schedule objects or empty array 
	*/

	public function getTalentSchedules (){

		$data = $this->db->query("SELECT * FROM talent_schedules WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentSchedule();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get image 
	*/
	public function getImage () {
		
		if(!empty($this->data['image_id'])) {
			return new Image($this->data['image_id']);
		}
		else{
			
			$image = new image();
			
			if($this->data['gender'] == 1){
				$url = Util::mapURL('/images/placeholder-boy.png');
			}
			else{
				$url = Util::mapURL('/images/placeholder-girl.png');
			}
			
			$image->i600 = $url;
			$image->i400 = $url;
			$image->thumb = $url;
			$image->url = $url;
			
			return $image;
		}			
	}
	
	
	/**
	* Get profile skill strings
	*/
	public function getSkillString (){
		
		$data = $this->db->query("SELECT GROUP_CONCAT(k.name) as skills FROM 
categories c
LEFT JOIN skills k ON (k.id = c.skill_id) 
WHERE profile_id = ".$this->data['id']." AND ISNULL(c.deleted)", NULL, 1);
		
		return $data['skills'];
		
	}
	
	
	/**
	* Get talent location
	*/
	public function getLocation () {
	
		return new Location($this->data['location_id']);
		
	}



	/**
	* Get associated invoices
	* Returns array of invoice objects or empty array 
	*/

	public function getInvoices (){

		$data = $this->db->query("SELECT * FROM invoices WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new invoice();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get associated appoinments
	* Returns array of talent_appoinment objects or empty array 
	* from: yyyy-mm-dd
	*/
	
	public function getAppoinments ($from = NULL, $to = NULL, $as_array = false) {

		if(empty($from)){
			//
			$from =  date("Y-m-d");
		}
		else {
			$f = explode(' ', $from);
			
			if(count($f) == 1){
				$from =  Util::ToSysDate($from,'00:00');
			}
		}
		
		if(empty($to)){
			$to = date("Y-m-d H:i:s", strtotime('+1 day', strtotime($from)));		
		}
		else{
			$t = explode(' ', $to);		
			if(count($t) == 1){
				$to =  Util::ToSysDate($to,'11:59PM');	
			}
		}
		
		if($as_array){
			return $this->db->query("SELECT a.*, b.talent_fee, l.venue, l.address, l.city, l.region, l.country, g.name AS with_name, p.id AS package_id, p.name AS package_name, s.name AS skill_name
FROM 
appoinments a
LEFT JOIN profiles g ON (g.id = a.agency_id)
LEFT JOIN bookings b ON (b.id = a.booking_id)
LEFT JOIN locations l ON (l.id = b.location_id)
LEFT JOIN packages p ON (p.id = a.package_id)
LEFT JOIN skills s ON (s.id = p.skill_id) 
WHERE a.profile_id = ".$this->data['id']." and a.begins >= '".$from."' and a.begins <= '".$to."' and isnull(a.deleted) and (confirmed = 1 || isnull(a.expired) ||  a.expired > NOW()) order by a.begins,a.id");


		}

		$data = $this->db->query("SELECT * FROM appoinments WHERE talent_id = ".$this->data['id']." and begins >= '".$from."' and begins <= '".$to."' and isnull(deleted) and (isnull(expired) || expired > NOW()) order by begins,id", NULL);
		
		

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Appoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get talent's time line for given time
	*/
	public function getTimeline ($datetime){
		
		$datetime = Util::ToSysDate($datetime,'');
		
		$timeline = new Timeline($datetime, $this->data['timeoffset']);
			
		$range = $timeline->getRange();
		
		$appointments = $this->getAppoinments($range[0],$range[1], true);

		if(!empty($appointments)){
			
			foreach ($appointments as $a) {
			
				if(empty($a['confirmed'])){
					continue;
				}
								
				if(empty($a['all_day'])){
					$timeline->addToLine($a['begins'], $a['duration']+$a['rest_time'] );	
				}
				else {
					$timeline->addFullDay($a['begins'], $this->data['timeoffset']);					
				}
				
			}
		}
		
		if(!empty($this->data['limited_hours'])){
			
			$schedule = $this->getSchedule();
			
			$timeline->setWorkHours($schedule, $this->odata['timeoffset']);			
		}
		
		// check simultaneous posibility
		// simultanious bookings will be check at category level
		
		return $timeline;
		
	}
	
	
	/**
	* Get monthly appointments, 
	*/
	public function getMontlyAppoinments ($year, $month){
		
		$dayone = strtotime($year."-".$month.'-01');
		$firstday = date('D',$dayone) == 'Sunday' ? $dayone : strtotime("last sunday", $dayone);
		$dayend = strtotime(date("Y-m-t", $dayone));
		$lastday = date('D',$dayend) == 'Saturday' ? $dayend : strtotime("next saturday", $dayend);
		
		$offset = Session::timeDiff();
		
		$appointmnets = $this->db->query("SELECT  DATE(ADDTIME(begins, '".$offset."')) AS dates,COUNT(id) AS total, SUM(duration) AS minutes, confirmed
FROM appoinments
WHERE profile_id = ".$this->data['id']." and DATE(begins) >= '".date('Y-m-d', $firstday)."' and DATE(begins) <= '".date('Y-m-d', $lastday)."' and isnull(deleted)
GROUP BY DATE(begins), confirmed");


		$data = array();
		if(!empty($appointmnets)){
			
			foreach ($appointmnets as $a){
				
				if(!isset($data[$a['dates']])){
					$data[$a['dates']] = array('confirmed' => 0, 'confirmed_time' => 0 ,'shadow' => 0,'shadow_time' => 0, 'minutes' => 0);
				}
				
				if(!empty($a['confirmed'])){
					$data[$a['dates']]['confirmed'] = $a['total'];
					$data[$a['dates']]['confirmed_time'] = $a['minutes'];
				}
				else{
					$data[$a['dates']]['shadow'] = $a['total'];
					$data[$a['dates']]['shadow_time'] = $a['minutes'];
				}
				
				$data[$a['dates']]['minutes'] += $a['minutes'];
				
			}
			
		}
		
	

		$out = array();
		$date = $firstday;
		
		while ($date <= $lastday){
			
			$dt = date('Y-m-d', $date);
			
			$out[$dt] = isset($data[$dt]) ? $data[$dt] : NULL;

	
			$date = strtotime('+1 day', $date);
		}
		
		return $out;
	}

	
	/**
	* Get detail infor about the talent
	*/
	public function getinfo () {
		
		$data = $this->db->query("SELECT f.id as profile__id, 
f.customer_id as profile__customer_id, 
f.is_agency as profile__is_agency, 
f.verified as profile__verified, 
f.location_id as profile__location_id, 
f.image_id as profile__image_id, 
f.agency_type as profile__agency_type, 
f.gender as profile__gender, 
f.currency as profile__currency, 
f.timezone as profile__timezone, 
f.timeoffset as profile__timeoffset, 
f.name as profile__name, 
f.name_changed as profile__name_changed, 
f.limited_hours as profile__limited_hours, 
f.active as profile__active, 
f.commitment as profile__commitment, 
f.created as profile__created, 
f.modified as profile__modified, 
f.deleted as profile__deleted,
c.id as customer__id, 
c.firstname as customer__firstname, 
c.lastname as customer__lastname, 
c.name_changed as customer__name_changed, 
c.dob as customer__dob, 
c.location_id as customer__location_id, 
c.timeoffset as customer__timeoffset, 
c.timezone as customer__timezone, 
c.currency as customer__currency, 
c.image_id as customer__image_id, 
c.active as customer__active, 
c.gender as customer__gender, 
c.is_trans as customer__is_trans, 
c.language as customer__language, 
c.stripe_id as customer__stripe_id, 
c.created as customer__created, 
c.modified as customer__modified, 
c.deleted as customer__deleted,
l.id as location__id, 
l.customer_id as location__customer_id, 
l.place_id as location__place_id, 
l.display as location__display, 
l.venue as location__venue, 
l.address as location__address, 
l.city as location__city, 
l.region as location__region, 
l.postalcode as location__postalcode, 
l.country as location__country, 
l.lng as location__lng, 
l.lat as location__lat, 
l.locked as location__locked, 
l.created as location__created, 
l.modified as location__modified, 
l.deleted as location__deleted,
i.id as image__id, 
i.customer_id as image__customer_id, 
i.i600 as image__i600, 
i.i400 as image__i400, 
i.thumb as image__thumb, 
i.url as image__url, 
i.orientation as image__orientation, 
i.width as image__width, 
i.height as image__height, 
i.created as image__created, 
i.modified as image__modified, 
i.deleted as image__deleted,
p.id as page__id, 
p.customer_id as page__customer_id, 
p.profile_id as page__profile_id, 
p.handle as page__handle, 
p.banner_id as page__banner_id, 
p.name as page__name, 
p.description as page__description, 
p.skills as page__skills, 
p.image as page__image, 
p.active as page__active, 
p.handle_updated as page__handle_updated, 
p.updated as page__updated, 
p.published as page__published, 
p.created as page__created, 
p.modified as page__modified, 
p.deleted as page__deleted,
GROUP_CONCAT(DISTINCT k.name SEPARATOR  ', ') AS profile__skills,
(SELECT GROUP_CONCAT(tag_id) FROM profile_tags WHERE profile_id = f.id and isnull(deleted)) as profile__tags
FROM profiles f 
LEFT JOIN customers c ON ( c.id = f.customer_id)
LEFT JOIN locations l ON ( l.id = f.location_id)
LEFT JOIN images i ON ( i.id = f.image_id)
LEFT JOIN pages p ON ( p.profile_id = f.id)
LEFT JOIN categories a ON ( a.profile_id = f.id and isnull(a.deleted))
LEFT JOIN skills k ON ( k.id = a.skill_id)
WHERE f.id = ".$this->data['id'], NULL, 1);

		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		$out['categories'] = $this->db->query("SELECT c.id AS category_id, s.name AS skill_name, s.id AS skill_id, a.begin_date, a.exp_date, p.name AS package_name, p.id AS package_id,
a.jobs, a.manage, c.renewal, p.validity,
(SELECT COUNT(*) FROM bookings WHERE subscription_id = s.id AND !ISNULL(responded_date) AND free_quota = 0 ) AS used
FROM categories c 
JOIN skills s  ON (s.id  = c.skill_id)
LEFT JOIN subscriptions a ON (a.category_id = c.id AND a.begin_date <= NOW() AND a.exp_date >= NOW() and isnull(a.deleted) and a.active = 1 and a.package_id = c.package_id)
LEFT JOIN packages p ON (p.id = c.package_id)
WHERE c.profile_id = ".$this->data['id']." and isnull(c.deleted) order by c.id");
		
		return $out;
		
		
	}

		/**
	* Get associated talent_schedules
	* Returns array of talent_schedule objects or empty array 
	*/
	
	public function getSchedule () {
		
		$data = $this->db->query("SELECT * FROM talent_schedules WHERE profile_id = ".$this->data['id'], NULL, 1);

		if(!empty($data)){
			
			$obj = new talentSchedule();
			$obj->populate($data);
			return $obj;
			
		}
		else {
			// create schedule
			$schedule = new talentSchedule();
			$schedule->talent_id = $this->data['id'];
			$schedule->save();
			
			return $schedule;	
		}
	}
	
	/****
	* Add a tag to profile
	**/
	function addTag ($tag){
		
		$tag = $this->db->escape(trim($tag));
		
		$tagid = App::getTagID($tag);
		if(empty($tagid)){
			return false;
		}
		
		$ex = $this->db->query("SELECT id FROM profile_tags WHERE tag_id = '{$tagid}' AND profile_id = {$this->data['id']} and isnull(deleted)", null, 1);
		
		if(empty($ex)){

			$pt = new ProfileTag();
			$pt->profile_id = $this->data['id'];
			$pt->tag_id = $tagid;
			$pt->save();
			
			return true;
		}
		
		return false;
	}
	
	/***
	* Remove a tag from profile
	*/
	function removeTag ($tag){
		
		
		$tag = $this->db->escape($tag);
		
		$tagid = App::getTagID($tag);
		if(empty($tagid)){
			return false;
		}
		
		$ex = $this->db->query("SELECT * FROM profile_tags WHERE tag_id = '{$tagid}' AND profile_id = {$this->data['id']} and isnull(deleted)", null, 1);
		
		if(!empty($ex)){

			$pt = new ProfileTag();
			$pt->populate($ex);
			$pt->delete();
			
			return true;
		}
		
		return false;
	}
	


	/**
	* Get associated agenda_shares
	* Returns array of agenda_share objects or empty array 
	*/

	public function getAgendaShares (){

		$data = $this->db->query("SELECT * FROM agenda_shares WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agendaShare();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	
	
	
	/**
	* get posible tags according to their subscriptions
	*/
	public function getPossibleTags (){
		
		$data = $this->db->query("SELECT t.id, t.tag
FROM categories c 
JOIN skill_tags s ON (s.skill_id = c.skill_id AND ISNULL(s.deleted))
JOIN tags t ON (t.id = s.tag_id)
WHERE c.profile_id = ".$this->data['id']." AND ISNULL(c.deleted) AND ISNULL(t.deleted)");
		
		if(!empty($data)){
			return Util::groupArray($data, 'id', 'tag');
		}
	}
	
		
	/**
	* get package status 
	*/
	public function packageStatus () {
		
		return $this->db->query("SELECT * FROM profile_packages WHERE profile_id = ".$this->data['id'], NULL, 1);
		
	}





	/**
	* Get associated booking_trails
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrails (){

		$data = $this->db->query("SELECT * FROM booking_trails WHERE profile_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
