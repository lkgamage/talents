<?php

/**********************************************
* Accesslog Class
**********************************************/

class Accesslog extends Model{

	public $table = 'accesslogs';

	public $fields = array(
		'id',
		'controller',
		'method',
		'parameter',
		'exec_time',
		'memory',
		'peak',
		'num_queries',
		'country',
		'created',
		'modified',
		'deleted'
	);

}
?>
