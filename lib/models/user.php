<?php
		
/****************************
* User Class
*****************************/

class User extends Model{

	public $table = 'users';

	public $fields = array(
			'id',
			'name',
			'position',
			'username',
			'password',
			'last_login',
			'active',
			'status',
			'level',
			'created',
			'modified',
			'deleted'
		);
	



	/**
	* Get associated customer_cares
	* Returns array of customer_care objects or empty array 
	*/

	public function getCustomerCares (){

		$data = $this->db->query("SELECT * FROM customer_cares WHERE user_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new customerCare();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated leads
	* Returns array of lead objects or empty array 
	*/

	public function getLeads (){

		$data = $this->db->query("SELECT * FROM leads WHERE user_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new lead();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated registrations
	* Returns array of registration objects or empty array 
	*/

	public function getRegistrations (){

		$data = $this->db->query("SELECT * FROM registrations WHERE user_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new registration();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}

	
}

?>