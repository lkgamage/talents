<?php

/**********************************************
* SearchLog Class
**********************************************/

class SearchLog extends Model{

	public $table = 'search_logs';

	public $fields = array(
		'id',
		'customer_id',
		'search',
		'country',
		'region',
		'city',
		'category',
		'agency',
		'created',
		'modified',
		'deleted'
	);

}
?>
