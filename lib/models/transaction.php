<?php
		
/****************************
* Transaction Class
*****************************/

class Transaction extends Model{

	public $table = 'transactions';

	public $fields = array(
			'id',
			'talent_id',
			'agency_id',
			'booking_id',
			'withdrawal_id',
			'total_fees',
			'percentage',
			'amount',
			'created',
			'modified',
			'deleted'
		);
	


}

?>