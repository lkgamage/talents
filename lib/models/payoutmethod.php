<?php

/**********************************************
* PayoutMethod Class
**********************************************/

class PayoutMethod extends Model{

	public $table = 'payout_methods';

	public $fields = array(
		'id',
		'name',
		'customer_id',
		'method',
		'location_id',
		'bank_name',
		'bank_address',
		'bank_location_id',
		'swift_code',
		'account_number',
		'account_name',
		'verified',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated payout_requests
	* Returns array of payout_request objects or empty array 
	*/

	public function getPayoutRequests (){

		$data = $this->db->query("SELECT * FROM payout_requests WHERE payout_method_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payoutRequest();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated location
	* Returns location object or NULL 
	*/

	public function getLocation (){

		$data = $this->db->query("SELECT * FROM locations WHERE id = ".$this->data['location_id'], NULL, 1);

		if(!empty($data)){
			$obj = new location();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
