<?php
		
/****************************
* Agenda Managemnt Class
*****************************/

class Agenda extends Model{
	
	
	public $event_id;
	
	public $begins;
	
	
	public $ends;
	
	
	public $items;
	
	private $slotwidth = 60;
	
	/** timeline has a slot for each minute **/
	public $timeline;
	
		
	
	 function __construct ($event_id, $from, $to) {

        parent::__construct();		
		
		$this->event_id = $event_id;
		$this->begins = $from;
		$this->ends = $to;
		
		
		$this->items = $this->db->query("SELECT *, TIMESTAMPDIFF(MINUTE, begins, ends) as width FROM event_items WHERE event_id = ".$this->event_id." and isnull(deleted) order by begins, ends");
		
		if(!empty($this->items)) {
			
			$items = Util::groupArray($this->items, 'id');
			
			$bookings = App::getAgendaBookings(array_keys($items));
			
			if(!empty($bookings)){
				
				$bookings = Util::groupMultiArray($bookings, 'item_id');
				
				foreach ($this->items as $i => $item){
					
					if(isset($bookings[$item['id']])){
						$this->items[$i]['bookings'] = $bookings[$item['id']];
					}
						
				}
				
			}
			
		}
		
		
		
		//Util::debug($this->items);
		
		$this->makeTimeline();
		
    }
	
	private function makeTimeline() {
		
		$ev_begin = strtotime($this->begins);
		$ev_ends = strtotime($this->ends);
		
		$delta = $ev_ends - $ev_begin;
		$slots = ceil($delta/$this->slotwidth);
		
		$this->timeline = array();
		
		for($s = 0; $s <= $slots; $s++){
		
			$this->timeline[$s] = array();
			
		}
		
		// adding items
		foreach ($this->items as $i => $item){
			
			$begin = floor((strtotime($item['begins']) - $ev_begin)/$this->slotwidth);
			$ends =  ceil((strtotime($item['ends']) - $ev_begin)/$this->slotwidth);
			for($a = $begin; $a < $ends; $a++){
				$this->timeline[$a][] = $i;	
			}			
		}
		
	}
	
	
	
	/**  Get event agenda **/
	public function getSchedule () {
	
		$displaing = array();
		$schedule = array();
		
		$ev_begin = strtotime($this->begins);
		
		$empty = 0;
		
		foreach ($this->timeline as $s => $slots){
			
			//echo $s.'  '.$empty.'<br>';
		
			if(!empty($slots)){
				
				if($empty > 0){
					// terminating empty slot
					$schedule[] = array(
							'begins' => date("Y-m-d H:i:s", $ev_begin+($s-$empty)*$this->slotwidth),
							'ends' => date("Y-m-d H:i:s", $ev_begin+($s)*$this->slotwidth),
							'width' => $empty
						);
						
					$empty = 0;	
				}
						
				
				foreach ($slots as $p){
					
					if(!isset($displaing[$p])){
						$displaing[$p] = true;
						$schedule[] = $this->items[$p];
					}
				}
				
				
			}
			else{
								
				
				
				if($s == count($this->timeline)-1){
					$schedule[] = array(
							'begins' => date("Y-m-d H:i:s", $ev_begin+($s-$empty)*$this->slotwidth),
							'ends' => date("Y-m-d H:i:s", $ev_begin+($s)*$this->slotwidth),
							'width' => $empty
						);
				}
				$empty++;
			}
			
			
		}
		
		return $schedule;
		
	}
	
	
	
	
	
	
	
}

?>