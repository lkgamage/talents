<?php

/**********************************************
* Subscription Class
**********************************************/

class Subscription extends Model{

	public $table = 'subscriptions';

	public $fields = array(
		'id',
		'category_id',
		'package_id',
		'sub_date',
		'begin_date',
		'exp_date',
		'active',
		'jobs',
		'manage',
		'catsub_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	/**
	* Check if has permission to update
	* page can be updated only by customer/talent profile or agency
	*/
	public function hasPermissionTo (){
		
		
		$profile = Session::getProfile();
		
		return (($profile['type'] == 't' && $this->data['talent_id'] == $profile['id']) || ($profile['type'] == 'a' && $this->data['agency_id'] == $profile['id']));
		
		

	}



	/**
	* Get associated skill
	* Returns skill object or NULL 
	*/

	public function getSkill (){

		$data = $this->db->query("SELECT * FROM skills WHERE id = ".$this->data['skill_id'], NULL, 1);

		if(!empty($data)){
			$obj = new skill();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated subscription_payment
	* Returns subscription_payment object or NULL 
	*/

	public function getSubscriptionPayment (){

		$data = $this->db->query("SELECT * FROM subscription_payments WHERE id = ".$this->data['payment_id'], NULL, 1);

		if(!empty($data)){
			$obj = new subscriptionPayment();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated auto_reneval
	* Returns auto_reneval object or NULL 
	*/

	public function getAutoReneval (){
		
		if(empty($this->data['reneval_id'])){
			return false;	
		}

		$data = $this->db->query("SELECT * FROM auto_renevals WHERE id = ".$this->data['reneval_id'], NULL, 1);

		if(!empty($data)){
			$obj = new autoReneval();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated package
	* Returns package object or NULL 
	*/

	public function getPackage (){

		$data = $this->db->query("SELECT * FROM packages WHERE id = ".$this->data['package_id'], NULL, 1);

		if(!empty($data)){
			$obj = new package();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated payments
	* Returns array of payment objects or empty array 
	*/

	public function getPayments (){

		$data = $this->db->query("SELECT * FROM payments WHERE subscription_id = ".$this->data['payment_id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get previous payments information
	*/
	public function getPreviousPayments () {
		
	}
	
	
	public function getinfo () {
		
		$data = $this->db->query("SELECT s.id AS subscription__id, s.talent_id AS subscription__talent_id, s.agency_id AS subscription__agency_id, s.skill_id AS subscription__skill_id, s.package_id AS subscription__package_id, s.payment_id AS subscription__payment_id, s.reneval_id AS subscription__reneval_id, s.term AS subscription__term, s.sub_date AS subscription__sub_date, s.begin_date AS subscription__begin_date, s.exp_date AS subscription__exp_date, s.active AS subscription__active, s.fees as subscription__fees, s.jobs as subscription__jobs, s.manage as subscription__manage, s.simultaneous as subscription__simultaneous, s.created AS subscription__created, s.modified AS subscription__modified, s.deleted AS subscription__deleted,
p.id AS package__id, p.skill_id AS package__skill_id, p.name AS package__name, p.description AS package__description, p.price_monthly AS package__price_monthly, p.price_annualy AS package__price_annualy, p.jobs AS package__jobs, p.begin_date AS package__begin_date, p.end_date AS package__end_date, p.active AS package__active, p.display_order AS package__display_order, p.selected AS package__selected, p.created AS package__created, p.modified AS package__modified, p.deleted AS package__deleted,
k.id AS skill__id, k.parent_id AS skill__parent_id, k.name AS skill__name, k.genre AS skill__genre, k.gender_required AS skill__gender_required, k.is_default AS skill__is_default, k.created AS skill__created, k.modified AS skill__modified, k.deleted AS skill__deleted,
r.id AS reneval__id, r.talent_id AS reneval__talent_id, r.package_id AS reneval__package_id, r.term AS reneval__term, r.method_id AS reneval__method_id, r.next_reneval AS reneval__next_reneval, r.active AS reneval__active, r.created AS reneval__created, r.modified AS reneval__modified, r.deleted AS reneval__deleted,
rm.id AS reneval_method__id, rm.customer_id AS reneval_method__customer_id, rm.type AS reneval_method__type, rm.name AS reneval_method__name, rm.card_name AS reneval_method__card_name, rm.card_number AS reneval_method__card_number, rm.card_exp AS reneval_method__card_exp, rm.ccvv AS reneval_method__ccvv, rm.token AS reneval_method__token, rm.reference AS reneval_method__reference, rm.created AS reneval_method__created, rm.modified AS reneval_method__modified, rm.deleted AS reneval_method__deleted,
py.id AS payment__id, py.invoice_id AS payment__invoice_id, py.subscription_id AS payment__subscription_id, py.method AS payment__method, py.amount AS payment__amount, py.currency AS payment__currency, py.card_name AS payment__card_name, py.card_type AS payment__card_type, py.card_num AS payment__card_num, py.card_exp AS payment__card_exp, py.card_cvv AS payment__card_cvv, py.payment_id AS payment__payment_id, py.transaction_id AS payment__transaction_id, py.bank_id AS payment__bank_id, py.ip_address AS payment__ip_address, py.status AS payment__status, py.success AS payment__success, py.released AS payment__released, py.created AS payment__created, py.modified AS payment__modified, py.deleted AS payment__deleted,
(SELECT COUNT(*) FROM bookings WHERE DATE(responded_date) >= s.begin_date && DATE(responded_date) <= s.exp_date and (talent_id = s.talent_id || agency_id = s.agency_id)) as usage__jobs,
(SELECT COUNT(*) FROM  agency_request WHERE STATUS = '".DataType::$AGENCY_REQEST_ACCEPTED."' AND active = 1 and agency_id = s.agency_id) as usage__manage
FROM subscriptions s 
JOIN packages p ON ( p.id = s.package_id)
JOIN skills k ON ( k.id = p.skill_id)
LEFT JOIN payments py ON ( py.id = s.payment_id)
LEFT JOIN auto_renevals r ON ( r.id = s.reneval_id)
LEFT JOIN payment_methods rm ON (rm.id = r.method_id)
WHERE s.id = ".$this->data['id']."", NULL, 1);

		
		
		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		return $out;
	}

	/**
	* Get payment history up to 4 years
	*/
	public function getPaymentHistory () {
		
		return $this->db->query("SELECT s.id, p.amount, m.name, p.created  FROM payments p
JOIN subscriptions s ON (s.payment_id = p.id)
JOIN auto_renevals r ON (r.id = s.reneval_id)
JOIN payment_methods m ON (m.id = r.method_id)
WHERE r.id = ".$this->data['reneval_id']." AND p.success = 1 AND s.id != ".$this->data['id']." order by p.created desc", NULL, 24);
		
	}
	
	
	/**
	* Get remiang balnace for rest of the month
	*  return array(
	*				'days' => 0,
	*				'jobs' => 0,
	*				'balanace' => 0					
	*			);
	*/
	public function getBalance (){
		
		$data = $this->db->query("SELECT s.id,  s.jobs, s.manage, s.begin_date, s.exp_date, i.amount, DATEDIFF(s.exp_date, s.begin_date) AS total_days, DATEDIFF(s.exp_date, NOW()) AS remain_days,
(SELECT COUNT(*) FROM bookings WHERE subscription_id = s.id AND !ISNULL(responded_date) AND free_quota = 0 ) AS used_jobs
FROM subscriptions s
JOIN invoices i ON (i.subscription_id = s.id)
WHERE s.exp_date > NOW() AND s.id =".$this->data['id'], NULL, 1);

		if(empty($data)){
			return array(
					'days' => 0,
					'jobs' => 0,
					'balance' => 0					
				);	
		}
		
				
		// based on number of used jobs and days
		$price = $data['amount'];
		$job = $data['jobs'];
		$days = $data['total_days'];
		
		$xjobs = $data['used_jobs'];
		$xdays = $data['total_days']-$data['remain_days'];
		
		if(empty($xdays)){
			$xdays = 1;
		}
				
		if(empty($xjobs)){
			$xjobs = 1;
		}
		
		$rmdays = ($job-$xjobs)/($xjobs/$xdays);

		
		if($days < $rmdays+$xdays){
			$rmdays = ($days - $xdays) > 0 ? ($days - $xdays) : ($days - 1)  	;
		}

		// processing fee 20%
		$balance = (($price*0.8)/($rmdays+$xdays))*$rmdays;
		
		if($balance < 0){
			$balance = 0;	
		}
		
		return array(
					'days' => $data['remain_days'],
					'jobs' => $data['jobs'] - $data['used_jobs'],
					'balance' => $balance > 0 ? $balance : 0				
				);	
	}


	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated category
	* Returns category object or NULL 
	*/

	public function getCategory (){

		$data = $this->db->query("SELECT * FROM categories WHERE id = ".$this->data['category_id'], NULL, 1);

		if(!empty($data)){
			$obj = new category();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated bookings
	* Returns array of booking objects or empty array 
	*/

	public function getBookings (){

		$data = $this->db->query("SELECT * FROM bookings WHERE subscription_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new booking();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated invoices
	* Returns array of invoice objects or empty array 
	*/

	public function getInvoice (){

		$data = $this->db->query("SELECT * FROM invoices WHERE subscription_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL, 1);

		$out = array();

		if(!empty($data)){
			
			$obj = new invoice();
			$obj->populate($data);
			return $obj;
		}


		return $out;
	}



	/**
	* Get associated invoices
	* Returns array of invoice objects or empty array 
	*/

	public function getInvoices (){

		$data = $this->db->query("SELECT * FROM invoices WHERE subscription_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new invoice();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated category_subscription
	* Returns category_subscription object or NULL 
	*/

	public function getCategorySubscription (){
		
		if(!empty($this->data['catsub_id'])) {

			$data = $this->db->query("SELECT * FROM category_subscriptions WHERE id = ".$this->data['catsub_id'], NULL, 1);

			if(!empty($data)){
				$obj = new categorySubscription();
				$obj->populate($data);
				return $obj;;
			}
			
		}


		return NULL;
	}


}
?>
