<?php
		
/****************************
* Match Class
*****************************/

class Match extends Model{

	public $table = 'matches';

	public $fields = array(
			'id',
			'course_id',
			'start_hole',
			'group1',
			'group2',
			'side',
			'created'
		);
	


}

?>