<?php
		
/****************************
* Event Class
*****************************/

class Event extends Model{

	public $table = 'events';

	public $fields = array(
		'id',
		'customer_id',
		'agency_id',
		'event_type',
		'name',
		'description',
		'begins',
		'ends',
		'charitable',
		'location_id',
		'num_attendees',
		'budget',
		'agenda',
		'created',
		'modified',
		'deleted'
		);
	



	/**
	* Get associated event_type
	* Returns event_type object or NULL 
	*/

	public function getEventType (){

		$data = $this->db->query("SELECT * FROM event_types WHERE id = ".$this->data['event_type'], NULL, 1);

		if(!empty($data)){
			$obj = new eventType();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated event_talents
	* Returns array of event_talent objects or empty array 
	*/

	public function getEventTalents (){

		$data = $this->db->query("SELECT * FROM event_talents WHERE event_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new eventTalent();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated location
	* Returns location object or NULL 
	*/

	public function getLocation (){

		$data = $this->db->query("SELECT * FROM locations WHERE id = ".$this->data['location_id'], NULL, 1);

		if(!empty($data)){
			$obj = new location();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated bookings
	* Returns array of booking objects or empty array 
	*/

	public function getBookings (){

		$data = $this->db->query("SELECT * FROM bookings WHERE event_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new booking();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	/**a.description
	* Get event details
	*/
	public function getinfo () {
		
		$data = $this->db->query("SELECT e.id AS event__id,
 e.customer_id AS event__customer_id,
 e.agency_id AS event__agency_id,
 e.event_type AS event__event_type,
 e.name AS event__name,
 e.description AS event__description,
 e.begins AS event__begins,
 e.ends AS event__ends,
 e.charitable AS event__charitable,
 l.venue AS location__venue,
 e.location_id AS event__location_id,
 e.budget AS event__budget,
 e.num_attendees AS event__attendees,
 e.created AS event__created,
 e.modified AS event__modified,
 e.deleted AS event__deleted, 
a.id AS agency__id,
 a.customer_id AS agency__customer_id,
 a.name AS agency__name,
 a.agency_type AS agency__agency_type,
 a.verified AS agency__verified,
   a.location_id AS agency__location_id,
 a.timezone AS agency__timezone,
 a.timeoffset AS agency__timeoffset,
  a.image_id AS agency__image_id,
 a.active AS agency__active,
 a.name_changed AS agency__name_changed,
 a.created AS agency__created,
 a.modified AS agency__modified,
 a.deleted AS agency__deleted,
t.id AS type__id,
 t.name AS type__name,
 t.corporate AS type__corporate,
 t.created AS type__created,
 t.modified AS type__modified,
 t.deleted AS type__deleted,
l.id AS location__id,
 l.address AS location__address,
 l.city AS location__city,
 l.region AS location__region,
 l.postalcode AS location__postalcode,
 l.country AS location__country,
 l.locked AS location__locked,
 l.created AS location__created,
 l.modified AS location__modified,
 l.deleted AS location__deleted
FROM EVENTS e 
LEFT JOIN profiles a ON ( a.id = e.agency_id)
LEFT JOIN event_types t ON ( t.id = e.event_type)
LEFT JOIN locations l ON ( l.id = e.location_id)
WHERE e.id =".$this->data['id'], NULL, 1);

		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		return $out;

	}



	/**
	* Get associated appoinments
	* Returns array of appoinment objects or empty array 
	*/

	public function getAppoinments (){
		
		return $this->db->query("SELECT a.*, t.name AS talent_name, g.name AS agency_name, ti.i400 as talent_image, tp.handle as talent_handle, t.gender as talent_gender, ai.i400 as agency_image, ap.handle as agency_handle
FROM 
appoinments a
JOIN profiles t ON (t.id = a.profile_id)
LEFT JOIN images ti ON (ti.id = t.image_id)
LEFT JOIN pages tp ON (tp.profile_id = t.id)
LEFT JOIN profiles g ON (g.id = a.agency_id)
LEFT JOIN images ai ON (ai.id = g.image_id)
LEFT JOIN pages ap ON (ap.profile_id = g.id)
WHERE a.event_id = ".$this->data['id']."  AND ISNULL(a.deleted) AND (ISNULL(a.expired) || a.expired > NOW()) ORDER BY a.begins, a.id");

/*

		$data = $this->db->query("SELECT * FROM appoinments WHERE event_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new appoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;*/
	}
	
	
	/**
	* Get brief information about the event
	*/
	public function brief () {
	
	
		return $this->db->query("SELECT e.*, DATE(e.begins) as event_date, l.address, l.city, l.region, l.postalcode, l.country, t.name AS type_name, a.name AS agency_name,
		(SELECT COUNT(*) FROM bookings WHERE event_id = e.id AND ISNULL(deleted)) AS bookings
FROM `events` e
JOIN locations l ON (l.id = e.location_id)
JOIN event_types t ON (t.id = e.event_type)
LEFT JOIN profiles a ON (a.id = e.agency_id) 
WHERE e.id = ".$this->data['id']." and isnull(e.deleted) ");
	}



	/**
	* Get associated message_boards
	* Returns array of message_board objects or empty array 
	*/

	public function getMessageBoard (){

		$data = $this->db->query("SELECT * FROM message_boards WHERE event_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){		
			
				$obj = new MessageBoard();
				$obj->populate($data);
				return $obj;
		}


		return false;
	}
	
	
	/**
	* get event partners
	* Get all customer, agency and talent data involved in to the event
	*/
	public function getPartners () {
	
		// get creater and bookings made for the  event
		$bdata = $this->db->query("SELECT b.customer_id, t.customer_id AS talent_customer, t.id AS talent_id, a.customer_id AS agency_customer, a.id AS agency_id
FROM bookings b
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
WHERE event_id = ".$this->data['id']." AND ISNULL(b.deleted) AND b.status != '".DataType::$BOOKING_REJECT."'");
		
		// get appointmnets made for the event
		$edata = $this->db->query("SELECT  t.customer_id AS talent_customer, t.id AS talent_id, a.customer_id AS agency_customer, a.id AS agency_id
FROM appoinments b
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
WHERE event_id = ".$this->data['id']." AND ISNULL(b.deleted)");

		$data = array_merge($bdata, $edata);
		
		$out = array();
		// event creator
		$out[$this->data['customer_id']] = array(
					'customer_id' => $this->data['customer_id'],
					'talent_id' => NULL,
					'agency_id' => NULL
				);
		
		foreach ($data as $item){
			
			if(!empty($item['talent_customer'])){
				
				$out[$item['talent_customer']] = array(
					'customer_id' => $item['talent_customer'],
					'talent_id' => $item['talent_id'],
					'agency_id' => NULL
				);
			}
			
			if(!empty($item['agency_customer'])){
				
				$out[$item['agency_customer']] = array(
					'customer_id' => $item['agency_customer'],
					'talent_id' => NULL,
					'agency_id' => $item['agency_id']
				);
			}
			
			if(empty($item['talent_customer']) || empty($item['agency_customer'])){
				
				$out[$item['customer_id']] = array(
					'customer_id' => $item['customer_id'],
					'talent_id' => NULL,
					'agency_id' => NULL
				);
			}
			
			
		}		
		
		return $out;

	}
	
	



	/**
	* Get associated message_boards
	* Returns array of message_board objects or empty array 
	*/

	public function getMessageBoards (){

		$data = $this->db->query("SELECT * FROM message_boards WHERE event_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new messageBoard();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated expences
	* Returns array of expencs or empty array 
	*/

	public function getExpences (){
		
		$bookings = $this->db->query("SELECT b.id, b.talent_fee AS amount, p.name AS description, IF(!ISNULL(b.talent_id), t.name, a.name) AS vendor, b.discount, s.name as skill, 'package' as type
FROM bookings b
JOIN talent_packages p ON (p.id = b.package_id)
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
LEFT join categories c on (c.id = p.category_id)
LEFT JOIN skills s on (s.id = c.skill_id)
WHERE b.event_id = ".$this->data['id']." and b.status not in ('".DataType::$BOOKING_CANCEL."','".DataType::$BOOKING_REJECT."','". DataType::$BOOKING_EXPIRED."') and isnull(b.deleted)");
				

		$expences = $this->db->query("SELECT e.id, e.amount, e.description, expence_type, if(!isnull(e.booking_id), e.booking_id, 0) as booking_id, 'expence' as type
FROM expences e 
LEFT JOIN bookings b ON (b.id = e.booking_id)
WHERE (e.event_id = ".$this->data['id']." OR b.event_id = ".$this->data['id'].") and isnull(e.deleted)", NULL);
	
		$out = array();

		if(!empty($expences)){
			$expences = Util::groupMultiArray($expences, 'booking_id');	
		}
		
		
		
		foreach ($bookings as $b){
		
			if(isset($expences[$b['id']])){
				$b['expences'] = $expences[$b['id']];	
			}
		
			$out[] = $b;			
		}
		
		if(!empty($expences[0])){
			$out = array_merge($out, $expences[0]);	
		}

		return $out;
	}



	/**
	* Get associated event_items
	* Returns array of event_item objects or empty array 
	*/

	public function getEventItems (){

		$data = $this->db->query("SELECT * FROM event_items WHERE event_id = ".$this->data['id']." and isnull(deleted) order by begins, ends", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new eventItem();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated agenda_shares
	* Returns array of agenda_share objects or empty array 
	*/

	public function getAgendaShares (){

		return $this->db->query("SELECT s.id, s.customer_id,  t.id AS talent_id ,  t.customer_id AS talent_customer_id,  t.name AS talent_name, i.i400 AS talent_image, t.gender AS talent_gender

FROM agenda_shares s
LEFT JOIN profiles t ON (t.id = s.profile_id)
LEFT JOIN images i ON (i.id = t.image_id)
WHERE s.event_id = ".$this->data['id']." and isnull(s.deleted)", NULL);

		
	}
	
	/**
	* Update agenda sharing settings
	*/
	public function updateAgendaShare ($bookings) {
		
		if(!empty($bookings)){
			
			foreach ($bookings as $i => $b){
				
				if(!is_numeric($b)){
					unset($bookings[$i]);	
				}					
			}
			
			if(empty($bookings)){
				return false;	
			}
			
			$existing = $this->db->query("SELECT id, customer_id,agency_id,talent_id  FROM agenda_shares WHERE event_id = ".$this->data['id']." AND ISNULL(deleted)");
			
			$bookings = $this->db->query("SELECT b.id,  t.id AS talent_id ,  t.customer_id AS talent_customer_id,  t.name AS talent_name, i.i400 AS talent_image, t.gender AS talent_gender,
a.id AS agency_id, a.name AS agency_name, a.customer_id AS agency_customer_id, ai.i400 AS agency_image
FROM bookings b
LEFT JOIN profiles t ON (t.id = b.talent_id)
LEFT JOIN images i ON (i.id = t.image_id)
LEFT JOIN profiles a ON (a.id = b.agency_id)
LEFT JOIN images ai ON (ai.id = a.image_id)
WHERE  b.id IN (".implode(',', $bookings).")");

			$contacts = array();
			
			if(!empty($bookings)) {
			
				foreach ($bookings as $b){
					
					if(!empty($b['talent_id'])){
						$contacts[$b['talent_customer_id']] = array('talent_id' => $b['talent_id']);
					}
					elseif(!empty($b['agency_id'])){
						$contacts[$b['agency_customer_id']] = array('agency_id' => $b['agency_id']);
					}
				}
			}
			
			if(!empty($existing)){
				$existing = Util::groupArray($existing, 'customer_id');				
			}
			
			$creates = array_diff(array_keys($contacts), array_keys($existing)); 
			$deletes = array_diff(array_keys($existing), array_keys($contacts));
			
			if(!empty($deletes)){
				
				foreach ($deletes as $id) {
					$this->db->execute("UPDATE agenda_shares SET deleted = NOW() WHERE id = ".$existing[$id]['id']);	
				}				
			}
			
			
			if(!empty($creates)) {
				
				foreach ($creates as $id){
					
					$share = new AgendaShare();
					$share->event_id = $this->data['id'];
					$share->customer_id = $id;
					
					if(isset($contacts[$id]['agency_id'])) {
						$share->agency_id = $contacts[$id]['agency_id'];
					}
					
					if(isset($contacts[$id]['talent_id'])) {
						$share->talent_id = $contacts[$id]['talent_id'];
					}
					
					$share->save();	
				}
					
			}
			
			
		}
		else{
			$this->db->execute("UPDATE agenda_shares SET deleted = NOW() WHERE event_id = ".$this->data['id']);
		}
		
		
		
	}
	
	
	/**
	* Get agenda
	*/
	public function getAgenda () {
		
		$agenda = new Agenda($this->data['id'], $this->data['begins'], $this->data['ends']);
		return $agenda;
		return $this->getEventItems();
		
	}


	/**
	* Create an agenda 
	* check existing events and associate them
	*/
	public function createAgenda () {
		
		$this->data['agenda'] = 1;
		$this->save();
		
	}

}

?>