<?php
		
/****************************
* Invoice Class
*****************************/

class Invoice extends Model{

	public $table = 'invoices';
	
	private $infodata;

	public $fields = array(
		'id',
		'type',
		'customer_id',
		'profile_id',
		'subscription_id',
		'amount',
		'currency',
		'effective_date',
		'due_date',
		'status',
		'stripe_id',
		'created',
		'modified',
		'deleted'
		);
	
	
	/**
	* Override update function 
	*/
	public function update () {
		$this->infodata = NULL;
		
		parent::update();
	}

	/**
	* Get related payment record
	* otherwise NULL
	*/
	public function getPayment () {
		
		$data = $this->db->query("select * from payments where invoice_id = ".$this->data['id'], NULL, 1);
	
		if(!empty($data)){
				
			$payment = new Payment();
			$payment->populate($data);
			return $payment;
		}
			
				
		return NULL;
	}


	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Create a new payment object 
	* Returns payment
	*/

	public function createPayment (){
		
		// create new payment and return	
		$payment = new Payment();
		$payment->invoice_id = $this->data['id'];
		$payment->amount = $this->data['amount'];
		$payment->currency = $this->data['currency'];		
		$payment->ip_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
		$payment->status = DataType::$PAYMENT_PROCESSING;
		$payment->success = 0;
		$payment->released = 0;
		$payment->save();
		
		return $payment;
		
	}
	
	



	/**
	* Get associated payments
	* Returns array of payment objects or empty array 
	*/

	public function getPayments (){

		$data = $this->db->query("SELECT * FROM payments WHERE invoice_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated invoice_items
	* Returns array of invoice_item objects or empty array 
	*/

	public function getInvoiceItems (){

		$data = $this->db->query("SELECT * FROM invoice_items WHERE invoice_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new invoiceItem();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Add item
	*/
	public function addItem ($desc, $amount) {
		
		$item = new InvoiceItem();
		$item->invoice_id = $this->data['id'];
		$item->description = substr($desc, 0,400);
		$item->amount = !empty($amount) ? $amount : 0;
		$item->save();
				
		$this->refresh();
	}
	
	/**
	* Clear all invoice items
	*/
	public function clearItems (){
		
		$this->db->execute("update invoice_items set deleted = NOW() where invoice_id = ".$this->data['id']);
		
		$this->data['amount'] = 0;
		$this->save();
		
	}
	
	/**
	* Refresh invoice total
	*/
	public function refresh () {
		applog("Refreshing amount");
		$amount = $this->db->query("SELECT SUM(amount) as total FROM invoice_items WHERE invoice_id = ".$this->data['id']." and isnull(deleted)", NULL, 1);
		$this->data['amount'] = !empty($amount['total']) ? $amount['total'] : 0;
		$this->save();
		
	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated subscription
	* Returns subscription object or NULL 
	*/

	public function getSubscription (){

		$data = $this->db->query("SELECT * FROM subscriptions WHERE id = ".$this->data['subscription_id'], NULL, 1);

		if(!empty($data)){
			$obj = new subscription();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	public function getinfo () {
		
		if(!empty($this->infodata)){
			return $this->infodata;
		}
		
		
		$data = $this->db->query("SELECT i.id AS invoice__id, 
i.type AS invoice__type, 
i.customer_id AS invoice__customer_id, 
i.profile_id AS invoice__profile_id, 
i.subscription_id AS invoice__subscription_id, 
i.amount AS invoice__amount, 
i.currency AS invoice__currency, 
i.due_date AS invoice__due_date, 
i.effective_date AS invoice__effective_date,
i.status AS invoice__status, 
i.created AS invoice__created, 
i.modified AS invoice__modified, 
i.deleted AS invoice__deleted,
p.id AS profile__id, 
p.customer_id AS profile__customer_id, 
p.is_agency AS profile__is_agency, 
p.verified AS profile__verified, 
p.location_id AS profile__location_id, 
p.image_id AS profile__image_id, 
p.agency_type AS profile__agency_type, 
p.gender AS profile__gender, 
p.currency AS profile__currency, 
p.timezone AS profile__timezone, 
p.timeoffset AS profile__timeoffset, 
p.name AS profile__name, 
p.name_changed AS profile__name_changed, 
p.limited_hours AS profile__limited_hours, 
p.active AS profile__active, 
p.commitment AS profile__commitment, 
p.created AS profile__created, 
p.modified AS profile__modified, 
p.deleted AS profile__deleted,
s.id AS subscription__id, 
s.category_id AS subscription__category_id, 
s.package_id AS subscription__package_id, 
s.sub_date AS subscription__sub_date, 
s.begin_date AS subscription__begin_date, 
s.exp_date AS subscription__exp_date, 
s.active AS subscription__active, 
s.jobs AS subscription__jobs, 
s.manage AS subscription__manage, 
s.created AS subscription__created, 
s.modified AS subscription__modified, 
s.deleted AS subscription__deleted,
c.id AS category__id, 
c.profile_id AS category__profile_id, 
c.skill_id AS category__skill_id, 
c.package_id AS category__package_id, 
c.term AS category__term, 
c.renewal AS category__renewal, 
c.renewal_date AS category__renewal_date, 
c.last_renewal AS category__last_renewal,
c.created AS category__created, 
c.modified AS category__modified, 
c.deleted AS category__deleted,
k.id AS skill__id, 
k.category AS skill__category, 
k.parent_id AS skill__parent_id, 
k.name AS skill__name, 
k.genre AS skill__genre, 
k.gender_required AS skill__gender_required, 
k.is_default AS skill__is_default, 
k.is_business AS skill__is_business, 
k.is_agency AS skill__is_agency, 
k.active AS skill__active, 
k.created AS skill__created, 
k.modified AS skill__modified, 
k.deleted AS skill__deleted,
a.id AS package__id, 
a.skill_id AS package__skill_id, 
a.name AS package__name, 
a.description AS package__description, 
a.tagline AS package__tagline, 
a.price AS package__price, 
a.price_shown AS package__price_shown, 
a.validity AS package__validity, 
a.jobs AS package__jobs, 
a.manage AS package__manage, 
a.begin_date AS package__begin_date, 
a.end_date AS package__end_date, 
a.active AS package__active, 
a.display_order AS package__display_order, 
a.selected AS package__selected, 
a.agency_only AS package__agency_only,
a.one_time AS package__one_time,
a.product_id AS package__product_id, 
a.price_id AS package__price_id, 
a.created AS package__created, 
a.modified AS package__modified, 
a.deleted AS package__deleted,
b.id as payment__id, 
b.invoice_id as payment__invoice_id, 
b.method_id as payment__method_id, 
b.amount as payment__amount, 
b.currency as payment__currency, 
b.intent_id as payment__intent_id, 
b.charge_id as payment__charge_id, 
b.status as payment__status, 
b.success as payment__success, 
b.released as payment__released, 
b.created as payment__created, 
b.modified as payment__modified, 
b.deleted as payment__deleted,
u.id as customer__id, 
u.firstname as customer__firstname, 
u.lastname as customer__lastname, 
u.name_changed as customer__name_changed, 
u.dob as customer__dob, 
u.location_id as customer__location_id, 
u.timeoffset as customer__timeoffset, 
u.timezone as customer__timezone, 
u.currency as customer__currency, 
u.image_id as customer__image_id, 
u.active as customer__active, 
u.gender as customer__gender, 
u.is_trans as customer__is_trans, 
u.language as customer__language, 
u.stripe_id as customer__stripe_id, 
u.created as customer__created, 
u.modified as customer__modified, 
u.deleted as customer__deleted,
csa.stripe_id AS stripe__active,
csp.stripe_id AS stripe__pending
FROM invoices i 
JOIN customers u on (u.id = i.customer_id)
LEFT JOIN profiles p ON ( p.id = i.profile_id)
LEFT JOIN subscriptions s ON ( s.id = i.subscription_id)
LEFT JOIN categories c ON ( c.id = s.category_id)
LEFT JOIN skills k ON ( k.id = c.skill_id)
LEFT JOIN packages a ON ( a.id = s.package_id)
LEFT JOIN payments b ON ( b.invoice_id = i.id)
LEFT JOIN category_subscriptions csa ON (csa.category_id = c.id AND csa.active = 1 AND ISNULL(csa.deleted))
LEFT JOIN category_subscriptions csp ON (csp.category_id = c.id AND csp.active = 0 AND ISNULL(csp.deleted))
WHERE i.id = {$this->data['id']}", NULL, 1);
		
		$out = array();
		
		foreach ($data as $key => $value){

			list($a, $b) = explode('__', $key);
			$out[$a][$b] = $value;
		}
		
		
		$out['items'] = $this->db->query("SELECT id, description,amount FROM invoice_items WHERE invoice_id = {$this->data['id']} and isnull(deleted)");
		
		$this->infodata = $out;
		
		return $out;
		
		
	}
	
	/**
	* Get a description about the invoice
	*/
	public function getDesciption () {
		
		
		$data = $this->db->query("SELECT p.name AS package, k.name AS skill, f.currency
FROM subscriptions s 
JOIN packages p ON (p.id = s.package_id)
JOIN skills k ON (k.id = p.skill_id)
JOIN categories c ON (c.id = s.category_id)
JOIN profiles f ON (f.id = c.profile_id)
WHERE s.id = ".$this->data['id'], NULL, 1);
		
		switch($this->data['type']){
			case DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION:
				$data['desc'] = 'Your new subscription '.$data['skill'].' - '.$data['package'].' activated.';
				$data['subject'] = "Subscription activated";
				break;
			case DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION:
				$data['desc'] = 'Your '.$data['skill'].' - '.$data['package'].' subscription renewed.';
				$data['subject'] = "Subscription renewed";
			case DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION:
			case DataType::$INVOICE_TYPE_UP_SUBSCRIPTION:
				$data['desc'] = 'Your subscription plan changed to '.$data['skill'].' - '.$data['package'];
				$data['subject'] = "Subscription plan changed";
			
		}
		
		$data['amount'] = currency($this->amount, $data['currency']);
		
		return $data;
	}
	
	
	/**
	* Get customer contact data
	*/
	public function getContact () {
		
		return $this->db->query("SELECT c.id, firstname, lastname, e.email, p.phone, p.country_code, p.id as phone_id, e.id as email_id, l.city, l.region, l.country, l.postalcode
FROM customers c
JOIN emails e ON (e.customer_id = c.id AND ISNULL(e.deleted))
JOIN phones p ON (p.customer_id = c.id AND ISNULL(p.deleted))
JOIN locations l ON (l.id = c.location_id)
WHERE c.id = ".$this->data['customer_id']." ORDER BY p.is_primary DESC, e.is_primary DESC ", NULL, 1);
	}


}

?>