<?php

/**********************************************
* Like Class
**********************************************/

class Like extends Model{

	public $table = 'likes';

	public $fields = array(
		'id',
		'customer_id',
		'page_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated page
	* Returns page object or NULL 
	*/

	public function getPage (){

		$data = $this->db->query("SELECT * FROM pages WHERE id = ".$this->data['page_id'], NULL, 1);

		if(!empty($data)){
			$obj = new page();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
