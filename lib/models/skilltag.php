<?php

/**********************************************
* SkillTag Class
**********************************************/

class SkillTag extends Model{

	public $table = 'skill_tags';

	public $fields = array(
		'id',
		'skill_id',
		'tag_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated tag
	* Returns tag object or NULL 
	*/

	public function getTag (){

		$data = $this->db->query("SELECT * FROM tags WHERE id = ".$this->data['tag_id'], NULL, 1);

		if(!empty($data)){
			$obj = new tag();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated skill
	* Returns skill object or NULL 
	*/

	public function getSkill (){

		$data = $this->db->query("SELECT * FROM skills WHERE id = ".$this->data['skill_id'], NULL, 1);

		if(!empty($data)){
			$obj = new skill();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
