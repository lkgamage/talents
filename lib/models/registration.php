<?php

/**********************************************
* Registration Class
**********************************************/

class Registration extends Model{

	public $table = 'registrations';

	public $fields = array(
		'id',
		'customer_id',
		'user_id',
		'marketer_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated user
	* Returns user object or NULL 
	*/

	public function getUser (){

		$data = $this->db->query("SELECT * FROM users WHERE id = ".$this->data['user_id'], NULL, 1);

		if(!empty($data)){
			$obj = new user();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated marketter
	* Returns marketter object or NULL 
	*/

	public function getMarketter (){

		$data = $this->db->query("SELECT * FROM marketters WHERE id = ".$this->data['marketer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new marketter();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
