<?php

/**********************************************
* Otus Class
**********************************************/

class Otus extends Model{

	public $table = 'otus';

	public $fields = array(
		'id',
		'booking_id',
		'url',
		'data',
		'visited',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
