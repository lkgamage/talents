<?php
		
/****************************
* Post Class
*****************************/

class Post extends Model{

	public $table = 'posts';

	public $fields = array(
			'id',
			'talent_id',
			'display_order',
			'caption',
			'story',
			'layout',
			'active',
			'created',
			'modified',
			'deleted'
		);
	
	
	/**
	* Reset post images
	*/
	public function resetImages ($ids){
		
		$this->db->execute("update post_images set deleted = NOW() where post_id = ".$this->data['id']." and image_id not in (".implode(",", $ids).")");
		
		foreach ($ids as $a => $b){
			$this->db->execute("update post_images set display_order = ".$a." where image_id = ".$b);	
		}
		
	}
	
	public function getData () {
		
		$images = $this->db->query("select i.id, i.i600, i.i400, i.url, p.post_id, i.orientation  from images i join post_images p on (p.image_id = i.id) where p.post_id = ".$this->data['id']." && isnull(p.deleted) && isnull(i.deleted) order by display_order");
		
		$data = array(
			'id' => $this->data['id'],
			'caption' => $this->data['caption'],
			'story' => $this->data['story'],
			'layout' => $this->data['layout'],
			'created' => $this->data['created'],
			'images' => $images
		);
		
		return $data;
	}



	/**
	* Get associated post_images
	* Returns array of post_image objects or empty array 
	*/

	public function getPostImages (){

		$data = $this->db->query("SELECT * FROM post_images WHERE post_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new postImage();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}

?>