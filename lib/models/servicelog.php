<?php

/**********************************************
* ServiceLog Class
**********************************************/

class ServiceLog extends Model{

	public $table = 'service_log';

	public $fields = array(
		'id',
		'service',
		'type',
		'code',
		'created',
		'modified',
		'deleted'
	);

}
?>
