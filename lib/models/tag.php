<?php

/**********************************************
* Tag Class
**********************************************/

class Tag extends Model{

	public $table = 'tags';

	public $fields = array(
		'id',
		'tag',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated profile_tags
	* Returns array of profile_tag objects or empty array 
	*/

	public function getProfileTags (){

		$data = $this->db->query("SELECT * FROM profile_tags WHERE tag_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new profileTag();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated skill
	* Returns skill object or NULL 
	*/

	public function getSkill (){

		$data = $this->db->query("SELECT * FROM skills WHERE id = ".$this->data['skill_id'], NULL, 1);

		if(!empty($data)){
			$obj = new skill();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated skill_tags
	* Returns array of skill_tag objects or empty array 
	*/

	public function getSkillTags (){

		$data = $this->db->query("SELECT * FROM skill_tags WHERE tag_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new skillTag();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
