<?php

/**********************************************
* BookingAttempt Class
**********************************************/

class BookingAttempt extends Model{

	public $table = 'booking_attempts';

	public $fields = array(
		'id',
		'customer_id',
		'agency_id',
		'location_id',
		'event_id',
		'booking_date',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated attempt_talents
	* Returns array of attempt_talent objects or empty array 
	*/

	public function getAttemptTalents (){

		$data = $this->db->query("SELECT * FROM attempt_talents WHERE attempt_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new attemptTalent();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
