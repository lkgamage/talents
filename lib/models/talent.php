<?php
		
/****************************
* Talent Class
*****************************/

class Talent extends Model{

	public $table = 'talents';

	public $fields = array(
		'id',
		'customer_id',
		'verified',
		'name',
		'location_id',
		'timeoffset',
		'timezone',
		'currency',
		'summary',
		'gender',
		'is_trans',
		'image_id',
		'agency_fee_type',
		'agency_fee',
		'limited_hours',
		'work_start',
		'work_end',
		'simultaneous_bookings',
		'active',
		'name_changed',
		'created',
		'modified',
		'deleted'
		);
	
	
	/**
	* general function to object check type
	*/
	public function isAgency () {
		return false;	
	}
	
	public function isTalent () {
		return true;	
	}
	
	/**
	* Get display picture
	*/
	public function getDP () {
	
		if(!empty($this->data['image_id'])){
			return new Image($this->data['image_id']);
		}
		
		return NULL;
	}
	
	
	/**
	* Check if has permission to update
	* page can be updated only by customer/talent profile or agency
	*/
	public function hasPermissionTo ($customer_id){
		
		
		if(!empty($this->data['customer_id']) && $this->data['customer_id'] == $customer_id){			
			return true;	
		}
		
		if(!empty($this->data['agency_id'])){
			
			$agency = $this->getAgency();
			if($agency->customer_id == $customer_id){
				
				return true;
				
			}
		}
		
		
		return false;
	}

	/**
	* Get logins
	*/
	public function getLogins ( $type = NULL){
	
		if(!empty($type)){
			$data = $this->db->query("select * from logins where talent_id = ".$this->data['id']." ");	
		}
		else{
			$data = $this->db->query("select * from logins where talent_id = ".$this->data['id']." and type = :type ", array('type' => $type));	
		}
		
		$out = array();
		
		if(!empty($data)){
			
			foreach ($data as $a){
				$l = new Login();
				$l->populate($a);
				$out[] = $l;	
			}
		}
		
		
		return $out;
	}
	
	/**
	* Get Talent packages
	* return arry of package objects
	*/
	public function getPackages ($as_array = false) {
		
		if($as_array){
			
			return $this->db->query("SELECT t.id, t.skill_id, s.name as skill_name, t.name, t.description, t.timespan, t.interval, t.fee, t.fee_local, t.items, t.active, t.deleted 
			FROM talent_packages t 
			join skills s on (s.id = t.skill_id) 
			join subscriptions p on (p.talent_id = t.talent_id and p.skill_id = t.skill_id and p.active = 1 && p.begin_date <= NOW() and p.exp_date >= date(NOW()) and isnull(p.deleted)) 
			WHERE t.talent_id = ".$this->data['id']." AND  t.active = 1  and isnull(t.deleted) order by t.fee");
			
		}
		
		$data = $this->db->query("select p.* 
		from talent_packages p
		join subscriptions s on (s.talent_id = p.talent_id and s.skill_id =  p.skill_id and s.begin_date < NOW() && s.exp_date >= DATE(NOW()) )
		where p.talent_id = ".$this->data['id']." and isnull(p.deleted) 
		order by name");
		
		$out = array();
		if(!empty($data)){
			
			foreach ($data as $item){
				$p = new Talentpackage();
				$p->populate($item);
				$out[] = $p;	
			}
			
		}
		
		return $out;
	}
	
	/**
	* Get total number of packages
	*/
	public function isNew () {
		
		$total = $this->db->query("SELECT COUNT(*) AS total, TIME_TO_SEC(TIMEDIFF( NOW(), MAX(created))) AS latest FROM talent_packages WHERE talent_id = ".$this->data['id'], NULL , 1);
		
		return $total['total'] < 2 && $total['latest'] < 3600;
	}
	
	/**
	* Get details regarding each packages
	* contain details about all packages
	*/
	public function getPackageDetail ($active_only = true) {
		
		$con = "p.talent_id = ".$this->data['id'];
		
		if($active_only){
			$con .= " and isnull(p.deleted)";	
		}
		
		$data = $this->db->query("SELECT p.id as package__id, p.active as package__active, p.talent_id as package__talent_id, p.skill_id as package__skill_id, p.name as package__name, p.description as package__description, p.items as package__items, p.timespan as package__timespan, p.interval as package__interval, p.location_required as package__location_required, p.fee as package__fee, p.advance_percentage as package__advance_percentage, p.is_private as package__is_private, p.fulday as package__fulday, p.created as package__created, p.modified as package__modified, p.deleted as package__deleted, t.currency as package__currency,
s.id as skill__id, s.parent_id as skill__parent_id, s.name as skill__name, s.genre as skill__genre, s.gender_required as skill__gender_required, s.is_default as skill__is_default, s.created as skill__created, s.modified as skill__modified, s.deleted as skill__deleted
FROM talent_packages as p 
JOIN talents t on (t.id = p.talent_id)
JOIN skills s ON ( s.id = p.skill_id)
JOIN subscriptions u on (u.talent_id = p.talent_id and u.skill_id = p.skill_id and u.begin_date < NOW() && u.exp_date >= DATE(NOW()) )
WHERE ".$con." order by fee");



		return Util::groupResult($data, true);
		
	}
	
	/**
	* Get talent location
	*/
	public function getLocation () {
	
		return new Location($this->data['location_id']);
		
	}
	
	/**
	* Get skills 
	* retun skill object
	*/
	public function getSkills (){
		
		$data = $this->db->query("SELECT s.* FROM   subscriptions  t join skills s on (s.id = t.skill_id) WHERE t.talent_id = ".$this->data['id']." and isnull(t.deleted) and t.active = 1 order by t.id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Skill();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
		
		
	}
	
	/**
	* Get talent category names as a string
	*/
	public function getSkillString () {
		
		$data = $this->db->query("select s.name from subscriptions c
join skills s on (s.id = c.skill_id)
where c.talent_id = ".$this->data['id']." and isnull(c.deleted) and c.exp_date > now() and c.active = 1");
		
		$out = array();
		
		foreach ($data as $i){
			$out[] = $i['name'];	
		}
		
		return implode(', ', $out);
		
	}
	
	/**
	* Get associated agencies
	*/
	public function getAgencies () {
		
		return $this->db->query("SELECT a.id , a.name, a.agency_type, a.verified, a.timeoffset, a.timezone,a.active, l.city, l.region, l.country, i.thumb, i.i400, i.url, p.handle, r.id as request_id, r.confirm_appointments, view_bookings, view_booking_detail, manage_bookings, manage_page  FROM 
agency_request r
JOIN agencies a ON (a.id = r.agency_id)
JOIN locations l ON (l.id = a.location_id)
LEFT JOIN images i ON (i.id = a.image_id)
LEFT JOIN pages p ON (p.agency_id = a.id)
WHERE r.talent_id = ".$this->data['id']." AND r.status = '".DataType::$AGENCY_REQEST_ACCEPTED."'");
		
	}
	
	/**
	* Get active agency request
	*/
	public function getAgencyRequest ($agency_id) {
	
		
		$data = $this->db->query("select * from agency_request where talent_id = ".$this->data['id']." and agency_id = ".$agency_id." and isnull(deleted)", NULL, 1);
		
		if(!empty($data)) {
			$req = new AgencyRequest();
			$req->populate($data);
			return $req;
			
		}
		
		return false;
	}
	
	
	/**
	* Get Feed by page number
	*/
	public function getFeed ($page, $pagesize = 10){
		
		$skip = ($page - 1) * $pagesize;
		
		$data = $this->db->query("SELECT id,talent_id,caption,story,layout,created FROM posts WHERE talent_id = ".$this->data['id']." and isnull(deleted) and active = 1 ORDER BY display_order ASC, id desc ", NULL, $pagesize, $skip);
		
		if(!empty($data)){
			
			$data = Util::groupArray($data, 'id');
			$ids = array_keys($data);
			
			$images = $this->db->query("select i.id, i.i600, i.i400, i.url, p.post_id, i.orientation from images i join post_images p on (p.image_id = i.id) where p.post_id in (".(implode(',', $ids)).") && isnull(p.deleted) && isnull(i.deleted) order by display_order");
			
			$images = Util::groupMultiArray($images, 'post_id');
			
			foreach ($data as $i => $v){
			
				if(!empty($images[$i])){
					$data[$i]['images'] = $images[$i];	
				}
				
			}
			
		}
		
		return $data;
	}
	
	
	/**
	* Get image 
	*/
	public function getImage () {
		
		if(!empty($this->data['image_id'])) {
			return new Image($this->data['image_id']);
		}
		else{
			
			$image = new image();
			
			if($this->data['gender'] == 1){
				$url = Util::mapURL('/images/placeholder-boy.png');
			}
			else{
				$url = Util::mapURL('/images/placeholder-girl.png');
			}
			
			$image->i600 = $url;
			$image->i400 = $url;
			$image->thumb = $url;
			$image->url = $url;
			
			return $image;
		}			
	}
	
	
	
	
	
	/**
	* Get bookings
	* Retuns buffer object
	*/
	public function getBookings ( $status = NULL, $date = NULL) {
		
		$buffer = App::getBookings($this->data['id'], NULL, NULL, $status, $date);
		$buffer->_component = "booking/bookings";
		return $buffer;
			
		
	}
	
	


	/**
	* Get associated posts
	* Returns array of post objects or empty array 
	*/

	public function getPosts (){

		$data = $this->db->query("SELECT * FROM posts WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new post();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	

	
	



	/**
	* Get associated appoinments
	* Returns array of talent_appoinment objects or empty array 
	* from: yyyy-mm-dd
	*/
	
	public function getAppoinments ($from = NULL, $to = NULL, $as_array = false) {

		if(empty($from)){
			//
			$from =  date("Y-m-d");
		}
		else {
			$f = explode(' ', $from);
			
			if(count($f) == 1){
				$from =  Util::ToSysDate($from,'00:00');
			}
		}
		
		if(empty($to)){
			$to = date("Y-m-d H:i:s", strtotime('+1 day', strtotime($from)));		
		}
		else{
			$t = explode(' ', $to);		
			if(count($t) == 1){
				$to =  Util::ToSysDate($to,'11:59PM');	
			}
		}
		
		if($as_array){
			return $this->db->query("SELECT a.*, b.talent_fee, l.venue, l.address, l.city, l.region, l.country, g.name AS with_name, p.id AS package_id, p.name AS package_name, s.name AS skill_name
FROM 
appoinments a
LEFT JOIN agencies g ON (g.id = a.agency_id)
LEFT JOIN bookings b ON (b.id = a.booking_id)
LEFT JOIN locations l ON (l.id = b.location_id)
LEFT JOIN packages p ON (p.id = a.package_id)
LEFT JOIN skills s ON (s.id = p.skill_id) 
WHERE a.talent_id = ".$this->data['id']." and a.begins >= '".$from."' and a.begins <= '".$to."' and isnull(a.deleted) and (confirmed = 1 || isnull(a.expired) ||  a.expired > NOW()) order by a.begins,a.id");


		}

		$data = $this->db->query("SELECT * FROM appoinments WHERE talent_id = ".$this->data['id']." and begins >= '".$from."' and begins <= '".$to."' and isnull(deleted) and (isnull(expired) || expired > NOW()) order by begins,id", NULL);
		
		

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Appoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get talent's time line for given time
	*/
	public function getTimeline ($datetime){
		
		$datetime = Util::ToSysDate($datetime,'');
		
		$timeline = new Timeline($datetime, $this->data['timeoffset']);
			
		$range = $timeline->getRange();
		
		$appointments = $this->getAppoinments($range[0],$range[1], true);

		if(!empty($appointments)){
			
			foreach ($appointments as $a) {
			
				if(empty($a['confirmed'])){
					continue;
				}
								
				if(empty($a['all_day'])){
					$timeline->addToLine($a['begins'], $a['duration']+$a['rest_time'] );	
				}
				else {
					$timeline->addFullDay($a['begins'], $this->data['timeoffset']);					
				}
				
			}
		}
		
		if(!empty($this->data['limited_hours'])){
			
			$schedule = $this->getSchedule();
			
			$timeline->setWorkHours($schedule, $this->odata['timeoffset']);			
		}
		
		// check simultaneous posibility
		if($this->data['simultaneous_bookings'] > 1){
			// see if this guy has a paid subscription
			if($this->hasPaidSubscription()) {
				$timeline->simultaneous = $this->data['simultaneous_bookings']	;
			}
		}
		
		return $timeline;
		
	}
	
	
	/**
	* Get monthly appointments, 
	*/
	public function getMontlyAppoinments ($year, $month){
		
		$dayone = strtotime($year."-".$month.'-01');
		$firstday = date('D',$dayone) == 'Sunday' ? $dayone : strtotime("last sunday", $dayone);
		$dayend = strtotime(date("Y-m-t", $dayone));
		$lastday = date('D',$dayend) == 'Saturday' ? $dayend : strtotime("next saturday", $dayend);
		
		$offset = Session::timeDiff();
		
		$appointmnets = $this->db->query("SELECT  DATE(ADDTIME(begins, '".$offset."')) AS dates,COUNT(id) AS total, SUM(duration) AS minutes, confirmed
FROM appoinments
WHERE talent_id = ".$this->data['id']." and DATE(begins) >= '".date('Y-m-d', $firstday)."' and DATE(begins) <= '".date('Y-m-d', $lastday)."' and isnull(deleted)
GROUP BY DATE(begins), confirmed");


		$data = array();
		if(!empty($appointmnets)){
			
			foreach ($appointmnets as $a){
				
				if(!isset($data[$a['dates']])){
					$data[$a['dates']] = array('confirmed' => 0, 'confirmed_time' => 0 ,'shadow' => 0,'shadow_time' => 0, 'minutes' => 0);
				}
				
				if(!empty($a['confirmed'])){
					$data[$a['dates']]['confirmed'] = $a['total'];
					$data[$a['dates']]['confirmed_time'] = $a['minutes'];
				}
				else{
					$data[$a['dates']]['shadow'] = $a['total'];
					$data[$a['dates']]['shadow_time'] = $a['minutes'];
				}
				
				$data[$a['dates']]['minutes'] += $a['minutes'];
				
			}
			
		}
		
	

		$out = array();
		$date = $firstday;
		
		while ($date <= $lastday){
			
			$dt = date('Y-m-d', $date);
			
			$out[$dt] = isset($data[$dt]) ? $data[$dt] : NULL;

	
			$date = strtotime('+1 day', $date);
		}
		
		return $out;
	}


	/**
	* Get associated talent_packages
	* Returns array of talent_package objects or empty array 
	* if all is not True, return only active packages which have subscriptions
	*/

	public function getTalentPackages (){

			$data = $this->db->query("SELECT * FROM talent_packages WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);
		
		

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentPackage();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent_media
	* Returns array of talent_media objects or empty array 
	*/

	public function getTalentMedia (){

		$data = $this->db->query("SELECT * FROM talent_media WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentMedia();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent_skills
	* Returns array of talent_skill objects or empty array 
	*/

	public function getTalentSkills (){

		$data = $this->db->query("SELECT * FROM talent_skills WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentSkill();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated ledgers
	* Returns array of ledger objects or empty array 
	*/

	public function getLedgers (){

		$data = $this->db->query("SELECT * FROM ledgers WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new ledger();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated payout_methods
	* Returns array of payout_method objects or empty array 
	*/

	public function getPayoutMethods (){

		$data = $this->db->query("SELECT * FROM payout_methods WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payoutMethod();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated emails
	* Returns array of email objects or empty array 
	*/

	public function getEmails (){

		$data = $this->db->query("SELECT * FROM emails WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new email();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated phones
	* Returns array of phone objects or empty array 
	*/

	public function getPhones (){

		$data = $this->db->query("SELECT * FROM phones WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new phone();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated usernames
	* Returns array of username objects or empty array 
	*/

	public function getUsernames (){

		$data = $this->db->query("SELECT * FROM usernames WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new username();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated pages
	* Returnsexisting page or create a new page
	*/

	public function getPage (){

		$data = $this->db->query("SELECT * FROM pages WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL, 1);
		
		$page = new Page();	
		
		if(!empty($data)){
			$page->populate($data);
		}
		else {
			$page->customer_id = $this->data['customer_id'];
			$page->talent_id = $this->data['id'];
			$page->active = 0;
			$page->save();
			
			// creating parts
			$sections = array('a', 'b', 'h', 'c', 'd', 'e', 'f', 'g');
			foreach ($sections as $s){
				$page->getSection($s);	
			}
		}
		
		return $page;
		
	}



	/**
	* Get associated booking_trail
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrail (){

		$data = $this->db->query("SELECT * FROM booking_trail WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated booking_trails
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrails (){

		$data = $this->db->query("SELECT * FROM booking_trails WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Check if given time and package is possible 
	*/
	public function isBookingPossible ( $datetime, $package){
		
		
		$begin = $datetime;
		$ends = strtotime($datetime) + $package->timespan*60;
		$ends = date('Y-m-d H:i:s',$ends);
		
		$data = $this->db->query("select * from appointments_view
		where 
		((ends > '".$begin."' and ends <= '".$ends."') or 
		(begins >= '".$begin."' and ends <= '".$ends."' ) or 
		( begins >= '".$begin."' and begins < '".$ends."' )) and  
		confirmed = 1 and isnull(deleted)");
		
		if(empty($data)){
			return true;	
		}
		
		// check simultanious bookings
		if($this->data['simultaneous_bookings'] > 1 && count($data) < $this->data['simultaneous_bookings']){
			return true;
		}
		
		return false;
	}



	/**
	* Get associated pages
	* Returns array of page objects or empty array 
	*/

	public function getPages (){

		$data = $this->db->query("SELECT * FROM pages WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new page();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get detail infor about the talent
	*/
	public function getinfo () {
		
		$data = $this->db->query("select
		t.id as talent__id, 
t.customer_id as talent__customer_id, 
t.verified as talent__verified, 
t.name as talent__name, 
t.location_id as talent__location_id, 
t.timeoffset as talent__timeoffset, 
t.currency as talent__currency, 
t.summary as talent__summary, 
t.gender as talent__gender, 
t.is_trans as talent__is_trans, 
t.image_id as talent__image_id, 
t.agency_fee_type as talent__agency_fee_type, 
t.agency_fee as talent__agency_fee, 
t.limited_hours as talent__limited_hours, 
t.work_start as talent__work_start, 
t.work_end as talent__work_end, 
t.simultaneous_bookings as talent__simultaneous_bookings, 
t.active as talent__active, 
t.created as talent__created, 
t.modified as talent__modified, 
t.deleted as talent__deleted,
c.id as customer__id, 
c.firstname as customer__firstname, 
c.lastname as customer__lastname, 
c.name_changed as customer__name_changed, 
c.dob as customer__dob, 
c.location_id as customer__location_id, 
c.timeoffset as customer__timeoffset, 
c.timezone as customer__timezone, 
c.currency as customer__currency, 
c.image_id as customer__image_id, 
c.active as customer__active, 
c.gender as customer__gender, 
c.is_trans as customer__is_trans, 
c.created as customer__created, 
c.modified as customer__modified, 
c.deleted as customer__deleted,
l.id as location__id, 
l.address as location__address, 
l.city as location__city, 
l.region as location__region, 
l.postalcode as location__postalcode, 
l.country as location__country, 
l.locked as location__locked, 
l.created as location__created, 
l.modified as location__modified, 
l.deleted as location__deleted,
i.id as image__id, 
i.i400 as image__i400, 
i.thumb as image__thumb, 
i.url as image__url, 
i.orientation as image__orientation,
p.id as page__id, 
p.customer_id as page__customer_id, 
p.talent_id as page__talent_id, 
p.agency_id as page__agency_id, 
p.handle as page__handle, 
p.banner_id as page__banner_id, 
p.description as page__description, 
p.active as page__active, 
p.created as page__created, 
p.modified as page__modified, 
p.deleted as page__deleted,
(SELECT  skill_id FROM subscriptions WHERE talent_id = t.id AND begin_date < NOW() && exp_date >= DATE(NOW() and active = 1) LIMIT 0,1) as skills__0,
(SELECT  skill_id FROM subscriptions WHERE talent_id = t.id AND begin_date < NOW() && exp_date >= DATE(NOW() and active = 1) LIMIT 1,1) as skills__1,
(SELECT  skill_id FROM subscriptions WHERE talent_id = t.id AND begin_date < NOW() && exp_date >= DATE(NOW() and active = 1) LIMIT 2,1) as skills__2
from talents t
join customers c on (c.id = t.customer_id)
join locations l on (l.id = t.location_id)
left join images i on (i.id = t.image_id)
left join pages p on (p.talent_id = t.id)
where t.id = ".$this->data['id'], NULL, 1);

		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		
		
		return $out;
		
		
	}



	/**
	* Get associated subscriptions
	* Returns array of Subscription objects or empty array 
	*/

	public function getsubscriptions (){

		$data = $this->db->query("SELECT * FROM subscriptions WHERE talent_id = ".$this->data['id']." and isnull(deleted) and exp_date > date(NOW()) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get subscription details
	*/
	public function getSubscriptionDetail () {
		
		$data = $this->db->query("SELECT s.id AS subscription__id, s.talent_id AS subscription__talent_id, s.skill_id AS subscription__skill_id, s.package_id AS subscription__package_id, s.payment_id AS subscription__payment_id, s.reneval_id AS subscription__reneval_id, s.term AS subscription__term, s.sub_date AS subscription__sub_date, s.begin_date AS subscription__begin_date, s.exp_date AS subscription__exp_date, s.active AS subscription__active, s.jobs as subscription__jobs, s.manage as subscription__manage, s.fees as subscription__fees, s.created AS subscription__created, s.modified AS subscription__modified, s.deleted AS subscription__deleted,
p.id AS package__id, p.skill_id AS package__skill_id, p.name AS package__name, p.description AS package__description, p.price_monthly AS package__price_monthly, p.price_annualy AS package__price_annualy, p.jobs AS package__jobs, p.begin_date AS package__begin_date, p.end_date AS package__end_date, p.active AS package__active, p.display_order AS package__display_order, p.selected AS package__selected, p.created AS package__created, p.modified AS package__modified, p.deleted AS package__deleted,
k.id AS skill__id, k.parent_id AS skill__parent_id, k.name AS skill__name, k.genre AS skill__genre, k.gender_required AS skill__gender_required, k.is_default AS skill__is_default, k.created AS skill__created, k.modified AS skill__modified, k.deleted AS skill__deleted,
r.id AS reneval__id, r.talent_id AS reneval__talent_id, r.package_id AS reneval__package_id, r.term AS reneval__term, r.method_id AS reneval__method_id, r.next_reneval AS reneval__next_reneval, r.active AS reneval__active, r.created AS reneval__created, r.modified AS reneval__modified, r.deleted AS reneval__deleted,
rm.id AS reneval_method__id, rm.customer_id AS reneval_method__customer_id, rm.type AS reneval_method__type, rm.name AS reneval_method__name, rm.card_name AS reneval_method__card_name, rm.card_number AS reneval_method__card_number, rm.card_exp AS reneval_method__card_exp, rm.ccvv AS reneval_method__ccvv, rm.token AS reneval_method__token, rm.reference AS reneval_method__reference, rm.created AS reneval_method__created, rm.modified AS reneval_method__modified, rm.deleted AS reneval_method__deleted,
py.id AS payment__id, py.invoice_id AS payment__invoice_id, py.subscription_id AS payment__subscription_id, py.method AS payment__method, py.amount AS payment__amount, py.currency AS payment__currency, py.card_name AS payment__card_name, py.card_type AS payment__card_type, py.card_num AS payment__card_num, py.card_exp AS payment__card_exp, py.card_cvv AS payment__card_cvv, py.payment_id AS payment__payment_id, py.transaction_id AS payment__transaction_id, py.bank_id AS payment__bank_id, py.ip_address AS payment__ip_address, py.status AS payment__status, py.success AS payment__success, py.released AS payment__released, py.created AS payment__created, py.modified AS payment__modified, py.deleted AS payment__deleted
FROM subscriptions s 
JOIN packages p ON ( p.id = s.package_id)
JOIN skills k ON ( k.id = p.skill_id)
LEFT JOIN payments py ON ( py.id = s.payment_id)
LEFT JOIN auto_renevals r ON ( r.id = s.reneval_id)
LEFT JOIN payment_methods rm ON (rm.id = r.method_id)
WHERE s.talent_id = ".$this->data['id']." and s.exp_date >= date(NOW()) and isnull(s.deleted) && s.active = 1");


		
		$subs = array();
		
		foreach ($data as $item) {
			
			$out = array();
			
			foreach ($item as $k => $v){
				
				list($t, $f) = explode('__',$k);
				if(!isset($out[$t])){
					$out[$t] = array();
				}
				
				$out[$t][$f] = $v;	
			}
			
			$subs[] = $out;
		}
		
		
		
		
		return $subs;
			
	}
	
	
	/**
	* Check if talent has a paid subscription
	*/
	public function hasPaidSubscription () {
		
		$data = $this->db->query("SELECT COUNT(*) AS total FROM subscriptions WHERE fees > 0 AND !ISNULL(payment_id) AND talent_id = ".$this->data['id']." AND ISNULL(deleted) and exp_date > date(NOW())", NULL, 1);
		
		return $data['total'] > 0;
		
	}



	/**
	* Get associated auto_renevals
	* Returns array of auto_reneval objects or empty array 
	*/

	public function getAutoRenevals (){

		$data = $this->db->query("SELECT * FROM auto_renevals WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new autoReneval();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent_schedules
	* Returns array of talent_schedule objects or empty array 
	*/

	public function getTalentSchedules (){

		$data = $this->db->query("SELECT * FROM talent_schedules WHERE talent_id = ".$this->data['id'], NULL, 1);

		if(!empty($data)){
			
			$obj = new talentSchedule();
			$obj->populate($data);
			return $obj;
			
		}
		else {
			// create schedule
			$schedule = new talentSchedule();
			$schedule->talent_id = $this->data['id'];
			$schedule->save();
			
			return $schedule;	
		}
	
	}


	public function getSchedule () {
		return $this->getTalentSchedules();	
	}


	/**
	* Get associated agency_request
	* Returns array of agency_request objects or empty array 
	*/

	public function getAgencyRequests ($as_array = false, $status = NULL){

		if($as_array){
			return $this->db->query("SELECT r.id, r.status, r.created, a.id AS agency_id, a.name AS agency_name, a.agency_type, i.i400, i.thumb, i.url, p.handle, l.city, l.region, l.country
FROM agency_request r 
JOIN agencies a ON (a.id = r.agency_id)
LEFT JOIN images i ON (i.id = a.image_id)
LEFT JOIN pages p ON (p.agency_id = a.id)
LEFT JOIN locations l on (l.id = a.location_id)
WHERE r.talent_id = ".$this->data['id']." AND ISNULL(r.deleted) ".(!empty($status) ? " and r.status = '".$status."'" : '')." order by r.created desc");
		}
		
		$data = $this->db->query("SELECT * FROM agency_request WHERE talent_id = ".$this->data['id']." and isnull(deleted) ".(!empty($status) ? " and status = '".$status."'" : '')." order by created desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agencyRequest();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


	/**
	* Get existing talent request
	*/
	public function getAgencyConnection ($agency_id){
		
		$data = $this->db->query("select * from agency_request where agency_id = ".$agency_id." and talent_id = ".$this->data['id']." and status = '".DataType::$AGENCY_REQEST_ACCEPTED."' and isnull(deleted) order by id desc", NULL, 1);
		
		if(!empty($data)){
			$req = new AgencyRequest();
			$req->populate($data);
			return $req;	
		}
		
		
		return false;
	}


	



	/**
	* Get associated board_participants
	* Returns array of board_participant objects or empty array 
	*/

	public function getBoardParticipants (){

		$data = $this->db->query("SELECT * FROM board_participants WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new boardParticipant();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated likes
	* Returns array of like objects or empty array 
	*/

	public function getLikes (){

		$data = $this->db->query("SELECT * FROM likes WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new like();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated agenda_shares
	* Returns array of agenda_share objects or empty array 
	*/

	public function getAgendaShares (){

		$data = $this->db->query("SELECT * FROM agenda_shares WHERE talent_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agendaShare();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}

	
}

?>