<?php
		
/****************************
* Login Class
*
* type: e:email, m:mobile, f:facebook, g:google
*****************************/

class Login extends Model{

	public $table = 'logins';
	
	private $key = 'YUY5NDcXODUwZjJl';

	public $fields = array(
		'id',
		'customer_id',
		'type',
		'ref_id',
		'email_id',
		'phone_id',
		'blocked',
		'created',
		'modified',
		'deleted'
		);
	
	/** when user send 3 email/sms and trying another, it issue a fater warning */
	public $fatel = 0;
	
	/** Holding an error **/
	public $error = '';

	/**
	* Lookup login by username
	* type : b is for both e and m
	*/
	public function Lookup ($id, $type = NULL){
		
		if(empty($type)) {
			return false;
		}
		else{
			
			if($type == 'p'){
				$data = $this->db->query("select * from logins where phone_id = {$id} and isnull(deleted)", NULL,  1);
			}
			else{
				$data = $this->db->query("select * from logins where email_id = {$id} and isnull(deleted)", NULL,  1);
			}
			
			
		}
				
		if(!empty($data)){

			$this->populate($data);
			return true;	
		}
		
		return false;
	}
	
	/**
	* Check if customer can login with this login record
	*/
	public function isPossible () {
		
		return true;
		
	}
	
	/**
	* Run nessasaru updates on user login
	*/
	public function loggedin () {
		
		if($this->data['is_verified'] == 0){
			$this->data['is_verified'] = 1;
			
			$this->db->execute("UPDATE customers SET active = 1 WHERE id = ".$this->data['customer_id']);
		}
		
		$this->data['last_login'] = 'NOW';
		$this->save();
		
	}
	
	/**
	* Easy functions to check type
	*/
	public function isMobile () {
		return $this->data['type'] == 'm';
	}
	
	public function isEmail () {
		return $this->data['type'] == 'e';
	}
	
	public function isFacebook () {
		return $this->data['type'] == 'f';
	}
	
	public function isGoogle () {
		return $this->data['type'] == 'g';
	}
	
	
	
	
	/**
	* Get related object
	*/
	public function getCustomer () {
		
		return new Customer($this->data['customer_id']);	
	}
	
	/**
	* Once login found, it can be verified by code 
	* Get unverified verification code sent for this login
	*/
	public function getCode () {
		
		$data = $this->db->query("select * from codes where login_id = ".$this->data['id']." and verified = 0 and attempts < 4 order by id desc", NULL, 1);
			
		if(!empty($data)){
			$code = new Code();
			$code->populate($data);
			return $code;	
		}
		
		return NULL;
		
	}
	
	/**
	* get login key
	*/
	public function getKey () {
		

		$key = $this->data['id']."|".$this->data['created'];
		$key = Util::encrypt($key, $this->key); 
		$key = base64_encode($key);
		return urlencode($key);
	}
	
	
	/**
	* Populate object by key
	*/
	public function setKey ($token) {

		$token = urldecode(trim($token));
		$key = base64_decode($token);
		$key = Util::decrypt($key, $this->key);

		$key = explode('|', $key);
		 if(!empty($key[0]) && is_numeric($key[0])) {

		 	$data = $this->read($this->table, $key[0]);

			if($data['created'] == $key[1]){
				$this->populate($data);	
				return true;
			}			
		 }		 
		 
		 return false;
		
	}
	
	/**
	* Get other login  associated with this customer by type
	*/
	public function getAssocLogin ( $type ){
		
		
		if($this->data['type'] != $type){
				
			$sib = $this->db->query("select * from logins where  customer_type = '".$this->data['customer_type']."' and customer_id = ".$this->data['customer_id']." and type = '".$type."' and isnull(deleted)", NULL, 1);
			
			if(!empty($sib)){
				$this->populate($sib);	
			}
		
		}
		return $this;
	}
	
	/**
	* Get all other login  associated with this customer 
	*/
	public function getAssocLogins ( ){
		
		$out = array();
		
		$data = $this->db->query("select * from logins where  customer_type = '".$this->data['customer_type']."' and customer_id = ".$this->data['customer_id']." and  isnull(deleted)", NULL );
	
		if(!empty($data) && count($data) > 1){
			
			foreach ($data as $item){
				
				if($item['id'] != $this->data['id']){
					
					$login = new Login();
					$login->populate($item);
					$out[] = $login;
						
				}					
			}			
		}
		
		
		return $out;
	}
	
	
	/**
	* Create and send OTP 
	*/
	/*
	public function sendOTP () {
		
		if(!empty($this->data['deleted'])){
			$this->error = "Login disabled";
			$this->fatel = true;
			return false;
		}
		
		
		$excodes = $this->db->query("SELECT login_id, COUNT(*) AS total, MAX(created) AS lastsend FROM codes
 WHERE login_id = ".$this->data['id']." AND verified = 0 AND created > '".date("Y-m-d H:i:s", strtotime("-1 hours"))."' ", NULL, 1);
		
		if(empty($excodes)) {
		
			$code = new Code();
			$code->type = $this->data['type'];
			$code->customer_id = $this->data['customer_id'];
			$code->login_id = $this->data['id'];
			$code->attempts = 0;
			$code->verified = 0;
			$code->save();
			
			$code->send();
			
			return $code;
		}
		else{
			
			// 	analize to see if spaming.
			//	if there are 3 unsuccesfull logins and tring to another
			// 	user should have wait 24 hours before sending another otp
			// 	interval between each is 2 min
			//
			
			if(strtotime($excodes['lastsend']) > time() - 119 ){
				
				
				$this->error = "Wait 2 mins before send another code ";
				//.date("H:i:s");
				return false;
			}
			
			if($excodes['total'] >= 3){
				
				$remians = strtotime($excodes['lastsend'])+ 3600 - time();
				
				$this->error = "We have sent 3 OTP codes.<br> Please request new code after ".ceil(($remians%3600)/60)." minutes";
				$this->fatel = true;
				return false;
			}
		
		
			$code = new Code();
			$code->type = $this->data['type'];
			$code->customer_id = $this->data['customer_id'];
			$code->login_id = $this->data['id'];
			$code->attempts = 0;
			$code->verified = 0;
			$code->save();
			
			$code->send();
		
			return $code;
				
		}		
		
		return false;
	}
	*/
	
	/**
	* Get hashed email address or phone number
	*/
	public function toDisplay () {
		
		
		if($this->data['type'] == 'e') {
			$parts = explode('@', $this->data['username']);
		
			return $parts[0][1].'***'.$parts[0][strlen($parts[0])-1].'@'.$parts[1];
		}
		else{
			return '****'.substr($this->data['username'], strlen($this->data['username'])-4, 4 );	
		}
		
	}


	/**
	* Get associated codes
	* Returns array of code objects or empty array 
	*/

	public function getCodes (){

		return $this->getCode();
	}



	/**
	* Get associated customer_logins
	* Returns array of customer_login objects or empty array 
	*/

	public function getCustomerLogins (){

		$data = $this->db->query("SELECT * FROM customer_logins WHERE login_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new customerLogin();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}

?>