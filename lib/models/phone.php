<?php

/**********************************************
* Phone Class
**********************************************/

class Phone extends Model{

	public $table = 'phones';

	public $fields = array(
		'id',
		'customer_id',
		'is_primary',
		'is_public',
		'type',
		'country_code',
		'phone',
		'sms',
		'whatsapp',
		'is_verified',
		'created',
		'modified',
		'deleted'
	);

	/**
	* Modify constructor function to accept either ID or countryt code+phone number
	*/
	function __construct ( $id_code = NULL, $number = NULL) {
		
		if(!empty($id_code)) {
			
			if(is_numeric($id_code)){
				parent::__construct($id_code);	
			}
			else{
				parent::__construct();
				
				$data = $this->db->query("select * from phones where country_code = :a and phone = :b", array( 'a' => $id_code, 'b' => $number), 1);
				
				if(!empty($data)){
					$this->populate($data);	
				}
			}
		}
		else{
			parent::__construct();	
		}
		
    }


	
	
	
	/**
	* Get hashed phone number
	*/
	public function toDisplay () {
		
		return '****'.substr($this->data['phone'], strlen($this->data['phone'])-4, 4 );
		
	}





	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated codes
	* Returns array of code objects or empty array 
	*/

	public function getCodes (){

		$data = $this->db->query("SELECT * FROM codes WHERE phone_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new code();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Make the phone a primary
	*/
	public function makePrimary () {
		
		$this->db->execute("UPDATE phones SET is_primary = 0 WHERE customer_id = {$this->data['customer_id']} AND id != {$this->data['id']}");
		
		$this->db->execute("UPDATE phones SET is_primary = 1, is_verified = 1 WHERE  id = {$this->data['id']}");
		
	}


}
?>
