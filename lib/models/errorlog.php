<?php

/**********************************************
* ErrorLog Class
**********************************************/

class ErrorLog extends Model{

	public $table = 'error_logs';

	public $fields = array(
		'id',
		'customer_id',
		'request',
		'response',
		'contact',
		'created',
		'modified',
		'deleted'
	);

}
?>
