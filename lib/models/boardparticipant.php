<?php

/**********************************************
* BoardParticipant Class
**********************************************/

class BoardParticipant extends Model{

	public $table = 'board_participants';

	public $fields = array(
		'id',
		'board_id',
		'customer_id',
		'profile_id',
		'muted',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated message_board
	* Returns message_board object or NULL 
	*/

	public function getMessageBoard (){

		$data = $this->db->query("SELECT * FROM message_boards WHERE id = ".$this->data['board_id'], NULL, 1);

		if(!empty($data)){
			$obj = new messageBoard();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
