<?php

/**********************************************
* InvoiceItem Class
**********************************************/

class InvoiceItem extends Model{

	public $table = 'invoice_items';

	public $fields = array(
		'id',
		'invoice_id',
		'description',
		'amount',
		'amount_local',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated invoice
	* Returns invoice object or NULL 
	*/

	public function getInvoice (){

		$data = $this->db->query("SELECT * FROM invoices WHERE id = ".$this->data['invoice_id'], NULL, 1);

		if(!empty($data)){
			$obj = new invoice();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
