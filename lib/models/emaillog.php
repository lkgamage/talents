<?php

/**********************************************
* EmailLog Class
**********************************************/

class EmailLog extends Model{

	public $table = 'email_logs';

	public $fields = array(
		'id',
		'customer_id',
		'email_id',
		'type',
		'created',
		'modified',
		'deleted'
	);

}
?>
