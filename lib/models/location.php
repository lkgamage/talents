<?php
		
/****************************
* Location Class
*****************************/

class Location extends Model{

	public $table = 'locations';

	public $fields = array(
		'id',
		'customer_id',
		'place_id',
		'display',
		'venue',
		'address',
		'city',
		'region',
		'postalcode',
		'country',
		'lng',
		'lat',
		'locked',
		'created',
		'modified',
		'deleted'
		);
	



	/**
	* Get associated bookings
	* Returns array of booking objects or empty array 
	*/

	public function getBookings (){

		$data = $this->db->query("SELECT * FROM bookings WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new booking();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talents
	* Returns array of talent objects or empty array 
	*/

	public function getTalents (){

		$data = $this->db->query("SELECT * FROM talents WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talent();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated bookers
	* Returns array of booker objects or empty array 
	*/

	public function getBookers (){

		$data = $this->db->query("SELECT * FROM bookers WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new booker();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customers
	* Returns array of customer objects or empty array 
	*/

	public function getCustomers (){

		$data = $this->db->query("SELECT * FROM customers WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new customer();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated events
	* Returns array of event objects or empty array 
	*/

	public function getEvents (){

		$data = $this->db->query("SELECT * FROM events WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new event();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated agencies
	* Returns array of agency objects or empty array 
	*/

	public function getAgencies (){

		$data = $this->db->query("SELECT * FROM agencies WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agency();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated payout_methods
	* Returns array of payout_method objects or empty array 
	*/

	public function getPayoutMethods (){

		$data = $this->db->query("SELECT * FROM payout_methods WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payoutMethod();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated profiles
	* Returns array of profile objects or empty array 
	*/

	public function getProfiles (){

		$data = $this->db->query("SELECT * FROM profiles WHERE location_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new profile();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}

?>