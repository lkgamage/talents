<?php
		
/****************************
* Complain Class
*****************************/

class Complain extends Model{

	public $table = 'complains';

	public $fields = array(
			'id',
			'booking_id',
			'description',
			'status',
			'created',
			'modified',
			'deleted'
		);
	



	/**
	* Get associated complain_messages
	* Returns array of complain_message objects or empty array 
	*/

	public function getComplainMessages (){

		$data = $this->db->query("SELECT * FROM complain_messages WHERE complain_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new complainMessage();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}

?>