<?php

/**********************************************
* EventBooking Class
**********************************************/

class EventBooking extends Model{

	public $table = 'event_bookings';

	public $fields = array(
		'id',
		'item_id',
		'booking_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated event_item
	* Returns event_item object or NULL 
	*/

	public function getEventItem (){

		$data = $this->db->query("SELECT * FROM event_items WHERE id = ".$this->data['item_id'], NULL, 1);

		if(!empty($data)){
			$obj = new eventItem();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
