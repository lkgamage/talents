<?php
		
/****************************
* Rating Class
*****************************/

class Skill extends Model{

	public $table = 'skills';

	public $fields = array(
		'id',
		'category',
		'parent_id',
		'name',
		'genre',
		'gender_required',
		'is_default',
		'is_business',
		'is_agency',
		'active',
		'created',
		'modified',
		'deleted'
		);
	



	/**
	* Get associated talent_skills
	* Returns array of talent_skill objects or empty array 
	*/

	public function getTalentSkills (){

		$data = $this->db->query("SELECT * FROM talent_skills WHERE skill_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentSkill();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent_packages
	* Returns array of talent_package objects or empty array 
	*/

	public function getTalentPackages (){

		$data = $this->db->query("SELECT * FROM talent_packages WHERE skill_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentPackage();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated Subscriptions
	* Returns array of Subscription objects or empty array 
	*/

	public function getSubscriptions (){

		$data = $this->db->query("SELECT * FROM Subscriptions WHERE skill_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated packages
	* Returns array of package objects or empty array 
	*/

	public function getPackages (){

		$data = $this->db->query("SELECT * FROM packages WHERE skill_id = ".$this->data['id']." and isnull(deleted) and one_time = 0 order by price", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new package();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get a single credit for one booking
	*/
	public function getOneCreditPackage (){

		$item = $this->db->query("SELECT * FROM packages WHERE skill_id = ".$this->data['id']." and isnull(deleted) and one_time = 1 order by price", NULL,1);

		$out = array();

		if(!empty($item)){
			
			$obj = new package();
			$obj->populate($item);
			return  $obj;

		}
		
		
	}

	

	/**
	* Get associated tags
	* Returns array of tag objects or empty array 
	*/

	public function getTags (){

		$data = $this->db->query("SELECT * FROM tags WHERE skill_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new tag();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated categories
	* Returns array of category objects or empty array 
	*/

	public function getCategories (){

		$data = $this->db->query("SELECT * FROM categories WHERE skill_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new category();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated skill_tags
	* Returns array of skill_tag objects or empty array 
	*/

	public function getSkillTags (){

		$data = $this->db->query("SELECT * FROM skill_tags WHERE skill_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new skillTag();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}

?>