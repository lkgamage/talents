<?php

/**********************************************
* CategorySubscription Class
**********************************************/

class CategorySubscription extends Model{

	public $table = 'category_subscriptions';

	public $fields = array(
		'id',
		'category_id',
		'stripe_id',
		'active',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated category
	* Returns category object or NULL 
	*/

	public function getCategory (){

		$data = $this->db->query("SELECT * FROM categories WHERE id = ".$this->data['category_id'], NULL, 1);

		if(!empty($data)){
			$obj = new category();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated subscriptions
	* Returns array of subscription objects or empty array 
	*/

	public function getSubscriptions (){

		$data = $this->db->query("SELECT * FROM subscriptions WHERE catsub_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* cativate this category subscription as default one
	
	*/
	public function activate () {
		
		if($this->data['active'] == 0){
			
			$this->db->execute("UPDATE category_subscriptions SET active = 0, deleted = NOW() WHERE  category_id = {$this->data['category_id']} and active = 1");
		
			$this->data['active'] = 1;
			$this->save();
			
		}
		
		
		
		return true;
		
	}

}
?>
