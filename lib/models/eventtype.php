<?php
		
/****************************
* Eventtype Class
*****************************/

class Eventtype extends Model{

	public $table = 'event_types';

	public $fields = array(
		'id',
		'name',
		'corporate',
		'created',
		'modified',
		'deleted'
		);
	



	/**
	* Get associated events
	* Returns array of event objects or empty array 
	*/

	public function getEvents (){

		$data = $this->db->query("SELECT * FROM events WHERE event_type = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new event();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}

?>