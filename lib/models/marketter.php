<?php

/**********************************************
* Marketter Class
**********************************************/

class Marketter extends Model{

	public $table = 'marketters';

	public $fields = array(
		'id',
		'name',
		'mobile1',
		'mobile2',
		'email',
		'display',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated leads
	* Returns array of lead objects or empty array 
	*/

	public function getLeads (){

		$data = $this->db->query("SELECT * FROM leads WHERE marketter_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new lead();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated registrations
	* Returns array of registration objects or empty array 
	*/

	public function getRegistrations (){

		$data = $this->db->query("SELECT * FROM registrations WHERE marketer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new registration();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
