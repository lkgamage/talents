<?php

/**********************************************
* Notification Class
**********************************************/

class Notification extends Model{

	public $table = 'notifications';

	public $fields = array(
		'id',
		'customer_id',
		'type',
		'ref',
		'message',
		'image_id',
		'seen',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
