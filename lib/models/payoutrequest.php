<?php

/**********************************************
* PayoutRequest Class
**********************************************/

class PayoutRequest extends Model{

	public $table = 'payout_requests';

	public $fields = array(
		'id',
		'payout_method_id',
		'amount',
		'ip_address',
		'verified',
		'status',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated payout_method
	* Returns payout_method object or NULL 
	*/

	public function getPayoutMethod (){

		$data = $this->db->query("SELECT * FROM payout_methods WHERE id = ".$this->data['payout_method_id'], NULL, 1);

		if(!empty($data)){
			$obj = new payoutMethod();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated withdrawals
	* Returns array of withdrawal objects or empty array 
	*/

	public function getWithdrawals (){

		$data = $this->db->query("SELECT * FROM withdrawals WHERE payout_request_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new withdrawal();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
