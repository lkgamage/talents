<?php

/**********************************************
* CustomerLogin Class
**********************************************/

class CustomerLogin extends Model{

	public $table = 'customer_logins';

	public $fields = array(
		'id',
		'customer_id',
		'login_id',
		'device_id',
		'fingureprint',
		'ip',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated login
	* Returns login object or NULL 
	*/

	public function getLogin (){

		$data = $this->db->query("SELECT * FROM logins WHERE id = ".$this->data['login_id'], NULL, 1);

		if(!empty($data)){
			$obj = new login();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer_device
	* Returns customer_device object or NULL 
	*/

	public function getCustomerDevice (){

		$data = $this->db->query("SELECT * FROM customer_devices WHERE id = ".$this->data['device_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customerDevice();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
