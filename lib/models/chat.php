<?php
		
/****************************
* Chat Class - Handles hig level chat operations
*****************************/

class Chat extends Model{

	public $error = '';
	
	public $data = array();
	
	/** last posted message **/
	public $message;
	
	/**
	* Get chatroom for given object
	*/
	public function getBoard () {
		
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
	
		if (!empty($_POST['board'])){
			
			if( is_numeric($_POST['board'])){
				$board =  new MessageBoard($_POST['board']);
				
				if(!$board->ready()){
					$this->error = "Invalid Chatroom ID";	
					return false;
				}
			}
			else{
				$this->error = "Invalid Chatroom ID";	
				return false;	
			}
			
		}
		elseif (!empty($_POST['event'])){
			
			if(is_numeric($_POST['event'])) {
				$data = $this->db->query("select * from message_boards where event_id = ".$_POST['event'], NULL, 1);
				
				if(!empty($data)){
					$board =  new MessageBoard();
					$board->populate($data);		
				}
				else{
					// create a board for the event
					$event = new Event($_POST['event']);
					
					if($event->ready()) {
						
						$partners = $event->getPartners();
						
						if(isset($partners[$customer['id']])){
							$board = $this->createBoard($event->id);														
						}
						else{
							$this->error = "Unauthorized event";
							return false;	
						}
						
						if(!empty($partners)){
							
							foreach($partners as $p){
								$board->addParticipant($p['customer_id'], $p['talent_id'], $p['agency_id']);	
							}							
						}
					
					}
					else{
						$this->error = "Invalid event";
						return false;
					}
				}
			}
			else{
				$this->error = "Invalid event ID";	
				return false;	
			}
			
		}
		elseif (!empty($_POST['booking'])){
			
			if(is_numeric($_POST['booking'])) {
				
				$data = $this->db->query("select * from message_boards where booking_id = ".$_POST['booking'], NULL, 1);
				
				if(!empty($data)){
					$board =  new MessageBoard();
					$board->populate($data);		
				}
				else{
					// create a board
					$booking = new Booking($_POST['booking']);
					
					if($booking->ready()){
						
						$partners = $booking->getPartners();
						
						if(isset($partners[$customer['id']])){
							$board = $this->createBoard(NULL, $booking->id);														
						}
						else{
							$this->error = "Unauthorized booking";
							return false;	
						}
						
						if(!empty($partners)){
							
							foreach($partners as $p){
								$board->addParticipant($p['customer_id'], $p['talent_id'], $p['agency_id']);	
							}							
						}
						
					}
					else{
						$this->error = "Invalid booking";
						return false;	
					}
				}
			}
			else{
				$this->error = "Invalid booking ID";
				return false;
			}
			
		}
		elseif (!empty($_POST['talent'])){
			
			if(is_numeric($_POST['talent'])) {
			
				$data = $this->db->query("SELECT mb.* FROM
	message_boards mb
	JOIN board_participants p ON (p.board_id = mb.id)
	WHERE p.talent_id = ".$_POST['talent']." AND mb.id IN (SELECT DISTINCT(board_id) FROM board_participants WHERE customer_id = ".$customer['id'].") and isnull(event_id) and isnull(booking_id)", NULL, 1);
	
				if(!empty($data)) {
					$board =  new MessageBoard();
					$board->populate($data);	
				}
				else{
					// create board
					// permissions drive on profile_settings
					$talent = new Talent($_POST['talent']);
					
					if($talent->ready()) {
						
						if($talent->customer_id == $customer['id']){
							return false;	
						}
						
						$board = $this->createBoard();
						
						$board->addParticipant($talent->customer_id, $talent->id);
						
						if(!empty($profile)){
							
							if($profile['type'] == 't'){
								$board->addParticipant($this->user->id, $profile['id']);
							}
							else{
								$board->addParticipant($this->user->id, NULL, $profile['id']);
							}
						}
						else{
							$board->addParticipant($this->user->id);
						}
						
					}
					else{
						$this->error = "Invalid talent ID";
						return false;		
					}
				}
		
			}
			else{
				$this->error = "Invalid talent ID";	
				return false;	
			}
			
			
		}
		elseif (!empty($_POST['agency'])){
			
			if(is_numeric($_POST['agency'])) {
			
				$data = $this->db->query("SELECT mb.* FROM
	message_boards mb
	JOIN board_participants p ON (p.board_id = mb.id)
	WHERE p.agency_id = ".$_POST['agency']." AND mb.id IN (SELECT DISTINCT(board_id) FROM board_participants WHERE customer_id = ".$customer['id'].") and isnull(event_id) and isnull(booking_id)", NULL, 1);
	
				if(!empty($data)) {
					$board =  new MessageBoard();
					$board->populate($data);	
				}
				else{
					// create board
					$agency = new Agency($_POST['agency']);
					
					if($agency->ready()){
						
						if($agency->customer_id == $customer['id']){
							$this->error = "Can not open a message board for yourself";
							return false;	
						}
					
						$board = $this->createBoard();
						
						$board->addParticipant($agency->customer_id, NULL, $agency->id);
						
						if(!empty($profile)){
							
							if($profile['type'] == 't'){
								$board->addParticipant($customer['id'], $profile['id']);
							}
							else{
								$board->addParticipant($customer['id'], NULL, $profile['id']);
							}
						}
						else{
							$board->addParticipant($this->user->id);
						}
						
					}
					else{
						$this->error = "Invalid agency ID";	
						return false;	
					}
				}
		
			}
			else{
				$this->error = "Invalid agency ID";
				return false;	
			}
			
		}
		elseif (!empty($_POST['customer'])){
			
			if(is_numeric($_POST['customer']) && $customer['id'] != $_POST['customer']){

				$profile = Session::getProfile();
			
				if(!empty($profile)) {
					
					if($profile['type'] == 't'){
						
						$data = $this->db->query("SELECT mb.* FROM
		message_boards mb
		JOIN board_participants p ON (p.board_id = mb.id and isnull(talent_id) and isnull(agency_id))
		WHERE p.customer_id = ".$_POST['customer']."  AND mb.id IN (SELECT DISTINCT(board_id) FROM board_participants WHERE talent_id = ".$profile['id'].") and isnull(mb.event_id) and isnull(mb.booking_id)", NULL, 1);
						
					}
					else{
						$data = $this->db->query("SELECT mb.* FROM
		message_boards mb
		JOIN board_participants p ON (p.board_id = mb.id and isnull(talent_id) and isnull(agency_id))
		WHERE p.customer_id = ".$_POST['customer']." AND mb.id IN (SELECT DISTINCT(board_id) FROM board_participants WHERE agency_id = ".$profile['id'].") and isnull(mb.event_id) and isnull(mb.booking_id)", NULL, 1);
					}
					
					
					if(!empty($data)) {
						$board =  new MessageBoard();
						$board->populate($data);	
					}
					else{
						// create board						
						$tcustomer = new Customer($_POST['customer']);
						
						if($tcustomer->ready()){
														
							$board = $this->createBoard();						
							$board->addParticipant($tcustomer->id);
							
							if($profile['type'] == 't'){
								$board->addParticipant($customer['id'], $profile['id']);
							}
							else{
								$board->addParticipant($customer['id'], NULL, $profile['id']);
							}
							
						}
						else{
							$this->error = "Invalid customer ID";
							return  false;
						}
					}
					
					
				}
				else{
					$this->error = "Please switch to talent or agency profile";	
					return  false;
				}
			
			}
			else{
				$this->error = "Invalid customer ID";
				return  false;	
			}
				
			
		}
		
		
		return $board;
		
	}
	
	
	
	/**
	* Send a chat message *
	*/
	public function send ($board_id, $body, $quote_id = NULL ) {
		
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
		
		$participants = $this->db->query("SELECT b.board_id, b.customer_id, e.id AS email_id, e.email , p.id AS phone_id, p.phone, p.country_code, 
(SELECT push_id FROM customer_devices WHERE customer_id = c.id AND !ISNULL(push_id) AND ISNULL(deleted) ORDER BY id LIMIT 0,1) AS push1,
(SELECT push_id FROM customer_devices WHERE customer_id = c.id AND !ISNULL(push_id) AND ISNULL(deleted) ORDER BY id LIMIT 1,1) AS push2,
(SELECT push_id FROM customer_devices WHERE customer_id = c.id AND !ISNULL(push_id) AND ISNULL(deleted) ORDER BY id LIMIT 2,1) AS push3,
(SELECT alert7_email FROM customer_settings WHERE customer_id = c.id ) AS alert,
(SELECT COUNT(*) FROM notifications WHERE customer_id = b.customer_id AND seen = 0 AND ref = b.id AND `type` = 'b') AS notified
FROM board_participants b
JOIN customers c ON (c.id = b.customer_id)
LEFT JOIN emails e ON (e.customer_id = c.id AND e.is_primary = 1 AND ISNULL(e.deleted))
LEFT JOIN phones p ON (p.customer_id = c.id AND p.is_primary = 1 AND ISNULL(p.deleted))
WHERE b.board_id = {$board_id} AND ISNULL(b.deleted)");
		
		if(empty($participants)){
			$this->error = "Invalid board ID";
			return false;
		}
		
		$participants = Util::groupArray($participants,'customer_id');
		
		if(!isset($participants[$customer['id']])){
			$this->error = "You do not have permission to send message to this board";
			return false;
		}
		
		$body = substr(strip_tags($body, '<br>'), 0, 2000);
						
		$message = new Message();
		$message->board_id = $board_id;
		$message->customer_id = $customer['id'];
		$message->message = $body ;// do it differently
		$message->originated = (!empty($profile)) ? $profile['type'] : 'c';
		//$message->edited = Validator::cleanup($_POST['message_edited'], 100);
		//$message->is_control = Validator::cleanup($_POST['message_is_control'], 100);
		
		if(!empty($quote_id) && is_numeric($quote_id)) {
			$message->quote_id = Validator::cleanup($quote_id, 16);
		}
		$message->save();
		
		$this->message = $message;
		
		$com = new ComEngine();
		
		// add receipients
		foreach ($participants as $cid => $res){
						
			$receipient = new MessageRecipient();
			$receipient->message_id = $message->id;
			$receipient->customer_id = $cid;
			$receipient->board_id = $board_id;
			$receipient->seen = ($cid == $customer['id']) ? 1 : 0;
			$receipient->notified = 0;
			$receipient->save();
			
			if($cid != $customer['id'] && $res['notified'] == 0) {

				// notify receipients
				$com->newMessage($res, $message);
			
			}
		}
		
		return true;		
		
	}
	
	/**
	* Get new messages or initialize chat board with latest messages
	*/
	public function getUpdates ( $last_id, $board_id = NULL) {
		
		$customer = Session::getCustomer();
		
		$cond = array();
		$cond[] = "r.customer_id = '".$customer['id']."'";
		$cond[] = "r.message_id > ".$last_id;
		$cond[] = "isnull(r.deleted) ";
		
		if(!empty($board_id)){
			$cond[] = "m.board_id =	".$board_id;
		}
		
		$data = $this->db->query("SELECT m.id, r.id as r, m.board_id as b, m.customer_id AS s, IF(isnull(m.deleted),m.message,'') AS m, m.edited AS e, DATE_FORMAT(m.created,'%b %e') AS d, TIME_FORMAT(m.created,'%h:%i%p') AS t , m.quote_id AS qid, q.customer_id AS qs, 
SUBSTR(q.message, 0,20) AS qm, IF(isnull(m.deleted),0,1) as del
FROM messages m
JOIN message_recipients r ON (r.message_id = m.id)
LEFT JOIN messages q ON (q.id = m.quote_id AND ISNULL(q.deleted))
WHERE ".implode(" AND ", $cond )." 
ORDER BY id desc", NULL, 10);
		
		if(!empty($data)) {
			$data = array_reverse($data);
		}
		
		if(empty($board_id)){
			$data = Util::groupMultiArray($data,'b');	
		}
		
		return $data;
		
	}
	/**
	* Get new messages or initialize chat board with latest messages
	*/
	public function getHistory ( $first_id, $board_id = NULL) {
		
		$customer = Session::getCustomer();
		
		$cond = array();
		$cond[] = "r.customer_id = '".$customer['id']."'";
		$cond[] = "r.message_id < ".$first_id;
		
		if(!empty($board_id)){
			$cond[] = "m.board_id =	".$board_id;
		}
		
		$data = $this->db->query("SELECT m.id, r.id as r, m.board_id as b, m.customer_id AS s, IF(isnull(m.deleted),m.message,'') AS m, m.edited AS e, DATE_FORMAT(m.created,'%b %e') AS d, TIME_FORMAT(m.created,'%h:%i%p') AS t , m.quote_id AS qid, q.customer_id AS qs, 
SUBSTR(q.message, 0,20) AS qm, IF(isnull(m.deleted),0,1) as del
FROM messages m
JOIN message_recipients r ON (r.message_id = m.id)
LEFT JOIN messages q ON (q.id = m.quote_id AND ISNULL(q.deleted))
WHERE ".implode(" AND ", $cond )." 
ORDER BY id desc", NULL, 10);
		
				
		if(empty($board_id)){
			$data = Util::groupMultiArray($data,'b');	
		}
		
		return $data;
		
	}
	
	
	
	
	/**
	* Create a message board
	*/
	private function createBoard ($event_id = NULL, $booking_id = NULL) {
		
		$customer = Session::getCustomer();
		
		$board = new Messageboard();
		
		if(!empty($event_id)) {
			$board->event_id = $event_id;
		}
		
		if(!empty($booking_id)) {
			$board->booking_id = $booking_id;	
		}
		
		$board->created_by = $customer['id'];
		$board->save();
				
		return $board;
		
	}
	
	/**
	* Update seen status
	*/
	public function ack ($board_id, $last_id){
		
		$customer = Session::getCustomer();
		
		$this->db->execute("update message_recipients set seen = 1, modified = NOW() where customer_id = ".$customer['id']." and board_id = {$board_id} and message_id <= {$last_id}");
		
	}
	

}

?>