<?php

/**********************************************
* Customer Class
**********************************************/

class Customer extends Model{

	public $table = 'customers';

	public $fields = array(
		'id',
		'firstname',
		'lastname',
		'name_changed',
		'dob',
		'location_id',
		'timeoffset',
		'timezone',
		'currency',
		'image_id',
		'active',
		'gender',
		'is_trans',
		'language',
		'stripe_id',
		'ct_confirmed',
		'created',
		'modified',
		'deleted'
	);
	
	
	/*
	* Check if user recently registered
	*/
	public function isNew() {
		return (time()-strtotime($this->data['created']) < 7*24*86400);
	}
	
	


	public function getDP () {
	
		if(!empty($this->data['image_id'])){
			return new Image($this->data['image_id']);
		}
		
		return NULL;
	}
	
	
	/**
	* Get profiles
	*/
	public function getProfiles ($as_array = false){
		
		if($as_array){
		
			$data = $this->db->query("SELECT p.id, p.name, p.is_agency, i.thumb, i.id as dpid, p.gender, pp.handle, GROUP_CONCAT(k.name SEPARATOR ', ') AS skills, commitment, agency_type
	FROM profiles p
	left join pages pp ON (pp.profile_id = p.id)
	LEFT JOIN images i ON (i.id = p.image_id)
	LEFT JOIN categories c ON (c.profile_id = p.id AND ISNULL(c.deleted))
	LEFT JOIN skills k ON (k.id = c.skill_id)
	WHERE  p.customer_id = {$this->data['id']} AND p.active = 1 AND ISNULL(p.deleted) GROUP BY p.id", NULL);

			return $data;
		}
		
		
		$data = $this->db->query("select * from profiles where customer_id = {$this->data['id']} and isnull(deleted)");

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Profile();
				$obj->populate($item);
				$out[] = $obj;
			}
		}

		return $out;		
	}
	
	/**
	* Get number of active profiles 
	*/
	public function getActiveProfilesCount () {
		
		$data = $this->db->query("SELECT COUNT(*) as total FROM profiles WHERE customer_id = {$this->data['id']} ", NULL, 1);
		//AND ISNULL(deleted) AND active = 1
		
		return $data['total'];
	}
	
	/**
	* Get accessible profiles
	*/
	public function getDisplayProfiles () {
		
		$profiles = $this->db->query("SELECT a.*, l.city, l.region, l.country, i.thumb, i.i400, i.url, i.id as dpid, p.handle
FROM profiles a
JOIN locations l ON (l.id = a.location_id)
LEFT JOIN images i ON (i.id = a.image_id)
LEFT JOIN pages p ON (p.profile_id = a.id)
WHERE a.customer_id = ".$this->data['id']." and isnull(a.deleted)", NULL, 1);
	

		$customer = $this->db->query("SELECT  l.city, l.region, l.country, i.thumb, i.i400, i.url
FROM customers c
JOIN locations l ON (l.id = c.location_id)
LEFT JOIN images i ON (i.id = c.image_id)
WHERE c.id = ".$this->data['id']." ", NULL, 1);

		$customer['name'] = $this->data['firstname'].' '.$this->data['lastname'];


		$customer = array_merge($this->data, $customer);
		
		return array(
			'customer' => $customer,
			'agency' => $agency,
			'talent' => $talent
		);
		
		
	}
	

	/**
	* Get associated bookings
	* Returns array of booking objects or empty array 
	*/

	/**
	* Get bookings
	* Retuns buffer object
	*/
	public function getBookings ( $status = NULL, $date = NULL) {
				
		return App::getBookings();


	}



	/**
	* Get associated talent
	* Returns  talent object or null
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL, 1);


		if(!empty($data)){
			
			$obj = new talent();
			$obj->populate($data);
			return $obj;
		}


		return false;
	}
	
	/**
	* Get talents (including agency talents
	*/
	public function getTalents ($as_array = false) {
		
		
		if($as_array) {
			
			return $this->db->query("
SELECT t.* , l.city, l.region, l.country, i.i400, i.thumb, p.id AS page_id, p.handle, p.active AS page_active,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id and active = 1 LIMIT 0,1 ) AS t1,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id and active = 1 LIMIT 1,1 ) AS t2,
(SELECT skill_id FROM subscriptions WHERE talent_id = t.id and active = 1 LIMIT 2,1 ) AS t3
FROM talents t 
LEFT JOIN images i ON (i.id = t.image_id)
LEFT JOIN locations l ON (l.id = t.location_id)
LEFT JOIN pages p ON (p.talent_id = t.id)
WHERE t.customer_id = ".$this->data['id']." ");
			
		}
		else {
			
			$data = $this->db->query("SELECT t.* FROM talents t 
	LEFT JOIN agencies a ON (a.customer_id = ".$this->data['id'].")
	WHERE t.customer_id = ".$this->data['id']." OR t.agency_id = a.id order by ");
	
			if(!empty($data)){
				
				$out = array();
				
				foreach ($data as $item) {
					$obj = new talent();
					$obj->populate($data);
					$out[] = $obj;
				}
				
				return $out;
			}
		
		}
		
		
		return NULL;
	}



	/**
	* Get associated agency
	* Returns  agency object or null
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL,1);

		if(!empty($data)){
			
			$obj = new agency();
			$obj->populate($data);
			return $obj;
		}


		return false;
	}
	
	/**
	* Check if customer has access to the given current profile
	*/
	public function hasProfileAccess (){
		
		$profile = Session::getProfile();
		
		if(empty($profile)){
			return false;	
		}
		
		if($profile['type'] == 'a'){
			return $this->hasAgencyAccess($profile['id']);	
		}
		elseif($profile['type'] == 't'){
			return $this->hasProfileAccess($profile['id']);
		}
		
		return false;
	}
		

	/**
	* Get associated location
	* Returns location object or NULL 
	*/

	public function getLocation (){

		$data = $this->db->query("SELECT * FROM locations WHERE id = ".$this->data['location_id'], NULL, 1);

		if(!empty($data)){
			$obj = new location();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated codes
	* Returns array of code objects or empty array 
	*/

	public function getCodes (){

		$data = $this->db->query("SELECT * FROM codes WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL,12);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new code();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated logins
	* Returns array of login objects or empty array 
	*/

	public function getLogins (){

		$data = $this->db->query("SELECT * FROM logins WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new login();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated emails
	* Returns array of email objects or empty array 
	*/

	public function getEmails (){

		$data = $this->db->query("SELECT * FROM emails WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new email();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	

	/**
	* Get primary or last verified email address
	*/
	public function getPrimatyEmail () {
				
		$data = $this->db->query("SELECT * FROM emails WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by is_primary desc, is_verified desc, modified", NULL, 1);
		
		if(!empty($data)){
			
			$obj = new email();
			$obj->populate($data);
			return $obj;
		}
		
		return false;
	}


	/**
	* Get associated phones
	* Returns array of phone objects or empty array 
	*/

	public function getPhones (){

		$data = $this->db->query("SELECT * FROM phones WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new phone();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get specific verified mobile phone 
	* pass NULL for each parameter to exclude 
	*/
	public function getPhone ( $is_primary = 1, $is_public = 0, $sms = NULL, $whatsapp = NULL){
		
		$con = array();
		$con[] = "customer_id = ".$this->data['id'];
		$con[] = "is_verified = 1";
		$con[] = "isnull(deleted)";
		
		if(!is_null($is_primary)){
			$con[] = "is_primary = ".$is_primary;
		}
		
		if(!is_null($is_public)){
			$con[] = "is_public = ".$is_public;
		}
		
		if(!is_null($sms)){
			$con[] = "sms = ".$sms;
		}
		
		if(!is_null($whatsapp)){
			$con[] = "whatsapp = ".$whatsapp;
		}
		
		
		$data = $this->db->query("select * from phones where ".implode(" and ", $con), NULL, 1);
		
		if(!empty($data)){
			$phone = new Phone();
			$phone->populate($data);
			return $phone;
		}
		
		return NULL;
	}
	
	/**
	* Update primary phone preferences
	*/
	public function update_phone_preference ($privat, $public) {
		
		$this->db->execute("update phones set is_primary = 0 where customer_id = ".$this->data['id']);
		
		$this->db->execute("update phones set is_primary = 1, is_public = 0 where customer_id = ".$this->data['id']." and id = ".$privat );
		
		if(!empty($public)) {
			$this->db->execute("update phones set is_primary = 1, is_public = 1 where customer_id = ".$this->data['id']." and id = ".$public );
		}
		
	}
	
	
	/**
	* Get customer contact data
	*/
	public function getContact () {
		
		return $this->db->query("SELECT firstname, lastname, e.email, p.phone, p.country_code, l.city, l.region, l.country, l.postalcode
FROM customers c
JOIN emails e ON (e.customer_id = c.id AND ISNULL(e.deleted))
JOIN phones p ON (p.customer_id = c.id AND ISNULL(p.deleted))
JOIN locations l ON (l.id = c.location_id)
WHERE c.id = ".$this->data['id'], NULL, 1);
	}



	/**
	* Get associated images
	* Returns array of image objects or empty array 
	*/

	public function getImages () {
		
		$sql = "SELECT id,url, thumb, i400, created, deleted FROM images WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by created desc";

		
		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = "SELECT count(*) as total FROM images WHERE customer_id = ".$this->data['id'];
		$buffer->_component = "customer/images";
		$buffer->_pagesize = 50;
		return $buffer;
		
	}




	/**
	* Get associated pages
	* Returns array of page objects or empty array 
	*/

	public function getPages (){

		$data = $this->db->query("SELECT * FROM pages WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new page();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}






	/**
	* Get associated messages
	* Returns array of message objects or empty array 
	*/

	public function getMessages (){

		$data = $this->db->query("SELECT * FROM messages WHERE to_customer = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new message();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated ledgers
	* Returns array of ledger objects or empty array 
	*/

	public function getLedgers (){

		$data = $this->db->query("SELECT * FROM ledgers WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new ledger();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated payout_methods
	* Returns array of payout_method objects or empty array 
	*/

	public function getPayoutMethods (){

		$data = $this->db->query("SELECT * FROM payout_methods WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new payoutMethod();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated booking_trail
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrail (){

		$data = $this->db->query("SELECT * FROM booking_trail WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated booking_trails
	* Returns array of booking_trail objects or empty array 
	*/

	public function getBookingTrails (){

		$data = $this->db->query("SELECT * FROM booking_trails WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new bookingTrail();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated events
	* Returns array of event objects or empty array 
	*/

	public function getEvents ( $as_array = false, $page = 1){
		
		$skip = ($page-1)*100;
		
		if($as_array){
		
			return $this->db->query("SELECT e.*, l.place_id, l.venue, l.address, l.city, l.region, l.postalcode, l.country, a.name AS agency_name, DATE(begins) as  event_date
FROM `events` e
JOIN locations l ON (l.id = e.location_id)
LEFT JOIN profiles a ON (a.id = e.agency_id) WHERE e.customer_id = ".$this->data['id']." and isnull(e.deleted) and begins > NOW() order by begins asc", NULL,100, $skip);
		
		}

		$data = $this->db->query("SELECT * FROM events WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by begins asc", NULL, 100, $skip);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new event();
				$obj->populate($item);
				$out[] = $obj;
			}
		}

		return $out;
	}
	
	/**
	* Search events
	*/
	public function searchEvents ($filter, $year, $month, $agency_id = NULL){
		
		$conditions = array();
		
		if(empty($filter)){
		// browse	
			$conditions[] = "((YEAR(e.begins) = {$year} and MONTH(e.begins) = {$month}) or (YEAR(e.ends) = {$year} and MONTH(e.ends) = {$month}))";		
			
			$order = "begins asc";
		}
		elseif($filter == 2){
		// previous	
			$dt = Util::ToSysDate(date("Y-m-d"));
			
			$conditions[] = "e.begins  < '{$dt}'";
			
			$order = "begins desc";
		}
		else{
			// upcomming
			$dt = Util::ToSysDate(date("Y-m-d"));
			
			$conditions[] = "(e.begins >= '{$dt}' or e.ends >= '{$dt}')";
			
			$order = "begins asc";
		}
		
		if(!empty($agency_id)){
			$profile = Session::getProfile();
			$conditions[] = "e.agency_id = ".$agency_id;	
		}
		else{
			$conditions[] = "e.customer_id = ".$this->data['id'];	
		}
		
		
		$sql = "SELECT e.*, DATE(e.begins) as event_date, l.address, l.city, l.region, l.postalcode, l.country, t.name AS type_name, a.name AS agency_name,
		(SELECT COUNT(*) FROM bookings WHERE event_id = e.id AND ISNULL(deleted)) AS bookings
FROM `events` e
JOIN locations l ON (l.id = e.location_id)
JOIN event_types t ON (t.id = e.event_type)
LEFT JOIN profiles a ON (a.id = e.agency_id) 
WHERE ".implode(" and ", $conditions)." and isnull(e.deleted) order by ".$order;

		//echo $sql;

		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = "select count(*) as total  FROM `events` e WHERE ".implode(" and ", $conditions)." and isnull(e.deleted)";
		
		$buffer->_component = "event/events";
		$buffer->_pagesize = 10;
		return $buffer;
	}
	
	
	/**
	* Get a brief infomation about customer
	* This helps to identify in-complete areas and draw atention
	*/
	public function getInfo () {
		
		$out = array();
		
		$data = $this->db->query("SELECT c.id AS customer__id, c.firstname AS customer__firstname, c.lastname AS customer__lastname, c.name_changed AS customer__name_changed, c.dob AS customer__dob, c.location_id AS customer__location_id, c.timeoffset AS customer__timeoffset, c.timezone AS customer__timezone, c.currency AS customer__currency, c.image_id AS customer__image_id, c.active AS customer__active, c.gender AS customer__gender, c.is_trans AS customer__is_trans, c.ct_confirmed as customer__ct_confirmed, c.created AS customer__created, c.modified AS customer__modified, c.deleted AS customer__deleted,
cl.id AS customer_location__id, cl.address AS customer_location__address, cl.city AS customer_location__city, cl.region AS customer_location__region, cl.postalcode AS customer_location__postalcode, cl.country AS customer_location__country, cl.locked AS customer_location__locked, cl.created AS customer_location__created, cl.modified AS customer_location__modified, cl.deleted AS customer_location__deleted,
t.id AS profiles__id, t.customer_id AS profiles__customer_id, t.commitment as profiles__commitment, t.agency_type as profiles__agency_type,  t.verified AS profiles__verified, t.name AS profiles__name, t.location_id AS profiles__location_id, t.timeoffset AS profiles__timeoffset, t.currency AS profiles__currency,  t.is_agency AS profiles__is_agency,  t.gender AS profiles__gender,  t.image_id AS profiles__image_id,  t.limited_hours AS profiles__limited_hours,  t.active AS profiles__active, t.created AS profiles__created, t.modified AS profiles__modified, t.deleted AS profiles__deleted,
tl.id AS profile_location__id, tl.address AS profile_location__address, tl.city AS profile_location__city, tl.region AS profile_location__region, tl.postalcode AS profile_location__postalcode, tl.country AS profile_location__country, tl.locked AS profile_location__locked, tl.created AS profile_location__created, tl.modified AS profile_location__modified, tl.deleted AS profile_location__deleted,
tp.id AS page__id, tp.customer_id AS page__customer_id, tp.profile_id AS page__profile_id,  tp.handle AS page__handle, tp.banner_id AS page__banner_id, tp.description AS page__description, tp.active AS page__active,  tp.published as page__published, tp.updated as page__updated, tp.created AS page__created, tp.modified AS page__modified, tp.deleted AS page__deleted,
(SELECT COUNT(*)  FROM phones WHERE customer_id = c.id AND ISNULL(deleted)) AS customer__phones,
(SELECT COUNT(*)  FROM emails WHERE customer_id = c.id AND ISNULL(deleted)) AS customer__emails,
(SELECT COUNT(*) FROM invoices WHERE customer_id = c.id AND `status` = 'p' AND due_date > NOW()) AS customer__invoices
FROM customers  c 
JOIN locations  cl ON ( cl.id = c.location_id)
LEFT JOIN profiles  t ON ( t.customer_id = c.id and isnull(t.deleted))
LEFT JOIN locations  tl ON ( tl.id = t.location_id)
LEFT JOIN pages  tp ON ( tp.profile_id = t.id)
LEFT JOIN images i on (i.id = t.image_id)
WHERE c.id = ".$this->data['id']);
		
		$out = array('customer' => []);
		$ids = array();
		
		foreach ($data as $i => $item){
			
		
			foreach ($item as $k => $v){

				list($t, $f) = explode('__',$k);
				
				if($t == 'page'){
					$out['profiles'][$i]['page'][$f] = $v;
					
				}
				elseif($t == 'profiles'){
					
					$out['profiles'][$i][$f] = $v;
				}
				elseif($t == 'customer_location'){
					$out['customer']['location'][$f] = $v;
					
				}
				elseif($t == 'profile_location'){
					$out['profiles'][$i]['location'][$f] = $v;
					
				}
				else{
					$out[$t][$f] = $v;	
				}
				
				if($k == 'profiles__id' && !empty($v)){
					$ids[] = $v;
				}

			}
		}
		
		// get packages	in category	
		$packages = [];
		$catids = [];
			
		if(!empty($ids)){
			
			$packages = $this->db->query("SELECT c.profile_id, c.id, s.name AS skill_name, c.skill_id, p.id AS package_id, p.name AS package_name, p.fee, p.fee_local, p.timespan
FROM categories c 
JOIN skills s ON (s.id = c.skill_id)
LEFT JOIN talent_packages p ON (p.category_id = c.id AND p.active = 1 AND ISNULL(p.deleted))
WHERE c.profile_id in  (".implode(',',$ids).") AND  ISNULL(c.deleted)");
			
			
			if(!empty($packages)){	
				$catids = Util::groupArray($packages, 'id');
				$packages = Util::groupMultiArray($packages, 'profile_id');
				$catids = array_keys($catids);
			}
		}
		
		// get subscriptions in category
		$subscriptions = [];
		if(!empty($catids)){
			
			$subs = $this->db->query("SELECT s.id, s.category_id, s.package_id,s.begin_date, s.sub_date, s.begin_date, s.exp_date, s.jobs, s.manage,
(SELECT COUNT(*) FROM bookings WHERE subscription_id = s.id AND !ISNULL(responded_date) AND free_quota = 0 ) AS used,
p.name AS package_name, p.price
FROM subscriptions s
JOIN packages p ON (p.id = s.package_id)
WHERE s.exp_date >= NOW() AND s.active = 1 AND ISNULL(s.deleted) AND s.category_id in (".implode(',', $catids).") ORDER BY begin_date");
			
			$subscriptions = Util::groupMultiArray($subs, 'category_id');
		}
				
		if(!empty($out['profiles'])){
			
			foreach ($out['profiles'] as $i => $profile){
				
				if(empty($profile['id'])){
					unset($out['profiles'][$i]);
				}
				
			
				if(isset($packages[$profile['id']])){
					
					$cats = [];
					
					foreach ($packages[$profile['id']] as $pkg) {
						
						if(!isset($cats[$pkg['skill_id']])){
							$cats[$pkg['skill_id']] = array(
								'id' => $pkg['id'],
								'skill_id' => $pkg['skill_id'],
								'name' => $pkg['skill_name'],
								'packages' => [],
								'subscriptions' => isset($subscriptions[$pkg['id']]) ? $subscriptions[$pkg['id']] :[]
								
							);
						}
						
						if(!empty($pkg['package_id'])){
							$cats[$pkg['skill_id']]['packages'][] = $pkg;	
						}
											
					}
					
					$out['profiles'][$i]['categories'] = array_values($cats);
				}
			}						
		}
			
		
		return $out;
	}



	/**
	* Get associated payment_methods
	* Returns array of payment_method objects or empty array 
	*/

	public function getPaymentMethods (){

		$data = $this->db->query("SELECT * FROM payment_methods WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new paymentMethod();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get payment method for given stripe method id
	*/
	public function getPaymentMethod ($method_id){
		
		$data = $this->db->query("SELECT * FROM payment_methods WHERE customer_id = {$this->data['id']} AND method_id = '{$method_id}'", NULL, 1);
		
		if(!empty($data)){
			
			$obj = new paymentMethod();
			$obj->populate($data);
			return $obj;
			
		}
		
		
		return false;
	}
	
	/**
	* Get most recently used payment method
	*/
	public function getRecentPaymentMethod () {
		
		$data = $this->db->query("SELECT * FROM payment_methods WHERE customer_id = {$this->data['id']} and isnull(deleted) order by id desc", NULL, 1);
		
		if(!empty($data)){
			
			$obj = new paymentMethod();
			$obj->populate($data);
			return $obj;
			
		}
		
		
		return false;
		
	}



	/**
	* Get associated page_views
	* Returns array of page_view objects or empty array 
	*/

	public function getPageViews (){

		$data = $this->db->query("SELECT * FROM page_views WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new pageView();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated board_participants
	* Returns array of board_participant objects or empty array 
	*/

	public function getBoardParticipants (){

		$data = $this->db->query("SELECT * FROM board_participants WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new boardParticipant();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated message_recipients
	* Returns array of message_recipient objects or empty array 
	*/

	public function getMessageRecipients (){

		$data = $this->db->query("SELECT * FROM message_recipients WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new messageRecipient();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated message_boards data
	* Returns buffer object
	*/

	public function getMessageBoards (){
		
		
		$query = "
SELECT b.id AS board_id, b.event_id, e.name AS event_name, b.booking_id, k.name AS booking_name,  p.profile_id, t.name AS profile_name, t.gender AS profile_gender, ti.thumb AS profile_image,
CONCAT(s.firstname,' ',s.lastname) AS customer_name, si.thumb AS customer_image,
(SELECT COUNT(*) FROM message_recipients WHERE board_id = b.id AND customer_id = s.id AND seen = 0 AND ISNULL(deleted)) AS unseen,
(SELECT MAX(created) FROM message_recipients WHERE board_id = b.id AND customer_id = s.id AND  ISNULL(deleted)) AS lastsend
FROM message_boards b
JOIN board_participants p ON (p.board_id = b.id  AND ISNULL(p.deleted))
JOIN customers s ON (s.id = p.customer_id)
LEFT JOIN profiles t ON (t.id = p.profile_id)
LEFT JOIN `events` e ON (e.id = b.event_id)
LEFT JOIN bookings k ON (k.id = b.booking_id)
LEFT JOIN images ti ON (ti.id = t.image_id)
LEFT JOIN images si ON (si.id = s.image_id)
WHERE b.id IN (SELECT board_id FROM board_participants WHERE customer_id = ".$this->data['id']." AND ISNULL(deleted)) AND  ISNULL(b.deleted)
GROUP BY b.id
ORDER BY unseen DESC";



		$buffer = new Buffer();
		$buffer->_datasql = $query;
		$buffer->_countsql = "SELECT COUNT(*) AS total FROM board_participants WHERE customer_id = ".$this->data['id']." AND ISNULL(deleted)";
		$buffer->_component = "customer/messages";
		$buffer->_pagesize = 50;
		$buffer->_page = 1; 
		return $buffer;

	}



	/**
	* Get associated blocked_contacts
	* Returns array of blocked_contact objects or empty array 
	*/

	public function getBlockedContacts (){

		$data = $this->db->query("SELECT * FROM blocked_contacts WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new blockedContact();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


	/**
	* Take a snap shot of current customer data
	*/
	public function snap () {
	
		$inf = $this->getInfo();
		return new CustomerSnap($inf);
			
	}
	
	/**
	* Get like or unlike record for given page
	*/
	public function updateLike ($page_id) {
		
		$data = $this->db->query("SELECT * FROM likes WHERE customer_id = ".$this->data['id']." AND page_id = ".$page_id, NULL , 1);
		
		if(!empty($data)){
			
			$like = new Like();
			$like->populate($data);
			//print_r($like);
			if($like->isDeleted()){
				$like->undelete();
				return true;	
			}
			else{
				$like->delete();
				return false;	
			}
		}
		else{
			$like = new Like();
			$like->customer_id = $this->data['id'];
			$like->page_id = $page_id;
			$like->save();	
			return true;
		}
		
	}
	
	/**
	* Get associated likes
	* Returns array of like objects or empty array 
	*/

	public function getLikes (){

		$data = $this->db->query("SELECT * FROM likes WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new like();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get associated customer_settings
	* Returns array of customer_setting objects or empty array 
	*/

	public function getCustomerSettings (){

		$data = $this->db->query("SELECT * FROM customer_settings WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL,1);

		$obj = new CustomerSetting();
		
		if(!empty($data)){
						
			$obj->populate($data);
		}
		else{
			
			$obj->customer_id = $this->data['id'];
			$obj->save();	
		}

		return $obj;
	}



	/**
	* Get associated customer_devices
	* Returns array of customer_device objects or empty array 
	*/

	public function getCustomerDevices (){

		$data = $this->db->query("SELECT * FROM customer_devices WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new customerDevice();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get device metch with given fingureprint
	*/
	public function getDevice ($fingureprint) {
		
		if(is_numeric($fingureprint)){
			
			$data = $this->db->query("SELECT * FROM customer_devices WHERE customer_id = ".$this->data['id']." AND fingureprint = '".$fingureprint."' and isnull(deleted)", NULL, 1);
			
			if(!empty($data)){
				
				$obj = new customerDevice();
				$obj->populate($data);
				return $obj;
			}
			
		}
		
		
		
		return false;	
	}
	
	/**
	* Remove records from given device
	*/
	public function removeDeviceLogins ($device_id){
	
		$this->db->execute("UPDATE customer_logins SET deleted = NOW() WHERE customer_id = ".$this->data['id']." AND device_id = ".$device_id);
		
	}



	/**
	* Get associated customer_logins
	* Returns array of customer_login objects or empty array 
	*/

	public function getCustomerLogins (){

		$data = $this->db->query("SELECT * FROM customer_logins WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new customerLogin();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get associated notifications
	* Returns array of notification objects or empty array 
	*/

	public function getNotifications (){

		$data = $this->db->query("SELECT * FROM notifications WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new notification();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


	/**
	* acknowladge notification seen
	*/
	public function ack ($notification_id){
		
		$this->db->execute("UPDATE notifications SET seen = 1, modified = NOW() WHERE customer_id = ".$this->data['id']." AND id <= ".$notification_id);
		
	}
	


	/**
	* Get associated agenda_shares
	* Returns array of agenda_share objects or empty array 
	*/

	public function getAgendaShares (){

		$data = $this->db->query("SELECT * FROM agenda_shares WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agendaShare();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customer_cares
	* Returns array of customer_care objects or empty array 
	*/

	public function getCustomerCares (){

		$data = $this->db->query("SELECT * FROM customer_cares WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new customerCare();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated invoices
	* Returns array of invoice objects or empty array 
	*/

	public function getInvoices (){

		$data = $this->db->query("SELECT * FROM invoices WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new invoice();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Get pendeing invoice details
	*/
	public function getPendingInvoices () {
		
		$data = $this->db->query("SELECT i.id AS invoice__id, 
i.type AS invoice__type, 
i.customer_id AS invoice__customer_id, 
i.profile_id AS invoice__profile_id, 
i.subscription_id AS invoice__subscription_id, 
i.amount AS invoice__amount, 
i.currency AS invoice__currency, 
i.due_date AS invoice__due_date, 
i.effective_date AS invoice__effective_date, 
i.status AS invoice__status, 
i.created AS invoice__created, 
i.modified AS invoice__modified, 
i.deleted AS invoice__deleted,
p.id AS profile__id, 
p.customer_id AS profile__customer_id, 
p.is_agency AS profile__is_agency, 
p.verified AS profile__verified, 
p.location_id AS profile__location_id, 
p.image_id AS profile__image_id, 
p.agency_type AS profile__agency_type, 
p.gender AS profile__gender, 
p.currency AS profile__currency, 
p.timezone AS profile__timezone, 
p.timeoffset AS profile__timeoffset, 
p.name AS profile__name, 
p.name_changed AS profile__name_changed, 
p.limited_hours AS profile__limited_hours, 
p.active AS profile__active, 
p.commitment AS profile__commitment, 
p.created AS profile__created, 
p.modified AS profile__modified, 
p.deleted AS profile__deleted,
s.id AS subscription__id, 
s.category_id AS subscription__category_id, 
s.package_id AS subscription__package_id, 
s.sub_date AS subscription__sub_date, 
s.begin_date AS subscription__begin_date, 
s.exp_date AS subscription__exp_date, 
s.active AS subscription__active, 
s.jobs AS subscription__jobs, 
s.manage AS subscription__manage, 
s.created AS subscription__created, 
s.modified AS subscription__modified, 
s.deleted AS subscription__deleted,
c.id AS category__id, 
c.profile_id AS category__profile_id, 
c.skill_id AS category__skill_id, 
c.package_id AS category__package_id, 
c.term AS category__term, 
c.renewal AS category__renewal, 
c.renewal_date AS category__renewal_date, 
c.last_renewal AS category__last_renewal, 
c.created AS category__created, 
c.modified AS category__modified, 
c.deleted AS category__deleted,
k.id AS skill__id, 
k.category AS skill__category, 
k.parent_id AS skill__parent_id, 
k.name AS skill__name, 
k.genre AS skill__genre, 
k.gender_required AS skill__gender_required, 
k.is_default AS skill__is_default, 
k.is_business AS skill__is_business, 
k.is_agency AS skill__is_agency, 
k.active AS skill__active, 
k.created AS skill__created, 
k.modified AS skill__modified, 
k.deleted AS skill__deleted,
a.id AS package__id, 
a.skill_id AS package__skill_id, 
a.name AS package__name, 
a.description AS package__description, 
a.tagline AS package__tagline, 
a.price AS package__price, 
a.price_shown AS package__price_shown, 
a.validity AS package__validity, 
a.jobs AS package__jobs, 
a.manage AS package__manage, 
a.begin_date AS package__begin_date, 
a.end_date AS package__end_date, 
a.active AS package__active, 
a.display_order AS package__display_order, 
a.selected AS package__selected, 
a.agency_only AS package__agency_only, 
a.product_id AS package__product_id, 
a.price_id AS package__price_id, 
a.created AS package__created, 
a.modified AS package__modified, 
a.deleted AS package__deleted
FROM invoices i 
LEFT JOIN profiles p ON ( p.id = i.profile_id and isnull(p.deleted))
LEFT JOIN subscriptions s ON ( s.id = i.subscription_id)
LEFT JOIN categories c ON ( c.id = s.category_id)
LEFT JOIN skills k ON ( k.id = c.skill_id)
LEFT JOIN packages a ON ( a.id = s.package_id)
WHERE i.customer_id = {$this->data['id']} and isnull(i.deleted) and i.status = '".DataType::$INVOICE_PENDING."' and i.effective_date <= NOW() and i.due_date > NOW()");
		
		$out = array();
		
		if(!empty($data)){
			
			foreach ($data as $i => $item ) {
			
				foreach ($item as $key => $value){

					list($a, $b) = explode('__', $key);
					$out[$i][$a][$b] = $value;


				}
		}
			
			
		}
		
		return $out;
	}



	/**
	* Get associated registrations
	* Returns array of registration objects or empty array 
	*/

	public function getRegistrations (){

		$data = $this->db->query("SELECT * FROM registrations WHERE customer_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new registration();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	


}
?>
