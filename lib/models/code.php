<?php
		
/****************************
* Code Class
* 
* Code type : e/p
* Ref Table : t-talents/l-logins/a-agencies/b-booker
* Code should expire in 30 mins (1800sec);
*****************************/

class Code extends Model{

	public $table = 'codes';
	
	private $key = 'YTY5NDc0ODUwZjJl';

	public $fields = array(
		'id',
		'customer_id',
		'phone_id',
		'email_id',
		'code',
		'attempts',
		'verified',
		'created',
		'modified',
		'deleted'
		);
	

	 public function create () {
		
		if ($this->status == 0) {
			// generte random code
			
			if(!empty($this->data['phone_id'])){
				$this->data['code'] = substr(mt_rand(100000, 999999)."",0,6);
			}
			else{
				// base 36: 111111 to zzzzzz
				$r = mt_rand(0, 346643856);
				$r = $r+2892332260;
				$this->data['code'] = substr(Util::base36encode($r),0,6);	
			}
		}
		
		parent::create();
		
	 }
	 
	 
	 /**
	 * Check if code expired
	 */
	 public function expired (){
		 
		 
		 $time = $this->db->query("SELECT TIME_TO_SEC(TIMEDIFF(NOW(),created)) as time FROM codes WHERE id = ".$this->data['id'], NULL, 1);
		
		return ($this->data['verified'] == 1 || $time['time'] > 3600);
		 			 
	 }
	 
	 
	 /**
	 * Check if given code is valid
	 */
	 public function validate ($code){
		 
		 $this->data['attempts'] += 1;
		 $this->save();

		if(!$this->expired() && empty($this->data['deleted']) && $this->data['attempts'] <= 4){
			
			if(strtoupper($code) == $this->data['code']){
				$this->data['verified'] = 1;
				$this->save();
												
				return true;	
			}
		}
		
		return false;
	 }
	 
	 /**
	 * Get token to pass into frotend
	 */
	 public function getToken () {
		 
		 $key = $this->data['id']."|".$this->data['created'];

		 $key = Util::encrypt($key, $this->key); 

		 $key =  base64_encode($key);
		 return urlencode($key);
		 
	 }
	 
	 /**
	 * Set token to populate object
	 */
	 public function setToken ($token){
		 
		 $token = urldecode($token);
		 $key = base64_decode(trim($token));
		 $key = Util::decrypt(trim($key), $this->key);

		 $key = explode('|', $key);
		 
		 if(!empty($key[0]) && is_numeric($key[0])) {
			 
		 	$data = $this->read($this->table, $key[0]);
			
			if($data['created'] == $key[1]){
				$this->populate($data);	
				return true;
			}
			
		 }		 
		 
		 return false;
	 }
	 
	 
	 /**
	 * Get related object
	 */
	 public function getCustomer (){
		
		return new Customer($this->data['customer_id']);
		 
	 }
	 
	

	/**
	* Get associated emails
	* Returns email object or phone based on the settings
	*/

	public function getObject (){

		if($this->data['type'] == 'e' && !empty($this->data['email_id'])){
			return new Email($this->data['email_id']);	
		}
		
		if($this->data['type'] == 'p' && !empty($this->data['phone_id'])){
			return new Phone($this->data['phone_id']);	
		}
		
		if($this->data['type'] == 'l' && !empty($this->data['login_id'])){
			return new Login($this->data['login_id']);	
		}
		
	}





	/**
	* Get associated login
	* Returns login record from active logins
	*/

	public function getLogin (){

		if(!empty($this->data['email_id'])){
			
			return $this->db->query("SELECT * FROM active_logins WHERE  email_id = ".$this->data['email_id'], NULL, 1);
			
		}
		elseif(!empty($this->data['phone_id'])){
			
			return $this->db->query("SELECT * FROM active_logins WHERE phone_id = ".$this->data['phone_id'], NULL, 1);
		}
		
		
	}



	/**
	* Get associated phone
	* Returns phone object or NULL 
	*/

	public function getPhone (){
		
		if(empty($this->data['phone_id'])){
			return false;
		}

		$data = $this->db->query("SELECT * FROM phones WHERE id = ".$this->data['phone_id'], NULL, 1);

		if(!empty($data)){
			$obj = new phone();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated email
	* Returns email object or NULL 
	*/

	public function getEmail (){
		
		if(empty($this->data['email_id'])){
			return false;
		}

		$data = $this->db->query("SELECT * FROM emails WHERE id = ".$this->data['email_id'], NULL, 1);

		if(!empty($data)){
			$obj = new email();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}

?>