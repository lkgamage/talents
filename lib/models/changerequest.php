<?php

/**********************************************
* ChangeRequest Class
**********************************************/

class ChangeRequest extends Model{

	public $table = 'change_requests';

	public $fields = array(
		'id',
		'booking_id',
		'created_by',
		'from_sender',
		'is_cancel',
		'session_start',
		'ex_start',
		'location_id',
		'ex_location',
		'package',
		'ex_package',
		'discount',
		'ex_discount',
		'approved_by',
		'approved_date',
		'status',
		'reason',
		'created',
		'modified',
		'deleted'
	);
	
	/**
	* List of changes
	*/
	public $changed = array();
	
	/** Override populate method to tract chnages **/
	public function populate ($values) {
		
		parent::populate($values);
		
		if(!empty($this->session_start)){
			$this->changed[] = 'date/time';	
		}
		
		if(!empty($this->location_id)){
			$this->changed[] = 'place';	
		}
		
		if(!empty($this->package)){
			$this->changed[] = 'package';	
		}
		
		if(!empty($this->discount)){
			$this->changed[] = 'discount';	
		}
	}
	
	
	/**
	* Check if this request about to cancel the booking
	*/
	public function isCancel () {
	
		return !empty($this->data['is_cancel']);	
		
	}


	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	public function getinfo (){

		return $this->db->query("SELECT cr.*, l.display AS new_location, xl.display AS old_location, p.name AS new_package, xp.name AS old_package, p.fee AS new_fee, xp.fee AS old_fee,
s.name AS new_skill, xs.name AS old_skill, p.timespan AS new_timespan, xp.timespan AS old_timespan
FROM change_requests cr
LEFT JOIN locations l ON (l.id = cr.location_id)
LEFT JOIN locations xl ON (xl.id = cr.ex_location)
LEFT JOIN talent_packages p ON (p.id = cr.package)
LEFT JOIN skills s ON (s.id = p.skill_id)
LEFT JOIN talent_packages xp ON (xp.id = cr.ex_package)
LEFT JOIN skills xs ON (xs.id = xp.skill_id)
WHERE cr.id = ".$this->data['id'], NULL, 1);

	}


}
?>
