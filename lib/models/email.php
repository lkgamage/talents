<?php

/**********************************************
* Email Class
**********************************************/

class Email extends Model{

	public $table = 'emails';

	public $fields = array(
		'id',
		'customer_id',
		'is_primary',
		'is_public',
		'email',
		'is_verified',
		'created',
		'modified',
		'deleted'
	);

	/*
	* Modify constructor function to accept either ID or email address
	*/
	function __construct ( $id_email = NULL) {
		
		if(!empty($id_email)) {
			
			if(is_numeric($id_email)){
				parent::__construct($id_email);	
			}
			elseif(Validator::isEmail($id_email)){
				
				parent::__construct();
				
				$data = $this->db->query("select * from emails where email = :a ", array( 'a' => $id_email), 1);
				
				if(!empty($data)){
					$this->populate($data);	
				}
			}
		}
		else{
			parent::__construct();	
		}
		
    }


	/**
	* Get associated code
	* Returns code object or NULL 
	*/

	public function getCode (){

		$data = $this->db->query("SELECT * FROM codes WHERE id = ".$this->data['vcode_id'], NULL, 1);

		if(!empty($data)){
			$obj = new code();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	
	



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated codes
	* Returns array of code objects or empty array 
	*/

	public function getCodes (){

		$data = $this->db->query("SELECT * FROM codes WHERE email_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new code();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Make the phone a primary
	*/
	public function makePrimary () {
		
		$this->db->execute("UPDATE emails SET is_primary = 0 WHERE customer_id = {$this->data['customer_id']} AND id != {$this->data['id']}");
		
		$this->db->execute("UPDATE emails SET is_primary = 1, is_verified = 1 WHERE  id = {$this->data['id']}");
		
	}


}
?>
