<?php

/**********************************************
* MessageBoard Class
**********************************************/

class MessageBoard extends Model{

	public $table = 'message_boards';

	public $fields = array(
		'id',
		'booking_id',
		'event_id',
		'created_by',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated messages
	* Returns array of message objects or empty array 
	*/

	public function getMessages (){

		$data = $this->db->query("SELECT * FROM messages WHERE board_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new message();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated board_participants
	* Returns array of board_participant objects or empty array 
	*/

	public function getBoardParticipants (){

		$data = $this->db->query("SELECT * FROM board_participants WHERE board_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new boardParticipant();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated event
	* Returns event object or NULL 
	*/

	public function getEvent (){

		$data = $this->db->query("SELECT * FROM events WHERE id = ".$this->data['event_id'], NULL, 1);

		if(!empty($data)){
			$obj = new event();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	/**
	* Get all chat info
	*/
	public function getinfo () {
		
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
						
		$participants = $this->db->query("SELECT p.id, c.id AS customer_id, p.profile_id, !ISNULL(p.deleted) AS removed, p.muted, pg.handle,
IF( !ISNULL(t.id),t.name, c.firstname)  AS `name`,
IF( !ISNULL(t.id),ti.thumb, ci.thumb)  AS `image`,
IF( !ISNULL(t.id),IF(t.is_agency = 1 , 'a', 't'), 'c' )  AS `type`
FROM board_participants p
JOIN customers c ON (c.id = p.customer_id)
LEFT JOIN images ci ON (ci.id = c.image_id)
LEFT JOIN profiles t ON (t.id = p.profile_id)
LEFT JOIN images ti ON (ti.id = t.image_id)
LEFT JOIN pages pg ON (pg.profile_id = t.id ) 
WHERE p.board_id =".$this->data['id']." ");

		$participants = Util::groupArray($participants, 'customer_id');

		$out = array(
			'name' => '',
			'image' => '',
			'type' => '',
			'link' => '',
			'muted' => false
		);

		if(!empty($this->data['event_id'])){
			// event chat room
			$event = $this->getEvent();
			
			$out = array(
				'name' => $event->name,
				'image' => Util::mapURL('/images/img-event.png'),
				'type' => 'Event',
				'link' => Util::mapURL('/dashboard/event/'.$event->id)
			);			
		}
		else{
			// booking chat room
			foreach ($participants as $cid => $part){
				
				if($cid != $customer['id']){
					
					$out = array(
						'name' => $part['name'],
						'image' => $part['image'],
						//'type' => !empty($this->data['booking_id']) ? 'Booking' : '',
						//'link' => Util::mapURL('/dashboard/link/'.$this->data['booking_id'])
					);
					
					if(!empty($this->data['booking_id'])){
						$out['link'] = Util::mapURL('/dashboard/booking/'.$this->data['booking_id']);	
						$out['type'] = 'Booking';
					}
					elseif($part['type'] != 'c'){
						$out['link'] = Util::mapURL('/'.$part['handle']);
						$out['type'] = 	($part['type'] == 't') ? 'Talent' : 'Agency';
					}
					else{
						$out['type'] = ' Customer';
						$out['link'] = NULL;
					}
					
					
				
					break;	
				}
				else{
					
				}
			}			
			
		}
		
		$out['id'] = $this->data['id'];
		$out['mute'] = !empty($participants[$customer['id']]['muted']);
		
		$out['participants'] = $participants;
		return $out;
	
		
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['created_by'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	/**
	* Add message board participants
	*/
	public function addParticipant ($customer_id, $talent_id = NULL, $agency_id = NULL){
		
		$part = new BoardParticipant();
		$part->board_id = $this->data['id'];
		$part->customer_id = $customer_id;
		
		if(!empty($talent_id)) {
			$part->talent_id = $talent_id;
		}
		
		if(!empty($agency_id)){
			$part->agency_id = $agency_id;	
		}
		
		$part->save();
		
		return $part;
	}
	
	/**
	* Get Participant record for currently logged in participant
	*/
	public function getCurrentParticipant (){
		
		$customer = Session::getCustomer();
		
		$data = $this->db->query("select* from board_participants where customer_id = ".$customer['id']." and board_id = ".$this->data['id'], NULL, 1);
		
		if(!empty($data)){
			
			$p = new BoardParticipant();
			$p->populate($data);
			return $p;	
		}
		
		return false;
	}
	
	/**
	* Get all participants
	*/
	public function getParticipants ($except_current = false) {
		
		$customer = Session::getCustomer();
		
		if(!empty($except_current)){
			$data = $this->db->query("SELECT * FROM board_participants WHERE board_id = ".$this->data['id']." and customer_id != ".$customer['id']." order by id desc", NULL);
		}
		else {
			$data = $this->db->query("SELECT * FROM board_participants WHERE board_id = ".$this->data['id']."  order by id desc", NULL);
		}

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new BoardParticipant();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
		
	}
	
	
	/**
	* Clear all mesasges for given current customer
	*/
	public function clearMessages () {
		
		$customer = Session::getCustomer();
		
		$this->db->execute("UPDATE message_recipients SET deleted = NOW() WHERE customer_id = ".$customer['id']." AND message_id IN (SELECT id FROM messages WHERE board_id = ".$this->data['id'].")");
		
			
	}
	
	/*
	* Mute chats for given customer
	*/
	public function mute () {
		
		$p = $this->getCurrentParticipant();
		$p->muted = 1;
		$p->save();
		
	}
	
	/*
	* Mute chats for given customer
	*/
	public function unMute () {
		
		$p = $this->getCurrentParticipant();
		$p->muted = 0;
		$p->save();
		
	}
	
	/**
	* Block customer
	*/
	public function block () {
	
		$participants = $this->getParticipants(true);
		$customer = Session::getCustomer();
		
		foreach ($participants as $part){
			
			$data = $this->db->query("select * from blocked_contacts where customer_id = ".$customer['id']." and blocked_id = ".$part->customer_id." and isnull(deleted)");
			
			if(empty($data)){
				$block = new BlockedContact();
				$block->customer_id = $customer['id'];
				$block->blocked_id = $part->customer_id;
				$block->save();
			}
			
			
		}
		
	}
	
	/**
	* Un-Block customer
	*/
	public function UnBlock () {
	
		$participants = $this->getParticipants(true);
		$customer = Session::getCustomer();
		
		foreach ($participants as $part){
			
			$data = $this->db->query("select * from blocked_contacts where customer_id = ".$customer['id']." and blocked_id = ".$part->customer_id." and !isnull(deleted)", NULL, 1);
			
			if(empty($data)){
				$block = new BlockedContact();
				$block->populate($data);
				$block->delete();
			}			
			
		}
		
	}
	
	/**
	* Quit from an event mesage board
	*/
	public function quit () {
		
		$customer = Session::getCustomer();
		$part = $this->getCurrentParticipant();
		$part->delete();
		
		
	}
	
	
	


	/**
	* Get associated message_recipients
	* Returns array of message_recipient objects or empty array 
	*/

	public function getMessageRecipients (){

		$data = $this->db->query("SELECT * FROM message_recipients WHERE board_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new messageRecipient();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
