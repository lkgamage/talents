<?php
/**
*********************************************
* PageContent Class
*
*	a : talent description
*	b : services
*	c : packages
* 	d : Achievements
*	e : video
*	f : portfolio
*	g : social
*	
*
*********************************************
*/

class PageContent extends Model{

	public $table = 'page_contents';

	public $fields = array(
		'id',
		'enabled',
		'parent_id',
		'page_id',
		'section',
		'order',
		'title',
		'image_id',
		'body',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated page
	* Returns page object or NULL 
	*/

	public function getPage (){

		$data = $this->db->query("SELECT * FROM pages WHERE id = ".$this->data['page_id'], NULL, 1);

		if(!empty($data)){
			$obj = new page();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	/**
	* get parts of this section
	*/
	public function getPart ($index) {
		
		if($this->data['section'] == 'a' ||	$this->data['section'] == 'c' || $this->data['section'] == 'e'){
			// thease sections may not have sub parts
			return false;	
		}
		
		$data = $this->db->query("SELECT * FROM page_contents WHERE  section = '".$this->data['section']."' and parent_id = ".$this->data['id']." and `order` = ".$index." ", NULL, 1);
		
		$pc = new PageContent();
		
		if(!empty($data)){			
			$pc->populate($data);
			
			if($pc->isDeleted()){
				$pc->undelete();	
			}
			return $pc;			
		}
		
		if($this->data['section'] == 'b'){
			// services
			$pc->title = "My Service";
			$pc->body = "My service description";	
			
		}
		elseif($this->data['section'] == 'd'){
			// achivements
			$pc->title = "My Biggest Achievement";
			$pc->body = "Include details about your achievement";
		}		
		elseif($this->data['section'] == 'g'){
			// social
			$sites = array('fb', 'tw','ut','ig','li','pn');
			
			if(isset($sites[$index])){
				$pc->title = $sites[$index];
			}
			else{
				return false;	
			}
		}
		
		$pc->parent_id = $this->data['id'];
		$pc->section = $this->data['section'];
		$pc->page_id = $this->data['page_id'];
		$pc->order = $index;
		$pc->enabled = 1;
		$pc->save();


		return $pc;
		
	}


}
?>
