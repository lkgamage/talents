<?php

/**********************************************
* SmsLog Class
**********************************************/

class SmsLog extends Model{

	public $table = 'sms_logs';

	public $fields = array(
		'id',
		'customer_id',
		'phone_id',
		'type',
		'created',
		'modified',
		'deleted'
	);

}
?>
