<?php

/**********************************************
* SubscriptionPayment Class
**********************************************/

class SubscriptionPayment extends Model{

	public $table = 'subscription_payments';

	public $fields = array(
		'id',
		'package_id',
		'talent_id',
		'term',
		'amount',
		'method_id',
		'transaction_id',
		'status',
		'notes',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated Subscriptions
	* Returns array of Subscription objects or empty array 
	*/

	public function getSubscriptions (){

		$data = $this->db->query("SELECT * FROM Subscriptions WHERE payment_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated package
	* Returns package object or NULL 
	*/

	public function getPackage (){

		$data = $this->db->query("SELECT * FROM packages WHERE id = ".$this->data['package_id'], NULL, 1);

		if(!empty($data)){
			$obj = new package();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated payment_method
	* Returns payment_method object or NULL 
	*/

	public function getPaymentMethod (){

		$data = $this->db->query("SELECT * FROM payment_methods WHERE id = ".$this->data['method_id'], NULL, 1);

		if(!empty($data)){
			$obj = new paymentMethod();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
