<?php

/**********************************************
* AgencyType Class
**********************************************/

class AgencyType extends Model{

	public $table = 'agency_types';

	public $fields = array(
		'id',
		'name',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated agencies
	* Returns array of agency objects or empty array 
	*/

	public function getAgencies (){

		$data = $this->db->query("SELECT * FROM agencies WHERE agency_type = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new agency();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
