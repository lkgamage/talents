<?php
// my comments
abstract class Model {
	
	 /** Hold active database class object */
    protected $db;
	
	/** related database table */
   	public $table = NULL;
	
	 /** object properties */
    protected $data = array();
	
	/** object status If 0 : not loaded/saved, 1 : loaded/saved, -1 : deleted */
    protected $_status = 0;
	
	/** table fields */
    protected $fields = array();
	
	/**
     * Abstract parent class of all model classes
     */
    function __construct ( $id = NULL) {

        $this->db = new Database();
		
		if (!empty($id)) {
            $this->populate($this->read($this->table, $id));
        }
		
    }
	
	 /**
     * Check whether object is initialized and loaded data
     * return true or false
     */
    public function ready () {
        return $this->_status != 0;
    }

    /**
     * Check whether this object is deleted
     * return true or false
     */
    public function isDeleted () {

        return !empty($this->data['deleted']);
    }
	
		
	 /**
     * Check whether given field is empty
     * return true or false
     */
    public function isEmpty ($field) {

        return empty($this->data[$field]);
    }
	
	/**
	* Check if this class has given property
	*/
	public function hasProperty ( $name){
		return in_array($name, $this->fields);	
	}
	
	/**
     * Magic method getter for class properies
     */
    public function __get ($name) {

        if (!in_array($name, $this->fields)) {
            return false;
        }

        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        return null;
    }

    /**
     * Magic method setter for class properties
     */
    public function __set ($name, $value) {
		
        if (!in_array($name, $this->fields)) {
            return false;
        }

        if (in_array($name, array("created", "modified", "deleted"))) {
            return false;
        }

        $this->data[$name] = $value;
    }
	
	 /**
     * Magic method isset for class properties
     */
		public function __isset ($name) {
	
			return isset($this->data[$name]);
		}
	
	    /**
     * Read single record from gien table
     * Often use to read object data by id
     * table : table name
     * id : object id /row id
     */
    protected function read ($table, $id) {
		
		if(!is_numeric($id)){
			return false;	
		}

        return $this->db->query("SELECT * FROM {$table} where ID = " . $this->db->escape($id), null, 1);
    }
	
	 /**
     * popualate properties from array
     * values : associative array contains value for class properties
     */
    public function populate ($values) {
		
		if(empty($values)){
			return;	
		}

        foreach ($values as $key => $value) {

            $key = strtolower($key);

            if (in_array($key, $this->fields)) {
                $this->data[$key] = $value;
            }
        }

        if (!empty($this->data['deleted'])) {
            $this->_status = -1;
        } elseif (!empty($this->data['id'])) {
            $this->_status = 1;
        }
    }
	
	/**
	* Duplicate this object 
	*/
	public function duplicate ( ) {
		
		foreach ($this->data as $k => $v){
			if(empty($v) && $v == ''){
				unset($this->data[$k]);
			}
		}
		
		$this->_status = 0;
		unset($this->data['id']);
		unset($this->data['created']);
		unset($this->data['modified']);
		unset($this->data['deleted']);
		
		return $this->create();
		
	}

    /**
     * Read a single record querying any column from the given table
     * Only use this on columns with indexes
     *
     * @param  [type] $table  [description]
     * @param  [type] $column [description]
     * @param  [type] $value  [description]
     * @return [type]         [description]
     */
    protected function query ($table, $column, $value) {

        return $this->db->query("SELECT * FROM {$table} where {$column} = {$value}", null, 1);
    }

    /**
     * invoke create method in database class
     * table : table name
     * fields : array of lowercase fields names
     * values : key value pair of field data
     *
     */
    public function create () {
		
		if ($this->_status == 0) {
			
			$values = $this->cleanUp($this->fields, $this->data);
			
			$id = $this->db->insert($this->table, $values);
            if (!empty($id)) {
                $this->populate($this->read($this->table, $id));

                return true;
            }
        }

        return false;		
    }

    /**
     * invoke update method in database class
     */
   public function update () {
		
		 if ($this->_status != 0) {
			 
			$values = $this->cleanUp($this->fields, $this->data);
			 
			return $this->db->update($this->table, $values) > 0;

           
        }
        return false;
        
    }
	
	/**
	* set Field value to null
	*/
	public function setNull ($name){
		
		$this->db->execute("update ".$this->table." set {$name} = NULL where id = ".$this->data['id']);
		
		unset($this->data[$name]);
		
	}
	
	 /**
     * status safe function to create or update record based on its status
     * this will update or create record
     */
    public function save () {

        if ($this->_status == 0) {
            return $this->create();
        } else {
            return $this->update();
        }
    }

    /**
     * invoke soft delete method in database class
     */
    public function delete () {
        
		if ($this->_status == 1) {
        	$this->db->softDelete($this->table, $this->data['id']);
			$this->populate($this->read($this->table, $this->data['id']));
			return true;
		}
		
		return false;
    }

    /**
     * invoke undo soft delete method in database class
     */
    public function undelete () {
		
		if ($this->_status == -1) {
			$this->db->undoSoftDelete($this->table, $this->data['id']);
			$this->populate($this->read($this->table, $this->data['id']));
			return true;
		}

        return false;
    }
	

    /**
     * remove fields that doesn't belongs to given table
     * fields : array of lowercase fields names
     * values : key value pair of field data
     */
    private function cleanUp ($fields, $values) {

        $out = array();
        foreach ($values as $key => $value) {

            if (in_array(strtolower($key), $fields)) {
                $out[$key] = $value;
            }
        }

        return $out;
    }

    /**
     * Get objact data to display on the page
     * Modify this function in each model class to return only nessasasry data
     */
    public function toArray () {

        return $this->data;
    }
	
	 public function asArray () {

        return $this->data;
    }
	
}


?>