<?php

/**********************************************
* CustomerDevice Class
**********************************************/

class CustomerDevice extends Model{

	public $table = 'customer_devices';

	public $fields = array(
		'id',
		'customer_id',
		'name',
		'os',
		'browser',
		'country',
		'push_id',
		'fingureprint',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer_logins
	* Returns array of customer_login objects or empty array 
	*/

	public function getCustomerLogins (){

		$data = $this->db->query("SELECT * FROM customer_logins WHERE device_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new customerLogin();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	


}
?>
