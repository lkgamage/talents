<?php

/**********************************************
* Category Class
**********************************************/

class Category extends Model{

	public $table = 'categories';

	public $fields = array(
		'id',
		'profile_id',
		'skill_id',
		'package_id',
		'simultaneous',
		'term',
		'renewal',
		'renewal_date',
		'last_renewal',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated skill
	* Returns skill object or NULL 
	*/

	public function getSkill (){

		$data = $this->db->query("SELECT * FROM skills WHERE id = ".$this->data['skill_id'], NULL, 1);

		if(!empty($data)){
			$obj = new skill();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated package
	* Returns package object or NULL 
	*/

	public function getPackage (){
		
		if(empty($this->data['package_id'])){
			return false;
		}

		$data = $this->db->query("SELECT * FROM packages WHERE id = ".$this->data['package_id'], NULL, 1);

		if(!empty($data)){
			$obj = new package();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	/***
	* Get latest subscription for this category
	* return subscription object or null
	*/
	public function getRecentSubscription ($as_array  = false) {
		
		if($as_array) {
			
			return $this->db->query("SELECT s.*, p.name AS package_name, p.price, p.validity, i.status AS invoice_status, i.due_date, i.id as invoice_id, k.name as skill
FROM subscriptions s
JOIN packages p ON (p.id = s.package_id)
JOIN invoices i ON (i.subscription_id = s.id and isnull(i.deleted))
JOIN skills k on (k.id = p.skill_id)
WHERE s.category_id = ".$this->data['id']." AND  ISNULL(s.deleted) and i.status = '".DataType::$INVOICE_PAID."'  ORDER BY begin_date DESC ", NULL, 1);
			
		}
		else {
			$data = $this->db->query("SELECT * FROM subscriptions WHERE category_id = ".$this->data['id']." and active = 1 and isnull(deleted) ORDER BY begin_date desc", NULL, 1);
		
			if(!empty($data)){
				$obj = new Subscription();
				$obj->populate($data);
				return $obj;;
			}
		}
		


		return NULL;
		
	}
	
	/***
	* Get currently active subscription for this category
	* return subscription object or null
	*/
	public function getActiveSubscription ($as_array  = false) {
		
		if($as_array) {
			
			return $this->db->query("SELECT s.*, p.name AS package_name, p.price, p.validity, i.status AS invoice_status, i.due_date, i.id as invoice_id, k.name as skill
FROM subscriptions s
JOIN packages p ON (p.id = s.package_id)
JOIN invoices i ON (i.subscription_id = s.id and isnull(i.deleted))
JOIN skills k on (k.id = p.skill_id)
WHERE s.category_id = ".$this->data['id']." AND s.package_id = ".(!empty($this->data['package_id']) ? $this->data['package_id'] : 0)." AND  ISNULL(s.deleted) and i.status = '".DataType::$INVOICE_PAID."'  ORDER BY begin_date DESC ", NULL, 1);
			
		}
		else {
			$data = $this->db->query("SELECT * FROM subscriptions WHERE category_id = ".$this->data['id']." AND package_id = {$this->data['package_id']} and active = 1 and isnull(deleted) ORDER BY begin_date desc", NULL, 1);
		
			if(!empty($data)){
				$obj = new Subscription();
				$obj->populate($data);
				return $obj;;
			}
		}
		


		return NULL;
		
	}



	/**
	* Get associated subscriptions
	* Returns array of subscription objects or empty array 
	*/

	public function getSubscriptions (){

		$data = $this->db->query("SELECT * FROM subscriptions WHERE category_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent_packages
	* Returns array of talent_package objects or empty array 
	*/

	public function getTalentPackages (){

		$data = $this->db->query("SELECT * FROM talent_packages WHERE category_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentPackage();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* get payment/invoice data
	*/
	public function getPayments () {
		
		$sql = "SELECT s.id, s.created, s.begin_date, s.exp_date, s.active, s.deleted, i.id AS invoice_id, i.amount, i.status, i.due_date, i.effective_date, i.deleted as invoice_deleted, p.created AS payment_date,  p.status AS payment_status, p.success AS payment_success, m.type AS card_type, m.card_number
FROM subscriptions s
JOIN packages k ON (k.id = s.package_id)
JOIN invoices i ON (i.subscription_id = s.id)
LEFT JOIN payments p ON (p.invoice_id = i.id)
LEFT JOIN payment_methods m ON (m.id = p.method_id)
WHERE s.category_id = {$this->data['id']}
ORDER BY i.created desc";
		
		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = "select count(*) as total  FROM subscriptions where category_id = {$this->data['id']}";
		$buffer->_component = "subscription/payment_list";
		$buffer->_pagesize = 12;
		$buffer->_page = 1; 
		return $buffer;
		
	}
	
	
	/**
	* Get info
	*/
	public function getinfo () {
		
		$data = $this->db->query("
SELECT f.id AS category__id, 
f.profile_id AS category__profile_id, 
f.skill_id AS category__skill_id, 
f.package_id AS category__package_id, 
f.simultaneous AS category__simultaneous, 
f.term AS category__term, 
f.simultaneous as category__simultaneous,
f.renewal AS category__renewal, 
f.renewal_date AS category__renewal_date, 
f.last_renewal AS category__last_renewal, 
f.created AS category__created, 
f.modified AS category__modified, 
f.deleted AS category__deleted,
k.id AS skill__id, 
k.category AS skill__category, 
k.parent_id AS skill__parent_id, 
k.name AS skill__name, 
k.genre AS skill__genre, 
k.gender_required AS skill__gender_required, 
k.is_default AS skill__is_default, 
k.is_business AS skill__is_business, 
k.is_agency AS skill__is_agency, 
k.active AS skill__active, 
k.created AS skill__created, 
k.modified AS skill__modified, 
k.deleted AS skill__deleted,
p.id AS package__id, 
p.skill_id AS package__skill_id, 
p.name AS package__name, 
p.description AS package__description, 
p.tagline AS package__tagline, 
p.price AS package__price, 
p.price_shown AS package__price_shown, 
p.validity AS package__validity, 
p.jobs AS package__jobs, 
p.manage AS package__manage, 
p.begin_date AS package__begin_date, 
p.end_date AS package__end_date, 
p.active AS package__active, 
p.display_order AS package__display_order, 
p.selected AS package__selected, 
p.agency_only AS package__agency_only, 
p.one_time AS package__one_time, 
p.product_id AS package__product_id, 
p.price_id AS package__price_id, 
p.created AS package__created, 
p.modified AS package__modified, 
p.deleted AS package__deleted,
s.id as subscription__id, 
s.category_id as subscription__category_id, 
s.package_id as subscription__package_id, 
s.sub_date as subscription__sub_date, 
s.begin_date as subscription__begin_date, 
s.exp_date as subscription__exp_date, 
s.active as subscription__active, 
s.jobs as subscription__jobs, 
s.manage as subscription__manage, 
s.created as subscription__created, 
s.modified as subscription__modified, 
s.deleted as subscription__deleted,
(SELECT COUNT(*) FROM bookings WHERE subscription_id = s.id AND !ISNULL(responded_date) AND free_quota = 0 ) AS subscription__used,
csa.stripe_id AS stripe__active,
csp.stripe_id AS stripe__pending
FROM categories f 
LEFT JOIN skills k ON ( k.id = f.skill_id)
LEFT JOIN packages p ON ( p.id = f.package_id)
LEFT JOIN subscriptions s ON ( s.category_id = f.id and s.begin_date <= NOW() and s.exp_date >= NOW() and isnull(s.deleted) and s.active = 1 )
LEFT JOIN category_subscriptions csa ON (csa.category_id = f.id AND csa.active = 1 AND ISNULL(csa.deleted))
LEFT JOIN category_subscriptions csp ON (csp.category_id = f.id AND csp.active = 0 AND ISNULL(csp.deleted))
where f.id = {$this->data['id']}", NULL, 1);
		
		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		return $out;
		
	}



	/**
	* Get associated category_subscriptions
	* Returns array of category_subscription objects or empty array 
	*/

	public function getCategorySubscriptions (){

		$data = $this->db->query("SELECT * FROM category_subscriptions WHERE category_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new categorySubscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get currently active subscription records
	* this may includes active single credits
	*/
	public function getActiveSubscriptions () {		
		
		$data = $this->db->query("SELECT * FROM subscriptions WHERE exp_date > NOW() AND active = 1 AND ISNULL(deleted) and category_id = ".$this->data['id'], NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Get pening invoices
	*/
	public function getPendingInvoices () {
		
		$data = $this->db->query("SELECT i.*
FROM subscriptions s 
JOIN invoices i ON (i.subscription_id = s.id)
WHERE s.category_id = {$this->data['id']} AND i.status = '".DataType::$INVOICE_PENDING."' and isnull(i.deleted) and i.due_date > NOW()");
		
		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new Invoice();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* This function used to check if stripe subscription id exists
	*/
	
	public function getStripeSubscription ($stripe_id) {
		
		$data = $this->db->query("SELECT * FROM category_subscriptions WHERE stripe_id = '".$stripe_id."' and category_id = {$this->data['id']} and isnull(deleted)", NULL, 1);

		if(!empty($data)){
			
			$obj = new categorySubscription();
			$obj->populate($data);
			return $obj;

		}

		return false;		
		
	}
	
	/**
	* Add a new in-active subscription record,
	*/ 
	public function addStripeSubscription ($stripe_id) {
		
		$sub = $this->getStripeSubscription($stripe_id);
		
		if(empty($sub)){
			$sub = new CategorySubscription();
			$sub->category_id = $this->data['id'];
			$sub->stripe_id = $stripe_id;
			$sub->active = 0;
			$sub->save();			
		}
		
		return $sub;
	}
	
	/**
	* Get active stripe subscription record
	*/
	public function getActiveStripeSubscription () {
		
		$data = $this->db->query("SELECT * FROM category_subscriptions WHERE active = 1 and  category_id = {$this->data['id']} and isnull(deleted)", NULL, 1);

		if(!empty($data)){
			
			$obj = new categorySubscription();
			$obj->populate($data);
			return $obj;
		}

		return false;
		
	}
	
	/**
	* Set a stripe subscription record active by stripe id
	*/
	public function setActiveStripeSubscription ($stripe_id) {
		
		$this->db->execute("UPDATE category_subscriptions SET active = 0, deleted = NOW() WHERE  category_id = {$this->data['id']} and active = 1");
		
		$this->db->execute("UPDATE category_subscriptions SET active = 1, deleted = NULL WHERE  stripe_id = '{$stripe_id}'");
		
		return true;
	}
	
	
	/**
	* Delete free subscriptions when activating paid 
	*/
	public function deleteFreeSubscriptions () {
		
		$data = $this->db->query("SELECT s.id, s.created
FROM subscriptions s 
JOIN packages p ON (p.id = s.package_id)
WHERE p.price = 0 AND category_id = {$this->data['id']} AND s.active = 1 AND ISNULL(s.deleted)");
		
		if(!empty($data)){
			$data = Util::groupArray($data, 'id');
			
			$this->db->execute("update subscriptions set active = 0, deleted = NOW() where id in (".implode(',', array_keys($data)).")");
			
			return true;
		}
		
		return false;
	}
	
	/**
	* Get upcomming subscription records which are 
	* schedule to be activated in future
	**/
	public function nextSubscription () {
		
		return $this->db->query("SELECT s.id, s.begin_date, s.exp_date, s.active, p.id AS package_id, p.name
FROM subscriptions s
JOIN packages p ON (p.id = s.package_id)
WHERE s.category_id = {$this->data['id']} AND s.begin_date > NOW() AND ISNULL(s.deleted)", NULL, 1);
		
	}
	
	/**
	* Delete all pending changes in local database
	*  If there are any stripe subscriptions to be deleted, return it
	*/
	public function cancelPendingChanges () {
		
		// delete pending invoices
		$invoices = $this->getPendingInvoices();
		if(!empty($invoices)){	

			foreach ($invoices as $inv){
				if($inv->type == DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION ||
				   $inv->type == DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION ||
				   $inv->type == DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION ||
				   $inv->type ==  DataType::$INVOICE_TYPE_UP_SUBSCRIPTION )
				$inv->delete();
				applog("Invoice {$inv->id} deleted");
			}			
		}

		// if there are any future subscriptions, it wil be deleted
		$ns = $this->nextSubscription();
		if(!empty($ns)){
			$nsub = new Subscription($ns['id']);
			$nsub->delete();
			applog("Next scheduled subscription {$nsub->id} deleted");
			
			if(!$nsub->isEmpty('catsub_id')){
				return $nsub->getCategorySubscription();
			}			
		}		
		
		return true;
	}


}
?>
