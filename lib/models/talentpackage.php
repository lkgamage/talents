<?php
		
/****************************
* Talent Packagen Class
*****************************/

class TalentPackage extends Model{

	public $table = 'talent_packages';

	public $fields = array(
		'id',
		'category_id',
		'active',
		'name',
		'description',
		'items',
		'timespan',
		'interval',
		'location_required',
		'fee',
		'fee_local',
		'advance_percentage',
		'is_private',
		'fulday',
		'image_id',
		'created',
		'modified',
		'deleted'
		);
	
	
	/**
	* Get talent
	*/
	public function getTalent () {
	
		return new Talent($this->data['talent_id']);
		
	}



	/**
	* Get associated bookings
	* Returns array of booking objects or empty array 
	*/

	public function getBookings (){

		$data = $this->db->query("SELECT * FROM bookings WHERE session_type = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new booking();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated appoinments
	* Returns array of talent_appoinment objects or empty array 
	*/

	public function getTalentAppoinments (){

		$data = $this->db->query("SELECT * FROM appoinments WHERE package_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentAppoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent_skill
	* Returns talent_skill object or NULL 
	*/

	public function getTalentSkill (){

		$data = $this->db->query("SELECT * FROM talent_skills WHERE id = ".$this->data['skill_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talentSkill();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated skill
	* Returns skill object or NULL 
	*/

	public function getSkill (){

		$data = $this->db->query("SELECT * FROM skills WHERE id = ".$this->data['skill_id'], NULL, 1);

		if(!empty($data)){
			$obj = new skill();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated appoinments
	* Returns array of appoinment objects or empty array 
	*/

	public function getAppoinments (){

		$data = $this->db->query("SELECT * FROM appoinments WHERE package_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new appoinment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Check whethere this package is avilabe for booking
	* Checking subscription for given skill
	*/
	public function isAvailable () {
		
		$sub = $this->db->query("SELECT  id FROM subscriptions 
WHERE talent_id = ".$this->data['talent_id']."  AND skill_id = ".$this->data['skill_id']." AND begin_date < NOW() && exp_date >= DATE(NOW()) ");

		if(!empty($sub)){
			return true;
		}
		
		return false;
	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated category
	* Returns category object or NULL 
	*/

	public function getCategory (){

		$data = $this->db->query("SELECT * FROM categories WHERE id = ".$this->data['category_id'], NULL, 1);

		if(!empty($data)){
			$obj = new category();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated image
	* Returns image object or NULL 
	*/

	public function getImage (){

		$data = $this->db->query("SELECT * FROM images WHERE id = ".$this->data['image_id'], NULL, 1);

		if(!empty($data)){
			$obj = new image();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}

?>