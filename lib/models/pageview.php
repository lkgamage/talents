<?php

/**********************************************
* PageView Class
**********************************************/

class PageView extends Model{

	public $table = 'page_views';

	public $fields = array(
		'id',
		'page_id',
		'customer_id',
		'country',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated page
	* Returns page object or NULL 
	*/

	public function getPage (){

		$data = $this->db->query("SELECT * FROM pages WHERE id = ".$this->data['page_id'], NULL, 1);

		if(!empty($data)){
			$obj = new page();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
