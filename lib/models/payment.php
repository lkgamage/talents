<?php
		
/****************************
* Payment Class
*****************************/

class Payment extends Model{

	public $table = 'payments';

	public $fields = array(
		'id',
		'invoice_id',
		'method_id',
		'amount',
		'currency',
		'intent_id',
		'charge_id',
		'status',
		'success',
		'released',
		'created',
		'modified',
		'deleted'
		);
	



	/**
	* Get associated invoice
	* Returns invoice object or NULL 
	*/

	public function getInvoice (){

		$data = $this->db->query("SELECT * FROM invoices WHERE id = ".$this->data['invoice_id'], NULL, 1);

		if(!empty($data)){
			$obj = new invoice();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated refunds
	* Returns array of refund objects or empty array 
	*/

	public function getRefunds (){

		$data = $this->db->query("SELECT * FROM refunds WHERE payment_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new refund();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated ledgers
	* Returns array of ledger objects or empty array 
	*/

	public function getLedgers (){

		$data = $this->db->query("SELECT * FROM ledgers WHERE payment_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new ledger();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated Subscriptions
	* Returns array of Subscription objects or empty array 
	*/

	public function getSubscriptions (){

		$data = $this->db->query("SELECT * FROM Subscriptions WHERE payment_id = ".$this->data['subscription_id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}

		return $out;
	}
	
	
	/**
	* Convert to a payment method
	* This this payment instrument as a payment method for future use
	*/
	public function savePaymentMethod ($customer_id) {
		
		// check if there is an method already saved
		$data = $this->db->query("select * from payment_methods where customer_id = ".$customer_id." and card_number = '".$this->data['card_num']."'", NULL, 1);
		
		$method = new PaymentMethod();
		
		if(empty($data)){
	
			$method->customer_id = $customer_id;
			$method->type = $this->data['card_type'];
			$method->name = strtoupper($this->data['card_type']).' - '.substr($this->data['card_num'], -4, 4);;
			$method->card_name = $this->data['card_name'];
			$method->card_number = $this->data['card_num'];
			$method->card_exp = $this->data['card_exp'];
			$method->ccvv = $this->data['card_cvv'];
			//$method->token = Validator::cleanup($_POST['method_token'], 400);
			//$method->reference = Validator::cleanup($_POST['method_reference'], 1000);
			$method->save();
			
			return $method;
		}
		else{
			
			$method->populate($data);
			return $method;
				
		}
		
	}
	


	/**
	* Get associated payment_method
	* Returns payment_method object or NULL 
	*/

	public function getPaymentMethod (){

		$data = $this->db->query("SELECT * FROM payment_methods WHERE id = ".$this->data['method_id'], NULL, 1);

		if(!empty($data)){
			$obj = new paymentMethod();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}

?>