<?php
		
/****************************
* Talentmedia Class
*****************************/

class Talentmedia extends Model{

	public $table = 'talent_media';

	public $fields = array(
			'id',
			'talent_id',
			'is_image',
			'embed',
			'source',
			'description',
			'location_id',
			'venue',
			'created',
			'modified',
			'deleted'
		);
	



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated image
	* Returns image object or NULL 
	*/

	public function getImage (){

		$data = $this->db->query("SELECT * FROM images WHERE id = ".$this->data['is_image'], NULL, 1);

		if(!empty($data)){
			$obj = new image();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}

?>