<?php

/**********************************************
* Message Class
**********************************************/

class Message extends Model{

	public $table = 'messages';

	public $fields = array(
		'id',
		'board_id',
		'customer_id',
		'message',
		'originated',
		'edited',
		'is_control',
		'quote_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['from_customer'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


	/**
	* Get information to display in frontend 
	*/
	public function getInfo() {
		
		$sql = "SELECT b.id AS board_id, b.event_id,  b.booking_id,  p.profile_id, t.name AS talent_name, t.gender AS talent_gender, ti.thumb AS talent_image, m.id AS message_id, s.gender AS customer_gender, t.gender AS talent_gender, m.originated, m.created, s.id AS customer_id,
CONCAT(s.firstname,' ',s.lastname) AS customer_name, si.thumb AS customer_image, m.message, m.deleted
FROM message_recipients r
JOIN messages m ON ( m.id = r.message_id )
JOIN message_boards b ON (b.id = m.board_id) 
JOIN board_participants p ON (p.board_id = m.board_id AND p.customer_id = m.customer_id)
JOIN customers s ON (s.id = p.customer_id)
LEFT JOIN profiles t ON (t.id = p.profile_id)
LEFT JOIN images ti ON (ti.id = t.image_id)
LEFT JOIN images si ON (si.id = s.image_id)
WHERE m.id = ".$this->data['id'];
		
		return $this->db->query($sql, NULL, 1);
			
	}

	


	/**
	* Get associated message_board
	* Returns message_board object or NULL 
	*/

	public function getMessageBoard (){

		$data = $this->db->query("SELECT * FROM message_boards WHERE id = ".$this->data['board_id'], NULL, 1);

		if(!empty($data)){
			$obj = new messageBoard();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated message_recipients
	* Returns array of message_recipient objects or empty array 
	*/

	public function getMessageRecipients (){

		$data = $this->db->query("SELECT * FROM message_recipients WHERE message_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new messageRecipient();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated message
	* Returns message object or NULL 
	*/

	public function getMessage (){

		$data = $this->db->query("SELECT * FROM messages WHERE id = ".$this->data['quote_id'], NULL, 1);

		if(!empty($data)){
			$obj = new message();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	
	/**
	 * Remove a message for given customer
	 *  
	 */
	 public function remove ($for_all = false){
		 
		 $customer = Session::getCustomer();
		 
		 
		 if($for_all && $this->data['customer_id'] == $customer['id']){
			$this->delete();
		 }
		 else{
			 
			$this->db->execute("UPDATE message_recipients SET deleted = NOW() WHERE message_id = ".$this->data['id']." AND customer_id = ".$customer['id']); 
			 
		 }
	 }


}
?>
