<?php

/**********************************************
* EventItem Class
**********************************************/

class EventItem extends Model{

	public $table = 'event_items';

	public $fields = array(
		'id',
		'event_id',
		'begins',
		'ends',
		'title',
		'description',
		'note',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated event
	* Returns event object or NULL 
	*/

	public function getEvent (){

		$data = $this->db->query("SELECT * FROM events WHERE id = ".$this->data['event_id'], NULL, 1);

		if(!empty($data)){
			$obj = new event();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated event_bookings
	* Returns array of event_booking objects or empty array 
	*/

	public function getBookings (){

		return App::getAgendaBookings($this->data['id']);
		// same query can be found in 
		
	}
	
	public function getEventBookings (){
		return $this->getBookings();	
	}
	
	/**
	* Reset associated bookings for an event item
	*/
	public function setBookings ($ids){
		
		// check ids
		if(empty($ids)){
			return false;	
		}
		
		foreach ($ids as $a => $b){
						
			if(!is_numeric($b)){
				unset($ids[$a]);
			}			
		}
		
		if(empty($ids)){
			return false;	
		}
		
		$bookings = $this->db->query("select id, booking_id from event_bookings WHERE item_id = ".$this->data['id']." && isnull(deleted)");
		
		$bookings = Util::groupArray($bookings, 'booking_id', 'id');
		
		$creates = array_diff($ids, array_keys($bookings));
		$deletes = array_diff(array_keys($bookings), $ids);
		
		// delete unnessaary
		if(!empty($deletes)){
			
			foreach ($deletes as $id){
				$this->db->execute("UPDATE event_bookings SET deleted = NOW() WHERE id = ".$bookings[$id]);	
			}			
		}
		
		// create new records
		if(!empty($creates)){
		
			foreach ($creates as $id){
				$booking = new EventBooking();
				$booking->item_id = $this->data['id'];
				$booking->booking_id = $id;
				$booking->save();
			}
			
		}
		
	}
	
	
	/**
	* Remove all bookings associated with this 
	*/
	public function clearBookings () {
		
		$bookings = $this->db->query("select id from event_bookings WHERE item_id = ".$this->data['id']." && isnull(deleted)");
		
		if(!empty($bookings)) {
			$this->db->execute("UPDATE event_bookings SET deleted = NOW() WHERE item_id = ".$this->data['id']);
		}
			
	}


}
?>
