<?php
		
/****************************
* Image Class
*****************************/

class Image extends Model{

	public $table = 'images';

	public $fields = array(
		'id',
		'customer_id',
		'i600',
		'i400',
		'thumb',
		'url',
		'orientation',
		'width',
		'height',
		'created',
		'modified',
		'deleted'
		);
	

	/**
	* Get image data
	*/
	public function getData () {
	
		return array(
			'id' => $this->data['id'],
			'thumb' => $this->data['thumb'],
			'i400' => $this->data['i400'],
			'i600' => $this->data['i600'],
			'url' => $this->data['url'],
			'orientation' => $this->data['orientation']
		);
		
	}

	/**
	* Get associated talent_media
	* Returns array of talent_media objects or empty array 
	*/

	public function getTalentMedia (){

		$data = $this->db->query("SELECT * FROM talent_media WHERE is_image = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentMedia();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated pages
	* Returns array of page objects or empty array 
	*/

	public function getPages (){

		$data = $this->db->query("SELECT * FROM pages WHERE banner_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new page();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	
	/**
	* Check if image is in use
	* returning array (
	*	agency => 0,
	*	talent => 0,
	*	customer => 0,
	*	page => 0
	* )
	*/
	public function usage () {
	
		return $this->db->query("SELECT
(SELECT COUNT(*) FROM customers WHERE image_id = i.id AND ISNULL(deleted)) AS customer,
(SELECT COUNT(*) FROM profiles WHERE image_id = i.id AND ISNULL(deleted)) AS profile,
(SELECT COUNT(*) FROM page_contents WHERE image_id = i.id AND ISNULL(deleted)) AS page
FROM images i WHERE id = ". $this->data['id'], NULL, 1);
		
	}
	
	/**
	* Get hosting paths of all image sizes
	*/
	public function getPaths () {
		
		$find = Config::$image_url;
		$replace =Config:: $base_path;
		
		$keys = array('thumb', 'i400', 'i600', 'url');
		$out = array();
		
		foreach ($keys as $key){
			$out[$key] =$this->data[$key];	
		}
		
		return $out;
	}



	/**
	* Get associated talent_packages
	* Returns array of talent_package objects or empty array 
	*/

	public function getTalentPackages (){

		$data = $this->db->query("SELECT * FROM talent_packages WHERE image_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new talentPackage();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}

	
}

?>