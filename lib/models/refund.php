<?php

/**********************************************
* Refund Class
**********************************************/

class Refund extends Model{

	public $table = 'refunds';

	public $fields = array(
		'id',
		'payment_id',
		'amount',
		'booking_fee_inc',
		'processed_by',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated payment
	* Returns payment object or NULL 
	*/

	public function getPayment (){

		$data = $this->db->query("SELECT * FROM payments WHERE id = ".$this->data['payment_id'], NULL, 1);

		if(!empty($data)){
			$obj = new payment();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated ledgers
	* Returns array of ledger objects or empty array 
	*/

	public function getLedgers (){

		$data = $this->db->query("SELECT * FROM ledgers WHERE refund_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new ledger();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
