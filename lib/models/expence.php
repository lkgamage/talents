<?php

/**********************************************
* Expence Class
**********************************************/

class Expence extends Model{

	public $table = 'expences';

	public $fields = array(
		'id',
		'event_id',
		'booking_id',
		'expence_type',
		'description',
		'amount',
		'amount_local',
		'created',
		'modified',
		'deleted'
	);
	
	
	
	/**
	* Check if this is an event expence or booking expence
	*/
	public function isEvent () {
	
		return empty($this->data['booking_id']) && !empty($this->data['event_id']);
		
	}


	/**
	* Get associated event
	* Returns event object or NULL 
	*/

	public function getEvent (){

		$data = $this->db->query("SELECT * FROM events WHERE id = ".$this->data['event_id'], NULL, 1);

		if(!empty($data)){
			$obj = new event();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
