<?php
		
/****************************
* Talentappoinment Class
*****************************/

class Talentappoinment extends Model{

	public $table = 'talent_appoinments';

	public $fields = array(
		'id',
		'talent_id',
		'package_id',
		'booking_id',
		'description',
		'note',
		'all_day',
		'time_off',
		'begins',
		'duration',
		'rest_time',
		'confirmed',
		'expired',
		'is_private',
		'created',
		'modified',
		'deleted'
		);
	

	/**
	* Check if given appinment is associaed witha  abookng
	*/
	public function isBooking () {
		return !empty($this->data['booking_id']);	
	}
	
	/**
	* Check if this ap[pointment is a past day/time appointment
	*/
	public function isPast () {
		return time() > strtotime($this->data['begins']);	
	}
	
	/**
	* Get status message to display
	*/
	public function getStatus () {
	
		$t = time();
		if($this->data['confirmed'] == 1){
			return 'Confirmed';	
		}
		elseif(!empty($this->data['expired']) && $t > strtotime($this->data['expired'])){
			return 'Expired';	
		}
		else{
			return 'Scheduled ';	
		}
		
	}


	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated talent_package
	* Returns talent_package object or NULL 
	*/

	public function getTalentPackage (){

		$data = $this->db->query("SELECT * FROM talent_packages WHERE id = ".$this->data['package_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talentPackage();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	
	
	/**
	* Gives printable date/times
	* return an array
	* array(
	*
	)
	*/
	public function getTimes () {
		
		$dt = strtotime($this->data['begins']);
		$tz = Session::timezone();
		$dt += $tz*60;
		
		$out = array('begin' => array(), 'end' => array(), 'duration' => '', 'interval' => array());
		
		$out['begin']['date'] = date('M j, Y', $dt);
		$out['begin']['time'] = date('h:ia', $dt);
		
		$dt += $this->data['duration']*60;
		
		$out['end']['date'] = date('M j, Y', $dt);
		$out['end']['time'] = date('h:ia', $dt);
		
		$out['duration'] = Util::ToTime($this->data['duration']);
		
		if(!empty($this->data['rest_time'])){
			
			$out['interval']['begin'] = $out['end']['time'];
			
			$dt += $this->data['rest_time']*60;
			$out['interval']['end'] =date('h:ia', $dt);
			
			$out['interval']['duration'] = Util::ToTime($this->data['rest_time']);
		}
		
		return $out;
		
	}


}

?>