<?php

/**********************************************
* AutoReneval Class
**********************************************/

class AutoReneval extends Model{

	public $table = 'auto_renevals';

	public $fields = array(
		'id',
		'talent_id',
		'agency_id',
		'package_id',
		'term',
		'method_id',
		'next_reneval',
		'active',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated Subscriptions
	* Returns array of Subscription objects or empty array 
	*/

	public function getSubscriptions (){

		$data = $this->db->query("SELECT * FROM Subscriptions WHERE reneval_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated package
	* Returns package object or NULL 
	*/

	public function getPackage (){

		$data = $this->db->query("SELECT * FROM packages WHERE id = ".$this->data['package_id'], NULL, 1);

		if(!empty($data)){
			$obj = new package();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated payment_method
	* Returns payment_method object or NULL 
	*/

	public function getPaymentMethod (){

		$data = $this->db->query("SELECT * FROM payment_methods WHERE id = ".$this->data['method_id'], NULL, 1);

		if(!empty($data)){
			$obj = new paymentMethod();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
