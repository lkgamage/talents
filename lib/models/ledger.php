<?php

/**********************************************
* Ledger Class
**********************************************/

class Ledger extends Model{

	public $table = 'ledgers';

	public $fields = array(
		'id',
		'customer_id',
		'profile_id',
		'payment_id',
		'amount',
		'balance',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated refund
	* Returns refund object or NULL 
	*/

	public function getRefund (){

		$data = $this->db->query("SELECT * FROM refunds WHERE id = ".$this->data['refund_id'], NULL, 1);

		if(!empty($data)){
			$obj = new refund();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated payment
	* Returns payment object or NULL 
	*/

	public function getPayment (){

		$data = $this->db->query("SELECT * FROM payments WHERE id = ".$this->data['payment_id'], NULL, 1);

		if(!empty($data)){
			$obj = new payment();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated withdrawal
	* Returns withdrawal object or NULL 
	*/

	public function getWithdrawal (){

		$data = $this->db->query("SELECT * FROM withdrawals WHERE id = ".$this->data['withdrawl_id'], NULL, 1);

		if(!empty($data)){
			$obj = new withdrawal();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
