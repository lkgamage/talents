<?php

/**********************************************
* Package Class
**********************************************/

class Package extends Model{

	public $table = 'packages';

	public $fields = array(
		'id',
		'skill_id',
		'name',
		'description',
		'tagline',
		'price',
		'price_shown',
		'validity',
		'jobs',
		'manage',
		'begin_date',
		'end_date',
		'active',
		'display_order',
		'selected',
		'agency_only',
		'one_time',
		'premium',
		'product_id',
		'price_id',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated skill
	* Returns skill object or NULL 
	*/

	public function getSkill (){

		$data = $this->db->query("SELECT * FROM skills WHERE id = ".$this->data['skill_id'], NULL, 1);

		if(!empty($data)){
			$obj = new skill();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated Subscriptions
	* Returns array of Subscription objects or empty array 
	*/

	public function getSubscriptions (){

		$data = $this->db->query("SELECT * FROM Subscriptions WHERE package_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscription();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated subscription_payments
	* Returns array of subscription_payment objects or empty array 
	*/

	public function getSubscriptionPayments (){

		$data = $this->db->query("SELECT * FROM subscription_payments WHERE package_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new subscriptionPayment();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated auto_renevals
	* Returns array of auto_reneval objects or empty array 
	*/

	public function getAutoRenevals (){

		$data = $this->db->query("SELECT * FROM auto_renevals WHERE package_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new autoReneval();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated categories
	* Returns array of category objects or empty array 
	*/

	public function getCategories (){

		$data = $this->db->query("SELECT * FROM categories WHERE package_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new category();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}
?>
