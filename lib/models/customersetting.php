<?php

/**********************************************
* CustomerSetting Class
**********************************************/

class CustomerSetting extends Model{

	public $table = 'customer_settings';

	public $fields = array(
		'id',
		'customer_id',
		'alert1_sms',
		'alert1_email',
		'alert2_sms',
		'alert2_email',
		'alert3_sms',
		'alert3_email',
		'alert4_sms',
		'alert4_email',
		'alert5_sms',
		'alert5_email',
		'alert6_sms',
		'alert6_email',
		'alert7_sms',
		'alert7_email',
		'alert8_sms',
		'alert8_email',
		'alert9_sms',
		'alert9_email',
		'alert10_sms',
		'alert10_email',
		'direct',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
