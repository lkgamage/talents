<?php
		
/****************************
* Withdrawal Class
*****************************/

class Withdrawal extends Model{

	public $table = 'withdrawals';

	public $fields = array(
		'id',
		'payout_request_id',
		'amount',
		'payment_id',
		'reference',
		'payment_date',
		'prepared_user_id',
		'approved_user_id',
		'status',
		'created',
		'modified',
		'deleted'
		);
	



	/**
	* Get associated payout_request
	* Returns payout_request object or NULL 
	*/

	public function getPayoutRequest (){

		$data = $this->db->query("SELECT * FROM payout_requests WHERE id = ".$this->data['payout_request_id'], NULL, 1);

		if(!empty($data)){
			$obj = new payoutRequest();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated ledgers
	* Returns array of ledger objects or empty array 
	*/

	public function getLedgers (){

		$data = $this->db->query("SELECT * FROM ledgers WHERE withdrawl_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new ledger();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}


}

?>