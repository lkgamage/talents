<?php
		
/****************************
* Paymentrequest Class
*****************************/

class Paymentrequest extends Model{

	public $table = 'payment_requests';

	public $fields = array(
			'id',
			'payment_method_id',
			'amount',
			'ip_address',
			'verified',
			'status',
			'created',
			'modified',
			'deleted'
		);
	


}

?>