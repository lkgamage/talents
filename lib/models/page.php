<?php

/**********************************************
* Page Class
**********************************************/

class Page extends Model{

	public $table = 'pages';

	public $fields = array(
		'id',
		'customer_id',
		'profile_id',
		'handle',
		'banner_id',
		'name',
		'description',
		'skills',
		'image',
		'active',
		'handle_updated',
		'updated',
		'published',
		'show_packages',
		'created',
		'modified',
		'deleted'
	);
	
	
	
	/************
	* Check if page is published
	***********/
	public function isPublished () {
		
		return !empty($this->data['published']);
		
	}
	


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}
	

	/**
	* Get associated image
	* Returns image object or NULL 
	*/

	public function getBanner (){


		if(!empty($this->data['banner_id'])) {
			$data = $this->db->query("SELECT * FROM images WHERE id = ".$this->data['banner_id'], NULL, 1);
	
			if(!empty($data)){
				$obj = new image();
				$obj->populate($data);
				return $obj;;
			}
		}
		
		$image = new image();
		$url = Util::mapURL('/images/static/static-bg.jpg');
		$image->i600 = $url;
		$image->i400 = $url;
		$image->thumb = $url;
		$image->url = $url;


		return $image;
	}
	



	/**
	* Get associated page_contents
	* Returns array of page_content objects or empty array 
	*/

	public function getPageContents (){

		$data = $this->db->query("SELECT c.*, i.url as image FROM page_contents c 
		LEFT JOIN images i on (i.id = c.image_id)
		WHERE c.page_id = ".$this->data['id']." and isnull(c.deleted) order by FIELD(section, 'a', 'b', 'h', 'c', 'd', 'e', 'f', 'g') ", NULL);

		$out = array();

		foreach ($data as $item) {
			
			if(!empty($item['parent_id'])){
				continue;
			}
				
			$out[$item['section']] = $item;
		}
		
		foreach ($data as $item) {
			
			if(empty($item['parent_id'])){
				continue;
			}
			
			if(!isset($out[$item['section']]['contents'])) {
				$out[$item['section']]['contents'] = array();
			}
			
			$out[$item['section']]['contents'][] = $item;
		}
		


		return $out;
	}
	
	
	/**
	* Get a section based on id/letter
	* Return existing or create new part
	*/
	public function getSection ($section_id){
		
		$data = $this->db->query("SELECT * FROM page_contents WHERE page_id = ".$this->data['id']." and section = '".$section_id."' and isnull(parent_id) and isnull(deleted) ", NULL, 1);
		
		$pc = new PageContent();
		
		if(!empty($data)){			
			$pc->populate($data);
			return $pc;			
		}
		
		
		if($section_id == 'a'){
			$pc->title = "ABOUT US";	
			$pc->body = "Brief description about you/business";
		}
		elseif($section_id == 'b'){
			$pc->title = "SERVICES";	
			$pc->body = "Description about your services";
		}
		elseif($section_id == 'c'){
			$pc->title = "PACKAGES";	
		}
		elseif($section_id == 'd'){
			$pc->title = "ACHIEVEMENTS";	
		}
		elseif($section_id == 'e'){
		//	$pc->body = '<img src="http://localhost/talent/site/images/static/video-placeholder.jpg">';	
		}
		
		elseif($section_id == 'f'){
			$pc->title = "OUR WORKS";	
		}
		elseif($section_id == 'g'){
			$pc->title = "SOCIAL PAGES";	
		}
		elseif($section_id == 'h'){
			$pc->title = "MISSION";	
			$pc->body = "Include your mission statement.";
		}
		
		$pc->section = $section_id;
		$pc->page_id = $this->data['id'];
		$pc->enabled = 1;
		$pc->save();
		
		// create part
		if($section_id == 'b' || $section_id == 'd'){
			$pc->getPart(0);	
		}
				
		
		return $pc;
	}



	/**
	* Get associated image
	* Returns image object or NULL 
	*/

	public function getImage (){

		$data = $this->db->query("SELECT * FROM images WHERE id = ".$this->data['banner_id'], NULL, 1);

		if(!empty($data)){
			$obj = new image();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated page_views
	* Returns array of page_view objects or empty array 
	*/

	public function getPageViews (){

		$data = $this->db->query("SELECT * FROM page_views WHERE page_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new pageView();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}



	/**
	* Get associated likes
	* Returns array of like objects or empty array 
	*/

	public function getLikes (){

		$data = $this->db->query("SELECT * FROM likes WHERE page_id = ".$this->data['id']." and isnull(deleted) order by id desc", NULL);

		$out = array();

		if(!empty($data)){
			
			foreach ($data as $item) {
			
				$obj = new like();
				$obj->populate($item);
				$out[] = $obj;
			}
		}


		return $out;
	}
	
	/**
	* Create a cache of the page
	*/
	public function publish () {
		
		// create a cache of the page
		$fn = Config::$cache_path."/pages/".$this->data['handle'].'.html';
		
		$view = new PageViewer();

		$view->page = $this;

		$object = $this->getProfile();
		$view->object = $object;

		// get page contents
		$sections = $this->getPageContents();
		$view->sections = $sections;

		$template = file_get_contents(Config::$base_path.'/templates/front/page.php');
		$html = $view->component($template);

		// update SEO content				
		$this->data['name'] = $object->name;
		if(!empty($sections['a']['image'])){
			$this->data['image'] = $sections['a']['image'];
		}

		$desc = (!empty($sections['a']['body'])) ? $sections['a']['body'] : '';
		$this->data['description'] = $desc;

		$this->data['skills'] = $object->getSkillString();				

		$this->data['published'] = "NOW";
		$this->data['active'] = 1;
		$this->save();

		file_put_contents($fn, $html);
		
		return true;
	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
