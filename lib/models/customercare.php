<?php

/**********************************************
* CustomerCare Class
**********************************************/

class CustomerCare extends Model{

	public $table = 'customer_cares';

	public $fields = array(
		'id',
		'customer_id',
		'user_id',
		'type',
		'reason',
		'notes',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated user
	* Returns user object or NULL 
	*/

	public function getUser (){

		$data = $this->db->query("SELECT * FROM users WHERE id = ".$this->data['user_id'], NULL, 1);

		if(!empty($data)){
			$obj = new user();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
