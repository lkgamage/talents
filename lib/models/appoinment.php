<?php

/**********************************************
* Appoinment Class
**********************************************/

class Appoinment extends Model{

	public $table = 'appoinments';

	public $fields = array(
		'id',
		'parent_id',
		'profile_id',
		'agency_id',
		'package_id',
		'booking_id',
		'event_id',
		'description',
		'note',
		'all_day',
		'time_off',
		'begins',
		'duration',
		'rest_time',
		'confirmed',
		'expired',
		'is_private',
		'created',
		'modified',
		'deleted'
	);

	/**
	* Check if given appinment is associaed witha  abookng
	*/
	public function isBooking () {
		return !empty($this->data['booking_id']);	
	}
	
	/**
	* Check if this ap[pointment is a past day/time appointment
	*/
	public function isPast () {
		return time() > strtotime($this->data['begins']);	
	}
	
	/**
	* check if current user is the owner
	*/
	public function isOwner () {
		
		$profile = Session::getProfile();
		
		if(!empty($this->data['talent_id']) && $profile['type'] == 't' && $this->data['talent_id'] == $profile['id']){
			return true;				
		}
		elseif(empty($this->data['talent_id']) && $profile['type'] == 'a' && $this->data['agency_id'] == $profile['id']) {
			return true;
		}

		return false;
	}
	
	/**
	* Check if current profile can delete the appointment
	*/
	public function canDelete () {
		
		$profile = Session::getProfile();
		
		// confirmed system generated appointments can not be deleted, unless booking cancelled
		if($this->data['confirmed'] == 1 && !empty($this->data['booking_id'])){
			return false;
		}
		
		// unconfirmed appoitments or talent created appointments
		if(!empty($this->data['talent_id']) && $profile['type'] == 't' && $this->data['talent_id'] == $profile['id']){
			return true;				
		}
		
		// unconfirmed  or agency created appoitments
		if(!empty($this->data['agency_id']) && $profile['type'] == 'a' && $this->data['agency_id'] == $profile['id']) {
			return true;
		}
		
		return false;
	}
	
	/**
	* Get status message to display
	*/
	public function getStatus () {
	
		$t = time();
		if($this->data['confirmed'] == 1){
			return 'Confirmed';	
		}
		elseif(!empty($this->data['expired']) && $t > strtotime($this->data['expired'])){
			return 'Expired';	
		}
		else{
			return 'Scheduled ';	
		}
		
	}


	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}

	/**
	* Get associated talent
	* Returns talent object or NULL 
	*/

	public function getTalent (){

		$data = $this->db->query("SELECT * FROM talents WHERE id = ".$this->data['talent_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talent();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated agency
	* Returns agency object or NULL 
	*/

	public function getAgency (){

		$data = $this->db->query("SELECT * FROM agencies WHERE id = ".$this->data['agency_id'], NULL, 1);

		if(!empty($data)){
			$obj = new agency();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated talent_package
	* Returns talent_package object or NULL 
	*/

	public function getTalentPackage (){

		$data = $this->db->query("SELECT * FROM talent_packages WHERE id = ".$this->data['package_id'], NULL, 1);

		if(!empty($data)){
			$obj = new talentPackage();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	
	
	/**
	* Gives printable date/times
	* return an array
	* array(
	*
	)
	*/
	public function getTimes () {
		
		return Util::calcAppointmentTime ($this->data['begins'], $this->data['duration'], $this->data['rest_time']);
		
		
	}
	
	
	/**
	* Get detail information about the appoitment
	*/
	
	public function getinfo () {
	
	
		$sql = "SELECT a.id AS appointment__id, 
a.profile_id AS appointment__talent_id, 
a.agency_id AS appointment__agency_id, 
a.package_id AS appointment__package_id, 
a.booking_id AS appointment__booking_id, 
a.event_id AS appointment__event_id, 
a.description AS appointment__description, 
a.note AS appointment__note, 
a.all_day AS appointment__all_day, 
a.time_off AS appointment__time_off, 
a.begins AS appointment__begins, 
a.duration AS appointment__duration, 
a.rest_time AS appointment__rest_time, 
a.confirmed AS appointment__confirmed, 
a.expired AS appointment__expired, 
a.is_private AS appointment__is_private, 
a.created AS appointment__created, 
a.modified AS appointment__modified, 
a.deleted AS appointment__deleted,
t.id AS talent__id, 
t.customer_id AS talent__customer_id, 
t.verified AS talent__verified, 
t.name AS talent__name, 
t.timeoffset AS talent__timeoffset, 
t.timezone AS talent__timezone, 
t.currency AS talent__currency, 
t.gender AS talent__gender, 
t.active AS talent__active, 
ti.i400 AS talent__i400, 
ti.thumb AS talent__thumb, 
ti.url AS talent__url,
tp.id AS talent__page_id,
tp.handle AS talent__handle,
p.active AS booking__package_active, 
p.name AS booking__package_name,
p.items AS booking__package_items, 
p.timespan AS booking__package_timespan, 
p.interval AS booking__package_interval,  
p.fee AS booking__package_fee, 
p.is_private AS booking__package_is_private, 
p.fulday AS booking__package_fulday,
s.name AS booking__skill_name,
b.id AS booking__id, 
b.original_id AS booking__original_id, 
b.customer_id AS booking__customer_id, 
b.event_id AS booking__event_id, 
b.talent_id AS booking__talent_id, 
b.agency_id AS booking__agency_id, 
b.location_id AS booking__location_id, 
b.charitable AS booking__charitable, 
b.timeoffset AS booking__timeoffset, 
b.session_start AS booking__session_start, 
b.session_ends AS booking__session_ends, 
b.status AS booking__status, 
b.status_changed AS booking__status_changed, 
b.talent_fee AS booking__talent_fee, 
b.other_exp AS booking__other_exp, 
b.charges AS booking__charges, 
b.tax AS booking__tax, 
b.discount AS booking__discount, 
b.total AS booking__total,
b.currency AS booking__currency, 
b.additional_people AS booking__additional_people, 
b.due_date AS booking__due_date, 
b.created AS booking__created, 
b.modified AS booking__modified, 
b.deleted AS booking__deleted,
bl.venue AS booking__venue, 
bl.address AS booking__address, 
bl.city AS booking__city, 
bl.region AS booking__region, 
bl.postalcode AS booking__postalcode, 
bl.country AS booking__country,
e.id AS event__id, 
e.customer_id AS event__customer_id, 
e.name AS event__name, 
e.description AS event__description, 
e.begins AS event__begins, 
e.ends AS event__ends, 
e.charitable AS event__charitable, 
e.deleted AS event__deleted,
et.name AS event__type_name, 
et.corporate AS event__corporate,
el.venue AS event__venue, 
el.address AS event__address, 
el.city AS event__city, 
el.region AS event__region, 
el.postalcode AS event__postalcode, 
el.country AS event__country,
g.id AS agency__id, 
g.customer_id AS agency__customer_id, 
g.name AS agency__name, 
g.verified AS agency__verified, 
g.timezone AS agency__timezone, 
g.timeoffset AS agency__timeoffset, 
g.active AS agency__active,
g.deleted AS agency__deleted,
ap.id AS agency__page_id, 
ap.handle AS agency__handle,
ai.i400 AS agency__i400, 
ai.thumb AS agency__thumb, 
ai.url AS agency__url
FROM appoinments a 
LEFT JOIN profiles t ON ( t.id = a.profile_id)
LEFT JOIN images ti ON ( ti.id = t.image_id)
LEFT JOIN pages tp ON ( tp.profile_id = t.id)
LEFT JOIN talent_packages p ON ( p.id = a.package_id)
LEFT JOIN categories c ON (c.id = p.category_id)
LEFT JOIN skills s ON (s.id = c.skill_id)
LEFT JOIN bookings b ON ( b.id = a.booking_id)
LEFT JOIN locations bl ON ( bl.id = b.location_id)
LEFT JOIN `events` e ON ( e.id = a.event_id)
LEFT JOIN event_types et ON ( et.id = e.event_type)
LEFT JOIN locations el ON ( el.id = e.location_id)
LEFT JOIN profiles g ON ( g.id = a.agency_id)
LEFT JOIN pages ap ON ( ap.profile_id = g.id)
LEFT JOIN images ai ON ( ai.id = g.image_id)
WHERE a.id = ".$this->data['id'];

		$data = $this->db->query($sql, NULL , 1);
		
		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		return $out;
		
	}



	/**
	* Get associated event
	* Returns event object or NULL 
	*/

	public function getEvent (){

		$data = $this->db->query("SELECT * FROM events WHERE id = ".$this->data['event_id'], NULL, 1);

		if(!empty($data)){
			$obj = new event();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated profile
	* Returns profile object or NULL 
	*/

	public function getProfile (){

		$data = $this->db->query("SELECT * FROM profiles WHERE id = ".$this->data['profile_id'], NULL, 1);

		if(!empty($data)){
			$obj = new profile();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
