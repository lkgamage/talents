<?php

/**********************************************
* MessageRecipient Class
**********************************************/

class MessageRecipient extends Model{

	public $table = 'message_recipients';

	public $fields = array(
		'id',
		'board_id',
		'message_id',
		'customer_id',
		'seen',
		'notified',
		'created',
		'modified',
		'deleted'
	);


	/**
	* Get associated customer
	* Returns customer object or NULL 
	*/

	public function getCustomer (){

		$data = $this->db->query("SELECT * FROM customers WHERE id = ".$this->data['customer_id'], NULL, 1);

		if(!empty($data)){
			$obj = new customer();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated message
	* Returns message object or NULL 
	*/

	public function getMessage (){

		$data = $this->db->query("SELECT * FROM messages WHERE id = ".$this->data['message_id'], NULL, 1);

		if(!empty($data)){
			$obj = new message();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated message_board
	* Returns message_board object or NULL 
	*/

	public function getMessageBoard (){

		$data = $this->db->query("SELECT * FROM message_boards WHERE id = ".$this->data['board_id'], NULL, 1);

		if(!empty($data)){
			$obj = new messageBoard();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}
?>
