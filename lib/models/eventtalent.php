<?php
		
/****************************
* Eventtalent Class
*****************************/

class Eventtalent extends Model{

	public $table = 'event_talents';

	public $fields = array(
			'id',
			'event_id',
			'category_id',
			'booking_id',
			'status',
			'created',
			'modified',
			'deleted'
		);
	



	/**
	* Get associated event
	* Returns event object or NULL 
	*/

	public function getEvent (){

		$data = $this->db->query("SELECT * FROM events WHERE id = ".$this->data['event_id'], NULL, 1);

		if(!empty($data)){
			$obj = new event();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}



	/**
	* Get associated booking
	* Returns booking object or NULL 
	*/

	public function getBooking (){

		$data = $this->db->query("SELECT * FROM bookings WHERE id = ".$this->data['booking_id'], NULL, 1);

		if(!empty($data)){
			$obj = new booking();
			$obj->populate($data);
			return $obj;;
		}


		return NULL;
	}


}

?>