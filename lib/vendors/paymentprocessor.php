<?php

/*************************************
* Stripe Pyement processor class
*  Woks with APi version 3
**************************************/

class PaymentProcessor  {
	
	private $stripe;
	
	
	/**
	* Newly created subscription
	*/
	public $subscription;
	
	
	 function __construct ( ) {

       $this->stripe = new \Stripe\StripeClient(Config::$stripe_api_key);
		
	 }
	
	/**
	* Create product in stripe
	*/
	 function savePackage (&$package) {
		 
		 $skill = $package->getSkill();
		 
		 applog("Creating product for ".$skill->name.' - '.$package->name.", id:".$package->id);
		 
		 if($package->price > 0) {
			 
			 if($package->isEmpty('product_id')) {
			 
				 // adding package
				 $product = $this->stripe->products->create(['name' => $skill->name.' - '.$package->name]);
				 //metadata
				 
				  applog("Product create response :".json_encode($product));

				 if(!empty($product) && !empty($product->id)){
					 $package->product_id = $product->id;
					 $package->save();
					 applog('Product created, '.$product->id);
				 }
				 else{
					 applog("Product creating failed, abort");
					 return false;
				 }
		 	}
			 else{	
				 
				 $product = $this->stripe->products->update($package->product_id, ['name' => $skill->name.' - '.$package->name, 'active' => true ]);
				 
				 applog("Product update request response: ".json_encode($product));
				 
				 
				 if(!empty($product) && !empty($product->id)){
				 	applog("Product already created, product name updated");
				 }
				 else{
					 applog("Product already created, product name update failed, abort");
				 }
			 }
			 
			 if($package->isEmpty('price_id')) {
			 
				 // adding price
				$price =  $this->stripe->prices->create([
					 'product' => $package->product_id,
					'unit_amount' => round($package->price*100),
					'currency' => 'usd',
					'recurring' => ['interval' => ($package->validity == 365 ? 'year' : 'month')],
				 ]);
				 //metadata
				  applog("Price create request response: ".json_encode($product));

				 if(!empty($price) && !empty($price->id)){
					 $package->price_id = $price->id;				 
					 $package->save();
					 applog('Price created, '.$price->id);
				 }
				 else{
					 applog("Price creating failed, package {$package->id} abort");
					 return false;
				 }				 
			 }
			 else{				
				 $price = $this->stripe->prices->update($package->price_id, ['unit_amount' => round($package->price*100)] );
				 
				  applog("Price update request response: ".json_encode($product));
				 
				 if(!empty($price) && !empty($price->id)){
				  	applog("Price already created, price updated {$package->id}");
				 }
				 else{
					 applog("Price already created, price update failed {$package->id}, abort");
				 }
			 }
			 
			 
			 return true;
		 }
		 
	 }
	
	
	/**
	* Delete package
	*/
	public function deletePackage ($package){
		
		if(!$package->isEmpty('product_id')) {		
			$this->stripe->products->update($package->product_id, ['active' => false]);	
			applog("Package {$package->id} archived");
		}
		else{
			applog("Package {$package->id} has no product id, abort");
		}
	}
	
	
	/**
	* Create customer
	*/
	public function createCustomer (&$customer) {
		
		applog("Creating customer record in stripe for ".$customer->id);
		
		$contact = $customer->getContact();
		
		$data = [
			'name' => $contact['firstname']." ".$contact['lastname'],
			'phone' => DataType::$countries[$contact['country_code']][1].$contact['phone'],
		];
		
		if(strpos(strtolower($contact['email']), '@curatalent.com') === false){
			$data['email'] = $contact['email'];
		}
		
		$data['address'] = [
			'city' => $contact['city'],
			'state' => $contact['region'],
			'country' => $contact['country']
		];
		
		applog("Stripe cutomer create request :".json_encode($data));
		
		$stripecustomer = $this->stripe->customers->create($data);
		
		
		applog("Stripe cutomer create response :".json_encode($stripecustomer));
		
		if(!empty($stripecustomer) && !empty($stripecustomer->id)){
			$customer->stripe_id = $stripecustomer->id;
			$customer->save();
			applog("Customer record created in stripe, ".$customer->stripe_id);
			return true;
		}
		else{
			applog("Stripe customer record creation failed, abort");
		}
		
		return false;
	}
	
	
	/**
	* Creating a pending subscription
	*/
	public function createSubscription (&$invoice, $add_trial = false){
		
		$info = $invoice->getinfo();
		
		//check if there are any subscription secrets in pending subscription,
		// if so, return it
		if(!empty($info['subscription']['secret'])){
			
			// if last modified around 12 hours
			if(time() - strtotime($info['subscription']['modified']) < 43200){
				return ['subscription_id' => $info['category']['stripe_id'], 'intent' => $info['subscription']['secret']];
			}			
		}
		
		$customer = new Customer();
		$customer->populate($info['customer']);
		
		applog("Creating stripe subscription invoice {$invoice->id}");
		
		if(empty($info['customer']['stripe_id'])){
						
			if(!$this->createCustomer($customer)){
				applog("Stripe customer creating failed, abort");
				return false;
			}
			
			$info['customer']['stripe_id'] = $customer->stripe_id;
		}
		
		if(empty($info['package']['price_id'])){
			
			$plan = new Package();
			$plan->populate($info['package']);
			
			if(!$this->savePackage($plan)){
				applog("Stripe package creating failed, abort");
				return false;
			}
			
			$info['package']['price_id'] = $plan->price_id;
		}
		
		if($add_trial){
			// adding tril period until next billing cycle starts
			$sub = [
				'customer' => $customer->stripe_id,	
				'items' => [[
					'price' => $info['package']['price_id'],
				]],
				'payment_behavior' => 'default_incomplete',
				'trial_end' => strtotime($info['subscription']['exp_date']),
				'expand' => ['latest_invoice.payment_intent'],
			];
			
			$pm = $customer->getRecentPaymentMethod();
			if(!empty($pm)){
				$sub['default_payment_method'] = $pm->method_id;
			}
		}
		else{
			$sub = [
				'customer' => $customer->stripe_id,
				'items' => [[
					'price' => $info['package']['price_id'],
				]],
				'payment_behavior' => 'default_incomplete',
				'expand' => ['latest_invoice.payment_intent'],
			];
		}
		applog("Subscription will be created as ".json_encode($sub));
		
		try {

			$subscription = $this->stripe->subscriptions->create($sub);
		}
		catch(Exception $ex){			
			applog("Subscription creation failed, {$ex->getMessage()}, abort");
			return false;
		}
		
		applog("Stripe subscription created ".$subscription->id.", status {$subscription->status}");
		
		if(!empty($subscription) && !empty($subscription->id) ){
			
			// adding subscription id into category
			$category = new Category();
			$category->populate($info['category']);		
			$sts = $category->addStripeSubscription ($subscription->id);
			
			// assign new stripe subscription id with this local subscription
			$ls = new Subscription();
			$ls->populate($info['subscription']);
			$ls->catsub_id = $sts->id;
			//$ls->begin_date = $subscription->status == 'trialing' ? date("Y-m-d", $subscription->trial_end) : date("Y-m-d",$subscription->current_period_start);
			$ls->exp_date = date("Y-m-d",$subscription->current_period_end);
			$ls->save();
			
			
			// creating new payment record for this subscription		
			$payment = new Payment();
			$payment->invoice_id = $invoice->id;			
			$payment->currency = 'USD';
			$payment->intent_id = (!empty($subscription->latest_invoice) && !empty($subscription->latest_invoice->payment_intent)) ? $subscription->latest_invoice->payment_intent->id : '';
			$payment->amount = $invoice->amount;
			$payment->status = DataType::$PAYMENT_PROCESSING;
			$payment->success = 0;
			$payment->released = 0;
			$payment->save();
			
			//updating invoice
			$invoice->stripe_id = $subscription->latest_invoice->id;
			$invoice->save();
			
			$this->subscription = $subscription;
			
			if(!empty($subscription->latest_invoice) && $subscription->latest_invoice->amount_due > 0){
				applog("Stripe subscription created for invoice {$invoice->id}, subscription id : {$subscription->id}, intent : {$subscription->latest_invoice->payment_intent->client_secret}, payment id : {$payment->id}");
				
				return ['intent' =>  $subscription->latest_invoice->payment_intent->client_secret ];
			}
			else{
				applog("Stripe subscription created for invoice {$invoice->id}, subscription id : {$subscription->id}, NO intent as this is package change, payment id : {$payment->id}");
				
				return true;
			}
						
		}
		
		applog("Subscription creation failed, abort");
		return false;
	}
	
	/**
	* Get  subscription status 
	* 
	* important part in subscription object is,
	* status : incomplete/active
	* cancel_at_period_end,canceled_at, cancel_at, ended_at 
	*/
	
	public function getSubscription ($id){
		
		try {
			return $this->stripe->subscriptions->retrieve( $id, []);			
		}
		catch(Exception $ex){
			applog("Subscription retrive failed, {$ex->getMessage()}, abort");
		}
		
		return false;
		
	}
	
	/**
	* Update subscription 
	*/
	public function updateSubscription (&$category, $info = NULL){
		
		$info = (!empty($info)) ? $info : $category->getinfo();
		
				
		return $this->stripe->subscriptions->update($category->stripe_id,[
			'proration_behavior' => 'none',
			'items' => [
				[ 'price' => $info['package']['price_id'] ]
			]
		]); 
		
	}
	
	/**
	* Cancel subscription
	*/
	public function cancelSubscription ($subscription_id, $cancel_now = false) {
		
		if($cancel_now) {
			
			try {
				$subscription = $this->stripe->subscriptions->cancel($subscription_id);		
			}
			catch(Exception $ex){
				applog("Subscription immediatly cancellation failed, {$ex->getMessage()}, abort");
				return false;
			}
		}
		else {
			try {
				$subscription = $this->stripe->subscriptions->update($subscription_id, ['cancel_at_period_end' => true]);
			}
			catch(Exception $ex){
				applog("Subscription update/cancellation failed, {$ex->getMessage()}, abort");
				return false;
			}
		}
		
		return $subscription;
	}
	
	/**
	* Reactivate subscription, subscription must be in the active period
	*/
	public function activateSubscription ($subscription_id){
		
		try {
			$subscription = $this->stripe->subscriptions->update($subscription_id, ['cancel_at_period_end' => false]);
		}
		catch(Exception $ex){
			applog("Subscription update/cancellation failed, {$ex->getMessage()}, abort");
			return false;
		}
		return $subscription;
	}
	
	
	/**
	* Get invoice
	* status : open / paid
	*/
	public function getInvoice ($id) {
		
		try {
			return $this->stripe->invoices->retrieve( $id, []);			
		}
		catch(Exception $ex){
			applog("Invoice retrive failed, {$ex->getMessage()}, abort");
		}
		
		return false;
		
	}
	
	/**
	* Get payment intent detail
	* status : succeeded / requires_payment_method
	*/
	public function getPaymentIntent ($id) {
		
		try {
			return $this->stripe->paymentIntents->retrieve( $id, []);			
		}
		catch(Exception $ex){
			applog("Payment intent retrive failed, {$ex->getMessage()}, abort");
		}
		
		return false;
		
	}
	
	/**
	* Get payment method
	* Card brand. Can be amex, diners, discover, jcb, mastercard, unionpay, visa, or unknown.
	*/
	public function getPaymentMethod ($id) {
		
		try {
			return $this->stripe->paymentMethods->retrieve( $id, []);			
		}
		catch(Exception $ex){
			applog("Payment intent retrive failed, {$ex->getMessage()}, abort");
		}
		
		return false;
		
	}
	
	
	/**
	* Create discount 
	*/
	public function createDiscount ($amount) {
		
		try {
			return $this->stripe->coupons->create(['amount_off' => $amount*100, 'duration' => 'once']);
		}
		catch(Exception $ex) {
			applog("Payment intent retrive failed, {$ex->getMessage()}, abort");
		}
		
	}
		
	
	/**
	* Create a payment intent for make payment
	*/
	public function createIntent ($amount, $meta = []){
		
		return $intent = \Stripe\PaymentIntent::create([
		  'amount' => round($amount*100), // convert to cents
		  'currency' => 'usd',
		  // Verify your integration in this guide by including this parameter
		  'metadata' => $meta,
		]);		
	}
	
	
	
	
}

?>