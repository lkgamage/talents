<?php

class Shoutout {

	
	/** Application token **/
	private $auth_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwZGI3YTM3MC01MDJhLTExZWMtODk1OS1hZGUwMDZjYjgwYjQiLCJzdWIiOiJTSE9VVE9VVF9BUElfVVNFUiIsImlhdCI6MTYzODA5MDIwOSwiZXhwIjoxOTUzNjIzMDA5LCJzY29wZXMiOnsiYWN0aXZpdGllcyI6WyJyZWFkIiwid3JpdGUiXSwibWVzc2FnZXMiOlsicmVhZCIsIndyaXRlIl0sImNvbnRhY3RzIjpbInJlYWQiLCJ3cml0ZSJdfSwic29fdXNlcl9pZCI6IjYyODk1Iiwic29fdXNlcl9yb2xlIjoidXNlciIsInNvX3Byb2ZpbGUiOiJhbGwiLCJzb191c2VyX25hbWUiOiIiLCJzb19hcGlrZXkiOiJub25lIn0.ii26_KFE0NsfqUFNvKVPaZ3zqJEFVmDWMu-DKo5qBOA';
	
	
	/** App url */
	private $application_url = 'https://api.getshoutout.com/coreservice/messages';
	
	/** From message **/
	private $from = 'Curatalent'; // 'Curatalent'; //ShoutDEMO '+14314300200';
	
	
	/* Phone number without country code */
	public $number = '';
		
	
	
	/** Country code */
	public $country = NULL;
	
	
	/** message **/
	public $message = '';
	
	
	public function send () {
		
		
		if(!empty($this->number) &&  !empty($this->message)){
			
			$url = $this->application_url;
			
			$this->number = ltrim($this->number, '+');
			
			$data = array(
				'source' => $this->from,
				'destinations' => array($this->number),
				'transports' =>  array('sms'),
				'content' => array('sms' => $this->message )
			);
			
			$data = json_encode($data);
			
			/*
			curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Apikey eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwZGI3YTM3MC01MDJhLTExZWMtODk1OS1hZGUwMDZjYjgwYjQiLCJzdWIiOiJTSE9VVE9VVF9BUElfVVNFUiIsImlhdCI6MTYzODA5MDIwOSwiZXhwIjoxOTUzNjIzMDA5LCJzY29wZXMiOnsiYWN0aXZpdGllcyI6WyJyZWFkIiwid3JpdGUiXSwibWVzc2FnZXMiOlsicmVhZCIsIndyaXRlIl0sImNvbnRhY3RzIjpbInJlYWQiLCJ3cml0ZSJdfSwic29fdXNlcl9pZCI6IjYyODk1Iiwic29fdXNlcl9yb2xlIjoidXNlciIsInNvX3Byb2ZpbGUiOiJhbGwiLCJzb191c2VyX25hbWUiOiIiLCJzb19hcGlrZXkiOiJub25lIn0.ii26_KFE0NsfqUFNvKVPaZ3zqJEFVmDWMu-DKo5qBOA' -d '{
"source": "ShoutDEMO",
"destinations": [
"94771234567"
],
"transports": [
"sms"
],
"content": {
"sms": "Sent via ShoutOUT Gateway"
}
}' 'https://api.getshoutout.com/coreservice/messages'
*/
			
			$headers = array();
			$headers[] = 'Content-Type: application/json';
			$headers[] = 'Accept: application/json';
			$headers[] = 'Authorization: Apikey '.$this->auth_token;

			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER,0);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//curl_setopt($ch, CURLOPT_USERPWD, $this->account_id . ":" . $this->auth_token);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$response =  curl_exec($ch);
			//$info = curl_getinfo($ch);
			curl_close($ch);
			
			////print_r($info);
			//echo $response;
			$response = json_decode($response, true);			
			
			// create sms log
			
			if(!empty($response['status']) && $response['status'] == 1001){
				return true;	
			}
			
		}
		
		return false;
		
	}
	
}



?>