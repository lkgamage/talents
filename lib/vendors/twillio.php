<?php

class Twillio {

	/** 
	* Application id
	*/
	private $account_id =  'AC2e9f5848309671cc573c69234ab79f4a';
	
	/** Application token **/
	private $auth_token = '15da7acadaee3703ed84b276e719d8a5';
	
	/** Mesaging service id **/
	private $service_id = 'MGa7cd6258f512f3078d715b730ae3da45';
	
	/** App url */
	private $application_url = 'https://api.twilio.com/2010-04-01/Accounts/';
	
	/** From message **/
	private $from = 'Curatalent'; //'+14314300200';
	
	
	/* Phone number without country code */
	public $number = '';
	
	
	
	
	/** Country code */
	public $country = NULL;
	
	
	/** message **/
	public $message = '';
	
	
	public function send () {
		
		
		if(!empty($this->number) &&  !empty($this->message)){
			
			$url = $this->application_url.$this->account_id.'/Messages.json';
			
			$txt = "To=".urlencode($this->number)
					."&From=".urlencode($this->from)
					."&Body=".urlencode($this->message)
					."&MessagingServiceSid=".urlencode($this->service_id);
			
			
			/*
			curl 'https://api.twilio.com/2010-04-01/Accounts/AC2e9f5848309671cc573c69234ab79f4a/Messages.json' -X POST \
--data-urlencode 'To=+94715334575' \
--data-urlencode 'MessagingServiceSid=MGa7cd6258f512f3078d715b730ae3da45' \
--data-urlencode 'Body=Testing sms' \
-u AC2e9f5848309671cc573c69234ab79f4a:[AuthToken]

https://console.twilio.com/us1/develop/sms/try-it-out/send-an-sms?frameUrl=%2Fconsole%2Fsms%2Fgetting-started%2Fbuild%3Fx-target-region%3Dus1
*/
			
			$headers = array();

			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER,0);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $txt);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERPWD, $this->account_id . ":" . $this->auth_token);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$response =  curl_exec($ch);
			//$info = curl_getinfo($ch);
			curl_close($ch);
			
			//print_r($info);
			
			$response = json_decode($response, true);			
			
			// create sms log
			
			if(!empty($response['sid'])){
				return true;	
			}
			
		}
		
		return false;
		
	}
	
}



?>