<?php

class BulkGate {

	/** 
	* Application id
	*/
	private $application_id =  23056;
	
	/** Application token **/
	private $application_token = 'EKX7vS99pqX3eagrrOwoWMPzZDRTdWraLS4yHBb2ePCidK2XV6';
	
	/** App url */
	private $application_url = 'https://portal.bulkgate.com/api/1.0/simple/transactional';
	
	/** Test sender profile **/
	private $sender_id = "gOwn"; //"gShort";
	
	private $sender_value = '+4314300200';


	/* Phone number without country code */
	public $number = '';
	
	
	/** Country code */
	public $country = '';
	
	
	/** message **/
	public $message = '';
	
	
	public function send () {
		
		
		if(!empty($this->number) && !empty($this->country) && !empty($this->message)){
			
			$data = array(
				"application_id" => $this->application_id, 
				"application_token" => $this->application_token, 
				"number" => $this->number, 
				"text" => $this->message, 
				"sender_id" => $this->sender_id,
				"sender_id_value" => $this->sender_value
			);
			
			
			if(!empty($this->country)){
				$data['country'] = $this->country;
			}
			
			$data = json_encode($data);
			
			$headers = array();
			$headers[] = 'Content-Type: application/json';
			$headers[] = 'Cache-Control: no-cache';
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->application_url);
			curl_setopt($ch, CURLOPT_HEADER,0);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response =  curl_exec($ch);
			//$info = curl_getinfo($ch);
			curl_close($ch);
			
			$response = json_decode($response, true);
			print_r($response);
			
		}
		
		return false;
		
	}
	
}



?>