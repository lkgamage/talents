<?php
/*****************************************************
*	Mailgun Email delivering class
*	Delivers email through mailgun message api
*
*	Uage
*	-----
*	$email = new Mailer();
*	$email->subject = "Sending from email class";
*	$email->text = "This is my message";
*	$email->to = "lasantha@zurigroup.com, lkgamage@yahoo.com";
*	$email->send();
*	print_r($email->response);
******************************************************/

class Mailer {

	// Email sender address
	public $from = "info@mail.curatalent.com";
	
	public $from_name = 'Curatalent';
	
	
	// Receipients
	public $to = "";
	
	
	// email subject
	public $subject = "";
	
	
	// email body
	public $text = "";
	public $html = "";
	
	
	//  result
	public $response = "";

	
	function __construct (){
		
	}
	
	
	public function send (){
		
		$emails = array();
		
		if(!empty($this->to)){
			$emails = preg_split('/[,; ]/', $this->to);
			
			foreach ($emails as $i => $e){
				
				if(empty($e)){
					continue;	
				}
				
				$emails[$i] = trim($e);	
			}
		}
		else{
			return false;	
		}
		
		$mail = new PHPMailer\PHPMailer();
		
		$mail->isSMTP();
		$mail->isHTML(true);
				
		foreach ($emails as $email){
			$mail->addAddress($email);	
		}

		$mail->Subject    = $this->subject;
		$mail->Body       = $this->html;
	    $mail->AltBody    = $this->text;
		return $mail->Send();
		
	}
	
	
}

?>