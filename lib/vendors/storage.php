<?php

class Storage {	

	private $client;
	
	function __construct (){
		
		$this->client = Aws\S3\S3Client::factory(array(
			'credentials' => array(
				'key'    => Config::$aws_access_key,
				'secret' => Config::$aws_secret_key,
			),
			'region' => 'us-east-2',			
			'version' => '2006-03-01',
			'scheme' => 'http'
		)); 		
	}
	
	
	
	public function upload ($local, $remote, $client_id) {
		//putObject
		//$result['ObjectURL']
		
		if(file_exists($local)){
			
			return $this->client->putObject([
				'Bucket'     => Config::$aws_bucket,
				'Key'        => $remote,
				'SourceFile' => $local,
				'Metadata'   => ['client' => $client_id]
				]);		
		}
		
		
		
	}
	
	public function download ($fileurl, $id){
		
		$key = str_replace(Config::$image_url.'/', '', $fileurl);
		$local = Config::$base_path . "/tmp/" .$id;
		
		return $this->client->getObject([
				'Bucket'     => Config::$aws_bucket,
				'Key'        => $key,
				'SaveAs' => $local
				]);
		
		
	}
	
	
	public function delete ($fileurl) {
		//deleteObject
		// $result['DeleteMarker']
		
		$key = str_replace(Config::$image_url.'/', '', $fileurl);
		
		 $result = $this->client->deleteObject([
			'Bucket' => Config::$aws_bucket,
			'Key'    => $key
		]);
		
		return $result['DeleteMarker'];
		
	}
	
	public function uploadDir () {
		
	}
	
	
}

?>