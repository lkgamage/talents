<?php
/**
* @package Amplify
* Request object class
* 
* Manage basic request handing and map all request properties into an object

*/

class RequestObject {
	
	/** hold post parameters */
	public $post = array();
	
	/** hold url parameters */
	public $get = array();
	
	/** hold cookie values */	
	public $cookie = array();
	
	
	/** Is this an Ajax request */
	public $isAjax = false;	
	
	/** hold array of request urls */
	public $parameters = array();
	
	public static $paths = array();
	
	
	/** Reserved space to hold operation status **/
	public $status = false;
	
	/** Reserved space to hold operation message **/
	public $message = ""; 
	
	/** tracking code name */
	public static $tracking_key = 'ea_tracking_id';
	
	/** individual com tracking key name */
	public static $activity_key = 'aref';
	
	/** is request made with https */
	public static $isSSL = false;
	
	/** User IP address */
	public $ip = NULL;
	
	/** Request host header */
	public static $domain = NULL;
	
	/** Custom URL settings ***/
	public static $custom_url = NULL;
	
	
	/** Decode & validate request */
	function __construct (){
		
		$path = array("");
	
		if(!empty($_GET['path'])) {
			$path = explode("/", trim($_GET['path'],'/'));
			
			//unset($_GET['path']);
		}
		
		$request = array();
		foreach ($path as $key => $value){
					
			if(!empty($value)) {
				$request[] = strtolower($value);	
			}
		}
		
		$this->parameters = $request;
		self::$paths = $this->parameters;
		
		$this->get = $_GET;
		
		$this->post = $_POST;	
		
		$this->cookie = $_COOKIE;
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
			$this->isAjax = true;
		}
		
		if(isset($_GET['lng']) && isset(DataType::$languages[$_GET['lng']])){
			Util::setCookie(DataType::$COOKIE_LANGUAGE,$_GET['lng'] );
		}
		
		//print_r($_SERVER);
		
		/*
		$headers = getallheaders();
		if(isset($headers['X-Requested-With']) && $headers['X-Requested-With'] == 'XMLHttpRequest'){
			$this->isAjax = true;	
		}*/
		/*
		// ssl capture
		if(isset($headers['X-Is-SSL']) || isset($_SERVER['HTTPS'])){
			self::$isSSL = true;
		}
		
		
		if(isset($headers['X-Forwarded-For']) && !empty($headers['X-Forwarded-For'])){
			$this->ip = $headers['X-Forwarded-For'];
		}
		elseif(isset($_SERVER['REMOTE_ADDR'])){
			$this->ip = $_SERVER['REMOTE_ADDR'];	
		}
		
		
		self::$domain = strtolower($_SERVER['HTTP_HOST']);
		
		// capture tracking codes
		if(isset($_GET[self::$tracking_key])){
			$this->captureTracking(self::$tracking_key, $_GET[self::$tracking_key]);	
		}
		if(isset($_POST[self::$tracking_key])){
			$this->captureTracking(self::$tracking_key, $_POST[self::$tracking_key]);	
		}
		
		
		// capture individual activity id
		if(isset($_GET[self::$activity_key])){
			$this->captureTracking(self::$activity_key, $_GET[self::$activity_key]);	
		}
		if(isset($_POST[self::$activity_key])){
			$this->captureTracking(self::$activity_key, $_POST[self::$activity_key]);	
		}
		
		*/
		
	}
	
	/**
	* Check whether this request method is GET
	*/
	public function isGet (){
		
		return $_SERVER['REQUEST_METHOD'] === 'GET';
		
	}
	
	
	/**
	* Check whether this request method is POST
	*/
	public function isPost(){
		
		return $_SERVER['REQUEST_METHOD'] === 'POST';
		
	}
	
	/**
	* Check whether access with mobile device
	*/
	public function isMobile(){
		
		return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
		
	}
	
	/**
	* Check whether this request is made via AJAX
	*/
	public function isAjax (){
		
		return $this->isAjax;
		
	}
	
	/**
	* Stop script execution and redirect user to new page
	*/
	public function redirect ( $path ){
		
		header("Location:".Util::mapURL($path));
		exit();
		
	}
	
	/**
	* Set HTTP response code
	*/
	public function setResponseCode ( $code ){
		http_response_code($code);
	}
	
	
	/**
	* Capture tracking code and save it for future use
	*/
	private function captureTracking ( $key,  $code ){
		
		if($key == self::$activity_key){
			
			$id = base64_decode($code);
			$t = new Tracking($id);
			
			if($t->ready() && $t->visit == 0){
				$t->visit = 1;
				$t->save();	
			}
				
		}
				
		$code = trim($code);
		$_COOKIE[$key] = $code;
				
		setcookie($key, $code, strtotime('+1 day'), "/");
	}
	
	
	
	/**
	* Get tracking code if exists
	*/
	public static function getTrackingCode ( $with_name = false){
		
	
		if(isset($_COOKIE[self::$tracking_key])){			
			
			if($with_name){
				return self::$tracking_key."=".	$_COOKIE[self::$tracking_key];
			}
			else{
				return $_COOKIE[self::$tracking_key];
			}
		}
		
		return "";
	}
	
	
	
	
	
	
} // class

?>