<?php		
/** 
* Data buffer class
* Result paginate helper
*
* $buffer = new Buffer();
* $buffer->_datasql = "";
* $buffer->_countsql = "";
* $buffer->_component = "";
* $buffer->_pagesize = n;
* $buffer->_page = n;
* $html = $buffer->getPage();
* $total = $buffer->getTotal();
 **/

class Buffer extends Controller {
	
	/** Buffer data hasing key */
	private $_hash = "MDM5Y2IzZThkYmNkYzJlYzhhNDJhNzRlNzg5N2M0MWU";
	
	/** sql query to pull data **/
	public $_datasql;	
	
	/** sql query to get total # of records**/
	public $_countsql;
	
	/** component file name **/
	public $_component;
		
	/** page size **/
	public $_pagesize = 25;
	
	/** current page **/
	public $_page = 1;
	
	/** DB object **/
	private $_db;
	
	/** Cutsom data elements **/
	private $_data = array();
	
	
	
	function __construct ($key = null) {
		
		 parent::__construct();

        $this->_db = new Database();
		
		if(!empty($key)){
			$this->setKey($key);	
		}
		
	}
	
	/**
	* Re-initiate buffer class
	*/
	public function setKey ($key){
		
		$txt = base64_decode(urldecode($key));
		$txt = Util::decrypt($txt, $this->_hash);
		$txt = explode('^', $txt);
		
		foreach ($txt as $item){
		
			list($k, $v) = explode('~', $item);
			if(property_exists($this, $k)){
				$this->$k = $v;
			}
			else{
				$this->_data[$k] = $v;	
			}
		}
		
	}
	
	/**
	* Get key for this buffer object
	*/
	public function getKey (){
		
		$txt[] = "_datasql~".$this->_datasql;
		$txt[] = "_component~".$this->_component;
		$txt[] = "_pagesize~".$this->_pagesize;
		
		if(!empty($this->_data)) {
			foreach($this->_data as $k => $v){
				$txt[] = $k.'~'.$v;
			}
		}
		
		$txt = implode('^', $txt);
		
		$txt = Util::encrypt ($txt, $this->_hash);
		return urlencode(base64_encode($txt)); 		
		
	}
	
	/**
     * Magic method getter for class properies
     */
    public function __get ($name) {

		if($name == 'datasql'){
			return $this->_datasql;	
		}
		elseif ($name == 'component' ){
			return $this->_component;
		}
        elseif (isset($this->_data[$name])) {
            return $this->_data[$name];
        }
		

        return null;
    }

    /**
     * Magic method setter for class properties
     */
    public function __set ($name, $value) {

        $this->_data[$name] = $value;
    }
	
	/**
	* Get Total number of records
	*/
	public function getTotal () {
		
		if(!empty($this->_countsql)){
			
			$data = $this->_db->query($this->_countsql, NULL, 1);
			$keys = array_keys($data);
			return $data[$keys[0]];
				
		}
		
		return 0;
	}
	
	public function count (){
		return $this->getTotal();	
	}
	
	
	/** Get rendered html*/
	public function getPage ($page_num = NULL) {
		
		if(!empty($page_num)){
			$this->_page = $page_num;	
		}
		
		$skip = ($this->_page -1 )* $this->_pagesize;
		
		if(!empty($this->_datasql)){

			$this->view->data = $this->_db->query($this->_datasql, NULL, $this->_pagesize, $skip);
			
			if(!empty($this->_data)){
				
				foreach ($this->_data as $k => $v){
					$this->view->$k = $v;	
				}
			}
			return $this->component($this->_component);
		}
		
		return "Buffer Configuration error";
	}
	
	/** Get row page data */
	public function getData ($page_num = NULL) {
		
		if(!empty($page_num)){
			$this->_page = $page_num;	
		}
		
		$skip = ($this->_page -1 )* $this->_pagesize;
		
		if(!empty($this->_datasql)){			
			return $this->_db->query($this->_datasql, NULL, $this->_pagesize, $skip);
		}
		
		return "Buffer Configuration error";
	}
	
	
	/** Get all row page data */
	public function getAllData () {
		
		if(!empty($this->_datasql)){			
			return $this->_db->query($sql);
		}
		
		return "Buffer Configuration error";
	}
	
	/**
	* Manual Paginate 
	* button should contains data-label="" and 
	* data-theme="orange/blue" attributes
	**/
	public function manualPaginate ($container_id, $button_id){
		
		$total = $this->getTotal();
		if($total <= $this->_pagesize){
			return '<style>#'.$button_id.' {display:none}</style>';	
		}
		
		$txt = '<script type="text/javascript">'."\n";
		$txt .= "new manualbuffer('".$container_id."','".$button_id."', ".$total." ,'".$this->getKey()."'); \n";
		$txt .= "</script>";
		return $txt;
		
	}
}

?>