<?php

class DashboardEngine extends Engine {
	
	/*** Current customer object **/
	public $customer;
	
	/*** Customer info array ***/
	public $info;
	
	/*** Current profile data *****/	
	public $profile;
	

	/*** Is current profile if for agency ***/
	public $is_agency = false;
	
	/****
	* Creating dashboard object with 
	* user : current user object
	* $info : current user into
	*/
	function __construct ($user, $info) {
		
		parent::__construct();
		
		$this->customer = $user;
		$this->info = $info;
		
		// get current profile
		$session = Session::getProfile();
		
		if(!empty($session) && !empty($info['profiles'])){			
			
			foreach ($info['profiles'] as $pf){
				if($pf['id'] == $session['id']){
					$this->profile = $pf;
					break;
				}
			}			
		}
		
		
		if(!empty($this->profile)){
			$this->is_agency = !empty($this->profile['is_agency']);
		}

		
	}
	
	
	// get received bookings (for talents and agencies)
	public function getRecivedBookings () {
		
		if($this->is_agency){
			return App::getBookings(NULL, NULL, $this->profile['id'], NULL, NULL, 1, NULL, 'r');
		}
		else{
			return App::getBookings(NULL, $this->profile['id'],NULL, NULL, NULL, 1);
		}
				
	}
	
	// get requested bookings (as a client or agency)
	public function getRequestedBookings () {
		
		
		if ($this->is_agency){
			return App::getBookings(NULL, NULL, $this->profile['id'], NULL, NULL, 1, NULL, 'm');
		}
		else{
			return App::getBookings($this->customer->id,NULL, NULL, NULL, 1);
		}
		//Util::debug($this->info);
	}
	
	/** Get events **/
	public function getEvents () {

		if(empty($this->profile)) {
			return $this->customer->searchEvents(1, NULL, NULL);
			component('event/event_list', array('events' => $events));	
		}
		elseif($this->is_agency){
			return $this->customer->searchEvents(1, NULL, NULL, $this->profile['id']);
		}
		
		return [];		
	}
	
	// get latest messages 
	public function getMessages ($limit = 10) {
		
		$boards = $this->customer->getMessageBoards();
		$boards->_pagesize = 10;
		return $boards;
		
		
	}
	
	/** Get today appointments **/
	public function getAppointments () {
		
		if(!empty($this->profile)){
		
			$date = date("Y-m-d");
			
			$profile = new Profile();
			$profile->populate($this->profile);
		    return $profile->getAppoinments($date, NULL, true);
			
		}
		
		return [];
	}
	
	/**
	* Get subscription credits
	*/
	public function getSubscriptions () {

		
		if(!empty($this->profile)){

			return $this->db->query("SELECT * FROM subscription_view WHERE profile_id = ".$this->profile['id']);
			
		}
		
	}
	
	/**
	* Get public page statistics
	*/
	public function pageInsight () {
		
		if(!empty($this->profile)){
		
			return $this->db->query("SELECT p.*,
(SELECT COUNT(*) AS total FROM page_views WHERE page_id = p.id) AS views
FROM pages p
WHERE p.profile_id =".$this->profile['id'], NULL, 1);
			
			
		}
		 

		
	}
	
	
	/** get packages statistics **/
	public function packageStatistics () {
		
	}
	
}


?>