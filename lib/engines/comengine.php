<?php
/**
* Communication controlling engine
* Responsible for emails, SMS, push notifications and Whatsapp notifications
*/

class ComEngine extends Engine {
	
	
	/** 
	* created a new booking 
	* notifying talent
	*/
	public function bookingCreated ($booking ) {
		
		$contacts = $booking->getContacts();
		$info = $booking->getInfo();

		$para = array();		
		
		$reciepient = new Customer($contacts['talent']['customer_id']);	
		$para['date'] = Util::toDate($booking->session_start, true, $info['talent']['timeoffset'] );
		$para['due'] = Util::toDate($booking->due_date, true, $info['talent']['timeoffset'] );
		$name = $contacts['talent']['name'];
				
		$settings = $reciepient->getCustomerSettings()->toArray();
				
		$para['name'] = $reciepient->firstname;	
		//$para['name'] = $name;	
		$para['id'] = $booking->id;
		$para['place'] = $info['location']['display'];		
		$para['package'] = (!empty($info['talent']['skill_name']) ? $info['talent']['skill_name'].' - ' : '').$info['package']['name'].' ('.Util::ToTime( $info['package']['timespan']).')';
		$para['currency'] = $booking->isTalentRecipient() ? $info['talent']['currency'] : $info['agency']['currency'];
		$para['fees'] = currency($info['package']['fee'], $para['currency']);
		
		// add notification
		$client = $booking->isAgencyCreated() ? $contacts['agency']['name'] : $contacts['customer']['name'];
		$this->notify($reciepient->id, 'b', $booking->id, $client.' sent you a new booking request', ($booking->isAgencyCreated() ? $info['agency']['image_id'] : $info['customer']['image_id']));
		
		// sending email
		if(!empty($settings['alert1_email'])){
			
			if($booking->isTalentRecipient()){
				if($this->sendEmail('new_booking', $contacts['talent']['email'], $para)) {
					EmailLog( $contacts['talent']['customer_id'], $contacts['talent']['email_id'], 'new_booking');
				}
			}
			else{
				if($this->sendEmail('new_booking', $contacts['agency']['email'],$para)) {
					EmailLog( $contacts['agency']['customer_id'], $contacts['agency']['email_id'], 'new_booking');
				}
			}						
		}
		
		// sending SMS
		//if(!empty($settings['alert1_sms']) && !empty($info['package']['paid'])){
		if(!empty($settings['alert1_sms']) ){
			
			if($booking->isTalentRecipient()){
				$msg = "New Booking Request - ".Util::toDate($booking->session_start, true, $info['talent']['timeoffset'] );
			}
			else{
				$msg = "New Booking Request - ".Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
			}
			
			$msg .= ' at '.$info['location']['display'];
			
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			
			if($booking->isTalentRecipient()){
				$number = DataType::$countries[$contacts['talent']['country_code']][1].$contacts['talent']['phone'];				
				
			}
			else{
				$number = DataType::$countries[$contacts['agency']['country_code']][1].$contacts['agency']['phone'];				
				
			}
			
			$msg = substr($msg, 0,270);
				
			if($this->sendSMS($number, $msg)){
				
				if($booking->isTalentRecipient()){
					SMSLog($contacts['talent']['customer_id'], $contacts['talent']['phone_id'], 'new_booking');
				}
				else{
					SMSLog($contacts['agency']['customer_id'], $contacts['agency']['phone_id'], 'new_booking');
				}				
			}			
		}
		
		
		// sending push
		if($booking->isTalentRecipient() && !empty($contacts['talent']['devices'])){
			$devices = $contacts['talent']['devices'];
		}
		elseif (!empty($contacts['agency']['devices'])){
			$devices = $contacts['agency']['devices'];			
		}
		
		if(!empty($devices)){			
			foreach ($devices as $dev){
				
				$pd = array('type' => 'newbooking', 'id' => $booking->id);
			
				$this->sendPush($dev['push_id'], $client.' sent you a new booking request', $pd);	
				
			}			
		}
		
		
		
		
			
	}
	
	
	
	/** Talent or agency has accepted the bookiking */
	public function bookingAccepted ($booking) {
		
		$contacts = $booking->getContacts();
		$info = $booking->getInfo();
		
		$reciepient = $booking->getCustomer();
		$settings = $reciepient->getCustomerSettings()->toArray();
		
		if($booking->isTalentRecipient()){
			$talent = $contacts['talent']['name'];
		}
		else{
			$talent = $contacts['agency']['name'];
		}
			
		if($booking->isAgencyCreated()){
			
			$para['date'] = Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
			$para['due'] = Util::toDate($booking->due_date, true, $info['agency']['timeoffset'] );
		}
		else{
			$para['date'] = Util::toDate($booking->session_start, true, $info['customer']['timeoffset'] );
			$para['due'] = Util::toDate($booking->due_date, true, $info['customer']['timeoffset'] );
		}
		
		$para['talent'] = $talent;
		$para['name'] = $reciepient->firstname;		
		$para['id'] = $booking->id;
		$para['place'] = $info['location']['display'];		
		$para['package'] = (!empty($info['talent']['skill_name']) ? $info['talent']['skill_name'].' - ' : '').$info['package']['name'].' ('.Util::ToTime( $info['package']['timespan']).')';
		$para['currency'] = $booking->isAgencyCreated() ? $info['agency']['currency'] : $info['customer']['currency'];
		
		// add notification
		$this->notify($reciepient->id, 'b', $booking->id, $talent." accepted your booking request", ($booking->isTalentRecipient() ? $info['talent']['image_id'] : $info['agency']['image_id']));

		// sending email
		if(!empty($settings['alert2_email'])){
			
			if($booking->isAgencyCreated()){
				if($this->sendEmail('booking_accept', $contacts['agency']['email'], $para)) {
					EmailLog( $contacts['agency']['customer_id'], $contacts['agency']['email_id'], 'booking_accept');
				}
			}
			else{
				if($this->sendEmail('booking_accept', $contacts['customer']['email'],$para)) {
					EmailLog( $contacts['customer']['id'], $contacts['customer']['email_id'], 'booking_accept');
				}
			}						
		}
		
		// sending SMS
		//if(!empty($settings['alert2_sms']) && !empty($info['package']['paid'])){
		if(!empty($settings['alert2_sms'])){
			
			if($booking->isAgencyCreated()){
				$msg = $talent." accepted your booking request - ".Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
			}
			else{
				$msg = $talent." accepted your booking request - ".Util::toDate($booking->session_start, true, $info['customer']['timeoffset'] );
			}
			
			$msg .= ' at '.$info['location']['display'];
			
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			
			if($booking->isAgencyCreated()){
				$number = DataType::$countries[$contacts['agency']['country_code']][1].$contacts['talent']['phone'];				
				
			}
			else{
				$number = DataType::$countries[$contacts['customer']['country_code']][1].$contacts['customer']['phone'];				
				
			}
			
			$msg = substr($msg, 0,270);
				
			if($this->sendSMS($number, $msg)){
				
				if($booking->isAgencyCreated()){
					SMSLog($contacts['agency']['customer_id'], $contacts['agency']['phone_id'], 'booking_accept');
				}
				else{
					SMSLog($contacts['customer']['id'], $contacts['customer']['phone_id'], 'booking_accept');
				}				
			}			
		}
		
		
		// sending push
		$devices = $booking->isAgencyCreated() ? $contacts['agency']['devices'] : $contacts['customer']['devices'];
		
		$image = $booking->isTalentRecipient() ? $info['talent']['thumb'] : $info['agency']['thumb'];
		
		if(!empty($devices)){	
				
			foreach ($devices as $dev){
				
				$pd = array('type' => 'bookingaccepted', 'id' => $booking->id, 'image' => $image);
			
				$this->sendPush($dev['push_id'], $talent.' accepted your booking request', $pd);	
				
			}			
			
		}
		
		
	}
	
	/** Client or agency confirm the booking */
	public function bookingConfirmed ($booking) {
		
		$contacts = $booking->getContacts();
		$info = $booking->getInfo();
		$para = array();				
		
		if($booking->isTalentRecipient()){
			$talent = $contacts['talent']['name'];
			$reciepient = new Customer($contacts['talent']['customer_id']);
			$para['date'] = Util::toDate($booking->session_start, true, $info['talent']['timeoffset'] );
		}
		else{
			$talent = $contacts['agency']['name'];
			$reciepient = new Customer($contacts['agency']['customer_id']);
			$para['date'] = Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
		}
		
		if($booking->isAgencyCreated()){
			$sender = new Customer($info['agency']['customer_id']);
			$customer = $info['agency']['name'];	
		}
		else{
			$sender = new Customer($info['customer']['id']);
			$customer = $info['customer']['firstname'];	
		}
		
		$settings = $reciepient->getCustomerSettings()->toArray();
				
		$para['talent'] = $talent;
		$para['name'] = $reciepient->firstname;		
		$para['id'] = $booking->id;
		$para['place'] = $info['location']['display'];		
		$para['package'] = (!empty($info['talent']['skill_name']) ? $info['talent']['skill_name'].' - ' : '').$info['package']['name'].' ('.Util::ToTime( $info['package']['timespan']).')';
		$para['currency'] = $booking->isTalentRecipient() ? $info['talent']['currency'] : $info['agency']['currency'];
		
		// add notification
		$this->notify($reciepient->id, 'b', $booking->id, $customer." has confirmed the booking", ($booking->isAgencyCreated() ? $info['agency']['image_id'] : $info['customer']['image_id']));

		// sending email to talent
		if(!empty($settings['alert2_email'])){
			
			if($booking->isTalentRecipient()){
				if($this->sendEmail('booking_confirm_talent', $contacts['talent']['email'], $para)) {
					EmailLog( $contacts['talent']['customer_id'], $contacts['talent']['email_id'], 'booking_confirm');
				}
			}
			else{
				if($this->sendEmail('booking_confirm_talent', $contacts['agency']['email'], $para)) {
					EmailLog( $contacts['agency']['customer_id'], $contacts['agency']['email_id'], 'booking_confirm');
				}
			}						
		}
		
		// sending SMS to talent
		if(!empty($settings['alert2_sms']) ){
					
			
			if($booking->isTalentRecipient()){
				$msg = $customer." has confirmed the booking - ".Util::toDate($booking->session_start, true, $info['talent']['timeoffset'] );
			}
			else{
				$msg = $customer." has confirmed the booking - ".Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
			}
			
			$msg .= ' at '.$info['location']['display'];
			
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			
			if($booking->isTalentRecipient()){
				$number = DataType::$countries[$contacts['talent']['country_code']][1].$contacts['talent']['phone'];				
				
			}
			else{
				$number = DataType::$countries[$contacts['agency']['country_code']][1].$contacts['agency']['phone'];				
				
			}
			
			$msg = substr($msg, 0,270);
				
			if($this->sendSMS($number, $msg)){
				
				if($booking->isTalentRecipient()){
					SMSLog($contacts['talent']['customer_id'], $contacts['talent']['phone_id'], 'new_booking');
				}
				else{
					SMSLog($contacts['agency']['customer_id'], $contacts['agency']['phone_id'], 'new_booking');
				}				
			}			
				
		}
		
		
		// sending push
		$devices = $booking->isTalentRecipient() ? $contacts['talent']['devices'] : $contacts['agency']['devices'];
		
		$image = $booking->isAgencyCreated() ? $info['agency']['thumb'] : $info['customer']['thumb'];
		
		if(!empty($devices)){	
				
			foreach ($devices as $dev){
				
				$pd = array('type' => 'bookingconfirmed', 'id' => $booking->id, 'image' => $image);
			
				$this->sendPush($dev['push_id'], $customer." has confirmed the booking", $pd);	
				
			}			
		}
		
		
		// send email to booking sender ********************************
		$settings = $sender->getCustomerSettings()->toArray();
		
		if($booking->isAgencyCreated()){
			
			$para['date'] = Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );			
		}
		else{
			$para['date'] = Util::toDate($booking->session_start, true, $info['customer']['timeoffset'] );			
		}
		
		
		
		$para['name'] = $sender->firstname;	
		$para['currency'] = $booking->isAgencyCreated() ? $info['agency']['currency'] : $info['customer']['currency'];
		
		$para['fees'] = currency($info['package']['fee'], $para['currency'] );
		$para['expences'] = (!empty($info['booking']['other_exp'])) ? currency($info['booking']['other_exp'], $para['currency'] ) : NULL;
		$para['discount'] = (!empty($info['booking']['discount'])) ? currency($info['booking']['discount'], $para['currency'] ) : NULL;
		$para['total'] = currency($info['booking']['total'], $para['currency'] );
		
		if(!empty($settings['alert2_email'])){
			
			if($booking->isAgencyCreated()){
				
				$para['date'] = Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
				
				if($this->sendEmail('booking_confirm_customer', $contacts['agency']['email'], $para)) {
					EmailLog( $contacts['agency']['customer_id'], $contacts['agency']['email_id'], 'booking_confirm');
				}
			}
			else{
				
				$para['date'] = Util::toDate($booking->session_start, true, $info['talent']['timeoffset'] );
				
				if($this->sendEmail('booking_confirm_customer', $contacts['customer']['email'],$para)) {
					EmailLog( $contacts['customer']['id'], $contacts['customer']['email_id'], 'booking_confirm');
				}
			}						
		}
		
		
	}
	
	/** Talent reject booking request **/
	public function bookingReject () {
		
		$contacts = $booking->getContacts();
		$info = $booking->getInfo();
		
		$reciepient = $booking->getCustomer();
		$settings = $reciepient->getCustomerSettings()->toArray();
		
		if($booking->isTalentRecipient()){
			$talent = $contacts['talent']['name'];
		}
		else{
			$talent = $contacts['agency']['name'];
		}
			
		if($booking->isAgencyCreated()){
			
			$para['date'] = Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
			
		}
		else{
			$para['date'] = Util::toDate($booking->session_start, true, $info['customer']['timeoffset'] );
			
		}
		
		$para['talent'] = $talent;
		$para['name'] = $reciepient->firstname;		
		$para['id'] = $booking->id;
		$para['city'] = Util::makeString($info['location']['city'], $info['location']['region'], DataType::$countries[$info['location']['country']][0]);	
		$para['handle'] = $booking->isTalentRecipient() ? $info['talent']['handle'] : $info['agency']['handle'] ;
		$para['category'] = $info['package']['skill_id'];
		$para['cat_name'] = $booking->isTalentRecipient() ? $info['talent']['skill_name'] : ' agency';
			
		//$para['package'] = $info['talent']['skill_name'].' - '.$info['package']['name'].' ('.Util::ToTime( $info['package']['timespan']).')';
		$para['currency'] = $booking->isAgencyCreated() ? $info['agency']['currency'] : $info['customer']['currency'];
		
		// add notification
		$this->notify($reciepient->id, 'b', $booking->id, $talent." can not fulfull your booking request", ($booking->isTalentRecipient() ? $info['talent']['image_id'] : $info['agency']['image_id']));

		// sending email
		if(!empty($settings['alert1_email'])){
			
			if($booking->isAgencyCreated()){
				if($this->sendEmail('booking_reject', $contacts['agency']['email'], $para)) {
					EmailLog( $contacts['agency']['customer_id'], $contacts['agency']['email_id'], 'booking_reject');
				}
			}
			else{
				if($this->sendEmail('booking_reject', $contacts['customer']['email'],$para)) {
					EmailLog( $contacts['customer']['id'], $contacts['customer']['email_id'], 'booking_reject');
				}
			}						
		}
		
		// sending SMS
		if(!empty($settings['alert1_sms'])){
			
			if($booking->isAgencyCreated()){
				$msg = $talent." can not fulfull your booking request  - ".Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
			}
			else{
				$msg = $talent." can not fulfull your booking request - ".Util::toDate($booking->session_start, true, $info['customer']['timeoffset'] );
			}
			
			$msg .= ' at '.$info['location']['display'];
			
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			
			if($booking->isAgencyCreated()){
				$number = DataType::$countries[$contacts['agency']['country_code']][1].$contacts['talent']['phone'];				
				
			}
			else{
				$number = DataType::$countries[$contacts['customer']['country_code']][1].$contacts['customer']['phone'];				
				
			}
			
			$msg = substr($msg, 0,270);
				
			if($this->sendSMS($number, $msg)){
				
				if($booking->isAgencyCreated()){
					SMSLog($contacts['agency']['customer_id'], $contacts['agency']['phone_id'], 'booking_accept');
				}
				else{
					SMSLog($contacts['customer']['id'], $contacts['customer']['phone_id'], 'booking_accept');
				}				
			}			
		}
		
		
		// sending push
		$devices = $booking->isAgencyCreated() ? $contacts['agency']['devices'] : $contacts['customer']['devices'];
		
		$image = $booking->isAgencyCreated() ? $info['agency']['thumb'] : $info['customer']['thumb'];
		
		if(!empty($devices)){	
				
			foreach ($devices as $dev){
				
				$pd = array('type' => 'bookingrejected', 'id' => $booking->id, 'image' => $image);
			
				$this->sendPush($dev['push_id'], $talent.' can not fulfull your booking request', $pd);	
				
			}			
			
		}
	}
	
	/** Booking due date reminder */
	public function bookingReminder () {
		
	}
	
	/** Booking update requeest created */
	public function bookingUpdateRequest ($booking, $change) {
		
		$customer = Session::getCustomer();
		
		$contact = $booking->getContacts($customer['id']);		
		
		if(empty($contact)){
			return false;
		}
		
		$info = $booking->getInfo();
		
		// finding who sent to whom
		if(!empty($info['talent']['id']) && $info['talent']['customer_id'] == $customer['id']){
			$source = $info['talent'];			
			$target = !empty($info['agency']['id']) ? 	$info['agency'] : $info['customer'];	
		}
		elseif(!empty($info['agency']['id']) && $info['agency']['customer_id'] == $customer['id']){
			$source = $info['agency'];
			$target = !empty($info['talent']['id']) ? 	$info['talent'] : $info['customer'];
		}
		else {
			$source = $info['customer'];
			$target = !empty($info['talent']['id']) ? 	$info['talent'] : $info['agency'];	
		}
		
		$reciepient = new Customer($target['customer_id']);
		$settings = $reciepient->getCustomerSettings()->toArray();
		
		$para = array();
		$para['id'] = $booking->id;
		$para['name'] = $target['name'];
		$para['sender'] = $source['name'];
		$para['description'] = $source['name']." requested to". ($change->isCancel() ? ' cancel' :" update ".(Util::getChangeRequestDesc($change->getinfo())). " in ").' your booking.';

		// add notification
		$this->notify($target['customer_id'], 'b', $booking->id, $source['name']." has sent you a booking ".($change->isCancel() ? 'cancellation' : 'change')." request.", $source['image_id']);
		
		// sending email
		if(!empty($settings['alert1_email'])){
			
			if($this->sendEmail('booking_change_request', $contact['email'], $para)) {
				EmailLog( $target['customer_id'], $contact['email_id'], 'booking_change_req');
			}			
		}
		
		// sending sms
		if(!empty($settings['alert1_sms']) ){
			
			$msg = $source['name']." has sent you a booking ".($change->isCancel() ? 'cancellation' : 'change')." request. Plesae review changes.";
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			$msg = substr($msg, 0,270);
			
			$number = DataType::$countries[$contact['country_code']][1].$contact['phone'];
			
			if($this->sendSMS($number, $msg)){
				SMSLog($target['customer_id'], $contact['phone_id'], 'booking_change_req');
			}
		}
		
		// send push
		$image = $source['thumb'];
				
		if(!empty($contact['devices'])){
		
			foreach ($contact['devices'] as $dev){
				
				$pd = array('type' => 'bookingchangereq', 'id' => $booking->id, 'image' => $image);
			
				$this->sendPush($dev['push_id'], $source['name']." has sent you a booking ".($change->isCancel() ? 'cancellation' : 'change')." request", $pd);	
				
			}				
		}
		
	}
	
	
	/** Boooking update request accepted , Booking updated *********/
	public function bookingUpdated ($booking, $change) {
		
		$customer = Session::getCustomer();
		
		$contact = $booking->getContacts($customer['id']);
		if(empty($contact)){
			return false;
		}
		
		$info = $booking->getInfo();
		
		// finding who accepted whom's request
		if(!empty($info['talent']['id']) && $info['talent']['customer_id'] == $customer['id']){
			$target = $info['talent'];			
			$source = !empty($info['agency']['id']) ? 	$info['agency'] : $info['customer'];	
		}
		elseif(!empty($info['agency']['id']) && $info['agency']['customer_id'] == $customer['id']){
			$target = $info['agency'];
			$source = !empty($info['talent']['id']) ? $info['talent'] : $info['customer'];
		}
		else {
			$target = $info['customer'];
			$source = !empty($info['talent']['id']) ? $info['talent'] : $info['agency'];	
		}
		
		$reciepient = new Customer($source['customer_id']);
		$settings = $reciepient->getCustomerSettings()->toArray();
				
		$para = array();
		$para['id'] = $booking->id;
		$para['name'] = $source['name'];
		$para['sender'] = $target['name'];
		$para['description'] = "Your booking ".(Util::getChangeRequestDesc($change->getinfo())). " has updated as you requested.";
		
		// add notification
		$this->notify($source['customer_id'], 'b', $booking->id, $target['name']." has accepted your booking changes.", $target['image_id']);
		
		// sending email
		if(!empty($settings['alert2_email'])){
			
			if($this->sendEmail('booking_updated', $contact['email'], $para)) {
				EmailLog( $source['customer_id'], $contact['email_id'], 'booking_changed');
			}			
		}
		
		// sending sms
		if(!empty($settings['alert1_sms'])){
			
			$msg = $target['name']." has accepted  ".(Util::getChangeRequestDesc($change->getinfo())). " changes to the booking.";
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			$msg = substr($msg, 0,270);
			
			$number = DataType::$countries[$contact['country_code']][1].$contact['phone'];
			
			if($this->sendSMS($number, $msg)){
				SMSLog($source['customer_id'], $contact['phone_id'], 'booking_changed');
			}
		}
		
		// send push
		$image = $source['thumb'];
				
		if(!empty($contact['devices'])){
		
			foreach ($contact['devices'] as $dev){
				
				$pd = array('type' => 'bookingchanged', 'id' => $booking->id, 'image' => $image);
			
				$this->sendPush($dev['push_id'], $target['name']." has accepted your booking changes", $pd);	
				
			}				
		}		
	}
	
	/** Boooking update request declined *********/
	public function bookingUpdatedDeclined ($booking, $change) {
		
		$customer = Session::getCustomer();
		
		$contact = $booking->getContacts($customer['id']);
		
		if(empty($contact)){
			return false;
		}
		
		$info = $booking->getInfo();
		
		// finding who accepted to whom's request
		if(!empty($info['talent']['id']) && $info['talent']['customer_id'] == $customer['id']){
			$target = $info['talent'];			
			$source = !empty($info['agency']['id']) ? 	$info['agency'] : $info['customer'];	
		}
		elseif(!empty($info['agency']['id']) && $info['agency']['customer_id'] == $customer['id']){
			$target = $info['agency'];
			$source = !empty($info['talent']['id']) ? $info['talent'] : $info['customer'];
		}
		else {
			$target = $info['customer'];
			$source = !empty($info['talent']['id']) ? $info['talent'] : $info['agency'];	
		}
		
		$reciepient = new Customer($source['customer_id']);
		$settings = $reciepient->getCustomerSettings()->toArray();
				
		$para = array();
		$para['id'] = $booking->id;
		$para['name'] = $source['name'];
		$para['sender'] = $target['name'];
		
		
		// add notification
		$this->notify($source['customer_id'], 'b', $booking->id, $target['name']." has declined your booking changes.", $target['image_id']);
		
		// sending email
		if(!empty($settings['alert2_email'])){
			
			if($this->sendEmail('booking_change_declined', $contact['email'], $para)) {
				EmailLog( $source['customer_id'], $contact['email_id'], 'booking_change_dec');
			}			
		}
		
		// sending sms
		if(!empty($settings['alert1_sms']) ){
			
			$msg = $target['name']." has declined request changes to the booking.";
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			$msg = substr($msg, 0,270);
			
			$number = DataType::$countries[$contact['country_code']][1].$contact['phone'];
			
			if($this->sendSMS($number, $msg)){
				SMSLog($source['customer_id'], $contact['phone_id'], 'booking_change_dec');
			}
		}
		
		// send push
		$image = $source['thumb'];
				
		if(!empty($contact['devices'])){
		
			foreach ($contact['devices'] as $dev){
				
				$pd = array('type' => 'bookingchanged', 'id' => $booking->id, 'image' => $image);
			
				$this->sendPush($dev['push_id'], $target['name']." has declined your booking changes", $pd);	
				
			}				
		}		
	}
	
	
	
	/** Booking cancellation requeest approved */
	public function bookingCanceled ($booking, $change = NULL) {
		
		$customer = Session::getCustomer();
		$para = array();
		
		$contact = $booking->getContacts($customer['id']);
		
		if(empty($contact)){
			return false;
		}
		
		$info = $booking->getInfo();
		
		$reciepient = new Customer($contact['id']);
		$settings = $reciepient->getCustomerSettings()->toArray();
	
		// idntify other party
		if(!empty($info['talent']['id']) && $info['talent']['customer_id'] == $customer['id']){
			$source = $info['talent'];			
			$target = !empty($info['agency']['id']) ? 	$info['agency'] : $info['customer'];
			$para['date'] = Util::toDate($booking->session_start, true, $info['talent']['timeoffset'] );	
		}
		elseif(!empty($info['agency']['id']) && $info['agency']['customer_id'] == $customer['id']){
			$source = $info['agency'];
			$target = !empty($info['talent']['id']) ? 	$info['talent'] : $info['customer'];
			$para['date'] = Util::toDate($booking->session_start, true, $info['agency']['timeoffset'] );
		}
		else {
			$source = $info['customer'];
			$target = !empty($info['talent']['id']) ? 	$info['talent'] : $info['agency'];
			$para['date'] = Util::toDate($booking->session_start, true, $info['customer']['timeoffset'] );	
		}
		
		
		$para['id'] = $booking->id;
		$para['name'] = $target['name'];
		$para['sender'] = $source['name'];
		$para['request'] = $change;
		$para['place'] = $info['location']['display'];	
		

		// add notification
		$this->notify($target['customer_id'], 'b', $booking->id, $source['name']." has canceled your booking.", $source['image_id']);
		
		// sending email
		if(!empty($settings['alert1_email'])){
			
			if($this->sendEmail('booking_cancel', $contact['email'], $para)) {
				EmailLog( $target['customer_id'], $contact['email_id'], 'booking_cancel');
			}			
		}
		
		// sending sms
		if(!empty($settings['alert1_sms'])){
			
			$msg = $source['name']." has canceled your booking ".(!empty($change) ? 'on your request' : '').".";
			$msg .= ' ('.Util::MapURL('/dashboard/booking/'.$booking->id).')';
			$msg = substr($msg, 0,270);
			
			$number = DataType::$countries[$contact['country_code']][1].$contact['phone'];
			
			if($this->sendSMS($number, $msg)){
				SMSLog($target['customer_id'], $contact['phone_id'], 'booking_cancel');
			}
		}
		
		
		// send push
		$image = $source['thumb'];
				
		if(!empty($contact['devices'])){
		
			foreach ($contact['devices'] as $dev){
				
				$pd = array('type' => 'bookingcancel', 'id' => $booking->id, 'image' => $image);
			
				$this->sendPush($dev['push_id'],  $source['name']." has canceled your booking ".(!empty($change) ? 'on your request' : '').".", $pd);	
				
			}				
		}
		
		
		
	}
	
	
	/** Event Updates **/
	public function eventUpdated () {
		
		
		
	}
	
	/** New message recived **/
	public function newMessage ($receipient, $message) {
		//print_r($receipient);
		// sending push notifications and add to message queue
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
		
		$from = (!empty($profile)) ? $profile['name'] : $customer['name'];
		$img = (!empty($profile)) ? $profile['dp'] : $customer['dp'];
		$img_id = (!empty($profile)) ? $profile['dpid'] : $customer['dpid'];
		
		// send push
		$devices = Util::makeArray($receipient['push1'], $receipient['push2'], $receipient['push3']);
		
		$pd = array('type' => 'chat', 'id' => $message->board_id, 'image' => $img);
		
		$this->notify($receipient['customer_id'], 'c', $message->board_id, $from.' sent you a message' ,$img_id);
		
		foreach ($devices as $dev){
			
			
			
			$this->sendPush($dev, $from.' sent you a message', $pd);	
			
		}
		
		
		
		
	}
	
	/** Appointment updated : agencies **/
	public function appointmentUpdated () {
		
	}
	
	/** Subscription close to expire **/
	public function subscriptionCloseExpire () {
		
	}
	
	
	/** New subscription activated **/
	public function subscriptionAdded () {
		
	}
	
	/** Subscription renewed **/
	public function subscriptionRenewed () {
		
	}
	
	
	/** Agency request received **/
	public function agencyRequestReceived () {
		
	}
	
	/** Agency request accepted */
	public function agencyRequestResponded () {
		
	}
	
	/** Daily briefing **/
	public function dailyBrief () {
		
	}
	
	/** Talent/Agency Welcome email */
	public function welcomeEmail ($user, $email, $code) {
		
		$data = array(
				'name' => $user->firstname,
				'code' => $code
			);
		
		EmailLog( $user->id, $email->id, 'welcome');
			
		return $this->sendEmail('welcome', $email->email, $data);
		
	}
	
	public function welcomeSMS ($user, $phone, $code) {
				
		$str = $user->firstname.", Welcome to Curatalent! Your security code is \n".$code;
		
		$number = DataType::$countries[$phone->country_code][1].$phone->phone;
		
		SMSLog($user->id, $phone->id, 'welcome');
		
		return $this->sendSMS($number, $str);
		
	}
	
	
	public function pushTest ($token, $fingureprint) {
		
		$data = array(
			'type' => 'checkin',
			'signature' => $fingureprint
		);
		
		$this->sendPush($token, 'Curatalent authorization', $data);
		
	}
	
	public function otpSMS ( $user, $phone, $code){
		
		$str = $user->firstname.", Your one time pass code is: \n".$code;
		
		$number = DataType::$countries[$phone->country_code][1].$phone->phone;
		
		SMSLog($user->id, $phone->id, 'welcome');
		
		return $this->sendSMS($number, $str);
		
	}
	
	public function otpEmail ($user, $email, $code){
		
		$data = array(
				'name' => $user->firstname,
				'code' => $code,
				'email_id' => $email->id
			);
		
		EmailLog($user->id, $email->id, 'otp');
			
		return $this->sendEmail('otp', $email->email, $data);
	}
	
	
	/**
	* Send payment notification
	*/
	public function paymentEmail ($contact, $subject, $line1, $line2 ){
			
		$data = array(
				'name' => $contact['firstname'],
				'line1' => $line1,
				'line2' => $line2,
			);
		
		EmailLog($contact['id'], $contact['email_id'], 'pay'); 
		
		return $this->sendEmail ('subscription', $contact['email'], $data, $subject);
	}
	
	/**
	* Send payment notification
	*/
	public function paymentSMS ($contact, $message ){

		$number = DataType::$countries[$contact['country_code']][1].$contact['phone'];
		
		SMSLog($contact['id'], $contact['phone_id'], 'pay'); 
		
		// addling link to dashboard
		$message .= ' bit.ly/3s6nyGt';
		
		return $this->sendSMS ($number, $message);
		//return $this->sendEmail ('subscription', $contact['email'], $data, $subject);
	}
	
	
	/*************************************************************************/
	
	/**
	* Send push notification
	* token : device token
	* title : notification title
	* data: array data to be sent
	*/
	
	private function sendPush ( $token, $title, $data){
		
		//return false;
		
		// dev
		//$url = 'http://dev.curatalent.com/push.php';
		//$headers = array ();
		
		
		$url = 'https://fcm.googleapis.com/fcm/send';
		$headers = array (
            'Authorization: key=' .Config::$push_key,
            'Content-Type: application/json'
   		);

		$data['message'] = $title;
		$data = array(
			'data' => $data, 
			'to' => $token,
		);
		$data = json_encode($data);
		
		//echo $data;

		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec ( $ch );
		//$info = curl_getinfo($ch);
		curl_close($ch);
		//print_r($info);
		//echo $result;
		return json_decode($result, true);
		
	}
	
	
	/**
	* Sending an email 
	*/
	public function sendEmail ($template, $address, $data, $subject = NULL){
		
		extract($data);
		
		if(empty($subject)) {
			switch ($template){
				case 'otp' : $subject = "Your one time password"; break;
				case 'welcome' : $subject = "Welcome to Curatalent!"; break;
				case 'new_booking' : $subject = "New booking request"; break;
				case 'booking_accept' : $subject = "Your booking accepted"; break;
				case 'booking_confirm_talent' : $subject = "Booking confirmed"; break;
				case 'booking_confirm_customer' : $subject = "Booking confirmation"; break;
				case 'booking_reject' : $subject = "Booking request declined"; break;
				case 'booking_cancel' : $subject = "Your booking  has been canceled"; break;
				case 'booking_change_request': $subject = "Booking change request"; break;
				case 'booking_updated': $subject = "Booking updated"; break;
				case 'booking_change_declined': $subject = "Booking change request declined"; break;	
			}
		}
		
		$mailer = new Mailer();
		$mailer->subject = $subject;
		$mailer->to = $address;
		
		
		// add seperate template for Gmail with external images
		
		ob_start();
		
		if(strpos($address, '@gmail.com') !== false){
			include(Config::$base_path.'/templates/emails/template_gmail.php');
		}
		else{
			include(Config::$base_path.'/templates/emails/template.php');
		}
		
		$html = ob_get_clean();
				
		$mailer->html = $html;

		
		return $mailer->send();
	}
	
	/**
	* Send an SMS message
	*/
	public function sendSMS ($number, $message){
		
		//$sms = new Twillio();
		$sms = new Shoutout();
		$sms->number = "+".trim($number,'+');
		$sms->message = $message;		
	
		return $sms->send();
		
	}
	
	/**
	* Add a notification
	*/
	public function notify ($customer_id, $type, $id, $message, $image_id = NULL){
		
		if($type == 'c'){
			
			$nf = $this->db->query("select id from notifications where customer_id = {$customer_id} and type = 'c' and ref = {$id}", NULL, 1);
			
			if(!empty($nf)){
				$this->db->execute("update notifications set modified = NOW(), created = NOW(), seen = 0 where id = ".$nf['id']);
				
				return;
			}
			
		}
		
		$notification = new Notification();
		$notification->customer_id = $customer_id;
		$notification->type = Validator::cleanup($type, 1);
		$notification->ref = Validator::cleanup($id, 100);
		$notification->message = Validator::cleanup($message, 400);
		
		if(!empty($image_id)) {
			$notification->image_id = Validator::cleanup($image_id, 20);
		}
		$notification->seen = 0;
		$notification->save();
		
	}
	
	
}


?>