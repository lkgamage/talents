<?php

/**
* Timeline class to calculate apointments and bookings
*/

class Timeline {
	
	/** given time */
	public $time ;
	
	/** given date */
	public $date;
	
	/** Time stamp */
	public $stamp;	
	
	/** Time stam for the 1st slot */
	public $first;
	 
	/** store 48 hours schedule **/
	private $line; 
	
	/** 
	* defalt distance from 0th slot 
	* It consider same number of slots from and to the time
	* 0 |-------delta-------- [time] -----delta--------| 2 x delta
	**/
	private $delta ;
	
	/*** Talent's time offset in seconds **/
	private $tzoffset;
	
	/** Slot with 15 m**/
	private $slot_width = 900;
	
		
	/** simultaneous appointmnets **/
	public $simultaneous = 1;
	
	/**
	* Accept yyyy-mm-dd h:i:s format 
	*/
	function __construct ( $datetime, $offset, $days = 1) {
		
		$line = $times = array();
		
		// time offset in seconds
		$this->tzoffset = $offset*60;
		
		// 48 of 15min slots per day
		$this->delta = 96 * $days;
		
		$stamp = strtotime($datetime);
		$this->stamp = round($stamp/$this->slot_width)*$this->slot_width;
		$this->first = $this->stamp - ($this->slot_width*$this->delta);
		$this->date = date('Y-m-d', $this->stamp);	
		$this->time = date('H:i', $this->stamp);
		
		for($i = 0; $i <= $this->delta*2;  $i++){
			$this->line[$i] = 0;	
		}				
	}
	
	/**
	* Set work hours for talent
	* take talent schedule object
	*/
	public function setWorkHours ($object){
		
		$data = $object->toArray();

		$dt = date('w H:i', $this->first+$this->tzoffset);
		$dt = explode(' ', $dt);
		
		$dow = $dt[0];

		$prefix = strtolower(substr( DataType::$days[$dow],0, 3));

		
		$works = array();
		$begin  = $this->timeToMinutes($data[$prefix.'_start']);
		$end =  $this->timeToMinutes($data[$prefix.'_ends']);
		$minutes = $this->timeToMinutes($dt[1]);
		if($end == 0){
			$end = 1440;	
		}
		
		//echo "End:".$end."\n";
		
		for($i = 0; $i < $this->delta*2; $i++){
			
			
			if($data[$prefix.'_avl'] == 0){
				$this->line[$i] = 999;
			}
			elseif ($begin > $minutes || $end < $minutes){
				$this->line[$i] = 999;
			}
			
			$minutes += 5;
			// number of minutes in a day
			if($minutes >= 1440){
				$minutes = 0;				
				$dow++;
				
				if($dow > 6){
					$dow = 0;
				}
				
				$prefix = strtolower(substr( DataType::$days[$dow],0, 3));
				$begin  = $this->timeToMinutes($data[$prefix.'_start']);
				$end =  $this->timeToMinutes($data[$prefix.'_ends']);
				
				if($end == 0){
					$end = 1440;	
				}
				//echo "Begin:".$begin."  End:".$end."\n";
			}
						
			
		}		
	}
	
	/**
	* Get date time range in the time line
	*/
	public function getRange () {
	
		return array(
			date("Y-m-d H:i", $this->first),
			date("Y-m-d H:i", $this->first+(86400*2)),
		);
		
	}
	
	
	/**
	* Add an appointment 
	*/
	public function addToLine ($time, $duration){

		$slots = $this->timeToSlot($time, $duration);

		for($i = $slots[0]; $i <= $slots[1]; $i++){
			$this->line[$i]++;	
		}
	}
	
	/**
	* Remove from time line
	*/
	public function removeFromLine ($time, $duration){
		
		
		$slots = $this->timeToSlot($time, $duration);

		for($i = $slots[0]; $i <= $slots[1]; $i++){
			
			if($this->line[$i] > 0){
				$this->line[$i]--;
			}
		}
		
	}
	
	/**
	* Add full day booking
	*/
	public function addFullDay ($date, $timezone){
		
		$dt = explode(' ',$date);
		$begin = strtotime($dt[0].' 00:00')+$timezone*60;
		
		$this->addToLine(date('Y-m-d H:i', $begin), 1440);
		
	}
	
	
	/**
	* Check if given time and and duration overlap with the schedule
	* items is the number of max possible simultanius appointmnts
	*/
	public function isOverlap ( $duration, $time = NULL) {
		
		if(!empty($time)) {
			$slots = $this->timeToSlot($time, $duration);
		}
		else{
			$slots = array(
				$this->delta, 
				$this->delta+ceil($duration/($this->slot_width/60)) -1
			);
			
		}
		
		if($slots[1] > count($this->line) - 1){
			$slots[1] = count($this->line) - 1;	
		}

		$max = 0;
		for($i = $slots[0]; $i <= $slots[1]; $i++){
			
			if($max < $this->line[$i]){
				$max = $this->line[$i];
			}
		}
		
		return $max >= $this->simultaneous;
	}
	
	/**
	* Scan for a  given duration
	* return date from and date to 
	*/
	public function scanFor ($duration){
				
		$slots = ceil($duration/($this->slot_width/60)) -1;	
		
		$free = array();
		$index = $this->delta;
		
		// scan forward 
		for($i = $this->delta; $i <= 2*$this->delta; $i++){
			
			if($this->line[$i] < $this->simultaneous){
				
				if(!isset($free[$index])) {
					$free[$index] = 1;
				}
				else{
					$free[$index]++;
				}
			}
			else{
				$index = $i+1;	
			}			
		}
		// scan backword
		for($i = $this->delta; $i >= 0; $i--){
			
			if($this->line[$i] < $this->simultaneous){
				
				if(!isset($free[$index])) {
					$free[$index] = 1;
				}
				else{
					$free[$index]++;
				}
			}
			else{
				$index = $i-1;	
			}			
		}
		
		$times = array();
		
		foreach ($free as $slot => $num){
			
			if($num >= $slots){
			
				if($slot < $this->delta){
					// before times
					$times[] = array($slot-$slots, $slot );
				}
				else{
					// after
					$times[] = array($slot, $slot+$slots);	
				}
			
			}
			
		}
		
		
	//	print_r($times);
		
		return $times;
	}
	
	
	/**
	* get begin and end slots for given time and duration
	*/
	private function timeToSlot ($time, $duration){
		
		$slots = ceil($duration/($this->slot_width/60)) -1;	

		$begin_slot = floor((strtotime($time) - $this->first)/($this->slot_width));
		$end_slot = $begin_slot+$slots;
		
		// out of range
		if( $end_slot < 0 || $begin_slot > 2*$this->delta){
			return false;
		}
		
		if($begin_slot < 0){
			$begin_slot = 0;	
		}
		
		if($end_slot > 2*$this->delta){
			$end_slot = 2*$this->delta;	
		}
		
		return array($begin_slot, $end_slot);
	}
	
	/**
	* Convert given time to number of minutes from 00:00
	*/
	public function timeToMinutes ($time){
		
		list($h, $m) = explode(':', $time);
		return ($h*60)+$m;
		
		
	}
	
	
	/**
	* Get a time at a given slot
	* index (+/-) from given time slot
	*/
	public function timeAtSlot ($index) {
		
		$newtime = $this->first + ($this->slot_width*$index);
		
		return array(
			date('Y-m-d', $newtime),
			date('H:i', $newtime)
		);
		
	}
	
	private function roundTo ($n){
		
		$x = $this->slot_width;
		
		return ($n%$x == 0) ? $n : round(($n+$x/2)/$x)*$x;	
	}
	
	
	/**
	* Get schedule
	*/
	public function getSchedule (){
	
		$data = array();
		
		foreach ($this->line as $i => $jobs){
			$t =  $this->timeAtSlot($i);
			// convert to end users time
			$data[$i] = Util::ToDate($t[0].' '.$t[1], 1);
			$data[$i][2] = $jobs;	
		}
		
		return $data;
	}
	
	
	function haversineGreatCircleDistance(
  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
	{
	  // convert from degrees to radians
	  $latFrom = deg2rad($latitudeFrom);
	  $lonFrom = deg2rad($longitudeFrom);
	  $latTo = deg2rad($latitudeTo);
	  $lonTo = deg2rad($longitudeTo);
	
	  $latDelta = $latTo - $latFrom;
	  $lonDelta = $lonTo - $lonFrom;
	
	  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
	  return $angle * $earthRadius;
	}
	
	
}


?>