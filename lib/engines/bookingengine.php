<?php
/**
* Booking task controlling engine
*/

class BookingEngine extends Engine {
	
	
	/** customer */
	public $customer_id = NULL;
	
	/** agency id (booking) */
	public $agency_id = NULL;
	
	/** Event id **/
	public $event_id = NULL;
	
	/*** Existing booking ids to make changes **/
	public $booking = NULL;
	
	
	/** cache date **/
	private $date;
	
	/** cache time **/
	private $time;
	
	/** cache skills **/
	private $skills;
	
	
	public $timeline;
	
	/** error message **/
	public $error;
	
		
	/**
	* Talant/Agency data (AttamptTalent)
	*/
	public $object;
	
	/** attampt object
	*/
	public $attempt;
	
	/** Store data regarding paid subscription */
	public $subscription = NULL;
	
	
	public $data;
	
	function __construct ( ) {
		
		parent::__construct();
		
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
		
		if(!empty($customer)){
			$this->customer_id = $customer['id'];	
		}
		
		if(!empty($profile) && $profile['type'] == 'a'){
			$this->agency_id = $profile['id'];	
		}		
		
		// read cookie and 
		$cookie = Util::getCookie(DataType::$COOKIE_BOOKING);

		if(!empty($cookie)){

			//  attempt id|object_id
			$cookie = trim($cookie);
			list($id,$object_id) = explode('|', $cookie);	
			
			if(!empty($object_id)){
				$this->read($object_id);
			}
						
		}
		
		applog("Booking engine started, Customer:{$this->customer_id}, Agency:{$this->agency_id}, Cookie:{$cookie}");
		
		/*
		$event_id = Session::getEvent();
		
		if(!empty($event_id)){
			$this->event_id = $event_id;	
		}
		*/
	}
	
	/**
	* Get date
	*/
	public function getDate ( $formatted = false){
	
		if(!empty($this->attempt) && !$this->attempt->isEmpty('booking_date')){
			
			if($formatted){
				
				if(!empty($this->date)){
					return $this->date;
				}
				
				$dt = Util::ToDate($this->attempt->booking_date, 1);
				$this->date = $dt[0];
			
				return $dt[0];	
			}
			else{
				return $this->attempt->booking_date;	
			}
		}
		
		return '';		
	}
	
	/**
	* Get time
	*/
	public function getTime ( $formatted = false){
		
		if($this->object->isEmpty('begin_time')){
			return '';
		}
	
		if($formatted){
			
			if(!empty($this->time)){
				
				return trim($this->time,'0');
			}
			
			$dt =  Util::ToDate($this->attempt->booking_date.' '. $this->object->begin_time);
			
			$this->date = $dt[0];
			$this->time = $dt[1];

			return $dt[1];	
		}
		else{
			return $this->attempt->booking_date;	
		}
			
		
		return '';	
	}
	
	/**
	* Get place
	*/
	public function getPlace ($formatted = false){
	
		if(!empty($this->data['location']['id'])){
			
			if($formatted){
				$address = explode(', ',$this->data['location']['display']);
				
				$txt = array_shift($address)."<br>";
				
				if($this->data['location']['country'] == Session::getCountry()){
					array_pop($address);
					$txt .= implode(', ', $address);
				}
				else{
					$cn = array_pop($address);
					$txt .= implode(', ', $address)."<br>".$cn;
				}
				
				return $txt;
			}
			else {
				return $this->data['location']['display'];	
			}
		}
		
		return '';
	}
	
	/**
	* Get place
	*/
	public function getPlaceID (){
	
		if(!empty($this->data['location']['id'])){
			return $this->data['location']['place_id'];	
		}
		
		return '';
	}
	
	/**
	* Check whether if engine can choose package for given date/time
	* date, time, place and agency/talent should be selected
	*/
	public function canSelectPackage () {
		
		if(empty($this->object)){
			$this->error = "Talent profile not selected";
			return false;	
		}
		
		// capturing data from event
		if(!$this->attempt->isEmpty('event_id')){
			applog("Make boking for an event {$this->attempt->event_id}");
			return true;	
		}
		
		if(!empty($this->date) && !empty($this->time) && !empty($this->data['location']['id'])){
			
			$datetime = Util::toSysDate($this->date, $this->time);
			
			if(empty($datetime)){
				$this->error = "Invalid date/time";
				return false;				
			}
			//echo $datetime.' '.$datetime;
			if(strtotime($datetime) < (7200) + time()){
				$this->error = "Booking should be made at least two hours before the expected time.<br><br>Please try different time";	
				return false;
			}
			
			
			// finally
			return true;	
		}
		
		Util::debug($this);
		
		$this->error = "Date/Time or Loctaion is not specified";
		
		return false;
		
	}
	
	/**
	* Check if all nesasaray info available to make booking
	*/
	public function canMakeBooking () {
		
		return $this->canSelectPackage() && !empty($this->data['package']['id']);
		
	}
	
	/**
	* Get formatted object address
	*/
	public function getObjectAddress () {
		
		$data = [];
		$data[] = $this->data['object_location']['city'];
		$data[] = $this->data['object_location']['region'];	
		
		if(Session::getCountry() == $this->data['object_location']['country']){
			return ucwords(implode(", ", $data));
		}
		
		return ucwords(implode(", ", $data)."<br/>".DataType::$countries[$this->data['object_location']['country']][0]);
		
	}
	
	/** 
	* Set object into search engine
	* object should be talent or agency
	*/
	public function setObject ($object){
		
		if($object->isDeleted() || $object->active != 1){
			$this->error = "Invalid object";
			return false;	
		}
		
		$page = $object->getPage();
		if($page->active != 1 || $page->isDeleted()){
			$this->error = "Object is not active";
			return false;	
		}
		
		if(empty($this->attempt)){
			$this->create();	
		}
		
		
		if(empty($this->object) || $this->object->profile_id != $object->id){
				$this->createObject($object->id);
				$this->data['profile'] = $object->toArray();
		}			
		
		
		
		return true	;	
	}
	
	
	/**
	* Get object (profile)
	*/
	public function getObject () {

		if(!empty($this->data)){
			
			if(!empty($this->data['profile']['id'])){
				
				$object = new Profile();
				$object->populate($this->data['profile']);
				return $object;	
			}
			
		}
		
		return false;
		
	}
	
	
	/**
	* Get last visited active event
	*/
	public function getActiveEvent () {
		
		$event_id = Session::getEvent();
		
		if(!empty($event_id)){
			
			if(!empty($this->data['event']['id']) && $event_id == $this->data['event']['id']){
				return $this->data['event'];
			}
			else{
				
				$event =  $this->db->query("SELECT e.*, l.place_id, l.venue, l.address, l.city, l.region, l.postalcode, l.country, a.name AS agency_name, DATE(e.begins) as event_date 
FROM `events` e
JOIN locations l ON (l.id = e.location_id)
LEFT JOIN profiles a ON (a.id = e.agency_id) WHERE e.id = ".$event_id, NULL, 1);

				if(empty($this->data['event']['id'])){
					$this->data['event'] = 	$event;				
				}
				
				return $event;
			}
			
		}		
		
	}
	
	/**
	* Update existing booking
	*/
	public function setBooking ($booking) {
		
		$this->booking = $booking;
		
		$date = Util::ToDate($this->booking->session_start, 1);
		$this->date = $date[0];
		$this->time = $date[1];
		
		if($booking->isTalentRecipient()){
			$object = $booking->getTalent();	
		}
		else{
			$object = $booking->getAgency();	
		}
		
		if(!$this->setObject($object)){
			return false;
		}

		$this->capture();
		
		if(empty($_POST['place_id'])) {
			$this->attempt->location_id = $booking->location_id;
			$this->attempt->save();
		}
		
		$this->read( $this->object->id);
	
		return true;			
	}
	
	
	/**
	* get related event from this booking attempt
	*/
	public function getEvent () {
		
		if(!empty($this->attempt) && !$this->attempt->isEmpty('event_id')){
			
			
			if(!empty($this->data['event']['id']) && $this->attempt->event_id == $this->data['event']['id']){
				return $this->data['event'];
			}
			else{			
				
				$event =  $this->db->query("SELECT e.*, l.place_id, l.venue, l.address, l.city, l.region, l.postalcode, l.country, a.name AS agency_name, DATE(e.begins) as event_date 
FROM `events` e
JOIN locations l ON (l.id = e.location_id)
LEFT JOIN profiles a ON (a.id = e.agency_id) WHERE e.id = ".$this->attempt->event_id, NULL, 1);
				$this->data['event'] = 	$event;	
				return $event;
			}
				
		}
	}
	
	/**
	* Clear attached or active event
	*/
	public function clearEvent () {
		
		if(!empty($this->attempt) && !$this->attempt->isEmpty('event_id')){
		
			$this->attempt->setNull('event_id');
			$this->attempt->setNull('location_id');
			
		}
		
		Session::clearEvent();
		
		unset($this->data['event']);
		
	}
	
	/**
	* Set event for current booking
	*/
	public function setEvent ($event){
		
		$customer = Session::getCustomer();
		
		if($event->ready() && !empty($customer) && $event->customer_id == $customer['id']){
				
				if(strtotime($event->begins) < time() +  (3600*4)){
					return false;
				}
				
				$this->data['event'] = $event->toArray();
				$dt = explode(' ', $event->begins);
				
				$this->date = $dt[0];
				$this->time = $dt[1];
				$this->event_id = $event->id;
				
				if(!empty($this->attempt)){
					
					$this->attempt->event_id = $event->id;
					$this->attempt->location_id = $event->location_id;
					$this->attempt->booking_date = $event->begins;
					$this->attempt->update();	
				}
				
				if(!empty($this->object)){
					$this->object->begin_time = $event->begins;
				}
				
				
				Session::setEvent($event->id);
				return true;
			
		}
		
		
		return false;
	}
	
	/**
	* Set a package
	*/
	public function setPackage ($id) {
		
		if(empty($this->time)){
			$this->error = "Please select a date and time";
			return false;	
		}
		
		$package = new talentPackage($id);
		
		if($package->active == 0 || $package->is_private == 1){
			$this->error = "This package is not available";
			return false;
		}
		
		if($package->talent_id != $this->object->talent_id && $package->agency_id != $this->object->agency_id){
			$this->error = "Package doesn't belong to the selected talent";
			return false;
		}
		
		
		$object = $this->getObject();
		$timeline = $object->getTimeline($this->date.' '.$this->time);
		
		if(!empty($this->booking)){
			// remove appointment for this booking
			// then chcek if booking is possible
			$appointment = $this->booking->getAppoinment();
			
			if(!empty($appointment) && $appointment->confirmed == 1){
				$timeline->removeFromLine($appointment->begins, $appointment->duration+$appointment->rest_time);
			}
			
		}


		if(!$timeline->isOverlap($package->timespan)){
			$this->object->package_id = $package->id;
			$this->object->save();
		}
		else{
			$this->error = "Sorry! This package is not available on ".$this->date.' '.$this->time;
			return false;	
		}
		
		$this->read( $this->object->id);
		
		return true;
	}
	
	
	/**
	* Get packages for selected time
	*/
	public function checkAvailability (){
				
		if(empty($this->object)){
			$this->error = "Please select talent or agency";
			return false;	
		}
		
		if(empty($this->date)){
			$this->error = "Please select the date";
			return false;	
		}
		
		if(empty($this->time)){
			$this->error = "Please select a time";
			return false;	
		}

		if($this->attempt->isEmpty('event_id') && empty($this->data['location']['id'])){
			$this->error = "Please select location";
			return false;	
		}
		
		$object = $this->getObject();
		
		if(empty($object) || !$object->ready()){
			$this->error = "Invalid talent";
			return false;
		}
		
		$packages = $object->getPackages(true);
			
		if($object->isTalent() || !$object->hasPaidSubscription()){
			
			// get active packages for the talent
			if(empty($packages)){
				$this->error = "No available packages";	
				return false;
			}
			
			$this->timeline = $object->getTimeline($this->date.' '.$this->time);
			
			$available = 0;
			
			foreach ($packages as $i => $pack){
				
				if($this->timeline->isOverlap($pack['timespan'])){
					$packages[$i]['available'] = false;
					$packages[$i]['alt'] = $this->timeline->scanFor($pack['timespan']);
					
				}
				else{
					$packages[$i]['available'] = true;	
					$available++;
				}				
			}
			
			return array('available' => $available, 'packages' => $packages );
		
		// talent end	
		}
		else {
			// agency
			
			$packages = $object->getPackages(true);
			return array('avilable' => $available, 'packages' => $packages );
		
			// agency ends	
		}
		
	}
	
	
	/**
	* get view data
	* useful to display on side view of the booking page
	*/
	public function getViewData () {	

		
		if(!empty($this->data['page']['id'])){
			
			$data = array(
					'handle' => $this->data['page']['handle'],
					'title' => $this->data['page']['title'],
					'image' => $this->data['page']['image'],
				    'packages' => $this->data['profile']['packages'],
					'min_fee' => $this->data['profile']['min_fee'],
					'max_fee' => $this->data['profile']['max_fee'],
					'body' => $this->data['page']['body'],
					'image' => $this->data['page']['image']
					
				);
			
			if(!empty($this->data['profile']['id'])){
				
				$object = new Profile();
				$object->populate($this->data['profile']);
			}
			
			
			$data['name'] = $object->name;
			$data['skills'] = $object->getSkillString();
			$data['city'] = $this->data['object_location']['city'];
			$data['region'] = $this->data['object_location']['region'];	
			$data['country'] = $this->data['object_location']['country'];			
			
			return $data;
			
		}
		
		
		return !empty($data) ? $data : false;
	}
	
	/**
	* set object by username/handle
	*/
	public function setHandle ($handle){
		
		 $handle = Validator::cleanup(trim($handle), 40);
		 $handle = $this->db->escape($handle);	
		
		applog("Setting handle {$handle}");
		 
		 if(empty($this->attempt)){
			$this->create();	
		 }
		 /*
		 $data = $this->db->query("SELECT p.id AS page__id, 
			p.customer_id AS page__customer_id, 
			p.handle AS page__handle, 
			p.banner_id AS page__banner_id, 
			p.description AS page__description, 
			p.active AS page__active, 
			p.handle_updated AS page__handle_updated, 
			p.updated AS page__updated, 
			p.published AS page__published, 
			p.created AS page__created, 
			p.modified AS page__modified, 
			p.deleted AS page__deleted,
			a.id AS profile__id, 
			a.customer_id AS profile__customer_id, 
			a.name AS profile__name, 
			a.agency_type AS profile__agency_type, 
			a.verified AS profile__verified, 
			a.is_agency AS profile__is_agency,
			a.location_id AS profile__location_id, 
			a.timezone AS profile__timezone, 
			a.timeoffset AS profile__timeoffset, 
			a.image_id AS profile__image_id, 
			a.active AS profile__active, 
			a.name_changed AS profile__name_changed, 
			a.currency AS profile__currency, 
			a.created AS profile__created, 
			a.modified AS profile__modified,
			a.currency AS profile__currency,  
			a.deleted AS profile__deleted,			
			a.limited_hours AS profile__limited_hours,
			l.id AS location__id, 
			l.place_id AS location__place_id,
			l.display as location__display,
			l.venue AS location__venue, 
			l.address AS location__address, 
			l.city AS location__city, 
			l.region AS location__region, 
			l.postalcode AS location__postalcode, 
			l.country AS location__country, 
			pc.id AS page__id,  
			pc.title AS page__title, 
			i.url AS page__image, 
			pc.body AS page__body 
			FROM pages p 
			LEFT JOIN profiles a ON ( a.id = p.profile_id)
			LEFT JOIN locations l ON ( l.id = a.location_id)
			LEFT JOIN page_contents pc ON ( pc.page_id = p.id AND section = 'a')
			LEFT JOIN images i ON (i.id = pc.image_id)
			WHERE p.handle = '".$handle."' and isnull(p.deleted)	", NULL, 1);
		
		// handle not found	
		if(empty($data['page__id'])){
			return false;
		}

		// format
		$out = array();
		foreach ($data as $k => $v){
			
			list($t, $f) = explode('__',$k);
			if(!isset($out[$t])){
				$out[$t] = array();
			}
			
			$out[$t][$f] = $v;	
		}
		
		if(empty($this->data)){
			$this->data = [];		
		}
		
		// merge 
		$this->data = array_merge($this->data, $out);

		$this->normalize();
		*/
		
		
		$page = App::findPage($handle);
		
		if(!empty($page) && $page->active == 1 && !$page->isDeleted()){
			
			if(empty($this->object) || $this->object->profile_id != $this->data['profile']['id']){
				$this->createObject($this->data['profile']['id']);
				applog("New booking talent object created {$this->object->id}");
				
			}
			else{
				applog("Procced with existing attampt record {$this->object->id}");
			}
		}
		else{
			applog("Trying to make a booking for disabled page {$handle}");
			$this->error = "This talent is not available";
			return false;
		}
		
		// create talent attmpt object
		if(empty($this->object) || $this->object->profile_id != $page->profile_id){
			$this->createObject($page->profile_id);				
		}			
		
		$this->read($this->object->id);
		
		applog("Booking Handle asociated with ".$this->data['profile']['id']);
		
		return true;
		
	}
	
	
	/** capture post data and update accordingly **/
	public function capture () {
		
		$attempt_changed = false;
		$object_changed = false;
		
		// if object is empty, creating new 
		if(empty($this->object)){

			$obj = new AttemptTalent();
			$obj->attempt_id = $this->attempt->id;
			$obj->save();
			
			$this->object = $obj;

		}
		
		// place
		if (!empty($_POST['place_id']) &&  !empty($_POST['country']) ){
		
			if(empty($this->attempt) || $this->data['location']['place_id'] != $_POST['place_id']){
				
				// Since location changed, create new attempt record
				if(empty($this->attempt)){
					$this->create();
				}
				// for pending bookings
				if(!empty($this->booking) && $this->booking->status == DataType::$BOOKING_PENDING) {
					$loc = $this->booking->getLocation();
					$loc = Util::saveLocation($loc);
				}
				else{
					$loc = Util::saveLocation();
				}

				$this->attempt->location_id = $loc->id;
				
				// update cache
				$this->data['location'] = $loc->toArray();
								
				$attempt_changed = true;
			}			
		}
		
		// date
		if (!empty($_POST['date']) && !empty($_POST['time'])){
			
			$date = Util::ToSysDate($_POST['date'], $_POST['time']);
						
			if(!empty($date) && strtotime($date) > time()){
				
				$this->date = $_POST['date'];
				$this->time = $_POST['time'];
		
				if(empty($this->attempt) || $this->attempt->booking_date != $date){
					
					if(empty($this->attempt)){
						// create new or duplicate					
						$this->create();
					}
					
					$this->attempt->booking_date = $date;
					
					$this->object->begin_time = $date;

					$object_changed = true;
					$attempt_changed = true;
					
				}
				
				
				
				if($this->object->begin_time != $date){	
				
					// when the time get chnaged, package will removed
					
					if($this->object->begin_time != $date && !$this->object->isEmpty('package_id')){
						
						$this->object->setNull('package_id');
					}
					
					$this->attempt->begin_time = $date;
					$this->object->begin_time = $date;

					$object_changed = true;
					$attempt_changed = true;					
				}							
			}			
		}
				
		// talent
		if (!empty($_POST['profile_id']) && is_numeric($_POST['profile_id'])){
			
			if(empty($this->object) || $this->object->profile_id != $_POST['profile_id']){
				$this->createObject($_POST['profile_id']);	
			}			
		}
		
		
		
		
		if($attempt_changed){
			$this->attempt->save();	
		}
		
		if($object_changed){
			$this->object->save();	
		}				
	}
	
	
	/** Create or duplicate attempt record **/
	private function create () {
		
		if(empty($this->attempt)){
			
			$customer = Session::getCustomer();
			$profile = Session::getProfile();
			
		
			$attempt = new BookingAttempt();
			
			if(!empty($this->customer_id)) {
				$attempt->customer_id = $this->customer_id;
			}
			
			
			if(!empty($this->agency_id)) {
				$attempt->agency_id = $this->agency_id;
			}
			
			//$attempt->location_id = Validator::cleanup($_POST['attempt_location_id'], 100);
			
			if(!empty($this->event_id)){
				$attempt->event_id = $this->event_id;	
			}
			
			$attempt->save();
			//$this->read($attempt->id);
			$this->attempt = $attempt;
			applog("New booking attampt created {$this->attempt->id}");
			
		}
		else{
			
			$this->attempt->duplicate();
			
		}			
	}
	
	
	/**
	* Create object that is going to book 
	*/

	private function createObject ($profile_id ) {
		
				
		$obj = new AttemptTalent();
		$obj->attempt_id = $this->attempt->id;
		
		if(!empty($profile_id)) {
			$obj->profile_id = $profile_id;
		}		
		
		$obj->save();
		$this->object = $obj;
		
	}
	
	
	
	/**
	* Push updated data into cookie
	*/
	public function push () {
		
		
		// attempt id|object_id
		if(empty($this->attempt) || !$this->attempt->ready()){
			return false;	
		}
		
		$object_id = '';
		
		if(!empty($this->object) && $this->object->ready()){
			$object_id = $this->object->id;	
		}
		
		
		Util::setCookie(DataType::$COOKIE_BOOKING, $this->attempt->id.'|'.$object_id, 7);
		
	}
	
	public function update() {
		$this->push();
	}
	
	
	/**
	* Read all nesassary data at once
	*/
	public function read ($object_id) {
	
		$data = $this->db->query("SELECT ba.id AS attempt__id, 
		ba.customer_id AS attempt__customer_id, 
		ba.agency_id AS attempt__agency_id, 
		ba.location_id AS attempt__location_id, 
		ba.event_id AS attempt__event_id,		
		ba.booking_date AS attempt__booking_date, 
		ba.created AS attempt__created, 
		ba.modified AS attempt__modified, 
		ba.deleted AS attempt__deleted,
		bt.id AS object__id, 
		bt.attempt_id AS object__attempt_id, 
		bt.profile_id AS object__profile_id, 
		bt.begin_time AS object__begin_time, 
		bt.package_id AS object__package_id,
		bt.booking_id AS object__booking_id,  
		bt.created AS object__created, 
		bt.modified AS object__modified, 
		bt.deleted AS object__deleted,
		f.id AS profile__id, 
		f.customer_id AS profile__customer_id, 
		f.name AS profile__name, 
		f.agency_type AS profile__agency_type, 
		f.verified AS profile__verified, 
		f.location_id AS profile__location_id, 
		f.timezone AS profile__timezone, 
		f.timeoffset AS profile__timeoffset,		
		f.image_id AS profile__image_id, 
		f.active AS profile__active, 
		f.currency AS profile__currency, 
		f.deleted AS profile__deleted,
		f.gender AS profile__gender, 
		f.limited_hours AS profile__limited_hours, 
		f.active AS profile__active, 
		l.id AS location__id, 
		l.place_id AS location__place_id, 
		l.display AS location__display, 
		l.venue AS location__venue, 
		l.address AS location__address, 
		l.city AS location__city, 
		l.region AS location__region, 
		l.postalcode AS location__postalcode, 
		l.country AS location__country,	
		el.place_id AS event__place_id, 
		el.venue AS event__venue, 
		el.address AS event__address, 
		el.city AS event__city, 
		el.region AS event__region, 
		el.postalcode AS event__postalcode, 
		el.country AS event__country,		
		ol.id AS object_location__id, 
		ol.place_id AS object_location__place_id, 
		ol.venue AS object_location__venue, 
		ol.address AS object_location__address, 
		ol.city AS object_location__city, 
		ol.region AS object_location__region, 
		ol.postalcode AS object_location__postalcode, 
		ol.country AS object_location__country, 
		tp.id AS package__id, 
		tp.active AS package__active, 
		tp.name AS package__name,
		tp.category_id AS package__category_id, 
		tp.description AS package__description, 
		tp.items AS package__items, 
		tp.timespan AS package__timespan, 
		tp.interval AS package__interval, 
		tp.location_required AS package__location_required, 
		tp.fee AS package__fee, 
		tp.advance_percentage AS package__advance_percentage, 
		tp.is_private AS package__is_private, 
		tp.fulday AS package__fulday, 
		tp.created AS package__created, 
		tp.modified AS package__modified, 
		tp.deleted AS package__deleted,		
		tps.category AS package__category, 
		tps.name AS package__skill_name, 
		tps.genre AS package__genre, 
		tps.is_business AS package__is_business, 
		tps.is_agency AS package__is_agency,	
		e.id AS event__id, 
		e.customer_id AS event__customer_id, 
		e.agency_id AS event__agency_id, 
		e.event_type AS event__event_type, 
		e.name AS event__name, 
		e.description AS event__description, 
		e.begins AS event__begins, 
		e.ends AS event__ends, 
		e.charitable AS event__charitable, 
		e.location_id AS event__location_id, 
		e.num_attendees AS event__num_attendees, 
		e.budget AS event__budget, 
		e.created AS event__created, 
		e.modified AS event__modified, 
		e.deleted AS event__deleted,
		ea.name AS event__agency_name,
		DATE(e.begins) AS event__event_date,
		p.id AS page__id, 
		p.customer_id AS page__customer_id, 
		p.profile_id AS page__profile_id, 
		p.handle AS page__handle, 
		p.active AS page__active, 
		p.updated AS page__updated, 
		p.published AS page__published, 
		p.created AS page__created, 
		p.modified AS page__modified, 
		p.deleted AS page__deleted, 
		pc.title AS page__title, 
		i.url AS page__image,
		fp.packages as profile__packages,
		fp.min_fee as profile__min_fee,
		fp.max_fee as profile__max_fee,
		pc.body AS page__body 
		FROM attempt_talents bt		 
		LEFT JOIN booking_attempts ba ON ( bt.attempt_id = ba.id)
		LEFT JOIN talent_packages tp ON (tp.id = bt.package_id)
		LEFT JOIN categories c ON (c.id = tp.category_id)
		LEFT JOIN skills tps ON (tps.id = c.skill_id)
		LEFT JOIN profiles f ON ( f.id = bt.profile_id)
		LEFT JOIN locations l ON ( l.id = ba.location_id)
		LEFT JOIN locations ol ON ( ol.id = f.location_id)
		LEFT JOIN `events` e ON ( e.id = ba.event_id)
		LEFT JOIN locations el ON ( el.id = e.location_id)
		LEFT JOIN profiles ea ON (ea.id = e.agency_id)
		LEFT JOIN pages p ON ( p.profile_id = bt.profile_id)
		LEFT JOIN page_contents pc ON ( pc.page_id = p.id AND section = 'a')
		LEFT JOIN images i ON (i.id = pc.image_id)
		left join profile_packages fp on (fp.profile_id = f.id)
		Where bt.id = ".$object_id, NULL, 1);
		
		
		if(!empty($data)){
			
			$out = array();
			foreach ($data as $k => $v){
				
				list($t, $f) = explode('__',$k);
				if(!isset($out[$t])){
					$out[$t] = array();
				}
				
				$out[$t][$f] = $v;	
			}
			
			$this->data = $out;
			$this->normalize();
		}
		else{
			$this->create();	
		}
		
	}
	
	
	
	private function normalize () {
	
		if(!empty($this->data['attempt']) || !empty($this->data['attempt']['id'])){
			$this->attempt = new BookingAttempt();
			$this->attempt->populate($this->data['attempt']);	
			
			unset($this->data['attempt']);		
		}
		
		if(!empty($this->data['object']) || !empty($this->data['object']['id'])){
			$this->object = new AttemptTalent();
			$this->object->populate($this->data['object']);
			
			unset($this->data['object']);			
		}
				
		if(!empty($this->attempt) && !$this->attempt->isEmpty('booking_date')){
			
			
			$dt =  Util::ToDate($this->attempt->booking_date, 1);
			$this->date = $dt[0];
			$this->time = $dt[1];		
			
		}		
		
	}
	
	/**
	* Validate booking data to see if all nesassary requirements are met
	*/
	
	public function validate (){
		
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
		
		// pre check
		if(!$this->canMakeBooking()){
			$this->error = "Information missing";	
			return false;
		}
		
		// validate each element
		// date time
		
		$dt = strtotime($this->date.' '.$this->time) ;

		if(empty($dt) || time() + 7200 > $dt ){
			$this->error = "Booking should be made at least two hours before the expected time.<br><br>Please try different time";	
			return false;
		}
		
		// validate object
		$object = $this->getObject();

		if($object->isDeleted() || $object->active != 1){
			$this->error = "Invalid object";
			return false;	
		}
		
		// page
		if(empty($this->data['page']['id']) || empty($this->data['package']['id'])){
			$this->error = "Invalid association";
			return false;
		}
		
		$data = $this->db->query("SELECT p.id AS page_id, p.active AS page_active, p.published AS page_published, p.deleted AS page_deleted, s.id AS sub_id, s.active AS sub_active, 
s.jobs AS sub_jobs, s.deleted AS sub_deleted, s.exp_date > NOW() AS sub_expired, f.active AS profile_active, f.deleted AS profile_deleted, 
(SELECT COUNT(*) FROM bookings WHERE subscription_id = s.id ) AS consumed
FROM categories c
JOIN profiles f ON (f.id = c.profile_id)
JOIN pages p ON (p.profile_id = f.id)
JOIN subscriptions s ON (s.category_id = c.id AND begin_date <= NOW() AND exp_date > NOW() AND s.active = 1 AND ISNULL(s.deleted))
WHERE c.id = {$this->data['package']['category_id']}", NULL, 1);
		//HAVING consumed < s.jobs


		if(empty($data)){
			$this->error = "No subscription";
			return false;
		}
		
		// for later use in com engine
		$this->subscription = $data;
				
		if(empty($data['page_active']) || empty($data['page_published']) || !empty($data['page_deleted'])){
			$this->error = "Object is not active";
			return false;	
		}
				
		if(empty($data['sub_active']) || empty($data['sub_expired']) || !empty($data['sub_deleted'])){
			$this->error = "No active subscription";
			return false;	
		}
		
		if(empty($this->data['package']['active']) || !empty($this->data['package']['deleted'])){
			$this->error = "Invalid package";
			return false;
		}
		
		if(!empty($data['event']['id'])){
			
			if(strtotime($this->data['event']['begins']) < time()){
				$this->error = "past event";
				return false;	
			}
			
			if(!empty($this->data['event']['deleted'])){
				$this->error = "Invalid event";
				return false;	
			}
			
			if($this->data['event']['customer_id'] != $customer['id']) {
				$this->error = "Aunathorized event";
				return false;
			}			
		}
		
				
		if(empty($this->data['location']['id']) || empty($this->data['location']['place_id'])){
			$this->error = "Invalid location";
			return false;	
		}
		
		return true;
		
	}
	
	/**
	* Submit boooking into the DB
	* attach object will be released
	*/
	public function submit () {
		
		// througly validate
		if(!$this->validate()){
			return false;	
		}
		
	//	print_r($this->data);
	//	print_r($this->object);
	//	print_r($this->attempt);
		
		
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
		
		// finding a name
		$name = (!empty($this->data['event']['name'])) ? $this->data['event']['name']. ' - ' : '';
		$name .= $this->data['location']['display'];
		
		
		// create booking record
		if(empty($this->booking)) {
			$booking = new Booking();
			
			$booking->customer_id = $customer['id'];
			
			$object = $this->getObject();
			
			$booking->talent_id = $object->id;
		
			if($object->isTalent()) {
				// agency booking
				if(!empty($profile) && $profile['type'] == 'a') {
					$booking->agency_id = $profile['id'];
				}
			}
			
			
			if(!empty($this->data['event']['id'])){
				$booking->event_id = $this->data['event']['id'];	
			}
		
			$booking->name = $name;
			
		}
		else{
			$booking = $this->booking;	
		}
		
		// check free quota eligibility 
		if(DataType::$FREE_BOOKINGS > 0){			
			
			$first = date("Y-m-")."01";
			
			$quota = $this->db->query("SELECT COUNT(*) AS free FROM bookings WHERE talent_id = 1 AND free_quota = 1 AND created >= '{$first}'", NULL, 1);
			
			if($quota['free'] < DataType::$FREE_BOOKINGS){
				$booking->free_quota = 1;
				applog("Adding a booking with free quota for {$booking->talent_id}");
			}			
		}
		
				
		//$booking->subscription_id = $this->subscription['sub_id'];
		// subscription assigns when they going to 
		$booking->location_id = $this->data['location']['id'];
		$booking->package_id = $this->data['package']['id'];
		
		$booking->session_start = Util::ToSysdate($this->date, $this->time);
		$booking->session_ends = date("Y-m-d H:i:s", strtotime($booking->session_start) + $this->data['package']['timespan']*60);
		$booking->status = DataType::$BOOKING_PENDING;
		$booking->talent_fee = $this->data['package']['fee'];
		$booking->charges =  App::bookingCharge($this->data['package']);	
		$booking->currency = 'USD'; //$talent->currency;
		$booking->advance_percentage = 0; //$package->advance_percentage;
		$booking->status_changed = 'NOW';
		
		if(empty($this->booking)) {
			$booking->setDueDate();
		}
		$booking->save();
		
		// sending notification
		$com = new ComEngine();
		
		if(empty($this->booking)) {
			$com->bookingCreated($booking);
		}
		else{
			//$com->bookingUpdated($booking);
		}
		
		
		// release object
		$this->object->booking_id = $booking->id;
		$this->object->save();
		
		// release object
		$this->object = NULL;
		$this->push();
		
		return $booking;
	}
	
	
	/**
	* Creating booking change request
	*/
	public function submitChangeRequest () {
		
		//check if it is possible to change
		if(!$this->validate()){
			return false;	
		}
		
		if(empty($this->booking)){
			$this->error = "Invalid booking";
			return false;
		}
		
		$customer = Session::getCustomer();
		$profile = Session::getProfile();
		
		$changed = 0;
		
		// creating request		
		$request = new ChangeRequest();
		$request->booking_id = $this->booking->id;
		$request->created_by = $customer['id'];
		$request->from_sender = $this->booking->isSender() ? 1 : 0;
		$request->is_cancel = 0;
		
		$start = Util::ToSysdate($this->date, $this->time);
		
		if($this->booking->session_start != $start){
			// date changed
			$request->session_start = $start;
			$request->ex_start = $this->booking->session_start;
			$changed++;
		}
				
		if($this->booking->location_id != $this->data['location']['id']){	
			// location changed	
			$request->location_id = $this->data['location']['id'];
			$request->ex_location = $this->booking->location_id;
			$changed++;
		}

		if($this->booking->package_id != $this->data['package']['id']) {
			// package changed
			$request->package = $this->data['package']['id'];
			$request->ex_package = $this->booking->package_id;
			$changed++;
		}
		
		
		if(isset($_POST['booking_discount']) && $this->booking->isRecipient()){
		// only receipient can add/change discount
		
			$exdiscount = (float)$this->booking->discount;
			$newdiscount = (!empty($_POST['booking_discount']) && is_numeric($_POST['booking_discount']) && $_POST['booking_discount'] >= 0) ? (float)$_POST['booking_discount'] : 0;
			
			if($newdiscount > $this->booking->total){
				$this->error = "Discount should not exceed the total booking amount";
				return false;	
			}
			
			//var_dump($exdiscount);
			//var_dump($newdiscount);
			if($exdiscount != $newdiscount){
				// discount changed
				$request->discount = Validator::cleanup($newdiscount, 12); 
				$request->ex_discount = $exdiscount;
				$changed++;
			}
		
		}
		
		if($changed == 0){
			$this->error = "No changes has been made.";
			return false;	
		}
		
		$request->status = 'p';
		$request->reason =  (!empty($_POST['reason'])) ? Validator::cleanup($_POST['reason'], 2) : 0;
		$request->save();
		
		
	
		return $request;
		
	}
	
}





?>