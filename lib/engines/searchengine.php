<?php
		
/****************************
* Talent/Agency search engine
*
*****************************/

class SearchEngine extends Engine{
	
	/** Last search id **/
	public $last_id;
	
	/** Cached search parameters **/
	public $cache;
	
	/** Search term **/
	public $term = NULL;
	
	/** skill category or agency type **/
	public $category = NULL;
	
	/** is this related to an agency **/
	public $agency = 0;
		
	/** City **/
	public $city = NULL;
	
	/** Region **/
	public $region = NULL;
	
	/** Country **/
	public $country = NULL;
	
	/** Current page **/
	public $page = 1;
	
	/** Page size **/
	public $pagesize = 48; //48;
	
	/** row size based on screen wdith **/
	public $rowsize = 3;
	
	/** search landing page box sizes **/
	public $boxsize = 6;
	
	/*** user location **/
	/** user city **/
	public $user_city = NULL;
	
	/** User region **/
	public $user_region = NULL;
	
	/** Tags ***/
	public $tags = [];
	
	
	/** cahing query results **/
	private $tag_categories = [];
	
	private $category_tags = [];
	
	
	function __construct ( ) {
		
		
			parent::__construct();
			
			$cookie = Util::getCookie(DataType::$COOKIE_SEARCH);
			if(!empty($cookie)){
				$this->last_id = $cookie;	
			}
			
			$this->capture();
								
	}
	
	/**
	* Capture all post parameters
	*/
	private function capture () {	
	
		if(!empty($_POST['cache']) && is_numeric($_POST['cache'])){
			$this->cache = SearchLog($_POST['cache']);				
			$this->city = $this->cache->city;
			$this->region = $this->cache->region;
			$this->country = $this->cache->country;							
		}
				
		if(!empty($_POST['page']) && is_numeric($_POST['page']) && $_POST['page'] > 0){
			$this->page = $_POST['page'];
		}
		
		if(!empty($_POST['query'])){
			$this->term = Validator::cleanup($_POST['query'], 100);
			$this->term = $this->db->escape($this->term);
		}	
		/*
		if(!empty($_POST['city'])){
			$this->city = Validator::cleanup($_POST['city'], 100);
			$this->city = $this->db->escape($this->city);
		}
		
		if(!empty($_POST['region'])){
			$this->region = Validator::cleanup($_POST['region'], 50);
			$this->region = $this->db->escape($this->region);
		}
		
		if(!empty($_POST['cat_id']) && is_numeric($_POST['cat_id']) && $_POST['cat_id']  > 0){
			$this->category = $_POST['cat_id'];			
		}
		*/
		
		if(!empty($_POST['country']) && isset(DataType::$countries[$_POST['country']])){		
			
			$this->country = Validator::cleanup($_POST['country'], 2);
		}
		else{			
			$this->country = Session::getCountry();	
		}
				
		if(isset($_POST['agency']) && $_POST['agency'] != ''){		
			$this->agency = (!empty($_POST['agency'])) ? 1 : 0;		
		}
		else{
			$this->agency = NULL;
		}
		
		if(!empty($_POST['width']) && is_numeric($_POST['width'])){
			if(	$_POST['width'] > 768 && $_POST['width'] <= 1200){
				$this->rowsize = 2;
			}
		}
				
		if(!empty($this->term) || !empty($this->city) || !empty($this->category)){
			
			$customer = Session::getCustomer();
			
			$log = new SearchLog();
			
			if(!empty($customer)) {
				$log->customer_id = $customer['id'];
			}
			
			if(!empty($this->term)) {
				$log->search = $this->term;
			}
			
			if(!empty($this->term)){
			
				// decompose results
				$term = strtolower($this->term);
				
				// seach parts in skills
				$skillsarray = $this->getSkills();
				$skills = array();
				
				// remove agency types from search string
				foreach ( DataType::$AGENCY_TYEPS as $id => $at ) {

					$name  = strtolower($at).' agency';
					//echo $name.', ';
					if(strpos($term, $name) !== false){
						$this->category = $id;
						$this->agency = 1;
						$term = str_replace($name, '', $term);
						break;
					}						
				}
				
				//echo $term;

				// organize
				
				if(!empty($term)){
					
					// match categories
					foreach ($skillsarray as $item) {

						$name  = strtolower($item['name']);

						// remove category labels from search string
						if(strpos($term, $name) !== false){
							$this->category = $item['id'];
							$term = str_replace($name, '', $term);
						}

						$skills[$item['id']] = $name;
					}
					
					// match tags
					$alltags = App::getTags();
					
					foreach ($alltags as $tid => $tag){
						
						//$tag = strtolower($tag);
						
						if(strpos($term, $tag) !== false){
							
							$this->tags[] = $tid;
							$term = str_replace($tag, '', $term);
						}
						
					}
					
					
				}
				
				
				$parts = !empty($term) ? explode(' ', $term) : array();
				
				// sanitize
				if(!empty($parts)) {
					foreach ($parts as $pi =>  $p){
						if(strlen($p) < 3 || $p == 'service' || $p == 's' || $p == 'ies'){
							unset($parts[$pi]);
						}
					}
			 	}
				
				if(!empty($term) && empty($this->category)) {
				
					// matching types
					foreach ($parts as $pi =>  $p){

						foreach ($skills as $sid => $s){

							if(strpos($p, $s) !== false){
								$this->category = $sid;
								unset($parts[$pi]);
								break;
							}					
						}				
					}
				}
				
				/*
				// matching tags
				if(!empty($parts)){
					
					$alltags = App::getTags();
					
					foreach ($parts as $pi => $p){
						
						$key = array_search($p, $alltags);
						
						if($key !== false){
							$this->tags[] = $key;
							unset($parts[$pi]);
						}						
					}
					
					if(!empty($this->tags)){
						$this->tags = array_unique($this->tags);
					}		
					
				}
*/
				// query for location
				//print_r($parts);
				if(!empty($parts)){
					$q = array();

					foreach ($parts as $p){
						if(empty($p)){
							continue;
						}
						$q[] = "city LIKE  '".$this->db->escape($p)."%'";
					}
					

					$cities = $this->db->query("SELECT city, region, country 
		FROM  locations WHERE ".implode(" OR ", $q)." GROUP BY city, region, country ");

					foreach ($cities as $city){

						if($city['country'] == $this->country && strpos($city['city'], ',') === false && strpos($city['city'], ' ') === false ){
							$this->city = $city['city'];
							$this->region = $city['region'];

							foreach ($parts as $pi => $p){
								if($p == strtolower($city['city'])){
									unset($parts[$pi]);
								}
							}
						}
					}
				}

				$this->term = implode(" ", $parts);
			
			}
			
			$log->country = $this->country;
			
			if(!empty($this->region)) {
				$log->region = $this->region;
			}
			
			if(!empty($this->city)) {
				$log->city = $this->city;
			}			
			if(!empty($this->category)) {
				$log->category = $this->category;
			}
			$log->agency = !empty($this->agency) ? 1 : 0;
			$log->save();
		//	echo "boo";
		}
		
	//	print_r($this);
	}
	
	
	/**
	* Get primary search results
	*/
	public function results () {
		
		
		if(!empty($_POST['list'])) {
			
			if($_POST['list'] == 'nt'){
				// new talents 
				return $this->newTalents();				
			}
			elseif($_POST['list'] == 'na'){
				// new agencies
				return $this->newAgencies();				
			}
			elseif($_POST['list'] == 'pt'){
				// populer talents
				return $this->populerTalents();			
			}
			elseif($_POST['list'] == 'pa'){
				// popler agencies
				return $this->populerAgencies();		
			}
			
		}
		
		
		
		$skip = ($this->page-1)*$this->pagesize;

		if(is_null($this->agency)){
			// search for both agency and talents
			$sql = $this->getQuery();
			//echo "combi";
		}
		elseif(!empty($this->agency)){
			// agency
			$sql = $this->getQuery();
			//echo "agency";			
		}
		else{
			// talents
			$sql = $this->getQuery();	
			//echo "talent";		
		}
		
		$order = '';
		if(!empty($this->city)){
			
			if(!empty($this->region)){
				
				$order = "ORDER BY (CASE WHEN packages > 0 THEN (CASE WHEN city = '".$this->city."' THEN 1 ELSE (CASE WHEN region = '".$this->region."' THEN 2 ELSE (CASE WHEN country = '".$this->country."' THEN 3 ELSE 5 END) END ) END) ELSE 4 END),  created desc";			
				
			}
			else{
				
				$order = "ORDER BY (CASE WHEN packages > 0 THEN  (CASE WHEN city = '".$this->city."' THEN 1 ELSE (CASE WHEN country = '".$this->country."' THEN 2 ELSE 5 END)  END) ELSE 4 END), created desc";
				
			}			
		}
		else{
			
			$order = "ORDER BY (CASE WHEN packages > 0 THEN (CASE WHEN country = '".$this->country."' THEN 1 ELSE 5 END) ELSE 4 END),  created desc  ";
			
		}
		
		
		$sql .= " ".$order." LIMIT ".$skip.",".$this->pagesize."";
		//echo $sql;
		return $this->db->query($sql);		
				
		
	}
	
	
	

	
	/**
	* get both agency and talents
	*/
	private function getQuery(){
		
		$cond = array("!isnull(p.published)", "(isnull(f.deleted) AND isnull(p.deleted))");
		
		$customer = Session::getCustomer();
		
		/*
		if(!empty($this->country)){			
			$cond[] = "(al.country = '".$this->country."' or tl.country = '".$this->country."' )";	
		}
		
		if(!empty($this->region)){			
			$cond[] = "(al.region = '".$this->region."' or al.region = '".$this->region."' )";	
		}
		*/
		if(!empty($this->city)){			
			//$cond[] = "(al.city = '".$this->city."' or al.city = '".$this->city."' )";	
			$cond[] = "l.country = '".$this->country."'";		
		}
		
		if(!empty($this->term)){
			$cond[] = "f.name like '%".$this->term."%'";
		}
		
		if(!empty($this->tags)){
			
			$cond[] = "pt.tag_id in (".implode(',', $this->tags).")";
			
		}
		elseif(!empty($this->category)){
			$cond[] = "c.skill_id = ".$this->category."";
		}
		
		$likes = (!empty($customer)) ?  '(SELECT id FROM likes WHERE page_id = p.id and isnull(deleted) and customer_id = '.$customer['id'].') as liked,': '0 as liked,';
		
		$sql = "SELECT  
p.id AS page_id,
p.handle,
p.active,
p.published,
f.name,
f.commitment,
f.is_agency,
f.created,
f.agency_type,
f.gender,
f.verified,
b.id AS subscription_id,
b.begin_date,
b.exp_date,
s.category,
s.name AS skill_name,
s.is_business,
i.thumb,
i.i400 AS image,
l.city,
l.region,
l.country,
(SELECT GROUP_CONCAT(skill) FROM talent_skills WHERE profile_id = f.id) AS skills,
pp.packages,
pp.max_fee,
pp.min_fee,
{$likes}
(SELECT COUNT(*) FROM bookings WHERE talent_id = f.id AND created > ADDDATE(NOW(), '-30 days') ) AS bookings
FROM profiles f
JOIN pages p ON (p.profile_id = f.id)
JOIN categories c ON (c.profile_id = f.id)
JOIN subscriptions b ON (begin_date <= NOW() AND b.exp_date > NOW() AND b.active = 1 AND ISNULL(b.deleted) AND b.category_id = c.id )
LEFT JOIN skills s ON (s.id = c.skill_id)
LEFT JOIN images i ON (i.id = f.image_id )
LEFT JOIN locations l ON (l.id = f.location_id)
LEFT JOIN profile_packages pp ON (pp.profile_id = f.id )
LEFT JOIN profile_tags pt on (pt.profile_id = f.id and isnull(pt.deleted))
";

		$sql .= "WHERE ".implode(" AND ", $cond);	
		$sql .= " GROUP BY f.id";
		
		return $sql;
	}
	
	
	/**
	* get additinal suggesions
	*/
	public function newAgencies () {
		
		$skip = 0;
		$size = $this->boxsize;
		
		if(!empty($_POST['list'])){
			$skip = ($this->page-1)*$this->pagesize;
			$size = $this->pagesize;
		}
		
		
		$sql = $this->getQuery();
		
		$order = "ORDER BY (CASE WHEN packages > 0 THEN (CASE WHEN country = '".$this->country."' THEN 1 ELSE 2 END) ELSE 5 END),created desc ";
		
		$sql .= " ".$order." LIMIT ".$skip.",".$size."";
		
		return $this->db->query($sql);
	}
	
	/**
	* get newest in area 
	*/
	public function newTalents () {
		
		$skip = 0;
		$size = $this->boxsize;
		
		if(!empty($_POST['list'])){
			$skip = ($this->page-1)*$this->pagesize;
			$size = $this->pagesize;
		}
		
		$sql = $this->getQuery();
		
		$order = "ORDER BY (CASE WHEN packages > 0 THEN (CASE WHEN country = '".$this->country."' THEN 1 ELSE 2 END) ELSE 5 END), created desc ";
		
		$sql .= " ".$order." LIMIT ".$skip.",".$size."";

		return $this->db->query($sql);
		
	}
	
	/**
	* get newest in area 
	*/
	public function homeWidget () {
		
		$skip = 0;
		$size = $this->boxsize;
		
		if(!empty($_POST['list'])){
			$skip = ($this->page-1)*$this->pagesize;
			$size = $this->pagesize;
		}
		
		$sql = "SELECT  p.id AS page_id, p.handle, p.profile_id,  p.active, p.published, t.name AS `name`, t.created AS `created`, t.agency_type, t.gender, t.is_agency,
t.verified,
ti.thumb, ti.i400 AS image, 
tl.city, tl.region, tl.country,
(SELECT GROUP_CONCAT(skill) FROM talent_skills WHERE profile_id = t.id) AS skills,
pp.packages, pp.min_fee, pp.max_fee
FROM profiles t
JOIN pages p ON (p.profile_id = t.id)
LEFT JOIN images ti ON (ti.id = t.image_id )
LEFT JOIN locations tl ON (tl.id = t.location_id)             
LEFT JOIN profile_packages pp ON (pp.profile_id = t.id)
WHERE pp.packages > 0 and isnull(p.deleted) and isnull(t.deleted)
ORDER BY RAND()
LIMIT 30";	
		
		return $this->db->query($sql);

		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = "select count(*) as total  FROM profiles where isnull(deleted)";
		$buffer->_component = "search_paging";
		$buffer->_pagesize = 50;
		$buffer->_page = 1; 
		return $buffer;
		
		
		
	}
	
	
	/**
	* Get trending talents in the area
	* within past 7 days
	*/
	public function populerTalents () {
		
		$skip = 0;
		$size = $this->boxsize;
		
		if(!empty($_POST['list'])){
			$skip = ($this->page-1)*$this->pagesize;
			$size = $this->pagesize;
		}
		
		$sql = $this->getQuery();
		
		$order = "ORDER BY (CASE WHEN packages > 0 THEN (CASE WHEN country = '".$this->country."' THEN 1 ELSE 2 END) ELSE 5 END), bookings desc ";
		
		$sql .= " ".$order." LIMIT ".$skip.",".$size."";
		
		return $this->db->query($sql);
		
	}
	
	/**
	* Get trending agencies in the area
	* within past 7 days
	*/
	public function populerAgencies () {
		
		$skip = 0;
		$size = $this->boxsize;
		
		if(!empty($_POST['list'])){
			$skip = ($this->page-1)*$this->pagesize;
			$size = $this->pagesize;
		}
		
		$sql = $this->getQuery();
		
		$order = "ORDER BY (CASE WHEN packages > 0 THEN (CASE WHEN country = '".$this->country."' THEN 1 ELSE 2 END) ELSE 5 END),bookings desc ";
		
		$sql .= " ".$order." LIMIT ".$skip.",".$size."";
		
		return $this->db->query($sql);
		
	}
	
	/**
	* get related searches
	*/
	public function relatedSearches (){
		
	}
	
	/**
	* Perform search and gives best result match with customer condition
	*/
	
	
	
	/**
	* Get all tallent skills to display
	* reading from cache, if not create new cache file
	*/
	public function getSkills () {
		
		$cache = getCache('system/skills', true);
		
		if(!empty($cache)){
			return $cache;	
		}
		
		$skills = $this->db->query("select * from skills WHERE is_agency = 0 order by category, is_business, name");	
		
		putCache('system/skills', $skills, true);
		
		return $skills;
	}
	
	
	
	/**
	* User favourites
	* Return sql buffer object
	*/
	public function favourites () {
		
		$customer = Session::getCustomer();
	
		$sql = "SELECT  p.id AS page_id, p.handle, p.profile_id,  p.active, p.published, t.name AS `name`, t.created AS `created`, t.agency_type, t.gender, t.is_agency,
t.verified,
ti.thumb, ti.i400 AS image, 
tl.city, tl.region, tl.country,
(SELECT GROUP_CONCAT(skill) FROM talent_skills WHERE profile_id = t.id) AS skills,
lk.id AS liked,
pp.packages, pp.min_fee, pp.max_fee
FROM likes lk
JOIN pages p ON (p.id = lk.page_id)
LEFT JOIN profiles t ON (t.id = p.profile_id)
LEFT JOIN images ti ON (ti.id = t.image_id )
LEFT JOIN locations tl ON (tl.id = t.location_id)
LEFT JOIN profile_packages pp ON (pp.profile_id = t.id)
WHERE lk.customer_id = ".$customer['id']." AND ISNULL(lk.deleted)
HAVING !ISNULL(liked)
ORDER BY lk.created desc";	

		$buffer = new Buffer();
		$buffer->_datasql = $sql;
		$buffer->_countsql = "select count(*) as total  FROM likes where customer_id = ".$customer['id']."  and isnull(deleted)";
		$buffer->_component = "search_paging";
		$buffer->_pagesize = 50;
		$buffer->_page = 1; 
		return $buffer;
		
	}
	
	
	
	/**
	* Get selected category 
	*/
	public function getCategoryName () {
		
		if(!empty($this->category)){
			$skills = $this->getSkills();
			$skills = Util::groupArray($skills, 'id','name');
			
			return $skills[$this->category];
		}
		
		return NULL;
	}
	
	public function getCategoryTags () {
		
		if(empty($this->category)){
			return [];
		}
		
		if(empty($this->category_tags)){
			$this->category_tags = $this->db->query("
		SELECT s.tag_id AS id
		FROM skill_tags s 
		WHERE s.skill_id = ".$this->category." AND ISNULL(s.deleted)
		");
		}
		
		return $this->category_tags;
		
	}
	
	/**
	* Get tag categories
	*/
	public function getTagsCategories () {
		
		if(empty($this->tags)){
			return NULL;
		}
		
		if(empty($this->tag_categories)){
		
			$this->tag_categories =  $this->db->query("SELECT k.id AS skill_id, k.name AS skill_name , GROUP_CONCAT(a.tag_id) AS tags
	FROM tags t
	JOIN skill_tags s ON (s.tag_id = t.id)
	JOIN skills k ON (k.id = s.skill_id)
	LEFT JOIN skill_tags a ON (a.skill_id = k.id AND ISNULL(a.deleted))
	WHERE t.id IN (".implode(',', $this->tags).")
	GROUP BY  s.id");
		}
		
		return $this->tag_categories;
		
	}
	
	
}

?>