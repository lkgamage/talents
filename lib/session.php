<?php
/*************************************************
* 
* 	Current ssession data class
* 	This will store/retrive session data
*	from cookies or any other means
*
*************************************************/

class Session {
	
	public static $logs = [];
	
	/**
	* Cache current user data
	*/
	public static function setCustomer ($customer){
		
		$img = $customer->getDP();
		$url = '';
		
		if(!empty($img)){
			$url = $img->i400;
		}
		else{
			$url = fixImage(NULL, $customer->gender);
		}
				
		// UserID|~|name|~|DP|~| DPID
		$para = $customer->id.'|~|'.$customer->firstname.'|~|'.$url.'|~|'.(!empty($img) ? $img->id : '');
		Util::setCookie(DataType::$COOKIE_USER, $para);
		
		// set default currency
		Util::setCookie(DataType::$COOKIE_CURRENCY, $customer->currency);
		
		// set default time zone cookie
		if($customer->timezone) {
			$offset = DataType::$timezones[$customer->timezone][2];
			Util::setCookie(DataType::$COOKIE_TIMEZONE, $offset);
		}
		elseif(isset($_POST['tz'])){
			$offset = $_POST['tz'];
			Util::setCookie(DataType::$COOKIE_TIMEZONE, $offset);
		}
		
	}
	
	/**
	* Clear customer cache
	*/
	public function clearCustomer() {
		Util::setCookie(DataType::$COOKIE_USER, '', -365);
	}
	
	/**
	* Cache current profile data
	*/
	public static function setProfile ($profile){
		
		$img = $profile->getDP();
		$url = '';
		
		//$url = fixImage();
		if(!empty($img)){
			$url = $img->i400;
		}
		else{
			$url = fixImage(NULL, $profile->gender, $profile->is_agency);			
		}
		
		// ProfileID|~|type|~|name|~|DP
		$para = $profile->id.'|~|'.($profile->is_agency == 1 ? 'a' : 't').'|~|'.$profile->name.'|~|'.$url.'|~|'.(!empty($img) ? $img->id : '');
		Util::setCookie(DataType::$COOKIE_PROFILE, $para);
		
		// set default currency
		Util::setCookie(DataType::$COOKIE_CURRENCY, $profile->currency);
		
		// timezone offset
		Util::setCookie(DataType::$COOKIE_TIMEZONE, $profile->timeoffset);
	}
	
	
	/**
	* get cached customer
	* return id,  DP
	*/
	public static function getCustomer(){
		
		$customer = Util::getCookie(DataType::$COOKIE_USER);
		
		if(!empty($customer)){
			
			$customer = explode('|~|', trim($customer));
			
			if(count($customer) == 4){
		
				// UserID|~|name|~|DP|~|dpid
				return array(
						'id' => $customer[0],
						'name' => $customer[1],
						'dp' => $customer[2],
						'dpid' => $customer[3],
						'tz' => Util::getCookie(DataType::$COOKIE_TIMEZONE)
					);
				}			
		}
			
		
		return NULL;
		
	}
	
	/**
	* get chashed profile 
	* return id,  DP and type (t/a)
	*/
	public static function getProfile(){
		
		$profile = Util::getCookie(DataType::$COOKIE_PROFILE);
		
		
		if(!empty($profile)){
			
			$profile = explode('|~|', trim($profile));
		
			// UserID|~|name|~|DP|~| Dp id
			if(count($profile) == 5) {
				return array(
						'id' => $profile[0],
						'type' => $profile[1],
						'name' => $profile[2],
						'dp' => $profile[3],
						'dpid' => $profile[4]
					);	
			}
		}
						
		return NULL;
		
	}
	
	
	/**
	* Clear profile
	*/
	public static function clearProfile () {
	
		Util::setCookie(DataType::$COOKIE_PROFILE,'',-365);
		
	}
	
	/**
	* Set or Get currency
	*/
	public static function currency ($crr = NULL){
		
		if(!empty($crr)){
			Util::setCookie(DataType::$COOKIE_CURRENCY, $crr);		
		}
		else{
			$crr = Util::getCookie(DataType::$COOKIE_CURRENCY);
		}
		
		return !empty($crr) ? $crr : 'USD';
	}
	
	/**
	* Get currency code
	* Return selected currency symbol
	*/
	public function currencyCode () {
		
		$crr = Util::getCookie(DataType::$COOKIE_CURRENCY);
		
		if(!empty($crr)){
			return 	 DataType::$csymbols[$crr];	
		}

		
		return '$';
	}
	
	public function currencyName () {
		
		$crr = Util::getCookie(DataType::$COOKIE_CURRENCY);
		
		if(!empty($crr)){
			return 	 DataType::$currencies[$crr];	
		}

		
		return DataType::$currencies['USD'];
	}
	
	/**
	* Get time offset in minutes
	*/
	public static function timezone ($formatted = false){
	
		if(isset($_COOKIE[DataType::$COOKIE_TIMEZONE])) {
			$tz =  Util::getCookie(DataType::$COOKIE_TIMEZONE);	
		}
		elseif(isset($_POST['tz'])){
			Util::setCookie(DataType::$COOKIE_TIMEZONE, $_POST['tz']);
			$tz = $_POST['tz'];	
		}
		else{
			$tz = 0;
		}
		
		if($formatted){
			return (($tz >= 0) ? floor($tz/60) : ceil($tz/60) ).':'.abs($tz%60);
		}
		
		return $tz;
	
	}
	
	/**
	* get time offset in hh:mm:ss format
	*/
	public static function timeDiff () {
			
		$a = Util::getCookie(DataType::$COOKIE_TIMEZONE);
		return 	($a < 0 ? '-' : '').abs(floor($a/60)).':'.abs($a%60).':00';
	}
	
	
	
	/**
	* Set booking detail for future use
	
	public static function setBooking () {
		
		$data = Util::serializePost();
		
		Util::setCookie(DataType::$COOKIE_BOOKING, $data, 7);
		
		return $data;
	}*/
	
	/**
	* Get last booking detail
	
	public static function getBooking () {
		
		$cookie = Util::getCookie(DataType::$COOKIE_BOOKING);
		
		Util::deserializePost($cookie, true);
		
		return $cookie;
	}
	*/
	/**
	* Clear booking 
	*/
	public static function clearBooking () {
		Util::clearCookie(DataType::$COOKIE_BOOKING,'',-365);
	}
	
	public static function setCountry ($code) {
		Util::setCookie(DataType::$COOKIE_COUNTRY, $code);
	}
			
	
	/**
	* Get country code for current request
	* Check country cookie, if not available, 
	* check it with IP2Location service
	*/
	public static function getCountry (){
		
		$country = Util::getCookie(DataType::$COOKIE_COUNTRY);
		
		if($_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
			$country =  'LK';
		}
		if(empty($country) ){
			
			$url = "https://ipgeolocation.abstractapi.com/v1/?api_key=".Config::$ip2locaton_key."&ip_address=".$_SERVER['REMOTE_ADDR'];
			
			$ch = curl_init();
        	curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);
			
			$response = @json_decode($response, true);
			
			// create service usage record
			$log = new ServiceLog();
			$log->service = 'IPL';
			$log->type = 'ip2_location';
			$log->code = $info['http_code'];
			$log->save();
						
			Util::setCookie(DataType::$COOKIE_COUNTRY, $response['country_code']);
			
			return $response['country_code'];
		}
		
		return $country;
	}
	
	/**
	* Set event id for temparary search
	*/
	public static function setEvent ($event_id){
		
		Util::setCookie(DataType::$COOKIE_EVENT, $event_id);
		
	}
	
	/**
	* Get event id saved in cookie
	*/
	public static function getEvent (){
		
		return Util::getCookie(DataType::$COOKIE_EVENT);
		
	}
	
	
	/**
	* Set event id for temparary search
	*/
	public static function clearEvent (){
		
		Util::setCookie(DataType::$COOKIE_EVENT,'',-365);
		
	}
	
	/**
	* Get curranet language setting
	*/
	public static function language ($lng = NULL) {
		
		if(!empty($lng) && isset(DataType::$languages[$lng]) ){
			Util::setCookie(DataType::$COOKIE_LANGUAGE, $lng );
		}
		elseif(isset($_GET['lng']) && isset(DataType::$languages[$_GET['lng']])){
			return $_GET['lng'];
		}
		else{
			$lng = Util::getCookie(DataType::$COOKIE_LANGUAGE );
		}
		
		return !empty($lng) ? $lng : 'en';
		
	}
	
	
}

?>