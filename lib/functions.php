<?php

/***
* Global functions
*/
/** 
* Get formatted amount with currency symbol
* Suitable for display 
*/
function currency ($amount, $currency = NULL, $convert = true){
	
	$amount = (float)$amount;
	
	if(empty($currency)){
		$currency = Session::currency();
	}
	
	if($currency != 'USD' && $convert){
		$amount = $amount*Currencies::$$currency;

		if($amount - floor($amount) > 0.99){
			$amount = ceil($amount);
			
		}
	}	

	$out = ($currency ? DataType::$csymbols[$currency].'' : '$');
	$out .= (((abs($amount)*100)%100) > 0) ? number_format($amount,2): number_format($amount);

	return $out;
}

/**
* Convert amount from profile currency to USD
* No currency symbol
*/
function toUSD ($amount, $currency = NULL){
	
	$amount = (float)$amount;
	
	if(empty($currency)){
		$currency = Session::currency();
	}
	
	if($currency != 'USD'){
		$amount = round($amount/Currencies::$$currency,4);
	}

	return $amount;	
}

/***
* Convert USD to profile currency
* No currency symbol
* sutaibel for form fields
*/
function toCurrency ($amount, $currency = NULL){
	
	$amount = (float)$amount;
	
	if(empty($currency)){
		$currency = Session::currency();
	}
	
	if($currency != 'USD'){
		$amount = round($amount*Currencies::$$currency,2);
	}

	return $amount;
}


/**
* build callable phone number
*/
function phone ($number, $country_code){
	
	$code = $country_code == 'LK' ? '0' : '+'.DataType::$countries[$contact['customer']['country_code']][1];
	
	return $code.$number;
}


/****
* Return default image url if image is not avilable for an object
*/
function fixImage ( $url, $gender = 1, $is_agency = false){
	
	if(!empty($url)){
		return $url;	
	}
	
	if(!empty($is_agency)){
		return Util::mapURL('/images/placeholder-business.png');
	}
	
	if($gender === '0'){
		
		return Util::mapURL('/images/placeholder-girl.png');
	}
	
	return Util::mapURL('/images/placeholder-boy.png');	
		
}

/**
* Get cached file content, if file not found, returns false
* file path, is_data
* is data = true : decode json
*/
function getCache ($filepath, $is_data = false){
	
	if(file_exists(Config::$cache_path.'/'.$filepath.'.ccf') ){	
		
		if(strpos($filepath, 'system') !== false && filemtime(Config::$cache_path.'/'.$filepath.'.ccf') < time()-3600 ){
			
			return false;
		}
	
		if($is_data) {	
			return json_decode(file_get_contents(Config::$cache_path.'/'.$filepath.'.ccf'), true);		
		}
		else{
			return file_get_contents(Config::$cache_path.'/'.$filepath.'.ccf');	
		}
	}
	
	
	return false;
}

/**
* Create or replace a cache file
* path, content, is_data 
* is_data = true : encode json before save 
*/
function putCache ($filepath, $content, $is_data = false){
	
	if($is_data) {
		$content = json_encode($content, JSON_UNESCAPED_UNICODE);
	}
	
	return file_put_contents(Config::$cache_path.'/'.$filepath.'.ccf', $content);
	
}


/**
* SMS log 
* Type: string (20 char)
*/
function EmailLog ($customer_id, $email_id, $type){
	
	$log = new EmailLog();
	$log->customer_id = $customer_id;
	$log->email_id = $email_id;
	$log->type = Validator::cleanup($type, 20);
	$log->save();
	
}

/**
* Email log 
* Type: string (20 char)
*/
function SMSLog ($customer_id, $phone_id, $type){
	
	$log = new SmsLog();
	$log->customer_id = $customer_id;
	$log->phone_id = $phone_id;
	$log->type = Validator::cleanup($type, 20);
	$log->save();
}

/**
* Get browser information
*/
function getBrowser() { 
  $u_agent = $_SERVER['HTTP_USER_AGENT'];
  $bname = 'Unknown';
  $platform = 'Unknown';
  $version= "";

  //First get the platform?
  if (preg_match('/linux/i', $u_agent)) {
    $platform = 'linux';
  }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
    $platform = 'mac';
  }elseif (preg_match('/windows|win32/i', $u_agent)) {
    $platform = 'windows';
  }

  // Next get the name of the useragent yes seperately and for good reason
  if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
    $bname = 'Internet Explorer';
    $ub = "MSIE";
  }elseif(preg_match('/Firefox/i',$u_agent)){
    $bname = 'Mozilla Firefox';
    $ub = "Firefox";
  }elseif(preg_match('/OPR/i',$u_agent)){
    $bname = 'Opera';
    $ub = "Opera";
  }elseif(preg_match('/Chrome/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
    $bname = 'Google Chrome';
    $ub = "Chrome";
  }elseif(preg_match('/Safari/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
    $bname = 'Apple Safari';
    $ub = "Safari";
  }elseif(preg_match('/Netscape/i',$u_agent)){
    $bname = 'Netscape';
    $ub = "Netscape";
  }elseif(preg_match('/Edge/i',$u_agent)){
    $bname = 'Edge';
    $ub = "Edge";
  }elseif(preg_match('/Trident/i',$u_agent)){
    $bname = 'Internet Explorer';
    $ub = "MSIE";
  }

  // finally get the correct version number
  $known = array('Version', $ub, 'other');
  $pattern = '#(?<browser>' . join('|', $known) .
')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
  if (!preg_match_all($pattern, $u_agent, $matches)) {
    // we have no matching number just continue
  }
  // see how many we have
  $i = count($matches['browser']);
  if ($i != 1) {
    //we will have two since we are not using 'other' argument yet
    //see if version is before or after the name
    if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
        $version= $matches['version'][0];
    }else {
        $version= $matches['version'][1];
    }
  }else {
    $version= $matches['version'][0];
  }

  // check if we have a number
  if ($version==null || $version=="") {$version="?";}

  return array(
    'userAgent' => $u_agent,
    'name'      => $bname,
    'version'   => $version,
    'platform'  => $platform,
    'pattern'    => $pattern
  );
}

function utf8ize( $mixed ) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } elseif (is_string($mixed)) {
        return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
    }
    return $mixed;
}

/*** 
* Creatuing log entry 
* [datetime] [file] [customer_id]:[profile_id] [type] [message]
********/
function applog ($message, $save_post = false){
	
	$customer = Session::getCustomer();
	$profile = Session::getProfile();
	$trace = debug_backtrace();	
	//print_r($trace);
	$trace = $trace[0];
	
	$line = date("Y-m-d H:i:s")."\t".str_replace(Config::$base_path, '', $trace['file']).':'.$trace['line']."\t";
	$line .= ((!empty($customer) ? $customer['id'] : 'Guest').':'.(!empty($profile) ? $profile['id'] : ''))."\t";
	$line .= $message."\n";
	
	if($save_post){
		$txt = "";
		foreach ($_POST as $key => $value) {
			$txt .= $key.'='.$value."&";
		}
		$line .= $txt."\n";
	}
	
	// add to log entry
	Session::$logs[] = $line;
	
}

?>