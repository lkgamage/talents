<?php
/**
* User must be logged in to access this data
*/

class IPGController extends Controller {
	
	
	function __construct ($request = NULL) {
					
		 parent::__construct($request);
		 
		 $this->template = 'ajax.php';
		
		
		 // sanirtize whole post as strings
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);


		// remove controller name	 
		 array_shift($this->data);

		 if(!empty($this->data[0]) && method_exists($this, $this->data[0])){

			 $method = array_shift($this->data);
			 $this->$method();
		 }
		 else{
			$this->view->error = "Method not found";
			$this->render(); 
		 }		
	}
	
	
	
	/**
	* Show payment screen for an invoice
	*/
	public function invoice_pay () {
	
		
		if(!$this->auth->isAuthenticated()){
			$this->view->error = "Please login or create account";
			return $this->render();;
		}
		
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			
			$invoice = new Invoice($_POST['id']);
			
			if($invoice->status == DataTYpe::$INVOICE_PAID){
				$this->view->error = "This invoice already paid";
				return $this->render();
			}
			elseif($invoice->status == DataTYpe::$INVOICE_EXPIRED){
				$this->view->error = "This invoice is expred. Please create another invoice";
				return $this->render();
			}
			
			$this->view->invoice = $invoice;
			
			$this->view->html = $this->component('ipg/payment');
			$this->view->id = 'dash-contents';
			$this->view->status = true;
			
			
		}
		else{
			$this->view->error = "Invalid request";
		}
		
		$this->render();
		
	}
	
	/**
	* Process payments
	*/
	public function invoice_process () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id']) ){
			
			applog("Invoice processing request recieved for id {$_POST['id']}");
			
			$invoice = new Invoice($_POST['id']);
			
			if($invoice->status == DataType::$INVOICE_PAID){
				$this->view->error = "Invoice already paid";
				return $this->render();
			}
			else{
				if(strtotime($invoice->due_date) < time() || $invoice->status == DataType::$INVOICE_EXPIRED){
					$this->view->error = "Invoice expired";
					return $this->render();
				}
			}
			
			$processor  = new PaymentProcessor();
			
			if($invoice->type == DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION){
				
				applog("Processing subscription");
				
				$res = $processor->createSubscription($invoice);
								
				if(!empty($res)){
					$this->view->data = $res;
					$this->view->action = 'payment_confirm';
					$this->view->status = true;
				}
				else{
					$this->view->error = "Payment processing error.<br>Please contact customer support";
					$this->view->status = false;
				}
			}			
			
		}
		else{
			$this->view->error = "Invalid invoice ID";
		}
		
		$this->render();
		
	}
	
	/**
	* Payment processed, check if we recived IPN into web hook, otherwise
	* manually check subscription status and active it
	* Check payment details
	*/
	public function processed () {
				
		
		if(!empty($_POST['id']) && !empty($_POST['secret']) && !empty($_POST['method'])){
			
			applog("Transaction processed, checking payment status", true);
			
			$intent_id = $_POST['id'];
			$secret = $_POST['secret'];
			$method = $_POST['method'];
			
			$invoice = App::getInvoiceByIntent($intent_id);

			if(!empty($invoice)){
				
				$info = $invoice->getinfo();
				$processor = new PaymentProcessor();
				
				$s_sub = $processor->getSubscription($info['stripe']['pending']);
				$intent = $processor->getPaymentIntent($intent_id);
				
				if($s_sub->status == 'active' && $intent->status == 'succeeded'){
					
					// activate subscription
					if(empty($info['subscription']['active'])){
					
						applog("Status active, activating subscription");

						$subscription = $invoice->getSubscription();

						if($subscription->active != 1){
							$subscription->active = 1;
							$subscription->save();
							applog("Subscription {$subscription->id} activated");
						}
						else{
							applog("Subscription {$subscription->id} already activated by IPN");
						}						
					}
					else{
						applog("Subscription {$subscription->id} already activated by IPN");
					}
					
					// if there are any free subscriptions activated before, delete them
					$info = $invoice->getinfo();
					$category = new Category();
					$category->populate($info['category']);
					$category->deleteFreeSubscriptions();
					
					// updating invoice status
					if($invoice->status != DataType::$INVOICE_PAID){
						$invoice->status = DataType::$INVOICE_PAID;
						$invoice->save();
						applog('Invoice status updated to PAID');				
						
					}
					else{
						applog('Invoice status updated by IPN');
					}
					
					// updating category with new package id
					if(empty($info['package']['one_time'])){
						
						$category = new Category();
						$category->populate($info['category']);
						
						if($category->package_id != $info['subscription']['package_id']){
							$category->package_id = $info['subscription']['package_id'];
							$category->save();
						}						
						
						$category->renewal_date = date('j');
						$category->last_renewal = 'NOW';
						$category->save();
					}
					
					// make stribe scubscription active
					$category->setActiveStripeSubscription($s_sub->id);
					applog("{$s_sub->id} set as default");
									
					// updating payment status
					$payment = $invoice->getPayment();
					
					if($payment->status == DataType::$PAYMENT_PROCESSING){
						
						$payment->status =DataType::$PAYMENT_SUCCESS;
						$payment->success = 1;
						$payment->amount = ($intent->amount_received/100);
						$payment->charge_id = $intent->charges->data[0]->id;
						$payment->save();
					}
					else{
						applog("Payment already updated by IPN");
					}
					
					// adding payment method
					$method = $this->user->getPaymentMethod($intent->payment_method);
					if(empty($method)){
						
						$sm = $processor->getPaymentMethod($intent->payment_method);
						
						$method = new PaymentMethod();
						$method->customer_id = $this->user->id;
						$method->method_id = $sm->id;
						$method->type = $sm->card->brand;
						$method->card_number = $sm->card->last4;
						$method->exp_month = $sm->card->exp_month;
						$method->exp_year = $sm->card->exp_year;
						$method->funding = $sm->card->funding;
						$method->save();
						
						$payment->method_id = $method->id;
						$payment->save();
							
						applog("Payment method {$intent->payment_method} created. {$method->id} ");
					}
					else{
						applog("Payment method {$intent->payment_method} already in database");
					}
					
					$this->view->invoice = $invoice;
					
					// sending payment notifications
					
					// showing success page
					$this->view->html = $this->component('ipg/success');
					$this->view->message = "Thank you for the payment!";
					$this->view->id = 'dash-contents';
					$this->view->status = true;
				}
				else{
					applog("Stripe subscription is not active yet, showing processing page {$s_sub->id},  abort");
					
					$this->view->html = $this->component('ipg/inpocess');
					$this->view->id = 'dash-contents';
					$this->view->status = true;
				}	
				
			}			
			else{
				// send immergency email to support
				
				applog("Invoice record not found for given intent {$intent_id}, showing error page, abort");
				$this->view->html = $this->component('ipg/error');
				$this->view->id = 'dash-contents';
				$this->view->status = true;
				
			}
				
			
		}
		
		$this->render();
	}
	
	
	/***************** Override render function for easy access *************/
	
	public function  render ($view_path = '') {
		
		parent::render('/common/void');
		
	}
	
	
}
	