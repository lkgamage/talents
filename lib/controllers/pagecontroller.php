<?php

class PageController extends Controller {
	
	// Admin auth object
	private $master;
	
	
	function __construct ($request = NULL) {
			
		
		 parent::__construct($request);
		 
		 $this->template = 'default.php';
		 
		 if(empty($this->data[0])){
			 $this->home();
		 }
		 elseif(method_exists($this, $this->data[0]) && $this->data[0] != 'showPage'){
			
			 $method = array_shift($this->data);
			 $this->$method();
		 }
		else{
			
			if(!is_numeric($this->data[0])){				
				
				$page = App::findPage($this->data[0]);
				if(!empty($page)) {
					$this->showPage($page);
				}
				else{
					$this->render('/front/404'); 
				}
					
			}
			 else {
				$this->render('/front/404'); 
			 }
		 }
		
	}
	
	
	/**
	* Home page
	*/
	private function home () {
		//$this->loadJS('talent.js');
		$this->render('/front/home'); 
		
	}
	
		
	/**
	* Talent Search page
	*/
	private function search () {
		
		$this->loadCSS('vanilla-calendar.css');
		$this->loadJS('vanilla-calendar-min.js');
		$this->loadJS('booking.js');
		//$this->loadJS('https://maps.googleapis.com/maps/api/js?key='.Config::$google_key.'&libraries=places', true);
		
		$engine = new SearchEngine();
		
		if(!empty($this->data[0])){
			
			// find category
			$term = strtolower($this->data[0]);
			
			if (strpos($term, 'agency') !== false) {
				// lookup on agency
				foreach ( DataType::$AGENCY_TYEPS as $id => $at ) {

					$name  = strtolower($at).' agency';

					if(strpos($term, $name) !== false){
						$category = $id;
						$this->view->agency = 1;
						$catname = $at.' Agency';
					}						
				}
				
			}
			else{
				// lookup on talent categories
				$skillsarray = $engine->getSkills();
				foreach ($skillsarray as $item) {

					$name  = strtolower($item['name']);

					// find category name
					if(strpos($name,'|') !== false) {
						
						$np = explode('|', $name);
						foreach ($np as $p){
							if(strpos($term, $p) !== false){
								$category = $item['id'];
								$catname = ucfirst($p);
							}
						}
						
					}
					elseif(strlen($term) >= strlen($name) && strpos($term, $name) !== false){
						$category = $item['id'];
						$catname = $item['name'];
					}
					elseif(strlen($term) < strlen($name) && strpos($name, $term) !== false){
						$category = $item['id'];
						$catname = $item['name'];
					}
					
					
					if(!empty($category)){
						break;
					}
				}
				
			}
			
			if(!empty($category)){
				$this->view->category = $category;
				$this->view->catname = $catname;
				$this->view->term = $catname;
				
				$engine->category = $category;
				$engine->agency = (!empty($agency)) ? 1 : 0;
				
				$title = "Book ".(in_array($catname[0], array('A','E','I','O','U')) ? 'an ' : 'a ').$catname;
			}
						

		
		}
		
		if(!empty($_GET['q'])){
			$term = preg_replace( '/[^a-z\' ]/i', '', strtolower(trim($_GET['q'])));
			// check if it is a city
			$cities = App::searchCity ($term);
			foreach ($cities as $city){

				if($city['country'] == $engine->country && strpos($city['city'], ',') === false && strpos($city['city'], ' ') === false ){
					$engine->city = $city['city'];
					$engine->region = $city['region'];
					break;
				}
			}
			
			
			$this->view->term = $term;
		}
		
		$this->view->title = !empty($title) ? $title : 'Book an artist, entertainer, place, or services for your party';	
		$this->view->description = !empty($title) ? $title.' in your area for your party or event' : 'Book an artist, entertainer, place, or services for your party.';
		
		$this->view->engine = $engine;
		$this->view->url = 'search'.(!empty($this->data[0]) ? '/'.$this->data[0] : '');
		
		$this->render('/front/search'); 
	}
	
	
	
	/**
	* Join into site page
	*/
	private function join () {
		
		//$this->template = 'signup';
		
		$this->loadJS('jquery.validate.min.js');
		$this->loadJS('signup.js?var=2');
		$this->loadJS('dashboard.js');
	//	$this->loadCSS('dashboard.css');
		$this->loadJS('https://accounts.google.com/gsi/client', true);
		$this->loadJS('https://maps.googleapis.com/maps/api/js?key='.Config::$google_key.'&libraries=places&callbac=palceInit', true);
		
		
		$this->view->maximized = 1;
		$this->view->title = "Join";
		
		if($this->auth->isAuthenticated()){
			
			$customer = $this->auth->getCustomer();
			
			if(!empty($this->data[0])){
			
				$this->data[0] = strtolower($this->data[0]);
				
				if($this->data[0] == 't' || $this->data[0] == 'talent'){
					
					$profiles = $customer->getProfiles();
					
					if(empty($profiles)){
						$this->render('/front/signup-talent');
					}
					else{
						$this->redirect('/dashboard');
						return;
					}
				}
				elseif($this->data[0] == 'a' || $this->data[0] == 'agency'){
					
					$profiles = $customer->getProfiles();
					
					if(empty($profiles)){
						$this->render('/front/signup-agency');
					}
					else{
						$this->redirect('/dashboard');
						return;
					}
				}
				elseif($this->data[0] == 'p' || $this->data[0] == 'page'){
					
					$profile = Session::getProfile();
			
					if(!empty($profile)){
												
						$object = new Profile($profile['id']);
						
						if(!$object->ready()){
							//Util::setCookie(DataType::$COOKIE_REDIRECT,'/join/'.$this->data[0]);
							$this->redirect('/join_continue');
							return;
						}
						
					}
					else{
						// check furhtehr for possible cases
						$profiles = $customer->getProfiles();
						if(!empty($profiles)){
							$object = $profiles[0];
							Session::setProfile($profile);
						}						
					}					
					
					if(!empty($object)){					
					
						$this->view->object = $object;
						$this->view->page = $object->getPage();							
						$this->loadJS('gallery.js');
						$this->loadJS('profile.js');
						$this->render('/front/signup-page');
					}
					else{
						$this->redirect('/join_continue');
						return;						
					}
					
					
				}
				/*elseif($this->data[0] == 'c' || $this->data[0] == 'customer'){
					// check if user has a booking
					// if so, continue;
				}*/
				else{
					$this->redirect('/dashboard');
					return;	
				}			
				
			}
			else{
				$this->redirect('/dashboard');
				return;	
			}
			
		}
		else{
			
			if(!empty($this->data[0])){
				Util::setCookie(DataType::$COOKIE_REDIRECT,'/join/'.$this->data[0]);
				$this->redirect('/login');
			}
			else{
				$this->render('/front/signup-home');
			}
			
			
			
			
			
		}
		
		
	}
	
	/**
	* Completing registration process
	*/
	private function join_continue () {
		
		//$this->template = 'signup';
		
		$this->loadJS('jquery.validate.min.js');
		$this->loadJS('signup.js');
		$this->view->maximized = 1;
		$this->view->title = "Create Profile";
		
		if($this->auth->isAuthenticated()){
			
			$this->render('/front/signup-selection');
			
		}
		else{
			$this->redirect('/join');
		}
		
	}
	
	/**
	* Login page
	*/
	private function login () {
		
		if(!$this->auth->isAuthenticated()){		

			
			Util::getCookie(DataType::$COOKIE_REDIRECT);
			$this->view->country = Session::getCountry();
			
			$this->view->maximized = 1;
			$this->view->title = "Login";
			
			$this->render('/front/login'); 	
		}
		else{
			$this->redirect('/dashboard');	
		}
	}
	
	/**
	* Log out 
	*/
	private function logout () {
		
		$this->auth->logout();		
		
		$this->redirect('/');
		
	}
	

	
	
	/**
	* username validation and log-in
	*/
	private function pwd_validate () {
		
		$this->template = 'ajax.php';
		
		if(!empty($_POST['pwd']) && !empty($_POST['loginkey'])){
			
			$pwd = trim($_POST['pwd']);
			$login = new Login();
			
			if($login->setKey($_POST['loginkey'])){
				
				if($this->auth->pwdlogin($login, $pwd)){
					
					$this->view->status = true;
					$this->view->trigger = 'login_complete';
					$this->view->message = "Thank you! Loading your dashboard.";
					
				}
				else{

					$this->view->error = array('id' => 'tm_password', 'message' => $this->auth->error);
					$this->view->message = $this->auth->error;
				}
				
			}
			else{
				$this->view->trigger = 'advance_to_un';
				$this->view->message = 'Invalid username';	
			}
			
		}
		else{
			$this->error = array('id' => 'tm_password', 'message' => "Please enter password");
		}

		$this->render('/common/void'); 
	}
	
	
	
	/**
	* Booking form for a talent
	*/
	private function book () {
		
				
		$this->loadCSS('vanilla-calendar.css');
		$this->loadJS('vanilla-calendar-min.js');
		$this->loadJS('booking.js');
		$this->loadJS('https://maps.googleapis.com/maps/api/js?key='.Config::$google_key.'&libraries=places&callbac=palceInit', true);
		$this->loadJS('jquery.validate.min.js');
		
		$engine = new BookingEngine();
		
		if(!empty($this->data[0])){
			
			$found = $engine->setHandle($this->data[0]);
			$engine->push();
			//print_r($engine);
			if($found){
				$this->view->engine = $engine;
				$this->render('/front/book');
			}
			
			else {
				$this->search();	
			}
			/**/
				
		}
		elseif($engine->canMakeBooking()){
			
				$this->view->summery = true;
			$this->view->engine = $engine;
				$this->render('/front/book');
				
		}
		else{
			$this->search();	
		}
		
		return;
		
		/*************************************/

		if(!empty($this->data[0]) && is_numeric($this->data[0])){
			
			Session::getBooking();	
			
			$talent = new Talent($this->data[0]);
			$_POST['talent_id'] = $talent->id;
						
			if($talent->ready() && !$talent->isDeleted()){
				$this->view->talent = $talent;
				$this->render('/front/book'); 
			}
			else{
				$this->view->error = "Sorry! We can't find the talent you are looking for.";
				$this->search();
			}
		}
		elseif (!empty($_COOKIE['TMLB']) ){
			
			// read from cookie		
			Util::deserializePost($_COOKIE['TMLB']);
			
			$talent = new Talent($_POST['talent_id']);
			
			if($talent->ready() && !$talent->isDeleted()){
				$this->view->talent = $talent;
				$this->render('/front/book'); 
			}
			else{
				$this->view->error = "Sorry! We can't find the talent you are looking for.";
				$this->search();
			}			
		}
		else{
			$this->view->error = "Sorry! We can't find the talent you are looking for.";
			$this->search();	
		}		
		
	}
	
	
	/**
	* Show support pages
	*/
	public function support () {
		
		$this->loadCSS('support.css');	
		
		if(!empty($this->data[0])){
		
			$page = preg_replace("/[^A-Za-z0-9\-_]/", '', $this->data[0]);

			
			if(file_exists(Config::$base_path.'/templates/support/'.$page.'.php')){
				$this->view->title = ucwords(preg_replace("/[0-9\-_]/", '', $page));
				$this->render('/support/'.$page.'');
			}
			else{
				$this->view->lost = true;
				$this->render('/support/home');
			}
			
		}
		else{
			
			$this->render('/support/home');	
		}
		
	}
	

	/***
	* Accept incomming auth requests
	*/
	public function accessctrl () {
		
		if(!empty($_GET['key'])){
			
			$str = urldecode($_GET['key']);			
			$str = base64_decode($str);
			$str  = Util::decrypt(rtrim($str,'='), Config::$cookie_encript_key );
			
			if(!empty($str)){
				
				$parts = explode('|', $str);
				
				if(count($parts) == 4 && is_numeric($parts[0]) && is_numeric($parts[2])){
					
					$customer = new Customer($parts[0]);
					$user = new User($parts[2]);
					
					if($customer->modified == $parts[1] && $user->password == $parts[3]){
						// authenticate customer
						
						Session::clearProfile();
						
						
						$phones = $customer->getPhones();
					
						if(!empty($phones)){
							
							$code = new Code();
							$code->type = 'p';
							$code->customer_id = $customer->id;
							$code->phone_id = $phones[0]->id;							
							$code->attempts = 0;
							$code->verified = 0;
							$code->save();							
							
							$this->view->code = $code;
							$this->render('/front/syslogin');
						}
												
						
						
					}
				}
				
			}
			
		}
		
		
		exit();
	}
	
	
	/**
	* Dashboard home page
	*/
	public function dashboard () {
		
		if(empty($this->user)){
			$this->redirect('/login');
			return;	
		}
		
		 $this->loadJS('https://maps.googleapis.com/maps/api/js?key='.Config::$google_key.'&libraries=places&callbac=palceInit', true);
		 $this->loadJS('dashboard.js');
		 $this->loadJS('gallery.js');
		 $this->loadCSS('dashboard.css');
		 $this->loadCSS('gallery.css');
		 $this->loadCSS('vanilla-calendar.css');
		 $this->loadJS('vanilla-calendar-min.js');
		
		if(!empty($this->data)){
			
			$action = array();
			
			$default = array('bookings', 'profile','appointments','events','packages', 'account', 'messages', 'favourite', 'settings','talents');
			
			//$items = array('booking','event','package','appointment','talent','message');
			
			if(in_array($this->data[0], $default) !== false){
				
				$action = array('cura' => 'dash_menu', 'id' => $this->data[0]);	
							
			}			
			elseif($this->data[0] == 'booking'){
				
				if(!empty($this->data[1]) && is_numeric($this->data[1])){
					$action = array('cura' => 'get_booking', 'id' => $this->data[1]);	
				}
				else {
					$action = array('cura' => 'dash_menu', 'id' => 'bookings');	
				}
							
			}
			elseif($this->data[0] == 'event'){
				
				if(!empty($this->data[1]) && is_numeric($this->data[1])){
					$action = array('cura' => 'event_view', 'id' => $this->data[1]);	
				}
				elseif(!empty($this->data[1]) && $this->data[1] == 'create'){
					$action = array('cura' => 'event_edit');
				}
				else {
					$action = array('cura' => 'dash_menu', 'id' => 'events');	
				}
							
			}
			elseif($this->data[0] == 'appointment'){
				
				if(!empty($this->data[1]) && is_numeric($this->data[1])){
					$action = array('cura' => 'appointment_options', 'id' => $this->data[1], 'action' => 'view');	
				}
				elseif(!empty($this->data[1]) && $this->data[1] == 'create'){
					$action = array('cura' => 'appointment_options', 'action' => 'edit');
				}
				else {
					$action = array('cura' => 'dash_menu', 'id' => 'appointments');	
				}							
			}
			elseif($this->data[0] == 'subscription'){
				if(!empty($this->data[1]) && is_numeric($this->data[1])){
					$action = array('cura' => 'subscription_view', 'id' => $this->data[1]);	
				}
				else{
					$action = array('cura' => 'dash_menu', 'id' => 'profile');	
				}
			}
			
			
			
			 $this->view->action = $action;
		}
		 
		
		 
		 $this->render('/dashboard/dashboard'); 
			
	}
	
	/**
	* Display talent/agency page
	*/
	public function showPage ($page) {
		
		
		if(!empty($this->user) && $page->customer_id == $this->user->id && !isset($_GET['cache'])){
			
			$this->view->tools = true;
			$this->loadJS('page-wizard.js');
			
			if(isset($_GET['mode'])){
				// poage in edit mode 
				
				if($_GET['mode'] == 'edit'){
					$this->view->edit = true;
					$this->loadCSS('profile-edit.css');
					$this->loadJS('jquery.ui.touch-punch.min.js');
				}
								
			}
			
			if($page->isEmpty('updated')){
			// if page never updated	
			
				if(!isset($_GET['mode']) || $_GET['mode'] != 'edit'){
					$this->loadJS('page-wizard.js');
				}
				$this->view->show_wizard = true;
			}
			
			$object = $page->getProfile();
			
			$this->_include_page_files();
			$this->view->object = $object;
			$this->view->page = $page;	
			$this->view->title = $object->name;	
			$this->render('/front/page'); 
		}
		else{
			// Guest/other users
			// server cache
			if($page->active == 1 && !$page->isDeleted()){
				
				$this->_include_page_files();
				
				$this->view->page = $page;	
				
				if($page->isPublished() ){
					
					if(!$this->renderCache('/pages/'.$page->handle)){
						// creatig cache
						$page->publish();
						$this->renderCache('/pages/'.$page->handle);
											
					}
										
					// add page view record
					$pageview = new PageView();
					$pageview->page_id = $page->id;

					if($this->auth->isAuthenticated()) {
						$pageview->customer_id = $this->user->id;
					}
					
					$pageview->country = Session::getCountry();
					$pageview->save();
					
				}
				
				
				else{
				//	echo "no cache";
					$this->render('/front/404'); 
				}
				
			}
			else{
			//	echo "page not active";
				$this->render('/front/404'); 
			}			
				
		}		
			
	}
	
	
	private function _include_page_files () {
		
		$this->loadCSS('vanilla-calendar.css');		
		$this->loadCSS('profile.css');
		$this->loadCSS('gallery.css');
		$this->loadJS('jquery-ui.min.js');
		$this->loadJS('vanilla-calendar-min.js');
		$this->loadJS('https://maps.googleapis.com/maps/api/js?key='.Config::$google_key.'&libraries=places&callbac=palceInit', true);
		$this->loadJS('jquery.validate.min.js');
		$this->loadJS('profile.js');
		$this->loadJS('booking.js');
		$this->loadJS('gallery.js');	
		
	}
	
	private function tiktok () {
		
		$this->view->title = 'කියුරාටැලන්ට් ටික්ටොක් චැලේන්ජ්';
		
		$this->render('/front/tiktok'); 
		
	}
	
	 
}
?>