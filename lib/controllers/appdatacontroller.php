<?php
/**
* Public app data controller
*/

class AppDataController extends Controller {
	
	// Admin auth object
	private $master;
	
	
	
	function __construct ($request = NULL) {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
					
		 parent::__construct($request);
		 
		 $this->template = 'ajax.php';

		 		 
		 array_shift($this->data);
		 
		 if(!empty($this->data[0]) && method_exists($this, $this->data[0])){
			 
			 $method = array_shift($this->data);
			 $this->$method();
			
		 }
		 else{
			$this->view->error = "Method not found";
			$this->render(); 
		 }
				 
		
	}
	
	/**
	* Customer accepting GDPR
	* creating cookie
	*/
	public function gdpr_accept () {
		
		Util::setCookie('GDPR',time());
		$this->view->status = true;
		$this->view->message = "Thank you!";
		
		$this->render(); 
	}
	
	
	/**
	* Buffer data 
	*/
	public function bff () {
		
		applog("Buffer paging requested", true);

		$this->view->status = false;
		$this->view->message = "Invalid request";
		
		if(!empty($_POST['key']) && !empty($_POST['page'])){
			
			$buffer = new Buffer($_POST['key']);

			$this->view->html = $buffer->getPage($_POST['page']);
			
			$pg = $buffer->_page;
			$this->view->data = array( 'page' => $pg, 'loaded' => $pg*$buffer->_pagesize );
			$this->view->status = true;
			$this->view->message = NULL;
			
		}
		
		$this->render(); 
	}
	
	
	/**
	* Select country on reg. from
	*/
	public function signup_country () {
		
		applog("Country set in signup form", true);
		
		if(!empty($_POST['country']) && isset(DataType::$countries[$_POST['country']])){
			$this->view->nodes = array(
				array('id' => 'phone-code', 'value'=> '+'.DataType::$countries[$_POST['country']][1]),
				array('id' => 'user_firstname', 'value' => '')
			);
			
			Session::setCountry($_POST['country']);
		}
		
		$this->render(); 
	}
	
	
	/**
	* Creating customer, initial step
	*/
	public function create_account () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		// validate reCaptcha
		applog("Creating an account", true);
		
		// clear all login  session
		$this->auth->logout();
		Session::clearProfile();
		Session::clearCustomer();
		
		// checking phone number
		$country = !empty($_POST['country']) ? $_POST['country'] : NULL;
		$region = !empty($_POST['region']) ? $_POST['region'] : NULL;
		$city = !empty($_POST['city']) ? $_POST['city'] : NULL;
		$firstname = !empty($_POST['user_firstname']) ? trim($_POST['user_firstname']) : NULL;
		$lastname = !empty($_POST['user_lastname']) ? trim($_POST['user_lastname']) : NULL;
		$mobile = !empty($_POST['user_phone']) ? $_POST['user_phone'] : NULL;
		$emailaddress = !empty($_POST['user_email']) ? $_POST['user_email'] : NULL;
		$type = !empty($_POST['customer']) ? $_POST['customer'] : NULL;
				
		
		if(empty($firstname) || empty($lastname)){
			$this->view->error = "Please enter your first and last names";
			applog("Firstname or last name is empty, abort", true);
			return $this->render(); 		
		}
		
		if(strlen($firstname) < 2 || strlen($lastname) < 2){
			$this->view->error = "First name or last name is too short";
			applog("Firstname or last name is too short, abort", true);
			return $this->render(); 		
		}
		
		if(empty($country) || empty($city)){
			$this->view->error = "Please select your nearest city";
			applog("city or country is empty, abort", true);
			return $this->render(); 	
		}
		
		if(!isset(DataType::$countries[$country])){
			$this->view->error = "Please select your nearest city";
			applog("Country not lised, abort", true);
			return $this->render(); 	
		}
		
		if(empty($mobile)){
			$this->view->error = "Mobile number is required to verify your account";
			applog("No mobile number, abort", true);
			return $this->render(); 	
		}
		
		if(empty($emailaddress) || !Validator::isEmail($emailaddress)){
			$this->view->error = "Please enter valid email address";	
			return $this->render(); 	
		}
		
		// send otp code
		$cn = DataType::$countries[$country][1];	
		$mobile = Validator::cleanupPhoneNumber($mobile, $cn);
		
		
		$phone = new phone($country, $mobile);
		
		if($phone->ready() && !$phone->isDeleted()){
			
			$this->view->error = 'Given mobile number is already associated with an account.<br>You may <a href="'.Util::mapURL('/login').'">signin to your account</a>';
			applog("Existing phone found, abort", true);
			return $this->render(); 
		}
		
		$email = new Email($emailaddress);
		if($email->ready() && !$email->isDeleted()){
			
			$this->view->error = 'Given email address is already associated with an account.<br>You may <a href="'.Util::mapURL('/login').'">signin to your account</a>';
			applog("Existing email found, abort", true);
			return $this->render(); 
			
		}
		
		$com = new ComEngine();
			
		// creating address
		$location = Util::saveLocation();	
		
		// set country cookie
		//Util::setCookie(DataType::$COOKIE_COUNTRY, $location->country);
		Session::setCountry($location->country);
		
		$user = new Customer();
		$user->firstname = Validator::cleanup($_POST['user_firstname'], 100);
		$user->lastname = Validator::cleanup($_POST['user_lastname'], 100);
		$user->location_id = $location->id;	
		$user->timeoffset = Validator::cleanup($_POST['tz'],6);
		$user->timezone = Util::findTimeZone($_POST['tz']);
		$user->active = 1;
		$user->save();
		applog("Customer account created ".$user->id);
		
		$this->view->user = $user;
	
		$phone = new Phone();
		$phone->customer_id = $user->id;
		$phone->is_primary = 1;
		$phone->type = 'M'; //Validator::cleanup($type, 1);
		$phone->country_code = $country;
		$phone->phone = Validator::cleanup($mobile, 14);
		$phone->sms = 1;
		$phone->whatsapp = 0;
		$phone->save();
		applog("Phone created ".$phone->id);
				
		$email = new Email();
		$email->customer_id = $user->id;
		$email->is_primary = 1;
		$email->is_public = 0;
		$email->email = Validator::cleanup($emailaddress, 100);
		$email->is_verified = 0;
		$email->save();	
		applog("Email created ".$phone->id);
		
		// create a login with given phone number
		$login = new Login();
		$login->customer_id = $user->id;
		$login->type = 'p';
		$login->phone_id = $phone->id;	
		$login->is_verified = 0;
		$login->save();
		applog("Login enable with phone number ".$phone->phone);
		
		$this->view->code = App::sendOTP('p', $phone->id, $user->id );
		
		// SEND SMS
		$com->welcomeSMS($user, $phone, $this->view->code->code);
		applog("Welcome SMS sent to  ".$phone->phone." with code id ".$this->view->code->id);
		
		$this->view->customer = $user;	
		
		
		// create a login with given email address
		$login2 = new Login();
		$login2->customer_id = $user->id;
		$login2->type = 'e';
		$login2->email_id = $email->id;				
		$login2->is_verified = 0;
		$login2->save();
		applog("Loggin enable with email ".$email->email);
		
		$this->view->login2 = $login2;
		
		$this->view->emailcode = App::sendOTP('e', $email->id, $user->id );
		
		// SEND Welcome email
		$com->welcomeEmail($user, $email, $this->view->emailcode->code);
		applog("Welcome SMS sent to  ".$email->email." with code id ".$this->view->emailcode->id);
		
		$this->view->signup = true;
		$this->view->html = $this->component('signup_code_verification');
		$this->view->message = "Your account created";	
		$this->view->id = 'signup_panel';	
		$this->view->action = 'otp_timer';
		
		// sending verification code
		$this->view->status = true;		
		
		$this->render(); 	
	}
	
	/**
	* Login validation process
	*/
	private function login_validate () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Login request recieved ", true);
		
		if(!empty($_POST['username'])){
			
			$un = str_replace(' ', '', trim($_POST['username'],'+'));
			$un = ltrim($un,'0');
			
			if(!is_numeric($un) && !Validator::isEmail($un)){
				$this->view->error = "Please enter email address or mobile number";
				applog("Not looks like a phone number or email address, abort");
				return $this->render();
			}
			
			if(is_numeric($un) && !empty($_POST['country']) && isset(DataType::$countries[$_POST['country']]) ){
				$country = $_POST['country'];
				Util::setCookie(DataType::$COOKIE_COUNTRY, $country);
			}
			else{
				$country = Session::getCountry();	
			}
			
			if(is_numeric($un)) {
				$un = $country.$un;
				applog("looking up login with phone number ".$un);
			}
			else{
				applog("looking up login with email address ".$un);
			}
		
			$com = new ComEngine();
			$record	= App::searchLogin($un);
		
			if(!empty($record)){
				
				$customer = new Customer();
				$customer->populate($record);
				
				applog("Customer record found ".$customer->id);
				
				if($customer->active == 0 || $customer->isDeleted()){
					$this->view->error = '<span class="txt-red">Your account has been disabled.</span><br>Please contact customer support<br> support@curatalent.com or call (+94) 70 507 7070';
					applog("Account is not active or deleted, abort ");
					return $this->render();
				}
					
				$id = ($record['type'] == 'e') ? $record['email_id'] : $record['phone_id'];

				$code = App::sendOTP($record['type'], $id, $record['id']);					
				
				if($code !== false){
					
					if($record['type'] == 'e'){
						// email code 	
						
						$this->view->username = substr($un,0,2).'****'.substr($un, strpos($un,'@'));
						$email = new Email($record['email_id']);
						
						// send OTP email
						$com->otpEmail($customer, $email, $code->code );
						
						applog("Emailing code id ".$code->id." to ".$email->id);
						
					}
					else{
						// sms  code
						$number = '+'.str_replace($country, DataType::$countries[$_POST['country']][1], $un);
						
						$this->view->username = '****'.substr($number, strlen($number)-4, 4 );
						$phone = new Phone($record['phone_id']);
						
						// send OTP SMS
						$com->otpSMS($customer, $phone, $code->code );
						
						applog("SMS code id ".$code->id." to ".$phone->id);
											
					}
					
					
					
					$this->view->user = $customer;
					$this->view->code = $code;
					
					$this->view->type = $record['type'];
					$this->view->purpose = 'login';
					$this->view->id = 'login_block';
					$this->view->action = 'otp_timer';
					$this->view->html = $this->component('code_verification');
										
					
					
				}
				else{
					$this->view->message = App::$error;
					$this->view->error = App::$error;
				}
				// write rest of codes
			}
			else{
				
				if(strpos($un, '@') !== false){
					$this->view->message = "No account found for given email address";
					$this->view->error = $this->view->message;
				}
				else{
					$this->view->message = "Mobile number not associated with any account";
					$this->view->error = '<img src="https://static.abstractapi.com/country-flags/'.$country.'_flag.png" class="inline-flag"> '.('+'.str_replace($country, DataType::$countries[$_POST['country']][1].' ', $un)).' mobile number not found. <br><a href="javascript:void(0)" cura="change_login_country">Change Country</a>';
				}
				
				applog("No account found for given username, abort");
							
			}
	
			
		}
		else{
			$this->view->message = 'Please enter your email or mobile number';
			$this->view->error = $this->view->message;	
			applog("No username, abort");
		}
		
		$this->render(); 
	}
	
	
	
	/**
	* Verify OTP
	*/
	private function validate_otp () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Otp validation request", true);
	
		if(!empty($_POST['purpose']) && !empty($_POST['vcode']) && !empty($_POST['code_id']) && is_numeric($_POST['code_id']) && !empty($_POST['device']) && !empty($_POST['dfp']) && is_numeric($_POST['dfp'])){
			
			$task = $_POST['purpose'];
			$value = Validator::cleanup($_POST['vcode'], 6);
			$fingureprint = Validator::cleanup($_POST['dfp'],12);
			
			$code = new Code($_POST['code_id']);
						
			if(!$code->ready()){				
				$this->resend_otp();
				applog("Invalid code ID, resending code");
				return;
			}
			
			if(!$code->validate($value)){

				if($code->attempts >= 4 || $code->expired()){
					applog("Code expired or max attempt reached");
					$this->resend_otp();
					return;
				}
				else {				
					
					if($task == 'signup' && !empty($_POST['email_code_id']) && is_numeric($_POST['email_code_id'])){

						$code = new Code($_POST['email_code_id']);
						
						if(!empty($code) && $code->validate($value)){
							// code is valid, proceed
							
						}
						else{
							$this->view->error = "Invalid OTP code";
							applog("code validation failed, abort");
							return $this->render();
						}
					
					}
					else{
							$this->view->error = "Invalid OTP code";
							applog("Code id is empty");
							return $this->render();
						}
					
				}
			}
			
			
			$login = $code->getLogin();
			
			
			// code is valid
			if(!empty($login)){
				
			$customer = new Customer();
			$customer->populate($login);				
			applog("Code validated, authenticating customer ".$customer->id);
				
				if($customer->active == 0){
					$customer->active = 1;
					$customer->save();
				}
				
				if(empty($login['is_verified'])){
					// verify phone or email
					
					if($login['type'] == 'e'){
						$email = new Email($login['email_id']);	
						$email->is_verified = 1;
						$email->save();
						applog("Verifying email address ".$email->email);
					}
					else{
						$phone = new Phone($login['phone_id']);
						$phone->is_verified = true;
						$phone->save();	
						applog("Verifying phone ".$phone->phone);
					}							
				}
				
				// identify device
				$device = $customer->getDevice($fingureprint);
				
				if(empty($device)){
					$device = Util::getDevice();
					$device->customer_id = $customer->id;
					$device->save();
					applog("Device created for customer ".$customer->id." with dev id ".$device->id);
					// shoot an email to notify							
				}
									
				$this->auth->authenticate($login, $device);
				$this->user = $customer;
				
				// create customer login record
				
				$cl = new CustomerLogin();
				$cl->customer_id = $customer->id;
				$cl->login_id = $login['login_id'];
				$cl->device_id = $device->id;
				$cl->fingureprint = $device->fingureprint;
				$cl->ip = $_SERVER['REMOTE_ADDR'];
				$cl->save();
		
				Session::clearProfile();
				Session::setCustomer($customer);
				applog("Customer authenticated with login id ".$cl->id);
								
				if($task == 'login'){
						
					$url = Util::getCookie(DataType::$COOKIE_REDIRECT);
					
					if(!empty($url)){						
						$this->view->data =  $url;
						Util::clearCookie(DataType::$COOKIE_REDIRECT);
					}
					else{
						$this->view->data = '/dashboard';	
					}
					
					$this->view->trigger = 'redirect';
					$this->view->message = "Thank you!";
					
					// change menu
					$this->view->nodes = array(
						array(
							'id' => 'header', 
							'html' => $this->component('menu')
						)
					);
				}
				elseif($task == 'signup'){
				
					$url = Util::getCookie(DataType::$COOKIE_REDIRECT);
					
					if(!empty($url)){						
						$this->view->data = $url;
						Util::clearCookie(DataType::$COOKIE_REDIRECT);
					}
					else{
						$this->view->data = '/join_continue';	
					}
					
					$this->view->trigger = 'redirect';
					$this->view->message = "Thank you!";
				}
				elseif($task == 'verify'){
					// verify aditional email or phone
						
				}
				
			}
			else {
				$this->view->error = $login->error;
				$this->view->message = $login->error;
			}			
		}
		
		//$this->view->action = 'validate_otp_end';
		$this->render();
		
	}
	
	/**
	* function resend OTP
	*/
	public function resend_otp () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		applog("Resending OTP", true);
		
		if(!empty($_POST['purpose']) && !empty($_POST['code_id']) && is_numeric($_POST['code_id']) && !empty($_POST['dfp']) && is_numeric($_POST['dfp'])){

			$code = new Code($_POST['code_id']);
			
			if(!$code->ready()){
				
				$this->view->error = "Invalid code";
				applog("Existing code is invalid, abort");
				return $this->render();
			}
			
			$record = $code->getLogin();
			
			$id = ($record['type'] == 'e') ? $record['email_id'] : $record['phone_id'];
			$newcode = App::sendOTP($record['type'], $id, $record['id']);			
			
			$com = new ComEngine();
			
			if($newcode != false) {
				
				$customer = $newcode->getCustomer();
				
				$un = $record['username'];
				
				if($record['type'] == 'e'){
					// email code 
					
					$email = new Email($id);						

					$com->otpEmail($customer, $email, $newcode->code );
					
					$this->view->error = "New OTP sent. Please check your inbox";
											
					applog("Emailing code id ".$newcode->id." to ".$email->email);
				}
				else{
					// sms  code
					$number = '+'.str_replace($record['country_code'], DataType::$countries[$record['country_code']][1], $un);
					
					$phone = new Phone($record['phone_id']);
					
					$com->otpSMS($customer, $phone, $newcode->code );
					
					$this->view->error = "New OTP sent. Please check your mobile";	
					applog("OTP id ".$newcode->id." sent to mobile ".$number);
				}
				
				$this->view->status = true;
				$this->view->message = "New OTP sent";
				$this->view->nodes = array(
								array('id' => 'code_id', 'value' => $newcode->id),
								array('id' => 'vcode', 'value' => ''),
							);
								
			}
			else{
				$this->view->message = App::$error;
				$this->view->error = $this->view->message;
				applog("OTP not sent on error :".App::$error);
			}
								
			
		}
		else{
			$this->view->error = "Invalid request";	
			applog("Invalid OTP resend request, abort");
		}
		
		$this->render();
	}
	
	/**
	* Creating a profile records for talents 
	*/
	public function signup_profile () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Creating a talent profile", true);
					
		if($this->auth->isAuthenticated()){
			// talent profile
			
			$count = $this->user->getActiveProfilesCount();
			applog("User has {$count} profiles");

			$name = !empty($_POST['talent_name']) ? $_POST['talent_name'] : '';
			$gender = !empty($_POST['gender']) ? $_POST['gender'] : 0;
			$currency = (!empty($_POST['currency']) && isset(DataType::$currencies[$_POST['currency']])) ? $_POST['currency'] : 'USD';

			if(!is_numeric($gender)) {				
				$this->view->error = "Please select gender";
				return $this->render();
			}									

			// add talent skills										
			$location = $this->user->getLocation();
			$location->duplicate();

			$profile = new Profile();
			$profile->customer_id = $this->user->id;
			$profile->verified = 0;
			$profile->active = 1;
			$profile->location_id = $location->id;
			$profile->currency = $currency;
			$profile->timezone = $this->user->timezone;
			$profile->timeoffset = $this->user->timeoffset;
			$profile->is_agency = 0;
			$profile->commitment = $count > 0 ? 1 : 0;

			if(!empty($name)) {
				$profile->name = Validator::cleanup($name, 400);
			}
			else{
				$profile->name = $this->user->firstname.' '.$this->user->lastname;
			}

			if(!empty($gender) && is_numeric($gender)){
				$profile->gender = $gender%2;

				$this->user->gender = $gender%2;
				$this->user->save();							
			}

			$profile->save();
			applog("Talent profile created, id ".$profile->id);

			// create a page
			$profile->getPage();

			// switch to profile
			Session::setProfile($profile);

			$this->view->message = "Profile created";
			$this->view->data = "/join/page";	
			$this->view->trigger = "redirect";

			

			// talent profile ends
		}

		else{
			Util::setCookie(DataType::$COOKIE_REDIRECT,'/join/talent');
			$this->view->trigger = "redirect";	
			$this->view->data = "/logint";
			applog('Customer not authenticated, sending to login page');
		}
					
		
		$this->render();
	}
	
	/**
	* Signup as an agancy
	*/
	private function signup_agency () {
		
		applog("Creating an agency profile", true);
	
		if($this->auth->isAuthenticated()){
			
			$count = $this->user->getActiveProfilesCount();
							
			if(!empty($_POST['agency_name']) && !empty($_POST['city']) && !empty($_POST['country']) && !empty($_POST['address'])){

				// creating address				
				$location = Util::saveLocation();

				$agency = new Profile();
				$agency->customer_id = $this->user->id;
				$agency->agency_type = (!empty($_POST['agency_type'])  && is_numeric($_POST['agency_type'])) ? $_POST['agency_type'] : 1;
				$agency->name = Validator::cleanup($_POST['agency_name'], 400);
				$agency->location_id = $location->id;
				$agency->timezone = $this->user->timezone;
				$agency->timeoffset = $this->user->timeoffset;
				$agency->is_agency = 1;
				$agency->active = 1;
				$agency->commitment = $count > 0 ? 1 : 0;
				$agency->save();

				// create a page
				$agency->getPage();

				// switch to profile
				Session::setProfile($agency);

				$this->view->status = true;
				$this->view->message = "Agency created";
				$this->view->data = "/join/page";	
				$this->view->trigger = "redirect";
				applog('Agency created with id '.$agency->id);

			}
			else{
				$this->view->error = "Please enter agency name and address";
				applog('Agency name or address missing, abort');
			}
				
			
			
		}
		else{
			Util::setCookie(DataType::$COOKIE_REDIRECT,'/join/agency');
			$this->view->trigger = "redirect";
			$this->view->data = "/login";	
			applog('Customer not authenticated, redirecting to login, abort');
		}
		
		$this->render();
	}
	
	/**
	* Create and publish page
	*/
	public function signup_page () {
		
		applog('Signup page creation request received', true);
		
		if(!empty($_POST['public-page']) && is_numeric($_POST['public-page']) && $this->auth->isAuthenticated()) {
			
			$page = new Page($_POST['public-page']);
			
			if($page->customer_id == $this->user->id){
				
				$handle = (!empty($_POST['page_handle'])) ?  Validator::cleanup($_POST['page_handle'],20) : NULL;
				$title = (!empty($_POST['section_title'])) ?  Validator::cleanup($_POST['section_title'],1000) : NULL;
				$body = (!empty($_POST['section_body'])) ?  Validator::cleanup($_POST['section_body'],2000) : NULL;
				
				$handle = trim($handle,'-');
				
				if(empty($handle)){
					$this->view->error = "Please choose a page handle";
					applog('Page handle not specified, abort');
					return $this->render();
				}
				else{
					
					$handle = str_replace(' ','-', trim($handle));
					$handle = preg_replace("/[\+\!\#\$\~%\^\&\*\(\)\{\}\[\]\'\"\|\/\<\>\,`\=\\@\:\;\?]/",'', $handle);
					
					if(strlen($handle) < 4 || strlen($handle) > 20){
						$this->view->error = "Page handle should have 4 - 20 characters";
						applog('page handle length error : "'.$handle.'", abort');
						return $this->render();
					}

					if(Validator::isReservedWord($handle)){

						$this->view->error =  "<span class=\"txt-bold\">{$handle}</span> is a reserved word.<br>Please choose another.";
						applog('Pahe handle is a reserved word, abort');
						return $this->render();	
					}

					if(is_numeric($handle)){
						$this->view->error = "Handle should have at least one alphabet character.";
						applog('Page handle has no alpahbet, abort');
						return $this->render();	
					}
					
					$pg = App::findPage($handle);
					if(!empty($pg) && !$pg->isDeleted() && $pg->customer_id != $this->user->id){
						$this->view->error = "Page url is already taken. choose another";
						applog('Agency handle is already in use, abort');
						return $this->render();	
					}
					
				}
				
				if(empty($title)){
					$this->view->error = "Please enter a title for your page";
					applog('Empty page title, abort');
					return $this->render();
				}
				
				if(!empty($title)){
					
					if(Validator::hasPhoneNumber($title)){
						$this->view->error = "Please remove phone number from the page title";
						return $this->render();
					}
					
					$title= Validator::removeEmails($title);					
				}
				
				if(empty($body) || strlen($body) < 10){
					$this->view->error = "Please describe more about you/your service";
					applog('Insufficient page description, abort');
					return $this->render();
				}
				
				if(!empty($body)){
					
					if(Validator::hasPhoneNumber($body)){
						$this->view->error = "Please remove phone number from the description";
						return $this->render();
					}
					
					$body= Validator::removeEmails($body);					
				}
				
				$section = $page->getSection('a');
				$section->title = $title;
				$section->body = $body;
				$section->save();				
				
				
				
				$page->handle = strtolower($handle);
				$page->save();
				
				$page->publish();
				
				$this->view->page = $page;
				
				$this->view->message = "Your page created";
				$this->view->status = true;
				$this->view->html = $this->component('signup-share');
				$this->view->id = 'signup_panel';
				//$this->view->data = "/dashboard";	
				//$this->view->action = "redirect";
				applog('Page id '.$page->id.' updated');
				
			}
			else{
				$this->view->error = "You do not have permission to update the page";
				$this->view->trigger = "/join";	
				$this->view->data = "redirect";
				applog("Customer {$this->user->id} does not have permission to update page {$page->id}, abort");
			}
			
		}
		else{
			$this->view->trigger = "/join";	
			$this->view->data = "redirect";	
			applog('Customer not authenticated, sending to login');
		}
		
		$this->render();
		
	}
	
	
	/**
	* Unset event from the current booking
	*/
	public function event_clear () {
				
		$engine = new BookingEngine();
		$engine->clearEvent();
		
		$this->view->status = true;
		$this->render();
		
		applog('Event removed from the booking engine ');
	}
	
	/**
	* Set event for current booking
	*/
	public function event_set (){
		
		applog("Booking event set rquest recived", true);
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$event = new Event($_POST['id']);
			
			$engine = new BookingEngine();
			
			if($engine->setEvent($event)){
				
				applog('Event '.$event->id.' set, checking packages');
			
				// display available packages
				//$packages = $this->o
				$packages = $engine->checkAvailability();
				
				if($packages === false){
					$this->view->error = $engine->error;
					return $this->render();
				}
				
				$this->view->packages = $packages;
				$this->view->engine = $engine;
				$engine->push();
				
			//	print_r($engine);
				$this->view->html = $this->component('booking/package-selection');
				$this->view->id = 'booking-panel';
				$this->view->status = true;
				
				applog(count($packags)." packages found, showing options");
			}
			else{
				$this->view->error = "Sorry, this event is not available for bookings";
				applog('Bookings not available for event '.$event->id.', abort');
			}
		}
		
		
		$this->view->status = true;
		$this->render();
		
	}
	
	/**
	* Display booking form
	*/
	public function booking_form (){
		
		$engine = new BookingEngine();
		
		if(!empty($engine->object)){
			$this->view->engine = $engine;
			$this->view->html = $this->component('booking/booking-form');
			$this->view->id = 'booking-panel';
			$this->view->status = true;
		}
		
		$this->render();		
	}
	
	
	/**
	* check_availability for given talent
	*/
	public function check_availability () {
		
		applog('Checking availability request', true);
		
		$engine = new BookingEngine();
		
		if( !empty($_POST['date']) || !empty($_POST['time'])){
			
			$engine->capture();
		}
		$this->view->engine = $engine;
			
		if($engine->canSelectPackage()){
			
			$packages = $engine->checkAvailability();
			
			
		//	print_r($engine->timeline);
			
			if($packages === false){
				$this->view->error = $engine->error;
				applog('package check error :'.$engine->error, true);
				return $this->render();
			}
			
			$this->view->packages = $packages;
			applog('date/time/place present, '.count($packages ).' available', true);
			
			$engine->push();
			
		//	print_r($engine);
			$this->view->html = $this->component('booking/package-selection');
			$this->view->id = 'booking-panel';
			$this->view->status = true;
			
		}
		else{
			// start over
			$this->view->error = $engine->error;
			applog('packages can not be selected error :'.$engine->error, true);
			/*
			$this->view->engine = $engine;
			$this->view->html = $this->component('booking/booking-form');
			$this->view->id = 'booking-panel';
			$this->view->status = true;	
			*/
		}
			
		
				
		$this->render();
	}
	
	/**
	* Select a package for the booking
	*/
	public function select_package () {
		
		applog('Package selection request received', true);
		
		$engine = new BookingEngine();
		
		if(!empty($_POST['package']) && is_numeric($_POST['package'])){
			
			if(!empty($_POST['date']) && !empty($_POST['time'])){
				$engine->capture();	
			}
			
			if($engine->setPackage($_POST['package'])){
				// show summery
				$this->view->engine = $engine;
				$this->view->html = $this->component('booking/booking-summery');
				applog('Package '.$_POST['package'].' available.showing summary');
			}
			else{
				$this->view->error = $engine->error;
				applog('Package '.$_POST['package'].'  not available. '.$engine->error);
			}
			
		}
		elseif($engine->canSelectPackage()){
			
			$packages = $engine->checkAvailability();
			
			if($packages === false){
				$this->view->error = $engine->error;
				return $this->render();
			}
			
			$this->view->packages = $packages;
			
			$engine->push();
			
			$this->view->engine = $engine;
			$this->view->html = $this->component('booking/package-selection');
			applog('Package '.$_POST['package'].'  not available.  Showing '.count($packages).' alt. pckages');

		}
		else{
			// start over
			$this->view->engine = $engine;
			$this->view->html = $this->component('booking/booking-form');
			applog('Package '.$_POST['package'].'  selection failed. Shwoing form again');
			
		}

		$this->view->id = 'booking-panel';
		$this->view->status = true;
		
		$this->render();
	}
	
	/**
	* Get Booking summery page
	*/
	public function booking_summery () {
		
		applog('Getting booking summery', true);
		
		$engine = new BookingEngine();
		$this->view->engine = $engine();
		
		if($engine->canMakeBooking()){
			$this->view->html = $this->component('booking/booking-summery');
			applog('Booking possible, showing summery');
		}
		elseif($engine->canSelectPackage()){
			
			applog('Booking not possible, checking availability');
			
			$packages = $engine->checkAvailability();
			
			if($packages === false){
				$this->view->error = $engine->error;
				return $this->render();
				applog('No packages available : '.$engine->error);
			}
			
			$this->view->packages = $packages;	
			$this->view->html = $this->component('booking/package-selection');
			applog(count($packages).' packages available, showing package selection screen');
		}
		else{
			$this->view->html = $this->component('booking/booking-form');	
			applog('Showing booking form on pkg selection error :'.$engine->error);
		}
		
		$this->view->id = 'booking-panel';
		$this->view->status = true;
		
		$this->render();
		
	}
	

	/**
	* Get booking review screen
	*/
	/*
	private function review_booking () {
		
	
		$this->view->status = false;
		$this->view->message = "Invalid request";
		
		$data = Session::getBooking();
	

		// display page
		if(!empty($_POST['session_id']) && is_numeric($_POST['session_id'])){
			
			$talent = new Talent($_POST['talent_id']);
			$package = new TalentPackage($_POST['session_id']);
			
			if(!$package->ready() || $package->talent_id != $talent->id && !$package->isAvailable()){
				$this->view->error = "Selected package is not available";
				return $this->render();
			}
			
						
			$data = Session::setBooking();
			$this->view->tblm = $data;
			
			// check talent avilability
						
			$datetime = Util::ToSysDate($_POST['b_date'], $_POST['b_time']);
			
			if($talent->isBookingPossible($datetime, $package)){
				
				$this->view->talent = $talent;
				$this->view->package = $package;
				
				$this->view->status = true;
				$this->view->message = NULL;
			//	$this->view->id = "booking";
			$this->view->id = 'booking-panel';
				$this->view->html = $this->component('booking/book-review');
				
			}
			else{
				
				$this->view->html = $this->component('booking/missed');
				$this->view->message = "You just missed it!";
				$this->view->action = 'popup';
				
				$this->view->data = array(
					//'title' => 'Appointment',
					'para' => array( ),
					'mask' => true,
					'buttons' => false
				);
					
			}
			// check dup booking on same time/date
			
			
		
			
			
		}
		
		$this->render();
		
	}
	*/
	
	/**
	* Create a booking from booking engine data
	*/
	private function submit_booking () {
		
		$engine = new BookingEngine();
		
		applog('Booking submit request', true);
		
		if(!$this->auth->isAuthenticated()){
			
			Util::setCookie(DataType::$COOKIE_REDIRECT,'/book/');
			$this->view->action = 'login';
			$this->view->html = $this->component('login-form');	
			
			return $this->render();
		}
		
		if($engine->canMakeBooking()){
			
			$booking = $engine->submit();
			
			if(!empty($booking)){
							
				$this->view->booking = $booking;
				$this->view->engine = $engine;
				$this->view->object = $engine->getObject();
				$this->view->html = $this->component('booking/confirmation');
				
				$this->view->status = true;
				applog('Booking submitted '.$booking->id);

			}
			else {
				$this->view->error = $engine->error;
				$this->view->engine = $engine;
				$this->view->html = $this->component('booking/booking-form');
				$this->view->status = true;
				applog('Can not submit the booking '.$engine->error);
			}
			
		}
		else{
			// show booking form
			$this->view->engine = $engine;
			$this->view->html = $this->component('booking/booking-form');
			$this->view->status = true;	
			applog('Can not make a booking, '.$engine->error);
		}
		
		$this->view->id = "booking-panel";
		
		$this->render();
	}
	
	
	
	
	/**
	* serach talenrs
	*/
	public function search_talents () {
		
		$this->template = 'ajax.php';
		
		//Session::setSearch();

		$engine = new SearchEngine();
		$this->view->engine = $engine;
		//$this->view->buffer = $app->search();
		//$this->view->buffer->_pagesize = 50;
		
		$this->view->status = true;
		
		if($engine->page == 1) {
			$this->view->id = 'search-result-container';
			$this->view->html = $this->component('search_results');		
		}
		else{
			$this->view->id = 'search-results';
			$this->view->append = $this->component('search_paging');
			
		}
		
		$catname = $engine->getCategoryName ();
		if(!empty($catname)) {
			$this->view->page = array(
					'title' => 'Book a '.$catname, 
					'url' => '/search/'.urlencode(strtolower($catname)).(!empty($engine->city) ? '?q='.strtolower($engine->city) : '')
				 );			
		}
		
		
		
		
		$this->render();


	}
	
	
	
	/**
	* Check date before make an appinment
	* request comming from profile page
	*/
	public function check_date () {
		
		
		if(empty($_POST['profile']) || !is_numeric($_POST['profile'])){
			$this->view->error = "Invalid Talant. Booking not possible";
			return $this->render();
		}
		
		
		$object = new Profile($_POST['profile']);
		
		
		
		if(empty($object) || $object->isDeleted() || $object->active == 0){
			$this->view->error = "Invalid talent or agency";
			return $this->render();
		}
						
		if(empty($_POST['date']) && empty($_POST['time'])){
			$this->view->error = "Please slect date and time";
			return $this->render();
		}
		
		$engine = new BookingEngine();
		
		if(!$engine->setObject($object)){
			$this->view->error = $engine->error;
			return $this->render();
		}		
		
		$engine->capture();
		$engine->push();
		
		// show form to select place
		$this->view->engine = $engine;
		$this->view->html = '<div class="profile-strip" id="booking-panel">'.$this->component('booking/booking-form')."</div>";		
		$this->view->id = 'page-booking';
		$this->view->status = true;
		
		
		$this->render();
	}
	
	
	/**
	* Submit an event data for booking
	*/
	public function event_submit () {
		
		
		if(!empty($_POST['booking_id']) && is_numeric($_POST['booking_id'])){
			
			$booking = new Booking($_POST['booking_id']);
			
			if($booking->customer_id == $this->user->id){
				
				if(!empty($_POST['event_id']) && is_numeric($_POST['event_id'])){
					
					$event = new Event($_POST['event_id']);
					
					if($event->customer_id == $this->user->id){
						
						$booking->event_id = $event->id;
						$booking->save();
						
						$this->view->status = true;
						// show confirmation
						
						$this->view->booking = $booking;
						$this->view->id = 'booking-panel';
						$this->view->status = true;
						$this->view->html = $this->component('booking/confirmation');
						$this->view->message = "Thank You!";
		
						
					}
					else{
						$this->view->error = "Invalid event";		
					}
					
				}
				else{
					
					if(empty($_POST['event_type']) && !is_numeric($_POST['event_type'])){
						$this->view->error = "Invalid event type";	
						$this->render();	
					}
					
					if(empty($_POST['event_name'])){
						$this->view->error = "Invalid event name";	
						$this->render();	
					}
					
					$event = new Event();
					$event->customer_id = $this->user->id;
					//$event->agency_id = Validator::cleanup($_POST['event_agency_id'], 100);
					$event->event_type = Validator::cleanup($_POST['event_type'], 5);
					$event->name = Validator::cleanup($_POST['event_name'], 400);
					$event->begins = $booking->session_start;
					$event->ends = $booking->session_ends;
					
					if(!$booking->isEmpty('venue')){
						$event->venue = $booking->venue;	
					}
					
					$event->location_id = $booking->location_id;
					$event->save();
					
					$booking->event_id = $event->id;
					$booking->save();
					
					// show confirmation
					$this->view->booking = $booking;
					$this->view->id = 'booking-panel';
					$this->view->status = true;
					$this->view->html = $this->component('booking/confirmation');
					$this->view->message = "Thank You!";
				}
			}
			else{
				$this->view->error = "Unauthorized access";	
			}
			
		}
		
		
		$this->render();
	}
	
	/**
	* Show booing confirmation page
	*/
	public function booking_confirmation () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$booking = new Booking($_POST['id']);
			
			if($booking->customer_id == $this->user->id){
				
				$this->view->booking = $booking;
				$this->view->id = 'booking-panel';
				$this->view->status = true;
				$this->view->html = $this->component('booking/confirmation');
				$this->view->message = "Thank You!";
				$this->render();				
			}
			
		}		
	}
	
	
	/**
	* Add or remove fav list
	*/
	public function like () {

		if($this->auth->isAuthenticated()){
			
			if(!empty($_POST['id']) && is_numeric($_POST['id'])){
				
				$res = $this->user->updateLike($_POST['id']);
				$this->view->data = array( 'id' => $_POST['id'], 'action' =>($res) ? 1 : 0);
				$this->view->status = true;
				$this->view->action = "favorite_update";	
			}
			else{
				$this->view->error = "Invalid request";	
			}
		}
		else{
			// popup login form
			Util::setCookie(DataType::$COOKIE_REDIRECT,'/search');
			$this->view->action = 'login';
			$this->view->html = $this->component('login-form');	
		}	
		
		$this->render();
	}
	
	/**
	* IP to locatio Data	
	*/
	public function get_location () {
	
		$ip = $_SERVER['REMOTE_ADDR'];
		
		if($ip != '127.0.0.1'){
		
			$this->view->data = $data = Util::IP2Location($ip);
			$this->view->message = 'remote';
			$this->view->status = true;
		}
		else{
			$this->view->status = true;
			$this->view->message = 'localhost';
			$this->view->data =array('country_code' => 'LK');	
		}
		
		$this->render();
		
	}
	
	/**
	* verify and Create a new device for customer
	*/
	public function device () {
		
	
		if($this->auth->isAuthenticated() && !empty($_POST['device'])){	
		
			$fingureprint = isset($_POST['dfp']) ? $_POST['dfp'] : NULL;
			$deviceinfo = json_decode($_POST['device'], true);

			if(empty($fingureprint)){
				// suspect situartion
				// send to login again
				$this->auth->logout();
				$this->view->action = 'redirect';
				$this->view->data = '/login';
				return $this->render();
			}
			
			$device = $this->user->getDevice($fingureprint);
			
			if(empty($device) && !empty($deviceinfo)){
				// add device
				$device = Util::getDevice();
				$device->customer_id = $this->user->id;
				$device->save();
				
				$this->auth->logout();
				$this->view->action = 'redirect';
				$this->view->data = '/login';
				return $this->render();
			}
			
		}
		else{
			$this->view->action = 'redirect';
			$this->view->data = '/login';	
		}
		
		
		$this->render();
	}
	
	/**
	* Read category data
	*/
	public function get_categories () {
		
		$this->view->data = array(
			'skills' => App::getSkills(),
			'agencies' => App::getAgencyTypes()
		);
		$this->view->status = true;
		$this->view->action = 'bio_start';
		$this->render();
		
	}
	
	/**
	* Login error
	*/
	public function error_report (){
		
		if(!empty($_POST['request']) && isset($_POST['response'])){
			
			$errorlog = new ErrorLog();
			
			if(!empty($this->user)){
				$errorlog->customer_id = $this->user->id;
			}
			
			$errorlog->request = Validator::cleanup($_POST['request'], 10000);
			$errorlog->response = Validator::cleanup($_POST['response'], 10000);
			$errorlog->save();
			
			if(empty($this->user)){
				
				$this->view->data = array(
					'title' => 'Unexpected error occured',
					'message' => '<div class="padding max-600">System found an unexpected error while processing your request.  It has already reported to engineers. <br><br>We apologize about the inconvenience caused. If you would like to our support team to get in touch with you, please enter your phone number or email.
					<div class="field-wrapper"><input data-id="'.$errorlog->id.'"  id="user_contact" type="text" maxlength="100" class="input-short"></div>
					</div>',
					'buttons' => array('ok' => array('label' => 'OK', 'action'=>'error_contact'))
				);
			}
			else{
				
				$this->view->data = array(
					'title' => 'Unexpected error occured',
					'message' => '<div class="padding max-600">System found an unexpected error while processing your request.  It has already reported to engineers. <span class="txt-bold">Error code is: '.$errorlog->id.' </span><br><br>We apologize about the inconvenience caused. Our support team will call you soon.<br><br>Thanks!</div>',
					'buttons' => array('ok' => array('label' => 'Close', 'class'=>'button-alt'))
				);
			}
			
			$this->view->action = 'dialog';
			$this->status = true;
			
			
		}
		$this->render();		
	}
	
	
	
	/**
	* Shwoing currency selection form
	*/
	public function currency_select () {
		
		$dialog = array(
			'title' => 'Select Currency',
			'message' => $this->component('customer/currency_selector'),
			'mask' => true,
			'buttons' => [
				'ok' => ['label' => 'Change','action' => 'currency_update'],
				'cancel' => ['label' => 'cancel', 'class' => 'button-alt']
			],
			'width' => 400
		);

		$this->view->action = 'dialog';
		$this->view->data = $dialog;
		$this->view->status = true;
		
		$this->render();
	}
	
	
	/**
	* Updating currency
	*/
	public function currency_update () {
		
		if($_POST['currency'] && isset(DataType::$currencies[$_POST['currency']])){
			
			Session::currency($_POST['currency']);
			$this->view->action = 'reload';
			
		}
		
		$this->render();
	}
	
	
	/**
	* Show packages in user currency
	*/
	public function packages_show () {
		
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$this->view->profile = new Profile($_POST['id']);
			$this->view->id = 'package_container';
			$this->view->html = $this->component('profile_packages');
			$this->view->status = true;
		}
		
		$this->render();
	}
	
	/*
	* Collect contact data
	*/
	public function error_contact () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id']) && !empty($_POST['contact'])){
			
			$errorlog = new ErrorLog($_POST['id']);
			$errorlog->contact = Session::getCountry()."|".Validator::cleanup($_POST['contact'], 100);
			$errorlog->save();
			
			$this->view->message = "Thank you!";
		}
		
		$this->render();
		
	}
	
	/**
	* Get Tags
	*/
	public function get_tags () {
		
		$this->view->data = App::getTags();
		$this->view->action = 'cura_tags';
		$this->render();
		
	}
	
	/***************** Override render function for easy access *************/
	
	public function  render ($view_path = '') {
		
		parent::render('/common/void');
		
	}
}