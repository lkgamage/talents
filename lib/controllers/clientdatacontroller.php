<?php
/**
* User must be logged in to access this data
*/

class ClientDataController extends Controller {
	
	
	function __construct ($request = NULL) {
					
		 parent::__construct($request);
		 
		 $this->template = 'ajax.php';

		 if($this->auth->isAuthenticated()) {
		 		 
			 array_shift($this->data);
			 
			 if(!empty($this->data[0]) && method_exists($this, $this->data[0])){
				 
				 $method = array_shift($this->data);
				 $this->$method();
			 }
			 else{
				$this->view->error = "Method not found";
				$this->render(); 
			 }
		 }
		 elseif($this->data[1] == 'feed'){
			 array_shift($this->data);
			 $method = array_shift($this->data);
			 $this->$method();
		 }
		 else{
			$this->view->error = "Authentication required";
			$this->render(); 
		 }
		
	}
	
	/**
	* Confirm customer city , time and currency
	*/
	public function ctl_confirm () {
		
		$this->user->ct_confirmed = 1;
		$this->user->save();
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/dashboard');
		$this->view->status = true;
		$this->view->message = 'Thank you!';
		
		
		$this->render();
	}
	
	
	/**
	* Show edit forms based on customer selections
	*/
	public function customer_edit () {
		
		if(!empty($_POST['profile'])) {
			
			
			applog("Profile edit request received", true);
					
			$session = $this->pickProfile();
			$object = new Profile($session['id']);
			
			if($object->customer_id != $this->user->id){
				
				applog("User {$this->user->id} has no permision to edit profile {$object->id}, abort");
				
				$this->view->error = "Unauthorized access";	
				return  $this->render();
			}			
						
		}
		else{
			$object = $this->user;
		}
	
		$part = $_POST['_action'];
		
		$this->view->status = true;
		$this->view->id = $part;
		$this->view->object = $object;
		
		if($part == 'customer_info_edit'){
			
			if(!empty($_POST['profile'])) {
				
				if($object->is_agency == 0){
					$this->view->html = $this->component('talent/info');
				}
				else {
					$this->view->html = $this->component('agency/info');
				}
				
			}
			else {
				$this->view->html = $this->component('customer/info');
			}
		}
		elseif($part == 'customer_city_edit'){
			$this->view->html = $this->component('customer/city');	
			$this->view->id = 'customer_location';
		}
		elseif ($part == 'customer_tz_edit'){
			$this->view->html = $this->component('customer/tz');
			$this->view->id = 'customer_timezone';	
		}
		elseif ($part == 'customer_currency_edit'){
			$this->view->html = $this->component('customer/currency');
			$this->view->id = 'customer_currency';	
		}
		
		
		$this->render(); 
	}
	
	/**
	* Update customer info
	* This informationcan be updtaed only once
	*/
	public function customer_info_update(){
		
		if(!empty($_POST['profile'])) {
					
			$session = $this->pickProfile();
			
			if(empty($session)){
				
				$profiles = $this->user->getProfiles();
				
				if(count($profiles) == 1){
					$object = $profiles[0];
					applog("No session, Auto profile {$object->id} selected");
					
				}
				else {
					applog("There are  {count($profiles)}, asking user to choose");
					$this->view->error = "Please select a profile";	
					return  $this->render();
				}				
			}
			else{
				$object = new Profile($session['id']);
			}
			
			if($object->customer_id != $this->user->id){
								
				applog("User {$this->user->id} has no permision to edit profile {$object->id}, abort");
				
				$this->view->error = "Unauthorized access";	
				return  $this->render();
			}
		}
		else{
			$object = $this->user;
		}
		
				
		if(!empty($_POST['profile'])) {
			
			if($object->isAgency()){
				
				$agencyname = !empty($_POST['agency_name']) ? $_POST['agency_name'] : NULL;
				$agencytype = !empty($_POST['agency_type']) ? $_POST['agency_type'] : NULL;
				
				if(empty($agencyname)){
					$this->view->error = "Please enter agency name";	
				}
				elseif(empty($agencytype) && !is_numeric($agencytype)){
					$this->view->error = "Please select agency type";	
				}
				else{
					
					$agencyname = Validator::cleanup($agencyname,	400);
					
					if($agencyname != $object->name) {
					
						$object->name = $agencyname;
						$object->name_changed = 1;
						
					}
					
					$object->agency_type = $agencytype;
					$object->save();
					
					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'profile';
					$this->view->message  = "Agency name updated";
					
					return $this->render();
				}
				
			}
			else{
				
				$displayname = !empty($_POST['display_name']) ? $_POST['display_name'] : NULL;
				if(empty($displayname)){
					$this->view->error = "Please enter display name";	
				}
				else{
					
					$displayname = Validator::cleanup($displayname,100);
					
					if($displayname != $object->name) {
					
						$object->name = $displayname;
						$object->name_changed = 'NOW';
						$object->save();
					}
					
					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'profile';
					$this->view->message  = "Talent name updated";
					
					return $this->render();						
					
				}
				
				
			}				
			
		}
		else {
			
			if($object->name_changed == 0){
		
				// upodate customer info
			
				$firstname = !empty($_POST['user_firstname']) ? $_POST['user_firstname'] : NULL;
				$lastname = !empty($_POST['user_lastname']) ? $_POST['user_lastname'] : NULL;
	
				$displayname = !empty($_POST['display_name']) ? $_POST['display_name'] : NULL;
				
				$gender = !empty($_POST['gender']) ? $_POST['gender'] : NULL;
				
				if(!empty($_POST['dob_month']) && !empty($_POST['dob_day']) && !empty($_POST['dob_year']) ){
					if(Validator::isDate($_POST['dob_year'], $_POST['dob_month'], $_POST['dob_day'])){
						$dob = $_POST['dob_year'].'-'.$_POST['dob_month'].'-'.$_POST['dob_day'];
					}
				}
				
				if(empty($firstname) || empty($lastname)){
					$this->view->error = "Please enter your first and last names";	
				}
				elseif(empty($dob)) {
					$this->view->error = "Invalid date of birth";
				}
				elseif(empty($gender)){
					$this->view->error = "Please select your gender";	
				}
				else{
					
					$this->user->firstname = Validator::cleanup($_POST['user_firstname'], 100);
					$this->user->lastname = Validator::cleanup($_POST['user_lastname'], 100);
					
					$this->user->dob = $dob;
					
					if(!empty($gender) && is_numeric($gender)){
						$this->user->gender = $gender%2;
						$this->user->is_trans = $gender > 2 ? 1 : 0;
					}
					$this->user->name_changed = 1;
					$this->user->save();
					
					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'account';
					$this->view->message  = "Personal detail updated";
				}
			
			
			}
			else{
				$this->view->message = "You have already updated your personal details. <br>Please contact customer support to make any changes.";
				$this->view->action = 'alert';	
			}
		}	
		
		$this->render(); 	
	}
	
	/**
	* Update customer settings
	*/
	public function customer_setting_update () {
		
		$success = false; 
		
		if(!empty($_POST['profile'])) {
			
			applog("Profile setting update request", true);
			
			$session = $this->pickProfile();
			
			if(empty($session)){
				
				$profiles = $this->user->getProfiles();
				
				if(count($profiles) == 1){
					$object = $profiles[0];
					applog("No session, Auto profile {$object->id} selected");
					
				}
				else {
					applog("There are  {count($profiles)}, asking user to choose");
					$this->view->error = "Please select a profile";	
					return  $this->render();
				}				
			}
			else{
				$object = new Profile($session['id']);
			}
			
			if($object->customer_id != $this->user->id){
								
				applog("User {$this->user->id} has no permision to edit profile {$object->id}, abort");
				
				$this->view->error = "Unauthorized access";	
				return  $this->render();
			}
			
			
		}
		else{
			$object = $this->user;
		}
		
				
		if(isset($_POST['user_city'])){
		
			if(empty($_POST['country']) || empty($_POST['city'])){
				$this->view->error = "Please enter current city";
			}
			else{
				$location = $object->getLocation();	
							
				if($location->country != $_POST['country']){
					$this->view->error = "If you want to change the country,<br>please contact customer support.";	
				}
				else{					
					$location = Util::saveLocation($location);
					$success = true;
					
					$this->view->message = "Current city updated";	
				}
			}
			
		}
		elseif (!empty($_POST['timezone'])){
			
			if(!empty(DataType::$timezones[$_POST['timezone']])) {
							
				$tz = DataType::$timezones[$_POST['timezone']];
				$object->timezone = $_POST['timezone'];
				$object->timeoffset = $tz[2];
				$object->save();
				
				Util::setCookie(DataType::$COOKIE_TIMEZONE, $tz[2]);
			
				$this->view->message = "Time zone updated";
				$success = true;
			}
			else{
				$this->view->error = "Invalid time zone";	
			}
		}
		elseif(!empty($_POST['currency'])){
		
			if(isset($_POST['currency']) && isset(DataType::$currencies[$_POST['currency']]) ) {
				
				$object->currency = $_POST['currency'];
				$object->save();
				
				Session::currency($object->currency);
				
				$success = true;
			}
			else{
				$this->view->error = "Invalid Currency";	
			}
		}
		
		
		if($success){
			$this->view->status = true;
			$this->view->action = 'recent';
			$this->view->message  = "Personal detail updated";	
		}
		
		$this->render(); 
	}
	
	/**
	* Edit phone
	*/
	public function phone_edit () {
		
		if(!empty($_POST['id'])){
			
			$phone = new Phone($_POST['id']);
			
			if($phone->customer_id == $this->user->id){
				$this->view->phone = $phone;	
				$this->view->html = $this->component('customer/phone');
				$this->view->id = 'phone_edit_'.$phone->id;	
			}
			else{
				$this->view->error = "Unauthorized access";	
			}
		}
		else{
			
			$this->view->type = 'p';
			
			$dialog = array(
				'title' => 'Add New Phone',
				'message' => $this->component('customer/add_contact'),
				'mask' => true,
				'buttons' => [
					'ok' => ['label' => 'Add', 'action' => 'phone_update'],
					'cancel' => ['label' => 'Cancel', 'class' => 'button-alt', ]
				],
				'width' => 400
			);
			$this->view->action = 'dialog';
			$this->view->data = $dialog;
			$this->view->status = true;
		}
		
		$this->render();	
	}
	
	/**
	* Update phone number
	*/
	public function phone_update () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(!empty($_POST['phone_phone'])){
			
			$co = Session::getCountry();
			
			// cleanup phone number
			$phonenumber = Validator::cleanupPhoneNumber($_POST['phone_phone'], $co);
			
			if(!is_numeric($phonenumber) || strlen($phonenumber) < 4 || strlen($phonenumber) > 14 ){
				$this->view->error = "Invalid phone number";
				return $this->render();
			}
			
			$mobile = new phone($co, $phonenumber);
			if($mobile->ready() && !$mobile->isDeleted() && $mobile->customer_id != $this->user->id){
				$this->view->error = '+'.DataType::$countries[$co][1].' '.$phonenumber." already associated with another account";
				return $this->render();
			}
						
			if(!empty($_POST['id']) && is_numeric($_POST['id'])){
				
				$phone = new Phone($_POST['id']);
				
				if($phone->customer_id == $this->user->id){
															
					if($phone->is_verified == 1){
						$this->view->error = "Verified phone number can not be updated";
						return $this->render();
					}
					
					$phone->phone = Validator::cleanup($phonenumber, 20);				
					$phone->save();
					
					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'account';
					$this->view->message  = "Phone updated";	
					
				}
				else{
					$this->view->error = "Unauthorized access";
				}
			}
			else{				
				applog("Adding new phone numebr {$phonenumber}");
				
				$phone = new Phone();
				$phone->customer_id = $this->user->id;
				$phone->type = DataType::$PHONE_TYPE_MOBILE;
				$phone->phone = Validator::cleanup($phonenumber, 20);
				$phone->country_code = $co;
				$phone->is_public = 0;				
				$phone->sms = 1;
				$phone->whatsapp = 0;
				$phone->save();
				
				applog("Phone numebr {$phonenumber} added as {$phone->id}");
				
				// create login record with this phone number
				$login = new Login();
				$login->customer_id = $phone->customer_id;
				$login->type = 'p';
				//$login->ref_id = Validator::cleanup($_POST['login_ref_id'], 100);
				//$login->email_id = Validator::cleanup($_POST['login_email_id'], 100);
				$login->phone_id = $phone->id;
				$login->save();
				applog("Login record {$login->id} created with the phone number ");
				
				$this->view->status = true;
				$this->view->action = 'get_dash_page';	
				$this->view->data = 'account';
				$this->view->message  = "Phone number added";
				
			}
			
		}
		
		$this->render();	
	}
	
	
	/**
	* Make a phone number or email primary
	*/
	public function make_primary () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(!empty($_POST['id']) && is_numeric($_POST['id']) && !empty($_POST['ref'])){
			
			//$email = new Email($emailaddress);
			if($_POST['ref'] == 'e'){
				$object = new Email($_POST['id']);
			}
			else{
				$object = new Phone($_POST['id']);
			}
			
			
			if($object->customer_id == $this->user->id){
				
				$com = new ComEngine();
				
				$code = App::sendOTP($_POST['ref'], $object->id, $this->user->id);	
				
				if($code !== false){
					
					if($_POST['ref'] == 'e'){
						$com->otpEmail($this->user, $object, $code->code );
					}
					else{
								// send OTP SMS
						$com->otpSMS($this->user, $object, $code->code);
					}
					$this->view->cura = 'set_primary';
					$this->view->type = $_POST['ref'];
					$this->view->code = $code;
					$dialog = array(
						'title' => 'Authorization code',
						'message' => $this->component('customer/otp'),
						'mask' => true,
						'buttons' => [
							'ok' => ['label' => 'Confirm', 'action' => 'set_primary'],
							'cancel' => ['label' => 'Cancel', 'class' => 'button-alt', ]
						],
						'width' => 400
					);
					$this->view->action = 'dialog';
					$this->view->data = $dialog;
					$this->view->status = true;
						
					
					
				}
				else{
					$this->view->message = App::$error;
					$this->view->error = App::$error;
				}
				
			}
			else{
				$this->view->error = "Unauthorized request";
			}
			
		}
		else{
			$this->view->error = "Invalid request";
		}
		
		$this->render();
	}
	
	
	/**
	* Set given email or phone as primary
	*/
	public function set_primary () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(!empty($_POST['code_id']) && is_numeric($_POST['code_id']) && !empty($_POST['code'])){
			
			$code = new Code($_POST['code_id']);
			$value = Validator::cleanup($_POST['code'], 6);
			
			if($code->ready() && $code->customer_id == $this->user->id){
				
				if($code->validate($value)){
				
					$phone = $code->getPhone();
					if(!empty($phone)){
						$phone->makePrimary();
					}
					else{
						$email = $code->getEmail();
						$email->makePrimary();
					}
					
					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'account';
					$this->view->message  = "Updated";
					
				}
				else{
					
					if($code->attempts < 3){
						
						$this->view->cura = 'set_primary';
						$this->view->type = $code->isEmpty('phone_id') ? 'e' : 'p';
						$this->view->code = $code;
						$dialog = array(
							'title' => 'Authorization code',
							'message' => $this->component('customer/otp'),
							'mask' => true,
							'buttons' => [
								'ok' => ['label' => 'Confirm', 'action' => 'set_primary'],
								'cancel' => ['label' => 'Cancel', 'class' => 'button-alt', ]
							],
							'width' => 400
						);
						$this->view->action = 'dialog';
						$this->view->data = $dialog;
						$this->view->status = true;
					}
					else{
						$this->view->error = "Invalid OTP code<br>Please try again later";
					}
				}
				
			}
			else{
				$this->view->error = "Invalid code, Please try again";
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Delete a phone number
	*/
	private function phone_delete () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			
			$phone = new Phone($_POST['id']);
				
				if($phone->customer_id == $this->user->id){
					
					if($phone->is_primary == 1){
						$this->view->error = "Primary number can not be deleted";
						return $this->render();;	
					}
					
					// remove login
					$login = new Login();
					
					if($login->lookup($phone->id, 'p')){
						$login->delete();
					}
					
					$phone->delete();
					
					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'account';
					$this->view->message  = "Phone removed";
					
				}
				else{
					$this->view->error = "Unauthorized access";
				}
			
		}
		
		$this->render();
		
	}
	
	/**
	* Verify email or phone 
	*/
	public function verify_contact () {
	
		if(!empty($_POST['id']) && !empty($_POST['type'])) {
			
			if($_POST['type'] == 'e'){
				$obj = new Email($_POST['id']);	
				$this->view->contact = $obj->email;
			}
			else{
				$obj = new Phone($_POST['id']);
				$this->view->contact = $obj->phone;
			}
			
			$code = App::sendOTP($obj, $_POST['type']);
			
			if(empty($code)){
				
				$codeid = Util::getCookie(DataType::$COOKIE_CODE);
				
				
				if(!empty($codeid)){
					$code = new Code($codeid);
				}
				else{
					$this->view->error = App::$error;
					return $this->render();
				}
				
			}
			
			$this->view->type = $_POST['type'];
			$this->view->code = $code;
			
			$dialog = array(
				'title' => 'Authorization code',
				'message' => $this->component('/customer/otp'),
				'mask' => true,
				'buttons' => false,
				'width' => 400
			);
			$this->view->action = 'dialog';
			$this->view->data = $dialog;
			$this->view->status = true;
		}
		else{
			$this->view->error = 'Bad request';	
		}
		$this->render();
	}
	
	/**
	* Validate token sent from to teh phone or email
	*/
	
	public function validate_auth () {
		
		
		if(!empty($_POST['vcode']) && !empty($_POST['token_id']) && is_numeric($_POST['token_id'])){
			
			$code = new Code($_POST['token_id']);
			
			if($code->validate($_POST['vcode'])){
				
				$obj = $code->getObject();
				$obj->is_verified = true;
				$obj->save();
				
				$un = $code->type == 'p' ? DataType::$countries[$obj->country_code][1].$obj->phone : $obj->email;
				
				$login = new Login();
				if($login->lookup($un)){
					// verify existing login
					if($login->is_verified == 0){
						$login->is_verified = 1;
						$login->save();	
					}
						
				}
				else {
					// creates new login
					$login->customer_id = $this->user->id;
					$login->type = $code->type;
					$login->username = $un;
					$login->password_login = 0;						
					$login->is_verified = 1;
					$login->save();	
				}
				
				$this->view->status = true;
				$this->view->action = 'contact_auth_completed';
				$this->view->message =  ($code->type == 'p' ? 'Mobile' : 'Email' )." verified";
					
			}
			else{
				$this->view->error = "Code is not valid. Try again.";	
			}
		}
		
		$this->render();
	}
	
	/***
	* Edit phone_preferences
	*/
	public function phone_preferences () {
		
		
		$this->view->phones = $this->user->getPhones();
		$this->view->html = $this->component('/customer/phone_preferences'); 
		$this->view->id = 'phone_preferences';
		
		$this->render(); 
		
	}
	
	/**
	* Show email update form
	*/
	public function email_edit () {
		
		if(!empty($_POST['id'])){
			
			$email = new Email($_POST['id']);
			
			if($email->customer_id == $this->user->id){
				$this->view->email = $email;
			}
			else{
				$this->view->error = "Unauthorized access";
				return $this->render();	
			}
			
			$this->view->id = 'email_edit_'.$email->id;
		}
		else{
			$this->view->type = 'e';

			$dialog = array(
				'title' => 'Add New Email',
				'message' => $this->component('customer/add_contact'),
				'mask' => true,
				'buttons' => [
					'ok' => ['label' => 'Add', 'action' => 'email_update'],
					'cancel' => ['label' => 'Cancel', 'class' => 'button-alt', ]
				],
				'width' => 400
			);
			$this->view->action = 'dialog';
			$this->view->data = $dialog;
			$this->view->status = true;
		}
		
		$this->view->html = $this->component('/customer/email'); 
		
		
		$this->render(); 
		
	}
	
	/**
	* Update email address
	*/
	public function email_update () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(!empty($_POST['email_email'])){
			
			if(!empty($_POST['id'])){
				$email = new Email($_POST['id']);
				
				if($email->customer_id == $this->user->id){
					if($email->email != $_POST['email_email']){
						$email->is_verified = 0;	
					}
				}
				else{
					$this->view->error = "Unauthorized access";
					return $this->render();	
				}		
				
			}
			else{
				$email = new Email($_POST['email_email']);
				
				if($email->ready() && !$email->isDeleted()){
					$this->view->error = "Email already associated with an account";
					return $this->render();
				}
				
				$email = new Email();
				$email->customer_id = $this->user->id;	
				
				$ex = $this->user->getEmails();
				
				if(empty($ex)){
					$email->is_primary = 1;	
				}
			}
			
			$email->email = Validator::cleanup($_POST['email_email'], 100);
			$email->save();
			
			$this->view->status = true;
			$this->view->action = 'get_dash_page';	
			$this->view->data = 'account';
			$this->view->message  = 'Preference  updated';
			
		}
		
		$this->render();
	}
	
	
	/**
	* delete an email address
	*/
	public function email_delete () {
		
		
		if(!empty($_POST['id'])){
			$email = new Email($_POST['id']);
			
			if($email->customer_id == $this->user->id){
				
				if($email->is_primary == 1){
					$this->view->error = "Primary email address can not be removed";
					return $this->render();
				}
				
								
				// remove login
				$login = new Login();
				
				if($login->lookup($email->email, 'e')){
					$login->delete();
				}
				
				$email->delete();
				
				$this->view->status = true;
				$this->view->action = 'get_dash_page';	
				$this->view->data = 'account';
				$this->view->message  = "Email removed";
				
			}
			else{
				$this->view->error = "Unauthorized access";
				return $this->render();	
			}		
			
		}
		else{
			$this->view->error = "Bad request";	
		}
			
		$this->render();	
	}
	
	/**
	* Update primapry phone preferences
	*/
	public function phone_preferences_update () {
		
		if(isset($_POST['public']) && !empty($_POST['private']) && is_numeric($_POST['public']) && is_numeric($_POST['private'])  ){
			
				$this->user->update_phone_preference($_POST['private'],$_POST['public']); 
				$this->view->status = true;
				$this->view->action = 'get_dash_page';	
				$this->view->data = 'account';
				$this->view->message  = 'Preference  updated';
		}
		else{
			$this->view->error = "Bad request";	
		}
		
		$this->render(); 
	}
	
	/**
	* Swicth profiles
	*/
	public function switch_profile () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Profile switching requesr recived from {$this->user->id}", true);
		
		if(isset($_POST['customer']) && is_numeric($_POST['customer'])){
			Session::clearProfile();
			Session::setCustomer($this->user);
			$this->view->action = 'redirect';
			$this->view->data = '/dashboard';
			applog("Profile switched customer");
		}
		elseif(isset($_POST['profile']) && is_numeric($_POST['profile'])){
			
			$profile = new Profile($_POST['profile']);
			
			if($profile->ready() && $profile->customer_id == $this->user->id){
				Session::setProfile($profile);
				$this->view->action = 'redirect';
				$this->view->data = '/dashboard';
				applog("Profile switched {$profile->id}");
			}			
			else{
				applog("Profile  {$profile->id} does not belongs to {$this->user->id}, abort");
				$this->view->error = "Unauthorized request";	
			}
		}	
		else{
			applog("Switch to type is not defined, abort");
			$this->view->error = "Bad request";	
		}
		
		
		$this->render(); 
	}
	
	
	public function packages_public () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		$profile = $this->pickProfile();
		
		if(!empty($profile)){
							
			$object = new Profile($profile['id']);

			if($object->customer_id != $this->user->id){
				$this->view->error  = "You have no permission to perform this action";
				return $this->render();
			}

			$page = $object->getPage();
			$page->show_packages = !empty($_POST['enable']) ? 1 : 0;
			$page->save();

			$this->view->message = !empty($_POST['enable']) ? "Packages are showing in public page" : "Packages are hidden in public page";
			$this->view->status = true;
			
			applog("Profile {$object->id} has changed package visibility ", true);
			
		}
		
		$this->render();
		
	}
	
	/**
	* Show edit form for packages
	*/
	public function package_edit () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog('Package edit request recived', true);
		
		$profile = $this->pickProfile();
		
		if(!empty($profile)){
			
			applog("Proceeding with profile {$profile['id']}");
		
			$this->view->object = new Profile($profile['id']);

			if(!empty($_POST['id']) && is_numeric($_POST['id'])){
				
				$package = new  TalentPackage($_POST['id']);
								
				if($package->ready()){
					
					$category = $package->getCategory();
					
					if($category->profile_id != $profile['id']){
						applog("User has no permission to update {$package->id} package in {$category->id}, abort");
						
						$this->view->error = "You have no permission to modify this package";	
						return $this->render();
					}
					
					$this->view->package = $package;					
				}
				else{
					applog("invalid package id {$package->id}, proceed as a new package");
				}				
				
			}
			elseif(!empty($_POST['skill']) && is_numeric($_POST['skill'])){
				$skill_id = $_POST['skill'];
				
				$category = $this->view->object->hasCategory($skill_id);
				
				if(empty($category)){
					applog("Profile {$profile['id']} has no category for {$skill_id}, abort");
					
					$this->view->error = "You have no subscription for given category<br>Please subscribe first.";	
					return $this->render();
				}
				
				$this->view->skill_id = $skill_id;
			}

			$this->view->id = 'dash-contents';
			$this->view->html = $this->component('package/package_form');
			$this->view->status = true;
			
		}	
				
		$this->render();
		
	}
	
	/**
	* Create or update package
	*/
	public function package_update () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Package save request received", true);
		
		if(!empty($_POST['package_category_id']) && is_numeric($_POST['package_category_id']) && !empty($_POST['package_name']) && !empty($_POST['package_description'])){
		
			$profile = $this->pickProfile();
			
			if(!empty($profile)){
				
				$object = new Profile($profile['id']);
				
				$category = new Category($_POST['package_category_id']);
				
				if($object->customer_id != $this->user->id){
					$this->view->error = "Unauthorized access";	
					return $this->render();
				}
				
				if(!$category->ready() || $category->profile_id != $object->id){
					applog("Profile {$profile['id']} has no category for {$category->id}, abort");
					
					$this->view->error = "You have no subscription for given category<br>Please subscribe first.";	
					return $this->render();
				}				

				if(!empty($_POST['package_timespan']) ){
					$timesmap = Util::ToMinutes($_POST['package_timespan']);
					if($timesmap <= 0){
						$this->view->error = "Invalid session duration";	
						return $this->render();
					}
					if($timesmap > 1440*30){
						$this->view->error = "We do not support sessions longer than 30 days";	
						return $this->render();
					}
				}
				else{
					$this->view->error = "Please enter valid session time";	
					return $this->render();				
				}

				if($_POST['package_interval']){

					$interval = Util::ToMinutes($_POST['package_interval']);

					if($interval < 0){
						$this->view->error = "Invalid interval";	
						return $this->render();
					}
					if($interval > 1440){
						$this->view->error = "Maximum interval should be one day. <br>You may create time-off appointments for additional days";	
						return $this->render();
					}
				}
				else{
					$interval = 0;
				}


				if(!empty($_POST['package_fee']) && is_numeric($_POST['package_fee'])){
					$amount = $_POST['package_fee'];

					if($amount <= 0){
						$this->view->error = "Invalid fee amount";	
						return $this->render();
					}

				}
				else{
					$this->view->error = "Please enter package fee";	
					return $this->render();
				}

				if(!empty($_POST['package_id'])){
					
					$package = new TalentPackage($_POST['package_id']);

				}
				else{
					$package = new TalentPackage();
					
				}
				
				$package->category_id = $category->id;

				//$package->skill_id = Validator::cleanup($_POST['package_skill_id'], 10);
				$package->active = !empty($_POST['package_active']) ? 1 : 0;
				$package->fullday = !empty($_POST['package_fullday']) ? 1 : 0;
				$package->is_private = !empty($_POST['package_is_private']) ? 1 : 0;
				$package->name = Validator::cleanup($_POST['package_name'], 100);
				$package->description = Validator::cleanup($_POST['package_description'], 1000);

				 if(!empty($_POST['package_items']) && is_numeric($_POST['package_items']) && $_POST['package_items'] > 0) {
					$items = (int)$_POST['package_items'] < 65000 ? $_POST['package_items'] : 65000;

					$package->items = Validator::cleanup($_POST['package_items'], 6);
				 }

				$package->timespan = $timesmap;
				$package->interval = $interval;
				$package->fee = Validator::cleanup(toUSD($amount), 12);
				$package->fee_local = Validator::cleanup($amount, 12);
				$package->save();
				
				applog("Package saved {$package->id}");
				
				$page = $object->getPage();
				$page->publish();

				$this->view->status = true;
				$this->view->action = 'get_dash_page';	
				$this->view->data = 'packages';
				$this->view->message  = 'Package  updated';
				
			}
			
		}
		else{
			applog("Requred paramters missing or invalid to process, abort");
			$this->view->error = "Invalid request";
		}
		
		$this->render();	
	}
	
	/**
	* Delete a package
	*/
	public function package_delete () {
		
		if(!empty($_POST['id'])){
			
			$package = new TalentPackage($_POST['id']);
			if(!$package->ready()){
				$this->view->error = "Invalid package ID";
				return $this->render();
			}
			
			$category = $package->getCategory();
			
			$profile = $this->pickProfile();
			
			if(!empty($profile)) {							

				if($category->profile_id == $profile['id']){
					applog("Deleting talent package {$package->id}");
					$package->delete();
					
					$page = $category->getProfile()->getPage();
					$page->publish();

					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'packages';
					$this->view->message  = 'Package  removed';
				}
				else{
					$this->view->error = "Selected profile has no access to given package";	
					return $this->render();
				}

			}
			
		}
		
		$this->render();	
		
	}

	
	


	/**
	* Update profile image
	*/
	public function update_image () {
	
		$this->view->status = false;
		$this->view->message = "Unauthorized request";
		
		applog("Customer is uploading profile image. Reference:".$_POST['ref'].". Customer:".$this->user->id);
		

		if( !empty($_POST['ref']) && !empty($_POST['id'])){
			
			if(empty($_POST['image']) && empty($_POST['image_id'])){
				$this->view->error = "Invalid request";	
				applog("Image data is not available in POSt data, abort");
				return $this->render();
			}			
			
			if(!empty($_POST['image'])) {
				
				$imgdata = explode(';base64,',urldecode($_POST['image']));
				$ext = explode('/',$imgdata[0]);
				$ext = $ext[1];
				
				$fid = $this->user->id."_".time()."_".rand(1000,9999).'.'.$ext;
				$path = Config::$base_path . "/tmp/" . $fid;
				
				
				if($ext == 'jpeg'){
					$ext = 'jpg';	
				}
				
				$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp' );
				
				if(!in_array($ext, $valid_extensions)) {
					applog("Image extention is invalid, ".$ext.", abort");
					$this->view->error = "Invalid image format";	
					return $this->render();;
				}
				
				applog("Image extention is valid, ".$ext);
				
				$imgdata[1] = str_replace(' ', '+', $imgdata[1]);
				file_put_contents($path, base64_decode($imgdata[1]));				
				
			}
			elseif (!empty($_POST['image_id']) && is_numeric($_POST['image_id'])){
				
				applog("Reusing library image ".$_POST['image_id']);
				
				$img = new Image($_POST['image_id']);
				
				if($img->customer_id != $this->user->id){
					$this->view->error = "Unauthorized access";	
					return $this->render();
				}
				
				$paths = $img->getPaths();
				
				// extract extention
				$url = explode('.', $paths['url']);
				$ext = strtolower(array_pop($url));				
				
				// download file
				$fid = $this->user->id."_".time()."_".rand(1000,9999).'.'.$ext;
				
				$s = new Storage();
				$res = $s->download($paths['url'], $fid);
				
				$path = Config::$base_path . "/tmp/" . $fid;
				
				$size = getimagesize($path);
				$width = $size[0];
				$height = $size[1];					
				
				if(!file_exists($path)){
					applog("Image coping error, abort");
					$this->view->error = "Error coping image";	
					return $this->render();
				}
								
			}
			else{
				$this->view->error = "Invalid access";	
				applog("Image data or image ID is not present in the request, abort");
				return $this->render();	
			}
			
			applog("croping image ".$path);
			
			if ($ext == 'jpg' || $ext == 'jpeg') {
            	$src = @imagecreatefromjpeg($path);
			} elseif ($ext == 'png') {
				$src = imagecreatefrompng($path);
			} elseif ($ext == 'gif') {
				$src = imagecreatefromgif($path);
			} else {
				$this->view->message = "Unsupported file format";
				applog("Unsuported file format ".$path);
				$this->render();
				return false;
			}
			
			$mark = (!empty($_POST['mark']) && is_numeric(empty($_POST['mark']))) ? $_POST['mark'] : 200;
			$scale = (!empty($_POST['scale']) && is_numeric($_POST['scale'])) ? $_POST['scale'] : 1;
			$top = (!empty($_POST['top']) && is_numeric($_POST['top'])) ? $_POST['top'] : 0;
			$left = (!empty($_POST['left']) && is_numeric($_POST['left'])) ? $_POST['left'] : 0;
			
			$crop_length = round($mark/$scale);
			$y = round((100 - $top)/$scale);
			$x = round((100 - $left)/$scale);			
			
			
			$canvas = imagecreatetruecolor($crop_length, $crop_length);
			
			if($ext == 'png' || $ext == 'gif'){
						
				imagealphablending($canvas, false);
				$white = imagecolorallocatealpha($canvas, 0, 0, 0, 127);
				imagefill($canvas, 0, 0, $white);
				
				imagecopyresampled($canvas, $src,0, 0,$x, $y,$crop_length, $crop_length, $crop_length, $crop_length);
				imagesavealpha($canvas, true);
				imagepng($canvas, $path,0);
			}
			else {					
				
				$white = imagecolorallocate($canvas, 255, 255, 255);
				imagefill($canvas, 0, 0, $white);
				imagecopyresampled($canvas, $src,0, 0,$x, $y,$crop_length, $crop_length, $crop_length, $crop_length);
				imagejpeg($canvas, $path, 100);
			}
			
			imagedestroy($canvas);
			imagedestroy($src);
						
			
			applog("Image saved. creating different sizes");
			
			$thumb = "contents/thumb/" . $fid;
			Util::generateThumbImage($path, $thumb, 120);
			
			$i400 = "contents/i400/" . $fid;
			Util::resizeMoveImage($path, $i400, 400);
			
			$i600 = "contents/i600/" . $fid;
			Util::resizeMoveImage($path, $i600, 600);
			
			$full = "contents/images/" . $fid;
			$ori = Util::resizeMoveImage($path, $full);
			
			// crating image
			$image = new Image();
			$image->customer_id = $this->user->id;
			$image->i600 = Config::$image_url."/contents/i600/" . $fid;
			$image->i400 = Config::$image_url."/contents/i400/" . $fid;
			$image->thumb = Config::$image_url."/contents/thumb/" . $fid;
			$image->url = Config::$image_url."/contents/images/" . $fid;
			//$image->orientation = $ori;
			$image->save();
			
			applog('Image record created with id:'.$image->id.", associating image");
			
			$data = $image->getData();
			
			// assign to user
			if($_POST['ref'] == 'customer'){
			
				$this->user->image_id = $image->id;
				$this->user->save();
				
				Session::setCustomer($this->user);	
				applog("Customer record updated with new image");
			}
			elseif($_POST['ref'] == 'profile'){
				
				$profile = new Profile($_POST['id']);
				
				if($profile->customer_id == $this->user->id){
					
					$profile->image_id = $image->id;
					$profile->save();
					
					Session::setProfile($profile);
					
					applog("Customer {$this->user->id}  update Profile {$profile->id} image");
				}
				else{
					$this->view->data = NULL;
					$this->view->error = "Unauthorized request";
					applog("Customer {$this->user->id} is trying to update profile {$profile->id} image. Unathorized, abort");
				}													
			}
			
						
			$data['class'] = $_POST['ref'].'_'.$_POST['id'];
			$this->view->data = $data;
			
			$this->view->status = true;
			$this->view->message = "Profile image updated";
			
		}
		
		
		$this->render();
	}
	
	/**
	* Search bookings
	*/
	public function search_booking () {
		
		$profile =  Session::getProfile();
		$status = (!empty($_POST['status']) && 
		in_array($_POST['status'], array('AP','CO','BA','PP','CN','RJ','BK','EX',1,0))) ? $_POST['status'] : NULL;
		
		if(empty($profile)){
			// customer level
			$this->view->bookings = $this->user->getBookings($status);
			$this->view->html = $this->component('booking/booking-list');
			$this->view->id = 'booking-list';
			$this->view->status = true;	
		}
		else{
			
			$this->view->bookings = App::getBookings(NULL, $profile['id'],NULL, NULL,$status);		
			$this->view->html = $this->component('booking/booking-list');
			$this->view->id = 'booking-list';
			$this->view->status = true;
		}
		
		$this->render();
	}
	
	/**
	* Get booking details
	* Display view vary depend on booking status and people customer type
	*/
	public function booking_get () {
		
		$this->view->status = false;
		
		if(!empty($_POST['id'])) {
			
			$customer = Session::getCustomer();
			//$profile = $this->pickProfile();
			
			$booking = new Booking($_POST['id']);
			$this->view->id = 'dash-contents';
			$this->view->booking = $booking;
			$this->view->html = $this->component('booking/booking-view');
			
			$this->view->page = array(
					'title' => 'Booking', 
					'url' => '/dashboard/booking/'.$booking->id
				 );
		}
		else{
			$this->view->error = "Invalid booking ID";
		}		
		$this->render();
	}
	
	/**
	* Offer a differet package
	
	public function package_change () {
		
		if(!empty($_POST['booking'])){
			
			$booking = new Booking($_POST['booking']);
			
			if($booking->hasPermission()){
				
				$this->view->booking = $booking;
				$this->view->packages = $booking->getTalent()->getPackages();
				$this->view->html = $this->component('booking/package_change');
				$this->view->id = 'package_change';
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
		}
		
		$this->render();
	}
	*/
	
	
	
	/**
	* Offer a different time
	*/
	public function date_change () {
		
		if(!empty($_POST['booking'])){
			
			$booking = new Booking($_POST['booking']);
			
			if($booking->hasPermission()){
				
				$this->view->booking = $booking;
				$this->view->html = $this->component('booking/date_change');
				$this->view->id = 'date_change';
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
		}
		
		$this->render();
		
	}
	
	
	/**
	* talent/Agency Accept booking request
	*/
	public function booking_accept () {
		
		
		
		if(!empty($_POST['booking']) && is_numeric($_POST['booking'])){
			
			$booking = new Booking($_POST['booking']);
			
			if($booking->ready() && $booking->isRecipient()){
				
				if($booking->status != DataType::$BOOKING_PENDING){
					
					if($booking->status == DataType::$BOOKING_REJECT){
						$this->view->error = 'You already REJECTED this booking request.';	
					}
					else{
						$this->view->error = 'You already ACCEPTED this booking request.';	
					}
								
					return $this->render();	
				}
				
				/****  Check if booking is entitle for free quote, otherwise check for subscription ***/
				if($booking->free_quota == 0){
					
					// assign an active subscription
					if(!$booking->assignSubscription()){
						applog("No active subscription for respond to this booking, asking to purchase on time credit ");
						
						$this->view->error = "Your booking credits are over.<br>Please upgrade your plan to accept this booking.";
						return $this->render();
						
					}
					else{
						applog("Subscription id {$booking->subscription_id} assigned to {$booking->id}");
					}
				}
				
												
				// discount
				if(!empty($_POST['discount'])){
					
					$dis = toUSD($_POST['discount']);
				
					if(is_numeric($_POST['discount']) && $_POST['discount'] > 0 && toUSD($_POST['discount']) <= $booking->talent_fee){
							$booking->discount = toUSD($_POST['discount']);
							
													
					}
					else{
						$this->view->error = "Invalid discount";
						return $this->render();							
					}									
				}
				
				// number of people
				if(!empty($_POST['additional_people'])){
				
					if(is_numeric($_POST['additional_people']) && $_POST['additional_people'] > 0 ){
						$booking->additional_people =  Validator::cleanup($_POST['additional_people'],3);
												
					}
					else{
						$this->view->error = "Invalid number of people";
						return $this->render();							
					}					
				}
				
				
				
				// additional charges
				if(!empty($_POST['expences'])){
					
					foreach ($_POST['expences'] as $e){
					
						if(is_numeric($e['amount']) && $e['amount'] >= 0 && strlen($e['amount']) <= 12) {
							
							$exp = new Expence();
							$exp->booking_id = $booking->id;
							$exp->expence_type = Validator::cleanup($e['type'], 3);
							$exp->description = Validator::cleanup($e['desc'], 400);
							$exp->amount = toUSD($e['amount']);
							$exp->amount_local = Validator::cleanup($e['amount'],12);
							$exp->save();						
							
						}
					}
				}
				
				// change status 
				$booking->setStatus(DataType::$BOOKING_ACCEPTED);
				$booking->responded_date = 'NOW';
				$booking->save();
				
				// sending notification
				$com = new ComEngine();
				$com->bookingAccepted ($booking);
				
				$this->view->booking = $booking;
				$this->view->html = $this->component('booking/booking-view');
				$this->view->id = 'dash-contents';	
				$this->view->status = true;
				
				$this->view->message = "Booking Accepted";
				
			}			
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
		
	}
	
	/**
	* Receipeint reject a booking
	*/
	public function booking_reject (){
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$booking = new Booking($_POST['id']);
			
			if($booking->ready() && $booking->isRecipient()){
				
				
				if($booking->status != DataType::$BOOKING_PENDING){
					
					if($booking->status == DataType::$BOOKING_REJECT){
						$this->view->error = 'You already REJECTED this booking request.';	
					}
					else{
						$this->view->error = 'You already ACCEPTED this booking request.';	
					}
								
					return $this->render();	
				}
				
				/****  Check if booking is entitle for free quote, otherwise check for subscription ***/
				if($booking->free_quota == 0){
					
					// assign an active subscription
					if(!$booking->assignSubscription()){
						applog("No active subscription for respond to this booking, asking to purchase on time credit ");
						
						$this->view->error = "Your booking credits are over.<br>Please upgrade your plan to accept this booking.";
						return $this->render();
						
					}
					else{
						applog("Subscription id {$booking->subscription_id} assigned to {$booking->id}");
					}
				}
				
				$booking->setStatus(DataType::$BOOKING_REJECT);
				$booking->responded_date = 'NOW';
				$booking->save();
				
				$this->view->booking = $booking;
				$this->view->html = $this->component('booking/booking-view');
				$this->view->id = 'dash-contents';	
				$this->view->status = true;
				
				$this->view->message = "Booking Rejected";
				
				
			}
			else{
				$this->view->error = "Unauthorized request";
			}
		}
		else{
			$this->view->error = "Invalid request";
		}
		
		$this->render();
	}
	
	/**
	* Customer confirm booking
	*/
	public function booking_confirm () {
		
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			if(empty($_POST['booking_conditions'])){
				$this->view->error = "Please accept booking conditions and terms";
				return $this->render();
			}
			
			$booking = new Booking($_POST['id']);
			
			
			if($booking->status == DataType::$BOOKING_ACCEPTED){
				 			
				if($booking->customer_id = $this->user->id ){
					
					// confirm booking
					// create/send invoice
					$booking->setStatus(DataType::$BOOKING_CONFIRM);
					$booking->save();
							
					//$booking->getInvoice();
					$this->view->booking = $booking;
					
					//$this->view->html = $this->component("booking/checkout");
					// sending notification
					$com = new ComEngine();
					$com->bookingConfirmed ($booking);
					
					$this->view->booking = $booking;
					$this->view->html = $this->component('booking/booking-view');
					$this->view->id = 'dash-contents';
					$this->view->status = true;
					$this->view->message = "Thank you!";
					
					
				}
				else{
					$this->view->error = "Unauthorized request";	
				}								
			}
			else{
				$this->view->booking = $booking;
				$this->view->html = $this->component('booking/booking-view');
				$this->view->id = 'dash-contents';
				$this->view->status = true;
				$this->view->message = "Booking updated";
			}
			
		}		
		
		$this->render();
		
	}
	
	/**
	* Cancel a booking by customer
	*/
	public function booking_cancel () {

		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			if($booking->status == DataType::$BOOKING_ACCEPTED){
				
				if($booking->customer_id = $this->user->id ){
				
					$booking->setStatus(DataType::$BOOKING_CANCEL);
					$booking->save();
					
					$com = new ComEngine();
					$com->bookingCanceled ($booking);
					
					$this->view->booking = $booking;
					$this->view->html = $this->component('booking/booking-view');
					$this->view->id = 'dash-contents';
					$this->view->status = true;
					$this->view->message = "Booking has been canceled";
					
				}
				else{
					$this->view->error = "You have no permission to update this booking";	
				}
				
			}
			
		}
		$this->render();
	}
	
	
	/**
	* Update booking or create a booking change request
	* If booking status is PENDING, booking will be updated
	* otherwise create a change request 
	*/
	public function booking_update () {
		
		if(!empty($_POST['booking_id']) && is_numeric($_POST['booking_id'])){
			
			$booking = new Booking($_POST['booking_id']);
			
			if(!$booking->isSender() && !$booking->isRecipient()){
				$this->view->error = "You have no permission to change the booking";
				return $this->render();	
			}
			
			
			if($booking->status == DataType::$BOOKING_PENDING) {
				
				if($booking->isRecipient()){
					$this->view->error = "Please accept the booking before make any changes";
					return $this->render();	
				}
				
				// update booking
				$engine = new BookingEngine();
				$engine->clearEvent();
				
				// issue with updating booking
				if(!$engine->setBooking($booking)){
					$this->view->error = $engine->error;
					return $this->render();
				}
				
				
				if(!empty($_POST['package_id']) && is_numeric($_POST['package_id']) && $booking->package_id != $_POST['package_id']){
					$packageset = $engine->setPackage($_POST['package_id']);				
				}
				else{
					$packageset = $engine->setPackage($booking->package_id);
				}

				// if package setting issue
				if(empty($packageset)){
					$this->view->error = $engine->error;
					return $this->render();		
				}
				
				$updated = $engine->submit();

				if($updated){
					$this->view->status = true;
					$this->view->message = "Booking updated";
					
					$this->view->booking = $booking;
					$this->view->html = $this->component('booking/booking-view');
					$this->view->id = 'dash-contents';
				}
				else{
					$this->view->error = $engine->error;
					return $this->render();	
				}
				
			}
			else{
				// sending request
				// update booking
				$engine = new BookingEngine();
				$engine->clearEvent();
				
				// issue with updating booking
				if(!$engine->setBooking($booking)){
					$this->view->error = $engine->error;
					return $this->render();
				}
				
				if(!empty($_POST['package_id']) && is_numeric($_POST['package_id'])){
					$packageset = $engine->setPackage($_POST['package_id']);				
				}
				else{
					$packageset = $engine->setPackage($booking->package_id);
				}

				// if package setting issue
				if(empty($packageset)){
					$this->view->error = $engine->error;
					return $this->render();		
				}
				
				$request = $engine->submitChangeRequest();
				
				if(!empty($request)){
					
					$com = new ComEngine();
					$com->bookingUpdateRequest($booking, $request);
					
					$this->view->booking = $booking;
					$this->view->request = $request;
					$this->view->html = $this->component('booking/booking-view');
					$this->view->id = 'dash-contents';
					$this->view->status = true;
					$this->view->message = "Booking change request has been sent";
				}
				else{
					$this->view->error = $engine->error;
					return $this->render();	
				}
				
			}		
			
		}
		
		$this->render();
	}
	
	
	/**
	* Commit booking changes
	*/
	public function booking_change_respond () {
			
		if(!empty($_POST['id']) && is_numeric($_POST['id']) && !empty($_POST['reply'])){
			
			$request = new ChangeRequest($_POST['id']);
			$booking = $request->getBooking();
			
			$customer = Session::getCustomer();
			
			// check permission
			$has_permission = false;
			
			if($request->from_sender == 1){
				
				$info = $booking->getinfo(); 
				
				if(!empty($info['talent']['customer_id']) && $info['talent']['customer_id'] == $customer['id']){
					$has_permission = true;
				}
				elseif(!empty($info['agency']['customer_id']) && $info['agency']['customer_id'] == $customer['id']){
					$has_permission = true;
				}
			}
			else{
				$has_permission = $booking->customer_id == $customer['id'];
			}
			
			if($has_permission){
				
				if($_POST['reply'] == 'accept'){
					
					if($request->is_cancel == 1) {
						
						$booking->setStatus(DataType::$BOOKING_CANCEL);
						$booking->save();
						
						$request->status = 'a';
						$request->approved_by = $customer['id'];
						$request->approved_date = 'NOW';
						$request->save();
						
						$com = new ComEngine();
						$com->bookingCanceled ($booking, $request);
						
						$this->view->message = "Booking canceled";
						
					}
					else {
						
						$appoinmnet = $booking->getAppoinment();
					
						if(!$request->isEmpty('session_start')){
							$booking->session_start = $request->session_start;
							$appoinmnet->begins = $booking->session_start;
						}
						
						if(!$request->isEmpty('location_id')){
							$booking->location_id = $request->location_id;
						}
						
						if(!$request->isEmpty('package')){
							$booking->package_id = $request->package;
							
							$package = new TalentPackage($request->package);
							$appoinmnet->duration = $package->timespan;
						}
						
						if((!$request->isEmpty('discount') || $request->discount == '0.00') && $request->discount != $booking->discount){
							$booking->discount = $request->discount;
						}
						
						$booking->save();
						$appoinmnet->save();					
						
						
						
						$request->status = 'a';
						$request->approved_by = $customer['id'];
						$request->approved_date = 'NOW';
						$request->save();						
						
						$com = new ComEngine();
						$com->bookingUpdated($booking, $request);
						
						$this->view->message = "Booking updated";
					
					}
					
				}
				else{
					$request->status = 'r';
					$request->approved_date = 'NOW';
					$request->save();
					// send notification
					
					$com = new ComEngine();
					$com->bookingUpdatedDeclined($booking, $request);
					
					
					$this->view->message = "Booking change request declined";
				}
				
				$this->view->id = 'dash-contents';
				$this->view->booking = $booking;
				$this->view->html = $this->component('booking/booking-view');
				
			}
			else{
				$this->view->error = "You do not have permission to perform this action";
			}
						
			
		}
		
		$this->render();		
	}
	
	/**
	* Send a booking cancellation request
	*/
	public function booking_change_cancel () {
		
		if(!empty($_POST['booking_id']) && is_numeric($_POST['booking_id'])){
			
			$booking = new Booking($_POST['booking_id']);
			
			if(!$booking->isSender() && !$booking->isRecipient()){
				$this->view->error = "You have no permission to change the booking";
				return $this->render();	
			}
			
			if($booking->status == DataType::$BOOKING_PENDING) {
				
				if($booking->isRecipient()){
					// perform reject action
					$booking->setStatus(DataType::$BOOKING_REJECT);
					$booking->responded_date = 'NOW';
					$booking->save();
					
					$com = new ComEngine();
					$com->bookingReject($booking);
					
					$this->view->message = "Booking canceled";

				}
				
				if($booking->isSender()){
					
					$booking->setStatus(DataType::$BOOKING_CANCEL);
					$booking->save();
					
					$com = new ComEngine();
					$com->bookingCanceled ($booking);
					
					$this->view->message = "Booking canceled";

				}
				
			}
			elseif($booking->status == DataType::$BOOKING_ACCEPTED && $booking->isSender()) {
				
				$booking->setStatus(DataType::$BOOKING_CANCEL);
				$booking->save();
				
				$com = new ComEngine();
				$com->bookingCanceled ($booking);
				
				$this->view->message = "Booking canceled";
								
			}
			else{
				
				// creating a cancel request
				$request = new ChangeRequest();
				$request->booking_id = $booking->id;
				$request->created_by = $this->user->id;
				$request->from_sender = $booking->isSender() ? 1 : 0;
				$request->is_cancel = 1;
				$request->save();
				
				$com = new ComEngine();
				$com->bookingUpdateRequest($booking, $request);
				
				$this->view->message = "Booking cancel request sent";
				
			}
			
			$this->view->booking = $booking;
			$this->view->html = $this->component('booking/booking-view');
			$this->view->id = 'dash-contents';
			$this->view->status = true;
						
			
		}
		
		
		$this->render();
		
	}
	
	
	/**
	* Get booking chnage list for given booking
	*/
	public function booking_changes () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$booking = new Booking($_POST['id']);
			
			$this->view->id = 'dash-contents';
			$this->view->booking = $booking;
			$this->view->html = $this->component('booking/change_list');
			
		}
		
		$this->render();		
	}
		
	/**
	* Update a booking note
	*/
	public function booking_note () {
		
		if(!empty($_POST['id'])) {
			
			$customer = Session::getCustomer();
			
			$booking = new Booking($_POST['id']);
			
			$text = Validator::cleanup ($_POST['value'],4000);
			
			if( $booking->isRecipient()){
				
				$booking->talent_note = $text;
				$booking->save();
				$this->view->message = "Note updated";
				$this->view->status = true;
			}
			elseif($booking->isSender() ){
				$booking->customer_note = $text;
				$booking->save();
				$this->view->message = "Note updated";
				$this->view->status = true;
			}
			else{
				$this->view->error = "You do not have permission to update notes";	
			}		
		}	
		
		$this->render();
	}
	
	/**
	* Update a booking note
	*/
	public function booking_name () {
		
		if(!empty($_POST['id'])) {
			
			$customer = Session::getCustomer();
			
			$booking = new Booking($_POST['id']);
			
			$text = Validator::cleanup ($_POST['value'],400);
			
			if( $booking->isRecipient() || $booking->isSender()){
				
				$booking->name = $text;
				$booking->save();
				
				$this->view->message = "Name updated";
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "You do not have permission to update name";	
			}			
		}	
		
		$this->render();
	}
	
	/**
	* Booking change request form
	*/
	public function booking_change () {
		
		if(!empty($_POST['id'])) {
			
			$booking = new Booking($_POST['id']);
			
			if( $booking->isRecipient() || $booking->isSender()){
				
				$this->view->booking = $booking;
				$this->view->id = 'dash-contents';
				$this->view->html = $this->component('booking/change_form');				
			}
			else{
				$this->view->error = "You do not have permission to modify this booking";
			}
			
		}
		$this->render();
	}
	
	/**
	* Send a message to talent or customer
	*/
	public function message_send () {
		
		if(!empty($_POST['id']) && !empty($_POST['value'])) {
			
			$booking = new Booking($_POST['id']);
			
			if($booking->ready()){
				
				// assign booking credit
				if($booking->free_quota == 0 && $booking->isEmpty('subscription_id')){
					// assign a subscription
					if(!$booking->assignSubscription()){
						$this->view->error = "Your booking credits are over.<br>Please upgrade your plan to accept this booking.";
						return $this->render();
					}
					else{
						$booking->responded_date = 'NOW';
						$booking->save();
					}
				}
				
				
				$profile = $this->pickProfile();
				$hasMessages = $booking->hasMessages();
				
				if($hasMessages || $booking->status != DataType::$BOOKING_PENDING ||					
					($booking->isRecipient() && $booking->status == DataType::$BOOKING_PENDING)) {
						
						$board = $booking->getMessageBoard();
						$message = Validator::cleanup($_POST['value'], 4000);
						
						$chat = new Chat();
						$res = $chat->send($board->id, $_POST['value']);					

					
						if($res){
							$this->view->status = true;
							
							$last = (!empty($_POST['l']) && is_numeric($_POST['l'])) ? $_POST['l'] : 0;
							/*
							$this->view->data = array( 
								 'boards' => $chat->getUpdates($last),
								 'self' => $customer['id']
								);
							
							$this->view->action = 'show_chat_messages';				
			*/
						}
						else{
							$this->view->error = $chat->error;	
						}
						
				}
				else{
					$this->view->error = "You are not allowed to send messages on this board";	
				}
									
				
				
				if(!empty($res)){

					$this->view->data = array( 0 => $chat->message->getInfo());
					$this->view->html = $this->component('messages');
				//	$this->view->data = NULL;
					$this->view->action = 'show_messages';
					
					
					// email notification
				}
				
			}
			else{
				$this->view->error = "Invalid request";	
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Dispaly appointment create form
	
	public function appointment_edit () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			$profile = $this->pickProfile();
			$appointment = new Appoinment($_POST['id']);
			
			if(!empty($profile)) {
				
				if($profile['type'] == 't' && $appointment->talent_id == $profile['id']  ){
					$this->view->appointment = $appointment;
					$this->view->html = $this->component('talent/appointment-form');
				}
				elseif($profile['type'] == 'a' && $appointment->agency_id == $profile['id']  ){
					$this->view->appointment = $appointment;
					$this->view->html = $this->component('appointment_view');
				}
			}
			else{
				$this->view->error = "Switch to talent or agency profile to edit appointment";	
			}
		}
		
		
		$this->view->id = 'dash-contents';
		
		$this->render();
						
	}
	*/
	
	
	/**
	* Display appointment options
	*/
	public function appointment_options () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		$profile = $this->pickProfile();
		
		if(!empty($profile)) {
		
			if(!empty($_POST['id']) && is_numeric($_POST['id']) && !empty($_POST['action'])){


				$appointment = new Appoinment($_POST['id']);

				if(!empty($profile)) {

					if($appointment->profile_id == $profile['id'] || $appointment->agency_id == $profile['id']  ){

						$this->view->appointment = $appointment;

						if($_POST['action'] == 'view'){
							$this->view->html = $this->component('appointment/appointment_view');
							$this->view->page = array(
								'title' => 'Appointment', 
								'url' => '/dashboard/appointment/'.$this->view->appointment->id
							 );
						}
						elseif($_POST['action'] == 'edit'){
							$this->view->html = $this->component('appointment/appointment-form');

						}
						else{
							$this->view->error = "Invalid request";	
						}

					}
					else{
						$this->view->error = "You have no necessary permissions to <br>view or update this appointment";	
					}

				}
				else{
					$this->view->error = "Switch to talent or agency profile to edit appointment";	
				}
			}
			elseif(!empty($profile)){
				$this->view->html = $this->component('appointment/appointment-form');
			}
			$this->view->id = 'dash-contents';
		
		
		}
		
		
		
		$this->render();
		
	}
	
	/**
	* Create or update an appointment
	*/
	public function appointment_update () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		$profile = $this->pickProfile();
		
		if(empty($profile)){
			return;	
		}
		
		if(!empty($_POST['appointment_id']) && is_numeric($_POST['appointment_id'])){
			
			$appointment = new Appoinment($_POST['appointment_id']);
			
			if(($profile['type'] == 't' && $appointment->talent_id != $profile['id']) && ($profile['type'] == 'a' && $appointment->agency_id != $profile['id'])){
				
				$this->view->error = "You do not have permission to update this appointment";
				return $this->render();
			}
		}
		
		if(empty($appointment)){
			$appointment = new Appoinment();
			$appointment->profile_id = $profile['id'];		
			
			if(!empty($_POST['confirm'])){
				$appointment->confirmed	= 1; 	
			}
		}
		
		$datetime = strtotime(Util::toSysDate($_POST['appointment_begins_date'].' '.$_POST['appointment_begins_time']));
		$duration = Util::ToMinutes($_POST['appointment_duration']);
		$rest = Util::ToMinutes($_POST['appointment_rest_time']);
		
		
		
		if(!$appointment->isBooking()){
		
			if(empty($datetime)){
				$this->view->error = "Invalid date/time";
				return $this->render();
			}
			
			if($datetime < time()){
				$this->view->error = "You may not create appointments for past dates";
				return $this->render();
			}
						
			if(empty($duration) || $duration <= 0){
				$this->view->error = "Invalid time duration";
				return $this->render();	
			}
			
			if($duration > 1440*7){
				$this->view->error = "Appointment time is too long.<br>You may create appointments for less than 7 days long.";
				return $this->render();
			}
			
			if($rest < 0){
				$this->view->error = "Invalid rest time";
				return $this->render();	
			}
			
			if($rest > 1440){
				$this->view->error = "Maximum rest time should be one day. <br>You may create time-off appointments for additional days";
				return $this->render();	
			}
			
			$datetime = Util::ToSysDate($_POST['appointment_begins_date'],$_POST['appointment_begins_time']);
			
			$appointment->all_day = (!empty($_POST['appointment_all_day'])) ? 1 : 0;
			$appointment->time_off = (!empty($_POST['appointment_time_off'])) ? 1 : 0;
			
			$appointment->begins = $datetime;
			$appointment->duration =  $duration;
		//	$appointment->confirmed	= !empty($_POST['appointment_confirmed']) ? 1 : 0;
		}
		
		$appointment->description = Validator::cleanup($_POST['appointment_description'], 400);
$appointment->note = Validator::cleanup($_POST['appointment_note'], 4000);
		$appointment->rest_time = $rest;
		$appointment->is_private = !empty($_POST['appointment_is_private']) ? 1 : 0;
		$appointment->save();
			
		$this->view->status = true;
		$this->view->action = 'history';
	//	$this->view->action = 'get_dash_page';	
	//	$this->view->data = 'appointments';
		$this->view->message  = 'Appointment updated';
				
		$this->render();
		
	}
	
	
	/**
	* Confirm an appointment
	*/
	public function appointment_confirm () {
		
		$profile = $this->pickProfile();
		
		if(empty($profile)){
			$this->view->error = "Switch to talent or agency profile to update appointment";
			return $this->render();		
		}
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$appointment = new Appoinment($_POST['id']);
			
			if(($profile['type'] == 't' && $appointment->talent_id = $profile['id']) || ($profile['type'] == 'a' && $appointment->isEmpty('talent_id') && $appointment->agency_id = $profile['id'])){
				
				$appointment->confirmed = 1;
				$appointment->save();
				
				// reload page
				$this->view->status = true;
				$this->view->message = "Appointment confirmed";
				$this->view->action = 'recent';
				
			}
			else{
				$this->view->error = "Unauthorized access";
				return $this->render();
			}
			
			
		}
		
		$this->render();	
	}
	 
	
	
	/**
	* Search for appointmnets
	*/
	public function appointment_search () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		$profile = $this->pickProfile();
		$timespan = (!empty($_POST['timespan'])) ? $_POST['timespan'] : 'day';
		
		if(!empty($profile)) {
			
			$object = new Profile($profile['id']);
		/*
			if($profile['type'] == 'a' && !empty($_POST['talent']) && is_numeric($_POST['talent'])){
				// appointments  for a talent / view by agency 
				$request = new AgencyRequest($_POST['talent']);

				if($request->agency_id == $profile['id']){
					$talent = $request->getTalent();	
					$timespan = 'day';
					$this->view->is_agency = true;
					$this->view->request = $request;
				}
			}
		*/	

			if($object->ready()) {

				if($timespan == 'day'){

					$date = (!empty($_POST['filter_date'])) ? date('Y-m-d',strtotime($_POST['filter_date'])) : date('Y-m-d');

					$this->view->date = $date;
					
					$this->view->appointments = $object->getAppoinments($date, NULL, true);		

					$this->view->html = $this->component('appointment/appointments-day');

				}
				elseif($timespan == 'week'){		


					$week = (!empty($_POST['filter_week'])) ? $_POST['filter_week'] : date("Y-W");
					list($y, $w) = explode('-', $week);

					$date = strtotime($y."-1-1");
					$date = strtotime('+'.$w.' weeks', $date);
					$begin = (date('D', $date) == 'Sun') ? date('Y-m-d 00:00', $date) : date('Y-m-d',strtotime('last sunday', $date));
					$end = (date('D', $date) == 'Sat') ? date('Y-m-d 23:59', $date) : date('Y-m-d',strtotime('next saturday', $date));

					$this->view->begin_date = $begin;
					$this->view->end_date = $end;

					$this->view->appointments = $object->getAppoinments($begin, $end, true);	

					$this->view->html = $this->component('appointment/appointments-week');
				}
				elseif($timespan == 'month'){

					$year = (!empty($_POST['filter_year']) && is_numeric($_POST['filter_year'])) ? $_POST['filter_year'] : date('Y');

					$month = (!empty($_POST['filter_month']) && is_numeric($_POST['filter_month'])) ? $_POST['filter_month'] : date('n');

					$this->view->appointments = $object->getMontlyAppoinments($year, $month);
					

					$this->view->html = $this->component('appointment/appointments-month');
				}
				else{
					$this->view->error = "Invalid request";	
				}
			}
		}
		
		$this->view->id = 'appointments';
		$this->view->status = true;
		//$this->view->message = 'Search done';
		
		$this->render();
		
	}
	
	/**
	* View brief detail about appointmnt
	*/
	public function appointment_brief () {
		
		// add security
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$this->view->appointment = new Appoinment($_POST['id']);
			
			$this->view->data = array(
				//'title' => 'Appointment',
				'para' => array( 'id' => $this->view->appointment->id),
				'mask' => true,
				'buttons' => false
			);
			
			/*'buttons' => array(
					'edit' => array('label' => "Edit", 'action' => 'appointment_edit'),
					'cancel' => array('label' => "Close", 'class' => 'button-alt')
				)*/
			
			$this->view->html = $this->component('appointment_brief');
			$this->view->action = 'popup';
		}
		else{
			$this->view->error = 'Invalid request';	
		}
		
		$this->render();
	}
	
	
	/**
	* View brief detail about appointmnt
	
	public function appointment_view () {
		
		// add security
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$this->view->appointment = new Appoinment($_POST['id']);
			
			$this->view->data = array(
				//'title' => 'Appointment',
				'para' => array( 'id' => $this->view->appointment->id),
				'mask' => true,
				'buttons' => false
			);
		
			
			$this->view->html = $this->component('appointment_view');
			$this->view->id = 'dash-contents';
		}
		else{
			$this->view->error = 'Invalid request';	
		}
		
		$this->render();
	}
	*/
	
	/**
	* Delete user created appoinment
	*/
	public function appointment_delete () {
		
		$profile = $this->pickProfile();
		
		if($profile['type'] == 't' ){
			
			if(!empty($_POST['id'])){
				
				$appointment = new Appoinment($_POST['id']);
				
				if($appointment->talent_id != $profile['id']){
					$this->view->error = "Un-authorized access";
					return $this->render();
				}
				
				if($appointment->isBooking()){
					$this->view->error = "Appointment can not be deleted";
					return $this->render();
				}
				
				$appointment->delete();
				
				$this->view->message = "Appointment deleted";
				$this->view->status = true;
				$this->view->action = 'get_dash_page';	
				$this->view->data = 'appointments';
			}
			else{
				$this->view->error = "Invalid request";	
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Update public page contents
	*/
	public function page_update () {
		
		if(!empty($_POST['page_id']) && is_numeric($_POST['page_id']) && !empty($_POST['section']) && !empty($_POST['field'])){
			
			$page = new Page($_POST['page_id']);
			
			if($page->customer_id == $this->user->id) {
				
				
				
				if($_POST['field'] == 'title'){
					$val = Validator::cleanup($_POST['value'],1000);
				}
				elseif($_POST['field'] == 'body'){
					$val = Validator::cleanup($_POST['value'],10000);
				}
				else{
					$this->view->error = "Invalid request";
					return $this->render();
				}
				
				$sec = Validator::cleanup($_POST['section'], 1);
				
				// check for emails and phone numebers
				if(!empty($val)){
					
					if($sec != 'e' && Validator::hasPhoneNumber($val)){
						$this->view->error = "Please remove phone number";
						return $this->render();
					}
					
					$val = Validator::removeEmails($val);					
				}
				
				
				
				$section = $page->getSection($sec);
				
				
				
				if(isset($_POST['index']) && is_numeric($_POST['index'])){
					$part = $section->getPart($_POST['index']);
					
					if($_POST['field'] == 'title'){
						$part->title = $val;
					}
					else{
						$part->body = $val;
					}
					
					$part->save();
					
					$cls = $part->section.'_'.$part->order.'_'.$_POST['field'];
				}
				else{
					
					if($sec == 'e'){
						
						$val = trim($val, '"');
						
						if(strpos($val, 'https://www.youtube.com') === 0 || 
						   strpos($val, 'https://www.facebook.com') === 0 ||
							strpos($val, 'https://www.dailymotion.com') === 0 ||
							strpos($val, 'https://player.vimeo.com') === 0 ) {
								$section->body = $val;
							}
						else{
							$this->view->error = "We do not support this video sharing site.";
							return $this->render();	
						}
						
					}					
					elseif($_POST['field'] == 'title'){
						$section->title = $val;
					}
					else{
						$section->body = $val;
					}
					
					$section->save();
					$cls = $section->section.'_'.$_POST['field'];	
				}
				
				$this->view->data = array(
								'section' => $section->section,
								'class' => $cls,
								'val' => $val								
							);
				
				$this->view->status = true;
				$this->view->message = "Updated";
				$this->view->action = "page_part_updated";
				
				$page->updated = "NOW";
				$page->save();
				
				$page->publish();
			
			}
			else{
				$this->view->error = "Unauthorized access";	
			}
			
			 
			
		}
		
		$this->render();
	}
	
	
	/** Publish page **/
	public function page_publish () {
		
		if(!empty($_POST['id'] )&& is_numeric($_POST['id'])){			
				
			$page = new Page($_POST['id']);
			
			if($page->isDeleted()){
				$this->view->error = "Page can not be published";	
			}
			
			if($page->customer_id == $this->user->id) {
				
				$page->publish();
				
				$this->view->status = true;
				$this->view->message = "Page published";
			}
			
		}
		
		$this->render();
		
	}
	
	
	/**
	* Enable or disabled page part
	*/
	public function page_section_switch () {
		
		if(!empty($_POST['page_id']) && is_numeric($_POST['page_id']) && !empty($_POST['section']) && isset($_POST['active'])){
			
			$page = new Page($_POST['page_id']);
			
			if($page->customer_id == $this->user->id) {
				
				$sec = Validator::cleanup($_POST['section'], 1);
				
				if($sec == 'b' || $sec == 'd' || $sec == 'e' || $sec == 'f' || $sec == 'g' || $sec == 'h'){
				
					$section = $page->getSection($sec);
					$section->enabled = !empty($_POST['active']) ? 1 : 0;
					$section->save();
					
					if($section->enabled == 1){
						$this->view->message = "Section enabled";	
					}
					else{
						$this->view->message = "Section disabled";	
					}
					
					$this->view->status = true;
				}
				
				$page->publish();
				
			}
			else{
				$this->view->error = "Unauthorized access";	
			}	
			
		}
	
		$this->render();		
	}
	
	/**
	* Remove a part of a section content
	*/
	public function page_part_remove () {
		
		if(isset($_POST['page_id']) && is_numeric($_POST['page_id']) && isset($_POST['index']) && is_numeric($_POST['index']) && !empty($_POST['section']) ){
			
			$page = new Page($_POST['page_id']);
			
			$section = $page->getSection($_POST['section']);
			
			if($section->ready() && $page->customer_id == $this->user->id){
			
				$part = $section->getPart($_POST['index']);
				
				if(!empty($part)){
					$part->delete();
					$this->view->message = "Removed";	
				}
				else{
					$this->view->error = "Invalid request";		
				}
				
				
			} // permission
			else {
					$this->view->error = "Unauthorized access";	
				}
		}
		
		
		$this->render();	
	}
	
	/**
	* Update page social links
	*/
	public function update_social_links () {
	
		if(isset($_POST['page_id']) && is_numeric($_POST['page_id'])){
			
			$page = new Page($_POST['page_id']);
			
			if($page->customer_id == $this->user->id) {
				
				$section = $page->getSection('g');
				
				$data = array('ok' => true);
				$errors = array();
				
				$updated = 0;
				
				foreach (DataType::$SOCIAL_CLASSES as $s => $sn){
					
					if(!empty($_POST[$s])){
						
						if(Validator::isURL($_POST[$s])){
							
							if($s == 'ww'){
								$data[$s] = $_POST[$s];	
							}
							elseif($s == 'fb'){
								
								if(strpos($_POST[$s],('://www.facebook.com')) != false || strpos($_POST[$s],('://facebook.com')) != false || strpos($_POST[$s],('://fb.com')) != false ){
									$data[$s] = $_POST[$s];	
									$updated++;
								}
								else{
									$errors[] = "Invalid Facebook URL";	
								}
							}
							elseif($s == 'tw'){
								
								if(strpos($_POST[$s],('://www.twitter.com')) != false || strpos($_POST[$s],('://twitter.com')) != false || strpos($_POST[$s],('://t.co')) != false || strpos($_POST[$s],('://www.t.co')) != false ){
									$data[$s] = $_POST[$s];
									$updated++;	
								}
								else{
									$errors[] = "Invalid Twitter URL";	
								}
							}
							elseif($s == 'yt'){
								
								if(strpos($_POST[$s],('://www.youtube.com')) != false || strpos($_POST[$s],('://youtube.com')) != false || strpos($_POST[$s],('://youtu.be')) != false ||  strpos($_POST[$s],('://www.youtu.be')) != false ){
									$data[$s] = $_POST[$s];	
									$updated++;
								}
								else{
									$errors[] = "Invalid Youtube URL";	
								}
							}
							elseif($s == 'ig'){
								
								if(strpos($_POST[$s],('://www.instagram.com')) != false || strpos($_POST[$s],('://instagram.com')) != false  ){
									$data[$s] = $_POST[$s];
									$updated++;	
								}
								else{
									$errors[] = "Invalid Instagram URL";	
								}
							}
							elseif($s == 'tk'){
								
								if(strpos($_POST[$s],('tiktok.com')) != false  ){
									$data[$s] = $_POST[$s];
									$updated++;	
								}
								else{
									$errors[] = "Invalid TikTok URL";	
								}
							}
							elseif($s == 'ln'){
								
								if(strpos($_POST[$s],('linkedin.com')) != false  ){
									$data[$s] = $_POST[$s];
									$updated++;	
								}
								else{
									$errors[] = "Invalid LinkedIn URL";	
								}
							}
							elseif($s == 'pn'){
								
								if(strpos($_POST[$s],('://www.pinterest.com')) != false || strpos($_POST[$s],('://pinterest.com')) != false ){
									$data[$s] = $_POST[$s];	
									$updated++;
								}
								else{
									$errors[] = "Invalid Pinterest URL";	
								}
							}
							
						}
							
					}
					
				}// sites
				
				$section->body = json_encode($data);
				$section->save();
				
				
				$page->publish();
				
				$this->view->status = true;
				$this->view->message = (!empty($updated)) ? "Social links updated" : "No links updated";
				$this->view->data = $data;
				$this->view->trigger = 'social_link_reset';
				
				if(!empty($errors)){
					$this->view->error = implode('<br>', $errors);	
				}
				
			}
			else{
				$this->view->error = "Unauthorized access";	
			}
			
		}
		
		$this->render();
	}
	
	/** 
	* Show subscription form
	**/
	public function subscription_add () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Subscription add page loading", true);
		
		$profile = $this->pickProfile();
		if($profile === false){
			// profile select dialog display
			return;
		}
		
		applog("Proceeding with profile {$profile['id']}");
		
		if($profile['type'] == 't') {
				
			$this->view->status = true;
			$this->view->html = $this->component('subscription/add');
			$this->view->id = 'dash-contents';
			$this->render();
		}
		elseif($profile['type'] == 'a') {
			
			$skill = App::getAgencyCategory() ;
			
			$_POST['category'] = $skill->id;
			$this->subscription_category();
		}
	}
	
	/**
	* Show subscription packages for selected category
	*/
	public function subscription_category () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Add new subscription request recived", true);
		
		$profile = $this->pickProfile();
		if($profile === false){
			// profile select dialog display
			return $this->render();
		}
		
		applog("Proceeding with profile {$profile['id']}");
		
		if(!empty($_POST['category']) && is_numeric($_POST['category'])){
			
			$skill = new Skill($_POST['category']);
			
			$object = new Profile($profile['id']);
			$cat = $object->hasCategory($skill->id);
			
			if(!empty($cat)){
				$_POST['id'] = $cat->id;
				$this->view->message = "You already subscribe for this category";
				$this->subscription_view();
				return;
			}
			
			if($skill->ready() && !$skill->isDeleted()){
				
				$object = isset($object) ? $object :  new Profile($profile['id']);
				
				if(empty($object)){
					$this->view->error = "Talent or Agency profile not found";
					applog("Profile not found, abort");
					return $this->render();	
				}
												
				$packages = $skill->getPackages();
				if(empty($packages)){
					applog("No packages found for  skill {$skill->id}, abort");
					$this->view->error = 'This category is temporary unavailable';
					return $this->render();	
				}
								

				// if there are only one package, and if it is free
				if(count($packages) == 1 && $packages[0]->price == 0){
					// automatically subscribed to package
					applog("There is only one free package for the skill, adding it");
					
					// procced to next step
					$_POST['id'] = $packages[0]->id;
					
					$this->subscription_package();
					return;
					
				}
				else{
					applog("There are many packages, showing package options");
				//	$this->view->category = $category;
					$this->view->skill = $skill;
					$this->view->packages = $packages;
					$this->view->status = true;
					$this->view->html = $this->component('subscription/dashboard_packages');
					$this->view->id = 'dash-contents';
				}
				
			}
			else {
				$this->view->error = "This category is no longer available";
				applog("Selected skill {$skill->id} is no longer available");
			}
			
			
			
		}
		else{
			$this->view->error = "Invalid request";	
			applog("Required parameters missing");
		}
			
		$this->render();
	}
	
	/**
	* Show payment form for subscriptions
	* Process subscription adding logics
	*/
	public function subscription_package () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

		applog('Subscription package setting request recieved', true);
		
		$profile = $this->pickProfile();
		if($profile === false){
			// profile select dialog display
			return $this->render();
		}
		
		applog("Proceeding with profile {$profile['id']}");
		
		if(!empty($_POST['id']) && is_numeric($_POST['id']) ){			
		
			$package = new Package($_POST['id']);					
						
			// choose right object				
			$object = new Profile($profile['id']);
					
			if($package->isDeleted() || $package->active == 0){
				applog("{$package->id} is deleted or not active, abort");
				// showing subscription adding form
				$this->view->error = "Selected plan is not available.<br>Please selct a different plan";

				return $this->render();
			}
			
			// check if user already has a subscription
			$category = $object->hasCategory($package->skill_id);

			if(!empty($category)){
				applog("Profile already has {$package->skill_id} skill category :".$category->id);	
			}
			else{
				$category = $object->addCategory($package->skill_id);
				applog("Skill {$package->skill_id} added to profile  category :".$category->id);
			}
			
			//$category->package_id = $package->id;
			//$category->save();
						
			$engine = new SubscriptionEngine($category, $package);
			
			if($engine->isNew()){
				applog("Adding new subscription");
				
				$engine->addSubscription();
				
				if($engine->invoice->status == DataType::$INVOICE_PENDING){
					applog("Subscription created a pending invoice {$engine->invoice->id}, showing payment form");
					$this->view->invoice = $engine->invoice;
					
					// show payment screen
					$this->view->status = true;
					$this->view->html = $this->component('ipg/payment');
					$this->view->id = 'dash-contents';
				}
				else{
					// show confirmation screen
					applog("Subscription created a pending invoice {$engine->invoice->id}, showing confirmation page");
					
					// update category
					$category->package_id = $package->id;
					$category->renewal_date = date('j');
					$category->last_renewal = 'NOW';
					$category->save();
					
					$this->view->invoice = $engine->invoice;
					$this->view->html = $this->component('ipg/success');
					$this->view->id = 'dash-contents';
					$this->view->status = true;
				}				
			}
			else {
				// upgrade/downgrade/reneval
				applog("Adding new subscription");
				
				// cehck if user already attempted to chnage the subscription to same plan
				$nextsub = $category->nextSubscription();
				
				if(!empty($nextsub) && $nextsub['package_id'] == $package->id){
					$this->view->error = "You already changed your plan to {$package->name}.<br><br>It will take effect from ".Util::ToDate($nextsub['begin_date']).".";
					return $this->render();
				}
				
				// closing existing pending invoices and pening subscriptions
				$ssub = $category->cancelPendingChanges();
				
				$engine->addSubscription();
				$processor = new PaymentProcessor();				
				
				if($ssub !== true){
					// closing pensing stripe subscription
					$processor->cancelSubscription($ssub->stripe_id, true);
					applog("Pening stripe subscription {$ssub->stripe_id} terminated");
				}
				

				if($engine->invoice->amount > 0) {

					// activate new subscription
					$res = $processor->createSubscription($engine->invoice, true);				

					if(!empty($res) && !empty($processor->subscription)){

						// deactivate exitsing subscription			
						$stripesub = $category->getActiveStripeSubscription();

						if(!empty($stripesub)){

							applog("New sebscripton {$processor->subscription->id} created. Cancelling existing active subscription {$stripesub->stripe_id}");

							$cancelled = $processor->cancelSubscription($stripesub->stripe_id);

							if(!empty($cancelled) && !empty($cancelled->cancel_at_period_end)){

								$this->view->invoice = $engine->invoice;
								$this->view->html = $this->component('ipg/success');
								$this->view->id = 'dash-contents';
								$this->view->status = true;

								applog("Plan changed  {$stripesub->stripe_id} cancelled, {$processor->subscription->id} activated");

							}
							else{
								//' notify insident to tech	m rooling back						
								applog("Error cancelling current sub {$stripesub->stripe_id}, rollbacking ");

								// cancelling new subscriptions
								$np = $processor->cancelSubscription($engine->subscription->id, true);

								if($np && $np->status == 'canceled'){
									applog("New stripe subscription {$engine->subscription->id} canceled");

									$sr = $engine->invoice->getSubscription()->delete();
									$engine->invoice->delete();
									$category->getStripeSubscription($engine->subscription->id)->delete();

									applog("Local DB records deleted, showing error screen");

									//$this->view->invoice = $engine->invoice;
									$this->view->message = "There was an error with updating your current subscription. Your currect plan will continue, No changes made.";
									$this->view->html = $this->component('ipg/error');
									$this->view->id = 'dash-contents';
									$this->view->status = true;	

								}
								else{
									applog("Error cancelling new subscription {$engine->subscription->id}, rollback failed");
								}


								applog("Error cancelling existing subscrip {$stripesub->stripe_id}, abort ");
							}
						}
						else{
							applog("No previous active subscripton found");

							$this->view->invoice = $engine->invoice;
							$this->view->html = $this->component('ipg/success');
							$this->view->id = 'dash-contents';
							$this->view->status = true;

							applog("Plan changed  {$processor->subscription->id} activated");
						}
					}			
					else{
						applog("New subscription creation failed, showing error page");
						//' notify insident to tech
						$this->view->invoice = $engine->invoice;
						$this->view->message = "There was an error with updating your current subscription. Your currect plan will continue, No changes made.";
						$this->view->html = $this->component('ipg/error');
						$this->view->id = 'dash-contents';
						$this->view->status = true;							

						applog("Error creating new subscription, abort ");
					}
					
				}
				else{
					// going with a free plan
					// deactivate exitsing subscription			
					$stripesub = $category->getActiveStripeSubscription();
					
					if(!empty($stripesub)) {
					
						$cancelled = $processor->cancelSubscription($stripesub->stripe_id);

						if(!empty($cancelled) && !empty($cancelled->cancel_at_period_end)){

							$this->view->invoice = $engine->invoice;
							$this->view->html = $this->component('ipg/success');
							$this->view->id = 'dash-contents';
							$this->view->status = true;

							applog("Plan changed  {$stripesub->stripe_id} cancelled, Free plan activated");

						}
						else{
						// rollback new subscription\
						$engine->subscription->delete();
						$engine->invoice->delete();
						
						applog("Local DB records deleted, showing error screen");
						
						$this->view->message = "There was an error with updating your current subscription. Your currect plan will continue, No changes made.";
						$this->view->html = $this->component('ipg/error');
						$this->view->id = 'dash-contents';
						$this->view->status = true;	
							
						
						// end free activatioon
					}
					
					}
					else{
						
						$this->view->invoice = $engine->invoice;
						$this->view->html = $this->component('ipg/success');
						$this->view->id = 'dash-contents';
						$this->view->status = true;

						applog("Plan changed , {$engine->subscription->id} activated");
					}
					
				}
				
			}
			
					
						
		}
		else{
			$this->view->error = "Invalid request";	
		}
			
		$this->render();
	
	}
	
		
	/**
	* Show subscription/category details
	*/
	public function subscription_view () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		$profile = $this->pickProfile();
		
		applog("Requesting to view subscription", true);
		
		if(!empty($profile)) {		

			if(!empty($_POST['id'])){

				$category = new Category($_POST['id']);

				if($category->profile_id == $profile['id']){

					$this->view->category = $category;

					$this->view->html = $this->component('subscription/view');
					$this->view->status = true;
					$this->view->id = 'dash-contents';
					
					$this->view->page = array(
					'title' => 'Subscription', 
					'url' => '/dashboard/subscription/'.$category->id
				 );

				}
				else{
					applog("profile has no access to {$category->id} subscription categoty");
				}

			}
			else{
				applog("Required parameters missing in request, abort");
				$this->view->error = "Invalid request";
			}
		}
		
		$this->render();	
	}
	
	/**
	* Request to cancel subscription
	*/
	public function subscription_cancel () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Subscription cancel request received", true);
		
		$profile = $this->pickProfile();
			
		if(!empty($profile)) {	
			
			if(!empty($_POST['id'])){
				
				$category = new Category($_POST['id']);
				
				if($category->profile_id != $profile['id']){
					$this->view->error = "Unathorized access";
					return $this->render();
				}
				
				if($category->isDeleted() ){
					$this->view->error = "This category has already canceled.";
					$this->view->id = 'package-change';
					$this->view->html= '<div></div>';
					return $this->render();
				}

				if($category->renewal == 0){
					$this->view->error = "This category has scheduled cancel.";
					$this->view->id = 'package-change';
					$this->view->html= '<div></div>';
					return $this->render();
				}
				
				$this->view->category = $category;
				$this->view->html = $this->component('subscription/cancel');					
				$this->view->id = 'package-change';
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Invalid request";
			}
			
		}		
		
		$this->render();		
	}
	
	/**
	* Closing the subscription
	* End stripe subscription (if any) and delete current one
	**/
	public function subscription_close () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("Subscription cancel confirmed", true);
				
		$profile = $this->pickProfile();
			
		if(!empty($profile)) {				

			if(!empty($_POST['id'])){
				
				$category = new Category($_POST['id']);

				if($category->profile_id == $profile['id']){
					
					$info = $category->getInfo();	
					
					applog("Cancelling stripe subscriptions \n".json_encode($info['stripe']));
					
					$processor = new PaymentProcessor();
					
					if(!empty($info['stripe']['active'])){
						$sup = $processor->cancelSubscription($info['stripe']['active']);
						applog("Active subscription canceled ".$info['stripe']['active']);
					}
					
					if(!empty($info['stripe']['pending'])){
						$processor->cancelSubscription($info['stripe']['pending']);
						applog("Pending subscription canceled ".$info['stripe']['pending']);			
					}
					
					if(empty($info['stripe']['active']) && empty($info['stripe']['pending'])){
						
						applog("No  active or pending stripe subscriptions, end subscription immiditely");
						
						$records = $category->getActiveSubscriptions();						
						if(!empty($records)){
							foreach ($records as $sub){														
								$sub->delete();
								applog("Subscription {$sub->id} deleted");						
								
							}
						}
												
						$category->delete();
						applog("Category {$category->id} deleted, Subscription terminated");
						
						$this->view->message = "Subscription canceled";
					}
					else{
						// subscription ends at next billing date
						$category->renewal = 0;
						$category->save();
						applog("Reneval turned off for category {$category->id}");
						
						$this->view->message = "Subscription will be canceled";
					}
					
					$invoices = $category->getPendingInvoices();
					if(!empty($invoices)){	
						
						foreach ($invoices as $inv){
							if($inv->type == DataType::$INVOICE_TYPE_NEW_SUBSCRIPTION ||
							   $inv->type == DataType::$INVOICE_TYPE_RENEW_SUBSCRIPTION ||
							   $inv->type == DataType::$INVOICE_TYPE_DOWN_SUBSCRIPTION ||
							   $inv->type ==  DataType::$INVOICE_TYPE_UP_SUBSCRIPTION  )
							$inv->delete();
							applog("Invoice {$inv->id} deleted");
						}
						
					}
					
					// showing results
					$this->view->category = $category;								
					$this->view->html = $this->component('subscription/view');
					$this->view->status = true;
					$this->view->id = 'dash-contents';
					
				}
				else{
					$this->view->error = "Unauthorized access";
				}
				
			}
		}
		
		$this->render();		
	}
	
	/**
	* Rollback plan changes
	*/
	public function subscription_change_cancel () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		applog("plan change cancel request received", true);
		
		$profile = $this->pickProfile();
			
		if(!empty($profile)) {			
			
			if(!empty($_POST['id'])){			

				$category = new Category($_POST['id']);
				
				// delete pending invoices
				$active = $category->getActiveSubscription();
				
				if(strtotime($active->exp_date) < time()){
					$this->view->error = "Subscription expired, can not cancel changes";
					return $this->render();					
				}
				
				if($active->isEmpty('catsub_id')){
					$this->view->error = "Changes can not be canceled";
					return $this->render();	
				}
				
				$catsub = $category->cancelPendingChanges();
				if($catsub !== true){
					applog("Cancelling scheduled subscription");
					
					$processor = new PaymentProcessor();
					$sub = $processor->cancelSubscription($catsub->stripe_id, true);
					applog("Scheduled subscription cancelled\n".json_encode($sub));
					// check if cancelled
				}
				
				$this->view->category = $category;

				$this->view->html = $this->component('subscription/view');
				$this->view->status = true;
				$this->view->id = 'dash-contents';
				$this->view->message = "Plan changes cancelled";
				
			}
		
		}
		
		$this->render();
	}
	
	
	/**
	* Show package edit screen
	*/
	public function subscription_edit () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		$profile = $this->pickProfile();
			
		if(!empty($profile)) {				

			if(!empty($_POST['id'])){			

				$category = new Category($_POST['id']);

				if($category->profile_id == $profile['id']){
					
					if($category->isDeleted() ){
						$this->view->error = "This category has canceled.<br>Can not change the package";
						$this->view->id = 'package-change';
						$this->view->html= '<div></div>';
						return $this->render();
					}
					
					if($category->renewal == 0){
						$this->view->error = "This category has scheduled cancel.<br>Can not change the package";
						$this->view->id = 'package-change';
						$this->view->html= '<div></div>';
						return $this->render();
					}

					$this->view->category = $category;

					$this->view->html = $this->component('subscription/package_change');
					$this->view->status = true;
					$this->view->id = 'package-change';

				}
				else{
					$this->view->error = "Unauthorized access";
				}

			}
			else{
				$this->view->error = "Invalid request";
			}
		
		}
		
		$this->render();
		
	}
	
	/**
	* Update subscription package
	*/
	public function subscription_package_update () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		applog("package change request received", true);
		
		$profile = $this->pickProfile();
			
		if(!empty($profile)) {
			
			if(!empty($_POST['id']) && is_numeric($_POST['id'])){
				
				$object = new Profile($profile['id']);
				
				$package = new Package($_POST['id']);
								
				if($package->isDeleted() || $package->active == 0){
					$this->view->error = "Selected plan is not available";
					applog("Package {$package->id} is not available, abort");
					return $this->render();
				}
				
				applog("Requesting to change plan to {$package->id}");
			
				$category = $object->hasCategory($package->skill_id);
								
				if(empty($category)){
					applog("There is no subscription category for {$package->skill_id}, showing add subscription form ");
					
					// show subscription form
					$this->subscription_add();
					return;
				}
				
				// check if user tries to chnage it to same backage before
				$nextsub = $category->nextSubscription();
				
				if(!empty($nextsub) && $nextsub['package_id'] == $package->id){
					$this->view->error = "You already changed your plan to {$package->name}.<br><br>It will take effect from ".Util::ToDate($nextsub['begin_date']).".";
					return $this->render();
				}
				
				// show invoice summery
				applog("Package change possible, showing invoice preview");
				
				$this->view->category = $category;
				$this->view->package = $package;
				
				$this->view->html = $this->component('subscription/package_change_confirm');
				$this->view->status = true;
				$this->view->id = 'package-change';
				
			}
			else{
				$this->view->error = "Invalid request";
			}
			
			
		}
		
		$this->render();
	}
	
	
	/*
	public function subscription_package_update () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(!empty($_POST['id'])){
			
			$subscription = new Subscription($_POST['id']);
			
			if($subscription->hasPermissionTo($this->user->id)){
				
				if(!empty($_POST['new_package']) && is_numeric($_POST['new_package'])) {
					
					$package = new Package($_POST['new_package']);
				
					$this->view->subscription = $subscription;
					
					$renewal = $subscription->getAutoReneval();
					$expackage = $subscription->getPackage();					
					
					if($package->skill_id != $expackage->skill_id){
						$this->view->error = "Invalid package";
					}
					elseif($package->active != 1 || $package->isDeleted()){
						$this->view->error = "This package is not available";	
					}
					elseif ($package->id == $expackage->id && $package->id == $subscription->package_id ) {
						
						if($package->id != $renewal->package_id){
							$renewal->package_id = $package->id;
							$renewal->save();
						}
						// nothing updated
						$this->view->html = '<div></div>';
						$this->view->status = true;
						$this->view->id = 'package-change';
					}
					else{
						$renewal->package_id = $package->id;
						$renewal->save();
						
						$balance = $subscription->getBalance();
						$required = ($renewal->term == 'm') ? $package->price_monthly : $package->price_annualy;
						$switchnow = ($balance['balance'] <= $required);
						//print_r($balance);
					
						//package_change_notification
						$this->view->html = '<div class="sep"></div><div class="bg-blue panel padding ">Package will be switched to <span class="txt-bold txt-blue">'.$package->name.'</span> on next revewal date.'.($switchnow ? '<div class="sep"></div><a cura="subscription_package" data-id="'.$package->id.'" data-category="'.$package->skill_id.'" >Switch immidiately.</a>' : '').'</div>';
						$this->view->status = true;
						$this->view->id = 'package-change';
					}
												
					
				
				}
				else{
					$this->view->error = "Please select a package";
				}
				
			}
								
		}
		
		$this->render();
		
	}
	*/
	
	/**
	* Change simultaneous bookings 
	*/
	public function simultaneous_change () {
		
		$profile = $this->pickProfile();
		
		if(empty($profile)){
			$this->view->error = "Please switch to talent or agency profile";
			return $this->render();
		}
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$subscription = new Subscription($_POST['id']);
			$this->view->subscription = $subscription;
			$this->view->html = $this->component('subscription/simultaneous');
			$this->view->status = true;
			$this->view->id = 'simultaneous-change';
			
		}
		else{
			$this->view->error = "Invalid parameters";			
		}
		
		$this->render();
	}
	
	public function simultaneous_update () {
		
		$profile = $this->pickProfile();
		
		if(empty($profile)){
			$this->view->error = "Please switch to talent or agency profile";
			return $this->render();
		}
		
		if(!empty($_POST['id']) && is_numeric($_POST['id']) && !empty($_POST['simultaneous']) && is_numeric($_POST['simultaneous'])){
			
			$bookings = $_POST['simultaneous'];
			
			if($bookings < 1){
				$bookings = 1;
			}
			elseif($bookings > 1000){
				$bookings = 1000;
			}
			
			$subscription = new Subscription($_POST['id']);
			
			if(($profile['type'] == 't' && $subscription->talent_id = $profile['id'])|| ($profile['type'] == 'a' && $subscription->agency_id == $profile['id'])){
				
				$subscription->simultaneous = $bookings;
				$subscription->save();

				$this->view->html = $bookings.($bookings > 1 ? ' bookings' : ' booking');
				$this->view->status = true;
				
				$this->view->nodes = array(
					array('id' => 'simultaneous-bookings', 'html' => $bookings.($bookings > 1 ? ' bookings' : ' booking')),
					array('id' => 'simultaneous-change', 'html' => '<div></div>'),
				);

				
			}
			else{
				$this->view->error = "You have no access to change this";

			}
			
		}
		else{
			$this->view->error = "Invalid parameters";
		}
		
		$this->render();
		
	}
	
	/**
	* Dispaly payment method change form
	*/
	public function subscription_paymentmethod () {
		
		if(!empty($_POST['id'])){
			
			$subscription = new Subscription($_POST['id']);
			
			if($subscription->hasPermissionTo($this->user->id)){
				
				$this->view->subscription = $subscription;
						
				$this->view->html = $this->component('subscription/payment_methods');
				$this->view->status = true;
				$this->view->id = 'payment-change';
				
			}
								
		}
		
		$this->render();
	}
	
		
	/**
	* Update payment method in reneval
	*/
	public function renewal_method_update () {
		
		if(!empty($_POST['id']) && !empty($_POST['payment_method']) && is_numeric($_POST['payment_method'])){
			
			$subscription = new Subscription($_POST['id']);
			
			if($subscription->hasPermissionTo($this->user->id)){
				
				$this->view->subscription = $subscription;
				$renewal = $subscription->getAutoReneval();
				$method = new PaymentMethod($_POST['payment_method']);
				
				if($method->customer_id = $this->user->id){
					
					$renewal->method_id = $method->id;
					$renewal->save();
				
					$this->view->html = '<div></div>';
					$this->view->status = true;
					$this->view->id = 'payment-change';
					$this->view->message = "Payment method updated";
					
					$this->view->nodes = array(
						array( 'id' => 'payment_method_name', 'html' => $method->name)
					);				
				}
				else{
					$this->view->error = "Unauthorized request";	
				}
								
			}
								
		}
		
		$this->render();
	}
	
	
	
	
	/**
	* Adding a new payment method form
	*/
	public function payment_method_add () {
	
		
		$this->view->data = array(
			'title' => 'New Payment Method',
			'message' => $this->component('new_payment_method'),
			'width' => 400,
			'mask' => true,
			'buttons' => array(
					'ok' => array('label' => 'Add Card', 'action' => 'payment_method_update'),
					'cancel' => array('label' => 'Cancel', 'class' => 'button-alt')
				)
		);
		$this->view->action = 'dialog';
		$this->view->status = true;
		
		$this->render();
	}
	
	/**
	* Adding a new payment method for a client
	*/
	public function payment_method_update () {
		
		$number = (!empty($_POST['payment_card_num'])) ? $_POST['payment_card_num'] : '';
		$cvv = (!empty($_POST['payment_card_cvv'])) ? $_POST['payment_card_cvv'] : '';
		$month = (!empty($_POST['card_exp_month'])) ? $_POST['card_exp_month'] : '';
		$year = (!empty($_POST['card_exp_year'])) ? $_POST['card_exp_year'] : '';
		
		
		if(!empty($number) && !empty($year) && !empty($month) && !empty($cvv)){
			
			$number = preg_replace("/[^0-9]/", "", $number);
			
			$type = Validator::getCardType($number);
			
			if(empty($type)){
				$this->view->error = 'Invalid card number';
				return $this->render();
			}
			
			if(strlen($cvv) < 3 || strlen($cvv) > 4 || !is_numeric($cvv)){
				$this->view->error = 'Invalid CVV';
				return $this->render();
			}
			
			if(!strlen($year) == 2 || !is_numeric($year)|| $year < date('y')){
				$this->view->error = 'Invalid card exp. year';
				return $this->render();
			}
			
			if(!is_numeric($month) || $month < 1 || $month > 12){
				$this->view->error = 'Invalid card exp. month';
				return $this->render();
			}
			
			if($year == date('y') && $month < date('n')){
				$this->view->error = 'Invalid card exp. month';
				return $this->render();
			}
			
			
			$method = new PaymentMethod();
			$method->customer_id = $this->user->id;
			$method->type = $type;
			$method->name = strtoupper($type).' - '.substr($number, -4,4);
			$method->card_name = $this->user->firstname.' '.$this->user->lastname;
			$method->card_number = $number;
			$method->card_exp =  $month.'/'.$year;
			$method->ccvv = $cvv;
			//$method->token = Validator::cleanup($_POST['method_token'], 400);
			//$method->reference = Validator::cleanup($_POST['method_reference'], 1000);
			$method->save();
			
			$this->view->status = true;
			$this->view->message = "Payment method added";
			$this->view->action = 'refresh_payment_methods';
			
			
		}
		else{
			$this->view->error = "Invalid request";	
		}
		
		$this->render();
	}
	
	
	
	/**
	* Get recent payments for subscription plans
	*/
	public function plan_payment_history () {
		
		if(!empty($_POST['id'])){
			
			$subscription = new Subscription($_POST['id']);
			
			if($subscription->hasPermissionTo($this->user->id)){
				
				$this->view->subscription = $subscription;
				
				$this->view->payments = $subscription->getPaymentHistory();
				$this->view->html = $this->component('subscription/payment_history');
				$this->view->status = true;
				$this->view->id = 'payment-history';
				
			}
								
		}
		
		$this->render();
			
	}
	
	/**
	* Switch working schedule
	*/
	public function switch_working_schedule (){
		
		$profile = $this->pickProfile();
		if(!empty($profile) && $profile['type'] == 't'){
			
			$talent = new Profile($profile['id']);
			if($talent->customer_id == $this->user->id){
				
				$talent->limited_hours = (empty($_POST['value'])) ? 0 : 1;
				$talent->save();
				
				$this->view->message = "Working schedule updated";
				$this->view->status = true;
				
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Switch working days
	*/
	public function switch_working_days () {
		
		$profile = $this->pickProfile();
		if(!empty($profile) && $profile['type'] == 't'){
			
			$talent = new Profile($profile['id']);
			if($talent->customer_id == $this->user->id){
								
				if(!empty($_POST['id']) && isset($_POST['working'])){
					
					$schedule = $talent->getSchedule();
					
					if($schedule->hasProperty($_POST['id'])){
						$key = $_POST['id'];
						$schedule->$key = (!empty($_POST['working'])) ? 1 : 0;
						$schedule->update();
						
						$this->view->message = "Working time updated";
						$this->view->status = true;
					}
										
				}
			}
			
		}
		
		$this->render();
	}
	
	public function working_time_update () {
	
		$profile = $this->pickProfile();
		if(!empty($profile) && $profile['type'] == 't'){
			
			$talent = new Talent($profile['id']);
			if($talent->customer_id == $this->user->id){
								
				if(!empty($_POST['name']) && !empty($_POST['value'])){
					
					$schedule = $talent->getSchedule();
					
					if($schedule->hasProperty($_POST['name'])){
						$key = $_POST['name'];
						$time = Util::timeToStamp($_POST['value']);
						$schedule->$key = $time;
						$schedule->update();
						
						$this->view->message = "Working time updated";
						$this->view->status = true;
					}
										
				}
			}
			
		}
		$this->render();
	}
	
	/**
	* Show talent/Agency handler edit
	*/
	public function handler_edit () {
	
		
		$session = $this->pickProfile();
			
		if(empty($session)){

			$profiles = $this->user->getProfiles();

			if(count($profiles) == 1){
				$object = $profiles[0];
				applog("No session, Auto profile {$object->id} selected");

			}
			else {
				applog("There are  {count($profiles)}, asking user to choose");
				$this->view->error = "Please select a profile";	
				return  $this->render();
			}				
		}
		else{
			$object = new Profile($session['id']);
		}

		if($object->customer_id != $this->user->id){

			applog("User {$this->user->id} has no permision to edit profile {$object->id}, abort");

			$this->view->error = "Unauthorized access";	
			return  $this->render();
		}
		
		
		$this->view->page = $object->getPage();
		$this->view->html = $this->component('handler_update');
		$this->view->id = 'handler_edit';
		$this->view->status = true;
		
		$this->render();
	}
	
	/**
	* Update page handle
	*/
	
	public function handler_update () {
		
		$this->view->status = false;
	
		$handle = (!empty($_POST['page_handle'])) ? $_POST['page_handle'] : NULL;
		
		if(!empty($handle)) {
			
			$handle = Validator::cleanup($handle, 20);
			$handle = str_replace(' ','-',$handle);
			$handle = preg_replace("/[\+\!\#\$\~%\^\&\*\(\)\{\}\[\]\'\"\|\/\<\>\,`\=\\@\:\;\?]/",'', $handle);
			
			if(strlen($handle) < 4 || strlen($handle) > 20){
				$this->view->error = "Page handle should have 4 - 20 characters";	
				return $this->render();
			}
			
			if(Validator::isReservedWord($handle)){
				
				$this->view->error =  "<span class=\"txt-bold\">{$handle}</span> is a reserved word.<br>Please choose another.";
				return $this->render();	
			}
			
			if(is_numeric($handle)){
				$this->view->error = "Handle should have at least one alphabet character.";
				return $this->render();	
			}
			
			
			if(!empty($profile) && $profile['type'] == 't'){
				
				$talent = new Talent($profile['id']);
				$page = $talent->getPage();
			}
			elseif(!empty($profile) && $profile['type'] == 'a'){
					
				$agency = new Agency($profile['id']);
				$page = $agency->getPage();
				
			}
			elseif(!empty($_POST['page']) && is_numeric($_POST['page'])){
				
				$page = new Page($_POST['page']);
				
			}
						
			if($page->customer_id != $this->user->id){
				$this->view->error = "Unauthorized request";
				$this->render();	
			}
			
			// check if it is avialable
			if(!App::isHandlerAvailable($handle) && $page->handle != $handle){
				$this->view->error = "<span class=\"txt-bold\">{$handle}</span> is already taken.<br>Please choose another one.";	
				
				return $this->render();	
			}
			
			if($page->handle != $handle) {
				$page->handle = strtolower($handle);
				$page->handle_updated = 1;
				$page->save();
			}
			
			$this->view->status = true;
			$this->view->message = "Page handle updated";
			
		
		}
		
		
		if($this->view->status){
			
			if(!empty($_POST['afteredit'])){
				$this->view->action = 'redirect';
				$this->view->data = '/'.$page->handle;
			}
			else {
				
				$this->view->nodes = array(
						array(
							'id' => 'handler_edit',
							'html' => "<div></div>"
						),
						array (
							'id' => 'profile_handle',
							'html' => '<a target="_blank" href="'.Util::mapURL('/').urlencode($handle).'">@'.$handle.'</a>'
						)
					);
				
			}
			
				
		}
		
		$this->render();
				
	}
	
	
	
	
	/**
	* Updating profile banner
	*/
	public function page_update_banner () {
	
		$this->view->status = false;
		$this->view->message = "Unauthorized request";
		
		$banner_width = 1920;
		$banner_height = 520;
		
		if(!empty($_POST['page_id'] )&& is_numeric($_POST['page_id']) && !empty($_POST['image'])){
			
			$page = new Page($_POST['page_id']);
			if($page->customer_id != $this->user->id) {
				$this->view->error = "Unauthorized access";	
				return $this->render();;
			}
			
			$imgdata = explode(';base64,',urldecode($_POST['image']));
			$ext = explode('/',$imgdata[0]);
			
			$ext = $ext[1];
			
			if($ext == 'jpeg'){
				$ext = 'jpg';	
			}
			
			$fid = $this->user->id."_".time()."_".rand(1000,9999).'.'.$ext;
			$path = Config::$base_path . "/tmp/" . $fid;
			
			$imgdata[1] = str_replace(' ', '+', $imgdata[1]);
			file_put_contents($path, base64_decode($imgdata[1]));
			
			if ($ext == 'jpg' || $ext == 'jpeg') {
            	$src = @imagecreatefromjpeg($path);
			} elseif ($ext == 'png') {
				$src = imagecreatefrompng($path);
			} elseif ($ext == 'gif') {
				$src = imagecreatefromgif($path);
			} else {
				$this->view->message = "Unsupported file format";
				$this->render();
				return false;
			}
			
			$width = (!empty($_POST['width'])) ? $_POST['width'] : $banner_width;
			$height = (!empty($_POST['height'])) ? $_POST['height'] : $banner_height;
			$scale = (!empty($_POST['scale'])) ? $_POST['scale'] : 1;
			$top = (!empty($_POST['top'])) ? $_POST['top'] : 0;
			$left = (!empty($_POST['left'])) ? $_POST['left'] : 0;
			
			$crop_length = round($width/$scale);
			$crop_height = round($height/$scale);
			$y = round(($top * -1)/$scale);
			$x = round(($left * -1)/$scale);
			
			//echo $scale." ".$crop_length.' '.$crop_height;
			
			$thumb = imagecreatetruecolor($banner_width, $banner_height);
        	$white = imagecolorallocate($thumb, 255, 255, 255);
        	imagefill($thumb, 0, 0, $white);
			
			imagecopyresampled($thumb, $src,0, 0,$x, $y, $banner_width, $banner_height, $crop_length, $crop_height);
			
			 // save image
			imagejpeg($thumb, $path, 100);

			imagedestroy($thumb);
			imagedestroy($src);
			
			$thumb = "contents/thumb/" . $fid;
			Util::generateThumbImage($path, $thumb, 120);			
			
			$full = "contents/images/" . $fid;
			$ori = Util::resizeMoveImage($path, $full, $banner_width);
			
			// crating image
			$image = new Image();
			$image->customer_type = 't';
			$image->customer_id = $this->user->id;
			$image->i600 = Config::$image_url."/contents/images/" . $fid;
			$image->i400 = Config::$image_url."/contents/images/" . $fid;
			$image->thumb = Config::$image_url."/contents/thumb/" . $fid;
			$image->url = Config::$image_url."/contents/images/" . $fid;
			//$image->orientation = $ori;
			$image->save();
			
			// assign to user
			$page->banner_id = $image->id;
			$page->save();
			
			$this->view->data = $image->getData();
			
			$this->view->status = true;
			//$this->view->action = "page_banner_updated";
			$this->view->message = "Banner updated";
			
		}
		
		
		$this->render();
		
	}
	
	/**
	* upload and save image for page parts
	*/
	public function page_update_image () {
		
		applog("Page image update request received, customer {$this->user->id}");
		
		$sec = $_POST['section'];
		
		if(!in_array($sec, array('a','b','c','d','e','f','g','h'))) {
			$this->view->error = "Invalid parameters";	
			applog("Invalid section: ".$sec.", abort");
			return $this->render();
		}
		
		if(!empty($_POST['page_id'] )&& is_numeric($_POST['page_id'])  && !empty($_POST['section']) ){
			
			
			if(empty($_FILES['image']) && (empty($_POST['image_id']) || !is_numeric($_POST['image_id']))){
				applog("Image data not present in the post, abort");
				$this->view->error = "Please select an image";
				return $this-->render();				
			}
			
			$page = new Page($_POST['page_id']);
			
			if($page->customer_id != $this->user->id) {
				applog("Page {$page->id} is not belongs to customer {$this->user->id}, abort");
				$this->view->error = "Unauthorized access";	
				return $this->render();;
			}
			
			
			// image upload scenario
			if(!empty($_FILES['image'])) {
				
				applog("permission OK, customer  {$this->user->id} uploading image to page {$page->id}, section {$sec}");
				
				$uploaded = $_FILES['image']['name'];
				$tmp = $_FILES['image']['tmp_name'];
				$ext = strtolower(pathinfo($uploaded, PATHINFO_EXTENSION));
				
				$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp' );
				
				if(!in_array($ext, $valid_extensions)) {
					applog("Invalid file extention {$ext}, abort");
					$this->view->error = "Invalid image format";	
					return $this->render();;
				}
				
				$fid = $this->user->id."_".time()."_".rand(1000,9999).'.'.$ext;
				$path = Config::$base_path . "/tmp/" . $fid;
				
				if(!move_uploaded_file($tmp,$path)) {
					applog("Image can not move to temp folder");					
					$this->view->error = "Image upload error";	
					return $this->render();;
				}
				
				applog("Image uploaded, resizing image for section {$sec}");
				
				// resize image
				if ($ext == 'jpg' || $ext == 'jpeg') {
					$src = @imagecreatefromjpeg($path);
				} elseif ($ext == 'png') {
					$src = imagecreatefrompng($path);
				} elseif ($ext == 'gif') {
					$src = imagecreatefromgif($path);
				}
				
				$size = getimagesize($path);
				$width = $size[0];
				$height = $size[1];
								
				// resize image based on each sectional requirements
				if($sec ==  'a' || $sec == 'f'){
				// big image in profile description
				// scale down to 1200px max
					$max = 1200;
					
					if($width > $max){
						$delta = $max/$width;
						$width = $max;
						$height = round($height*$delta);	
					}
					
					if($height > $max){
						$delta = $max/$height;
						$height = $max;
						$width = $width*$delta;	
					}
				
					$canvas = imagecreatetruecolor($width, $height);
					
					if($ext == 'png' || $ext == 'gif'){
						
						imagealphablending($canvas, false);
						$white = imagecolorallocatealpha($canvas, 0, 0, 0, 127);
						imagefill($canvas, 0, 0, $white);
						
						imagecopyresampled($canvas, $src,0, 0,0,0, $width, $height, $size[0], $size[1]);
						imagesavealpha($canvas, true);
						imagepng($canvas, $path,0);
					}
					else {					
						
						$white = imagecolorallocate($canvas, 255, 255, 255);
						imagefill($canvas, 0, 0, $white);
						imagecopyresampled($canvas, $src,0, 0,0,0, $width, $height, $size[0], $size[1]);
						imagejpeg($canvas, $path, 95);
					}
					
					applog("Image resized and saved");
				
				}
				elseif ($sec == 'b'){
					// small image for service description
					$max = 300;					
					
					$mark = (!empty($_POST['mark']) && is_numeric(empty($_POST['mark']))) ? $_POST['mark'] : 200;
					$scale = (!empty($_POST['scale']) && is_numeric($_POST['scale'])) ? $_POST['scale'] : 1;
					$top = (!empty($_POST['top']) && is_numeric($_POST['top'])) ? $_POST['top'] : 0;
					$left = (!empty($_POST['left']) && is_numeric($_POST['left'])) ? $_POST['left'] : 0;
					
					$crop_length = round($mark/$scale);
					
					if($crop_length > $max){
						$width = $max;
					}
					else{
						$width = $crop_length;
					}
					
					$y = round((100 - $top)/$scale);
					$x = round((100 - $left)/$scale);
					
					$canvas = imagecreatetruecolor($width, $width);
					
					if($ext == 'png' || $ext == 'gif'){
						
						imagealphablending($canvas, false);
						$white = imagecolorallocatealpha($canvas, 0, 0, 0, 127);
						imagefill($canvas, 0, 0, $white);
						
						imagecopyresampled($canvas, $src,0, 0,$x, $y,$width, $width, $crop_length, $crop_length);
						imagesavealpha($canvas, true);
						imagepng($canvas, $path,0);
					}
					else {					
						
						$white = imagecolorallocate($canvas, 255, 255, 255);
						imagefill($canvas, 0, 0, $white);
						imagecopyresampled($canvas, $src,0, 0,$x, $y,$width, $width, $crop_length, $crop_length);
						imagejpeg($canvas, $path, 95);
					}
					
				}
				elseif ($sec == 'd'){
					
					// Achievement image 700x350
					$width = 660;
					$height = 330;
					//$mark = 225;
					$markleft = 35;
					$marktop = 142;
					
					$scale = (!empty($_POST['scale'])) ? $_POST['scale'] : 1;
					$top = (!empty($_POST['top'])) ? $_POST['top'] : 0;
					$left = (!empty($_POST['left'])) ? $_POST['left'] : 0;
					
					$sw = round(($width/2)/$scale);
					$sh = round(($height/2)/$scale);
					$sy = round(($marktop - $top)/$scale);
					$sx = round(($markleft - $left)/$scale);
					
				//	echo $scale.' '.$sx.' , '.$sy.' , '. $tw.' , '. $height.' , '.$sw.' , '. $sh;					
					$canvas = imagecreatetruecolor($width, $height);
					
					if($ext == 'png' || $ext == 'gif'){
						imagealphablending($canvas, false);
						$white = imagecolorallocatealpha($canvas, 0, 0, 0, 127);
						imagefill($canvas, 0, 0, $white);
						imagecopyresampled($canvas, $src,0, 0,$sx,$sy, $width, $height,$sw, $sh);
						imagesavealpha($canvas, true);
						imagepng($canvas, $path,0);
					}
					else{
						$white = imagecolorallocate($canvas, 255, 255, 255);
						imagefill($canvas, 0, 0, $white);
						imagecopyresampled($canvas, $src,0, 0,$sx,$sy, $width, $height,$sw, $sh);
						imagejpeg($canvas, $path, 95);
					}
					
					applog("Image resized and saved");
				} // section D
				
				imagedestroy($canvas);
				imagedestroy($src);
				
				applog("Creating thumb images for {$path}");
				
				$thumb = "contents/thumb/" . $fid;
				Util::generateThumbImage($path, $thumb, 120);
				
				$full = "contents/images/" . $fid;
				$ori = Util::resizeMoveImage($path, $full, $width);
				
				$image = new Image();
				$image->customer_type = 't';
				$image->customer_id = $this->user->id;
				$image->i600 = Config::$image_url."/contents/images/" . $fid;
				$image->i400 = Config::$image_url."/contents/images/" . $fid;
				$image->thumb = Config::$image_url."/contents/thumb/" . $fid;
				$image->url = Config::$image_url."/contents/images/" . $fid;
				$image->width = $width;
				$image->height = $height;
				//$image->orientation = $ori;
				$image->save();
				
				applog("Image record created {$image->id}");
				
			}// upload ends
			elseif (!empty($_POST['image_id']) && is_numeric($_POST['image_id'])){
			// provess existing galery image
				
				$img = new Image($_POST['image_id']);
				
				if($img->customer_id != $this->user->id){
					applog("Customer {$this->user->id} has no access to image {$img->id}, abort");
					$this->view->error = "Unauthorized access";	
					return $this->render();
				}
				
				applog("Processing libraray image {$img->id}");
				
				$paths = $img->getPaths();
				
				// extract extention
				$url = explode('.', $paths['url']);
				$ext = strtolower(array_pop($url));				
				
				// download file
				$fid = $this->user->id."_".time()."_".rand(1000,9999).'.'.$ext;
				
				$s = new Storage();
				$res = $s->download($paths['url'], $fid);
				
				$path = Config::$base_path . "/tmp/" . $fid;
				
				$size = getimagesize($path);
				$width = $size[0];
				$height = $size[1];					
				
				if(!file_exists($path)){
					applog("Image coping error, abort");
					$this->view->error = "Error coping image";	
					return $this->render();
				}	
				
				if ($sec == 'd'){
					
					// Achievement image 550x300
					$width = 660;
					$height = 330;
					//$mark = 225;
					$markleft = 35;
					$marktop = 142;
					
					$scale = (!empty($_POST['scale']) && is_numeric($_POST['scale'])) ? $_POST['scale'] : 1;
					$top = (!empty($_POST['top']) &&  is_numeric($_POST['top'])) ? $_POST['top'] : 0;
					$left = (!empty($_POST['left']) &&  is_numeric($_POST['left'])) ? $_POST['left'] : 0;
					
					$fid = $this->user->id."_".time()."_".rand(1000,9999).'.'.$ext;
					$path = Config::$base_path . "/tmp/" . $fid;
					
					if(!copy($paths['url'], $path)){
						applog("Image coping error ".$path.", abort");
						$this->view->error = "Error coping image";	
						return $this->render();
					}
										
					//$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
					
					// resize image
					if ($ext == 'jpg' || $ext == 'jpeg') {
						$src = @imagecreatefromjpeg($path);
					} elseif ($ext == 'png') {
						$src = imagecreatefrompng($path);
					} elseif ($ext == 'gif') {
						$src = imagecreatefromgif($path);
					}
									
					$sw = round(($width/2)/$scale);
					$sh = round(($height/2)/$scale);
					$sy = round(($marktop - $top)/$scale);
					$sx = round(($markleft - $left)/$scale);
					
					$canvas = imagecreatetruecolor($width, $height);
					
					if($ext == 'png' || $ext == 'gif'){
						imagealphablending($canvas, false);
						$white = imagecolorallocatealpha($canvas, 0, 0, 0, 127);
						imagefill($canvas, 0, 0, $white);
						imagecopyresampled($canvas, $src,0, 0,$sx,$sy, $width, $height,$sw, $sh);
						imagesavealpha($canvas, true);
						imagepng($canvas, $path,0);
					}
					
					else{
						$white = imagecolorallocate($canvas, 255, 255, 255);
						imagefill($canvas, 0, 0, $white);
						imagecopyresampled($canvas, $src,0, 0,$sx,$sy, $width, $height,$sw, $sh);
						imagejpeg($canvas, $path, 100);
					}
					
					imagedestroy($canvas);
					imagedestroy($src);	
					
					applog("image rezised, creating sizes");
					
					$thumb = "contents/thumb/" . $fid;
					Util::generateThumbImage($path, $thumb, 120);
					
					$full = "contents/images/" . $fid;
					$ori = Util::resizeMoveImage($path, $full, $width);
					
					$image = new Image();
				//	$image->customer_type = 't';
					$image->customer_id = $this->user->id;
					$image->i600 = Config::$image_url."/contents/images/" . $fid;
					$image->i400 = Config::$image_url."/contents/images/" . $fid;
					$image->thumb = Config::$image_url."/contents/thumb/" . $fid;
					$image->url = Config::$image_url."/contents/images/" . $fid;
					$image->width = $width;
					$image->height = $height;
					//$image->orientation = $ori;
					$image->save();	
					applog("Image record created {$image->id}");
					
				}
				elseif ($sec == 'b'){
					// small image for service description
					
					$fid = $this->user->id."_".time()."_".rand(1000,9999).'.'.$ext;
					$path = Config::$base_path . "/tmp/" . $fid;
					
					if(!copy($paths['url'], $path)){
						applog("Image coping error ".$path.", abort");
						$this->view->error = "Error coping image";	
						return $this->render();
					}
					
					// resize image
					if ($ext == 'jpg' || $ext == 'jpeg') {
						$src = @imagecreatefromjpeg($path);
					} elseif ($ext == 'png') {
						$src = imagecreatefrompng($path);
					} elseif ($ext == 'gif') {
						$src = imagecreatefromgif($path);
					}
					
					$mark = (!empty($_POST['mark']) && is_numeric(empty($_POST['mark']))) ? $_POST['mark'] : 200;
					$scale = (!empty($_POST['scale']) && is_numeric($_POST['scale'])) ? $_POST['scale'] : 1;
					$top = (!empty($_POST['top']) && is_numeric($_POST['top'])) ? $_POST['top'] : 0;
					$left = (!empty($_POST['left']) && is_numeric($_POST['left'])) ? $_POST['left'] : 0;
					
					$crop_length = round($mark/$scale);
					$y = round((100 - $top)/$scale);
					$x = round((100 - $left)/$scale);
					
					$canvas = imagecreatetruecolor($crop_length, $crop_length);
					
					if($ext == 'png' || $ext == 'gif'){
						
						imagealphablending($canvas, false);
						$white = imagecolorallocatealpha($canvas, 0, 0, 0, 127);
						imagefill($canvas, 0, 0, $white);
						
						imagecopyresampled($canvas, $src,0, 0,$x, $y,$crop_length, $crop_length, $crop_length, $crop_length);
						imagesavealpha($canvas, true);
						imagepng($canvas, $path,0);
					}
					else {					
						
						$white = imagecolorallocate($canvas, 255, 255, 255);
						imagefill($canvas, 0, 0, $white);
						imagecopyresampled($canvas, $src,0, 0,$x, $y,$crop_length, $crop_length, $crop_length, $crop_length);
						imagejpeg($canvas, $path, 100);
					}
					
					imagedestroy($canvas);
					imagedestroy($src);	
					
					applog("image rezised, creating sizes");
					
					$thumb = "contents/thumb/" . $fid;
					Util::generateThumbImage($path, $thumb, 120);
					
					$full = "contents/images/" . $fid;
					$ori = Util::resizeMoveImage($path, $full, $width);
					
					$image = new Image();
					//$image->customer_type = 't';
					$image->customer_id = $this->user->id;
					$image->i600 = Config::$image_url."/contents/images/" . $fid;
					$image->i400 = Config::$image_url."/contents/images/" . $fid;
					$image->thumb = Config::$image_url."/contents/thumb/" . $fid;
					$image->url = Config::$image_url."/contents/images/" . $fid;
					$image->width = $width;
					$image->height = $height;
					//$image->orientation = $ori;
					$image->save();	
					
					applog("Image record created {$image->id}");
					
				}				
				
				else{
					$image = $img;
					applog("Image {$image->id} will be used as it is");
				}
				
			}// re-purpose existing image
			else{
				$this->view->error = "Invalid access";
				applog("Image data or libraray image id not present, abort");
				return $this->render();	
			}
			
			$section = $page->getSection($_POST['section']);
			
			if(isset($_POST['index']) && is_numeric($_POST['index'])){
				$part = $section->getPart($_POST['index']);
				$part->image_id = $image->id;
				$part->save();
				applog("Image saved for {$page->id}, section {$_POST['section']}, part index {$_POST['index']}");
			}
			else{
				$section->image_id = $image->id;
				$section->save();
				applog("Image saved for {$page->id}, section {$_POST['section']}");
			}
			
			applog("Page image upload complated, sending results");
			
			$out = array(
				'img' => $image->getData(),
				'part' => array(
					'id' => (!empty($part)) ? $part->id : $section->id,
					'section' => (!empty($part)) ? $part->section : $section->section,
					'index' => (!empty($part)) ? $part->order : '' 
				)
			);
			
			$this->view->data = $out;
			$this->view->status = true;
			$this->view->action = 'page_update_image';
			
			$page->publish();
			applog("Page {$page->id} => {$page->handle} publshed");			
			
		}
		else{
			$this->view->error = "Invalid request";	
		}
		
		$this->render();
	}
	
	/**
	* List all images associated to customer
	*/
	public function gallery_images () {
		
		$this->view->buffer = $this->user->getImages();
		$this->view->status = true;
		$this->view->html = $this->component('customer/image-gallery');
		$this->view->id = 'stage';
		$this->render();
	}
	
	/**
	* Remove an image from the server
	*/
	public function image_remove () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$img = new Image($_POST['id']);
			
			if($img->customer_id == $this->user->id){
				
				$usage = $img->usage();
				
				if(!empty($usage['customer'])){
					$this->view->error = "This image is using in your profile";	
					return $this->render();
				}
				
				if(!empty($usage['profile'])){
					$this->view->error = "This image is using in your profile";	
					return $this->render();
				}
				
								
				if(!empty($usage['page'])){
					$this->view->error = "This image is using in your public page";	
					return $this->render();
				}
				
				//$img->delete();
				$paths = $img->getPaths();
				$s = new Storage();
				
				foreach ($paths as $p){
					
					if(file_exists($p)){
						unlink($p);
					}
					
					// delete from cdn				
					$res = $s->delete($p);
				}
				
				$img->delete();	
				
				
				
				$this->view->status = true;
				$this->view->action = 'image_removed';
				$this->view->id = $img->id;
				$this->view->message = "Image deleted";
				
					
			}
			else{
				$this->view->error = 'You do not have permision to delete this image';	
			}
			
		}
		$this->render();
	}
	
		
	/**
	* Update customer settings 
	*/
	
	public function setting_update () {
		
		if(!empty($_POST)) {
		
			$settings = $this->user->getCustomerSettings();
		
			foreach (DataType::$NOTIFICATION_TYPES as $index => $name) {
				
				$email = 'alert'.$index.'_email';
				$sms = 'alert'.$index.'_sms';
				
				$settings->$email = (!empty($_POST[$email])) ? 1 : 0;
				$settings->$sms = (!empty($_POST[$sms])) ? 1 : 0;
			}
			
			$settings->direct = !empty($_POST['direct']) ? 1 : 0;
			
			$settings->save();
			$this->view->status = true;
			$this->view->message = "Notification settings updated";
			
		}
		
		$this->render();
	}
	
	/**
	* Refresh bookng messages
	*/
	public function booking_messages () {
		
		if(!empty($_POST['booking_id']) && is_numeric($_POST['booking_id'])){
			
			$booking = new Booking($_POST['booking_id']);
			
			if($booking->isRecipient() || $booking->isSender()){
			
				$this->view->booking = $booking;
				$this->view->html = $this->component('booking/message_board');
				$this->view->status = true;
				$this->view->id = 'booking_msg_board';
			}
				
		}
		
		$this->render();
		
	}
	
	
	/**
	* Remove client's device
	* This will log off user from the device
	*/
	public function device_remove () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$device = new CustomerDevice ($_POST['id']);
			
			if($device->customer_id == $this->user->id){
				
				$device->delete();
				$this->user->removeDeviceLogins($device->id);
				
				$this->view->nodes = array(
					array('id' => 'push_id_'.$device->fingureprint, 'html' => 'Removed')
				);
				
					if($_POST['dfp'] == $device->fingureprint){
						$dialog = array(
						'title' => 'Device Removed',
						'message' => "You have logged off from this device.<br><br>You may want to login again.",
						'mask' => true,
						'buttons' => false,
						'width' => 400
					);
					$this->view->action = 'dialog';
					$this->view->data = $dialog;
			
				}
				else{
					$this->view->action  = 'recent';	
				}
				$this->view->message = "Device removed";
				$this->view->status = true;
			}
			else{
				$this->view->error = "Unauthorized access";	
			}
			
		}
		
		$this->render();
	}
	
	public function pushtoken_update () {
		
		if(!empty($_POST['token']) && !empty($_POST['dfp']) && is_numeric($_POST['dfp'])) {
			
			$device = $this->user->getDevice($_POST['dfp']);
			
			if(!empty($device)) {
				$device->push_id = Validator::cleanup($_POST['token'], 250);
				$device->save();
				
				$com = new ComEngine();
				$com->pushTest($_POST['token'], $device->fingureprint);
				
				$this->view->status = true;
				$this->view->message = "Push notification enabled";
			}
			else{
				$this->view->action = 'device';	
			}
			
			
		}
		
		$this->render();	
	}
	
	/**
	* Update notofications as seen
	*/
	public function notify_ack () {
		
		if(!empty($_POST['nid']) && is_numeric($_POST['nid'])){
			$this->user->ack($_POST['nid']);
			$this->view->status = true;
			
		}
		$this->render();
	}
	
	
	
	
	/**
	* Closing profile page
	*/
	public function profile_closing() {
		
		
		$profile = $this->pickProfile();
		
		if(!empty($profile)){
			
			$this->view->html = $this->component('dashboard/profile_close');
			$this->view->id = 'dash-contents';	
			
		}
		else{
			$this->view->error = "Plesae select a profile";
		}
		
		
		$this->render();
	}
	
	
	/**
	* Closing account
	*/
	public function account_closing () {
		
		$this->view->html = $this->component('dashboard/account_close');
		$this->view->id = 'dash-contents';
		
		$this->render();
	}
	
	/**
	* De activating profile
	*/
	public function profile_delete () {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(empty(!$_POST['id']) && $_POST['id'] == 'confirm'){		
		
			$session = $this->pickProfile();

			if(!empty($session)){
				
				$profile = new Profile($session['id']);

				$this->_removeProfile($profile);
				
				Session::clearProfile();
				$this->view->action = "redirect";
				$this->view->data = '/dashboard';
			}			
		}
		else{
			$this->view->error = "Invalid request";
		}
				
		
		$this->render();
	}
	
	/**
	* Delete account
	*/
	public function account_delete () {		
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		if(empty(!$_POST['id']) && $_POST['id'] == 'confirm'){		
		
			$profiles = $this->user->getProfiles();
			
			if(!empty($profiles)){
				
				foreach($profiles as $profile){
					$this->_removeProfile($profile);
				}				
			}
			
			$this->auth->logout();
			$this->user->delete();
			applog("User accoount {$this->user->id} deleted");

			$this->view->action = "redirect";
			$this->view->data = '/';
			
		}
		else{
			$this->view->error = "Invalid request";
		}
				
		
		$this->render();		
	}
	
	private function _removeProfile ($profile){
		
		applog("Deleting profile {$profile->id}");
		
		$categories = $profile->getCategories();
		$processor = new PaymentProcessor();
		
		if(!empty($categories)){			
			
			foreach ($categories as $cat){
				
				$info = $cat->getinfo();
				
				if(!empty($info['stripe']['active'])){
					$sub = $processor->cancelSubscription($info['stripe']['active'], true);
					applog("Stripe subscription {$info['stripe']['active']} ended\n".json_encode($sub));
					
				}
				
				if(!empty($info['stripe']['pending'])){
					$sub = $processor->cancelSubscription($info['stripe']['pending'], true);
					applog("Stripe subscription {$info['stripe']['pending']} ended\n".json_encode($sub));
				}
				
			}
			
		}
		
		$page = $profile->getPage();
		$page->delete();
		applog("Page {$page->id} deleted");
		
		$profile->delete();
		applog("Profile {$profile->id} deleted");
		return true;
		
	}
	
	/**
	* Adding a tag
	*/
	public function tag_add () {
		
		if(!empty($_POST['tag'])){
			
			$tag = filter_var($_POST['tag'], FILTER_SANITIZE_STRING);
			
			$profile = $this->pickProfile();
			
			if(!empty($profile )){
				
				$object = new Profile($profile['id']);
			
				if($object->addTag($tag)){
					$this->view->message = "Tag added";
					$this->view->status = true;
					applog("Tag {$tag} added to {$object->id} profile");
				}
				else{
					$this->view->status = true;
				}
			}
			
		}
		else{
			applog("Tag not specified, abort");
			$this->view->error = "Invalid request";	
		}
		
		$this->render();
	}
	
	/**
	* Adding a tag
	*/
	public function tag_remove () {
		
		if(!empty($_POST['tag'])){
			
			$tag = filter_var($_POST['tag'], FILTER_SANITIZE_STRING);
			
			$profile = $this->pickProfile();
		
			if(empty($profile)){
				$this->pickProfile();
				return;
			}
			
			$object = new Profile($profile['id']);
			
			if($object->removeTag($tag)){
				$this->view->message = "Tag removed";
				$this->view->status = true;
				applog("Tag {$tag} removed from {$object->id} profile");
			}
			else{
				$this->view->status = true;
			}
			
		}
		else{
			applog("Tag not specified, abort");
			$this->view->error = "Invalid request";	
		}
		
		$this->render();
	}
	
	
	
	
	
	/***************** Override render function for easy access *************/
	
	public function  render ($view_path = '') {
		
		parent::render('/common/void');
		
	}
	

}


