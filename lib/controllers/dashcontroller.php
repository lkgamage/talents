<?php

class DashController extends Controller {
	
	// Admin auth object
	private $master;
	
	
	function __construct ($request = NULL) {
			
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
		
		parent::__construct($request);
		 
		 
		 if($this->data[0] == 'dash' && !empty($this->data[1])) {
		 	$this->template = 'ajax.php';
		 }
		 
		 if(!$this->auth->isAuthenticated()){
			 //	$this->redirect('/login');
			 
			 	$this->view->action = 'redirect';
				$this->view->data = '/login';
				return $this->render('/common/void');
		 }
		 
		 
		 array_shift($this->data);
		 
		 if(empty($this->data[0])){
			 
			$this->redirect('/dashboard'); 
		 }
		 elseif(method_exists($this, $this->data[0])){
			 			 
			 $method = array_shift($this->data);
			 $this->$method();
		 }
		 else{
			$this->render('/front/404'); 
		 }
		
	}
	
	
	
	/**
	* Home
	*/
	private function home () {
	/*
		 $this->loadJS('https://maps.googleapis.com/maps/api/js?key='.Config::$google_key.'&libraries=places', true);
		 $this->loadJS('dashboard.js');
		 $this->loadCSS('dashboard.css');
		 $this->loadCSS('vanilla-calendar.css');
		$this->loadJS('vanilla-calendar-min.js');
		 
		 $this->render('/dashboard/dashboard.php'); 
		//$this->render('/dashboard/wizard.php'); 
		*/
	}
	
	
	
	
	/**
	* Current profile dashboard
	*/
	private function dashboard () {
		
		$profile = Session::getProfile();
				
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/dashboard');
		
		$this->view->page = array(
						'title' => ((empty($profile) ? 'Customer Dashboard' : ($profile['type'] == 'a' ? 'Agency Dashboard' : 'Talent Dashboard'))), 
						'url' => '/dashboard'
					 );
		
		$this->render('/common/void');
	}
	
	
	/**
	* Manage bookings
	*/
	private function bookings () {
		
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/bookings');
		
		$this->view->page = array(
						'title' => 'Bookings', 
						'url' => '/dashboard/bookings'
					 );
		
		$this->render('/common/void');
	}
	
	/**
	* Manage events
	*/
	public function events () {		
		
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/events');
		$this->view->page = array(
						'title' => 'Events', 
						'url' => '/dashboard/events'
					 );
		
		$this->render('/common/void');
		
	}
	
	/**
	* Manage packages
	*/
	private function packages () {
		
		$profile = $this->pickProfile();
		
		if(!empty($profile)){
		
			$this->view->profile = new Profile($profile['id']);
			$this->view->packages = $this->view->profile->getPackages(true);

			$this->view->html = $this->component('/dashboard/packages');
			$this->view->id = 'dash-contents';
			$this->view->status = true;


			$this->view->page = array(
							'title' => 'Packages', 
							'url' => '/dashboard/packages'
						 );

		}
		
		$this->render('/common/void');
		
	}
	/**n
	* Manage calendar
	*/
	private function appointments () {
		
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/appointments');
		$this->view->page = array(
						'title' => 'Appointments', 
						'url' => '/dashboard/appointments'
					 );
		$this->render('/common/void');
	}
	
	
	
	/**
	* Talents
	*/
	private function talents () {
		
		$profile = $this->pickProfile();
		
		if(!empty($profile) && $profile['type'] == 'a'){
			
			$agency = new Agency($profile['id']);
			$this->view->agency = $agency;
			$this->view->talents = $agency->getTalents(true);
		
			$this->view->id = 'dash-contents';
			$this->view->html = $this->component('dashboard/talents');
			$this->view->page = array(
						'title' => 'Talents', 
						'url' => '/dashboard/talents'
					 );		
		
		}
		
		$this->render('/common/void');
	}
	
	/**
	* Talents
	*/
	private function find_talents () {
		
		$profile = $this->pickProfile();
		
		if(!empty($profile) && $profile['type'] == 'a'){
			
			$agency = new Agency($profile['id']);
			$this->view->agency = $agency;
			$location = $agency->getLocation();
			
			$engine = new SearchEngine();
			$engine->city = $location->city;
			$engine->region = $location->region;
			$engine->country = $location->country;
			$this->view->engine = $engine;
			
			//$this->view->talents = $agency->getTalents(true);
		
			$this->view->id = 'dash-contents';
			$this->view->html = $this->component('dashboard/find_talents');		
		
		}
		
		$this->render('/common/void');
	}
	
	/**
	* Manage public profile
	*/
	private function profile () {
		
		$this->view->id = 'dash-contents';
		
		$profile = $this->pickProfile();
		
		if(!empty($profile)){
			
			$this->view->profile = new Profile($profile['id']);
			$this->view->html = $this->component('dashboard/profile');
			$this->view->page = array(
					'title' => $this->view->profile->name, 
					'url' => '/dashboard/profile'
				 );
			
		}
				
		
		$this->render('/common/void');
	}
	
	/**
	* Manage public page
	*/
	private function page () {
		
		$profile = $this->pickProfile();
		
		if(!empty($_POST['id'])){
			
		
			$page = new Page($_POST['id']);
			
			if($page->customer_id == $this->user->id){
				$this->view->page = $page;
				$this->view->html = $this->component('dashboard/page');
			}
			else{
				$this->view->error = "Page not found";	
			}
			
		}
		elseif(!empty($profile)) {
			
			if($profile['type'] == 't'){
				$talent = new Talent($profile['id']);	
				$this->view->page = $talent->getPage();
			}
			else{
				
				$agency = new Agency($profile['id']);
				$this->view->page = $agency->getPage();	
			}
		
			
			$this->view->html = $this->component('dashboard/page');
		
		}
		else{
			$this->view->error = "Page not found";	
		}
		
		$this->view->id = 'dash-contents';
		$this->render('/common/void');
	}
	
	
	/**
	* Manage insight info
	*/
	private function insight () {
		
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/insight');
		$this->view->page = array(
						'title' => 'Insignt', 
						'url' => '/dashboard/insight'
					 );
		$this->render('/common/void');
	}
	
	/**
	* Manage messages
	*/
	private function messages () {
		
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/messages');
		$this->view->page = array(
						'title' => 'Messages', 
						'url' => '/dashboard/messages'
					 );
		$this->render('/common/void');
	}
	
	/**
	* Manage saved talent data
	*/
	private function favourite () {
		
		$this->view->id = 'dash-contents';
		
		$engine = new SearchEngine();
		$this->view->buffer = $engine->favourites();
		
		
		$this->view->html = $this->component('dashboard/favourite');
		$this->view->page = array(
						'title' => 'Favourite', 
						'url' => '/dashboard/favourite'
					 );
		
		$this->render('/common/void');
	}
	
	
	/**
	* Manage user account
	*/
	private function account () {
		
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/account');
		$this->view->page = array(
						'title' => 'Account', 
						'url' => '/dashboard/account'
					 );
		$this->render('/common/void');
	}
	
	/**
	* Manage global settings
	*/
	private function settings () {
		
		$this->view->id = 'dash-contents';
		$this->view->html = $this->component('dashboard/settings');
		$this->view->page = array(
						'title' => 'Account Settings', 
						'url' => '/dashboard/settings'
					 );
					 
	//	$this->view->action = 'device';
		$this->render('/common/void');
	}
	
	/**
	* Get previously sent pending request
	*/
	public function agency_pending_requests () {
		
		
		$profile = $this->pickProfile();
		
		if($profile['type'] == 'a'){
			$this->view->agency = new Agency($profile['id']);
			$this->view->html = $this->component('dashboard/agency_requests');
			$this->view->status = true;
			$this->view->id = 'dash-contents';
			
		}
		
		$this->render('/common/void');
		
	}
	
	/**
	* New profile form
	*/
	public function newprofile () {
		
		
		
		$this->view->html = $this->component('dashboard/newprofile');
		$this->view->status = true;
		$this->view->id = 'dash-contents';
		
		$this->render('/common/void');
		
	}
		
	
	
}