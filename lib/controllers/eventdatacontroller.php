<?php
/**
* User must be logged in to access this data
*/

class EventDataController extends Controller {
	
	// Admin auth object
	private $master;
	
	
	function __construct ($request = NULL) {
		
		$_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
					
		 parent::__construct($request);
		 
		 $this->template = 'ajax.php';

		 if($this->auth->isAuthenticated()) {
		 		 
			 array_shift($this->data);
			 
			 if(!empty($this->data[0]) && method_exists($this, $this->data[0])){
				 
				 $method = array_shift($this->data);
				 $this->$method();
			 }
			 else{
				$this->view->error = "Method not found";
				$this->render(); 
			 }
		 }		
		 else{
			$this->view->error = "Authentication required";
			$this->render(); 
		 }
		
	}
	

	/**
	* Show event edit form
	*/
	public function booking_event_edit () {
		
		if(!empty($_POST['id'])){
			
			$event = new Event($_POST['id']);
			
			if($event->customer_id = $this->user->id){
				
				$this->view->event = $event;
				$this->view->html = $this->component('customer/event-form');
				
				$this->view->id = (!empty($_POST['target'])) ?  $_POST['target'] : 'booking_event';
				
				$this->view->status = true;
			}
			else{
				$this->view->error = "Unauthorized access";	
			}
			
		}
		
		$this->render();
			
	}
	
	/**
	* Create or update an event
	*/
	public function booking_event_update () {
		
		if(!empty($_POST['event_event_type']) && !empty($_POST['event_name']) && !empty($_POST['event_begins_date'])) {
			
			$dt =  strtotime($_POST['event_begins_date'].' '.$_POST['event_begins_time'] );
			
			if(empty($dt)){
				$this->view->error = "Invalid event begin date";
				return $this->render();
			}
			
			if($dt < time()){
				$this->view->error = "Please select future date as event begin date";
				return $this->render();
			}
			
			if(!empty($_POST['event_ends_date']) && !empty($_POST['event_ends_time'])) {
				
				$et = strtotime($_POST['event_ends_date'].' '.$_POST['event_ends_time']);
				
				if(empty($dt)){
					$this->view->error = "Invalid event end date";
					return $this->render();
				}
				
				if($dt >  $et){
					$this->view->error = "Event end date/time should be greater then event begin date/time";
					return $this->render();
				}
			}
			
		
			$event = new Event();
			$event->customer_id = $this->user->id;
			$event->event_type = Validator::cleanup($_POST['event_event_type'], 5);
			$event->name = Validator::cleanup($_POST['event_name'], 400);
			$event->description = Validator::cleanup($_POST['event_description'], 1000);
			$event->begins = Util::ToSysDate($_POST['event_begins_date'], $_POST['event_begins_time'] );
			
			if(!empty($_POST['event_ends_date']) && !empty($_POST['event_ends_time'])) {
				$event->ends = Util::ToSysDate($_POST['event_ends_date'], $_POST['event_ends_time']  );
			}
			elseif(!empty($_POST['event_ends_time'])){
				
				$event->ends = Util::ToSysDate($_POST['event_begins_date'], $_POST['event_ends_time']  );
			}
			
			
			
			$event->charitable =(!empty($_POST['event_charitable'])) ? 1 : 0;
			
			
			if(!empty($_POST['event_location_id'])) {
				$event->venue = Validator::cleanup($_POST['event_venue'], 400);
				$event->location_id = Validator::cleanup($_POST['event_location_id'], 20);				
			}
			else {
				// create new location	
				$location = Util::saveLocation();
				
				$event->location_id = $location->id;
				$event->venue = Validator::cleanup($_POST['place'], 400);
				
				$this->view->id = (!empty($_POST['target'])) ?  $_POST['target'] : 'booking_event';
			}
			
			$event->save();
			
			// update booking with this event data
			if(!empty($_POST['booking_id'])){
				$booking = new Booking($_POST['booking_id']);
				$booking->event_id = $event->id;
				$booking->save();	
			}
					
			$this->view->event = $event;
			$this->view->html = $this->component('customer/event-view');
			
			$this->view->id = (!empty($_POST['target'])) ?  $_POST['target'] : 'booking_event';
			
			$this->view->status = true;
		}		
		else{
			$this->view->error = "Invalid request";
		}
		
		$this->render();
	}
	
	
	/**
	* Agency create an appointment for event
	*/
	public function event_appointment_update () {
		
		
		if(!empty($_POST['event_id']) && is_numeric($_POST['event_id']) && !empty($_POST['talent_id']) && is_numeric($_POST['talent_id']) && !empty($_POST['appointment_begins_date']) && !empty($_POST['appointment_begins_time']) && !empty($_POST['appointment_duration']) ){
			
			
			$profile = Session::getProfile();
			$event = new Event($_POST['event_id']);
			$talent = new Talent($_POST['talent_id']);
			$datetime = !empty(strtotime($_POST['appointment_begins_date'].' '. $_POST['appointment_begins_time'])) ? Util::ToSysDate($_POST['appointment_begins_date'], $_POST['appointment_begins_time']) : NULL;
			
			$span = (!empty($_POST['appointment_duration'])) ? Util::ToMinutes($_POST['appointment_duration']) : NULL;
			
			if(!empty($profile) && $profile['type'] == 'a' && $event->agency_id == $profile['id']){
				$request = $talent->getAgencyConnection($profile['id']);
				
				if(empty($request)){
					$this->view->error = "Talent is not working with the Agency" ;
					return $this->render();
				}
				
				if(empty($datetime)){
					$this->view->error = "Invalid date time";
					return $this->render();		
				}
				
				if(empty($span)){
					$this->view->error = "Invalid duration";
					return $this->render();
				}
			
			
				if(!empty($_POST['appointment_id'])){
			
					$appointment = new Appoinment($_POST['appointment_id']);
					
					if($appointment->talent_id != $request->talent_id || $appointment->agency_id != $request->agency_id){
						$this->view->error = "Unauthorized access";
						return $this->render();
					}
					
					$this->view->message = "Appointment updated";
					
				}
				else{
					$appointment = new Appoinment();						
					$appointment->talent_id = $request->talent_id;
					$appointment->agency_id = $request->agency_id;
					
					$this->view->message = "Appointment created";
				}
		
				$appointment->description = Validator::cleanup($_POST['appointment_description'], 400);
				$appointment->begins = $datetime;
				$appointment->event_id = $event->id;
				$appointment->duration =  Util::ToMinutes($_POST['appointment_duration']);
				$appointment->save();
				
				// display same
				$this->view->action = 'recent';
				$this->view->status = true;
					
				
			}
			else{
				$this->view->error = "Unauthorized request";
			}
		}
		
		$this->render();
	}
	

	/**
	* Create a booking or an agency event
	*/
	
	public function event_booking_update () {
		
				
		if(!empty($_POST['event_id']) && is_numeric($_POST['event_id']) && !empty($_POST['talent_id']) && is_numeric($_POST['talent_id']) ){
			
			if(empty($_POST['b_date']) || empty($_POST['b_time']) || empty(strtotime($_POST['b_date'].' '.$_POST['b_time']))){
				
				$this->view->error = "Invalid date time";
				return $this->render();	
			}
						
			if(empty($_POST['session_id']) || !is_numeric($_POST['session_id'])){
				$this->view->error = "Please select a package";
				return $this->render();
			}
			
			
			
			$profile = Session::getProfile();			
			$event = new Event($_POST['event_id']);
			$talent = new Talent($_POST['talent_id']);
			$package = new TalentPackage($_POST['session_id']);
			$datetime = Util::ToSysDate($_POST['b_date'], $_POST['b_time']);
			
			if(empty($profile) || $profile['type'] != 'a' || $event->agency_id != $profile['id']){
				$this->view->error = "Unauthorized request" ;
				return $this->render();	
			}
			
			if($package->talent_id != $talent->id || !$package->isAvailable()){
				$this->view->error = "Selected package is not available" ;
				return $this->render();	
			}
			
			$request = $talent->getAgencyConnection($profile['id']);
			
			if(empty($request)){
				$this->view->error = "Talent is not working with the Agency" ;
				return $this->render();
			}
			
			$booking = new Booking();
			$booking->talent_id = $talent->id;
			$booking->agency_id = $profile['id'];
			$booking->customer_id = $this->user->id;
			$booking->event_id = $event->id;
			$booking->location_id =$event->location_id;
			$booking->venue = $event->venue;
			$booking->timeoffset = $talent->timeoffset;
			$booking->package_id = $package->id;
			
			$booking->session_start = Util::ToSysDate($_POST['b_date'], $_POST['b_time']);
			$booking->session_ends = date("Y-m-d H:i:s", strtotime($booking->session_start) + $package->timespan*60);
			$booking->status = DataType::$BOOKING_PENDING;
			$booking->talent_fee = $package->fee;
			$booking->charges =  App::bookingCharge($package);	
			$booking->currency = $talent->currency;
			$booking->status_changed = date("Y-m-d H:i:00");
			$booking->setDueDate();
			$booking->save();
			
			$this->view->message = "Booking created";
			$this->view->action = 'recent';
			
		}
		else{
			$this->view->error = "Invalid request";	
		}
		
		$this->render();
	}
	
		
	/**
	* List events 
	*/
	public function event_search () {
		
		$status = isset($_POST['status']) ? $_POST['status'] : 0;
		
		if(!is_numeric($status) || $status < 0 || $status > 2){
			$status = 0;	
		}
		
		if($status == 0){
			
			$year = (!empty($_POST['filter_year']) && is_numeric($_POST['filter_year'])) ? $_POST['filter_year'] : date('Y');
			$month = (!empty($_POST['filter_month']) && is_numeric($_POST['filter_month'])) ? $_POST['filter_month'] : date('n');
		
			if($year < 2021 || $year > date('Y')+10){
				$year = date('Y');
			}
			
			if($month < 1 || $month > 12){
				$month = date('n');	
			}
			
		}
		else{
			$year = NULL;
			$month = NULL;	
		}
		
		
		
		$profile = Session::getProfile();
		$agency_id = (!empty($profile) && $profile['type'] == 'a') ? $profile['id'] : NULL;
		
		$this->view->events = $this->user->searchEvents($status, $year, $month, $agency_id);
		
		$this->view->status = true;
		$this->view->html = $this->component('event/event_list');
		$this->view->id = 'event-list';
		
		$this->render();
	}
	
		/**
	* Get list of events to display
	*/
	public function browser_events () {
		
		$profile = Session::getProfile();
		
		if(!empty($profile) ) {
		
			if($profile['type'] == 'a'){
				
				$events = $this->user->searchEvents (1, NULL, NULL, $profile['id']);
				
			}
			
		}
		else{
			$events = $this->user->getEvents(1);
		}
		
		if(!empty($events)){
			$this->view->events = $events;
			$this->view->html = $this->component('/event/event_select_list');
			$this->view->status = true;
			$this->view->id = 'event_list';	
		}
		
		$this->render();
	}
	
	/**
	* Get event block with an input to display inside a form
	*/
	public function get_event_block () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$event = new Event($_POST['id']);
			
			if($event->customer_id == $this->user->id){
			
				$this->view->data = $event->brief();
				$this->view->cura = '';
				$this->view->no_date = true; 
				$this->view->html = $this->component('event/events');
				$this->view->id = 'event_list';
			}
			
			
		}
		
		$this->render();
	}
	
	/**
	* Display event edit form
	*/
	public function event_edit () {	
	
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$event = new Event($_POST['id']);
			
			if(!$event->ready() || $event->customer_id != $this->user->id){
				
				$this->view->error = 'Unauthorized request';
				return $this->render();
			}
			
			$this->view->event = $event;
			
		}
		else{
			$this->view->page = array(
					'title' => 'Create an event', 
					'url' => '/dashboard/event/create'
				 );	
		}
		
		$this->view->status = true;
		$this->view->html = $this->component('event/event_form');
		$this->view->id = 'dash-contents';
		
		$this->render();	
	}
	
	/**
	* Update an event
	*/
	public function event_update (){

		$profile = Session::getProfile();
		
		$errors = array();
		
		if(empty($_POST['event_event_type']) || !is_numeric($_POST['event_event_type'])){
			$errors[] = "Invalid event type";	
		}
		
		if(empty($_POST['event_name']) ){
			$errors[] = "Event name required";	
		}
		
		if(empty($_POST['event_begins_date']) || empty($_POST['event_begins_time'])){
				$errors[] = "Event begin date/time required";	

		}
		else{
			$dt1 = strtotime($_POST['event_begins_date'].' '.$_POST['event_begins_time']);
			
			if(empty($dt1)){
				$errors[] = "Invalid event begin date/time";
			}
			elseif($dt1 < time()){
				$errors[] = "Please select future date as begin date";
			}
		}
				
		
		
		if(!empty($_POST['event_ends_date']) && !empty($_POST['event_ends_time'])){
			
			$dt2 = strtotime($_POST['event_ends_date'].' '.$_POST['event_ends_time']);
			
			if(empty($dt2)){
				$errors[] = "Invalid event end date/time";
			}
			elseif($dt2 < $dt1){
				$errors[] = "Event end date/time should be greate than event begin time";
			}
			elseif(($dt2-$dt1) > 86400*30){
				$errors[] = "We do not support events longer than 30 days";
			}
			
			$endtime = Util::ToSysDate(	$_POST['event_ends_date'], $_POST['event_ends_time']);
		}
		
		
		if(!empty($errors)){
			// display errors
			$this->view->error = implode('<br>', $errors);
			return $this->render();
		}
		
		if(!empty($_POST['event_id']) && is_numeric($_POST['event_id'])){
			
			$event = new Event($_POST['event_id']);
			
			if(!$event->ready() || $event->customer_id != $this->user->id){
				
				$this->view->error = 'Unauthorized request';
				return $this->render();
			}
			$location = $event->getLocation();
			
			if(!empty($_POST['place_id']) || empty($_POST['city']) || empty($_POST['country']) || !isset(DataType::$countries[$_POST['country']])){	
				$location = Util::saveLocation($location);
			}
		}
		else{
			$event = new Event();
			$event->customer_id = $this->user->id;
			
			if(!empty($profile) && $profile['type'] == 'a') {
				$event->agency_id = $profile['id'];
			}
			
			if(empty($_POST['place_id']) || empty($_POST['city']) || empty($_POST['country']) || !isset(DataType::$countries[$_POST['country']])){		
				$this->view->error = 'Please select a location';
				return $this->render();
			}
			
			$location = Util::saveLocation();
			$event->location_id = $location->id;
		}
			
		$begintime = Util::ToSysDate($_POST['event_begins_date'], $_POST['event_begins_time']);
		
		$event->event_type = Validator::cleanup($_POST['event_event_type'], 5);
		$event->name = Validator::cleanup($_POST['event_name'], 400);
		$event->description = Validator::cleanup($_POST['event_description'], 1000);
		$event->begins = $begintime;
		
		if(!empty($_POST['event_budget']) && is_numeric($_POST['event_budget'])){
			$event->budget = Validator::cleanup(toUSD($_POST['event_budget']), 12);
		}
		else{
			$event->budget = 0;
		}
		
		if(!empty($_POST['event_num_attendees']) && is_numeric($_POST['event_num_attendees'])){
			$event->num_attendees = Validator::cleanup($_POST['event_num_attendees'], 4);
			
		}
		
		if(!empty($endtime)) {
			$event->ends = $endtime;
		}
		$event->charitable = !empty($_POST['event_charitable']) ? 1 : 0;
	
		if(!empty($_POST['place'])) {
			$event->venue = Validator::cleanup($_POST['place'], 400);
		}
		
		$event->save();
		
		$this->view->event = $event;
		
		$this->view->status = true;
		$this->view->html = $this->component('event/event_view');
		$this->view->id = 'dash-contents';
		
		
		$this->render();
		
	}
	
	/**
	* View event details
	*/
	public function event_view () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$event = new Event($_POST['id']);
			
			if($event->ready() && $event->customer_id == $this->user->id){
				
				Session::setEvent($event->id);
				
				$this->view->event = $event;
				$this->view->status = true;
				$this->view->html = $this->component('event/event_view');
				$this->view->id = 'dash-contents';
				$this->view->page = array(
					'title' => $event->name, 
					'url' => '/dashboard/event/'.$event->id
				 );
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Enable agendata for the event */
	public function agenda_create () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$event = new Event($_POST['id']);
			
			if($event->ready() && $event->customer_id == $this->user->id){
				
				$event->createAgenda();
			
				
				$this->view->event = $event;
				$this->view->html = $this->component('event/agenda');
				$this->view->id = 'agenda';
				$this->view->status = true;
				
				$this->view->message = "Agenda created";
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
		
	}
	
	
	/**
	* Agenda form
	*/
	public function agenda_item_add () {
	
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$event = new Event($_POST['id']);
			
			if($event->customer_id != $this->user->id){
				$this->view->error = "Unauthorized access to the event";
				return $this->render();	
			}
			
			$this->view->event = $event;
			$this->view->html = $this->component('event/agenda_form');
			$this->view->id = 'add_agenda';
			$this->view->status = true;
			
		}
		
		$this->render();
	}
	
	/**
	* Edit or delete agenda item
	*/
	public function agenda_option () {
		
		if(!empty($_POST['id']) && is_numeric( $_POST['id']) && !empty($_POST['action'])){
			
			$item = new EventItem($_POST['id']);
			$event = $item->getEvent();
			
			if($event->customer_id != $this->user->id){
				$this->view->error = "Unauthorized access to the event";
				return $this->render();	
			}
			
			if($_POST['action'] == 'edit'){
				
				$this->view->item = $item;
				$this->view->event = $event;
				$this->view->html = $this->component('event/agenda_form');
				$this->view->id = 'add_agenda';
				$this->view->status = true;
				
			}
			elseif($_POST['action'] == 'delete'){
				
				$item->delete();
				$this->view->event = $event;
				$this->view->nodes = array(
								array(
								'id' => 'agenda', 
								'html' => $this->component('event/agenda')
								),
								array('id'=>'add_agenda', 'html' => '<div></div>')
								
							);
				$this->view->message = "Agenda item deleted";
				$this->view->status = true;
				
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Add or update agenda item
	*/
	public function event_item_update () {
	
		if(!empty($_POST['item_event_id']) && is_numeric($_POST['item_event_id'])){
			
			$event = new Event($_POST['item_event_id']);
			
			if($event->customer_id != $this->user->id){
				$this->view->error = "Unauthorized access to the event";
				return $this->render();	
			}
			
			if(!empty($_POST['item_id']) && is_numeric($_POST['item_id'])){
				
				$item = new EventItem($_POST['item_id']);	
				
				if($item->event_id != $event->id){
					$this->view->error = "Unauthorized access to the agenda";
					return $this->render();	
				}
			}
			else{
				$item = new EventItem();
				$item->event_id = $event->id;	
			}
			
			// checking begin time
			if(empty($_POST['item_begins_time']) || empty($_POST['item_begins_date'])){
				$this->view->error = "Invalid begin date/time";
				return $this->render();	
			}
			
			$begins = Util::ToSysDate($_POST['item_begins_date'],$_POST['item_begins_time'] );
			if(empty($begins)){
				$this->view->error = "Invalid begin date/time";
				return $this->render();	
			}
			
			if(strtotime($event->begins) > strtotime($begins)){
				$this->view->error = "Item should be within the event begin and end time span";
				return $this->render();	
			}
			
			// checking end time
			if(empty($_POST['item_ends_time']) || empty($_POST['item_ends_date'])){
				$this->view->error = "Invalid end date/time";
				return $this->render();	
			}
			
			$ends = Util::ToSysDate($_POST['item_ends_date'],$_POST['item_ends_time'] );
			if(empty($ends)){
				$this->view->error = "Invalid end date/time";
				return $this->render();	
			}
			
			if(strtotime($begins) > strtotime($ends)){
				$this->view->error = "Plesae select an end time after the begin time";
				return $this->render();	
			}
			
			$item->begins = $begins;
			$item->ends = $ends;
			$item->title = Validator::cleanup($_POST['item_title'], 400);
			
			if(!empty($_POST['item_description'])) {
				$item->description = Validator::cleanup($_POST['item_description'], 1000);
			}
			
			if(!empty($_POST['item_note'])) {
				$item->note = Validator::cleanup($_POST['item_note'], 1000);
			}
			$item->save();
			
			// reset bookings
			if(!empty($_POST['bookings'])){
				$item->setBookings($_POST['bookings']);	
			}else{
				$item->clearBookings();	
			}
			
			$this->view->message = "Agenda updated";
			$this->view->status = true;
			
			$this->view->event = $event;
			$this->view->nodes = array(
								array(
								'id' => 'agenda', 
								'html' => $this->component('event/agenda')
								),
								array('id'=>'add_agenda', 'html' => '<div></div>')
								
							);
			
			
		}
		$this->render();
	}
	
	/**
	* Get event bookings to link with agenda
	*/
	public function agenda_linking () {
		
		if(!empty($_POST['event']) && is_numeric($_POST['event'])){
			
			$event = new Event($_POST['event']);
			
			if($event->customer_id != $this->user->id){
				$this->view->error = "Unathorized event";	
				return $this->render();
			}
			
			$this->view->bookings = App::getBookings(NULL, NULL, $event->id);;
			
			if(!empty($_POST['id']) && is_numeric($_POST['id'])){
				$item = new EventItem($_POST['id']);
				$this->view->associates = $item->getBookings();
			}
			
			$this->view->html = $this->component('event/agenda_linking');
			$this->view->id = 'agenda_linking';
			$this->view->status = true;
			
		}		
		$this->render();
	}
	
	/**
	* Show agenda sharing box
	*/
	public function agenda_share () {
		
		if(!empty($_POST['event']) && is_numeric($_POST['event'])){
			
			$event = new Event($_POST['event']);
			
			if($event->customer_id != $this->user->id){
				$this->view->error = "Unathorized event";	
				return $this->render();
			}
			$this->view->event = $event;
			$this->view->shares = $event->getAgendaShares();
			$this->view->bookings = App::getBookings(NULL, NULL, $event->id);
			
			
			$this->view->html = $this->component('event/agenda_share');
			$this->view->id = 'agenda_share';
			$this->view->status = true;
			
		}		
		
		$this->render();
	}
	
	
	/**
	* Agenda sharing settings
	* We'll map sharing based on bookings
	* frontend pass booking id and we map it with the customer, agency and talent
	*/
	public function agenda_share_update () {
		
		if(!empty($_POST['event_id']) && is_numeric($_POST['event_id'])){
			
			$event = new Event($_POST['event_id']);
			
			if(!empty($_POST['bookings'])){
				$event->updateAgendaShare($_POST['bookings']);
			}
			else{
				$event->updateAgendaShare(NULL);
			}
			
			// display names
			$this->view->event = $event;
			$this->view->html = $this->component('event/agenda_share_names');
			$this->view->id = 'agenda_share';
			
			$this->view->status = true;
			$this->view->message = "Sharing updated";
		}
		
		$this->render();
	}
	
	public function agenda_share_cancel () {
		
		if(!empty($_POST['event_id']) && is_numeric($_POST['event_id'])){
		
			$event = new Event($_POST['event_id']);
			
			$this->view->event = $event;
			$this->view->html = $this->component('event/agenda_share_names');
			$this->view->id = 'agenda_share';
		
		}
		$this->render();
	}
	
	
	
	/**
	* Creating a booking for an event
	*/
	public function agency_event_booking () {
		
		if(!empty($_POST['event']) && is_numeric($_POST['event']) && !empty($_POST['talent']) && is_numeric($_POST['talent'])){
			
			$event = new Event($_POST['event']);
			$talent = new Talent($_POST['talent']);
			
			if($event->ready() && $event->customer_id == $this->user->id){				
				
				if($talent->ready()) {
					$this->view->talent = $talent;
				}
				
				$this->view->event = $event;
				$this->view->with_header = true;
				$this->view->html = $this->component('/agency/talent_booking_form');
				$this->view->id = 'new_booking';
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
		
	}
	
	/** Add event expence **/
	public function expence_add () {
		
		$this->view->html = $this->component('event/expence_form');
		$this->view->id = 'add_expences';
		
		$this->render();
	}
	
	// create an expence for event
	public function expence_save (){
		
		if(empty($_POST['expence_type']) || !is_numeric($_POST['expence_type'])){
			$this->view->error = "Invalid expnce type";
			return $this->render();
		}
		
		if(empty($_POST['expence_amount']) || !is_numeric($_POST['expence_amount']) || $_POST['expence_amount'] < 0 ){
			$this->view->error = "Invalid amount";
			return $this->render();
		}
		
		if(empty($_POST['expence_event_id']) && empty($_POST['expence_booking_id'])){
			
			$this->view->error = "Associated event or booking is required";
			return $this->render();
			
		}
		elseif(!empty($_POST['expence_event_id'])){
			
			if(!is_numeric($_POST['expence_event_id'])){
				
				$this->view->error = "Invalid event";
				return $this->render();
			}
			
			$event = new Event($_POST['expence_event_id']);
			
			if(!$event->ready() || $event->customer_id != $this->user->id){
				$this->view->error = "You have no access to this event";
				return $this->render();	
			}	
			
		}
		elseif(!empty($_POST['expence_booking_id'])){
			
			if(!is_numeric($_POST['expence_booking_id'])){
				$this->view->error = "Invalid booking";
				return $this->render();
			}	
			
			$booking = new Booking($_POST['expence_booking_id']);
			
			if(!$booking->ready() || !$booking->isRecipient()){
				$this->view->error = "You have no access to this booking";
				return $this->render();	
			}
				
						
		}
		
		$expence = new Expence();
		
		if(!empty($_POST['expence_event_id'])) {
			$expence->event_id = Validator::cleanup($_POST['expence_event_id'], 100);
		}
		
		if(!empty($_POST['expence_booking_id'])) {
			$expence->booking_id = Validator::cleanup($_POST['expence_booking_id'], 20);
		}
		$expence->expence_type = Validator::cleanup($_POST['expence_type'], 100);
		$expence->description = Validator::cleanup($_POST['expence_description'], 400);
		$expence->amount = Validator::cleanup(toUSD($_POST['expence_amount']), 12);
		$expence->save();

		if(!empty($event)){
			$this->view->budget = $event->budget;
			$this->view->owner = true;
			$this->view->expences = $event->getExpences();
			$this->view->html = $this->component('event/expences');
		}
		elseif(!empty($booking)){
			$this->expences = $booking->getExpences();
			$this->view->html = $this->component('booking/expences');
		}
		
		$this->view->status = true;
		$this->view->trigger = 'expence_changed';
		
		$this->render();
	}
	
	/** Remove an expence */
	public function expence_remove () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$expence = new Expence($_POST['id']);
			
			if(!$expence->ready()){
				$this->view->error = "Invalid expence";
				return $this->render();	
			}
			
			if($expence->isEvent()){
				
				$event = $expence->getEvent();
				
				if($event->customer_id != $this->user->id){
					$this->view->error = "You have no permission to delete this expence";
					return $this->render();
				}				
			}
			else{
				
				$booking = $expence->getBooking();
				if(!$booking->ready() || !$booking->isRecipient()){	
					$this->view->error = "You have no permission to delete this expence";
					return $this->render();
				}
			}
			
			
			$expence->delete();
			
			if(!empty($event)){
				$this->view->budget = $event->budget;
				$this->view->owner = true;
				$this->view->expences = $event->getExpences();
				$this->view->html = $this->component('event/expences');
			}
			elseif(!empty($booking)){
				$this->expences = $booking->getExpences();
				$this->view->html = $this->component('booking/expences');
			}
			
			$this->view->status = true;
			$this->view->trigger = 'expence_changed';
			
		}
		
		$this->render();
		
	}
	
	
	
	/***************** Override render function for easy access *************/
	
	public function  render ($view_path = '') {
		
		parent::render('/common/void');
		
	}
	

}


