<?php

class Controller {
	
	 /** autenticaton object */
    protected $auth = NULL;

    /** request object */
    protected $request = NULL;
	
	/** Request parameters */
	protected $data = NULL;
	
	 /** Page rendering object */
    protected $view;
	
	/** Currently logged in user **/
	protected $user;
	
	/** default template to display */
    protected $template = "default.php";
	
	/** Additional file list **/
	protected $files = array();
	
	/** Iniitialize user object from auth **/
	protected $init_user = true;
	
	
	function __construct ($request = NULL) {

        $this->auth = new Auth();
        $this->view = new PageViewer();
        //Util::debug($this->auth);
		
		if(!empty($request)){
			$this->request = $request;
			$this->data = $request->parameters;
		}
		
		if($this->auth->isAuthenticated()){
			
			if($this->init_user) {
				$this->user = $this->auth->getCustomer();	
			}

		}
		
    }
	
	
	/**
	* Redirect user to given url
	*/
	protected function redirect ($url){
		
		header("Location:".Util::mapURL($url));
		exit();
			
	}
	
	/**
	* Load additional JS files
	*/
	protected function loadJS ( $fn, $no_mapping = false){
		
		if(strpos($fn,'?') > 0){
			$fn .= "&ctvar=_".Version::$build;
		}
		else{
			$fn .= "?ctvar=_".Version::$build;
		}
		
		if($no_mapping){
			$this->files[] = '<script type="text/javascript" src="'.$fn.'"  async defer ></script>';
		}
		else {
			$this->files[] = '<script type="text/javascript" src="'.Util::mapURL('/js/'.$fn).'"></script>';
		}
	}
	
	
	/**
	* Load additional CSS files
	*/
	protected function loadCSS ($fn){
		
		$fn .= "?ctvar=_".Version::$build;
	
		
		$this->files[] = '<link rel="stylesheet" type="text/css" href="'.Util::mapURL('/css/'.$fn).'" >';
	}
	
	
	
	
	/**
     * display static page
     * Often use in root level pages
     */
    protected function render ($view_path) {
		
		$lng = Session::language();
		
		if(file_exists(Config::$base_path . "/templates" . $view_path.'_'.$lng.'.php')){
			$view_path .= '_'.$lng;
		}
		
		$view_path = Config::$base_path . "/templates" . $view_path.'.php';
        $template = Config::$base_path . "/templates/templates/" . $this->template;
		
		if(!empty($this->user)){
			$this->view->user = $this->user;	
		}
		
		if(!empty($this->auth)){
			$this->view->auth = $this->auth;	
		}
		
		$view = file_get_contents($view_path);
		
		$template = file_get_contents($template);

        $this->view->showStatic($view, $template, $this->files);

	}
	
	
	/**
	* Rendering cached page
	*/
	public function renderCache ($cache_file, $templete = 'default'){
		
		if(file_exists(Config::$cache_path.$cache_file.'.html')){
			$view = file_get_contents(Config::$cache_path.$cache_file.'.html');
		}
		else{
			return false;	
		}
		
		if(!empty($this->user)){
			$this->view->user = $this->user;	
		}
		
		if(!empty($this->auth)){
			$this->view->auth = $this->auth;	
		}
		
		$template = Config::$base_path . "/templates/templates/" .$templete.'.php';
		
		$template = file_get_contents($template);

        $this->view->showStatic($view, $template, $this->files);
		
		return true;
	}
	
	
	
	
	/**
	* Get component codes
	*/
	protected function component ($name){
		
		$lng = Session::language();
		
		if(file_exists(Config::$base_path . "/templates/components/" . $name.'_'.$lng.'.php')){
			$view_path .= '_'.$lng;
		}
		else {
			$view_path = Config::$base_path . "/templates/components/" . $name.'.php';
		}
		
		
		if(!empty($this->user)){
			$this->view->user = $this->user;	
		}
		
		if(!empty($this->auth)){
			$this->view->auth = $this->auth;	
		}
		
		$view = file_get_contents($view_path);
		
		return $this->view->component($view);
		
	}
	
	/**
	* Send email
	*/
	public function sendEmail ($template,  $to = NULL, $subject = NULL){
		
		if(empty($subject)){
			switch($template){
				case 'otp' : $subject = "Your on-time passcode";	
				break;
				case 'welcome' : $subject = "Welcome to Curatalent!";	
				break;
			}
		}
		
		
					
		if(!empty($this->user)){
			$this->view->user = $this->user;	
		}
		
		if(!empty($this->auth)){
			$this->view->auth = $this->auth;	
		}
		
		
		$mailer = new Mailer();
		$mailer->subject = $subject;
		
		if(empty($to)){
			$email = $this->user->getPrimaryEmail();
			
			if(!empty($email)) {
				$mailer->to = $email->email;
			}
			else{
				return false;	
			}
		}
		else{
			$mailer->to = $to;	
		}
		
		$view_path = Config::$base_path . "/templates/emails/" . $template.'.php';
		$view = file_get_contents($view_path);
			
		$mailer->html = $this->view->component($view);
		
		//return $mailer->send();
		return true;

	}
	
	/**
	* Pick profile for the user, if picking failed, 
	* it shows profile pick dialog
	*/
	
	public function pickProfile () {	
				
		$profile = Session::getProfile();
		
		if(empty($profile)){
			
			$profiles = $this->user->getProfiles(true);
			
			if(count($profiles) == 1){
		
				$pf = new Profile($profiles[0]['id']);
				
				Session::setProfile($pf);
				applog("Profile {$profiles[0]['id']} automatically selected for {$this->user->id}");
				
				return array(
					'id' => $profiles[0]['id'],
					'type' => $profiles[0]['is_agency'] ? 'a' : 't',
					'name' => $profiles[0]['name'],
					'dp' => $profiles[0]['thumb'],
					'dpid' => $profiles[0]['dpid']
				);
			}
			else{
				// prompt to select profile
				$this->view->profiles = $profiles;

				$dialog = array(
					'title' => 'Select Profile',
					'message' => $this->component('customer/profile_selector'),
					'mask' => true,
					'buttons' => false,
					'width' => 400
				);
				
				$this->view->action = 'dialog';
				$this->view->data = $dialog;
				$this->view->status = true;
				//$this->render('/common/void');
				
				return false;
			}			
		}
		else{
			return $profile;
		}
		
				
	}
	
	
	/**
	* Sending an SMS message
	*/
	public function sendSMS ($number, $message) {
				
		$sms = new Twillio();
		$sms->number = "+".trim($number,'+');
		$sms->message = $message;
		
		//return $sms->send();
		return true;
		
		
		
	}
	
	
	
}


?>