<?php
/**
* User must be logged in to access this data
*/

class AgencyDataController extends Controller {
	
	// Admin auth object
	private $master;
	
	
	function __construct ($request = NULL) {
					
		 parent::__construct($request);
		 
		 $this->template = 'ajax.php';

		 if($this->auth->isAuthenticated()) {
		 		 
			 array_shift($this->data);
			 
			 if(!empty($this->data[0]) && method_exists($this, $this->data[0])){
				 
				 $method = array_shift($this->data);
				 $this->$method();
			 }
			 else{
				$this->view->error = "Method not found";
				$this->render(); 
			 }
		 }
		 else{
			$this->view->error = "Authentication required";
			$this->render(); 
		 }
		
	}

	
	/**
	* Display form to send a request from Agency to talant
	*/
	public function talent_request () {
	
		$profile = Session::getProfile();
		
		if($profile['type'] == 'a'){
			
			$this->view->html = $this->component('talent_request');
			$this->view->status = true;
			$this->view->id = 'talent_request';
			
		}
		
		$this->render();
		
	}
	
	/**
	* Search talent by name, or handle
	*/
	public function talent_request_find () {
	
		$profile = Session::getProfile();
		
		$keyword = (!empty($_POST['name_handle'])) ? $_POST['name_handle'] : NULL;
		
		$this->view->talents = App::searchTalentsByName($keyword, $profile['id']);
		$this->view->html = $this->component('talent_request_result');
		$this->view->status = true;
		$this->view->id = 'talent_results';
		
		$this->render();
			
	}
	
	/**
	* Get previously sent pending request
	*/
	public function agency_pending_requests () {
		
		
		$profile = Session::getProfile();
		
		if($profile['type'] == 'a'){
			
			$this->view->html = $this->component('dashboard/talent_requests');
			$this->view->status = true;
			$this->view->id = 'dash-contents';
			
		}
		
		$this->render();
		
	}
	
	
	/**
	* Ask for access
	*/
	public function talent_request_access () {
		
		$profile = Session::getProfile();
		if($profile['type'] == 'a'){
			
			if(is_numeric($_POST['id'])){
			
				$agency = new Agency($profile['id']);
				$request = $agency->requestTalentAccess($_POST['id']);
				
				if($request->status == 	DataType::$AGENCY_REQEST_REJECTED || $request->status == DataType::$AGENCY_REQEST_AGENCY_REMOVED || $request->status ==DataType::$AGENCY_REQEST_TALENT_REMOVED) {
					
					// calculate time difference in days
					$diff = ceil((time() - strtotime($request->modified))/86400);
					
					if($diff >= 7){
						$request = $agency->requestTalentAccess($_POST['id'], true);
					}
					else{
						$html = 'You may send another request in '.$diff.' days.';	
					}
					
				}
				
				switch ($request->status){
					case DataType::$AGENCY_REQEST_PENDING : $html = '<div class="txt-blue">Requested</div> <a class="button-alt" cura="talent_request_cancel" data-id="'.$_POST['id'].'">Cancel</a>';
					break;
					case DataType::$AGENCY_REQEST_ACCEPTED : $html = "Accepted";
					break;

				}
				
				$this->view->html = $html;
				$this->view->id = 'talent_request_'.$_POST['id'];
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Invalid talent ID";
			}
			
		}
		
		
		$this->render();
	}
	
	/**
	* Cancel request before accept
	*/
	public function talent_request_cancel () {
		
		$profile = Session::getProfile();
		if($profile['type'] == 'a'){
			
			if(is_numeric($_POST['id'])){
			
				$agency = new Agency($profile['id']);
				
				$request = $agency->requestTalentAccess($_POST['id']);				
				
				
				if($request->status ==  DataType::$AGENCY_REQEST_PENDING) {
				
					$request->delete();
					
					$this->view->html = ' <a class="button" title="Request Access" cura="talent_request_access" data-id="'.$_POST['id'].'" >Request Access</a>';
					$this->view->id = 'talent_request_'.$_POST['id'];
					$this->view->status = true;
				
				}
				else{
					$this->view->error = "Request can not be canceled";	
				}
				
			}
			else{
				$this->view->error = "Invalid talent ID";
			}
			
		}
		
		
		$this->render();
	}
	
	/**
	* Agency remove client access from agency
	*/
	public function talent_remove_access () {
		
		$profile = Session::getProfile();
		
		if($profile['type'] == 'a' && !empty($_POST['talent']) && is_numeric($_POST['talent'])){
			
			$request = new AgencyRequest($_POST['talent']);
			//$request = $talent->getAgencyRequest ($profile['id']);
			
			if(!empty($request) && $request->status == DataType::$AGENCY_REQEST_ACCEPTED ){
				
				
				
				if(!empty($_POST['agency_confirmed'])) {
					//$talent->setNull('agency_id');
					$request->status = DataType::$AGENCY_REQEST_AGENCY_REMOVED;
					$request->save();
					
					$this->view->status = true;
					$this->view->action = 'get_dash_page';	
					$this->view->data = 'profile';
					$this->view->message  = "Talent removed";
					
				}
				else{
				
					$this->view->request = $request;
					$this->view->talent = $request->getTalent();
					$this->view->html = $this->component('/agency/talent_remove_confirm');
					$this->view->id = 'talent_manage_'.$_POST['talent'];
					
				}
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Show bookings and appoinments for Agency managing talents
	*/
	public function agency_talent () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$request = new AgencyRequest($_POST['id']);
			$profile = Session::getProfile();
			
			if(!empty($profile) && $profile['type'] == 'a'){
				
				if($request->ready() && $request->status == DataType::$AGENCY_REQEST_ACCEPTED && $request->agency_id == $profile['id']){
					
					$this->view->talent = $request->getTalent();
					$this->view->request = $request;
					
					if($_POST['action'] == 'appointments'){
						// show list of appointments
						//$this->view->date = date('Y-m-d');
						//$this->view->appointments = $this->view->talent->getAppoinments(date('Y-m-d'), NULL, true);
						$agency = $request->getAgency();
						
						$this->view->appointments = $agency->getTalentAppointments($request->talent_id, 1);
						$this->view->html = $this->component('/agency/talent_appoinments');
						
						
					}
					elseif ($_POST['action'] == 'new_appointment'){
						
						// show apointmnet form
						$this->view->html = $this->component('/agency/talent_appointment_form');
						
					}
					elseif ($_POST['action'] == 'edit_appointment'){
						
						// edit apointment fomr
						if(!empty($_POST['appointment']) && is_numeric($_POST['appointment'])) {
							
							$appointment = new Appoinment($_POST['appointment']);
							
							if($appointment->ready() && $appointment->talent_id == $request->talent_id){
								$this->view->appointment = $appointment;
								$this->view->html = $this->component('/agency/talent_appointment_form');	
							}
							else{
								$this->view->error = "Unauthorized request";	
							}
							
						}
						else{
							$this->view->error = "Invalid request";
						}
						
					}
					elseif ($_POST['action'] == 'bookings'){
						
						// booking for given talent
						//$this->view->request = $request;
						//$this->view->buffer = 
						$this->view->html = $this->component('/agency/talent_bookings');
					}
					elseif ($_POST['action'] == 'new_booking'){
						
						// show apointmnet form
						$this->view->html = $this->component('/agency/talent_booking_form');
						
					}
					elseif($_POST['action'] == 'edit_booking'){
						
						if(!empty($_POST['booking']) && is_numeric($_POST['booking'])){
							
							$booking = new Booking($_POST['booking']);
							
							if($booking->ready() && $booking->agency_id == $request->agency_id && $booking->status == DataType::$BOOKING_PENDING){
								$this->view->booking = $booking;
								$this->view->html = $this->component('/agency/talent_booking_form');
							}
							else{
								$this->view->error = "Unauthorized request";
							}
						}
						else{
							$this->view->error = "Invalid request";	
						}
						
						
					}
					
					$this->view->id = 'talent_manage_'.$_POST['id'];;
					
				}
				else{
					$this->view->error = "Unauthorized request";
				}				
			}
			
			
		}
		
		
		$this->render();
	}
	
	/**
	* List Agency talent appointments
	*/
	public function agency_talent_appointments () {
		
		$profile = Session::getProfile();
		
		if($profile['type'] == 'a' && !empty($_POST['talent']) && is_numeric($_POST['talent'])){
			
			$agency = new Agency($profile['id']);
			$request = new AgencyRequest($_POST['talent']);
			
			if($request->status == DataType::$AGENCY_REQEST_ACCEPTED && $request->agency_id = $profile['id']){
				
				$status = (isset($_POST['status']) && in_array($_POST['status'],array(0,1,2))) ? $_POST['status'] : 1;
				
				$date = strtotime($_POST['filter_date']);
				
				$date = (!empty($date)) ? $_POST['filter_date'] : date('Y-m-d'); 
			
				$this->view->appointments = $agency->getTalentAppointments($request->talent_id, $status, $date);
				$this->view->html = $this->component('/agency/talent_appointment_list');
				$this->view->id = 'appointments';
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
	}
	
	/**
	* Agency create an appointment for Talent
	*/
	public function agency_appointment_update () {
		
		
		if(!empty($_POST['request_id']) && is_numeric($_POST['request_id'])){
			
			$request = new AgencyRequest($_POST['request_id']);
			$profile = Session::getProfile();
			
			if(!empty($_POST['event_id']) && is_numeric($_POST['event_id'])){
				$event = new Event($_POST['event_id']);	
			}
			
			if(!empty($profile) && $profile['type'] == 'a'){
				
				if($request->ready() && $request->status == DataType::$AGENCY_REQEST_ACCEPTED && $request->agency_id == $profile['id']){
					
					$datetime = !empty(strtotime($_POST['appointment_begins_date'].' '. $_POST['appointment_begins_time'])) ? Util::ToSysDate($_POST['appointment_begins_date'], $_POST['appointment_begins_time']) : NULL;
					
					$span = (!empty($_POST['appointment_duration'])) ? Util::ToMinutes($_POST['appointment_duration']) : NULL;
					
					if(empty($datetime)){
						$this->view->error = "Invalid date time";
						return $this->render();
					}
					
					if(empty($span)){
						$this->view->error = "Invalid duration";
						return $this->render();
					}
					
					if(!empty($event) && $event->customer_id != $this->user->id){
						$this->view->error = "Invalid event";
						return $this->render();	
					}
					
					if(!empty($_POST['appointment_id']) && is_numeric($_POST['appointment_id'])){
				
						$appointment = new Appoinment($_POST['appointment_id']);
						
						if($appointment->talent_id != $request->talent_id || $appointment->agency_id != $request->agency_id){
							$this->view->error = "Unauthorized access";
							return $this->render();
						}
						
						$this->view->message = "Appointment updated";
						
					}
					else{
						$appointment = new Appoinment();						
						$appointment->talent_id = $request->talent_id;
						$appointment->agency_id = $request->agency_id;
						
						$this->view->message = "Appointment created";
					}
			
					$appointment->description = Validator::cleanup($_POST['appointment_description'], 400);
					$appointment->begins = $datetime;
					$appointment->duration =  $span;
					
					if(!empty($event)){
						$appointment->event_id = $event->id;	
					}
					$appointment->save();
					
					// display same
					$this->view->talent = $request->getTalent();
					$agency = $request->getAgency();
					$this->view->request = $request;
					
					$this->view->date = $appointment->begins;

					$this->view->appointments = $agency->getTalentAppointments($request->talent_id, 1, NULL);
					$this->view->html = $this->component('/agency/talent_appoinments');
					
					$this->view->id = 'talent_manage_'.$request->id;
					$this->view->status = true;
					
				}
				else{
					$this->view->error = "Unauthorized request";
				}
			}
			else{
				$this->view->error = "Unauthorized request";
			}
		}
		
		$this->render();
	}
	
	
	/**
	* Agency remove an appointment from Talent
	* id : request id
	* appointment : appointment id
	*/
	public function agency_appointment_remove () {
				
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$request = new AgencyRequest($_POST['id']);
			$profile = Session::getProfile();
			
			if(!empty($profile) && $profile['type'] == 'a'){
				
				if($request->ready() && $request->status == DataType::$AGENCY_REQEST_ACCEPTED && $request->agency_id == $profile['id']){
					
					if(!empty($_POST['appointment']) && is_numeric($_POST['appointment'])){
				
						$appointment = new Appoinment($_POST['appointment']);
						
						if($appointment->talent_id != $request->talent_id || $appointment->agency_id != $request->agency_id){
							$this->view->error = "Unauthorized access";
							return $this->render();
						}
						
						$appointment->delete();
						
						$this->view->message = "Appointment removed";
						
						// display same
						$this->view->talent = $request->getTalent();
						$this->view->request = $request;
						
						$this->view->date = $appointment->begins;
						$this->view->appointments = $this->view->talent->getAppoinments(Util::ToDate($appointment->begins), NULL, true);
						$this->view->html = $this->component('/agency/talent_appoinments');
						
						$this->view->id = 'talent_manage_'.$request->id;
						$this->view->status = true;
						
					}
					else{						
						$this->view->error = "Invalid request";
					}
			
				}
				else{
					$this->view->error = "Unauthorized request";
				}
			}
			else{
				$this->view->error = "Unauthorized request";
			}
		}
		
		$this->render();
	}
	
	/**
	* Agency making abooking
	*/
	public function agency_booking_update () {
		
		if(!empty($_POST['request_id']) && is_numeric($_POST['request_id'])){
			
			
			if(empty($_POST['session_id']) || !is_numeric($_POST['session_id'])){
				$this->view->error = "Please select a package";
				return $this->render();	
			}
			
			
			if(empty($_POST['b_date']) || empty($_POST['b_time']) || empty(strtotime($_POST['b_date'].' '.$_POST['b_time']))){
				
				$this->view->error = "Invalid date time";
				return $this->render();	
			}
						
			$request = new AgencyRequest($_POST['request_id']);
			$talent = $request->getTalent();
			$package = new TalentPackage($_POST['session_id']);
			$profile = Session::getProfile();
			$datetime = Util::ToSysDate($_POST['b_date'], $_POST['b_time']);
			
			if(!empty($profile) && $request->ready() && $profile['type'] == 'a' && $profile['id'] == $request->agency_id){
				
				if(!$talent->isBookingPossible($datetime, $package) && empty($_POST['override'])){
						$this->view->talent = $talent;
						$this->view->package = $package;
						
						$this->view->html = $this->component('booking/missed');
						$this->view->message = "There is ";
						$this->view->action = 'popup';
						
						$this->view->data = array(
							//'title' => 'Appointment',
							'para' => array( ),
							'mask' => true,
							'buttons' => false
						);
						
						return $this->render();
						
				}
				
				if(!empty($_POST['booking_id']) && is_numeric($_POST['booking_id'])){
					$booking = new Booking($_POST['booking_id']);
					
					if($booking->status != DataType::$BOOKING_PENDING ){
						$this->view->error = "You can not modify this booking";	
						return $this->render();
					}
					
					$loc = $booking->getLocation();
					$loc = Util::saveLocation($loc);
				}
				else{
					$booking = new Booking();
					$booking->talent_id = $request->talent_id;
					$booking->agency_id = $request->agency_id;
					$booking->customer_id = $this->user->id;
					
					$loc = Util::saveLocation();
					
					$booking->location_id = $loc->id;
				}
				
				$booking->venue = Validator::cleanup($_POST['place'], 400);;
				$booking->timeoffset = $talent->timeoffset;
				$booking->package_id = $package->id;
				
				$booking->session_start = Util::ToSysDate($_POST['b_date'], $_POST['b_time']);
				$booking->session_ends = date("Y-m-d H:i:s", strtotime($booking->session_start) + $package->timespan*60);
				$booking->status = DataType::$BOOKING_PENDING;
				$booking->talent_fee = $package->fee;
				
				$booking->charges =  App::bookingCharge($package);	
				$booking->currency = $talent->currency;
				$booking->advance_percentage = 0; //$package->advance_percentage;
				$booking->status_changed = date("Y-m-d H:i:00");
				$booking->setDueDate();
				$booking->save();
				
				// show newly created booking
				$this->view->request = $request;
				$this->view->html = $this->component('/agency/talent_bookings');
						
				$this->view->id = 'talent_manage_'.$request->id;
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
		
	}
	

	/**
	* Show booking view
	*/
	public function agency_booking_view () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$profile = Session::getProfile();
			$booking = new Booking($_POST['id']);
			
			if(!empty($profile) && $profile['type'] == 'a' && $profile['id'] == $booking->agency_id){
				$agency = $booking->getAgency();
				
				$request = $agency->getTalentRequest($booking->talent_id);
				
				$this->view->booking = $booking;
				$this->view->request = $request;
				$this->view->html = $this->component('/agency/talent_booking_view');
				$this->view->id = 'talent_manage_'.$request->id;
				$this->view->status = true;
			}
			
			
		}
		
		$this->render();
	}
		
	/**
	* Creating an apointment for an event
	*/
	public function agency_event_appointment() {
		
		if(!empty($_POST['event']) && is_numeric($_POST['event']) && !empty($_POST['talent']) && is_numeric($_POST['talent'])){
			
			$event = new Event($_POST['event']);
			$talent = new Talent($_POST['talent']);
			
			if($event->ready() && $event->customer_id == $this->user->id){				
				
				if($talent->ready()) {
					$this->view->talent = $talent;
				}
				
				$this->view->event = $event;
				$this->view->with_header = true;
				$this->view->html = $this->component('/agency/talent_appointment_form');
				$this->view->id = 'new_appointment';
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		
		$this->render();
		
	}
	
	
	/**
	* List talents associated to given agency
	*/
	public function agency_talent_select () {
		
		$profile = Session::getProfile();
		
		if(!empty($profile) && $profile['type'] == 'a'){
			
			$agency = new Agency($profile['id']);
			$this->view->talents = $agency->getTalents(true);
			
			if(!empty($_POST['cura'])){
				$this->view->cura = $_POST['cura'];	
			}
			
						
			$this->view->html = $this->component('agency/select_talents');
			
			if(!empty($_POST['id'])) {
				$this->view->id = $_POST['id'];
			}
			
			
			
		}
		
		$this->render();
	}
	
	/**
	* Respond to agency request
	*/
	public function agency_request_respond () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			$request = new AgencyRequest($_POST['id']);
			$talent = $request->getTalent();
			
			if($talent->customer_id = $this->user->id){
				
				$this->view->request = $request;
				
				$this->view->status = true;
				
				if($request->status != DataType::$AGENCY_REQEST_PENDING){
					
					$this->view->html = "<div class='padding'>You already ".(($request->status == DataType::$AGENCY_REQEST_ACCEPTED) ? 'accepted' : 'rejected')." this request</div>";
					$this->view->id = 'agency_request_'.$_POST['id'];	
					
					return $this->render();
				}
				
				
				if($_POST['action'] == 'accept'){
					$this->view->html = $this->component('talent/agency_request_accept');
				}
				else {
					$this->view->html = $this->component('talent/agency_request_reject');
				}
				
				
			}
			else{
				$this->view->error = 'Unauthorized request';	
			}
			
			
			$this->view->id = 'agency_request_'.$_POST['id'];
		}
		
		
		$this->render();
		
	}
	
	/**
	* Accept agency requets
	*/
	public function agency_request_accept (){
		
		if(!empty($_POST['request_id']) && is_numeric($_POST['request_id'])){
			
			$request = new AgencyRequest($_POST['request_id']);
			
			if($request->status != DataType::$AGENCY_REQEST_PENDING){
					
				$this->view->html = "<div class='padding'>You already ".(($request->status == DataType::$AGENCY_REQEST_ACCEPTED) ? 'accepted' : 'rejected')." this request</div>";
				
				$this->view->id = 'agency_request_'.$_POST['request_id'];	
					
				return $this->render();
			}
			
			$talent = $request->getTalent();
			
			if($talent->customer_id == $this->user->id){
				
				//value=ok&_action=agency_request_accept_confirm&request_id=9&confirm_appointments=on&view_bookings=on&view_booking_detail=on&manage_bookings=on&manage_page=on&tz=330
				
				$request->status = DataType::$AGENCY_REQEST_ACCEPTED;
				
				$request->confirm_appointments = !empty($_POST['confirm_appointments']) ? 1 : 0;
				
				
				if(!empty($_POST['view_bookings'])) {
					
					$request->view_bookings = 1;
					
					if(!empty($_POST['view_booking_detail'])) {
						$request->view_booking_detail = 1;
						
						if(!empty($_POST['manage_bookings'])) {
							$request->manage_bookings = 1;
						}
						else{
							$request->manage_bookings = 0;
						}
					}
					else{
						$request->view_booking_detail = 0;
						$request->manage_bookings = 0;
					}				
				}
				else{
					$request->view_bookings = 0;
					$request->view_booking_detail = 0;
					$request->manage_bookings = 0;	
				}

				$request->confirm_appointments =  !empty($_POST['confirm_appointments']) ? 1 : 0; 
				$request->manage_page = !empty($_POST['manage_page']) ? 1 : 0;
				$request->save();
				
				$this->view->status = true;
				$this->view->action = 'get_dash_page';	
				$this->view->data = 'profile';
				$this->view->message  = "Request accepted";
						
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		else{
			$this->view->error = "Invalid request";	
		}
		
		$this->render();
	}
	
	/**
	* Rejecting agency requets
	*/
	public function agency_request_reject (){
		
		if(!empty($_POST['request_id']) && is_numeric($_POST['request_id'])){
			
			$request = new AgencyRequest($_POST['request_id']);
			
			if($request->status != DataType::$AGENCY_REQEST_PENDING){
					
				$this->view->html = "<div class='padding'>You already ".(($request->status == DataType::$AGENCY_REQEST_ACCEPTED) ? 'accepted' : 'rejected')." this request</div>";
				
				$this->view->id = 'agency_request_'.$_POST['request_id'];	
					
				return $this->render();
			}
			
			$talent = $request->getTalent();
			
			if($talent->customer_id == $this->user->id){
				
				//value=ok&_action=agency_request_accept_confirm&request_id=9&confirm_appointments=on&view_bookings=on&view_booking_detail=on&manage_bookings=on&manage_page=on&tz=330
				
				$request->status = DataType::$AGENCY_REQEST_REJECTED;		
				
				$request->save();
				
				$this->view->status = true;
				$this->view->action = 'recent';	
				//$this->view->data = 'profile';
				$this->view->message  = "Request rejected";
						
				
			}
			else{
				$this->view->error = "Unauthorized request";	
			}
			
		}
		else{
			$this->view->error = "Invalid request";	
		}
		
		$this->render();
	}
	
	
	
	
	/**
	* Display talent request manage form for talent
	*/
	public function agency_access_manage () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
		
			$request = new AgencyRequest($_POST['id']);
			
			$talent = $this->user->hasTalentAccess($request->talent_id);
			
			if(!empty($talent)){
				
				$this->view->request = $request;
				$this->view->html = $this->component('talent/agency_access_manage');
				$this->view->id = 'agency_access_'.$_POST['id'];
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";
			}
			
			
		}
		
		$this->render();
	}
	
	/**
	* Update existing agency acceess
	*/
	public function agency_access_update (){
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
		
			$request = new AgencyRequest($_POST['id']);
			
			$talent = $this->user->hasTalentAccess($request->talent_id);
			
			if(!empty($talent) && $request->status == DataType::$AGENCY_REQEST_ACCEPTED){
				
				if(!empty($_POST['view_bookings'])) {
					
					$request->view_bookings = 1;
					
					if(!empty($_POST['view_booking_detail'])) {
						$request->view_booking_detail = 1;
						
						if(!empty($_POST['manage_bookings'])) {
							$request->manage_bookings = 1;
						}
						else{
							$request->manage_bookings = 0;
						}
					}
					else{
						$request->view_booking_detail = 0;
						$request->manage_bookings = 0;
					}				
				}
				else{
					$request->view_bookings = 0;
					$request->view_booking_detail = 0;
					$request->manage_bookings = 0;	
				}
				
				$request->confirm_appointments =  !empty($_POST['confirm_appointments']) ? 1 : 0; 

				$request->manage_page = !empty($_POST['manage_page']) ? 1 : 0;
				$request->save();
				
				$this->view->html = '<div></div>';
				$this->view->id = 'agency_access_'.$_POST['id'];
				$this->view->message = "Permission updated";
				$this->view->status = true;
				
			}
			else{
				$this->view->error = "Unauthorized request";
			}
			
		}
		
		$this->render();	
	}
	
	/**
	* Remove agency access
	*/
	public function agency_access_remove () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
		
			$request = new AgencyRequest($_POST['id']);
			
			$talent = $this->user->hasTalentAccess($request->talent_id);
			
			if(!empty($talent) && $request->status == DataType::$AGENCY_REQEST_ACCEPTED){
				
				$request->status = DataType::$AGENCY_REQEST_TALENT_REMOVED;
				$request->save();
				
				$this->view->status = true;
				$this->view->action = 'get_dash_page';	
				$this->view->data = 'profile';
				$this->view->message  = "Agency removed";
			}
		}
		else{
				$this->view->error = "Unauthorized request";
			}
		
		$this->render();
	}
	
	
	
	/***************** Override render function for easy access *************/
	
	public function  render ($view_path = '') {
		
		parent::render('/common/void');
		
	}
	

}


