<?php

class ChatController extends Controller {
	
	
	protected $init_user = false;

	
	function __construct ($request = NULL) {
			
		
		 parent::__construct($request);
		 
		 $this->template = 'ajax.php';
		 
		  if(!$this->auth->isAuthenticated()){
				$this->redirect('/login'); 
		 }
		 
		 array_shift($this->data);
		 
		 if(empty($this->data[0])){
			 
			$this->redirect('/dashboard'); 
		 }
		 elseif(method_exists($this, $this->data[0])){
			 			 
			 $method = array_shift($this->data);
			 $this->$method();
		 }
		 else{
			$this->render('/front/404'); 
		 }
		
	}
	
	
	
	/**
	* Get data to open a chat room
	* If there is no chatroom for given object, create and return
	*/
	public function open () {
		
		if(!empty($_POST['board']) || !empty($_POST['event']) || !empty($_POST['booking']) || !empty($_POST['talent']) || !empty($_POST['agency']) || !empty($_POST['customer']) ) {
			
			$chat = new Chat();
			$board = $chat->getBoard();

			if(!empty($board)){
				$this->view->data = $board->getinfo();
				$this->view->status = true;
				$this->view->action = 'chat_show';	
			}
			else{
				$this->view->error = $chat->error;
			}
		}
		
		$this->render();
	}
	
	/**
	* Get chat feed
	* if last id is empty, retuning from top
	*/
	public function feed () {
		
	//	print_r($_POST);
	//	exit();
		
		if(!empty($_POST['id']) && is_numeric($_POST['id']) ){
			
			$customer = Session::getCustomer();
				
			$chat = new Chat();
			
			if(isset($_POST['l'])) {
			
				$last = (!empty($_POST['l']) && is_numeric($_POST['l'])) ? $_POST['l'] : 0;
			
				$this->view->data = array( 
					 'board' => $_POST['id'],
					 'messages' => $chat->getUpdates($last, $_POST['id'] ),
					 'self' => $customer['id'],
					 'dir' => false
					);
				
			}
			elseif(isset($_POST['f'])) {
				
				$last = (!empty($_POST['f']) && is_numeric($_POST['f'])) ? $_POST['f'] : 0;
			
				$this->view->data = array( 
					 'board' => $_POST['id'],
					 'messages' => $chat->getHistory($last, $_POST['id'] ),
					 'self' => $customer['id'],
					 'dir' => true
					);
				
				
			}
			
			$this->view->status = true;
			$this->view->action = 'show_chat_messages';
			
		}
		
		$this->render();
	}
	
	/**
	* Get updates for the chat room
	*/
	public function updates () {
		
		if(isset($_POST['l'])){
			
			$customer = Session::getCustomer();
				
			$chat = new Chat();
			
			$last = (!empty($_POST['l']) && is_numeric($_POST['l'])) ? $_POST['l'] : 0;
			
			$this->view->data = array( 
				 'boards' => $chat->getUpdates($last),
				 'self' => $customer['id']
				);
			
			$this->view->status = true;
			$this->view->action = 'show_chat_messages';
			
		}
		
		$this->render();
	}
	
	/**
	* Manage chat room
	* mute/block or delete room
	*/
	public function manage () {
		
		if(!empty($_POST['id']) && is_numeric($_POST['id'])){
			
			
			if(!empty($_POST['action']) && 
				in_array($_POST['action'], array('clear','mute','unmute','block','unblock','remove','removeall'))){
				
				
				
				if($_POST['action'] == 'clear'){
					$board = new MessageBoard($_POST['id']);
					$board->clearMessages();
				}
				elseif($_POST['action'] == 'mute'){
					$board = new MessageBoard($_POST['id']);
					$board->mute();	
				}
				elseif($_POST['action'] == 'unmute'){
					$board = new MessageBoard($_POST['id']);
					$board->unMute();	
				}
				elseif($_POST['action'] == 'block'){
					$board = new MessageBoard($_POST['id']);
					$board->block();	
				}				
				elseif($_POST['action'] == 'unblock'){
					$board = new MessageBoard($_POST['id']);
					$board->unBlock();	
				}
				elseif($_POST['action'] == 'quit'){
					$board = new MessageBoard($_POST['id']);
					$board->quit();	
				}
				elseif($_POST['action'] == 'removeall'){
					$message = new Message($_POST['id']);
					$message->remove(true);
				}
				elseif($_POST['action'] == 'remove'){
					$message = new Message($_POST['id']);
					$message->remove(false);	
				}
				
				$this->view->status = true;
				$this->view->data = array(
					'id' => $_POST['id'],
					'action' => $_POST['action']
				);
				$this->view->action = "board_action";
			}
			else{
				$this->view->error = "Invalid action";	
			}
			
			
			
		}
		
		
		$this->render();
	}
	
	/**
	* Send a message
	*/
	public function send () {
		
		if(!empty($_POST['board']) && is_numeric($_POST['board']) && !empty($_POST['message'])){
			
			$chat = new Chat();

			$res = $chat->send($_POST['board'], $_POST['message']);
			
			$customer = Session::getCustomer();
			
			if($res){
				$this->view->status = true;
				
				$last = (!empty($_POST['l']) && is_numeric($_POST['l'])) ? $_POST['l'] : 0;
				
				$this->view->data = array( 
					 'boards' => $chat->getUpdates($last),
					 'self' => $customer['id']
					);
				
				$this->view->action = 'show_chat_messages';				

			}
			else{
				$this->view->error = $chat->error;	
			}
			
			
		}
		else{
			$this->view->error = "Invalid message";	
		}
		
		$this->render();
	}
	
	/**
	* Update seen 
	*/
	public function ack () {
		
		if(!empty($_POST['b']) && is_numeric($_POST['b']) && !empty($_POST['l']) && is_numeric($_POST['l'])){
			
			$chat = new Chat();
			$chat->ack($_POST['b'], $_POST['l']);
			$this->view->status = true;
		}
		
	}
	
	
	/***************** Override render function for easy access *************/
	
	public function  render ($view_path = '') {
		
		parent::render('/common/void');
		
	}
}
?>