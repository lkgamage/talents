<?php

/**
 * @package Curatalent
 * Auto class loader on demand
 */

// loading gloabl functions
//include_once(dirname(__FILE__) . "/phpmailer/class.phpmailer.php");

include_once(dirname(__FILE__) . "/functions.php");
include_once(dirname(__FILE__) . "/vendors/Aws/functions.php");
include_once(dirname(__FILE__) . "/vendors/GuzzleHttp/functions.php");
include_once(dirname(__FILE__) . "/vendors/GuzzleHttp/Psr7/functions.php");
include_once(dirname(__FILE__) . "/vendors/GuzzleHttp/Promise/functions.php");

$GLOBALS['start_time'] = microtime(true);

// Autoloader
function talentploader ($class) {
	
	$class = str_replace('\\','/', $class);
	//var_dump(file_exists(dirname(__FILE__) . "/vendors/" . $class . ".php"));
	
	if (file_exists(dirname(__FILE__) . "/" . $class . ".php")) {
		include_once(dirname(__FILE__) . "/" . $class . ".php");
	} elseif (file_exists(dirname(__FILE__) . "/models/" . $class . ".php")) {
		include_once(dirname(__FILE__) . "/models/" . $class . ".php");
	} elseif (file_exists(dirname(__FILE__) . "/controllers/" . $class . ".php")) {
		include_once(dirname(__FILE__) . "/controllers/" . $class . ".php");
	}
	 elseif (file_exists(dirname(__FILE__) . "/engines/" . $class . ".php")) {
		include_once(dirname(__FILE__) . "/engines/" . $class . ".php");
	}
	elseif (file_exists(dirname(__FILE__) . "/vendors/" . $class . ".php")) {
		include_once(dirname(__FILE__) . "/vendors/" . $class . ".php");
	}	
	elseif (file_exists(dirname(__FILE__) . "/vendors/Aws/S3/" . $class . ".php")) {
		include_once(dirname(__FILE__) . "/vendors/Aws/S3/" . $class . ".php");
	}
	else {
	 	$class = strtolower($class);

		if (file_exists(dirname(__FILE__) . "/" . $class . ".php")) {
			include_once(dirname(__FILE__) . "/" . $class . ".php");
		} elseif (file_exists(dirname(__FILE__) . "/models/" . $class . ".php")) {
			include_once(dirname(__FILE__) . "/models/" . $class . ".php");
		} elseif (file_exists(dirname(__FILE__) . "/controllers/" . $class . ".php")) {
			include_once(dirname(__FILE__) . "/controllers/" . $class . ".php");
		}
		 elseif (file_exists(dirname(__FILE__) . "/engines/" . $class . ".php")) {
			include_once(dirname(__FILE__) . "/engines/" . $class . ".php");
		}
		elseif (file_exists(dirname(__FILE__) . "/vendors/" . $class . ".php")) {
			include_once(dirname(__FILE__) . "/vendors/" . $class . ".php");
		}
		else{
			echo dirname(__FILE__) . "/vendors/Aws/S3/" . $class . ".php\n";
		}
	}
		
		
	/*elseif (file_exists(dirname(__FILE__) . "/widgets/" . $class . ".php")) {
        include_once(dirname(__FILE__) . "/widgets/" . $class . ".php");
    }*/
    /*
    elseif(strpos($class,'controller') != false ){
        include_once(dirname(__FILE__)."/controllers/".str_replace('controller','Controller', $class).".php");
    }
    elseif(strpos($class,'viewer') != false ){
        include_once(dirname(__FILE__)."/".str_replace('viewer','Viewer', $class).".php");
    }*/
}

function closingJob () {
	//echo '/*'. memory_get_usage().'*/';
	
	if(Config::$log){
	
		$paths = RequestObject::$paths;

		$log = new Accesslog();
		$log->controller = isset($paths[0]) ? $paths[0] : 'landing';
		$log->method = isset($paths[1]) ? $paths[1] : '';
		$log->parameter = isset($paths[2]) ? $paths[2] : '';
		$log->exec_time = microtime(true) - $GLOBALS['start_time'];
		$log->memory = round( memory_get_usage()/1024);
		$log->peak = round(memory_get_peak_usage()/1024);
		$log->num_queries = Database::$numq;
		$log->country = Session::getCountry();
		$log->save();
		
	}
	
	// closing ,mysql connection
	Database::close();
	
	// creating log file
	// manage files
	$file = Config::$log_path.'/applog.txt';
	
	if(file_exists($file)) {
		// file size > 10mb, create new file
		if(filesize($file) > 1048576){
			rename($file, Config::$log_path.'/applog-'.date('Y-m-d-H-i').'.csv');
		}		
	}
	
	file_put_contents($file, implode('', Session::$logs), FILE_APPEND);

}

spl_autoload_register('talentploader');
register_shutdown_function ('closingJob');



?>