<?php
/**

 * Mysqli database connection class
 *
 * This class provides basic CRUD operations
 * All instance of this calss hold same undelaying database connection
 */

include_once('config.php');

class Database {
	
    private static $conn;
	
	public static $numq = 0;

    private $debug = false;

    function __construct () {

        if (!self::$conn) {
            $this->connect();
        }
    }

    /**
     * connect to oracle if not connected yet
     * or use existing open connection
     */
    public function connect () {
		
		if (!self::$conn) {
			self::$conn = new mysqli(Config::$db_server, Config::$db_user, Config::$db_pass,Config::$db_database);
			if (self::$conn->connect_error) {
				$this->showError(debug_backtrace(), array('message' => 'Mysql connection failed', 'sqltext' => ""));
			}
		}
    }

    /**
     * Check whether oracle server connected
     * if not attempt to connect
     */
    private function isConnected () {

        if (!self::$conn) {
            $this->connect();
        }

        if (self::$conn) {
            return true;
        }

        return false;
    }

    /**
     * Close database connection explicitly
     * not required to call unless you want to close connection at the middle of the process
     * connection will be automatically close after executing the function
     */
    public static function close () {

        self::$conn->close();
        self::$conn = null;
    }

    /**
     * Run query statement on the database
     * statement should have placeholders for varable values
     * values array shoukld contain key => value pair of field and values
     *
     * Statement : SELECT * FROM SITES WHERE ID = :id
     * values : array ('id' => 5)
     * limit: number of records to fetch
     * skip: how many records to skip from top of results (paging)
     * return array of result or a row if $limit = 1
     * return FALSE on error
     */
    public function query ($statement, $values = array(), $limit = -1, $skip = 0) {

        if (!$this->isConnected()) {
            return false;
        }

		self::$numq++;
			
		if (!empty($values)) {
			
			foreach ($values as $k => $v){
				$v = self::$conn->escape_string($v);
				
				if(is_numeric($v)){
					$statement = str_replace(':'.$k, $v, $statement);
				}else{
					$statement = str_replace(':'.$k, "'".$v."'", $statement);
				}				
			}			
		}
		
		if($limit > 0){
			$statement .= " limit {$skip},{$limit}";	
		}
		


        $res = self::$conn->query($statement);
		
		if(!$res){
			$this->showError(debug_backtrace(), array('message' => self::$conn->error, 'sqltext' => $statement));
		}
		
		if($limit == 1){
			
			if($res){
				$row = $res->fetch_array(MYSQLI_ASSOC);
				$res->free();
				return $row;
			}
			
			return array();
		}
		else{
			$data = array();
			
			if($res){
				while($row = $res->fetch_array(MYSQLI_ASSOC)){
					$data[] = $row;	
				}
				
				$res->free();			
				return $data;
			}
			
			return array();
		}
        
		
       
    }

    /**
     * Run query on the DB where it doesn't returen results such as INSERT, UPDATE or DELETE
     * statement should have placeholders for varable values
     * values array shoukld contain key => value pair of field and values
     * return number of rows effected
     * return FALSE on error
     */
    public function execute ($statement, $values = array()) {

        if (!$this->isConnected()) {
            return false;
        }

        if (!empty($values)) {
			
			foreach ($values as $k => $v){
				$v = self::$conn->escape_string($v);
				
				if(is_numeric($v)){
					$statement = str_replace(':'.$k, $v, $statement);
				}else{
					$statement = str_replace(':'.$k, "'".$v."'", $statement);
				}				
			}			
		}
		
		$res = self::$conn->query($statement);
		if(!$res){
			$this->showError(debug_backtrace(), array('message' => self::$conn->error, 'sqltext' => $statement));
		}
		
        
        return self::$conn->affected_rows;
    }

    /**
     * insert record into a table
     * table : table name
     * values : key value pair of fields to be populated
     * return newly created row ID
     * return FALSe on error
     */

    public function insert ($table, $values) {

        if (!$this->isConnected()) {
            return false;
        }
/*
        $idKey = $this->getIDKeyName($values);
        if ($idKey !== false) {
            unset($values[$idKey]);
        }
*/		
		$values['created'] = 'NOW';
		$values['modified'] = 'NOW';

        $keys = array_keys($values);

        $sql = "INSERT INTO {$table} ";
        $sql .= "(`" . implode("`,`", $keys) . "`) ";
        
		
		$ka = array();
		foreach ($values as $k => $v) {
			
			if ($v === 'NOW' || $v === 'now') {
				$ka[] = 'NOW()';
			}
			else{
				$ka[] = "'".self::$conn->escape_string($v)."'";	
			}
		}
		
		$sql .= "VALUES (". implode(",", $ka) .") ";

        $res = self::$conn->query($sql);
		if(!$res){
			$this->showError(debug_backtrace(), array('message' => self::$conn->error, 'sqltext' => $sql));
		}

        return self::$conn->insert_id;
    }

    /**
     * upadet record in the table
     * table : table name
     * values : key value pair of fields to be updated
     * return 1 on success, 0 on failur or record not found
     * return FALSE on error
     */
    public function update ($table, $values) {

        if (!$this->isConnected()) {
            return false;
        }

        $idKey = $this->getIDKeyName($values);
        if ($idKey === false) {
            // generate exception
            $this->showError(debug_backtrace(), array('message' => 'ID not present', 'sqltext' => ""));

            return false;
        }
		
		$values['modified'] = 'NOW';
		
		$ka = array();
		foreach ($values as $k => $v) {
			
			if ($k == $idKey) {
                continue;
            }
			
			if(is_null($v)){
				continue;
			}
			
			if ($v === 'NOW' || $v === 'now') {
				$ka[] = '`'.$k.'` = NOW()';
			}
			else{
				$ka[] = '`'.$k."` = '".self::$conn->escape_string($v)."'";	
			}
		}

        $sql = "UPDATE {$table} SET ";
        $sql .= implode(", ", $ka);
        $sql .= " WHERE id = '".self::$conn->escape_string($values[$idKey])."'";

        $res = self::$conn->query($sql);
		if(!$res){
			$this->showError(debug_backtrace(), array('message' => self::$conn->error, 'sqltext' => $sql));
		}
        
        return self::$conn->affected_rows;
    }

    /**
     * +++++ DO NOT +++++ use this method unless you really wants to delete a record
     * permanatly delete record
     * table: table name
     * id : row id
     * return 1 on success, 0 on faliur
     * return FALSE on error
     */
    public function hardDelete ($table, $id) {

        if (!$this->isConnected()) {
            return false;
        }

        if (empty($id)) {
            // generate exception
            $this->showError(debug_backtrace(), array('message' => 'ID not present', 'sqltext' => ""));

            return false;
        }

        $sql = "DELETE FROM {$table} WHERE id = '".self::$conn->escape_string($id)."'";

        $res = self::$conn->query($sql);
		if(!$res){
			$this->showError(debug_backtrace(), array('message' => self::$conn->error, 'sqltext' => $statement));
		}
        
         return self::$conn->affected_rows;
    }

    /**
     * soft delete record
     * table: table name
     * id : row id
     * return 1 on success, 0 on faliur
     * return FALSE on error
     */
    public function softDelete ($table, $id) {

        if (!$this->isConnected()) {
            return false;
        }

        if (empty($id)) {
            // generate exception
            $this->showError(debug_backtrace(), array('message' => 'ID not present', 'sqltext' => ""));

            return false;
        }

        $sql = "UPDATE {$table} SET `deleted` = NOW(), `modified` = NOW() WHERE id = '".self::$conn->escape_string($id)."'";
		

         $res = self::$conn->query($sql);
		 if(!$res){
			$this->showError(debug_backtrace(), array('message' => self::$conn->error, 'sqltext' => $statement));
		}
        
         return self::$conn->affected_rows;
    }

    /**
     * undo soft delete
     * table: table name
     * id : row id
     * return 1 on success, 0 on faliur
     * return FALSE on error
     */
    public function undoSoftDelete ($table, $id) {

        if (!$this->isConnected()) {
            return false;
        }

        if (empty($id)) {
            // generate exception
            $this->showError(debug_backtrace(), array('message' => 'ID not present', 'sqltext' => ""));

            return false;
        }

        $sql = "UPDATE {$table} SET `deleted` = NULL, `modified` = NOW() WHERE id = '".self::$conn->escape_string($id)."'";

         $res = self::$conn->query($sql);
		 if(!$res){
			$this->showError(debug_backtrace(), array('message' => self::$conn->error, 'sqltext' => $statement));
		}
        
         return self::$conn->affected_rows;
    }

    /**
     * private function to find key name (id) in values array
     * this will test for different combination on upper/lower cases
     * return key name
     * return FALSE when not found id field
     */
    private function getIDKeyName ($array) {

        foreach (array('id', 'ID', 'Id', 'iD') as $kn) {
            if (isset($array[$kn])) {
                return $kn;
            }
        }

        return false;
    }

    /**
     * Display errors
     * if DEBUG is ON in config file, this function shows database error and calling script info
     *  TO DO: Extend this method to throws exception if nessasary
     */
    private function showError ($trace, $error) {

        if (Config::$debug) {

            echo '<style>.dberror{background-color:#999;} .dberror td{background-color:#fff;}</style>
				<table border="0" cellspacing="1" cellpadding="5" class="dberror">
				  <tr>
					<td colspan="2" style="background-color:#F00; color:#fff; font-weight:bold">DATABASE ERROR | ' . $error['message'] . '</td>
				  </tr>
				  <tr>
					<td>SQL</td>
					<td>' . $error['sqltext'] . '</td>
				  </tr>
				  <tr>
					<td>Method</td>
					<td>' . $trace[0]['function'] . '</td>
				  </tr>';
            foreach ($trace as $t) {

                echo ' <tr>
					<td>File</td>
					<td>' . $t['file'] . ' on line ' . $t['line'] . '</td>
				  </tr>';
            }
            echo '	</table>';
        }
		
		applog('DATABASE ERROR | '.$error['message'] .' | '.$trace[2]['file'].':'.$trace[2]['line']);
		
    }
	
	public function escape ( $str ){
		
		return self::$conn->escape_string($str);
			
	}

    private function logquery ($msg, $para = array()) {

        if (!$this->debug) {
            return;
        }

        $str = "";

        if (!empty($para)) {

            $str = "\r\nPARA: ";

            foreach ($para as $key => $val) {
                $str .= $key . ":" . $val . ", ";
            }

            $msg .= $str;
        }

        list($sc, $mt) = explode(".", microtime(true));
        $msg = date("m/d/Y H:i:s") . "." . $mt . "\t" . $msg;

        $link = fopen(dirname(__FILE__) . "/log.txt", 'a');
        fwrite($link, "\r\n" . $msg);
        fclose($link);
    }
} // class Database

?>