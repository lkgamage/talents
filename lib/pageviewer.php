<?php

/**
 * @package Amplify
 * Template engine class
 *
 */
class PageViewer {
    /** store all data that pass into template */
    private $data = array();

    /** directive to turn on/off avarioable evaluation */
    public $evaluate_vars = true;

    function __construct () {
    }

    /**
     * Magic method getter for class properies
     */
    public function __get ($name) {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        return null;
    }

    /**
     * Magic method setter for class properties
     */
    public function __set ($name, $value) {
        $this->data[$name] = $value;
        /*if(preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/',$name)){
            
        }
        else{
            Error::InvalidVarName();	
        }*/
    }

    /**
     * Magic method isset for class properties
     */
    public function __isset ($name) {

        return !empty($this->data[$name]);
    }

    /**
     * Render statip page
     */
    public function showStatic ($view_html, $template_html, $files = NULL) {

        // isolate page rendering function to avoid execusion of injected  php codes
        PageViewer_showPage($view_html, $template_html, $this->data, $files);
		
    }
	
	public function component ($view_html){
		
		
		
		return PageViewer_getPage($view_html, $this->data);
	}

   
} // calss

function PageViewer_showPage ($view_html, $template_html, $_data = array(), $__files = NULL) {

    // get data into current symbolic table
    if (!empty($_data)) {
        extract($_data);
    }
		

    // execute view
    ob_start();
    eval('?>' . $view_html);
    $contents = ob_get_clean();

    // if anything changed, get fresh data into current symbolic table
    if (!empty($_data)) {
        extract($_data);
    }

    // execute template
    ob_start();
    eval('?>' . $template_html);
    $contents = ob_get_clean();

    echo $contents;
	
}

function PageViewer_getPage ($view_html, $_data = array()){
	
	 // get data into current symbolic table
    if (!empty($_data)) {
        extract($_data);
    }
	
	 // execute view
    ob_start();
    eval('?>' . $view_html);
    return ob_get_clean();
}

function component ($name, $_data = array()){
	
	if (!empty($_data)) {
        extract($_data);
    }
	
	$lng = Session::language();
		
	if(file_exists(Config::$base_path . "/templates/components/" . $name.'_'.$lng.'.php')){
		include(Config::$base_path . "/templates/components/" . $name.'_'.$lng.'.php');
	}
	else {
		include( Config::$base_path . "/templates/components/" . $name.'.php');
		
	}	
	
	
	
}


?>